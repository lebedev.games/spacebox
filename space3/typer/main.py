import os
import re
from dataclasses import dataclass
from typing import List

from jinja2 import Template


@dataclass
class BpyField:
    name: str
    type: str
    description: str


@dataclass
class BpyMethod:
    name: str
    is_class_method: bool
    ret: str


@dataclass
class BpyType:
    name: str
    base: str
    fields: List[BpyField]
    methods: List[BpyMethod]


def parse_type(text: str, comment=True, owner='', default=None) -> str:
    if text.startswith('int in ['):
        return 'int'

    if text.startswith('float in ['):
        return 'float'

    if text.startswith('boolean'):
        return 'bool'

    if text.startswith('string'):
        return 'str'

    if text.startswith('<a class="reference internal" href="#bpy.types.PropertyGroupItem.string"'):
        return 'str'

    if text.startswith('<code class="xref py py-class docutils literal notranslate">'):
        return 'Any'

    # '<dd class="field-odd"><p><a class="reference internal" href="bpy.types.bpy_prop_collection.html#bpy.types.bpy_prop_collection" title="bpy.types.bpy_prop_collection"><code class="xref py py-class docutils literal notranslate"><span class="pre">bpy_prop_collection</span></code></a> of <a class="reference internal" href="bpy.types.MeshUVLoop.html#bpy.types.MeshUVLoop" title="bpy.types.MeshUVLoop"><code class="xref py py-class docutils literal notranslate"><span class="pre">MeshUVLoop</span></code></a>, (readonly)</p>'

    if text.startswith('<a class="reference internal"'):
        if groups := re.findall(r'title="bpy.types.(\w+)"', text):
            type = groups[0]
            if type == owner:
                # recursion def
                type = f"'{type}'"

            if type == 'bpy_prop_collection':
                return f'List[{groups[1]}]'
            else:
                return type


    if text.startswith('enum in []'):
        return 'str'

    if match := re.match(r'enum in \[(.+)\], default ‘(\w+)’', text):
        values = [value.replace('‘', '').replace('’', '') for value in match.group(1).split(', ')]
        default = match.group(2)
        if comment:
            return f"str = '{default}' # {values}"
        else:
            return f"str"

    if text.startswith('enum set in {}'):
        return 'Set[str]'

    if match := re.match(r'enum set in \{(.+)\}', text):
        values = {value.replace('‘', '').replace('’', '') for value in match.group(1).split(', ')}
        if comment:
            return f"Set[str] # {values}"
        else:
            return 'Set[str]'

    if text.startswith('float multi-dimensional array'):
        return 'List[float]'

    if match := re.match(r'float array of (\d+) items', text):
        count = int(match.group(1))
        tuple = ', '.join(['float'] * count)
        return f'Tuple[{tuple}]'

    if match := re.match(r'int array of (\d+) items', text):
        count = int(match.group(1))
        tuple = ', '.join(['int'] * count)
        return f'Tuple[{tuple}]'

    return default or text


def generate_typing(source: str, output: str):
    context = ''
    is_field_type = False
    is_return_type = False
    bpy_type = BpyType('', 'bpy_struct', [], [])
    with open(source) as source:
        for line in source.readlines():
            line = line.strip()

            if '<dl class="py class">' in line:
                context = 'type'

            if '<dl class="py data">' in line:
                context = 'field'

            if '<dl class="py attribute">' in line:
                context = 'field'

            if '<dl class="py method">' in line:
                context = 'method'

            if context == 'type':
                if match := re.match(r'<dt id="bpy.types.(\w+)">', line):
                    bpy_type.name = match.group(1)

                if match := re.search(
                        r'</span><em class="sig-param"><span class="n"><span class="pre">(\w+)</span></span></em><span class="sig-paren">',
                        line):
                    bpy_type.base = match.group(1)

            if context == 'field':
                if match := re.match(r'<dt id="bpy.types.(\w+).(\w+)">', line):
                    name = match.group(2)
                    bpy_type.fields.append(BpyField(name, 'Any', ''))

                if match := re.match(r'<dd><p>(.+)</p>', line):
                    description = match.group(1)
                    bpy_type.fields[-1].description = description

                if line == '<dt class="field-odd">Type</dt>':
                    is_field_type = True

                if is_field_type and (match := re.match(r'<dd class="field-odd"><p>(.+)</p>', line)):
                    type_representation = match.group(1)
                    bpy_type.fields[-1].type = parse_type(type_representation, owner=bpy_type.name)
                    is_field_type = False

            if context == 'method':
                if match := re.match(r'<dt id="bpy.types.(\w+).(\w+)">', line):
                    name = match.group(2)
                    bpy_type.methods.append(BpyMethod(name, False, 'None'))

                if '<em class="property"><span class="pre">classmethod</span>' in line:
                    bpy_type.methods[-1].is_class_method = True

                if line == '<dt class="field-odd">Return type</dt>':
                    is_return_type = True

                if is_return_type and (match := re.match(r'<dd class="field-odd"><p>(.+)</p>', line)):
                    type_representation = match.group(1)
                    bpy_type.methods[-1].ret = parse_type(type_representation, comment=False, owner=bpy_type.name, default='Any')
                    is_return_type = False

    with open('type.j2', 'r') as template_file:
        template = Template(template_file.read())

    if bpy_type.name == 'bpy_struct':
        bpy_type.base = 'object'

    if bpy_type.name == 'SceneObjects':
        bpy_type.base = 'bpy_struct, List[Object]'

    if bpy_type.name == 'MeshVertices':
        bpy_type.base = 'bpy_struct, List[MeshVertex]'

    if bpy_type.name == 'MeshLoops':
        bpy_type.base = 'bpy_struct, List[MeshLoop]'

    if bpy_type.name == 'IDMaterials':
        bpy_type.base = 'bpy_struct, List[Material]'

    if bpy_type.name == 'UVLoopLayers':
        bpy_type.base = 'bpy_struct, List[MeshUVLoopLayer]'

    if bpy_type.name == 'MeshPolygons':
        bpy_type.base = 'bpy_struct, List[MeshPolygon]'

    if bpy_type.name == 'MeshLoopTriangles':
        bpy_type.base = 'bpy_struct, List[MeshLoopTriangle]'

    if bpy_type.name == 'ArmatureBones':
        bpy_type.base = 'bpy_struct, List[Bone]'

    if bpy_type.name == 'VertexGroups':
        bpy_type.base = 'bpy_struct, List[VertexGroup]'

    if bpy_type.name == 'ObjectModifiers':
        bpy_type.base = 'bpy_struct, List[Modifier]'

    with open(output, 'w') as output:
        output.write(template.render(
            name=bpy_type.name,
            base=bpy_type.base,
            fields=bpy_type.fields,
            methods=bpy_type.methods,
        ))


if __name__ == '__main__':
    imports = [
        'from ._custom import *',
        'from .material import Material'
    ]
    for name in os.listdir('../api/blender_python_reference_2_93'):
        if match := re.match(r'bpy.types.(\w+).html', name):
            class_name = match.group(1)
            class_file_name = class_name
            generate_typing(
                f'../api/blender_python_reference_2_93/bpy.types.{class_name}.html',
                f'../bpy/types/{class_file_name}.py'
            )
            imports.append(f'from .{class_file_name} import {class_name}')

    with open('../bpy/types/__init__.py', 'w') as init:
        init.write('\n'.join(imports))
