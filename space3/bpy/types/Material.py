from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Material(ID):
    
    # A pixel is rendered only if its alpha value is above this threshold
    alpha_threshold: float
    
    # Animation data for this data-block
    animation_data: AnimData
    
    # Blend Mode for Transparent Faces
    blend_method: str = 'OPAQUE' # ['OPAQUE', 'CLIP', 'HASHED', 'BLEND']
    
    # Cycles material settings
    cycles: Any
    
    # Diffuse color of the material
    diffuse_color: Tuple[float, float, float, float]
    
    # Grease pencil color settings for material
    grease_pencil: MaterialGPencilStyle
    
    # True if this material has grease pencil data
    is_grease_pencil: bool
    
    # Line color used for Freestyle line rendering
    line_color: Tuple[float, float, float, float]
    
    # The line color of a higher priority is used at material boundaries
    line_priority: int
    
    # Line art settings for material
    lineart: MaterialLineArt
    
    # Amount of mirror reflection for raytrace
    metallic: float
    
    # Node tree for node based materials
    node_tree: NodeTree
    
    # Index of active texture paint slot
    paint_active_slot: int
    
    # Index of clone texture paint slot
    paint_clone_slot: int
    
    # Index number for the “Material Index” render pass
    pass_index: int
    
    # Type of preview render
    preview_render_type: str = 'SPHERE' # ['FLAT', 'SPHERE', 'CUBE', 'HAIR', 'SHADERBALL', 'CLOTH', 'FLUID']
    
    # Approximate the thickness of the object to compute two refraction event (0 is disabled)
    refraction_depth: float
    
    # Roughness of the material
    roughness: float
    
    # Shadow mapping method
    shadow_method: str = 'OPAQUE' # ['NONE', 'OPAQUE', 'CLIP', 'HASHED']
    
    # Limit transparency to a single layer (avoids transparency sorting problems)
    show_transparent_back: bool
    
    # Specular color of the material
    specular_color: Tuple[float, float, float]
    
    # How intense (bright) the specular reflection is
    specular_intensity: float
    
    # Texture images used for texture painting
    texture_paint_images: List[Image]
    
    # Texture slots defining the mapping and influence of textures
    texture_paint_slots: List[TexPaintSlot]
    
    # Use back face culling to hide the back side of faces
    use_backface_culling: bool
    
    # Use shader nodes to render the material
    use_nodes: bool
    
    # Use the current world background to light the preview render
    use_preview_world: bool
    
    # Use raytraced screen space refractions
    use_screen_refraction: bool
    
    # Add translucency effect to subsurface
    use_sss_translucency: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...