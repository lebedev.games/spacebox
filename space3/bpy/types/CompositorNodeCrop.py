from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CompositorNodeCrop(CompositorNode):
    
    max_x: int
    
    max_y: int
    
    min_x: int
    
    min_y: int
    
    rel_max_x: float
    
    rel_max_y: float
    
    rel_min_x: float
    
    rel_min_y: float
    
    # Use relative values to crop image
    relative: bool
    
    # Whether to crop the size of the input image
    use_crop_size: bool

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    def update(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...