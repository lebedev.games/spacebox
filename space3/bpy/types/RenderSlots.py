from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class RenderSlots(bpy_struct):
    
    # Active render slot of the image
    active: RenderSlot
    
    # Active render slot of the image
    active_index: int

    def new(self, *args, **kwargs) -> RenderSlot:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...