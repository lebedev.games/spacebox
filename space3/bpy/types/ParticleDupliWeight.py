from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ParticleDupliWeight(bpy_struct):
    
    # The number of times this object is repeated with respect to other objects
    count: int
    
    # Particle instance object name
    name: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...