from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Sound(ID):
    
    # Sound sample file used by this Sound data-block
    filepath: str
    
    packed_file: PackedFile
    
    # The sound file is decoded and loaded into RAM
    use_memory_cache: bool
    
    # If the file contains multiple audio channels they are rendered to a single one
    use_mono: bool
    
    # The aud.Factory object of the sound.
    factory: Any

    def pack(self, *args, **kwargs) -> None:
        ...

    def unpack(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...