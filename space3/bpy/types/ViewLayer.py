from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ViewLayer(bpy_struct):
    
    # Active AOV
    active_aov: AOV
    
    # Index of active aov
    active_aov_index: int
    
    # Active layer collection in this view layer’s hierarchy
    active_layer_collection: LayerCollection
    
    aovs: AOVs
    
    # Cycles ViewLayer Settings
    cycles: Any
    
    # Dependencies in the scene data
    depsgraph: Depsgraph
    
    # View layer settings for Eevee
    eevee: ViewLayerEEVEE
    
    freestyle_settings: FreestyleSettings
    
    # For Zmask, only render what is behind solid z values instead of in front
    invert_zmask: bool
    
    # Root of collections hierarchy of this view layer,its ‘collection’ pointer property is the same as the scene’s master collection
    layer_collection: LayerCollection
    
    # Material to override all other materials in this view layer
    material_override: Material
    
    # View layer name
    name: str
    
    # All the objects in this layer
    objects: LayerObjects
    
    # Z, Index, normal, UV and vector passes are only affected by surfaces with alpha transparency equal to or higher than this threshold
    pass_alpha_threshold: float
    
    # Sets how many unique objects can be distinguished per pixel
    pass_cryptomatte_depth: int
    
    # Override number of render samples for this view layer, 0 will use the scene setting
    samples: int
    
    # Enable or disable rendering of this View Layer
    use: bool
    
    # Fill in Z values for solid faces in invisible layers, for masking
    use_all_z: bool
    
    # Render Ambient Occlusion in this Layer
    use_ao: bool
    
    # Render edge-enhance in this layer (only works for solid faces)
    use_edge_enhance: bool
    
    # Render stylized strokes in this Layer
    use_freestyle: bool
    
    # Render Halos in this Layer (on top of Solid)
    use_halo: bool
    
    # Deliver Ambient Occlusion pass
    use_pass_ambient_occlusion: bool
    
    # Deliver full combined RGBA buffer
    use_pass_combined: bool
    
    # Generate a more accurate cryptomatte pass
    use_pass_cryptomatte_accurate: bool
    
    # Render cryptomatte asset pass, for isolating groups of objects with the same parent
    use_pass_cryptomatte_asset: bool
    
    # Render cryptomatte material pass, for isolating materials in compositing
    use_pass_cryptomatte_material: bool
    
    # Render cryptomatte object pass, for isolating objects in compositing
    use_pass_cryptomatte_object: bool
    
    # Deliver diffuse color pass
    use_pass_diffuse_color: bool
    
    # Deliver diffuse direct pass
    use_pass_diffuse_direct: bool
    
    # Deliver diffuse indirect pass
    use_pass_diffuse_indirect: bool
    
    # Deliver emission pass
    use_pass_emit: bool
    
    # Deliver environment lighting pass
    use_pass_environment: bool
    
    # Deliver glossy color pass
    use_pass_glossy_color: bool
    
    # Deliver glossy direct pass
    use_pass_glossy_direct: bool
    
    # Deliver glossy indirect pass
    use_pass_glossy_indirect: bool
    
    # Deliver material index pass
    use_pass_material_index: bool
    
    # Deliver mist factor pass (0.0 to 1.0)
    use_pass_mist: bool
    
    # Deliver normal pass
    use_pass_normal: bool
    
    # Deliver object index pass
    use_pass_object_index: bool
    
    # Deliver shadow pass
    use_pass_shadow: bool
    
    # Deliver subsurface color pass
    use_pass_subsurface_color: bool
    
    # Deliver subsurface direct pass
    use_pass_subsurface_direct: bool
    
    # Deliver subsurface indirect pass
    use_pass_subsurface_indirect: bool
    
    # Deliver transmission color pass
    use_pass_transmission_color: bool
    
    # Deliver transmission direct pass
    use_pass_transmission_direct: bool
    
    # Deliver transmission indirect pass
    use_pass_transmission_indirect: bool
    
    # Deliver texture UV pass
    use_pass_uv: bool
    
    # Deliver speed vector pass
    use_pass_vector: bool
    
    # Deliver Z values pass
    use_pass_z: bool
    
    # Render Sky in this Layer
    use_sky: bool
    
    # Render Solid faces in this Layer
    use_solid: bool
    
    # Render Strands in this Layer
    use_strand: bool
    
    # Render volumes in this Layer
    use_volumes: bool
    
    # Only render what’s in front of the solid z values
    use_zmask: bool
    
    # Render Z-transparent faces in this layer (on top of Solid and Halos)
    use_ztransp: bool

    @classmethod
    def update_render_passes(cls, *args, **kwargs) -> None:
        ...

    def update(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...