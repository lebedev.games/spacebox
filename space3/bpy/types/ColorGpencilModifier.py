from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ColorGpencilModifier(GpencilModifier):
    
    # Custom curve to apply effect
    curve: CurveMapping
    
    # Color Hue
    hue: float
    
    # Inverse filter
    invert_layer_pass: bool
    
    # Inverse filter
    invert_layers: bool
    
    # Inverse filter
    invert_material_pass: bool
    
    # Inverse filter
    invert_materials: bool
    
    # Layer name
    layer: str
    
    # Layer pass index
    layer_pass: int
    
    # Material used for filtering effect
    material: Material
    
    # Set what colors of the stroke are affected
    modify_color: str = 'BOTH' # ['BOTH', 'STROKE', 'FILL']
    
    # Pass index
    pass_index: int
    
    # Color Saturation
    saturation: float
    
    # Use a custom curve to define color effect along the strokes
    use_custom_curve: bool
    
    # Color Value
    value: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...