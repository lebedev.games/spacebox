from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class UVLoopLayers(bpy_struct, List[MeshUVLoopLayer]):
    
    # Active UV loop layer
    active: MeshUVLoopLayer
    
    # Active UV loop layer index
    active_index: int

    def new(self, *args, **kwargs) -> MeshUVLoopLayer:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...