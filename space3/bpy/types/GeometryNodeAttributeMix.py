from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class GeometryNodeAttributeMix(GeometryNode):
    
    blend_type: str = 'MIX' # ['MIX', 'DARKEN', 'MULTIPLY', 'BURN', 'LIGHTEN', 'SCREEN', 'DODGE', 'ADD', 'OVERLAY', 'SOFT_LIGHT', 'LINEAR_LIGHT', 'DIFFERENCE', 'SUBTRACT', 'DIVIDE', 'HUE', 'SATURATION', 'COLOR', 'VALUE']
    
    input_type_a: str = 'ATTRIBUTE' # ['ATTRIBUTE', 'FLOAT', 'VECTOR', 'COLOR']
    
    input_type_b: str = 'ATTRIBUTE' # ['ATTRIBUTE', 'FLOAT', 'VECTOR', 'COLOR']
    
    input_type_factor: str = 'ATTRIBUTE' # ['ATTRIBUTE', 'FLOAT']

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...