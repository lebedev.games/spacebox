from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Spline(bpy_struct):
    
    # Collection of points for Bezier curves only
    bezier_points: SplineBezierPoints
    
    # Location of this character in the text data (only for text curves)
    character_index: int
    
    # Hide this curve in Edit mode
    hide: bool
    
    material_index: int
    
    # NURBS order in the U direction (for splines and surfaces, higher values let points influence a greater area)
    order_u: int
    
    # NURBS order in the V direction (for surfaces only, higher values let points influence a greater area)
    order_v: int
    
    # Total number points for the curve or surface in the U direction
    point_count_u: int
    
    # Total number points for the surface on the V direction
    point_count_v: int
    
    # Collection of points that make up this poly or nurbs spline
    points: SplinePoints
    
    # The type of radius interpolation for Bezier curves
    radius_interpolation: str = 'LINEAR' # ['LINEAR', 'CARDINAL', 'BSPLINE', 'EASE']
    
    # Curve or Surface subdivisions per segment
    resolution_u: int
    
    # Surface subdivisions per segment
    resolution_v: int
    
    # The type of tilt interpolation for 3D, Bezier curves
    tilt_interpolation: str = 'LINEAR' # ['LINEAR', 'CARDINAL', 'BSPLINE', 'EASE']
    
    # The interpolation type for this curve element
    type: str = 'POLY' # ['POLY', 'BEZIER', 'BSPLINE', 'CARDINAL', 'NURBS']
    
    # Make this nurbs curve or surface act like a Bezier spline in the U direction (Order U must be 3 or 4, Cyclic U must be disabled)
    use_bezier_u: bool
    
    # Make this nurbs surface act like a Bezier spline in the V direction (Order V must be 3 or 4, Cyclic V must be disabled)
    use_bezier_v: bool
    
    # Make this curve or surface a closed loop in the U direction
    use_cyclic_u: bool
    
    # Make this surface a closed loop in the V direction
    use_cyclic_v: bool
    
    # Make this nurbs curve or surface meet the endpoints in the U direction (Cyclic U must be disabled)
    use_endpoint_u: bool
    
    # Make this nurbs surface meet the endpoints in the V direction (Cyclic V must be disabled)
    use_endpoint_v: bool
    
    # Smooth the normals of the surface or beveled curve
    use_smooth: bool

    def calc_length(self, *args, **kwargs) -> float:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...