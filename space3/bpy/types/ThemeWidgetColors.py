from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ThemeWidgetColors(bpy_struct):
    
    inner: Tuple[float, float, float, float]
    
    inner_sel: Tuple[float, float, float, float]
    
    item: Tuple[float, float, float, float]
    
    outline: Tuple[float, float, float]
    
    # Amount of edge rounding
    roundness: float
    
    shadedown: int
    
    shadetop: int
    
    show_shaded: bool
    
    text: Tuple[float, float, float]
    
    text_sel: Tuple[float, float, float]

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...