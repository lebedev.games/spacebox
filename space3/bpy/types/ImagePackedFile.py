from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ImagePackedFile(bpy_struct):
    
    filepath: str
    
    packed_file: PackedFile

    def save(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...