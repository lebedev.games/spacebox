from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ShaderFxShadow(ShaderFx):
    
    # Amplitude of Wave
    amplitude: float
    
    # Number of pixels for blurring shadow (set to 0 to disable)
    blur: Tuple[int, int]
    
    # Object to determine center of rotation
    object: Object
    
    # Offset of the shadow
    offset: Tuple[int, int]
    
    # Direction of the wave
    orientation: str = 'HORIZONTAL' # ['HORIZONTAL', 'VERTICAL']
    
    # Period of Wave
    period: float
    
    # Phase Shift of Wave
    phase: float
    
    # Rotation around center or object
    rotation: float
    
    # Number of Blur Samples (zero, disable blur)
    samples: int
    
    # Offset of the shadow
    scale: Tuple[float, float]
    
    # Color used for Shadow
    shadow_color: Tuple[float, float, float, float]
    
    # Use object as center of rotation
    use_object: bool
    
    # Use wave effect
    use_wave: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...