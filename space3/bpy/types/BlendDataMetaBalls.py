from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class BlendDataMetaBalls(bpy_struct):

    def new(self, *args, **kwargs) -> MetaBall:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    def tag(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...