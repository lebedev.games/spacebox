from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class HookGpencilModifier(GpencilModifier):
    
    center: Tuple[float, float, float]
    
    # Custom light falloff curve
    falloff_curve: CurveMapping
    
    # If not zero, the distance from the hook where influence ends
    falloff_radius: float
    
    falloff_type: str = 'SMOOTH' # ['NONE', 'CURVE', 'SMOOTH', 'SPHERE', 'ROOT', 'INVERSE_SQUARE', 'SHARP', 'LINEAR', 'CONSTANT']
    
    # Inverse filter
    invert_layer_pass: bool
    
    # Inverse filter
    invert_layers: bool
    
    # Inverse filter
    invert_material_pass: bool
    
    # Inverse filter
    invert_materials: bool
    
    # Inverse filter
    invert_vertex: bool
    
    # Layer name
    layer: str
    
    # Layer pass index
    layer_pass: int
    
    # Material used for filtering effect
    material: Material
    
    # Reverse the transformation between this object and its target
    matrix_inverse: List[float]
    
    # Parent Object for hook, also recalculates and clears offset
    object: Object
    
    # Pass index
    pass_index: int
    
    # Relative force of the hook
    strength: float
    
    # Name of Parent Bone for hook (if applicable), also recalculates and clears offset
    subtarget: str
    
    # Compensate for non-uniform object scale
    use_falloff_uniform: bool
    
    # Vertex group name for modulating the deform
    vertex_group: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...