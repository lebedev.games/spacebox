from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class IDOverrideLibrary(bpy_struct):
    
    # List of overridden properties
    properties: IDOverrideLibraryProperties
    
    # Linked ID used as reference by this override
    reference: ID

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...