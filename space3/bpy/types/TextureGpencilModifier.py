from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class TextureGpencilModifier(GpencilModifier):
    
    # Additional rotation applied to dots and square strokes
    alignment_rotation: float
    
    # Additional offset of the fill UV
    fill_offset: Tuple[float, float]
    
    # Additional rotation of the fill UV
    fill_rotation: float
    
    # Additional scale of the fill UV
    fill_scale: float
    
    fit_method: str = 'CONSTANT_LENGTH' # ['CONSTANT_LENGTH', 'FIT_STROKE']
    
    # Inverse filter
    invert_layer_pass: bool
    
    # Inverse filter
    invert_layers: bool
    
    # Inverse filter
    invert_material_pass: bool
    
    # Inverse filter
    invert_materials: bool
    
    # Inverse filter
    invert_vertex: bool
    
    # Layer name
    layer: str
    
    # Layer pass index
    layer_pass: int
    
    # Material used for filtering effect
    material: Material
    
    mode: str = 'STROKE' # ['STROKE', 'FILL', 'STROKE_AND_FILL']
    
    # Pass index
    pass_index: int
    
    # Offset value to add to stroke UVs
    uv_offset: float
    
    # Factor to scale the UVs
    uv_scale: float
    
    # Vertex group name for modulating the deform
    vertex_group: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...