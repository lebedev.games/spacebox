from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ColorMixSequence(EffectSequence):
    
    # Method for controlling how the strip combines with other strips
    blend_effect: str = 'DARKEN' # ['DARKEN', 'MULTIPLY', 'BURN', 'LINEAR_BURN', 'LIGHTEN', 'SCREEN', 'DODGE', 'ADD', 'OVERLAY', 'SOFT_LIGHT', 'HARD_LIGHT', 'VIVID_LIGHT', 'LINEAR_LIGHT', 'PIN_LIGHT', 'DIFFERENCE', 'EXCLUSION', 'SUBTRACT', 'HUE', 'SATURATION', 'COLOR', 'VALUE']
    
    # Percentage of how much the strip’s colors affect other strips
    factor: float
    
    # First input for the effect strip
    input_1: Sequence
    
    # Second input for the effect strip
    input_2: Sequence
    
    input_count: int

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...