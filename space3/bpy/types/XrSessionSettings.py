from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class XrSessionSettings(bpy_struct):
    
    # Rotation angle around the Z-Axis to apply the rotation deltas from the VR headset to
    base_pose_angle: float
    
    # Coordinates to apply translation deltas from the VR headset to
    base_pose_location: Tuple[float, float, float]
    
    # Object to take the location and rotation to which translation and rotation deltas from the VR headset will be applied to
    base_pose_object: Object
    
    # Define where the location and rotation for the VR view come from, to which translation and rotation deltas from the VR headset will be applied to
    base_pose_type: str = 'SCENE_CAMERA' # ['SCENE_CAMERA', 'OBJECT', 'CUSTOM']
    
    # VR viewport far clipping distance
    clip_end: float
    
    # VR viewport near clipping distance
    clip_start: float
    
    shading: View3DShading
    
    # Show annotations for this view
    show_annotation: bool
    
    # Show the ground plane grid
    show_floor: bool
    
    # Allow VR headsets to affect the location in virtual space, in addition to the rotation
    use_positional_tracking: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...