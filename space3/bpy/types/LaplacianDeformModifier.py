from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class LaplacianDeformModifier(Modifier):
    
    # Invert vertex group influence
    invert_vertex_group: bool
    
    # Whether geometry has been bound to anchors
    is_bind: bool
    
    iterations: int
    
    # Name of Vertex Group which determines Anchors
    vertex_group: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...