from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MovieTrackingDopesheet(bpy_struct):
    
    # Include channels from objects/bone that aren’t visible
    show_hidden: bool
    
    # Only include channels relating to selected objects and data
    show_only_selected: bool
    
    # Method to be used to sort channels in dopesheet view
    sort_method: str = 'NAME' # ['NAME', 'LONGEST', 'TOTAL', 'AVERAGE_ERROR']
    
    # Invert sort order of dopesheet channels
    use_invert_sort: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...