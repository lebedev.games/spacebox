from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ThemeOutliner(bpy_struct):
    
    active: Tuple[float, float, float]
    
    active_object: Tuple[float, float, float]
    
    edited_object: Tuple[float, float, float, float]
    
    match: Tuple[float, float, float]
    
    # Overlay color on every other row
    row_alternate: Tuple[float, float, float, float]
    
    selected_highlight: Tuple[float, float, float]
    
    selected_object: Tuple[float, float, float]
    
    # Settings for space
    space: ThemeSpaceGeneric

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...