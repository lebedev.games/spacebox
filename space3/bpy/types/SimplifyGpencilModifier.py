from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SimplifyGpencilModifier(GpencilModifier):
    
    # Distance between points
    distance: float
    
    # Factor of Simplify
    factor: float
    
    # Inverse filter
    invert_layer_pass: bool
    
    # Inverse filter
    invert_layers: bool
    
    # Inverse filter
    invert_material_pass: bool
    
    # Inverse filter
    invert_materials: bool
    
    # Layer name
    layer: str
    
    # Layer pass index
    layer_pass: int
    
    # Length of each segment
    length: float
    
    # Material used for filtering effect
    material: Material
    
    # How to simplify the stroke
    mode: str = 'FIXED' # ['FIXED', 'ADAPTIVE', 'SAMPLE', 'MERGE']
    
    # Pass index
    pass_index: int
    
    # Number of times to apply simplify
    step: int

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...