from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SceneRenderView(bpy_struct):
    
    # Suffix to identify the cameras to use, and added to the render images for this view
    camera_suffix: str
    
    # Suffix added to the render images for this view
    file_suffix: str
    
    # Render view name
    name: str
    
    # Disable or enable the render view
    use: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...