from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SpaceGraphEditor(Space):
    
    # Automatic time snapping settings for transformations
    auto_snap: str = 'NONE' # ['NONE', 'STEP', 'TIME_STEP', 'FRAME', 'SECOND', 'MARKER']
    
    # Graph Editor 2D-Value cursor - X-Value component
    cursor_position_x: float
    
    # Graph Editor 2D-Value cursor - Y-Value component
    cursor_position_y: float
    
    # Settings for filtering animation data
    dopesheet: DopeSheet
    
    # Graph Editor instance has some ghost curves stored
    has_ghost_curves: bool
    
    # Editing context being displayed
    mode: str = 'FCURVES' # ['FCURVES', 'DRIVERS']
    
    # Pivot center for rotation/scaling
    pivot_point: str = 'BOUNDING_BOX_CENTER' # ['BOUNDING_BOX_CENTER', 'CURSOR', 'INDIVIDUAL_ORIGINS']
    
    # Show 2D cursor
    show_cursor: bool
    
    show_extrapolation: bool
    
    # Show handles of Bezier control points
    show_handles: bool
    
    # If any exists, show markers in a separate row at the bottom of the editor
    show_markers: bool
    
    show_region_hud: bool
    
    show_region_ui: bool
    
    # Show timing in seconds not frames
    show_seconds: bool
    
    # Show sliders beside F-Curve channels
    show_sliders: bool
    
    # Automatically merge nearby keyframes
    use_auto_merge_keyframes: bool
    
    # Automatically recalculate curve normalization on every curve edit
    use_auto_normalization: bool
    
    # Display F-Curves using Anti-Aliasing and other fancy effects (disable for better performance)
    use_beauty_drawing: bool
    
    # Display curves in normalized range from -1 to 1, for easier editing of multiple curves with different ranges
    use_normalization: bool
    
    # Only keyframes of selected F-Curves are visible and editable
    use_only_selected_curves_handles: bool
    
    # Only show and edit handles of selected keyframes
    use_only_selected_keyframe_handles: bool
    
    # When transforming keyframes, changes to the animation data are flushed to other views
    use_realtime_update: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...

    def draw_handler_add(self, *args, **kwargs) -> Any:
        ...

    def draw_handler_remove(self, *args, **kwargs) -> None:
        ...