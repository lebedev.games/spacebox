from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CopyLocationConstraint(Constraint):
    
    # Target along length of bone: Head is 0, Tail is 1
    head_tail: float
    
    # Invert the X location
    invert_x: bool
    
    # Invert the Y location
    invert_y: bool
    
    # Invert the Z location
    invert_z: bool
    
    # Armature bone, mesh or lattice vertex group, …
    subtarget: str
    
    # Target object
    target: Object
    
    # Follow shape of B-Bone segments when calculating Head/Tail position
    use_bbone_shape: bool
    
    # Add original location into copied location
    use_offset: bool
    
    # Copy the target’s X location
    use_x: bool
    
    # Copy the target’s Y location
    use_y: bool
    
    # Copy the target’s Z location
    use_z: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...