from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class GreasePencil(ID):
    
    # Base color for ghosts after the active frame
    after_color: Tuple[float, float, float]
    
    # Animation data for this data-block
    animation_data: AnimData
    
    # Base color for ghosts before the active frame
    before_color: Tuple[float, float, float]
    
    # Angle threshold to be treated as corners
    curve_edit_corner_angle: float
    
    # Curve conversion error threshold
    curve_edit_threshold: float
    
    # Number of segments generated between control points when editing strokes in curve mode
    edit_curve_resolution: int
    
    # Color for editing line
    edit_line_color: Tuple[float, float, float, float]
    
    # Maximum number of frames to show after current frame (0 = don’t show any frames after current)
    ghost_after_range: int
    
    # Maximum number of frames to show before current frame (0 = don’t show any frames before current)
    ghost_before_range: int
    
    # Settings for grid and canvas in the 3D viewport
    grid: GreasePencilGrid
    
    # Current data-block is an annotation
    is_annotation: bool
    
    # Draw Grease Pencil strokes on click/drag
    is_stroke_paint_mode: bool
    
    # Sculpt Grease Pencil strokes instead of viewport data
    is_stroke_sculpt_mode: bool
    
    # Grease Pencil vertex paint
    is_stroke_vertex_mode: bool
    
    # Grease Pencil weight paint
    is_stroke_weight_mode: bool
    
    layers: GreasePencilLayers
    
    materials: IDMaterials
    
    # Change fade opacity of displayed onion frames
    onion_factor: float
    
    # Type of keyframe (for filtering)
    onion_keyframe_type: str = 'KEYFRAME' # ['ALL', 'KEYFRAME', 'BREAKDOWN', 'MOVING_HOLD', 'EXTREME', 'JITTER']
    
    # Mode to display frames
    onion_mode: str = 'ABSOLUTE' # ['ABSOLUTE', 'RELATIVE', 'SELECTED']
    
    # Scale conversion factor for pixel size (use larger values for thicker lines)
    pixel_factor: float
    
    # Defines how the strokes are ordered in 3D space (for objects not displayed ‘In Front’)
    stroke_depth_order: str = '2D' # ['2D', '3D']
    
    # Set stroke thickness in screen space or world space
    stroke_thickness_space: str = 'WORLDSPACE' # ['WORLDSPACE', 'SCREENSPACE']
    
    # Set the resolution of each editcurve segment dynamically depending on the length of the segment. The resolution is the number of points generated per unit distance
    use_adaptive_curve_resolution: bool
    
    # Lock automatically all layers except active one to avoid accidental changes
    use_autolock_layers: bool
    
    # Edit strokes using curve handles
    use_curve_edit: bool
    
    # Use custom colors for ghost frames
    use_ghost_custom_colors: bool
    
    # Ghosts are shown in renders and animation playback. Useful for special effects (e.g. motion blur)
    use_ghosts_always: bool
    
    # Edit strokes from multiple grease pencil keyframes at the same time (keyframes must be selected to be included)
    use_multiedit: bool
    
    # Display onion keyframes with a fade in color transparency
    use_onion_fade: bool
    
    # Display onion keyframes for looping animations
    use_onion_loop: bool
    
    # Show ghosts of the keyframes before and after the current frame
    use_onion_skinning: bool
    
    # Edit Grease Pencil strokes instead of viewport data
    use_stroke_edit_mode: bool
    
    # Offset amount when drawing in surface mode
    zdepth_offset: float

    def clear(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...