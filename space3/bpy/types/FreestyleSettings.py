from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class FreestyleSettings(bpy_struct):
    
    # Renders Freestyle output to a separate pass instead of overlaying it on the Combined pass
    as_render_pass: bool
    
    # Angular threshold for detecting crease edges
    crease_angle: float
    
    # Kr derivative epsilon for computing suggestive contours
    kr_derivative_epsilon: float
    
    linesets: Linesets
    
    # Select the Freestyle control mode
    mode: str = 'SCRIPT' # ['SCRIPT', 'EDITOR']
    
    # A list of style modules (to be applied from top to bottom)
    modules: FreestyleModules
    
    # Sphere radius for computing curvatures
    sphere_radius: float
    
    # Enable advanced edge detection options (sphere radius and Kr derivative epsilon)
    use_advanced_options: bool
    
    # If enabled, out-of-view edges are ignored
    use_culling: bool
    
    # Enable material boundaries
    use_material_boundaries: bool
    
    # Enable ridges and valleys
    use_ridges_and_valleys: bool
    
    # Take face smoothness into account in view map calculation
    use_smoothness: bool
    
    # Enable suggestive contours
    use_suggestive_contours: bool
    
    # Keep the computed view map and avoid recalculating it if mesh geometry is unchanged
    use_view_map_cache: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...