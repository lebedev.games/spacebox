from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class WorldLighting(bpy_struct):
    
    # Factor for ambient occlusion blending
    ao_factor: float
    
    # Length of rays, defines how far away other faces give occlusion effect
    distance: float
    
    # Use Ambient Occlusion to add shadowing based on distance between objects
    use_ambient_occlusion: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...