from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CompositorNodeOutputFile(CompositorNode):
    
    # Active input index in details view list
    active_input_index: int
    
    # Base output path for the image
    base_path: str
    
    file_slots: CompositorNodeOutputFileFileSlots
    
    format: ImageFormatSettings
    
    layer_slots: CompositorNodeOutputFileLayerSlots

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    def update(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...