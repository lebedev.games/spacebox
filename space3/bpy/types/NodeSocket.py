from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class NodeSocket(bpy_struct):
    
    bl_idname: str
    
    # Socket tooltip
    description: str
    
    # Socket shape
    display_shape: str = 'CIRCLE' # ['CIRCLE', 'SQUARE', 'DIAMOND', 'CIRCLE_DOT', 'SQUARE_DOT', 'DIAMOND_DOT']
    
    # Enable the socket
    enabled: bool
    
    # Hide the socket
    hide: bool
    
    # Hide the socket input value
    hide_value: bool
    
    # Unique identifier for mapping sockets
    identifier: str
    
    # True if the socket is connected
    is_linked: bool
    
    # True if the socket can accept multiple ordered input links
    is_multi_input: bool
    
    # True if the socket is an output, otherwise input
    is_output: bool
    
    # Custom dynamic defined socket label
    label: str
    
    # Max number of links allowed for this socket
    link_limit: int
    
    # Socket name
    name: str
    
    # Node owning this socket
    node: Node
    
    # Socket links are expanded in the user interface
    show_expanded: bool
    
    # Data type
    type: str = 'VALUE' # ['CUSTOM', 'VALUE', 'INT', 'BOOLEAN', 'VECTOR', 'STRING', 'RGBA', 'SHADER', 'OBJECT', 'IMAGE', 'GEOMETRY', 'COLLECTION']
    
    # List of node links from or to this socket.
    links: Any

    def draw(self, *args, **kwargs) -> None:
        ...

    def draw_color(self, *args, **kwargs) -> Tuple[float, float, float, float]:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...