from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class WaveModifier(Modifier):
    
    # Number of frames in which the wave damps out after it dies
    damping_time: float
    
    # Distance after which it fades out
    falloff_radius: float
    
    # Height of the wave
    height: float
    
    # Invert vertex group influence
    invert_vertex_group: bool
    
    # Lifetime of the wave in frames, zero means infinite
    lifetime: float
    
    # Distance between the top and the base of a wave, the higher the value, the more narrow the wave
    narrowness: float
    
    # Speed of the wave, towards the starting point when negative
    speed: float
    
    # Object which defines the wave center
    start_position_object: Object
    
    # X coordinate of the start position
    start_position_x: float
    
    # Y coordinate of the start position
    start_position_y: float
    
    texture: Texture
    
    texture_coords: str = 'LOCAL' # ['LOCAL', 'GLOBAL', 'OBJECT', 'UV']
    
    # Bone to set the texture coordinates
    texture_coords_bone: str
    
    # Object to set the texture coordinates
    texture_coords_object: Object
    
    # Either the starting frame (for positive speed) or ending frame (for negative speed.)
    time_offset: float
    
    # Cyclic wave effect
    use_cyclic: bool
    
    # Displace along normals
    use_normal: bool
    
    # Enable displacement along the X normal
    use_normal_x: bool
    
    # Enable displacement along the Y normal
    use_normal_y: bool
    
    # Enable displacement along the Z normal
    use_normal_z: bool
    
    # X axis motion
    use_x: bool
    
    # Y axis motion
    use_y: bool
    
    # UV map name
    uv_layer: str
    
    # Vertex group name for modulating the wave
    vertex_group: str
    
    # Distance between the waves
    width: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...