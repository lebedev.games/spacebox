from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Operator(bpy_struct):
    
    bl_description: str
    
    bl_idname: str
    
    bl_label: str
    
    # Options for this operator type
    bl_options: Set[str] # {'BLOCKING', 'INTERNAL}', 'default {REGISTER', 'REGISTER', 'UNDO_GROUPED', 'UNDO', 'GRAB_CURSOR_Y', 'PRESET', 'MACRO', 'GRAB_CURSOR', 'GRAB_CURSOR_X'}
    
    bl_translation_context: str
    
    bl_undo_group: str
    
    # Operator has a set of reports (warnings and errors) from last execution
    has_reports: bool
    
    layout: UILayout
    
    macros: List[Macro]
    
    name: str
    
    # Runtime options
    options: OperatorOptions
    
    properties: OperatorProperties
    
    bl_property: Any

    def report(self, *args, **kwargs) -> None:
        ...

    def is_repeat(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def poll(cls, *args, **kwargs) -> bool:
        ...

    def execute(self, *args, **kwargs) -> None:
        ...

    def check(self, *args, **kwargs) -> None:
        ...

    def invoke(self, *args, **kwargs) -> None:
        ...

    def modal(self, *args, **kwargs) -> None:
        ...

    def draw(self, *args, **kwargs) -> None:
        ...

    def cancel(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def description(cls, *args, **kwargs) -> None:
        ...

    def as_keywords(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...