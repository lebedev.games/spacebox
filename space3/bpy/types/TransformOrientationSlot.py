from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class TransformOrientationSlot(bpy_struct):
    
    custom_orientation: TransformOrientation
    
    # Transformation orientation
    type: str = 'GLOBAL' # ['GLOBAL', 'LOCAL', 'NORMAL', 'GIMBAL', 'VIEW', 'CURSOR']
    
    # Use scene orientation instead of a custom setting
    use: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...