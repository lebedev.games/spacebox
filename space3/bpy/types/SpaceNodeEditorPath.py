from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SpaceNodeEditorPath(bpy_struct):
    
    to_string: str

    def clear(self, *args, **kwargs) -> None:
        ...

    def start(self, *args, **kwargs) -> None:
        ...

    def append(self, *args, **kwargs) -> None:
        ...

    def pop(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...