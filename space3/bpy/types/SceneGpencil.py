from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SceneGpencil(bpy_struct):
    
    # Threshold for edge detection algorithm (higher values might over-blur some part of the image)
    antialias_threshold: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...