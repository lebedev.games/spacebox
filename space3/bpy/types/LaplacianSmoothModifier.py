from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class LaplacianSmoothModifier(Modifier):
    
    # Invert vertex group influence
    invert_vertex_group: bool
    
    iterations: int
    
    # Lambda factor in border
    lambda_border: float
    
    # Smooth factor effect
    lambda_factor: float
    
    # Improve and stabilize the enhanced shape
    use_normalized: bool
    
    # Apply volume preservation after smooth
    use_volume_preserve: bool
    
    # Smooth object along X axis
    use_x: bool
    
    # Smooth object along Y axis
    use_y: bool
    
    # Smooth object along Z axis
    use_z: bool
    
    # Name of Vertex Group which determines influence of modifier per point
    vertex_group: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...