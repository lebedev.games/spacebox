from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class FModifierEnvelopeControlPoint(bpy_struct):
    
    # Frame this control-point occurs on
    frame: float
    
    # Upper bound of envelope at this control-point
    max: float
    
    # Lower bound of envelope at this control-point
    min: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...