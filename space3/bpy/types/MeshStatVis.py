from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MeshStatVis(bpy_struct):
    
    # Maximum angle to display
    distort_max: float
    
    # Minimum angle to display
    distort_min: float
    
    overhang_axis: str = 'NEG_Z' # ['POS_X', 'POS_Y', 'POS_Z', 'NEG_X', 'NEG_Y', 'NEG_Z']
    
    # Maximum angle to display
    overhang_max: float
    
    # Minimum angle to display
    overhang_min: float
    
    # Maximum angle to display
    sharp_max: float
    
    # Minimum angle to display
    sharp_min: float
    
    # Maximum for measuring thickness
    thickness_max: float
    
    # Minimum for measuring thickness
    thickness_min: float
    
    # Number of samples to test per face
    thickness_samples: int
    
    # Type of data to visualize/check
    type: str = 'OVERHANG' # ['OVERHANG', 'THICKNESS', 'INTERSECT', 'DISTORT', 'SHARP']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...