from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class UIList(bpy_struct):
    
    # The value of the reserved bitflag ‘FILTER_ITEM’ (in filter_flags values)
    bitflag_filter_item: int
    
    # If this is set, the uilist gets a custom ID, otherwise it takes the name of the class used to define the uilist (for example, if the class name is “OBJECT_UL_vgroups”, and bl_idname is not set by the script, then bl_idname = “OBJECT_UL_vgroups”)
    bl_idname: str
    
    # Only show items matching this name (use ‘*’ as wildcard)
    filter_name: str
    
    layout_type: str = 'DEFAULT' # ['DEFAULT', 'COMPACT', 'GRID']
    
    # Invert filtering (show hidden items, and vice versa)
    use_filter_invert: bool
    
    # Show filtering options
    use_filter_show: bool
    
    # Sort items by their name
    use_filter_sort_alpha: bool
    
    # Lock the order of shown items (user cannot change it)
    use_filter_sort_lock: bool
    
    # Reverse the order of shown items
    use_filter_sort_reverse: bool

    def draw_item(self, *args, **kwargs) -> None:
        ...

    def draw_filter(self, *args, **kwargs) -> None:
        ...

    def filter_items(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...