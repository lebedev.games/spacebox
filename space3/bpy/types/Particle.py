from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Particle(bpy_struct):
    
    alive_state: str = 'DEAD' # ['DEAD', 'UNBORN', 'ALIVE', 'DYING']
    
    angular_velocity: Tuple[float, float, float]
    
    birth_time: float
    
    die_time: float
    
    hair_keys: List[ParticleHairKey]
    
    is_exist: bool
    
    is_visible: bool
    
    lifetime: float
    
    location: Tuple[float, float, float]
    
    particle_keys: List[ParticleKey]
    
    prev_angular_velocity: Tuple[float, float, float]
    
    prev_location: Tuple[float, float, float]
    
    prev_rotation: Tuple[float, float, float, float]
    
    prev_velocity: Tuple[float, float, float]
    
    rotation: Tuple[float, float, float, float]
    
    size: float
    
    velocity: Tuple[float, float, float]

    def uv_on_emitter(self, *args, **kwargs) -> Tuple[float, float]:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...