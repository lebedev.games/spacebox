from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ExplodeModifier(Modifier):
    
    # Invert vertex group influence
    invert_vertex_group: bool
    
    # UV map to change with particle age
    particle_uv: str
    
    # Clean vertex group edges
    protect: float
    
    # Show mesh when particles are alive
    show_alive: bool
    
    # Show mesh when particles are dead
    show_dead: bool
    
    # Show mesh when particles are unborn
    show_unborn: bool
    
    # Cut face edges for nicer shrapnel
    use_edge_cut: bool
    
    # Use particle size for the shrapnel
    use_size: bool
    
    vertex_group: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...