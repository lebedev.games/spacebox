from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MultiplyGpencilModifier(GpencilModifier):
    
    # Distance of duplications
    distance: float
    
    # How many copies of strokes be displayed
    duplicates: int
    
    # Fade center
    fading_center: float
    
    # Fade influence of stroke’s opacity
    fading_opacity: float
    
    # Fade influence of stroke’s thickness
    fading_thickness: float
    
    # Inverse filter
    invert_layer_pass: bool
    
    # Inverse filter
    invert_layers: bool
    
    # Inverse filter
    invert_material_pass: bool
    
    # Inverse filter
    invert_materials: bool
    
    # Layer name
    layer: str
    
    # Layer pass index
    layer_pass: int
    
    # Material used for filtering effect
    material: Material
    
    # Offset of duplicates. -1 to 1: inner to outer
    offset: float
    
    # Pass index
    pass_index: int
    
    # Fade the stroke thickness for each generated stroke
    use_fade: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...