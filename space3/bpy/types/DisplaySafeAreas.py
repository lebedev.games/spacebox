from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class DisplaySafeAreas(bpy_struct):
    
    # Safe area for general elements
    action: Tuple[float, float]
    
    # Safe area for general elements in a different aspect ratio
    action_center: Tuple[float, float]
    
    # Safe area for text and graphics
    title: Tuple[float, float]
    
    # Safe area for text and graphics in a different aspect ratio
    title_center: Tuple[float, float]

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...