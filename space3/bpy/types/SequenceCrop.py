from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SequenceCrop(bpy_struct):
    
    # Number of pixels to crop from the right side
    max_x: int
    
    # Number of pixels to crop from the top
    max_y: int
    
    # Number of pixels to crop from the left side
    min_x: int
    
    # Number of pixels to crop from the bottom
    min_y: int

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...