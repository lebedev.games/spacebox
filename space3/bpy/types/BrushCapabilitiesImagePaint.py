from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class BrushCapabilitiesImagePaint(bpy_struct):
    
    has_accumulate: bool
    
    has_color: bool
    
    has_radius: bool
    
    has_space_attenuation: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...