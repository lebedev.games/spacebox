from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class FollowTrackConstraint(Constraint):
    
    # Camera to which motion is parented (if empty active scene camera is used)
    camera: Object
    
    # Movie Clip to get tracking data from
    clip: MovieClip
    
    # Object used to define depth in camera space by projecting onto surface of this object
    depth_object: Object
    
    # How the footage fits in the camera frame
    frame_method: str = 'STRETCH' # ['STRETCH', 'FIT', 'CROP']
    
    # Movie tracking object to follow (if empty, camera object is used)
    object: str
    
    # Movie tracking track to follow
    track: str
    
    # Use 3D position of track to parent to
    use_3d_position: bool
    
    # Use active clip defined in scene
    use_active_clip: bool
    
    # Parent to undistorted position of 2D track
    use_undistorted_position: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...