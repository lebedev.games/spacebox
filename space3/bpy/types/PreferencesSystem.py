from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class PreferencesSystem(bpy_struct):
    
    # Quality of the anisotropic filtering (values greater than 1.0 enable anisotropic filtering)
    anisotropic_filter: str = 'FILTER_0' # ['FILTER_0', 'FILTER_2', 'FILTER_4', 'FILTER_8', 'FILTER_16']
    
    # Audio channel count
    audio_channels: str = 'STEREO' # ['MONO', 'STEREO', 'SURROUND4', 'SURROUND51', 'SURROUND71']
    
    # Audio output device
    audio_device: str = 'None' # ['None']
    
    # Number of samples used by the audio mixing buffer
    audio_mixing_buffer: str = 'SAMPLES_2048' # ['SAMPLES_256', 'SAMPLES_512', 'SAMPLES_1024', 'SAMPLES_2048', 'SAMPLES_4096', 'SAMPLES_8192', 'SAMPLES_16384', 'SAMPLES_32768']
    
    # Audio sample format
    audio_sample_format: str = 'FLOAT' # ['U8', 'S16', 'S24', 'S32', 'FLOAT', 'DOUBLE']
    
    # Audio sample rate
    audio_sample_rate: str = 'RATE_48000' # ['RATE_44100', 'RATE_48000', 'RATE_96000', 'RATE_192000']
    
    dpi: int
    
    # Clip alpha below this threshold in the 3D textured view
    gl_clip_alpha: float
    
    # Limit the texture size to save graphics memory
    gl_texture_limit: str = 'CLAMP_OFF' # ['CLAMP_OFF', 'CLAMP_8192', 'CLAMP_4096', 'CLAMP_2048', 'CLAMP_1024', 'CLAMP_512', 'CLAMP_256', 'CLAMP_128']
    
    # Method used for displaying images on the screen
    image_draw_method: str = 'AUTO' # ['AUTO', '2DTEXTURE', 'GLSL']
    
    # For backwards compatibility only
    legacy_compute_device_type: int
    
    # Color of the ambient light that uniformly lit the scene
    light_ambient: Tuple[float, float, float]
    
    # Memory cache limit (in megabytes)
    memory_cache_limit: int
    
    # Type of computer back-end used with OpenSubdiv
    opensubdiv_compute_type: str = 'NONE' # ['NONE', 'CPU', 'OPENMP']
    
    pixel_size: float
    
    # Maximum number of lines to store for the console buffer
    scrollback: int
    
    # Smaller compression will result in larger files, but less decoding overhead
    sequencer_disk_cache_compression: str = 'NONE' # ['NONE', 'LOW', 'HIGH']
    
    # Override default directory
    sequencer_disk_cache_dir: str
    
    # Disk cache limit (in gigabytes)
    sequencer_disk_cache_size_limit: int
    
    # When and how proxies are created
    sequencer_proxy_setup: str = 'AUTOMATIC' # ['MANUAL', 'AUTOMATIC']
    
    # Lights used to display objects in solid shading mode
    solid_lights: List[UserSolidLight]
    
    # Number of seconds between each run of the GL texture garbage collector
    texture_collection_rate: int
    
    # Time since last access of a GL texture in seconds after which it is freed (set to 0 to keep textures allocated)
    texture_time_out: int
    
    # Suggested line thickness and point size in pixels, for add-ons displaying custom user interface elements, based on operating system settings and Blender UI scale
    ui_line_width: float
    
    # Size multiplier to use when displaying custom user interface elements, so that they are scaled correctly on screens with different DPI. This value is based on operating system DPI settings and Blender display scale
    ui_scale: float
    
    # Enable Edit-Mode edge smoothing, reducing aliasing, requires restart
    use_edit_mode_smooth_wire: bool
    
    # Enable overlay smooth wires, reducing aliasing
    use_overlay_smooth_wire: bool
    
    # Display tool/property regions over the main region
    use_region_overlap: bool
    
    # Use the depth buffer for picking 3D View selection (without this the front most object may not be selected first)
    use_select_pick_depth: bool
    
    # Store cached images to disk
    use_sequencer_disk_cache: bool
    
    # View the result of the studio light editor in the viewport
    use_studio_light_edit: bool
    
    # Number of seconds between each run of the GL Vertex buffer object garbage collector
    vbo_collection_rate: int
    
    # Time since last access of a GL Vertex buffer object in seconds after which it is freed (set to 0 to keep vbo allocated)
    vbo_time_out: int
    
    # Method of anti-aliasing in 3d viewport
    viewport_aa: str = '8' # ['OFF', 'FXAA', '5', '8', '11', '16', '32']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...