from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CameraDOFSettings(bpy_struct):
    
    # Number of blades in aperture for polygonal bokeh (at least 3)
    aperture_blades: int
    
    # F-Stop ratio (lower numbers give more defocus, higher numbers give a sharper image)
    aperture_fstop: float
    
    # Distortion to simulate anamorphic lens bokeh
    aperture_ratio: float
    
    # Rotation of blades in aperture
    aperture_rotation: float
    
    # Distance to the focus point for depth of field
    focus_distance: float
    
    # Use this object to define the depth of field focal point
    focus_object: Object
    
    # Use Depth of Field
    use_dof: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...