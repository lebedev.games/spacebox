from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class TriangulateModifier(Modifier):
    
    keep_custom_normals: bool
    
    # Triangulate only polygons with vertex count greater than or equal to this number
    min_vertices: int
    
    # Method for splitting the n-gons into triangles
    ngon_method: str = 'BEAUTY' # ['BEAUTY', 'CLIP']
    
    # Method for splitting the quads into triangles
    quad_method: str = 'SHORTEST_DIAGONAL' # ['BEAUTY', 'FIXED', 'FIXED_ALTERNATE', 'SHORTEST_DIAGONAL']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...