from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class BoidRuleAvoid(BoidRule):
    
    # Avoid object if danger from it is above this threshold
    fear_factor: float
    
    # Object to avoid
    object: Object
    
    # Predict target movement
    use_predict: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...