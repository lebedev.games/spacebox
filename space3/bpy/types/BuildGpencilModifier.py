from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class BuildGpencilModifier(GpencilModifier):
    
    # When should strokes start to appear/disappear
    concurrent_time_alignment: str = 'START' # ['START', 'END']
    
    # End Frame (when Restrict Frame Range is enabled)
    frame_end: float
    
    # Start Frame (when Restrict Frame Range is enabled)
    frame_start: float
    
    # Inverse filter
    invert_layer_pass: bool
    
    # Inverse filter
    invert_layers: bool
    
    # Layer name
    layer: str
    
    # Layer pass index
    layer_pass: int
    
    # Maximum number of frames that the build effect can run for (unless another GP keyframe occurs before this time has elapsed)
    length: float
    
    # How many strokes are being animated at a time
    mode: str = 'SEQUENTIAL' # ['SEQUENTIAL', 'CONCURRENT']
    
    # Defines how much of the stroke is visible
    percentage_factor: float
    
    # Number of frames after each GP keyframe before the modifier has any effect
    start_delay: float
    
    # How are strokes animated (i.e. are they appearing or disappearing)
    transition: str = 'GROW' # ['GROW', 'SHRINK', 'FADE']
    
    # Use a percentage factor to determine the visible points
    use_percentage: bool
    
    # Only modify strokes during the specified frame range
    use_restrict_frame_range: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...