from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Image(ID):
    
    # Representation of alpha in the image file, to convert to and from when saving and loading the image
    alpha_mode: str = 'STRAIGHT' # ['STRAIGHT', 'PREMUL', 'CHANNEL_PACKED', 'NONE']
    
    # OpenGL bindcode
    bindcode: int
    
    # Number of channels in pixels buffer
    channels: int
    
    # Input color space settings
    colorspace_settings: ColorManagedInputColorspaceSettings
    
    # Image bit depth
    depth: int
    
    # Display Aspect for this image, does not affect rendering
    display_aspect: Tuple[float, float]
    
    # Format used for re-saving this file
    file_format: str = 'TARGA' # ['BMP', 'IRIS', 'PNG', 'JPEG', 'JPEG2000', 'TARGA', 'TARGA_RAW', 'CINEON', 'DPX', 'OPEN_EXR_MULTILAYER', 'OPEN_EXR', 'HDR', 'TIFF', 'AVI_JPEG', 'AVI_RAW', 'FFMPEG']
    
    # Image/Movie file name
    filepath: str
    
    # Image/Movie file name (without data refreshing)
    filepath_raw: str
    
    # Duration (in frames) of the image (1 when not a video/sequence)
    frame_duration: int
    
    # Fill color for the generated image
    generated_color: Tuple[float, float, float, float]
    
    # Generated image height
    generated_height: int
    
    # Generated image type
    generated_type: str = 'UV_GRID' # ['BLANK', 'UV_GRID', 'COLOR_GRID']
    
    # Generated image width
    generated_width: int
    
    # True if the image data is loaded into memory
    has_data: bool
    
    # Image has changed and is not saved
    is_dirty: bool
    
    # True if this image is stored in floating-point buffer
    is_float: bool
    
    # Image has more than one view
    is_multiview: bool
    
    # Image has left and right views
    is_stereo_3d: bool
    
    # First packed file of the image
    packed_file: PackedFile
    
    # Collection of packed images
    packed_files: List[ImagePackedFile]
    
    # Image pixels in floating-point values
    pixels: float
    
    # Render slots of the image
    render_slots: RenderSlots
    
    # X/Y pixels per meter
    resolution: Tuple[float, float]
    
    # Width and height in pixels, zero when image data cant be loaded
    size: Tuple[int, int]
    
    # Where the image comes from
    source: str = 'FILE' # ['FILE', 'SEQUENCE', 'MOVIE', 'GENERATED', 'VIEWER', 'TILED']
    
    # Settings for stereo 3d
    stereo_3d_format: Stereo3dFormat
    
    # Tiles of the image
    tiles: UDIMTiles
    
    # How to generate the image
    type: str = 'IMAGE' # ['IMAGE', 'MULTILAYER', 'UV_TEST', 'RENDER_RESULT', 'COMPOSITING']
    
    # Deinterlace movie file on load
    use_deinterlace: bool
    
    # Generate floating-point buffer
    use_generated_float: bool
    
    # Use 16 bits per channel to lower the memory usage during rendering
    use_half_precision: bool
    
    # Use Multiple Views (when available)
    use_multiview: bool
    
    # Apply render part of display transformation when displaying this image on the screen
    use_view_as_render: bool
    
    # Mode to load image views
    views_format: str = 'INDIVIDUAL' # ['INDIVIDUAL', 'STEREO_3D']

    def save_render(self, *args, **kwargs) -> None:
        ...

    def save(self, *args, **kwargs) -> None:
        ...

    def pack(self, *args, **kwargs) -> None:
        ...

    def unpack(self, *args, **kwargs) -> None:
        ...

    def reload(self, *args, **kwargs) -> None:
        ...

    def update(self, *args, **kwargs) -> None:
        ...

    def scale(self, *args, **kwargs) -> None:
        ...

    def gl_touch(self, *args, **kwargs) -> int:
        ...

    def gl_load(self, *args, **kwargs) -> int:
        ...

    def gl_free(self, *args, **kwargs) -> None:
        ...

    def filepath_from_user(self, *args, **kwargs) -> str:
        ...

    def buffers_free(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...