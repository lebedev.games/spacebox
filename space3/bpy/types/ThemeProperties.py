from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ThemeProperties(bpy_struct):
    
    active_modifier: Tuple[float, float, float, float]
    
    match: Tuple[float, float, float]
    
    # Settings for space
    space: ThemeSpaceGeneric

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...