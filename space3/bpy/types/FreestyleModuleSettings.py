from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class FreestyleModuleSettings(bpy_struct):
    
    # Python script to define a style module
    script: Text
    
    # Enable or disable this style module during stroke rendering
    use: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...