from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ShaderNodeTexPointDensity(ShaderNode):
    
    # Texture interpolation
    interpolation: str = 'Linear' # ['Closest', 'Linear', 'Cubic']
    
    # Object to take point data from
    object: Object
    
    # Data to derive color results from
    particle_color_source: str = 'PARTICLE_AGE' # ['PARTICLE_AGE', 'PARTICLE_SPEED', 'PARTICLE_VELOCITY']
    
    # Particle System to render as points
    particle_system: ParticleSystem
    
    # Point data to use as renderable point density
    point_source: str = 'PARTICLE_SYSTEM' # ['PARTICLE_SYSTEM', 'OBJECT']
    
    # Radius from the shaded sample to look for points within
    radius: float
    
    # Resolution used by the texture holding the point density
    resolution: int
    
    # Coordinate system to calculate voxels in
    space: str = 'OBJECT' # ['OBJECT', 'WORLD']
    
    # Vertex attribute to use for color
    vertex_attribute_name: str
    
    # Data to derive color results from
    vertex_color_source: str = 'VERTEX_COLOR' # ['VERTEX_COLOR', 'VERTEX_WEIGHT', 'VERTEX_NORMAL']

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    def cache_point_density(self, *args, **kwargs) -> None:
        ...

    def calc_point_density(self, *args, **kwargs) -> None:
        ...

    def calc_point_density_minmax(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...