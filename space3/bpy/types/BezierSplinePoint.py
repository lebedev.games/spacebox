from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class BezierSplinePoint(bpy_struct):
    
    # Coordinates of the control point
    co: Tuple[float, float, float]
    
    # Coordinates of the first handle
    handle_left: Tuple[float, float, float]
    
    # Handle types
    handle_left_type: str = 'FREE' # ['FREE', 'VECTOR', 'ALIGNED', 'AUTO']
    
    # Coordinates of the second handle
    handle_right: Tuple[float, float, float]
    
    # Handle types
    handle_right_type: str = 'FREE' # ['FREE', 'VECTOR', 'ALIGNED', 'AUTO']
    
    # Visibility status
    hide: bool
    
    # Radius for beveling
    radius: float
    
    # Control point selection status
    select_control_point: bool
    
    # Handle 1 selection status
    select_left_handle: bool
    
    # Handle 2 selection status
    select_right_handle: bool
    
    # Tilt in 3D View
    tilt: float
    
    # Softbody goal weight
    weight_softbody: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...