from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Panel(bpy_struct):
    
    bl_category: str
    
    # The context in which the panel belongs to. (TODO: explain the possible combinations bl_context/bl_region_type/bl_space_type)
    bl_context: str
    
    bl_description: str
    
    # If this is set, the panel gets a custom ID, otherwise it takes the name of the class used to define the panel. For example, if the class name is “OBJECT_PT_hello”, and bl_idname is not set by the script, then bl_idname = “OBJECT_PT_hello”
    bl_idname: str
    
    # The panel label, shows up in the panel header at the right of the triangle used to collapse the panel
    bl_label: str
    
    # Options for this panel type
    bl_options: Set[str] # {'DEFAULT_CLOSED', 'DRAW_BOX}', 'default {DEFAULT_CLOSED', 'HEADER_LAYOUT_EXPAND', 'HIDE_HEADER', 'INSTANCED'}
    
    # Panels with lower numbers are default ordered before panels with higher numbers
    bl_order: int
    
    bl_owner_id: str
    
    # If this is set, the panel becomes a sub-panel
    bl_parent_id: str
    
    # The region where the panel is going to be used in
    bl_region_type: str = 'WINDOW' # ['WINDOW', 'HEADER', 'CHANNELS', 'TEMPORARY', 'UI', 'TOOLS', 'TOOL_PROPS', 'PREVIEW', 'HUD', 'NAVIGATION_BAR', 'EXECUTE', 'FOOTER', 'TOOL_HEADER']
    
    # The space where the panel is going to be used in
    bl_space_type: str = 'EMPTY' # ['EMPTY', 'VIEW_3D', 'IMAGE_EDITOR', 'NODE_EDITOR', 'SEQUENCE_EDITOR', 'CLIP_EDITOR', 'DOPESHEET_EDITOR', 'GRAPH_EDITOR', 'NLA_EDITOR', 'TEXT_EDITOR', 'CONSOLE', 'INFO', 'TOPBAR', 'STATUSBAR', 'OUTLINER', 'PROPERTIES', 'FILE_BROWSER', 'SPREADSHEET', 'PREFERENCES']
    
    bl_translation_context: str
    
    # When set, defines popup panel width
    bl_ui_units_x: int
    
    # Panel data
    custom_data: Constraint
    
    is_popover: bool
    
    # Defines the structure of the panel in the UI
    layout: UILayout
    
    # XXX todo
    text: str
    
    # Show the panel on all tabs
    use_pin: bool

    @classmethod
    def poll(cls, *args, **kwargs) -> bool:
        ...

    def draw(self, *args, **kwargs) -> None:
        ...

    def draw_header(self, *args, **kwargs) -> None:
        ...

    def draw_header_preset(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...