from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class GPencilLayer(bpy_struct):
    
    # Frame currently being displayed for this layer
    active_frame: GPencilFrame
    
    # Set annotation Visibility
    annotation_hide: bool
    
    # Base color for ghosts after the active frame
    annotation_onion_after_color: Tuple[float, float, float]
    
    # Maximum number of frames to show after current frame
    annotation_onion_after_range: int
    
    # Base color for ghosts before the active frame
    annotation_onion_before_color: Tuple[float, float, float]
    
    # Maximum number of frames to show before current frame
    annotation_onion_before_range: int
    
    # Blend mode
    blend_mode: str = 'REGULAR' # ['REGULAR', 'HARDLIGHT', 'ADD', 'SUBTRACT', 'MULTIPLY', 'DIVIDE']
    
    # Custom color for animation channel in Dopesheet
    channel_color: Tuple[float, float, float]
    
    # Color for all strokes in this layer
    color: Tuple[float, float, float]
    
    # Sketches for this layer on different frames
    frames: GPencilFrames
    
    # Set layer Visibility
    hide: bool
    
    # Layer name
    info: str
    
    # True when the layer parent object is set
    is_parented: bool
    
    # This is a special ruler layer
    is_ruler: bool
    
    # Thickness change to apply to current strokes (in pixels)
    line_change: int
    
    # Values for change location
    location: Tuple[float, float, float]
    
    # Protect layer from further editing and/or frame changes
    lock: bool
    
    # Lock current frame displayed by layer
    lock_frame: bool
    
    # Avoids editing locked materials in the layer
    lock_material: bool
    
    # List of Masking Layers
    mask_layers: GreasePencilMaskLayers
    
    # Parent inverse transformation matrix
    matrix_inverse: List[float]
    
    # Local Layer transformation inverse matrix
    matrix_inverse_layer: List[float]
    
    # Local Layer transformation matrix
    matrix_layer: List[float]
    
    # Layer Opacity
    opacity: float
    
    # Parent object
    parent: Object
    
    # Name of parent bone in case of a bone parenting relation
    parent_bone: str
    
    # Type of parent relation
    parent_type: str = 'OBJECT' # ['OBJECT', 'ARMATURE', 'BONE']
    
    # Index number for the “Layer Index” pass
    pass_index: int
    
    # Values for changes in rotation
    rotation: Tuple[float, float, float]
    
    # Values for changes in scale
    scale: Tuple[float, float, float]
    
    # Layer is selected for editing in the Dope Sheet
    select: bool
    
    # Make the layer display in front of objects
    show_in_front: bool
    
    # Show the points which make up the strokes (for debugging purposes)
    show_points: bool
    
    # Thickness of annotation strokes
    thickness: int
    
    # Color for tinting stroke colors
    tint_color: Tuple[float, float, float]
    
    # Factor of tinting color
    tint_factor: float
    
    # Display annotation onion skins before and after the current frame
    use_annotation_onion_skinning: bool
    
    # Enable the use of lights on stroke and fill materials
    use_lights: bool
    
    # The visibility of drawings on this layer is affected by the layers in its masks list
    use_mask_layer: bool
    
    # Display onion skins before and after the current frame
    use_onion_skinning: bool
    
    # In Paint mode display only layers with keyframe in current frame
    use_solo_mode: bool
    
    # Vertex Paint mix factor
    vertex_paint_opacity: float
    
    # Only include Layer in this View Layer render output (leave blank to include always)
    viewlayer_render: str

    def clear(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...