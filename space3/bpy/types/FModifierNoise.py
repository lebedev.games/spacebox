from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class FModifierNoise(FModifier):
    
    # Method of modifying the existing F-Curve
    blend_type: str = 'REPLACE' # ['REPLACE', 'ADD', 'SUBTRACT', 'MULTIPLY']
    
    # Amount of fine level detail present in the noise
    depth: int
    
    # Time offset for the noise effect
    offset: float
    
    # A random seed for the noise effect
    phase: float
    
    # Scaling (in time) of the noise
    scale: float
    
    # Amplitude of the noise - the amount that it modifies the underlying curve
    strength: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...