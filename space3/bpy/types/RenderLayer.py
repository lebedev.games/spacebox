from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class RenderLayer(bpy_struct):
    
    # For Zmask, only render what is behind solid z values instead of in front
    invert_zmask: bool
    
    # View layer name
    name: str
    
    passes: RenderPasses
    
    # Fill in Z values for solid faces in invisible layers, for masking
    use_all_z: bool
    
    # Render Ambient Occlusion in this Layer
    use_ao: bool
    
    # Render edge-enhance in this layer (only works for solid faces)
    use_edge_enhance: bool
    
    # Render Halos in this Layer (on top of Solid)
    use_halo: bool
    
    # Deliver Ambient Occlusion pass
    use_pass_ambient_occlusion: bool
    
    # Deliver full combined RGBA buffer
    use_pass_combined: bool
    
    # Deliver diffuse color pass
    use_pass_diffuse_color: bool
    
    # Deliver diffuse direct pass
    use_pass_diffuse_direct: bool
    
    # Deliver diffuse indirect pass
    use_pass_diffuse_indirect: bool
    
    # Deliver emission pass
    use_pass_emit: bool
    
    # Deliver environment lighting pass
    use_pass_environment: bool
    
    # Deliver glossy color pass
    use_pass_glossy_color: bool
    
    # Deliver glossy direct pass
    use_pass_glossy_direct: bool
    
    # Deliver glossy indirect pass
    use_pass_glossy_indirect: bool
    
    # Deliver material index pass
    use_pass_material_index: bool
    
    # Deliver mist factor pass (0.0 to 1.0)
    use_pass_mist: bool
    
    # Deliver normal pass
    use_pass_normal: bool
    
    # Deliver object index pass
    use_pass_object_index: bool
    
    # Deliver shadow pass
    use_pass_shadow: bool
    
    # Deliver subsurface color pass
    use_pass_subsurface_color: bool
    
    # Deliver subsurface direct pass
    use_pass_subsurface_direct: bool
    
    # Deliver subsurface indirect pass
    use_pass_subsurface_indirect: bool
    
    # Deliver transmission color pass
    use_pass_transmission_color: bool
    
    # Deliver transmission direct pass
    use_pass_transmission_direct: bool
    
    # Deliver transmission indirect pass
    use_pass_transmission_indirect: bool
    
    # Deliver texture UV pass
    use_pass_uv: bool
    
    # Deliver speed vector pass
    use_pass_vector: bool
    
    # Deliver Z values pass
    use_pass_z: bool
    
    # Render Sky in this Layer
    use_sky: bool
    
    # Render Solid faces in this Layer
    use_solid: bool
    
    # Render Strands in this Layer
    use_strand: bool
    
    # Render volumes in this Layer
    use_volumes: bool
    
    # Only render what’s in front of the solid z values
    use_zmask: bool
    
    # Render Z-transparent faces in this layer (on top of Solid and Halos)
    use_ztransp: bool

    def load_from_file(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...