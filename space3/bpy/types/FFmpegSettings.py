from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class FFmpegSettings(bpy_struct):
    
    # Audio bitrate (kb/s)
    audio_bitrate: int
    
    # Audio channel count
    audio_channels: str = 'STEREO' # ['MONO', 'STEREO', 'SURROUND4', 'SURROUND51', 'SURROUND71']
    
    # FFmpeg audio codec to use
    audio_codec: str = 'NONE' # ['NONE', 'AAC', 'AC3', 'FLAC', 'MP2', 'MP3', 'OPUS', 'PCM', 'VORBIS']
    
    # Audio samplerate(samples/s)
    audio_mixrate: int
    
    # Audio volume
    audio_volume: float
    
    # Rate control: buffer size (kb)
    buffersize: int
    
    # FFmpeg codec to use for video output
    codec: str = 'H264' # ['NONE', 'DNXHD', 'DV', 'FFV1', 'FLASH', 'H264', 'HUFFYUV', 'MPEG1', 'MPEG2', 'MPEG4', 'PNG', 'QTRLE', 'THEORA', 'WEBM']
    
    # Constant Rate Factor (CRF); tradeoff between video quality and file size
    constant_rate_factor: str = 'MEDIUM' # ['NONE', 'LOSSLESS', 'PERC_LOSSLESS', 'HIGH', 'MEDIUM', 'LOW', 'VERYLOW', 'LOWEST']
    
    # Tradeoff between encoding speed and compression ratio
    ffmpeg_preset: str = 'GOOD' # ['BEST', 'GOOD', 'REALTIME']
    
    # Output file container
    format: str = 'MKV' # ['MPEG1', 'MPEG2', 'MPEG4', 'AVI', 'QUICKTIME', 'DV', 'OGG', 'MKV', 'FLASH', 'WEBM']
    
    # Distance between key frames, also known as GOP size; influences file size and seekability
    gopsize: int
    
    # Maximum number of B-frames between non-B-frames; influences file size and seekability
    max_b_frames: int
    
    # Rate control: max rate (kbit/s)
    maxrate: int
    
    # Rate control: min rate (kbit/s)
    minrate: int
    
    # Mux rate (bits/second)
    muxrate: int
    
    # Mux packet size (byte)
    packetsize: int
    
    # Autosplit output at 2GB boundary
    use_autosplit: bool
    
    # Use lossless output for video streams
    use_lossless_output: bool
    
    # Set a maximum number of B-frames
    use_max_b_frames: bool
    
    # Video bitrate (kbit/s)
    video_bitrate: int

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...