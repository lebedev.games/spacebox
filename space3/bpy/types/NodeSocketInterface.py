from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class NodeSocketInterface(bpy_struct):
    
    bl_socket_idname: str
    
    # Socket tooltip
    description: str
    
    # Hide the socket input value even when the socket is not connected
    hide_value: bool
    
    # Unique identifier for mapping sockets
    identifier: str
    
    # True if the socket is an output, otherwise input
    is_output: bool
    
    # Socket name
    name: str

    def draw(self, *args, **kwargs) -> None:
        ...

    def draw_color(self, *args, **kwargs) -> None:
        ...

    def register_properties(self, *args, **kwargs) -> None:
        ...

    def init_socket(self, *args, **kwargs) -> None:
        ...

    def from_socket(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...