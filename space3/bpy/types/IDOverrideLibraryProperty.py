from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class IDOverrideLibraryProperty(bpy_struct):
    
    # List of overriding operations for a property
    operations: IDOverrideLibraryPropertyOperations
    
    # RNA path leading to that property, from owning ID
    rna_path: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...