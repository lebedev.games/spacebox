from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ShaderFxGlow(ShaderFx):
    
    # Blend mode
    blend_mode: str = 'REGULAR' # ['REGULAR', 'ADD', 'SUBTRACT', 'MULTIPLY', 'DIVIDE']
    
    # Color used for generated glow
    glow_color: Tuple[float, float, float]
    
    # Glow mode
    mode: str = 'LUMINANCE' # ['LUMINANCE', 'COLOR']
    
    # Effect Opacity
    opacity: float
    
    # Rotation of the effect
    rotation: float
    
    # Number of Blur Samples
    samples: int
    
    # Color selected to apply glow
    select_color: Tuple[float, float, float]
    
    # Size of the effect
    size: Tuple[float, float]
    
    # Limit to select color for glow effect
    threshold: float
    
    # Glow only areas with alpha (not supported with Regular blend mode)
    use_glow_under: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...