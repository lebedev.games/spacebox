from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ShaderFxColorize(ShaderFx):
    
    # Mix factor
    factor: float
    
    # Second color used for effect
    high_color: Tuple[float, float, float, float]
    
    # First color used for effect
    low_color: Tuple[float, float, float, float]
    
    # Effect mode
    mode: str = 'GRAYSCALE' # ['GRAYSCALE', 'SEPIA', 'DUOTONE', 'TRANSPARENT', 'CUSTOM']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...