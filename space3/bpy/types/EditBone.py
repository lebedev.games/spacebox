from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class EditBone(bpy_struct):
    
    # X-axis handle offset for start of the B-Bone’s curve, adjusts curvature
    bbone_curveinx: float
    
    # Y-axis handle offset for start of the B-Bone’s curve, adjusts curvature
    bbone_curveiny: float
    
    # X-axis handle offset for end of the B-Bone’s curve, adjusts curvature
    bbone_curveoutx: float
    
    # Y-axis handle offset for end of the B-Bone’s curve, adjusts curvature
    bbone_curveouty: float
    
    # Bone that serves as the end handle for the B-Bone curve
    bbone_custom_handle_end: 'EditBone'
    
    # Bone that serves as the start handle for the B-Bone curve
    bbone_custom_handle_start: 'EditBone'
    
    # Length of first Bezier Handle (for B-Bones only)
    bbone_easein: float
    
    # Length of second Bezier Handle (for B-Bones only)
    bbone_easeout: float
    
    # Selects how the end handle of the B-Bone is computed
    bbone_handle_type_end: str = 'AUTO' # ['AUTO', 'ABSOLUTE', 'RELATIVE', 'TANGENT']
    
    # Selects how the start handle of the B-Bone is computed
    bbone_handle_type_start: str = 'AUTO' # ['AUTO', 'ABSOLUTE', 'RELATIVE', 'TANGENT']
    
    # Roll offset for the start of the B-Bone, adjusts twist
    bbone_rollin: float
    
    # Roll offset for the end of the B-Bone, adjusts twist
    bbone_rollout: float
    
    # X-axis scale factor for start of the B-Bone, adjusts thickness (for tapering effects)
    bbone_scaleinx: float
    
    # Y-axis scale factor for start of the B-Bone, adjusts thickness (for tapering effects)
    bbone_scaleiny: float
    
    # X-axis scale factor for end of the B-Bone, adjusts thickness (for tapering effects)
    bbone_scaleoutx: float
    
    # Y-axis scale factor for end of the B-Bone, adjusts thickness (for tapering effects)
    bbone_scaleouty: float
    
    # Number of subdivisions of bone (for B-Bones only)
    bbone_segments: int
    
    # B-Bone X size
    bbone_x: float
    
    # B-Bone Z size
    bbone_z: float
    
    # Bone deformation distance (for Envelope deform only)
    envelope_distance: float
    
    # Bone deformation weight (for Envelope deform only)
    envelope_weight: float
    
    # Location of head end of the bone
    head: Tuple[float, float, float]
    
    # Radius of head of bone (for Envelope deform only)
    head_radius: float
    
    # Bone is not visible when in Edit Mode
    hide: bool
    
    # Bone is able to be selected
    hide_select: bool
    
    # Specifies how the bone inherits scaling from the parent bone
    inherit_scale: str = 'FULL' # ['FULL', 'FIX_SHEAR', 'ALIGNED', 'AVERAGE', 'NONE', 'NONE_LEGACY']
    
    # Layers bone exists in
    layers: bool
    
    # Length of the bone. Changing moves the tail end
    length: float
    
    # Bone is not able to be transformed when in Edit Mode
    lock: bool
    
    # Matrix combining location and rotation of the bone (head position, direction and roll), in armature space (does not include/support bone’s length/size)
    matrix: List[float]
    
    name: str
    
    # Parent edit bone (in same Armature)
    parent: 'EditBone'
    
    # Bone rotation around head-tail axis
    roll: float
    
    select: bool
    
    select_head: bool
    
    select_tail: bool
    
    # Bone is always displayed in wireframe regardless of viewport shading mode (useful for non-obstructive custom bone shapes)
    show_wire: bool
    
    # Location of tail end of the bone
    tail: Tuple[float, float, float]
    
    # Radius of tail of bone (for Envelope deform only)
    tail_radius: float
    
    # When bone has a parent, bone’s head is stuck to the parent’s tail
    use_connect: bool
    
    # When bone doesn’t have a parent, it receives cyclic offset effects (Deprecated)
    use_cyclic_offset: bool
    
    # Enable Bone to deform geometry
    use_deform: bool
    
    # Add Roll Out of the Start Handle bone to the Roll In value
    use_endroll_as_inroll: bool
    
    # When deforming bone, multiply effects of Vertex Group weights with Envelope influence
    use_envelope_multiply: bool
    
    # Bone inherits rotation or scale from parent bone
    use_inherit_rotation: bool
    
    # DEPRECATED: Bone inherits scaling from parent bone
    use_inherit_scale: bool
    
    # Bone location is set in local space
    use_local_location: bool
    
    # Object children will use relative transform, like deform
    use_relative_parent: bool
    
    # The name of this bone before any ‘.’ character
    basename: Any
    
    # The midpoint between the head and the tail.
    center: Any
    
    # A list of all the bones children.
    children: Any
    
    # A list of all children from this bone.
    children_recursive: Any
    
    children_recursive_basename: Any
    
    # A list of parents, starting with the immediate parent
    parent_recursive: Any
    
    vector: Any
    
    # Vector pointing down the x-axis of the bone.
    x_axis: Any
    
    # Vector pointing down the y-axis of the bone.
    y_axis: Any
    
    # Vector pointing down the z-axis of the bone.
    z_axis: Any

    def align_roll(self, *args, **kwargs) -> None:
        ...

    def align_orientation(self, *args, **kwargs) -> None:
        ...

    def parent_index(self, *args, **kwargs) -> None:
        ...

    def transform(self, *args, **kwargs) -> None:
        ...

    def translate(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...