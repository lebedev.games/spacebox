from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ViewLayerEEVEE(bpy_struct):
    
    # Deliver bloom pass
    use_pass_bloom: bool
    
    # Deliver volume direct light pass
    use_pass_volume_direct: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...