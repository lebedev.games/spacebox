from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class BooleanModifier(Modifier):
    
    # Use mesh objects in this collection for Boolean operation
    collection: Collection
    
    # Debugging options, only when started with ‘-d’
    debug_options: Set[str] # {'NO_CONNECT_REGIONS}', 'NO_DISSOLVE', 'default {', 'SEPARATE'}
    
    # Threshold for checking overlapping geometry
    double_threshold: float
    
    # Mesh object to use for Boolean operation
    object: Object
    
    operand_type: str = 'OBJECT' # ['OBJECT', 'COLLECTION']
    
    operation: str = 'DIFFERENCE' # ['INTERSECT', 'UNION', 'DIFFERENCE']
    
    # Method for calculating booleans
    solver: str = 'EXACT' # ['FAST', 'EXACT']
    
    # Better results when there are holes (slower)
    use_hole_tolerant: bool
    
    # Allow self-intersection in operands
    use_self: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...