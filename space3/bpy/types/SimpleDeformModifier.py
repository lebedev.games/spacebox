from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SimpleDeformModifier(Modifier):
    
    # Angle of deformation
    angle: float
    
    # Deform around local axis
    deform_axis: str = 'X' # ['X', 'Y', 'Z']
    
    deform_method: str = 'TWIST' # ['TWIST', 'BEND', 'TAPER', 'STRETCH']
    
    # Amount to deform object
    factor: float
    
    # Invert vertex group influence
    invert_vertex_group: bool
    
    # Lower/Upper limits for deform
    limits: Tuple[float, float]
    
    # Do not allow deformation along the X axis
    lock_x: bool
    
    # Do not allow deformation along the Y axis
    lock_y: bool
    
    # Do not allow deformation along the Z axis
    lock_z: bool
    
    # Offset the origin and orientation of the deformation
    origin: Object
    
    # Vertex group name
    vertex_group: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...