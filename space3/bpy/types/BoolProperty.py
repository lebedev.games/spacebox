from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class BoolProperty(Property):
    
    # Length of each dimension of the array
    array_dimensions: Tuple[int, int, int]
    
    # Maximum length of the array, 0 means unlimited
    array_length: int
    
    # Default value for this number
    default: bool
    
    # Default value for this array
    default_array: bool
    
    is_array: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...