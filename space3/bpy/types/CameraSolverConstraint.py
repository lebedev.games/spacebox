from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CameraSolverConstraint(Constraint):
    
    # Movie Clip to get tracking data from
    clip: MovieClip
    
    # Use active clip defined in scene
    use_active_clip: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...