from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MeshVertColorLayer(bpy_struct):
    
    # Sets the sculpt vertex color layer as active for display and editing
    active: bool
    
    # Sets the sculpt vertex color layer as active for rendering
    active_render: bool
    
    data: List[MeshVertColor]
    
    # Name of Sculpt Vertex color layer
    name: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...