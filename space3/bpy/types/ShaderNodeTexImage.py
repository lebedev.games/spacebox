from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ShaderNodeTexImage(ShaderNode):
    
    # Color mapping settings
    color_mapping: ColorMapping
    
    # How the image is extrapolated past its original bounds
    extension: str = 'REPEAT' # ['REPEAT', 'EXTEND', 'CLIP']
    
    image: Image
    
    # Parameters defining which layer, pass and frame of the image is displayed
    image_user: ImageUser
    
    # Texture interpolation
    interpolation: str = 'Linear' # ['Linear', 'Closest', 'Cubic', 'Smart']
    
    # Method to project 2D image on object with a 3D texture vector
    projection: str = 'FLAT' # ['FLAT', 'BOX', 'SPHERE', 'TUBE']
    
    # For box projection, amount of blend to use between sides
    projection_blend: float
    
    # Texture coordinate mapping settings
    texture_mapping: TexMapping

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...