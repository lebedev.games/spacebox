from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SoundSequence(Sequence):
    
    # Animation end offset (trim end)
    animation_offset_end: int
    
    # Animation start offset (trim start)
    animation_offset_start: int
    
    # Playback panning of the sound (only for Mono sources)
    pan: float
    
    # Playback pitch of the sound
    pitch: float
    
    # Display the audio waveform inside the strip
    show_waveform: bool
    
    # Sound data-block used by this sequence
    sound: Sound
    
    # Playback volume of the sound
    volume: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...