from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MaskLayer(bpy_struct):
    
    # Render Opacity
    alpha: float
    
    # Method of blending mask layers
    blend: str = 'ADD' # ['MERGE_ADD', 'MERGE_SUBTRACT', 'ADD', 'SUBTRACT', 'LIGHTEN', 'DARKEN', 'MUL', 'REPLACE', 'DIFFERENCE']
    
    # Falloff type the feather
    falloff: str = 'SMOOTH' # ['SMOOTH', 'SPHERE', 'ROOT', 'INVERSE_SQUARE', 'SHARP', 'LINEAR']
    
    # Restrict visibility in the viewport
    hide: bool
    
    # Restrict renderability
    hide_render: bool
    
    # Restrict selection in the viewport
    hide_select: bool
    
    # Invert the mask black/white
    invert: bool
    
    # Unique name of layer
    name: str
    
    # Layer is selected for editing in the Dope Sheet
    select: bool
    
    # Collection of splines which defines this layer
    splines: MaskSplines
    
    # Calculate holes when filling overlapping curves
    use_fill_holes: bool
    
    # Calculate self intersections and overlap before filling
    use_fill_overlap: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...