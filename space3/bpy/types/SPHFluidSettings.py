from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SPHFluidSettings(bpy_struct):
    
    # Artificial buoyancy force in negative gravity direction based on pressure differences inside the fluid
    buoyancy: float
    
    # Fluid interaction radius
    fluid_radius: float
    
    # Linear viscosity
    linear_viscosity: float
    
    # How much the spring rest length can change after the elastic limit is crossed
    plasticity: float
    
    # How strongly the fluid tries to keep from clustering (factor of stiffness)
    repulsion: float
    
    # Fluid rest density
    rest_density: float
    
    # Spring rest length (factor of particle radius)
    rest_length: float
    
    # The code used to calculate internal forces on particles
    solver: str = 'DDR' # ['DDR', 'CLASSICAL']
    
    # Spring force
    spring_force: float
    
    # Create springs for this number of frames since particles birth (0 is always)
    spring_frames: int
    
    # Creates viscosity for expanding fluid
    stiff_viscosity: float
    
    # How incompressible the fluid is (speed of sound)
    stiffness: float
    
    # Density is calculated as a factor of default density (depends on particle size)
    use_factor_density: bool
    
    # Interaction radius is a factor of 4 * particle size
    use_factor_radius: bool
    
    # Repulsion is a factor of stiffness
    use_factor_repulsion: bool
    
    # Spring rest length is a factor of 2 * particle size
    use_factor_rest_length: bool
    
    # Stiff viscosity is a factor of normal viscosity
    use_factor_stiff_viscosity: bool
    
    # Use the initial length as spring rest length instead of 2 * particle size
    use_initial_rest_length: bool
    
    # Use viscoelastic springs instead of Hooke’s springs
    use_viscoelastic_springs: bool
    
    # How much the spring has to be stretched/compressed in order to change its rest length
    yield_ratio: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...