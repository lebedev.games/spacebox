from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MirrorGpencilModifier(GpencilModifier):
    
    # Inverse filter
    invert_layer_pass: bool
    
    # Inverse filter
    invert_layers: bool
    
    # Inverse filter
    invert_material_pass: bool
    
    # Inverse filter
    invert_materials: bool
    
    # Layer name
    layer: str
    
    # Layer pass index
    layer_pass: int
    
    # Material used for filtering effect
    material: Material
    
    # Object used as center
    object: Object
    
    # Pass index
    pass_index: int
    
    # Mirror the X axis
    use_axis_x: bool
    
    # Mirror the Y axis
    use_axis_y: bool
    
    # Mirror the Z axis
    use_axis_z: bool
    
    # Clip points
    use_clip: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...