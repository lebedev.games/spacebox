from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SpaceNodeEditor(Space):
    
    # Channels of the image to draw
    backdrop_channels: str = 'COLOR' # ['COLOR_ALPHA', 'COLOR', 'ALPHA', 'RED', 'GREEN', 'BLUE']
    
    # Backdrop offset
    backdrop_offset: Tuple[float, float]
    
    # Backdrop zoom factor
    backdrop_zoom: float
    
    # Location for adding new nodes
    cursor_location: Tuple[float, float]
    
    # Node tree being displayed and edited
    edit_tree: NodeTree
    
    # Data-block whose nodes are being edited
    id: ID
    
    # Data-block from which the edited data-block is linked
    id_from: ID
    
    # Direction to offset nodes on insertion
    insert_offset_direction: str = 'RIGHT' # ['RIGHT', 'LEFT']
    
    # Base node tree from context
    node_tree: NodeTree
    
    # Path from the data-block to the currently edited node tree
    path: SpaceNodeEditorPath
    
    # Use the pinned node tree
    pin: bool
    
    # Type of data to take shader from
    shader_type: str = 'OBJECT' # ['OBJECT', 'WORLD', 'LINESTYLE']
    
    # Show annotations for this view
    show_annotation: bool
    
    # Use active Viewer Node output as backdrop for compositing nodes
    show_backdrop: bool
    
    show_region_toolbar: bool
    
    show_region_ui: bool
    
    # Type of data to take texture from
    texture_type: str = 'WORLD' # ['WORLD', 'BRUSH', 'LINESTYLE']
    
    # Node tree type to display and edit
    tree_type: str = 'DUMMY' # ['DUMMY']
    
    # Re-render and composite changed layers on 3D edits
    use_auto_render: bool
    
    # Automatically offset the following or previous nodes in a chain when inserting a new node
    use_insert_offset: bool

    def cursor_location_from_region(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...

    def draw_handler_add(self, *args, **kwargs) -> Any:
        ...

    def draw_handler_remove(self, *args, **kwargs) -> None:
        ...