from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class TextureNodeBricks(TextureNode):
    
    offset: float
    
    # Offset every N rows
    offset_frequency: int
    
    squash: float
    
    # Squash every N rows
    squash_frequency: int

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...