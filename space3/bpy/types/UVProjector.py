from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class UVProjector(bpy_struct):
    
    # Object to use as projector transform
    object: Object

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...