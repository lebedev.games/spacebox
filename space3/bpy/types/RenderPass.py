from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class RenderPass(bpy_struct):
    
    channel_id: str
    
    channels: int
    
    fullname: str
    
    name: str
    
    rect: float
    
    view_id: int

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...