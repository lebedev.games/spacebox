from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class VertexWeightProximityModifier(Modifier):
    
    # How weights are mapped to their new values
    falloff_type: str = 'LINEAR' # ['LINEAR', 'CURVE', 'SHARP', 'SMOOTH', 'ROOT', 'ICON_SPHERECURVE', 'RANDOM', 'STEP']
    
    # Invert the resulting falloff weight
    invert_falloff: bool
    
    # Invert vertex group mask influence
    invert_mask_vertex_group: bool
    
    # Custom mapping curve
    map_curve: CurveMapping
    
    # Global influence of current modifications on vgroup
    mask_constant: float
    
    # Which bone to take texture coordinates from
    mask_tex_map_bone: str
    
    # Which object to take texture coordinates from
    mask_tex_map_object: Object
    
    # Which texture coordinates to use for mapping
    mask_tex_mapping: str = 'LOCAL' # ['LOCAL', 'GLOBAL', 'OBJECT', 'UV']
    
    # Which texture channel to use for masking
    mask_tex_use_channel: str = 'INT' # ['INT', 'RED', 'GREEN', 'BLUE', 'HUE', 'SAT', 'VAL', 'ALPHA']
    
    # UV map name
    mask_tex_uv_layer: str
    
    # Masking texture
    mask_texture: Texture
    
    # Masking vertex group name
    mask_vertex_group: str
    
    # Distance mapping to weight 1.0
    max_dist: float
    
    # Distance mapping to weight 0.0
    min_dist: float
    
    # Normalize the resulting weights (otherwise they are only clamped within 0.0 to 1.0 range)
    normalize: bool
    
    # Use the shortest computed distance to target object’s geometry as weight
    proximity_geometry: Set[str] # {'default {FACE', 'VERTEX', 'FACE}', 'EDGE'}
    
    # Which distances to target object to use
    proximity_mode: str = 'GEOMETRY' # ['OBJECT', 'GEOMETRY']
    
    # Object to calculate vertices distances from
    target: Object
    
    # Vertex group name
    vertex_group: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...