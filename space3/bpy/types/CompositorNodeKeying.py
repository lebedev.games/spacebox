from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CompositorNodeKeying(CompositorNode):
    
    # Matte blur size which applies after clipping and dilate/eroding
    blur_post: int
    
    # Chroma pre-blur size which applies before running keyer
    blur_pre: int
    
    # Value of non-scaled matte pixel which considers as fully background pixel
    clip_black: float
    
    # Value of non-scaled matte pixel which considers as fully foreground pixel
    clip_white: float
    
    # Balance between non-key colors used to detect amount of key color to be removed
    despill_balance: float
    
    # Factor of despilling screen color from image
    despill_factor: float
    
    # Matte dilate/erode side
    dilate_distance: int
    
    # Radius of kernel used to detect whether pixel belongs to edge
    edge_kernel_radius: int
    
    # Tolerance to pixels inside kernel which are treating as belonging to the same plane
    edge_kernel_tolerance: float
    
    # Distance to grow/shrink the feather
    feather_distance: int
    
    # Falloff type the feather
    feather_falloff: str = 'SMOOTH' # ['SMOOTH', 'SPHERE', 'ROOT', 'INVERSE_SQUARE', 'SHARP', 'LINEAR']
    
    # Balance between two non-primary channels primary channel is comparing against
    screen_balance: float

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    def update(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...