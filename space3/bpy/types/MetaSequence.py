from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MetaSequence(Sequence):
    
    # Representation of alpha information in the RGBA pixels
    alpha_mode: str = 'STRAIGHT' # ['STRAIGHT', 'PREMUL']
    
    # Animation end offset (trim end)
    animation_offset_end: int
    
    # Animation start offset (trim start)
    animation_offset_start: int
    
    color_multiply: float
    
    # Adjust the intensity of the input’s color
    color_saturation: float
    
    crop: SequenceCrop
    
    proxy: SequenceProxy
    
    # Sequences nested in meta strip
    sequences: SequencesMeta
    
    # Only display every nth frame
    strobe: float
    
    transform: SequenceTransform
    
    # Remove fields from video movies
    use_deinterlace: bool
    
    # Flip on the X axis
    use_flip_x: bool
    
    # Flip on the Y axis
    use_flip_y: bool
    
    # Convert input to float data
    use_float: bool
    
    # Use a preview proxy and/or time-code index for this strip
    use_proxy: bool
    
    # Reverse frame order
    use_reverse_frames: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...