from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ThemeUserInterface(bpy_struct):
    
    axis_x: Tuple[float, float, float]
    
    axis_y: Tuple[float, float, float]
    
    axis_z: Tuple[float, float, float]
    
    # Color of the outline of the editors and their round corners
    editor_outline: Tuple[float, float, float]
    
    gizmo_a: Tuple[float, float, float]
    
    gizmo_b: Tuple[float, float, float]
    
    gizmo_hi: Tuple[float, float, float]
    
    gizmo_primary: Tuple[float, float, float]
    
    gizmo_secondary: Tuple[float, float, float]
    
    gizmo_view_align: Tuple[float, float, float]
    
    # Transparency of icons in the interface, to reduce contrast
    icon_alpha: float
    
    # Control the intensity of the border around themes icons
    icon_border_intensity: float
    
    icon_collection: Tuple[float, float, float, float]
    
    # Color of folders in the file browser
    icon_folder: Tuple[float, float, float, float]
    
    icon_modifier: Tuple[float, float, float, float]
    
    icon_object: Tuple[float, float, float, float]
    
    icon_object_data: Tuple[float, float, float, float]
    
    # Saturation of icons in the interface
    icon_saturation: float
    
    icon_scene: Tuple[float, float, float, float]
    
    icon_shading: Tuple[float, float, float, float]
    
    # Blending factor for menu shadows
    menu_shadow_fac: float
    
    # Width of menu shadows, set to zero to disable
    menu_shadow_width: int
    
    # Primary color of checkerboard pattern indicating transparent areas
    transparent_checker_primary: Tuple[float, float, float]
    
    # Secondary color of checkerboard pattern indicating transparent areas
    transparent_checker_secondary: Tuple[float, float, float]
    
    # Size of checkerboard pattern indicating transparent areas
    transparent_checker_size: int
    
    wcol_box: ThemeWidgetColors
    
    wcol_list_item: ThemeWidgetColors
    
    wcol_menu: ThemeWidgetColors
    
    wcol_menu_back: ThemeWidgetColors
    
    wcol_menu_item: ThemeWidgetColors
    
    wcol_num: ThemeWidgetColors
    
    wcol_numslider: ThemeWidgetColors
    
    wcol_option: ThemeWidgetColors
    
    wcol_pie_menu: ThemeWidgetColors
    
    wcol_progress: ThemeWidgetColors
    
    wcol_pulldown: ThemeWidgetColors
    
    wcol_radio: ThemeWidgetColors
    
    wcol_regular: ThemeWidgetColors
    
    wcol_scroll: ThemeWidgetColors
    
    wcol_state: ThemeWidgetStateColors
    
    wcol_tab: ThemeWidgetColors
    
    wcol_text: ThemeWidgetColors
    
    wcol_toggle: ThemeWidgetColors
    
    wcol_tool: ThemeWidgetColors
    
    wcol_toolbar_item: ThemeWidgetColors
    
    wcol_tooltip: ThemeWidgetColors
    
    # Color of the 1px shadow line underlying widgets
    widget_emboss: Tuple[float, float, float, float]
    
    # Color of the interface widgets text insertion cursor (caret)
    widget_text_cursor: Tuple[float, float, float]

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...