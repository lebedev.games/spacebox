from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ThemeBoneColorSet(bpy_struct):
    
    # Color used for active bones
    active: Tuple[float, float, float]
    
    # Color used for the surface of bones
    normal: Tuple[float, float, float]
    
    # Color used for selected bones
    select: Tuple[float, float, float]
    
    # Allow the use of colors indicating constraints/keyed status
    show_colored_constraints: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...