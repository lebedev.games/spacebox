from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class PreferencesEdit(bpy_struct):
    
    # Mode of automatic keyframe insertion for Objects and Bones (default setting used for new Scenes)
    auto_keying_mode: str = 'ADD_REPLACE_KEYS' # ['ADD_REPLACE_KEYS', 'REPLACE_KEYS']
    
    # Display size of the empty when new collection instances are created
    collection_instance_empty_size: float
    
    # Auto Handle Smoothing mode used for newly added F-Curves
    fcurve_new_auto_smoothing: str = 'CONT_ACCEL' # ['NONE', 'CONT_ACCEL']
    
    # The opacity of unselected F-Curves against the background of the Graph Editor
    fcurve_unselected_alpha: float
    
    # Color of new annotation layers
    grease_pencil_default_color: Tuple[float, float, float, float]
    
    # Radius of eraser ‘brush’
    grease_pencil_eraser_radius: int
    
    # Distance moved by mouse when drawing stroke to include
    grease_pencil_euclidean_distance: int
    
    # Pixels moved by mouse per axis when drawing stroke
    grease_pencil_manhattan_distance: int
    
    # Handle type for handles of new keyframes
    keyframe_new_handle_type: str = 'AUTO_CLAMPED' # ['FREE', 'ALIGNED', 'VECTOR', 'AUTO', 'AUTO_CLAMPED']
    
    # Interpolation mode used for first keyframe on newly added F-Curves (subsequent keyframes take interpolation from preceding keyframe)
    keyframe_new_interpolation_type: str = 'BEZIER' # ['CONSTANT', 'LINEAR', 'BEZIER', 'SINE', 'QUAD', 'CUBIC', 'QUART', 'QUINT', 'EXPO', 'CIRC', 'BACK', 'BOUNCE', 'ELASTIC']
    
    # Toggle whether the material is linked to object data or the object block
    material_link: str = 'OBDATA' # ['OBDATA', 'OBJECT']
    
    # Minimum distance between nodes for Auto-offsetting nodes
    node_margin: int
    
    # When adding objects from a 3D View menu, either align them with that view or with the world
    object_align: str = 'WORLD' # ['WORLD', 'VIEW', 'CURSOR']
    
    # Color of texture overlay
    sculpt_paint_overlay_color: Tuple[float, float, float]
    
    # Maximum memory usage in megabytes (0 means unlimited)
    undo_memory_limit: int
    
    # Number of undo steps available (smaller values conserve memory)
    undo_steps: int
    
    # Use animation channel group colors; generally this is used to show bone group colors
    use_anim_channel_group_colors: bool
    
    # Automatic keyframe insertion for Objects and Bones (default setting used for new Scenes)
    use_auto_keying: bool
    
    # Show warning indicators when transforming objects and bones if auto keying is enabled
    use_auto_keying_warning: bool
    
    # Place the cursor without ‘jumping’ to the new location (when lock-to-cursor is used)
    use_cursor_lock_adjust: bool
    
    # Causes actions to be duplicated with the data-blocks
    use_duplicate_action: bool
    
    # Causes armature data to be duplicated with the object
    use_duplicate_armature: bool
    
    # Causes curve data to be duplicated with the object
    use_duplicate_curve: bool
    
    # Causes grease pencil data to be duplicated with the object
    use_duplicate_grease_pencil: bool
    
    # Causes hair data to be duplicated with the object
    use_duplicate_hair: bool
    
    # Causes light data to be duplicated with the object
    use_duplicate_light: bool
    
    # Causes light probe data to be duplicated with the object
    use_duplicate_lightprobe: bool
    
    # Causes material data to be duplicated with the object
    use_duplicate_material: bool
    
    # Causes mesh data to be duplicated with the object
    use_duplicate_mesh: bool
    
    # Causes metaball data to be duplicated with the object
    use_duplicate_metaball: bool
    
    # Causes particle systems to be duplicated with the object
    use_duplicate_particle: bool
    
    # Causes point cloud data to be duplicated with the object
    use_duplicate_pointcloud: bool
    
    # Causes surface data to be duplicated with the object
    use_duplicate_surface: bool
    
    # Causes text data to be duplicated with the object
    use_duplicate_text: bool
    
    # Causes volume data to be duplicated with the object
    use_duplicate_volume: bool
    
    # Enter Edit Mode automatically after adding a new object
    use_enter_edit_mode: bool
    
    # Global undo works by keeping a full copy of the file itself in memory, so takes extra memory
    use_global_undo: bool
    
    # Color for newly added transformation F-Curves (Location, Rotation, Scale) and also Color is based on the transform axis
    use_insertkey_xyz_to_rgb: bool
    
    # Automatic keyframe insertion in available F-Curves
    use_keyframe_insert_available: bool
    
    # Keyframe insertion only when keyframe needed
    use_keyframe_insert_needed: bool
    
    # Use the surface depth for cursor placement
    use_mouse_depth_cursor: bool
    
    # Current frame number can be manually set to a negative value
    use_negative_frames: bool
    
    # Use Visual keying automatically for constrained objects
    use_visual_keying: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...