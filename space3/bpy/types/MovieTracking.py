from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MovieTracking(bpy_struct):
    
    # Index of active object
    active_object_index: int
    
    camera: MovieTrackingCamera
    
    dopesheet: MovieTrackingDopesheet
    
    # Collection of objects in this tracking data object
    objects: MovieTrackingObjects
    
    # Collection of plane tracks in this tracking data object
    plane_tracks: MovieTrackingPlaneTracks
    
    reconstruction: MovieTrackingReconstruction
    
    settings: MovieTrackingSettings
    
    stabilization: MovieTrackingStabilization
    
    # Collection of tracks in this tracking data object
    tracks: MovieTrackingTracks

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...