from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MaintainVolumeConstraint(Constraint):
    
    # The free scaling axis of the object
    free_axis: str = 'SAMEVOL_X' # ['SAMEVOL_X', 'SAMEVOL_Y', 'SAMEVOL_Z']
    
    # The way the constraint treats original non-free axis scaling
    mode: str = 'STRICT' # ['STRICT', 'UNIFORM', 'SINGLE_AXIS']
    
    # Volume of the bone at rest
    volume: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...