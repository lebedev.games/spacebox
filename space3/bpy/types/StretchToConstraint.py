from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class StretchToConstraint(Constraint):
    
    # Factor between volume variation and stretching
    bulge: float
    
    # Maximum volume stretching factor
    bulge_max: float
    
    # Minimum volume stretching factor
    bulge_min: float
    
    # Strength of volume stretching clamping
    bulge_smooth: float
    
    # Target along length of bone: Head is 0, Tail is 1
    head_tail: float
    
    # The rotation type and axis order to use
    keep_axis: str = 'PLANE_X' # ['PLANE_X', 'PLANE_Z', 'SWING_Y']
    
    # Length at rest position
    rest_length: float
    
    # Armature bone, mesh or lattice vertex group, …
    subtarget: str
    
    # Target object
    target: Object
    
    # Follow shape of B-Bone segments when calculating Head/Tail position
    use_bbone_shape: bool
    
    # Use upper limit for volume variation
    use_bulge_max: bool
    
    # Use lower limit for volume variation
    use_bulge_min: bool
    
    # Maintain the object’s volume as it stretches
    volume: str = 'VOLUME_XZX' # ['VOLUME_XZX', 'VOLUME_X', 'VOLUME_Z', 'NO_VOLUME']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...