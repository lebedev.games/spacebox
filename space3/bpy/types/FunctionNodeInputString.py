from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class FunctionNodeInputString(FunctionNode):
    
    string: <a class="reference internal" href="#bpy.types.FunctionNodeInputString.string" title="bpy.types.FunctionNodeInputString.string">string</a>, default “”, (never None)

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...