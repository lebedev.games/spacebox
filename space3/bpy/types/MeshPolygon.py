from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MeshPolygon(bpy_struct):
    
    # Read only area of this polygon
    area: float
    
    # Center of this polygon
    center: Tuple[float, float, float]
    
    hide: bool
    
    # Index of this polygon
    index: int
    
    # Index of the first loop of this polygon
    loop_start: int
    
    # Number of loops used by this polygon
    loop_total: int
    
    material_index: int
    
    # Local space unit length normal vector for this polygon
    normal: Tuple[float, float, float]
    
    select: bool
    
    # Face mark for Freestyle line rendering
    use_freestyle_mark: bool
    
    use_smooth: bool
    
    # Vertex indices
    vertices: Tuple[int, int, int]
    
    # (readonly)
    edge_keys: Any
    
    # (readonly)
    loop_indices: Any

    def flip(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...