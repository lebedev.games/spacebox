from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class KeyMap(bpy_struct):
    
    # Internal owner
    bl_owner_id: str
    
    # Indicates that a keymap is used for translate modal events for an operator
    is_modal: bool
    
    # Keymap is defined by the user
    is_user_modified: bool
    
    # Items in the keymap, linking an operator to an input event
    keymap_items: KeyMapItems
    
    # Name of the key map
    name: str
    
    # Optional region type keymap is associated with
    region_type: str = 'WINDOW' # ['WINDOW', 'HEADER', 'CHANNELS', 'TEMPORARY', 'UI', 'TOOLS', 'TOOL_PROPS', 'PREVIEW', 'HUD', 'NAVIGATION_BAR', 'EXECUTE', 'FOOTER', 'TOOL_HEADER']
    
    # Children expanded in the user interface
    show_expanded_children: bool
    
    # Expanded in the user interface
    show_expanded_items: bool
    
    # Optional space type keymap is associated with
    space_type: str = 'EMPTY' # ['EMPTY', 'VIEW_3D', 'IMAGE_EDITOR', 'NODE_EDITOR', 'SEQUENCE_EDITOR', 'CLIP_EDITOR', 'DOPESHEET_EDITOR', 'GRAPH_EDITOR', 'NLA_EDITOR', 'TEXT_EDITOR', 'CONSOLE', 'INFO', 'TOPBAR', 'STATUSBAR', 'OUTLINER', 'PROPERTIES', 'FILE_BROWSER', 'SPREADSHEET', 'PREFERENCES']

    def active(self, *args, **kwargs) -> None:
        ...

    def restore_to_default(self, *args, **kwargs) -> None:
        ...

    def restore_item_to_default(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...