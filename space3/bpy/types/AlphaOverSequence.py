from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class AlphaOverSequence(EffectSequence):
    
    # First input for the effect strip
    input_1: Sequence
    
    # Second input for the effect strip
    input_2: Sequence
    
    input_count: int

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...