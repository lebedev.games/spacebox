from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Speaker(ID):
    
    # Animation data for this data-block
    animation_data: AnimData
    
    # How strong the distance affects volume, depending on distance model
    attenuation: float
    
    # Angle of the inner cone, in degrees, inside the cone the volume is 100%
    cone_angle_inner: float
    
    # Angle of the outer cone, in degrees, outside this cone the volume is the outer cone volume, between inner and outer cone the volume is interpolated
    cone_angle_outer: float
    
    # Volume outside the outer cone
    cone_volume_outer: float
    
    # Maximum distance for volume calculation, no matter how far away the object is
    distance_max: float
    
    # Reference distance at which volume is 100%
    distance_reference: float
    
    # Mute the speaker
    muted: bool
    
    # Playback pitch of the sound
    pitch: float
    
    # Sound data-block used by this speaker
    sound: Sound
    
    # How loud the sound is
    volume: float
    
    # Maximum volume, no matter how near the object is
    volume_max: float
    
    # Minimum volume, no matter how far away the object is
    volume_min: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...