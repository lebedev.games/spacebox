from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SpaceFileBrowser(Space):
    
    active_operator: Operator
    
    # User’s bookmarks
    bookmarks: List[FileBrowserFSMenuEntry]
    
    # Index of active bookmark (-1 if none)
    bookmarks_active: int
    
    # Type of the File Editor view (regular file browsing or asset browsing)
    browse_mode: str = 'FILES' # ['FILES', 'ASSETS']
    
    operator: Operator
    
    # Parameters and Settings for the Filebrowser
    params: FileSelectParams
    
    recent_folders: List[FileBrowserFSMenuEntry]
    
    # Index of active recent folder (-1 if none)
    recent_folders_active: int
    
    show_region_toolbar: bool
    
    show_region_ui: bool
    
    # System’s bookmarks
    system_bookmarks: List[FileBrowserFSMenuEntry]
    
    # Index of active system bookmark (-1 if none)
    system_bookmarks_active: int
    
    # System’s folders (usually root, available hard drives, etc)
    system_folders: List[FileBrowserFSMenuEntry]
    
    # Index of active system folder (-1 if none)
    system_folders_active: int

    def activate_asset_by_id(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...

    def draw_handler_add(self, *args, **kwargs) -> Any:
        ...

    def draw_handler_remove(self, *args, **kwargs) -> None:
        ...