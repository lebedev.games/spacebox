from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class KeyConfigurations(bpy_struct):
    
    # Active key configuration (preset)
    active: KeyConfig
    
    # Key configuration that can be extended by add-ons, and is added to the active configuration when handling events
    addon: KeyConfig
    
    # Default builtin key configuration
    default: KeyConfig
    
    # Final key configuration that combines keymaps from the active and add-on configurations, and can be edited by the user
    user: KeyConfig

    def new(self, *args, **kwargs) -> KeyConfig:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    def find_item_from_operator(self, *args, **kwargs) -> None:
        ...

    def update(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...