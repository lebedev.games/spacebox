from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CameraBackgroundImage(bpy_struct):
    
    # Image opacity to blend the image against the background color
    alpha: float
    
    # Movie clip displayed and edited in this space
    clip: MovieClip
    
    # Parameters defining which frame of the movie clip is displayed
    clip_user: MovieClipUser
    
    # Display under or over everything
    display_depth: str = 'BACK' # ['BACK', 'FRONT']
    
    # How the image fits in the camera frame
    frame_method: str = 'STRETCH' # ['STRETCH', 'FIT', 'CROP']
    
    # Image displayed and edited in this space
    image: Image
    
    # Parameters defining which layer, pass and frame of the image is displayed
    image_user: ImageUser
    
    offset: Tuple[float, float]
    
    # Rotation for the background image (ortho view only)
    rotation: float
    
    # Scale the background image
    scale: float
    
    # Show this image as background
    show_background_image: bool
    
    # Show the expanded in the user interface
    show_expanded: bool
    
    # Show this image in front of objects in viewport
    show_on_foreground: bool
    
    # Data source used for background
    source: str = 'IMAGE' # ['IMAGE', 'MOVIE_CLIP']
    
    # Use movie clip from active scene camera
    use_camera_clip: bool
    
    # Flip the background image horizontally
    use_flip_x: bool
    
    # Flip the background image vertically
    use_flip_y: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...