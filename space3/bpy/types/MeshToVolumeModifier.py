from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MeshToVolumeModifier(Modifier):
    
    # Density of the new volume
    density: float
    
    # Width of the volume outside of the mesh
    exterior_band_width: float
    
    # Width of the volume inside of the mesh
    interior_band_width: float
    
    # Object
    object: Object
    
    # Mode for how the desired voxel size is specified
    resolution_mode: str = 'VOXEL_AMOUNT' # ['VOXEL_AMOUNT', 'VOXEL_SIZE']
    
    # Initialize the density grid in every cell inside the enclosed volume
    use_fill_volume: bool
    
    # Approximate number of voxels along one axis
    voxel_amount: int
    
    # Smaller values result in a higher resolution output
    voxel_size: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...