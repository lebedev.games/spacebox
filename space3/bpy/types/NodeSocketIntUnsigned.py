from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class NodeSocketIntUnsigned(NodeSocketStandard):
    
    # Input value used for unconnected socket
    default_value: int
    
    # List of node links from or to this socket.
    links: Any

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...