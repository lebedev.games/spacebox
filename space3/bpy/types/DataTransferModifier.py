from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class DataTransferModifier(Modifier):
    
    # Which edge data layers to transfer
    data_types_edges: Set[str] # {'CREASE', 'FREESTYLE_EDGE}', 'default {SHARP_EDGE', 'BEVEL_WEIGHT_EDGE', 'SHARP_EDGE', 'SEAM'}
    
    # Which face corner data layers to transfer
    data_types_loops: Set[str] # {'UV}', 'VCOL', 'CUSTOM_NORMAL', 'default {CUSTOM_NORMAL'}
    
    # Which poly data layers to transfer
    data_types_polys: Set[str] # {'SMOOTH', 'FREESTYLE_FACE}', 'default {SMOOTH'}
    
    # Which vertex data layers to transfer
    data_types_verts: Set[str] # {'BEVEL_WEIGHT_VERT}', 'default {VGROUP_WEIGHTS', 'VGROUP_WEIGHTS'}
    
    # Method used to map source edges to destination ones
    edge_mapping: str = 'NEAREST' # ['TOPOLOGY', 'VERT_NEAREST', 'NEAREST', 'POLY_NEAREST', 'EDGEINTERP_VNORPROJ']
    
    # Invert vertex group influence
    invert_vertex_group: bool
    
    # Factor controlling precision of islands handling (typically, 0.1 should be enough, higher values can make things really slow)
    islands_precision: float
    
    # How to match source and destination layers
    layers_uv_select_dst: str = 'NAME' # ['ACTIVE', 'NAME', 'INDEX']
    
    # Which layers to transfer, in case of multi-layers types
    layers_uv_select_src: str = 'ALL' # ['ACTIVE', 'ALL', 'BONE_SELECT', 'BONE_DEFORM']
    
    # How to match source and destination layers
    layers_vcol_select_dst: str = 'NAME' # ['ACTIVE', 'NAME', 'INDEX']
    
    # Which layers to transfer, in case of multi-layers types
    layers_vcol_select_src: str = 'ALL' # ['ACTIVE', 'ALL', 'BONE_SELECT', 'BONE_DEFORM']
    
    # How to match source and destination layers
    layers_vgroup_select_dst: str = 'NAME' # ['ACTIVE', 'NAME', 'INDEX']
    
    # Which layers to transfer, in case of multi-layers types
    layers_vgroup_select_src: str = 'ALL' # ['ACTIVE', 'ALL', 'BONE_SELECT', 'BONE_DEFORM']
    
    # Method used to map source faces’ corners to destination ones
    loop_mapping: str = 'NEAREST_POLYNOR' # ['TOPOLOGY', 'NEAREST_NORMAL', 'NEAREST_POLYNOR', 'NEAREST_POLY', 'POLYINTERP_NEAREST', 'POLYINTERP_LNORPROJ']
    
    # Maximum allowed distance between source and destination element, for non-topology mappings
    max_distance: float
    
    # Factor to use when applying data to destination (exact behavior depends on mix mode, multiplied with weights from vertex group when defined)
    mix_factor: float
    
    # How to affect destination elements with source values
    mix_mode: str = 'REPLACE' # ['REPLACE', 'ABOVE_THRESHOLD', 'BELOW_THRESHOLD', 'MIX', 'ADD', 'SUB', 'MUL']
    
    # Object to transfer data from
    object: Object
    
    # Method used to map source faces to destination ones
    poly_mapping: str = 'NEAREST' # ['TOPOLOGY', 'NEAREST', 'NORMAL', 'POLYINTERP_PNORPROJ']
    
    # ‘Width’ of rays (especially useful when raycasting against vertices or edges)
    ray_radius: float
    
    # Enable edge data transfer
    use_edge_data: bool
    
    # Enable face corner data transfer
    use_loop_data: bool
    
    # Source elements must be closer than given distance from destination one
    use_max_distance: bool
    
    # Evaluate source and destination meshes in global space
    use_object_transform: bool
    
    # Enable face data transfer
    use_poly_data: bool
    
    # Enable vertex data transfer
    use_vert_data: bool
    
    # Method used to map source vertices to destination ones
    vert_mapping: str = 'NEAREST' # ['TOPOLOGY', 'NEAREST', 'EDGE_NEAREST', 'EDGEINTERP_NEAREST', 'POLY_NEAREST', 'POLYINTERP_NEAREST', 'POLYINTERP_VNORPROJ']
    
    # Vertex group name for selecting the affected areas
    vertex_group: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...