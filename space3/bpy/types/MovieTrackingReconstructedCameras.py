from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MovieTrackingReconstructedCameras(bpy_struct):

    def find_frame(self, *args, **kwargs) -> MovieReconstructedCamera:
        ...

    def matrix_from_frame(self, *args, **kwargs) -> List[float]:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...