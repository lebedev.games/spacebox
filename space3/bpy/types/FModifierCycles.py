from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class FModifierCycles(FModifier):
    
    # Maximum number of cycles to allow after last keyframe (0 = infinite)
    cycles_after: int
    
    # Maximum number of cycles to allow before first keyframe (0 = infinite)
    cycles_before: int
    
    # Cycling mode to use after last keyframe
    mode_after: str = 'NONE' # ['NONE', 'REPEAT', 'REPEAT_OFFSET', 'MIRROR']
    
    # Cycling mode to use before first keyframe
    mode_before: str = 'NONE' # ['NONE', 'REPEAT', 'REPEAT_OFFSET', 'MIRROR']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...