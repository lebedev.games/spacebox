from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class TextSequence(EffectSequence):
    
    # Align the text along the X axis, relative to the text bounds
    align_x: str = 'LEFT' # ['LEFT', 'CENTER', 'RIGHT']
    
    # Align the text along the Y axis, relative to the text bounds
    align_y: str = 'TOP' # ['TOP', 'CENTER', 'BOTTOM']
    
    box_color: Tuple[float, float, float, float]
    
    # Box margin as factor of image width
    box_margin: float
    
    # Text color
    color: Tuple[float, float, float, float]
    
    # Font of the text. Falls back to the UI font by default
    font: VectorFont
    
    # Size of the text
    font_size: int
    
    input_count: int
    
    # Location of the text
    location: Tuple[float, float]
    
    shadow_color: Tuple[float, float, float, float]
    
    # Text that will be displayed
    text: str
    
    # Display text as bold
    use_bold: bool
    
    # Display colored box behind text
    use_box: bool
    
    # Display text as italic
    use_italic: bool
    
    # Display shadow behind text
    use_shadow: bool
    
    # Word wrap width as factor, zero disables
    wrap_width: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...