from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CompositorNodeColorSpill(CompositorNode):
    
    channel: str = 'R' # ['R', 'G', 'B']
    
    limit_channel: str = 'R' # ['R', 'G', 'B']
    
    limit_method: str = 'SIMPLE' # ['SIMPLE', 'AVERAGE']
    
    # Scale limit by value
    ratio: float
    
    # Blue spillmap scale
    unspill_blue: float
    
    # Green spillmap scale
    unspill_green: float
    
    # Red spillmap scale
    unspill_red: float
    
    # Compensate all channels (differently) by hand
    use_unspill: bool

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    def update(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...