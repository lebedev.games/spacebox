from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Armature(ID):
    
    # Animation data for this data-block
    animation_data: AnimData
    
    # The position for the axes on the bone. Increasing the value moves it closer to the tip; decreasing moves it closer to the root
    axes_position: float
    
    bones: ArmatureBones
    
    display_type: str = 'OCTAHEDRAL' # ['OCTAHEDRAL', 'STICK', 'BBONE', 'ENVELOPE', 'WIRE']
    
    edit_bones: ArmatureEditBones
    
    # True when used in editmode
    is_editmode: bool
    
    # Armature layer visibility
    layers: bool
    
    # Protected layers in Proxy Instances are restored to Proxy settings on file reload and undo
    layers_protected: bool
    
    # Show armature in binding pose or final posed state
    pose_position: str = 'POSE' # ['POSE', 'REST']
    
    # Display bone axes
    show_axes: bool
    
    # Display bones with their custom shapes
    show_bone_custom_shapes: bool
    
    # Display bone group colors
    show_group_colors: bool
    
    # Display bone names
    show_names: bool
    
    # Apply changes to matching bone on opposite side of X-Axis
    use_mirror_x: bool

    def transform(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...