from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class PropertyGroupItem(bpy_struct):
    
    collection: List[PropertyGroup]
    
    double: float
    
    double_array: Tuple[float]
    
    float: float
    
    float_array: Tuple[float]
    
    group: PropertyGroup
    
    id: ID
    
    idp_array: List[PropertyGroup]
    
    int: int
    
    int_array: Tuple[int]
    
    string: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...