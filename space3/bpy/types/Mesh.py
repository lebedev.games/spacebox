from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Mesh(ID):
    
    # Animation data for this data-block
    animation_data: AnimData
    
    # Geometry attributes
    attributes: AttributeGroup
    
    # Maximum angle between face normals that will be considered as smooth (unused if custom split normals data are available)
    auto_smooth_angle: float
    
    # Adjust active object’s texture space automatically when transforming object
    auto_texspace: bool
    
    # Cycles mesh settings
    cycles: Any
    
    # Edges of the mesh
    edges: MeshEdges
    
    face_maps: MeshFaceMapLayers
    
    # True if there are custom split normals data in this mesh
    has_custom_normals: bool
    
    # True when used in editmode
    is_editmode: bool
    
    # Tessellation of mesh polygons into triangles
    loop_triangles: MeshLoopTriangles
    
    # Loops of the mesh (polygon corners)
    loops: MeshLoops
    
    materials: IDMaterials
    
    polygon_layers_float: PolygonFloatProperties
    
    polygon_layers_int: PolygonIntProperties
    
    polygon_layers_string: PolygonStringProperties
    
    # Polygons of the mesh
    polygons: MeshPolygons
    
    remesh_mode: str = 'VOXEL' # ['VOXEL', 'QUAD']
    
    # Reduces the final face count by simplifying geometry where detail is not needed, generating triangles. A value greater than 0 disables Fix Poles
    remesh_voxel_adaptivity: float
    
    # Size of the voxel in object space used for volume evaluation. Lower values preserve finer details
    remesh_voxel_size: float
    
    # All vertex colors
    sculpt_vertex_colors: VertColors
    
    shape_keys: Key
    
    # All skin vertices
    skin_vertices: List[MeshSkinVertexLayer]
    
    # Derive texture coordinates from another mesh
    texco_mesh: 'Mesh'
    
    # Texture space location
    texspace_location: Tuple[float, float, float]
    
    # Texture space size
    texspace_size: Tuple[float, float, float]
    
    # Use another mesh for texture indices (vertex indices must be aligned)
    texture_mesh: 'Mesh'
    
    # Selected edge count in editmode
    total_edge_sel: int
    
    # Selected face count in editmode
    total_face_sel: int
    
    # Selected vertex count in editmode
    total_vert_sel: int
    
    # Auto smooth (based on smooth/sharp faces/edges and angle between faces), or use custom split normals data if available
    use_auto_smooth: bool
    
    # Adjust active object’s texture space automatically when transforming object
    use_auto_texspace: bool
    
    use_customdata_edge_bevel: bool
    
    use_customdata_edge_crease: bool
    
    use_customdata_vertex_bevel: bool
    
    # Use topology based mirroring (for when both sides of mesh have matching, unique topology)
    use_mirror_topology: bool
    
    # Mirror the left/right vertex groups when painting. The symmetry axis is determined by the symmetry settings
    use_mirror_vertex_groups: bool
    
    # Enable symmetry in the X axis
    use_mirror_x: bool
    
    # Enable symmetry in the Y axis
    use_mirror_y: bool
    
    # Enable symmetry in the Z axis
    use_mirror_z: bool
    
    # Face selection masking for painting
    use_paint_mask: bool
    
    # Vertex selection masking for painting
    use_paint_mask_vertex: bool
    
    # Produces less poles and a better topology flow
    use_remesh_fix_poles: bool
    
    # Keep the current mask on the new mesh
    use_remesh_preserve_paint_mask: bool
    
    # Keep the current Face Sets on the new mesh
    use_remesh_preserve_sculpt_face_sets: bool
    
    # Keep the current vertex colors on the new mesh
    use_remesh_preserve_vertex_colors: bool
    
    # Projects the mesh to preserve the volume and details of the original mesh
    use_remesh_preserve_volume: bool
    
    # Smooth the normals of the remesher result
    use_remesh_smooth_normals: bool
    
    # UV loop layer to be used as cloning source
    uv_layer_clone: MeshUVLoopLayer
    
    # Clone UV loop layer index
    uv_layer_clone_index: int
    
    # UV loop layer to mask the painted area
    uv_layer_stencil: MeshUVLoopLayer
    
    # Mask UV loop layer index
    uv_layer_stencil_index: int
    
    # All UV loop layers
    uv_layers: UVLoopLayers
    
    # All vertex colors
    vertex_colors: LoopColors
    
    vertex_layers_float: VertexFloatProperties
    
    vertex_layers_int: VertexIntProperties
    
    vertex_layers_string: VertexStringProperties
    
    # Vertex paint mask
    vertex_paint_masks: List[MeshPaintMaskLayer]
    
    # Vertices of the mesh
    vertices: MeshVertices
    
    # (readonly)
    edge_keys: Any

    def transform(self, *args, **kwargs) -> None:
        ...

    def flip_normals(self, *args, **kwargs) -> None:
        ...

    def calc_normals(self, *args, **kwargs) -> None:
        ...

    def create_normals_split(self, *args, **kwargs) -> None:
        ...

    def calc_normals_split(self, *args, **kwargs) -> None:
        ...

    def free_normals_split(self, *args, **kwargs) -> None:
        ...

    def split_faces(self, *args, **kwargs) -> None:
        ...

    def calc_tangents(self, *args, **kwargs) -> None:
        ...

    def free_tangents(self, *args, **kwargs) -> None:
        ...

    def calc_loop_triangles(self, *args, **kwargs) -> None:
        ...

    def calc_smooth_groups(self, *args, **kwargs) -> None:
        ...

    def normals_split_custom_set(self, *args, **kwargs) -> None:
        ...

    def normals_split_custom_set_from_vertices(self, *args, **kwargs) -> None:
        ...

    def update(self, *args, **kwargs) -> None:
        ...

    def update_gpu_tag(self, *args, **kwargs) -> None:
        ...

    def unit_test_compare(self, *args, **kwargs) -> str:
        ...

    def clear_geometry(self, *args, **kwargs) -> None:
        ...

    def validate(self, *args, **kwargs) -> bool:
        ...

    def validate_material_indices(self, *args, **kwargs) -> None:
        ...

    def count_selected_items(self, *args, **kwargs) -> None:
        ...

    def from_pydata(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...