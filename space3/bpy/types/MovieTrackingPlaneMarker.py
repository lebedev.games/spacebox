from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MovieTrackingPlaneMarker(bpy_struct):
    
    # Array of coordinates which represents UI rectangle corners in frame normalized coordinates
    corners: List[float]
    
    # Frame number marker is keyframed on
    frame: int
    
    # Is marker muted for current frame
    mute: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...