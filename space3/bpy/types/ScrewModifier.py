from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ScrewModifier(Modifier):
    
    # Angle of revolution
    angle: float
    
    # Screw axis
    axis: str = 'Z' # ['X', 'Y', 'Z']
    
    # Number of times to apply the screw operation
    iterations: int
    
    # Limit below which to merge vertices
    merge_threshold: float
    
    # Object to define the screw axis
    object: Object
    
    # Number of steps in the revolution
    render_steps: int
    
    # Offset the revolution along its axis
    screw_offset: float
    
    # Number of steps in the revolution
    steps: int
    
    # Merge adjacent vertices (screw offset must be zero)
    use_merge_vertices: bool
    
    # Calculate the order of edges (needed for meshes, but not curves)
    use_normal_calculate: bool
    
    # Flip normals of lathed faces
    use_normal_flip: bool
    
    # Use the distance between the objects to make a screw
    use_object_screw_offset: bool
    
    # Output faces with smooth shading rather than flat shaded
    use_smooth_shade: bool
    
    # Stretch the U coordinates between 0 and 1 when UV’s are present
    use_stretch_u: bool
    
    # Stretch the V coordinates between 0 and 1 when UV’s are present
    use_stretch_v: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...