from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class RigidBodyConstraint(bpy_struct):
    
    # Impulse threshold that must be reached for the constraint to break
    breaking_threshold: float
    
    # Disable collisions between constrained rigid bodies
    disable_collisions: bool
    
    # Enable this constraint
    enabled: bool
    
    # Lower limit of X axis rotation
    limit_ang_x_lower: float
    
    # Upper limit of X axis rotation
    limit_ang_x_upper: float
    
    # Lower limit of Y axis rotation
    limit_ang_y_lower: float
    
    # Upper limit of Y axis rotation
    limit_ang_y_upper: float
    
    # Lower limit of Z axis rotation
    limit_ang_z_lower: float
    
    # Upper limit of Z axis rotation
    limit_ang_z_upper: float
    
    # Lower limit of X axis translation
    limit_lin_x_lower: float
    
    # Upper limit of X axis translation
    limit_lin_x_upper: float
    
    # Lower limit of Y axis translation
    limit_lin_y_lower: float
    
    # Upper limit of Y axis translation
    limit_lin_y_upper: float
    
    # Lower limit of Z axis translation
    limit_lin_z_lower: float
    
    # Upper limit of Z axis translation
    limit_lin_z_upper: float
    
    # Maximum angular motor impulse
    motor_ang_max_impulse: float
    
    # Target angular motor velocity
    motor_ang_target_velocity: float
    
    # Maximum linear motor impulse
    motor_lin_max_impulse: float
    
    # Target linear motor velocity
    motor_lin_target_velocity: float
    
    # First Rigid Body Object to be constrained
    object1: Object
    
    # Second Rigid Body Object to be constrained
    object2: Object
    
    # Number of constraint solver iterations made per simulation step (higher values are more accurate but slower)
    solver_iterations: int
    
    # Damping on the X rotational axis
    spring_damping_ang_x: float
    
    # Damping on the Y rotational axis
    spring_damping_ang_y: float
    
    # Damping on the Z rotational axis
    spring_damping_ang_z: float
    
    # Damping on the X axis
    spring_damping_x: float
    
    # Damping on the Y axis
    spring_damping_y: float
    
    # Damping on the Z axis
    spring_damping_z: float
    
    # Stiffness on the X rotational axis
    spring_stiffness_ang_x: float
    
    # Stiffness on the Y rotational axis
    spring_stiffness_ang_y: float
    
    # Stiffness on the Z rotational axis
    spring_stiffness_ang_z: float
    
    # Stiffness on the X axis
    spring_stiffness_x: float
    
    # Stiffness on the Y axis
    spring_stiffness_y: float
    
    # Stiffness on the Z axis
    spring_stiffness_z: float
    
    # Which implementation of spring to use
    spring_type: str = 'SPRING1' # ['SPRING1', 'SPRING2']
    
    # Type of Rigid Body Constraint
    type: str = 'POINT' # ['FIXED', 'POINT', 'HINGE', 'SLIDER', 'PISTON', 'GENERIC', 'GENERIC_SPRING', 'MOTOR']
    
    # Constraint can be broken if it receives an impulse above the threshold
    use_breaking: bool
    
    # Limit rotation around X axis
    use_limit_ang_x: bool
    
    # Limit rotation around Y axis
    use_limit_ang_y: bool
    
    # Limit rotation around Z axis
    use_limit_ang_z: bool
    
    # Limit translation on X axis
    use_limit_lin_x: bool
    
    # Limit translation on Y axis
    use_limit_lin_y: bool
    
    # Limit translation on Z axis
    use_limit_lin_z: bool
    
    # Enable angular motor
    use_motor_ang: bool
    
    # Enable linear motor
    use_motor_lin: bool
    
    # Override the number of solver iterations for this constraint
    use_override_solver_iterations: bool
    
    # Enable spring on X rotational axis
    use_spring_ang_x: bool
    
    # Enable spring on Y rotational axis
    use_spring_ang_y: bool
    
    # Enable spring on Z rotational axis
    use_spring_ang_z: bool
    
    # Enable spring on X axis
    use_spring_x: bool
    
    # Enable spring on Y axis
    use_spring_y: bool
    
    # Enable spring on Z axis
    use_spring_z: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...