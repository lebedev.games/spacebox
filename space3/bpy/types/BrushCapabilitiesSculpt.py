from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class BrushCapabilitiesSculpt(bpy_struct):
    
    has_accumulate: bool
    
    has_auto_smooth: bool
    
    has_color: bool
    
    has_direction: bool
    
    has_gravity: bool
    
    has_height: bool
    
    has_jitter: bool
    
    has_normal_weight: bool
    
    has_persistence: bool
    
    has_pinch_factor: bool
    
    has_plane_offset: bool
    
    has_rake_factor: bool
    
    has_random_texture_angle: bool
    
    has_sculpt_plane: bool
    
    has_secondary_color: bool
    
    has_smooth_stroke: bool
    
    has_space_attenuation: bool
    
    has_strength_pressure: bool
    
    has_tilt: bool
    
    has_topology_rake: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...