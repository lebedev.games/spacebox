from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ConstraintTargetBone(bpy_struct):
    
    # Target armature bone
    subtarget: str
    
    # Target armature
    target: Object
    
    # Blending weight of this bone
    weight: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...