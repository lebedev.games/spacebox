from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class NodeInternal(Node):

    @classmethod
    def poll(cls, *args, **kwargs) -> None:
        ...

    def poll_instance(self, *args, **kwargs) -> None:
        ...

    def update(self, *args, **kwargs) -> None:
        ...

    def draw_buttons(self, *args, **kwargs) -> None:
        ...

    def draw_buttons_ext(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...