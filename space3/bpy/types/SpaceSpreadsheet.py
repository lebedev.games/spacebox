from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SpaceSpreadsheet(Space):
    
    # Attribute domain to display
    attribute_domain: str = 'POINT' # ['POINT', 'EDGE', 'FACE', 'CORNER', 'CURVE']
    
    # Context path to the data being displayed
    context_path: SpreadsheetContextPath
    
    display_context_path_collapsed: bool
    
    # Part of the geometry to display data from
    geometry_component_type: str = 'MESH' # ['MESH', 'POINTCLOUD', 'INSTANCES']
    
    # Context path is pinned
    is_pinned: bool
    
    object_eval_state: str = 'EVALUATED' # ['EVALUATED', 'ORIGINAL']
    
    # Only include rows that correspond to selected elements
    show_only_selected: bool
    
    show_region_footer: bool

    def set_geometry_node_context(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...

    def draw_handler_add(self, *args, **kwargs) -> Any:
        ...

    def draw_handler_remove(self, *args, **kwargs) -> None:
        ...