from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CompositorNodeDefocus(CompositorNode):
    
    # Bokeh shape rotation offset
    angle: float
    
    # Blur limit, maximum CoC radius
    blur_max: float
    
    bokeh: str = 'CIRCLE' # ['OCTAGON', 'HEPTAGON', 'HEXAGON', 'PENTAGON', 'SQUARE', 'TRIANGLE', 'CIRCLE']
    
    # Amount of focal blur, 128 (infinity) is perfect focus, half the value doubles the blur radius
    f_stop: float
    
    # Scene from which to select the active camera (render scene if undefined)
    scene: Scene
    
    # CoC radius threshold, prevents background bleed on in-focus midground, 0 is disabled
    threshold: float
    
    # Enable gamma correction before and after main process
    use_gamma_correction: bool
    
    # Enable low quality mode, useful for preview
    use_preview: bool
    
    # Disable when using an image as input instead of actual z-buffer (auto enabled if node not image based, eg. time node)
    use_zbuffer: bool
    
    # Scale the Z input when not using a z-buffer, controls maximum blur designated by the color white or input value 1
    z_scale: float

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    def update(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...