from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class OceanModifier(Modifier):
    
    # How much foam accumulates over time (baked ocean only)
    bake_foam_fade: float
    
    # Choppiness of the wave’s crest (adds some horizontal component to the displacement)
    choppiness: float
    
    # Damp reflected waves going in opposite direction to the wind
    damping: float
    
    # Depth of the solid ground below the water surface
    depth: float
    
    # This is the distance from a lee shore, called the fetch, or the distance over which the wind blows with constant velocity. Used by ‘JONSWAP’ and ‘TMA’ models
    fetch_jonswap: float
    
    # Path to a folder to store external baked images
    filepath: str
    
    # Amount of generated foam
    foam_coverage: float
    
    # Name of the vertex color layer used for foam
    foam_layer_name: str
    
    # End frame of the ocean baking
    frame_end: int
    
    # Start frame of the ocean baking
    frame_start: int
    
    # Method of modifying geometry
    geometry_mode: str = 'GENERATE' # ['GENERATE', 'DISPLACE']
    
    # Invert the spray direction map
    invert_spray: bool
    
    # Whether the ocean is using cached data or simulating
    is_cached: bool
    
    # Seed of the random generator
    random_seed: int
    
    # Repetitions of the generated surface in X
    repeat_x: int
    
    # Repetitions of the generated surface in Y
    repeat_y: int
    
    # Resolution of the generated surface for rendering and baking
    resolution: int
    
    # Peak sharpening for ‘JONSWAP’ and ‘TMA’ models
    sharpen_peak_jonswap: float
    
    # Surface scale factor (does not affect the height of the waves)
    size: float
    
    # Size of the simulation domain (in meters), and of the generated geometry (in BU)
    spatial_size: int
    
    # Spectrum to use
    spectrum: str = 'PHILLIPS' # ['PHILLIPS', 'PIERSON_MOSKOWITZ', 'JONSWAP', 'TEXEL_MARSEN_ARSLOE']
    
    # Name of the vertex color layer used for the spray direction map
    spray_layer_name: str
    
    # Current time of the simulation
    time: float
    
    # Generate foam mask as a vertex color channel
    use_foam: bool
    
    # Output normals for bump mapping - disabling can speed up performance if its not needed
    use_normals: bool
    
    # Generate map of spray direction as a vertex color channel
    use_spray: bool
    
    # Viewport resolution of the generated surface
    viewport_resolution: int
    
    # How much the waves are aligned to each other
    wave_alignment: float
    
    # Main direction of the waves when they are (partially) aligned
    wave_direction: float
    
    # Scale of the displacement effect
    wave_scale: float
    
    # Shortest allowed wavelength
    wave_scale_min: float
    
    # Wind speed
    wind_velocity: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...