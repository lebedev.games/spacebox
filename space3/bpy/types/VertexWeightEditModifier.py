from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class VertexWeightEditModifier(Modifier):
    
    # Lower bound for a vertex’s weight to be added to the vgroup
    add_threshold: float
    
    # Default weight a vertex will have if it is not in the vgroup
    default_weight: float
    
    # How weights are mapped to their new values
    falloff_type: str = 'LINEAR' # ['LINEAR', 'CURVE', 'SHARP', 'SMOOTH', 'ROOT', 'ICON_SPHERECURVE', 'RANDOM', 'STEP']
    
    # Invert the resulting falloff weight
    invert_falloff: bool
    
    # Invert vertex group mask influence
    invert_mask_vertex_group: bool
    
    # Custom mapping curve
    map_curve: CurveMapping
    
    # Global influence of current modifications on vgroup
    mask_constant: float
    
    # Which bone to take texture coordinates from
    mask_tex_map_bone: str
    
    # Which object to take texture coordinates from
    mask_tex_map_object: Object
    
    # Which texture coordinates to use for mapping
    mask_tex_mapping: str = 'LOCAL' # ['LOCAL', 'GLOBAL', 'OBJECT', 'UV']
    
    # Which texture channel to use for masking
    mask_tex_use_channel: str = 'INT' # ['INT', 'RED', 'GREEN', 'BLUE', 'HUE', 'SAT', 'VAL', 'ALPHA']
    
    # UV map name
    mask_tex_uv_layer: str
    
    # Masking texture
    mask_texture: Texture
    
    # Masking vertex group name
    mask_vertex_group: str
    
    # Normalize the resulting weights (otherwise they are only clamped within 0.0 to 1.0 range)
    normalize: bool
    
    # Upper bound for a vertex’s weight to be removed from the vgroup
    remove_threshold: float
    
    # Add vertices with weight over threshold to vgroup
    use_add: bool
    
    # Remove vertices with weight below threshold from vgroup
    use_remove: bool
    
    # Vertex group name
    vertex_group: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...