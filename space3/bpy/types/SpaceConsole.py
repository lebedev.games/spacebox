from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SpaceConsole(Space):
    
    # Font size to use for displaying the text
    font_size: int
    
    # Command history
    history: List[ConsoleLine]
    
    # Command line prompt language
    language: str
    
    # Command line prompt
    prompt: str
    
    # Command output
    scrollback: List[ConsoleLine]
    
    select_end: int
    
    select_start: int

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...

    def draw_handler_add(self, *args, **kwargs) -> Any:
        ...

    def draw_handler_remove(self, *args, **kwargs) -> None:
        ...