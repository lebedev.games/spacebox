from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MetaBall(ID):
    
    # Animation data for this data-block
    animation_data: AnimData
    
    # Cycles mesh settings
    cycles: Any
    
    # Metaball elements
    elements: MetaBallElements
    
    # True when used in editmode
    is_editmode: bool
    
    materials: IDMaterials
    
    # Polygonization resolution in rendering
    render_resolution: float
    
    # Polygonization resolution in the 3D viewport
    resolution: float
    
    # Texture space location
    texspace_location: Tuple[float, float, float]
    
    # Texture space size
    texspace_size: Tuple[float, float, float]
    
    # Influence of metaball elements
    threshold: float
    
    # Metaball edit update behavior
    update_method: str = 'UPDATE_ALWAYS' # ['UPDATE_ALWAYS', 'HALFRES', 'FAST', 'NEVER']
    
    # Adjust active object’s texture space automatically when transforming object
    use_auto_texspace: bool

    def transform(self, *args, **kwargs) -> None:
        ...

    def update_gpu_tag(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...