from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ThemeInfo(bpy_struct):
    
    # Background color of Debug icon
    info_debug: Tuple[float, float, float, float]
    
    # Foreground color of Debug icon
    info_debug_text: Tuple[float, float, float]
    
    # Background color of Error icon
    info_error: Tuple[float, float, float, float]
    
    # Foreground color of Error icon
    info_error_text: Tuple[float, float, float]
    
    # Background color of Info icon
    info_info: Tuple[float, float, float, float]
    
    # Foreground color of Info icon
    info_info_text: Tuple[float, float, float]
    
    # Background color of Operator icon
    info_operator: Tuple[float, float, float, float]
    
    # Foreground color of Operator icon
    info_operator_text: Tuple[float, float, float]
    
    # Background color of Property icon
    info_property: Tuple[float, float, float, float]
    
    # Foreground color of Property icon
    info_property_text: Tuple[float, float, float]
    
    # Background color of selected line
    info_selected: Tuple[float, float, float]
    
    # Text color of selected line
    info_selected_text: Tuple[float, float, float]
    
    # Background color of Warning icon
    info_warning: Tuple[float, float, float, float]
    
    # Foreground color of Warning icon
    info_warning_text: Tuple[float, float, float]
    
    # Settings for space
    space: ThemeSpaceGeneric

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...