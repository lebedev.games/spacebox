from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MaskLayers(bpy_struct):
    
    # Active layer in this mask
    active: MaskLayer

    def new(self, *args, **kwargs) -> MaskLayer:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    def clear(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...