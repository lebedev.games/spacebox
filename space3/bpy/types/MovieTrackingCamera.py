from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MovieTrackingCamera(bpy_struct):
    
    # First coefficient of fourth order Brown-Conrady radial distortion
    brown_k1: float
    
    # Second coefficient of fourth order Brown-Conrady radial distortion
    brown_k2: float
    
    # Third coefficient of fourth order Brown-Conrady radial distortion
    brown_k3: float
    
    # Fourth coefficient of fourth order Brown-Conrady radial distortion
    brown_k4: float
    
    # First coefficient of second order Brown-Conrady tangential distortion
    brown_p1: float
    
    # Second coefficient of second order Brown-Conrady tangential distortion
    brown_p2: float
    
    # Distortion model used for camera lenses
    distortion_model: str = 'POLYNOMIAL' # ['POLYNOMIAL', 'DIVISION', 'NUKE', 'BROWN']
    
    # First coefficient of second order division distortion
    division_k1: float
    
    # Second coefficient of second order division distortion
    division_k2: float
    
    # Camera’s focal length
    focal_length: float
    
    # Camera’s focal length
    focal_length_pixels: float
    
    # First coefficient of third order polynomial radial distortion
    k1: float
    
    # Second coefficient of third order polynomial radial distortion
    k2: float
    
    # Third coefficient of third order polynomial radial distortion
    k3: float
    
    # First coefficient of second order Nuke distortion
    nuke_k1: float
    
    # Second coefficient of second order Nuke distortion
    nuke_k2: float
    
    # Pixel aspect ratio
    pixel_aspect: float
    
    # Optical center of lens
    principal: Tuple[float, float]
    
    # Width of CCD sensor in millimeters
    sensor_width: float
    
    # Units used for camera focal length
    units: str = 'PIXELS' # ['PIXELS', 'MILLIMETERS']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...