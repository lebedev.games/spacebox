from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ConsoleLine(bpy_struct):
    
    # Text in the line
    body: str
    
    current_character: int
    
    # Console line type when used in scrollback
    type: str = 'OUTPUT' # ['OUTPUT', 'INPUT', 'INFO', 'ERROR']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...