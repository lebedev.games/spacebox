from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class BoidState(bpy_struct):
    
    active_boid_rule: BoidRule
    
    active_boid_rule_index: int
    
    falloff: float
    
    # Boid state name
    name: str
    
    rule_fuzzy: float
    
    rules: List[BoidRule]
    
    # How the rules in the list are evaluated
    ruleset_type: str = 'FUZZY' # ['FUZZY', 'RANDOM', 'AVERAGE']
    
    volume: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...