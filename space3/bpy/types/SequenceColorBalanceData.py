from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SequenceColorBalanceData(bpy_struct):
    
    # Color balance gain (highlights)
    gain: Tuple[float, float, float]
    
    # Color balance gamma (midtones)
    gamma: Tuple[float, float, float]
    
    # Invert the gain color`
    invert_gain: bool
    
    # Invert the gamma color
    invert_gamma: bool
    
    # Invert the lift color
    invert_lift: bool
    
    # Color balance lift (shadows)
    lift: Tuple[float, float, float]

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...