from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ClampToConstraint(Constraint):
    
    # Main axis of movement
    main_axis: str = 'CLAMPTO_AUTO' # ['CLAMPTO_AUTO', 'CLAMPTO_X', 'CLAMPTO_Y', 'CLAMPTO_Z']
    
    # Target Object (Curves only)
    target: Object
    
    # Treat curve as cyclic curve (no clamping to curve bounding box)
    use_cyclic: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...