from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MeshPolygonIntPropertyLayer(bpy_struct):
    
    data: List[MeshPolygonIntProperty]
    
    name: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...