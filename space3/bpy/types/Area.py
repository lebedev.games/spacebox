from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Area(bpy_struct):
    
    # Area height
    height: int
    
    # Regions this area is subdivided in
    regions: List[Region]
    
    # Show menus in the header
    show_menus: bool
    
    # Spaces contained in this area, the first being the active space (NOTE: Useful for example to restore a previously used 3D view space in a certain area to get the old view orientation)
    spaces: AreaSpaces
    
    # Current editor type for this area
    type: str = 'VIEW_3D' # ['EMPTY', 'VIEW_3D', 'IMAGE_EDITOR', 'NODE_EDITOR', 'SEQUENCE_EDITOR', 'CLIP_EDITOR', 'DOPESHEET_EDITOR', 'GRAPH_EDITOR', 'NLA_EDITOR', 'TEXT_EDITOR', 'CONSOLE', 'INFO', 'TOPBAR', 'STATUSBAR', 'OUTLINER', 'PROPERTIES', 'FILE_BROWSER', 'SPREADSHEET', 'PREFERENCES']
    
    # Current editor type for this area
    ui_type: str
    
    # Area width
    width: int
    
    # The window relative vertical location of the area
    x: int
    
    # The window relative horizontal location of the area
    y: int

    def tag_redraw(self, *args, **kwargs) -> None:
        ...

    def header_text_set(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...