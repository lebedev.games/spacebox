from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class BrushCapabilities(bpy_struct):
    
    has_overlay: bool
    
    has_random_texture_angle: bool
    
    has_smooth_stroke: bool
    
    has_spacing: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...