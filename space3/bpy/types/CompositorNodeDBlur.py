from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CompositorNodeDBlur(CompositorNode):
    
    angle: float
    
    center_x: float
    
    center_y: float
    
    distance: float
    
    iterations: int
    
    spin: float
    
    use_wrap: bool
    
    zoom: float

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    def update(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...