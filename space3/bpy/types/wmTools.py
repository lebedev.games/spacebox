from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class wmTools(bpy_struct):

    def from_space_view3d_mode(self, *args, **kwargs) -> None:
        ...

    def from_space_image_mode(self, *args, **kwargs) -> None:
        ...

    def from_space_node(self, *args, **kwargs) -> None:
        ...

    def from_space_sequencer(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...