from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CompositorNodeBokehImage(CompositorNode):
    
    # Angle of the bokeh
    angle: float
    
    # Level of catadioptric of the bokeh
    catadioptric: float
    
    # Number of flaps
    flaps: int
    
    # Level of rounding of the bokeh
    rounding: float
    
    # Shift of the lens components
    shift: float

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    def update(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...