from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class PathCompare(bpy_struct):
    
    path: str
    
    # Enable wildcard globbing
    use_glob: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...