from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class StucciTexture(Texture):
    
    # Noise basis used for turbulence
    noise_basis: str = 'BLENDER_ORIGINAL' # ['BLENDER_ORIGINAL', 'ORIGINAL_PERLIN', 'IMPROVED_PERLIN', 'VORONOI_F1', 'VORONOI_F2', 'VORONOI_F3', 'VORONOI_F4', 'VORONOI_F2_F1', 'VORONOI_CRACKLE', 'CELL_NOISE']
    
    # Scaling for noise input
    noise_scale: float
    
    noise_type: str = 'SOFT_NOISE' # ['SOFT_NOISE', 'HARD_NOISE']
    
    stucci_type: str = 'PLASTIC' # ['PLASTIC', 'WALL_IN', 'WALL_OUT']
    
    # Turbulence of the noise
    turbulence: float
    
    # Materials that use this texture
    users_material: Any
    
    # Object modifiers that use this texture
    users_object_modifier: Any

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...