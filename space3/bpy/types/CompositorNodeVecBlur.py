from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CompositorNodeVecBlur(CompositorNode):
    
    # Scaling factor for motion vectors (actually, ‘shutter speed’, in frames)
    factor: float
    
    samples: int
    
    # Maximum speed, or zero for none
    speed_max: int
    
    # Minimum speed for a pixel to be blurred (used to separate background from foreground)
    speed_min: int
    
    # Interpolate between frames in a Bezier curve, rather than linearly
    use_curved: bool

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    def update(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...