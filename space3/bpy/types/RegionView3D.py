from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class RegionView3D(bpy_struct):
    
    clip_planes: List[float]
    
    # Is current view an orthographic side view
    is_orthographic_side_view: bool
    
    is_perspective: bool
    
    # Lock view rotation in side views
    lock_rotation: bool
    
    # Current perspective matrix (<code class="docutils literal notranslate"><span class="pre">window_matrix</span> <span class="pre">*</span> <span class="pre">view_matrix</span></code>)
    perspective_matrix: List[float]
    
    # Sync view position between side views
    show_sync_view: bool
    
    # Clip objects based on what’s visible in other side views
    use_box_clip: bool
    
    use_clip_planes: bool
    
    # View shift in camera view
    view_camera_offset: Tuple[float, float]
    
    # Zoom factor in camera view
    view_camera_zoom: float
    
    # Distance to the view location
    view_distance: float
    
    # View pivot location
    view_location: Tuple[float, float, float]
    
    # Current view matrix
    view_matrix: List[float]
    
    # View Perspective
    view_perspective: str = 'ORTHO' # ['PERSP', 'ORTHO', 'CAMERA']
    
    # Rotation in quaternions (keep normalized)
    view_rotation: Tuple[float, float, float, float]
    
    # Current window matrix
    window_matrix: List[float]

    def update(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...