from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SpacePreferences(Space):
    
    # Search term for filtering in the UI
    filter_text: str
    
    # Filter method
    filter_type: str = 'NAME' # ['NAME', 'KEY']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...

    def draw_handler_add(self, *args, **kwargs) -> Any:
        ...

    def draw_handler_remove(self, *args, **kwargs) -> None:
        ...