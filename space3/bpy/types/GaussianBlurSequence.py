from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class GaussianBlurSequence(EffectSequence):
    
    # First input for the effect strip
    input_1: Sequence
    
    input_count: int
    
    # Size of the blur along X axis
    size_x: float
    
    # Size of the blur along Y axis
    size_y: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...