from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class BrightContrastModifier(SequenceModifier):
    
    # Adjust the luminosity of the colors
    bright: float
    
    # Adjust the difference in luminosity between pixels
    contrast: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...