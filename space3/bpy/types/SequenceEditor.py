from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SequenceEditor(bpy_struct):
    
    # Sequencer’s active strip
    active_strip: Sequence
    
    # Meta strip stack, last is currently edited meta strip
    meta_stack: List[Sequence]
    
    # Number of frames to offset
    overlay_frame: int
    
    proxy_dir: str
    
    # How to store proxies for this project
    proxy_storage: str = 'PER_STRIP' # ['PER_STRIP', 'PROJECT']
    
    # Top-level strips only
    sequences: SequencesTopLevel
    
    # All strips, recursively including those inside metastrips
    sequences_all: List[Sequence]
    
    # Visualize cached images on the timeline
    show_cache: bool
    
    # Visualize cached composite images
    show_cache_composite: bool
    
    # Visualize cached complete frames
    show_cache_final_out: bool
    
    # Visualize cached pre-processed images
    show_cache_preprocessed: bool
    
    # Visualize cached raw images
    show_cache_raw: bool
    
    # Partial overlay on top of the sequencer with a frame offset
    show_overlay: bool
    
    # Cache intermediate composited images, for faster tweaking of stacked strips at the cost of memory usage
    use_cache_composite: bool
    
    # Cache final image for each frame
    use_cache_final: bool
    
    # Cache pre-processed images, for faster tweaking of effects at the cost of memory usage
    use_cache_preprocessed: bool
    
    # Cache raw images read from disk, for faster tweaking of strip parameters at the cost of memory usage
    use_cache_raw: bool
    
    use_overlay_lock: bool
    
    # Render frames ahead of current frame in the background for faster playback
    use_prefetch: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...