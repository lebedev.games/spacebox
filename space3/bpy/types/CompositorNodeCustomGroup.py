from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CompositorNodeCustomGroup(CompositorNode):
    
    # Interface socket data
    interface: PropertyGroup
    
    node_tree: NodeTree

    def update(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...