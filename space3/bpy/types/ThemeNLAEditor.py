from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ThemeNLAEditor(bpy_struct):
    
    # Animation data-block has active action
    active_action: Tuple[float, float, float, float]
    
    # Animation data-block doesn’t have active action
    active_action_unset: Tuple[float, float, float, float]
    
    # Nonlinear Animation Channel
    dopesheet_channel: Tuple[float, float, float]
    
    frame_current: Tuple[float, float, float]
    
    grid: Tuple[float, float, float]
    
    # Color of keyframe border
    keyframe_border: Tuple[float, float, float, float]
    
    # Color of selected keyframe border
    keyframe_border_selected: Tuple[float, float, float, float]
    
    # Unselected Meta Strip (for grouping related strips)
    meta_strips: Tuple[float, float, float]
    
    # Selected Meta Strip (for grouping related strips)
    meta_strips_selected: Tuple[float, float, float]
    
    # Nonlinear Animation Track
    nla_track: Tuple[float, float, float]
    
    # Color of preview range overlay
    preview_range: Tuple[float, float, float, float]
    
    # Unselected Sound Strip (for timing speaker sounds)
    sound_strips: Tuple[float, float, float]
    
    # Selected Sound Strip (for timing speaker sounds)
    sound_strips_selected: Tuple[float, float, float]
    
    # Settings for space
    space: ThemeSpaceGeneric
    
    # Settings for space list
    space_list: ThemeSpaceListGeneric
    
    # Unselected Action-Clip Strip
    strips: Tuple[float, float, float]
    
    # Selected Action-Clip Strip
    strips_selected: Tuple[float, float, float]
    
    time_marker_line: Tuple[float, float, float, float]
    
    time_marker_line_selected: Tuple[float, float, float, float]
    
    time_scrub_background: Tuple[float, float, float, float]
    
    # Unselected Transition Strip
    transition_strips: Tuple[float, float, float]
    
    # Selected Transition Strip
    transition_strips_selected: Tuple[float, float, float]
    
    # Color for strip/action being “tweaked” or edited
    tweak: Tuple[float, float, float]
    
    # Warning/error indicator color for strips referencing the strip being tweaked
    tweak_duplicate: Tuple[float, float, float]
    
    view_sliders: Tuple[float, float, float]

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...