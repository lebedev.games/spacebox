from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SpaceView3D(Space):
    
    # Active camera used in this view (when unlocked from the scene’s active camera)
    camera: Object
    
    # 3D View far clipping distance
    clip_end: float
    
    # 3D View near clipping distance (perspective view only)
    clip_start: float
    
    icon_from_show_object_viewport: int
    
    # Viewport lens angle
    lens: float
    
    # Display an isolated subset of objects, apart from the scene visibility
    local_view: 'SpaceView3D'
    
    # 3D View center is locked to this bone’s position
    lock_bone: str
    
    # Enable view navigation within the camera view
    lock_camera: bool
    
    # 3D View center is locked to the cursor’s position
    lock_cursor: bool
    
    # 3D View center is locked to this object’s position
    lock_object: Object
    
    # Synchronize the viewer perspective of virtual reality sessions with this 3D viewport
    mirror_xr_session: bool
    
    # Settings for display of overlays in the 3D viewport
    overlay: View3DOverlay
    
    # 3D region in this space, in case of quad view the camera region
    region_3d: RegionView3D
    
    # 3D regions (the third one defines quad view settings, the fourth one is same as ‘region_3d’)
    region_quadviews: List[RegionView3D]
    
    # Maximum X value for the render region
    render_border_max_x: float
    
    # Maximum Y value for the render region
    render_border_max_y: float
    
    # Minimum X value for the render region
    render_border_min_x: float
    
    # Minimum Y value for the render region
    render_border_min_y: float
    
    # Settings for shading in the 3D viewport
    shading: View3DShading
    
    # Show names for reconstructed tracks objects
    show_bundle_names: bool
    
    # Show reconstructed camera path
    show_camera_path: bool
    
    # Show gizmos of all types
    show_gizmo: bool
    
    # Gizmo to adjust camera focus distance (depends on limits display)
    show_gizmo_camera_dof_distance: bool
    
    # Gizmo to adjust camera focal length or orthographic scale
    show_gizmo_camera_lens: bool
    
    # Context sensitive gizmos for the active item
    show_gizmo_context: bool
    
    # Gizmo to adjust the force field
    show_gizmo_empty_force_field: bool
    
    # Gizmo to adjust image size and position
    show_gizmo_empty_image: bool
    
    # Gizmo to adjust the direction of the light
    show_gizmo_light_look_at: bool
    
    # Gizmo to adjust spot and area size
    show_gizmo_light_size: bool
    
    # Viewport navigation gizmo
    show_gizmo_navigate: bool
    
    # Gizmo to adjust rotation
    show_gizmo_object_rotate: bool
    
    # Gizmo to adjust scale
    show_gizmo_object_scale: bool
    
    # Gizmo to adjust location
    show_gizmo_object_translate: bool
    
    # Active tool gizmo
    show_gizmo_tool: bool
    
    show_object_select_armature: bool
    
    show_object_select_camera: bool
    
    show_object_select_curve: bool
    
    show_object_select_empty: bool
    
    show_object_select_font: bool
    
    show_object_select_grease_pencil: bool
    
    show_object_select_hair: bool
    
    show_object_select_lattice: bool
    
    show_object_select_light: bool
    
    show_object_select_light_probe: bool
    
    show_object_select_mesh: bool
    
    show_object_select_meta: bool
    
    show_object_select_pointcloud: bool
    
    show_object_select_speaker: bool
    
    show_object_select_surf: bool
    
    show_object_select_volume: bool
    
    show_object_viewport_armature: bool
    
    show_object_viewport_camera: bool
    
    show_object_viewport_curve: bool
    
    show_object_viewport_empty: bool
    
    show_object_viewport_font: bool
    
    show_object_viewport_grease_pencil: bool
    
    show_object_viewport_hair: bool
    
    show_object_viewport_lattice: bool
    
    show_object_viewport_light: bool
    
    show_object_viewport_light_probe: bool
    
    show_object_viewport_mesh: bool
    
    show_object_viewport_meta: bool
    
    show_object_viewport_pointcloud: bool
    
    show_object_viewport_speaker: bool
    
    show_object_viewport_surf: bool
    
    show_object_viewport_volume: bool
    
    # Display reconstruction data from active movie clip
    show_reconstruction: bool
    
    show_region_hud: bool
    
    show_region_tool_header: bool
    
    show_region_toolbar: bool
    
    show_region_ui: bool
    
    # Show the left and right cameras
    show_stereo_3d_cameras: bool
    
    # Show the stereo 3D convergence plane
    show_stereo_3d_convergence_plane: bool
    
    # Show the stereo 3D frustum volume
    show_stereo_3d_volume: bool
    
    stereo_3d_camera: str = 'S3D' # ['LEFT', 'RIGHT', 'S3D']
    
    # Opacity (alpha) of the convergence plane
    stereo_3d_convergence_plane_alpha: float
    
    # Current stereo eye being displayed
    stereo_3d_eye: str = 'LEFT_EYE' # ['LEFT_EYE', 'RIGHT_EYE']
    
    # Opacity (alpha) of the cameras’ frustum volume
    stereo_3d_volume_alpha: float
    
    # Display size of tracks from reconstructed data
    tracks_display_size: float
    
    # Viewport display style for tracks
    tracks_display_type: str = 'PLAIN_AXES' # ['PLAIN_AXES', 'ARROWS', 'SINGLE_ARROW', 'CIRCLE', 'CUBE', 'SPHERE', 'CONE']
    
    # Use a local camera in this view, rather than scene’s active camera
    use_local_camera: bool
    
    # Display a different set of collections in this viewport
    use_local_collections: bool
    
    # Use a region within the frame size for rendered viewport (when not viewing through the camera)
    use_render_border: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...

    def draw_handler_add(self, *args, **kwargs) -> Any:
        ...

    def draw_handler_remove(self, *args, **kwargs) -> None:
        ...