from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MetaElement(bpy_struct):
    
    co: Tuple[float, float, float]
    
    # Hide element
    hide: bool
    
    radius: float
    
    # Normalized quaternion rotation
    rotation: Tuple[float, float, float, float]
    
    # Select element
    select: bool
    
    # Size of element, use of components depends on element type
    size_x: float
    
    # Size of element, use of components depends on element type
    size_y: float
    
    # Size of element, use of components depends on element type
    size_z: float
    
    # Stiffness defines how much of the element to fill
    stiffness: float
    
    # Metaball types
    type: str = 'BALL' # ['BALL', 'CAPSULE', 'PLANE', 'ELLIPSOID', 'CUBE']
    
    # Set metaball as negative one
    use_negative: bool
    
    # Scale stiffness instead of radius
    use_scale_stiffness: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...