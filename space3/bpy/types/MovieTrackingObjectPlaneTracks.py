from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MovieTrackingObjectPlaneTracks(bpy_struct):
    
    # Active track in this tracking data object
    active: MovieTrackingTrack

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...