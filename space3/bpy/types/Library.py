from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Library(ID):
    
    # Path to the library .blend file
    filepath: str
    
    packed_file: PackedFile
    
    parent: 'Library'
    
    # Version of Blender the library .blend was saved with
    version: Tuple[int, int, int]
    
    # ID data blocks which use this library
    users_id: Any

    def reload(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...