from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Lattice(ID):
    
    # Animation data for this data-block
    animation_data: AnimData
    
    interpolation_type_u: str = 'KEY_BSPLINE' # ['KEY_LINEAR', 'KEY_CARDINAL', 'KEY_CATMULL_ROM', 'KEY_BSPLINE']
    
    interpolation_type_v: str = 'KEY_BSPLINE' # ['KEY_LINEAR', 'KEY_CARDINAL', 'KEY_CATMULL_ROM', 'KEY_BSPLINE']
    
    interpolation_type_w: str = 'KEY_BSPLINE' # ['KEY_LINEAR', 'KEY_CARDINAL', 'KEY_CATMULL_ROM', 'KEY_BSPLINE']
    
    # True when used in editmode
    is_editmode: bool
    
    # Points of the lattice
    points: List[LatticePoint]
    
    # Point in U direction (can’t be changed when there are shape keys)
    points_u: int
    
    # Point in V direction (can’t be changed when there are shape keys)
    points_v: int
    
    # Point in W direction (can’t be changed when there are shape keys)
    points_w: int
    
    shape_keys: Key
    
    # Only display and take into account the outer vertices
    use_outside: bool
    
    # Vertex group to apply the influence of the lattice
    vertex_group: str

    def transform(self, *args, **kwargs) -> None:
        ...

    def update_gpu_tag(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...