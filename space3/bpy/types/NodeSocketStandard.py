from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class NodeSocketStandard(NodeSocket):
    
    # List of node links from or to this socket.
    links: Any

    def draw(self, *args, **kwargs) -> None:
        ...

    def draw_color(self, *args, **kwargs) -> Tuple[float, float, float, float]:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...