from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class TintGpencilModifier(GpencilModifier):
    
    # Color used for tinting
    color: Tuple[float, float, float]
    
    # Color ramp used to define tinting colors
    colors: ColorRamp
    
    # Custom curve to apply effect
    curve: CurveMapping
    
    # Factor for tinting
    factor: float
    
    # Inverse filter
    invert_layer_pass: bool
    
    # Inverse filter
    invert_layers: bool
    
    # Inverse filter
    invert_material_pass: bool
    
    # Inverse filter
    invert_materials: bool
    
    # Inverse filter
    invert_vertex: bool
    
    # Layer name
    layer: str
    
    # Layer pass index
    layer_pass: int
    
    # Material used for filtering effect
    material: Material
    
    # Parent object to define the center of the effect
    object: Object
    
    # Pass index
    pass_index: int
    
    # Defines the maximum distance of the effect
    radius: float
    
    # Select type of tinting algorithm
    tint_type: str = 'UNIFORM' # ['UNIFORM', 'GRADIENT']
    
    # Use a custom curve to define vertex color effect along the strokes
    use_custom_curve: bool
    
    # Vertex group name for modulating the deform
    vertex_group: str
    
    # Defines how vertex color affect to the strokes
    vertex_mode: str = 'STROKE' # ['STROKE', 'FILL', 'BOTH']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...