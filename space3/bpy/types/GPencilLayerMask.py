from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class GPencilLayerMask(bpy_struct):
    
    # Set mask Visibility
    hide: bool
    
    # Invert mask
    invert: bool
    
    # Mask layer name
    name: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...