from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class FloorConstraint(Constraint):
    
    # Location of target that object will not pass through
    floor_location: str = 'FLOOR_X' # ['FLOOR_X', 'FLOOR_Y', 'FLOOR_Z', 'FLOOR_NEGATIVE_X', 'FLOOR_NEGATIVE_Y', 'FLOOR_NEGATIVE_Z']
    
    # Offset of floor from object origin
    offset: float
    
    # Armature bone, mesh or lattice vertex group, …
    subtarget: str
    
    # Target object
    target: Object
    
    # Use the target’s rotation to determine floor
    use_rotation: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...