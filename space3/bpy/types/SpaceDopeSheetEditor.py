from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SpaceDopeSheetEditor(Space):
    
    # Action displayed and edited in this space
    action: Action
    
    # Automatic time snapping settings for transformations
    auto_snap: str = 'NONE' # ['NONE', 'STEP', 'TIME_STEP', 'FRAME', 'SECOND', 'MARKER']
    
    # Show the active object’s cloth point cache
    cache_cloth: bool
    
    # Show the active object’s Dynamic Paint cache
    cache_dynamicpaint: bool
    
    # Show the active object’s particle point cache
    cache_particles: bool
    
    # Show the active object’s Rigid Body cache
    cache_rigidbody: bool
    
    # Show the active object’s smoke cache
    cache_smoke: bool
    
    # Show the active object’s softbody point cache
    cache_softbody: bool
    
    # Settings for filtering animation data
    dopesheet: DopeSheet
    
    # Editing context being displayed
    mode: str = 'ACTION' # ['DOPESHEET', 'TIMELINE', 'ACTION', 'SHAPEKEY', 'GPENCIL', 'MASK', 'CACHEFILE']
    
    # Show the status of cached frames in the timeline
    show_cache: bool
    
    # Mark keyframes where the key value flow changes direction, based on comparison with adjacent keys
    show_extremes: bool
    
    # Display keyframe handle types and non-bezier interpolation modes
    show_interpolation: bool
    
    # If any exists, show markers in a separate row at the bottom of the editor
    show_markers: bool
    
    # Show markers belonging to the active action instead of Scene markers (Action and Shape Key Editors only)
    show_pose_markers: bool
    
    show_region_ui: bool
    
    # Show timing in seconds not frames
    show_seconds: bool
    
    # Show sliders beside F-Curve channels
    show_sliders: bool
    
    # Editing context being displayed
    ui_mode: str = 'ACTION' # ['DOPESHEET', 'ACTION', 'SHAPEKEY', 'GPENCIL', 'MASK', 'CACHEFILE']
    
    # Automatically merge nearby keyframes
    use_auto_merge_keyframes: bool
    
    # Sync Markers with keyframe edits
    use_marker_sync: bool
    
    # When transforming keyframes, changes to the animation data are flushed to other views
    use_realtime_update: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...

    def draw_handler_add(self, *args, **kwargs) -> Any:
        ...

    def draw_handler_remove(self, *args, **kwargs) -> None:
        ...