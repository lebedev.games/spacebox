from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class GPencilInterpolateSettings(bpy_struct):
    
    # Custom curve to control ‘sequence’ interpolation between Grease Pencil frames
    interpolation_curve: CurveMapping

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...