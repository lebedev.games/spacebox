from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class StudioLights(bpy_struct):

    def load(self, *args, **kwargs) -> StudioLight:
        ...

    def new(self, *args, **kwargs) -> StudioLight:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    def refresh(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...