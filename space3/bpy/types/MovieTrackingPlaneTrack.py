from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MovieTrackingPlaneTrack(bpy_struct):
    
    # Image displayed in the track during editing in clip editor
    image: Image
    
    # Opacity of the image
    image_opacity: float
    
    # Collection of markers in track
    markers: MovieTrackingPlaneMarkers
    
    # Unique name of track
    name: str
    
    # Plane track is selected
    select: bool
    
    # Automatic keyframe insertion when moving plane corners
    use_auto_keying: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...