from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SequenceElements(bpy_struct):

    def append(self, *args, **kwargs) -> SequenceElement:
        ...

    def pop(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...