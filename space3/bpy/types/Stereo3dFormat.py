from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Stereo3dFormat(bpy_struct):
    
    anaglyph_type: str = 'RED_CYAN' # ['RED_CYAN', 'GREEN_MAGENTA', 'YELLOW_BLUE']
    
    display_mode: str = 'ANAGLYPH' # ['ANAGLYPH', 'INTERLACE', 'SIDEBYSIDE', 'TOPBOTTOM']
    
    interlace_type: str = 'ROW_INTERLEAVED' # ['ROW_INTERLEAVED', 'COLUMN_INTERLEAVED', 'CHECKERBOARD_INTERLEAVED']
    
    # Swap left and right stereo channels
    use_interlace_swap: bool
    
    # Right eye should see left image and vice versa
    use_sidebyside_crosseyed: bool
    
    # Combine both views in a squeezed image
    use_squeezed_frame: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...