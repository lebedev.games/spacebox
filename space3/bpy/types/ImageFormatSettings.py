from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ImageFormatSettings(bpy_struct):
    
    # Log conversion reference blackpoint
    cineon_black: int
    
    # Log conversion gamma
    cineon_gamma: float
    
    # Log conversion reference whitepoint
    cineon_white: int
    
    # Bit depth per channel
    color_depth: str = '8' # ['8', '10', '12', '16', '32']
    
    # Choose BW for saving grayscale images, RGB for saving red, green and blue channels, and RGBA for saving red, green, blue and alpha channels
    color_mode: str = 'BW' # ['BW', 'RGB', 'RGBA']
    
    # Amount of time to determine best compression: 0 = no compression with fast file output, 100 = maximum lossless compression with slow file output
    compression: int
    
    # Settings of device saved image would be displayed on
    display_settings: ColorManagedDisplaySettings
    
    # Codec settings for OpenEXR
    exr_codec: str = 'NONE' # ['NONE', 'PXR24', 'ZIP', 'PIZ', 'RLE', 'ZIPS', 'B44', 'B44A', 'DWAA']
    
    # File format to save the rendered images as
    file_format: str = 'PNG' # ['BMP', 'IRIS', 'PNG', 'JPEG', 'JPEG2000', 'TARGA', 'TARGA_RAW', 'CINEON', 'DPX', 'OPEN_EXR_MULTILAYER', 'OPEN_EXR', 'HDR', 'TIFF', 'AVI_JPEG', 'AVI_RAW', 'FFMPEG']
    
    # Codec settings for Jpeg2000
    jpeg2k_codec: str = 'JP2' # ['JP2', 'J2K']
    
    # Quality for image formats that support lossy compression
    quality: int
    
    # Settings for stereo 3D
    stereo_3d_format: Stereo3dFormat
    
    # Compression mode for TIFF
    tiff_codec: str = 'DEFLATE' # ['NONE', 'DEFLATE', 'LZW', 'PACKBITS']
    
    # Convert to logarithmic color space
    use_cineon_log: bool
    
    # Use Openjpeg Cinema Preset (48fps)
    use_jpeg2k_cinema_48: bool
    
    # Use Openjpeg Cinema Preset
    use_jpeg2k_cinema_preset: bool
    
    # Save luminance-chrominance-chrominance channels instead of RGB colors
    use_jpeg2k_ycc: bool
    
    # When rendering animations, save JPG preview images in same directory
    use_preview: bool
    
    # Save the z-depth per pixel (32-bit unsigned integer z-buffer)
    use_zbuffer: bool
    
    # Color management settings applied on image before saving
    view_settings: ColorManagedViewSettings
    
    # Format of multiview media
    views_format: str = 'INDIVIDUAL' # ['INDIVIDUAL', 'STEREO_3D']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...