from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CurvePaintSettings(bpy_struct):
    
    # Angles above this are considered corners
    corner_angle: float
    
    # Type of curve to use for new strokes
    curve_type: str = 'BEZIER' # ['POLY', 'BEZIER']
    
    # Method of projecting depth
    depth_mode: str = 'CURSOR' # ['CURSOR', 'SURFACE']
    
    # Allow deviation for a smoother, less precise line
    error_threshold: int
    
    # Curve fitting method
    fit_method: str = 'REFIT' # ['REFIT', 'SPLIT']
    
    # Radius to use when the maximum pressure is applied (or when a tablet isn’t used)
    radius_max: float
    
    # Minimum radius when the minimum pressure is applied (also the minimum when tapering)
    radius_min: float
    
    # Taper factor for the radius of each point along the curve
    radius_taper_end: float
    
    # Taper factor for the radius of each point along the curve
    radius_taper_start: float
    
    # Offset the stroke from the surface
    surface_offset: float
    
    # Plane for projected stroke
    surface_plane: str = 'NORMAL_VIEW' # ['NORMAL_VIEW', 'NORMAL_SURFACE', 'VIEW']
    
    # Detect corners and use non-aligned handles
    use_corners_detect: bool
    
    # Apply a fixed offset (don’t scale by the radius)
    use_offset_absolute: bool
    
    # Map tablet pressure to curve radius
    use_pressure_radius: bool
    
    # Use the start of the stroke for the depth
    use_stroke_endpoints: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...