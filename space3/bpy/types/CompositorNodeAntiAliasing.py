from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CompositorNodeAntiAliasing(CompositorNode):
    
    # How much to eliminate spurious edges to avoid artifacts (the larger value makes less active; the value 2.0, for example, means discard a detected edge if there is a neighboring edge that has 2.0 times bigger contrast than the current one)
    contrast_limit: float
    
    # How much sharp corners will be rounded
    corner_rounding: float
    
    # Threshold to detect edges (smaller threshold makes more sensitive detection)
    threshold: float

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    def update(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...