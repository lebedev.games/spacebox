from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ColorMapping(bpy_struct):
    
    # Blend color to mix with texture output color
    blend_color: Tuple[float, float, float]
    
    blend_factor: float
    
    # Mode used to mix with texture output color
    blend_type: str = 'MIX' # ['MIX', 'DARKEN', 'MULTIPLY', 'LIGHTEN', 'SCREEN', 'ADD', 'OVERLAY', 'SOFT_LIGHT', 'LINEAR_LIGHT', 'DIFFERENCE', 'SUBTRACT', 'DIVIDE', 'HUE', 'SATURATION', 'COLOR', 'VALUE']
    
    # Adjust the brightness of the texture
    brightness: float
    
    color_ramp: ColorRamp
    
    # Adjust the contrast of the texture
    contrast: float
    
    # Adjust the saturation of colors in the texture
    saturation: float
    
    # Toggle color ramp operations
    use_color_ramp: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...