from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class WeldModifier(Modifier):
    
    # Invert vertex group influence
    invert_vertex_group: bool
    
    # Limit below which to merge vertices
    merge_threshold: float
    
    # Mode defines the merge rule
    mode: str = 'ALL' # ['ALL', 'CONNECTED']
    
    # Vertex group name for selecting the affected areas
    vertex_group: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...