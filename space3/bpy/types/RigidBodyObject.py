from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class RigidBodyObject(bpy_struct):
    
    # Amount of angular velocity that is lost over time
    angular_damping: float
    
    # Collision collections rigid body belongs to
    collision_collections: bool
    
    # Threshold of distance near surface where collisions are still considered (best results when non-zero)
    collision_margin: float
    
    # Collision Shape of object in Rigid Body Simulations
    collision_shape: str = 'BOX' # ['BOX', 'SPHERE', 'CAPSULE', 'CYLINDER', 'CONE', 'CONVEX_HULL', 'MESH', 'COMPOUND']
    
    # Angular Velocity below which simulation stops simulating object
    deactivate_angular_velocity: float
    
    # Linear Velocity below which simulation stops simulating object
    deactivate_linear_velocity: float
    
    # Rigid Body actively participates to the simulation
    enabled: bool
    
    # Resistance of object to movement
    friction: float
    
    # Allow rigid body to be controlled by the animation system
    kinematic: bool
    
    # Amount of linear velocity that is lost over time
    linear_damping: float
    
    # How much the object ‘weighs’ irrespective of gravity
    mass: float
    
    # Source of the mesh used to create collision shape
    mesh_source: str = 'BASE' # ['BASE', 'DEFORM', 'FINAL']
    
    # Tendency of object to bounce after colliding with another (0 = stays still, 1 = perfectly elastic)
    restitution: float
    
    # Role of object in Rigid Body Simulations
    type: str = 'ACTIVE' # ['ACTIVE', 'PASSIVE']
    
    # Enable deactivation of resting rigid bodies (increases performance and stability but can cause glitches)
    use_deactivation: bool
    
    # Rigid body deforms during simulation
    use_deform: bool
    
    # Use custom collision margin (some shapes will have a visible gap around them)
    use_margin: bool
    
    # Deactivate rigid body at the start of the simulation
    use_start_deactivated: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...