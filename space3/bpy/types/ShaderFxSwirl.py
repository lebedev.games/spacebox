from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ShaderFxSwirl(ShaderFx):
    
    # Angle of rotation
    angle: float
    
    # Object to determine center location
    object: Object
    
    # Radius to apply
    radius: int
    
    # Make image transparent outside of radius
    use_transparent: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...