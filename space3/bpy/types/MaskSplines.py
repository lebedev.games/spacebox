from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MaskSplines(bpy_struct):
    
    # Active spline of masking layer
    active: MaskSpline
    
    # Active spline of masking layer
    active_point: MaskSplinePoint

    def new(self, *args, **kwargs) -> None:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...