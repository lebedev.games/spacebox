from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class GpPaint(Paint):
    
    # Paint Mode
    color_mode: str = 'MATERIAL' # ['MATERIAL', 'VERTEXCOLOR']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...