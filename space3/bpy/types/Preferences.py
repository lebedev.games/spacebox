from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Preferences(bpy_struct):
    
    # Active section of the preferences shown in the user interface
    active_section: str = 'INTERFACE' # ['INTERFACE', 'THEMES', 'VIEWPORT', 'LIGHTS', 'EDITING', 'ANIMATION', 'ADDONS', 'INPUT', 'NAVIGATION', 'KEYMAP', 'SYSTEM', 'SAVE_LOAD', 'FILE_PATHS', 'EXPERIMENTAL']
    
    addons: Addons
    
    app_template: str
    
    autoexec_paths: PathCompareCollection
    
    # Settings for interacting with Blender data
    edit: PreferencesEdit
    
    # Settings for features that are still early in their development stage
    experimental: PreferencesExperimental
    
    # Default paths for external files
    filepaths: PreferencesFilePaths
    
    # Settings for input devices
    inputs: PreferencesInput
    
    # Preferences have changed
    is_dirty: bool
    
    # Shortcut setup for keyboards and other input devices
    keymap: PreferencesKeymap
    
    studio_lights: StudioLights
    
    # Graphics driver and operating system settings
    system: PreferencesSystem
    
    themes: List[Theme]
    
    ui_styles: List[ThemeStyle]
    
    # Save preferences on exit when modified (unless factory settings have been loaded)
    use_preferences_save: bool
    
    # Version of Blender the userpref.blend was saved with
    version: Tuple[int, int, int]
    
    # Preferences related to viewing data
    view: PreferencesView

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...