from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SpaceImageEditor(Space):
    
    # 2D cursor location for this view
    cursor_location: Tuple[float, float]
    
    # Channels of the image to display
    display_channels: str = 'COLOR' # ['COLOR_ALPHA', 'COLOR', 'ALPHA', 'Z_BUFFER', 'RED', 'GREEN', 'BLUE']
    
    # Grease pencil data for this space
    grease_pencil: GreasePencil
    
    # Image displayed and edited in this space
    image: Image
    
    # Parameters defining which layer, pass and frame of the image is displayed
    image_user: ImageUser
    
    # Mask displayed and edited in this space
    mask: Mask
    
    # Display type for mask splines
    mask_display_type: str = 'OUTLINE' # ['OUTLINE', 'DASH', 'BLACK', 'WHITE']
    
    # Overlay mode of rasterized mask
    mask_overlay_mode: str = 'ALPHACHANNEL' # ['ALPHACHANNEL', 'COMBINED']
    
    # Editing context being displayed
    mode: str = 'VIEW' # ['VIEW', 'UV', 'PAINT', 'MASK']
    
    # Settings for display of overlays in the UV/Image editor
    overlay: SpaceImageOverlay
    
    # Rotation/Scaling Pivot
    pivot_point: str = 'BOUNDING_BOX_CENTER' # ['BOUNDING_BOX_CENTER', 'CURSOR', 'INDIVIDUAL_ORIGINS', 'MEDIAN_POINT', 'ACTIVE_ELEMENT']
    
    # Sampled colors along line
    sample_histogram: Histogram
    
    # Scopes to visualize image statistics
    scopes: Scopes
    
    # Show annotations for this view
    show_annotation: bool
    
    show_mask_overlay: bool
    
    show_mask_smooth: bool
    
    # Show Mask editing related properties
    show_maskedit: bool
    
    # Show paint related properties
    show_paint: bool
    
    show_region_hud: bool
    
    show_region_tool_header: bool
    
    show_region_toolbar: bool
    
    show_region_ui: bool
    
    # Show render related properties
    show_render: bool
    
    # Display the image repeated outside of the main view
    show_repeat: bool
    
    # Display the image in Stereo 3D
    show_stereo_3d: bool
    
    # Show UV editing related properties
    show_uvedit: bool
    
    # Editing context being displayed
    ui_mode: str = 'VIEW' # ['VIEW', 'PAINT', 'MASK']
    
    # Display current image regardless of object selection
    use_image_pin: bool
    
    # Update other affected window spaces automatically to reflect changes during interactive operations such as transform
    use_realtime_update: bool
    
    # UV editor settings
    uv_editor: SpaceUVEditor
    
    # Zoom factor
    zoom: Tuple[float, float]

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...

    def draw_handler_add(self, *args, **kwargs) -> Any:
        ...

    def draw_handler_remove(self, *args, **kwargs) -> None:
        ...