from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ThemeSpaceGeneric(bpy_struct):
    
    back: Tuple[float, float, float]
    
    button: Tuple[float, float, float, float]
    
    button_text: Tuple[float, float, float]
    
    button_text_hi: Tuple[float, float, float]
    
    button_title: Tuple[float, float, float]
    
    execution_buts: Tuple[float, float, float, float]
    
    header: Tuple[float, float, float, float]
    
    header_text: Tuple[float, float, float]
    
    header_text_hi: Tuple[float, float, float]
    
    navigation_bar: Tuple[float, float, float, float]
    
    panelcolors: ThemePanelColors
    
    tab_active: Tuple[float, float, float]
    
    tab_back: Tuple[float, float, float, float]
    
    tab_inactive: Tuple[float, float, float]
    
    tab_outline: Tuple[float, float, float]
    
    text: Tuple[float, float, float]
    
    text_hi: Tuple[float, float, float]
    
    title: Tuple[float, float, float]

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...