from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ShaderFxFlip(ShaderFx):
    
    # Flip image horizontally
    flip_horizontal: bool
    
    # Flip image vertically
    flip_vertical: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...