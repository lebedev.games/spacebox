from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class BoidRuleFight(BoidRule):
    
    # Attack boids at max this distance
    distance: float
    
    # Flee to this distance
    flee_distance: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...