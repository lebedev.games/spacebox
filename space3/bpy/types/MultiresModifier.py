from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MultiresModifier(Modifier):
    
    # Controls how open boundaries are smoothed
    boundary_smooth: str = 'ALL' # ['PRESERVE_CORNERS', 'ALL']
    
    # Path to external displacements file
    filepath: str
    
    # Store multires displacements outside the .blend file, to save memory
    is_external: bool
    
    # Number of subdivisions to use in the viewport
    levels: int
    
    # Accuracy of vertex positions, lower value is faster but less precise
    quality: int
    
    # The subdivision level visible at render time
    render_levels: int
    
    # Number of subdivisions to use in sculpt mode
    sculpt_levels: int
    
    # Skip drawing/rendering of interior subdivided edges
    show_only_control_edges: bool
    
    # Number of subdivisions for which displacements are stored
    total_levels: int
    
    # Use mesh edge crease information to sharpen edges
    use_creases: bool
    
    # Interpolates existing custom normals to resulting mesh
    use_custom_normals: bool
    
    # Make Sculpt Mode tools deform the base mesh while previewing the displacement of higher subdivision levels
    use_sculpt_base_mesh: bool
    
    # Controls how smoothing is applied to UVs
    uv_smooth: str = 'PRESERVE_BOUNDARIES' # ['NONE', 'PRESERVE_CORNERS', 'PRESERVE_CORNERS_AND_JUNCTIONS', 'PRESERVE_CORNERS_JUNCTIONS_AND_CONCAVE', 'PRESERVE_BOUNDARIES', 'SMOOTH_ALL']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...