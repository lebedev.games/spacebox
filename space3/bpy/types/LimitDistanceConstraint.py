from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class LimitDistanceConstraint(Constraint):
    
    # Radius of limiting sphere
    distance: float
    
    # Target along length of bone: Head is 0, Tail is 1
    head_tail: float
    
    # Distances in relation to sphere of influence to allow
    limit_mode: str = 'LIMITDIST_INSIDE' # ['LIMITDIST_INSIDE', 'LIMITDIST_OUTSIDE', 'LIMITDIST_ONSURFACE']
    
    # Armature bone, mesh or lattice vertex group, …
    subtarget: str
    
    # Target object
    target: Object
    
    # Follow shape of B-Bone segments when calculating Head/Tail position
    use_bbone_shape: bool
    
    # Transforms are affected by this constraint as well
    use_transform_limit: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...