from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ShaderNodeOutputLineStyle(ShaderNode):
    
    blend_type: str = 'MIX' # ['MIX', 'DARKEN', 'MULTIPLY', 'BURN', 'LIGHTEN', 'SCREEN', 'DODGE', 'ADD', 'OVERLAY', 'SOFT_LIGHT', 'LINEAR_LIGHT', 'DIFFERENCE', 'SUBTRACT', 'DIVIDE', 'HUE', 'SATURATION', 'COLOR', 'VALUE']
    
    # True if this node is used as the active output
    is_active_output: bool
    
    # Which renderer and viewport shading types to use the shaders for
    target: str = 'ALL' # ['ALL', 'EEVEE', 'CYCLES']
    
    # Include alpha of second input in this operation
    use_alpha: bool
    
    # Clamp result of the node to 0.0 to 1.0 range
    use_clamp: bool

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...