from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MeshVertex(bpy_struct):
    
    # Weight used by the Bevel modifier ‘Only Vertices’ option
    bevel_weight: float
    
    co: Tuple[float, float, float]
    
    # Weights for the vertex groups this vertex is member of
    groups: List[VertexGroupElement]
    
    hide: bool
    
    # Index of this vertex
    index: int
    
    # Vertex Normal
    normal: Tuple[float, float, float]
    
    select: bool
    
    # For meshes with modifiers applied, the coordinate of the vertex with no deforming modifiers applied, as used for generated texture coordinates
    undeformed_co: Tuple[float, float, float]

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...