from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class NodeLink(bpy_struct):
    
    from_node: Node
    
    from_socket: NodeSocket
    
    # Link is hidden due to invisible sockets
    is_hidden: bool
    
    is_muted: bool
    
    is_valid: bool
    
    to_node: Node
    
    to_socket: NodeSocket

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...