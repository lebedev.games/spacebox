from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class RigidBodyWorld(bpy_struct):
    
    # Collection containing objects participating in this simulation
    collection: Collection
    
    # Collection containing rigid body constraint objects
    constraints: Collection
    
    effector_weights: EffectorWeights
    
    # Simulation will be evaluated
    enabled: bool
    
    point_cache: PointCache
    
    # Number of constraint solver iterations made per simulation step (higher values are more accurate but slower)
    solver_iterations: int
    
    # Number of simulation steps taken per frame (higher values are more accurate but slower)
    substeps_per_frame: int
    
    # Change the speed of the simulation
    time_scale: float
    
    # Reduce extra velocity that can build up when objects collide (lowers simulation stability a little so use only when necessary)
    use_split_impulse: bool

    def convex_sweep_test(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...