from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Screen(ID):
    
    # Areas the screen is subdivided into
    areas: List[Area]
    
    # Animation playback is active
    is_animation_playing: bool
    
    # True when the user is scrubbing through time
    is_scrubbing: bool
    
    is_temporary: bool
    
    # An area is maximized, filling this screen
    show_fullscreen: bool
    
    # Show status bar
    show_statusbar: bool
    
    # Follow current frame in editors
    use_follow: bool
    
    use_play_3d_editors: bool
    
    use_play_animation_editors: bool
    
    use_play_clip_editors: bool
    
    use_play_image_editors: bool
    
    use_play_node_editors: bool
    
    use_play_properties_editors: bool
    
    use_play_sequence_editors: bool
    
    use_play_top_left_3d_editor: bool

    def statusbar_info(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...