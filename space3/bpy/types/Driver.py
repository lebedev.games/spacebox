from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Driver(bpy_struct):
    
    # Expression to use for Scripted Expression
    expression: str
    
    # The scripted expression can be evaluated without using the full python interpreter
    is_simple_expression: bool
    
    # Driver could not be evaluated in past, so should be skipped
    is_valid: bool
    
    # Driver type
    type: str = 'AVERAGE' # ['AVERAGE', 'SUM', 'SCRIPTED', 'MIN', 'MAX']
    
    # Include a ‘self’ variable in the name-space, so drivers can easily reference the data being modified (object, bone, etc…)
    use_self: bool
    
    # Properties acting as inputs for this driver
    variables: ChannelDriverVariables

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...