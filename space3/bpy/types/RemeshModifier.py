from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class RemeshModifier(Modifier):
    
    # Reduces the final face count by simplifying geometry where detail is not needed, generating triangles. A value greater than 0 disables Fix Poles
    adaptivity: float
    
    mode: str = 'VOXEL' # ['BLOCKS', 'SMOOTH', 'SHARP', 'VOXEL']
    
    # Resolution of the octree; higher values give finer details
    octree_depth: int
    
    # The ratio of the largest dimension of the model over the size of the grid
    scale: float
    
    # Tolerance for outliers; lower values filter noise while higher values will reproduce edges closer to the input
    sharpness: float
    
    # If removing disconnected pieces, minimum size of components to preserve as a ratio of the number of polygons in the largest component
    threshold: float
    
    use_remove_disconnected: bool
    
    # Output faces with smooth shading rather than flat shaded
    use_smooth_shade: bool
    
    # Size of the voxel in object space used for volume evaluation. Lower values preserve finer details
    voxel_size: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...