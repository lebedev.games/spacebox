from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class KeyingSetPath(bpy_struct):
    
    # Index to the specific setting if applicable
    array_index: int
    
    # Path to property setting
    data_path: str
    
    # Name of Action Group to assign setting(s) for this path to
    group: str
    
    # Method used to define which Group-name to use
    group_method: str = 'NAMED' # ['NAMED', 'NONE', 'KEYINGSET']
    
    # ID-Block that keyframes for Keying Set should be added to (for Absolute Keying Sets only)
    id: ID
    
    # Type of ID-block that can be used
    id_type: str = 'OBJECT' # ['ACTION', 'ARMATURE', 'BRUSH', 'CAMERA', 'CACHEFILE', 'CURVE', 'FONT', 'GREASEPENCIL', 'COLLECTION', 'IMAGE', 'KEY', 'LIGHT', 'LIBRARY', 'LINESTYLE', 'LATTICE', 'MASK', 'MATERIAL', 'META', 'MESH', 'MOVIECLIP', 'NODETREE', 'OBJECT', 'PAINTCURVE', 'PALETTE', 'PARTICLE', 'LIGHT_PROBE', 'SCENE', 'SIMULATION', 'SOUND', 'SPEAKER', 'TEXT', 'TEXTURE', 'HAIR', 'POINTCLOUD', 'VOLUME', 'WINDOWMANAGER', 'WORLD', 'WORKSPACE']
    
    # When an ‘array/vector’ type is chosen (Location, Rotation, Color, etc.), entire array is to be used
    use_entire_array: bool
    
    # Only insert keyframes where they’re needed in the relevant F-Curves
    use_insertkey_needed: bool
    
    # Override default setting to only insert keyframes where they’re needed in the relevant F-Curves
    use_insertkey_override_needed: bool
    
    # Override default setting to insert keyframes based on ‘visual transforms’
    use_insertkey_override_visual: bool
    
    # Override default setting to set color for newly added transformation F-Curves (Location, Rotation, Scale) to be based on the transform axis
    use_insertkey_override_xyz_to_rgb: bool
    
    # Insert keyframes based on ‘visual transforms’
    use_insertkey_visual: bool
    
    # Color for newly added transformation F-Curves (Location, Rotation, Scale) is based on the transform axis
    use_insertkey_xyz_to_rgb: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...