from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class KeyingSet(bpy_struct):
    
    # A short description of the keying set
    bl_description: str
    
    # If this is set, the Keying Set gets a custom ID, otherwise it takes the name of the class used to define the Keying Set (for example, if the class name is “BUILTIN_KSI_location”, and bl_idname is not set by the script, then bl_idname = “BUILTIN_KSI_location”)
    bl_idname: str
    
    bl_label: str
    
    # Keying Set defines specific paths/settings to be keyframed (i.e. is not reliant on context info)
    is_path_absolute: bool
    
    # Keying Set Paths to define settings that get keyframed together
    paths: KeyingSetPaths
    
    # Callback function defines for built-in Keying Sets
    type_info: KeyingSetInfo
    
    # Only insert keyframes where they’re needed in the relevant F-Curves
    use_insertkey_needed: bool
    
    # Override default setting to only insert keyframes where they’re needed in the relevant F-Curves
    use_insertkey_override_needed: bool
    
    # Override default setting to insert keyframes based on ‘visual transforms’
    use_insertkey_override_visual: bool
    
    # Override default setting to set color for newly added transformation F-Curves (Location, Rotation, Scale) to be based on the transform axis
    use_insertkey_override_xyz_to_rgb: bool
    
    # Insert keyframes based on ‘visual transforms’
    use_insertkey_visual: bool
    
    # Color for newly added transformation F-Curves (Location, Rotation, Scale) is based on the transform axis
    use_insertkey_xyz_to_rgb: bool

    def refresh(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...