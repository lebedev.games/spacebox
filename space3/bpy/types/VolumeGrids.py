from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class VolumeGrids(bpy_struct):
    
    # Index of active volume grid
    active_index: int
    
    # If loading grids failed, error message with details
    error_message: str
    
    # Frame number that volume grids will be loaded at, based on scene time and volume parameters
    frame: int
    
    # Volume file used for loading the volume at the current frame. Empty if the volume has not be loaded or the frame only exists in memory
    frame_filepath: str
    
    # List of grids and metadata are loaded in memory
    is_loaded: bool

    def load(self, *args, **kwargs) -> None:
        ...

    def unload(self, *args, **kwargs) -> None:
        ...

    def save(self, *args, **kwargs) -> bool:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...