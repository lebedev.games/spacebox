from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SolidifyModifier(Modifier):
    
    # Edge bevel weight to be added to outside edges
    bevel_convex: float
    
    # Assign a crease to inner edges
    edge_crease_inner: float
    
    # Assign a crease to outer edges
    edge_crease_outer: float
    
    # Assign a crease to the edges making up the rim
    edge_crease_rim: float
    
    # Invert the vertex group influence
    invert_vertex_group: bool
    
    # Offset material index of generated faces
    material_offset: int
    
    # Offset material index of generated rim faces
    material_offset_rim: int
    
    # Selects the boundary adjustment algorithm
    nonmanifold_boundary_mode: str = 'NONE' # ['NONE', 'ROUND', 'FLAT']
    
    # Distance within which degenerated geometry is merged
    nonmanifold_merge_threshold: float
    
    # Selects the used thickness algorithm
    nonmanifold_thickness_mode: str = 'CONSTRAINTS' # ['FIXED', 'EVEN', 'CONSTRAINTS']
    
    # Offset the thickness from the center
    offset: float
    
    # Vertex group that the generated rim geometry will be weighted to
    rim_vertex_group: str
    
    # Vertex group that the generated shell geometry will be weighted to
    shell_vertex_group: str
    
    # Selects the used algorithm
    solidify_mode: str = 'EXTRUDE' # ['EXTRUDE', 'NON_MANIFOLD']
    
    # Thickness of the shell
    thickness: float
    
    # Offset clamp based on geometry scale
    thickness_clamp: float
    
    # Thickness factor to use for zero vertex group influence
    thickness_vertex_group: float
    
    # Maintain thickness by adjusting for sharp corners (slow, disable when not needed)
    use_even_offset: bool
    
    # Make faces use the minimal vertex weight assigned to their vertices(ensures new faces remain parallel to their original ones, slow, disable when not needed)
    use_flat_faces: bool
    
    # Invert the face direction
    use_flip_normals: bool
    
    # Calculate normals which result in more even thickness (slow, disable when not needed)
    use_quality_normals: bool
    
    # Create edge loops between the inner and outer surfaces on face edges (slow, disable when not needed)
    use_rim: bool
    
    # Only add the rim to the original data
    use_rim_only: bool
    
    # Clamp thickness based on angles
    use_thickness_angle_clamp: bool
    
    # Vertex group name
    vertex_group: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...