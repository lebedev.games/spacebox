from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MotionPath(bpy_struct):
    
    # Custom color for motion path
    color: Tuple[float, float, float]
    
    # End frame of the stored range
    frame_end: int
    
    # Starting frame of the stored range
    frame_start: int
    
    # Path is being edited
    is_modified: bool
    
    # Number of frames cached
    length: int
    
    # Line thickness for motion path
    line_thickness: int
    
    # Use straight lines between keyframe points
    lines: bool
    
    # Cached positions per frame
    points: List[MotionPathVert]
    
    # For PoseBone paths, use the bone head location when calculating this path
    use_bone_head: bool
    
    # Use custom color for this motion path
    use_custom_color: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...