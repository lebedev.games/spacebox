from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class AnimViz(bpy_struct):
    
    # Motion Path settings for visualization
    motion_path: AnimVizMotionPaths

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...