from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class FCurveKeyframePoints(bpy_struct):

    def insert(self, *args, **kwargs) -> Keyframe:
        ...

    def add(self, *args, **kwargs) -> None:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...