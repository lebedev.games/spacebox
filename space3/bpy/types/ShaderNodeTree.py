from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ShaderNodeTree(NodeTree):

    def get_output_node(self, *args, **kwargs) -> ShaderNode:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...