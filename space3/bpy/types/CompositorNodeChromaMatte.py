from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CompositorNodeChromaMatte(CompositorNode):
    
    # Alpha falloff
    gain: float
    
    # Alpha lift
    lift: float
    
    # Adjusts the brightness of any shadows captured
    shadow_adjust: float
    
    # Tolerance below which colors will be considered as exact matches
    threshold: float
    
    # Tolerance for a color to be considered a keying color
    tolerance: float

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    def update(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...