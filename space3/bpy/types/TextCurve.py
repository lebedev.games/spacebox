from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class TextCurve(Curve):
    
    active_textbox: int
    
    # Text horizontal align from the object center
    align_x: str = 'LEFT' # ['LEFT', 'CENTER', 'RIGHT', 'JUSTIFY', 'FLUSH']
    
    # Text vertical align from the object center
    align_y: str = 'TOP_BASELINE' # ['TOP_BASELINE', 'TOP', 'CENTER', 'BOTTOM', 'BOTTOM_BASELINE']
    
    # Content of this text object
    body: str
    
    # Stores the style of each character
    body_format: List[TextCharacterFormat]
    
    # Editing settings character formatting
    edit_format: TextCharacterFormat
    
    # Use objects as font characters (give font objects a common name followed by the character they represent, eg. ‘family-a’, ‘family-b’, etc, set this setting to ‘family-‘, and turn on Vertex Instancing)
    family: str
    
    # Curve deforming text object
    follow_curve: Object
    
    font: VectorFont
    
    font_bold: VectorFont
    
    font_bold_italic: VectorFont
    
    font_italic: VectorFont
    
    # Horizontal offset from the object origin
    offset_x: float
    
    # Vertical offset from the object origin
    offset_y: float
    
    # Handle the text behavior when it doesn’t fit in the text boxes
    overflow: str = 'NONE' # ['NONE', 'SCALE', 'TRUNCATE']
    
    # Italic angle of the characters
    shear: float
    
    size: float
    
    # Scale of small capitals
    small_caps_scale: float
    
    space_character: float
    
    space_line: float
    
    space_word: float
    
    text_boxes: List[TextBox]
    
    underline_height: float
    
    # Vertical position of underline
    underline_position: float
    
    # Don’t fill polygons while editing
    use_fast_edit: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...