from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SmoothGpencilModifier(GpencilModifier):
    
    # Custom curve to apply effect
    curve: CurveMapping
    
    # Amount of smooth to apply
    factor: float
    
    # Inverse filter
    invert_layer_pass: bool
    
    # Inverse filter
    invert_layers: bool
    
    # Inverse filter
    invert_material_pass: bool
    
    # Inverse filter
    invert_materials: bool
    
    # Inverse filter
    invert_vertex: bool
    
    # Layer name
    layer: str
    
    # Layer pass index
    layer_pass: int
    
    # Material used for filtering effect
    material: Material
    
    # Pass index
    pass_index: int
    
    # Number of times to apply smooth (high numbers can reduce fps)
    step: int
    
    # Use a custom curve to define smooth effect along the strokes
    use_custom_curve: bool
    
    # The modifier affects the position of the point
    use_edit_position: bool
    
    # The modifier affects the color strength of the point
    use_edit_strength: bool
    
    # The modifier affects the thickness of the point
    use_edit_thickness: bool
    
    # The modifier affects the UV rotation factor of the point
    use_edit_uv: bool
    
    # Vertex group name for modulating the deform
    vertex_group: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...