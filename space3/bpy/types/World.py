from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class World(ID):
    
    # Animation data for this data-block
    animation_data: AnimData
    
    # Color of the background
    color: Tuple[float, float, float]
    
    # Cycles world settings
    cycles: Any
    
    # Cycles visibility settings
    cycles_visibility: Any
    
    # World lighting settings
    light_settings: WorldLighting
    
    # World mist settings
    mist_settings: WorldMistSettings
    
    # Node tree for node based worlds
    node_tree: NodeTree
    
    # Use shader nodes to render the world
    use_nodes: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...