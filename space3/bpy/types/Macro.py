from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Macro(bpy_struct):
    
    bl_description: str
    
    bl_idname: str
    
    bl_label: str
    
    # Options for this operator type
    bl_options: Set[str] # {'BLOCKING', 'INTERNAL}', 'default {REGISTER', 'REGISTER', 'UNDO_GROUPED', 'UNDO', 'GRAB_CURSOR_Y', 'PRESET', 'MACRO', 'GRAB_CURSOR', 'GRAB_CURSOR_X'}
    
    bl_translation_context: str
    
    bl_undo_group: str
    
    name: str
    
    properties: OperatorProperties

    def report(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def poll(cls, *args, **kwargs) -> bool:
        ...

    def draw(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...