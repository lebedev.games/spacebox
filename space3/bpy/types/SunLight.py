from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SunLight(Light):
    
    # Angular diameter of the Sun as seen from the Earth
    angle: float
    
    # Bias to avoid self shadowing
    contact_shadow_bias: float
    
    # World space distance in which to search for screen space occluder
    contact_shadow_distance: float
    
    # Pixel thickness used to detect occlusion
    contact_shadow_thickness: float
    
    # Sunlight strength in watts per meter squared (W/m^2)
    energy: float
    
    # Bias for reducing self shadowing
    shadow_buffer_bias: float
    
    # Shadow map clip start, below which objects will not generate shadows
    shadow_buffer_clip_start: float
    
    # Number of shadow buffer samples
    shadow_buffer_samples: int
    
    # Resolution of the shadow buffer, higher values give crisper shadows but use more memory
    shadow_buffer_size: int
    
    # Number of texture used by the cascaded shadow map
    shadow_cascade_count: int
    
    # Higher value increase resolution towards the viewpoint
    shadow_cascade_exponent: float
    
    # How smooth is the transition between each cascade
    shadow_cascade_fade: float
    
    # End distance of the cascaded shadow map (only in perspective view)
    shadow_cascade_max_distance: float
    
    # Color of shadows cast by the light
    shadow_color: Tuple[float, float, float]
    
    # Light size for ray shadow sampling (Raytraced shadows)
    shadow_soft_size: float
    
    # Use screen space raytracing to have correct shadowing near occluder, or for small features that does not appear in shadow maps
    use_contact_shadow: bool
    
    use_shadow: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...