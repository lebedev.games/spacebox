from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class GPencilStrokePoints(bpy_struct):

    def add(self, *args, **kwargs) -> None:
        ...

    def pop(self, *args, **kwargs) -> None:
        ...

    def update(self, *args, **kwargs) -> None:
        ...

    def weight_get(self, *args, **kwargs) -> float:
        ...

    def weight_set(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...