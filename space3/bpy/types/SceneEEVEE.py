from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SceneEEVEE(bpy_struct):
    
    # Maximum intensity a bloom pixel can have (0 to disabled)
    bloom_clamp: float
    
    # Color applied to the bloom effect
    bloom_color: Tuple[float, float, float]
    
    # Blend factor
    bloom_intensity: float
    
    # Makes transition between under/over-threshold gradual
    bloom_knee: float
    
    # Bloom spread distance
    bloom_radius: float
    
    # Filters out pixels under this level of brightness
    bloom_threshold: float
    
    # Amount of flicker removal applied to bokeh highlights
    bokeh_denoise_fac: float
    
    # Max size of the bokeh shape for the depth of field (lower is faster)
    bokeh_max_size: float
    
    # Maximum brightness to consider when rejecting bokeh sprites based on neighborhood (lower is faster)
    bokeh_neighbor_max: float
    
    # Apply blur to each jittered sample to reduce under-sampling artifacts
    bokeh_overblur: float
    
    # Brightness threshold for using sprite base depth of field
    bokeh_threshold: float
    
    # Auto bake indirect lighting when editing probes
    gi_auto_bake: bool
    
    # Info on current cache status
    gi_cache_info: str
    
    # Size of the cubemap spheres to debug captured light
    gi_cubemap_display_size: float
    
    # Size of every cubemaps
    gi_cubemap_resolution: str = '512' # ['64', '128', '256', '512', '1024', '2048', '4096']
    
    # Number of time the light is reinjected inside light grids, 0 disable indirect diffuse light
    gi_diffuse_bounces: int
    
    # Take more samples during cubemap filtering to remove artifacts
    gi_filter_quality: float
    
    # Clamp pixel intensity to reduce noise inside glossy reflections from reflection cubemaps (0 to disabled)
    gi_glossy_clamp: float
    
    # Size of the irradiance sample spheres to debug captured light
    gi_irradiance_display_size: float
    
    # Smoother irradiance interpolation but introduce light bleeding
    gi_irradiance_smoothing: float
    
    # Display captured cubemaps in the viewport
    gi_show_cubemaps: bool
    
    # Display irradiance samples in the viewport
    gi_show_irradiance: bool
    
    # Size of the shadow map applied to each irradiance sample
    gi_visibility_resolution: str = '32' # ['8', '16', '32', '64']
    
    # Distance of object that contribute to the ambient occlusion effect
    gtao_distance: float
    
    # Factor for ambient occlusion blending
    gtao_factor: float
    
    # Precision of the horizon search
    gtao_quality: float
    
    # Minimum light intensity for a light to contribute to the lighting
    light_threshold: float
    
    # Lower values will reduce background bleeding onto foreground elements
    motion_blur_depth_scale: float
    
    # Maximum blur distance a pixel can spread over
    motion_blur_max: int
    
    # Offset for the shutter’s time interval, allows to change the motion blur trails
    motion_blur_position: str = 'CENTER' # ['START', 'CENTER', 'END']
    
    # Time taken in frames between shutter open and close
    motion_blur_shutter: float
    
    # Controls accuracy of motion blur, more steps means longer render time
    motion_blur_steps: int
    
    # Percentage of render size to add as overscan to the internal render buffers
    overscan_size: float
    
    # Size of sun light shadow maps
    shadow_cascade_size: str = '1024' # ['64', '128', '256', '512', '1024', '2048', '4096']
    
    # Size of point and area light shadow maps
    shadow_cube_size: str = '512' # ['64', '128', '256', '512', '1024', '2048', '4096']
    
    # Screen percentage used to fade the SSR
    ssr_border_fade: float
    
    # Clamp pixel intensity to remove noise (0 to disabled)
    ssr_firefly_fac: float
    
    # Do not raytrace reflections for roughness above this value
    ssr_max_roughness: float
    
    # Precision of the screen space raytracing
    ssr_quality: float
    
    # Pixel thickness used to detect intersection
    ssr_thickness: float
    
    # Rotate samples that are below this threshold
    sss_jitter_threshold: float
    
    # Number of samples to compute the scattering effect
    sss_samples: int
    
    # Number of samples per pixels for rendering
    taa_render_samples: int
    
    # Number of samples, unlimited if 0
    taa_samples: int
    
    # High brightness pixels generate a glowing effect
    use_bloom: bool
    
    # Sample all pixels in almost in-focus regions to eliminate noise
    use_bokeh_high_quality_slight_defocus: bool
    
    # Jitter camera position to create accurate blurring using render samples
    use_bokeh_jittered: bool
    
    # Enable ambient occlusion to simulate medium scale indirect shadowing
    use_gtao: bool
    
    # Compute main non occluded direction to sample the environment
    use_gtao_bent_normals: bool
    
    # An approximation to simulate light bounces giving less occlusion on brighter objects
    use_gtao_bounce: bool
    
    # Enable motion blur effect (only in camera view)
    use_motion_blur: bool
    
    # Internally render past the image border to avoid screen-space effects disappearing
    use_overscan: bool
    
    # Use 32-bit shadows
    use_shadow_high_bitdepth: bool
    
    # Randomize shadowmaps origin to create soft shadows
    use_soft_shadows: bool
    
    # Enable screen space reflection
    use_ssr: bool
    
    # Raytrace at a lower resolution
    use_ssr_halfres: bool
    
    # Enable screen space Refractions
    use_ssr_refraction: bool
    
    # Denoise image using temporal reprojection (can leave some ghosting)
    use_taa_reprojection: bool
    
    # Enable scene light interactions with volumetrics
    use_volumetric_lights: bool
    
    # Generate shadows from volumetric material (Very expensive)
    use_volumetric_shadows: bool
    
    # End distance of the volumetric effect
    volumetric_end: float
    
    # Maximum light contribution, reducing noise
    volumetric_light_clamp: float
    
    # Distribute more samples closer to the camera
    volumetric_sample_distribution: float
    
    # Number of samples to compute volumetric effects
    volumetric_samples: int
    
    # Number of samples to compute volumetric shadowing
    volumetric_shadow_samples: int
    
    # Start distance of the volumetric effect
    volumetric_start: float
    
    # Control the quality of the volumetric effects (lower size increase vram usage and quality)
    volumetric_tile_size: str = '8' # ['2', '4', '8', '16']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...