from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class UVProjectModifier(Modifier):
    
    # Horizontal aspect ratio (only used for camera projectors)
    aspect_x: float
    
    # Vertical aspect ratio (only used for camera projectors)
    aspect_y: float
    
    # Number of projectors to use
    projector_count: int
    
    projectors: List[UVProjector]
    
    # Horizontal scale (only used for camera projectors)
    scale_x: float
    
    # Vertical scale (only used for camera projectors)
    scale_y: float
    
    # UV map name
    uv_layer: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...