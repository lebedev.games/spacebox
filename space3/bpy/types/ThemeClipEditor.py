from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ThemeClipEditor(bpy_struct):
    
    # Color of active marker
    active_marker: Tuple[float, float, float]
    
    # Color of disabled marker
    disabled_marker: Tuple[float, float, float]
    
    frame_current: Tuple[float, float, float]
    
    grid: Tuple[float, float, float, float]
    
    handle_align: Tuple[float, float, float]
    
    handle_auto: Tuple[float, float, float]
    
    handle_auto_clamped: Tuple[float, float, float]
    
    handle_free: Tuple[float, float, float]
    
    handle_sel_align: Tuple[float, float, float]
    
    handle_sel_auto: Tuple[float, float, float]
    
    handle_sel_auto_clamped: Tuple[float, float, float]
    
    handle_sel_free: Tuple[float, float, float]
    
    handle_vertex: Tuple[float, float, float]
    
    handle_vertex_select: Tuple[float, float, float]
    
    handle_vertex_size: int
    
    # Color of locked marker
    locked_marker: Tuple[float, float, float]
    
    # Color of marker
    marker: Tuple[float, float, float]
    
    # Color of marker’s outline
    marker_outline: Tuple[float, float, float]
    
    metadatabg: Tuple[float, float, float]
    
    metadatatext: Tuple[float, float, float]
    
    # Color of path after current frame
    path_after: Tuple[float, float, float]
    
    # Color of path before current frame
    path_before: Tuple[float, float, float]
    
    # Color of path after current frame
    path_keyframe_after: Tuple[float, float, float]
    
    # Color of path before current frame
    path_keyframe_before: Tuple[float, float, float]
    
    # Color of selected marker
    selected_marker: Tuple[float, float, float]
    
    # Settings for space
    space: ThemeSpaceGeneric
    
    # Settings for space list
    space_list: ThemeSpaceListGeneric
    
    strips: Tuple[float, float, float]
    
    strips_selected: Tuple[float, float, float]
    
    time_marker_line: Tuple[float, float, float, float]
    
    time_marker_line_selected: Tuple[float, float, float, float]
    
    time_scrub_background: Tuple[float, float, float, float]

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...