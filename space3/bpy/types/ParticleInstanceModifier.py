from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ParticleInstanceModifier(Modifier):
    
    # Pole axis for rotation
    axis: str = 'Z' # ['X', 'Y', 'Z']
    
    # Custom data layer name for the index
    index_layer_name: str
    
    # Object that has the particle system
    object: Object
    
    # Amount of particles to use for instancing
    particle_amount: float
    
    # Relative offset of particles to use for instancing, to avoid overlap of multiple instances
    particle_offset: float
    
    particle_system: ParticleSystem
    
    particle_system_index: int
    
    # Position along path
    position: float
    
    # Randomize position along path
    random_position: float
    
    # Randomize rotation around path
    random_rotation: float
    
    # Rotation around path
    rotation: float
    
    # Show instances when particles are alive
    show_alive: bool
    
    # Show instances when particles are dead
    show_dead: bool
    
    # Show instances when particles are unborn
    show_unborn: bool
    
    # Space to use for copying mesh data
    space: str = 'WORLD' # ['LOCAL', 'WORLD']
    
    # Create instances from child particles
    use_children: bool
    
    # Create instances from normal particles
    use_normal: bool
    
    # Create instances along particle paths
    use_path: bool
    
    # Don’t stretch the object
    use_preserve_shape: bool
    
    # Use particle size to scale the instances
    use_size: bool
    
    # Custom data layer name for the randomized value
    value_layer_name: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...