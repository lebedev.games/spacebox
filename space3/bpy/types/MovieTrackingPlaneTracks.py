from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MovieTrackingPlaneTracks(bpy_struct):
    
    # Active plane track in this tracking data object
    active: MovieTrackingPlaneTrack

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...