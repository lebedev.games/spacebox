from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class FileSelectEntry(bpy_struct):
    
    # Asset data, valid if the file represents an asset
    asset_data: AssetMetaData
    
    name: str
    
    # Unique integer identifying the preview of this file as an icon (zero means invalid)
    preview_icon_id: int

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...