from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MaskSpline(bpy_struct):
    
    # The method used for calculating the feather offset
    offset_mode: str = 'EVEN' # ['EVEN', 'SMOOTH']
    
    # Collection of points
    points: MaskSplinePoints
    
    # Make this spline a closed loop
    use_cyclic: bool
    
    # Make this spline filled
    use_fill: bool
    
    # Prevent feather from self-intersections
    use_self_intersection_check: bool
    
    # The type of weight interpolation for spline
    weight_interpolation: str = 'LINEAR' # ['LINEAR', 'EASE']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...