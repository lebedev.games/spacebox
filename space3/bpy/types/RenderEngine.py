from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class RenderEngine(bpy_struct):
    
    bl_idname: str
    
    bl_label: str
    
    # Handles freestyle rendering on its own, instead of delegating it to EEVEE
    bl_use_custom_freestyle: bool
    
    # Uses Eevee for viewport shading in LookDev shading mode
    bl_use_eevee_viewport: bool
    
    # Enable OpenGL context for the render method, for engines that render using OpenGL
    bl_use_gpu_context: bool
    
    # Apply compositing on render results
    bl_use_postprocess: bool
    
    # Render engine supports being used for rendering previews of materials, lights and worlds
    bl_use_preview: bool
    
    # Support render to an on disk buffer during rendering
    bl_use_save_buffers: bool
    
    # Don’t expose Cycles and Eevee shading nodes in the node editor user interface, so own nodes can be used instead
    bl_use_shading_nodes_custom: bool
    
    # Support spherical stereo camera models
    bl_use_spherical_stereo: bool
    
    # Support rendering stereo 3D viewport
    bl_use_stereo_viewport: bool
    
    camera_override: Object
    
    is_animation: bool
    
    is_preview: bool
    
    layer_override: bool
    
    render: RenderSettings
    
    resolution_x: int
    
    resolution_y: int
    
    tile_x: int
    
    tile_y: int
    
    use_highlight_tiles: bool

    def update(self, *args, **kwargs) -> None:
        ...

    def bake(self, *args, **kwargs) -> None:
        ...

    def view_update(self, *args, **kwargs) -> None:
        ...

    def view_draw(self, *args, **kwargs) -> None:
        ...

    def update_script_node(self, *args, **kwargs) -> None:
        ...

    def update_render_passes(self, *args, **kwargs) -> None:
        ...

    def tag_redraw(self, *args, **kwargs) -> None:
        ...

    def tag_update(self, *args, **kwargs) -> None:
        ...

    def begin_result(self, *args, **kwargs) -> RenderResult:
        ...

    def update_result(self, *args, **kwargs) -> None:
        ...

    def end_result(self, *args, **kwargs) -> None:
        ...

    def add_pass(self, *args, **kwargs) -> None:
        ...

    def get_result(self, *args, **kwargs) -> None:
        ...

    def test_break(self, *args, **kwargs) -> None:
        ...

    def active_view_get(self, *args, **kwargs) -> None:
        ...

    def active_view_set(self, *args, **kwargs) -> None:
        ...

    def camera_shift_x(self, *args, **kwargs) -> float:
        ...

    def camera_model_matrix(self, *args, **kwargs) -> List[float]:
        ...

    def use_spherical_stereo(self, *args, **kwargs) -> None:
        ...

    def update_stats(self, *args, **kwargs) -> None:
        ...

    def frame_set(self, *args, **kwargs) -> None:
        ...

    def update_progress(self, *args, **kwargs) -> None:
        ...

    def update_memory_stats(self, *args, **kwargs) -> None:
        ...

    def report(self, *args, **kwargs) -> None:
        ...

    def error_set(self, *args, **kwargs) -> None:
        ...

    def bind_display_space_shader(self, *args, **kwargs) -> None:
        ...

    def unbind_display_space_shader(self, *args, **kwargs) -> None:
        ...

    def support_display_space_shader(self, *args, **kwargs) -> None:
        ...

    def get_preview_pixel_size(self, *args, **kwargs) -> None:
        ...

    def free_blender_memory(self, *args, **kwargs) -> None:
        ...

    def register_pass(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...