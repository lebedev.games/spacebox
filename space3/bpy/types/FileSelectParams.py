from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class FileSelectParams(bpy_struct):
    
    # Directory displayed in the file browser
    directory: str
    
    # Change the size of the display (width of columns or thumbnails size)
    display_size: str = 'TINY' # ['TINY', 'SMALL', 'NORMAL', 'LARGE']
    
    # Display mode for the file list
    display_type: str = 'LIST_VERTICAL' # ['LIST_VERTICAL', 'LIST_HORIZONTAL', 'THUMBNAIL']
    
    # Active file in the file browser
    filename: str
    
    # UNIX shell-like filename patterns matching, supports wildcards (‘*’) and list of patterns separated by ‘;’
    filter_glob: str
    
    # Which ID types to show/hide, when browsing a library
    filter_id: FileSelectIDFilter
    
    # Filter by name, supports ‘*’ wildcard
    filter_search: str
    
    # Numbers of dirtree levels to show simultaneously
    recursion_level: str = 'NONE' # ['NONE', 'BLEND', 'ALL_1', 'ALL_2', 'ALL_3']
    
    # Show a column listing the date and time of modification for each file
    show_details_datetime: bool
    
    # Show a column listing the size of each file
    show_details_size: bool
    
    # Show hidden dot files
    show_hidden: bool
    
    sort_method: str = 'FILE_SORT_ALPHA' # ['FILE_SORT_ALPHA', 'FILE_SORT_EXTENSION', 'FILE_SORT_TIME', 'FILE_SORT_SIZE']
    
    # Title for the file browser
    title: str
    
    # Enable filtering of files
    use_filter: bool
    
    # Hide .blend files items that are not data-blocks with asset metadata
    use_filter_asset_only: bool
    
    # Show .blend1, .blend2, etc. files
    use_filter_backup: bool
    
    # Show .blend files
    use_filter_blender: bool
    
    # Show .blend files items (objects, materials, etc.)
    use_filter_blendid: bool
    
    # Show folders
    use_filter_folder: bool
    
    # Show font files
    use_filter_font: bool
    
    # Show image files
    use_filter_image: bool
    
    # Show movie files
    use_filter_movie: bool
    
    # Show script files
    use_filter_script: bool
    
    # Show sound files
    use_filter_sound: bool
    
    # Show text files
    use_filter_text: bool
    
    # Show 3D volume files
    use_filter_volume: bool
    
    # Whether we may browse blender files’ content or not
    use_library_browsing: bool
    
    # Sort items descending, from highest value to lowest
    use_sort_invert: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...