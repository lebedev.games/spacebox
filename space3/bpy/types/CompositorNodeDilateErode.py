from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CompositorNodeDilateErode(CompositorNode):
    
    # Distance to grow/shrink (number of iterations)
    distance: int
    
    # Edge to inset
    edge: float
    
    # Falloff type the feather
    falloff: str = 'SMOOTH' # ['SMOOTH', 'SPHERE', 'ROOT', 'INVERSE_SQUARE', 'SHARP', 'LINEAR']
    
    # Growing/shrinking mode
    mode: str = 'STEP' # ['STEP', 'THRESHOLD', 'DISTANCE', 'FEATHER']

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    def update(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...