from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class NodeCustomGroup(Node):
    
    # Interface socket data
    interface: PropertyGroup
    
    node_tree: NodeTree

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...