from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class WalkNavigation(bpy_struct):
    
    # Maximum height of a jump
    jump_height: float
    
    # Speed factor for when looking around, high values mean faster mouse movement
    mouse_speed: float
    
    # Interval of time warp when teleporting in navigation mode
    teleport_time: float
    
    # Walk with gravity, or free navigate
    use_gravity: bool
    
    # Reverse the vertical movement of the mouse
    use_mouse_reverse: bool
    
    # View distance from the floor when walking
    view_height: float
    
    # Base speed for walking and flying
    walk_speed: float
    
    # Multiplication factor when using the fast or slow modifiers
    walk_speed_factor: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...