from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ShapeKey(bpy_struct):
    
    data: List[UnknownType]
    
    # Frame for absolute keys
    frame: float
    
    # Interpolation type for absolute shape keys
    interpolation: str = 'KEY_LINEAR' # ['KEY_LINEAR', 'KEY_CARDINAL', 'KEY_CATMULL_ROM', 'KEY_BSPLINE']
    
    # Toggle this shape key
    mute: bool
    
    # Name of Shape Key
    name: str
    
    # Shape used as a relative key
    relative_key: 'ShapeKey'
    
    # Maximum for slider
    slider_max: float
    
    # Minimum for slider
    slider_min: float
    
    # Value of shape key at the current frame
    value: float
    
    # Vertex weight group, to blend with basis shape
    vertex_group: str

    def normals_vertex_get(self, *args, **kwargs) -> None:
        ...

    def normals_polygon_get(self, *args, **kwargs) -> None:
        ...

    def normals_split_get(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...