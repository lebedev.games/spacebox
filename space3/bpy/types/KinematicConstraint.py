from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class KinematicConstraint(Constraint):
    
    # How many bones are included in the IK effect - 0 uses all bones
    chain_count: int
    
    # Radius of limiting sphere
    distance: float
    
    ik_type: str = 'COPY_POSE' # ['COPY_POSE', 'DISTANCE']
    
    # Maximum number of solving iterations
    iterations: int
    
    # Distances in relation to sphere of influence to allow
    limit_mode: str = 'LIMITDIST_INSIDE' # ['LIMITDIST_INSIDE', 'LIMITDIST_OUTSIDE', 'LIMITDIST_ONSURFACE']
    
    # Constraint position along X axis
    lock_location_x: bool
    
    # Constraint position along Y axis
    lock_location_y: bool
    
    # Constraint position along Z axis
    lock_location_z: bool
    
    # Constraint rotation along X axis
    lock_rotation_x: bool
    
    # Constraint rotation along Y axis
    lock_rotation_y: bool
    
    # Constraint rotation along Z axis
    lock_rotation_z: bool
    
    # For Tree-IK: Weight of orientation control for this target
    orient_weight: float
    
    # Pole rotation offset
    pole_angle: float
    
    pole_subtarget: str
    
    # Object for pole rotation
    pole_target: Object
    
    # Constraint axis Lock options relative to Bone or Target reference
    reference_axis: str = 'BONE' # ['BONE', 'TARGET']
    
    # Armature bone, mesh or lattice vertex group, …
    subtarget: str
    
    # Target object
    target: Object
    
    # Chain follows position of target
    use_location: bool
    
    # Chain follows rotation of target
    use_rotation: bool
    
    # Enable IK Stretching
    use_stretch: bool
    
    # Include bone’s tail as last element in chain
    use_tail: bool
    
    # For Tree-IK: Weight of position control for this target
    weight: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...