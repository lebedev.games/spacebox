from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class BoidRule(bpy_struct):
    
    # Boid rule name
    name: str
    
    type: str = 'GOAL' # ['GOAL', 'AVOID', 'AVOID_COLLISION', 'SEPARATE', 'FLOCK', 'FOLLOW_LEADER', 'AVERAGE_SPEED', 'FIGHT']
    
    # Use rule when boid is flying
    use_in_air: bool
    
    # Use rule when boid is on land
    use_on_land: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...