from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class BuildModifier(Modifier):
    
    # Total time the build effect requires
    frame_duration: float
    
    # Start frame of the effect
    frame_start: float
    
    # Seed for random if used
    seed: int
    
    # Randomize the faces or edges during build
    use_random_order: bool
    
    # Deconstruct the mesh instead of building it
    use_reverse: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...