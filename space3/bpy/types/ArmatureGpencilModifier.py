from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ArmatureGpencilModifier(GpencilModifier):
    
    # Invert vertex group influence
    invert_vertex_group: bool
    
    # Armature object to deform with
    object: Object
    
    # Bind Bone envelopes to armature modifier
    use_bone_envelopes: bool
    
    # Deform rotation interpolation with quaternions
    use_deform_preserve_volume: bool
    
    # Bind vertex groups to armature modifier
    use_vertex_groups: bool
    
    # Name of Vertex Group which determines influence of modifier per point
    vertex_group: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...