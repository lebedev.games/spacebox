from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ColorRamp(bpy_struct):
    
    # Set color mode to use for interpolation
    color_mode: str = 'RGB' # ['RGB', 'HSV', 'HSL']
    
    elements: ColorRampElements
    
    # Set color interpolation
    hue_interpolation: str = 'NEAR' # ['NEAR', 'FAR', 'CW', 'CCW']
    
    # Set interpolation between color stops
    interpolation: str = 'LINEAR' # ['EASE', 'CARDINAL', 'LINEAR', 'B_SPLINE', 'CONSTANT']

    def evaluate(self, *args, **kwargs) -> Tuple[float, float, float, float]:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...