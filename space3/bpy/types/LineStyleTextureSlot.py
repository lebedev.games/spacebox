from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class LineStyleTextureSlot(TextureSlot):
    
    # Amount texture affects alpha
    alpha_factor: float
    
    # Amount texture affects diffuse color
    diffuse_color_factor: float
    
    mapping: str = 'FLAT' # ['FLAT', 'CUBE', 'TUBE', 'SPHERE']
    
    mapping_x: str = 'X' # ['NONE', 'X', 'Y', 'Z']
    
    mapping_y: str = 'Y' # ['NONE', 'X', 'Y', 'Z']
    
    mapping_z: str = 'Z' # ['NONE', 'X', 'Y', 'Z']
    
    # Texture coordinates used to map the texture onto the background
    texture_coords: str = 'ALONG_STROKE' # ['WINDOW', 'GLOBAL', 'ALONG_STROKE', 'ORCO']
    
    # The texture affects the alpha value
    use_map_alpha: bool
    
    # The texture affects basic color of the stroke
    use_map_color_diffuse: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...