from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ArrayGpencilModifier(GpencilModifier):
    
    # Value for the distance between items
    constant_offset: Tuple[float, float, float]
    
    # Number of items
    count: int
    
    # Inverse filter
    invert_layer_pass: bool
    
    # Inverse filter
    invert_layers: bool
    
    # Inverse filter
    invert_material_pass: bool
    
    # Inverse filter
    invert_materials: bool
    
    # Layer name
    layer: str
    
    # Layer pass index
    layer_pass: int
    
    # Material used for filtering effect
    material: Material
    
    # Use the location and rotation of another object to determine the distance and rotational change between arrayed items
    offset_object: Object
    
    # Pass index
    pass_index: int
    
    # Value for changes in location
    random_offset: Tuple[float, float, float]
    
    # Value for changes in rotation
    random_rotation: Tuple[float, float, float]
    
    # Value for changes in scale
    random_scale: Tuple[float, float, float]
    
    # The size of the geometry will determine the distance between arrayed items
    relative_offset: Tuple[float, float, float]
    
    # Index of the material used for generated strokes (0 keep original material)
    replace_material: int
    
    # Random seed
    seed: int
    
    # Enable offset
    use_constant_offset: bool
    
    # Enable object offset
    use_object_offset: bool
    
    # Enable shift
    use_relative_offset: bool
    
    # Use the same random seed for each scale axis for a uniform scale
    use_uniform_random_scale: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...