from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class TimeGpencilModifier(GpencilModifier):
    
    # Final frame of the range
    frame_end: int
    
    # Evaluation time in seconds
    frame_scale: float
    
    # First frame of the range
    frame_start: int
    
    # Inverse filter
    invert_layer_pass: bool
    
    # Inverse filter
    invert_layers: bool
    
    # Layer name
    layer: str
    
    # Layer pass index
    layer_pass: int
    
    mode: str = 'NORMAL' # ['NORMAL', 'REVERSE', 'FIX']
    
    # Number of frames to offset original keyframe number or frame to fix
    offset: int
    
    # Define a custom range of frames to use in modifier
    use_custom_frame_range: bool
    
    # Retiming end frames and move to start of animation to keep loop
    use_keep_loop: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...