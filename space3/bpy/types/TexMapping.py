from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class TexMapping(bpy_struct):
    
    mapping: str = 'FLAT' # ['FLAT', 'CUBE', 'TUBE', 'SPHERE']
    
    mapping_x: str = 'NONE' # ['NONE', 'X', 'Y', 'Z']
    
    mapping_y: str = 'NONE' # ['NONE', 'X', 'Y', 'Z']
    
    mapping_z: str = 'NONE' # ['NONE', 'X', 'Y', 'Z']
    
    # Maximum value for clipping
    max: Tuple[float, float, float]
    
    # Minimum value for clipping
    min: Tuple[float, float, float]
    
    rotation: Tuple[float, float, float]
    
    scale: Tuple[float, float, float]
    
    translation: Tuple[float, float, float]
    
    # Whether to use maximum clipping value
    use_max: bool
    
    # Whether to use minimum clipping value
    use_min: bool
    
    # Type of vector that the mapping transforms
    vector_type: str = 'POINT' # ['POINT', 'TEXTURE', 'VECTOR', 'NORMAL']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...