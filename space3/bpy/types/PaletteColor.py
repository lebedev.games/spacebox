from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class PaletteColor(bpy_struct):
    
    color: Tuple[float, float, float]
    
    strength: float
    
    weight: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...