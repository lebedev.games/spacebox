from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class DynamicPaintSurfaces(bpy_struct):
    
    # Active Dynamic Paint surface being displayed
    active: DynamicPaintSurface
    
    active_index: int

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...