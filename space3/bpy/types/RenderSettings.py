from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class RenderSettings(bpy_struct):
    
    bake: BakeSettings
    
    # Bias towards faces further away from the object (in blender units)
    bake_bias: float
    
    # Extends the baked result as a post process filter
    bake_margin: int
    
    # Number of samples used for ambient occlusion baking from multires
    bake_samples: int
    
    # Choose shading information to bake into the image
    bake_type: str = 'NORMALS' # ['NORMALS', 'DISPLACEMENT']
    
    # Instead of automatically normalizing to the range 0 to 1, apply a user scale to the derivative map
    bake_user_scale: float
    
    # Maximum X value for the render region
    border_max_x: float
    
    # Maximum Y value for the render region
    border_max_y: float
    
    # Minimum X value for the render region
    border_min_x: float
    
    # Minimum Y value for the render region
    border_min_y: float
    
    # Amount of dithering noise added to the rendered image to break up banding
    dither_intensity: float
    
    # Engine to use for rendering
    engine: str = 'BLENDER_EEVEE' # ['BLENDER_EEVEE']
    
    # FFmpeg related settings for the scene
    ffmpeg: FFmpegSettings
    
    # The file extension used for saving renders
    file_extension: str
    
    # Directory/name to save animations, # characters defines the position and length of frame numbers
    filepath: str
    
    # World background is transparent, for compositing the render over another background
    film_transparent: bool
    
    # Width over which the reconstruction filter combines samples
    filter_size: float
    
    # Framerate, expressed in frames per second
    fps: int
    
    # Framerate base
    fps_base: float
    
    # How many frames the Map Old will last
    frame_map_new: int
    
    # Old mapping value in frames
    frame_map_old: int
    
    # Additional subdivision along the hair
    hair_subdiv: int
    
    # Hair shape type
    hair_type: str = 'STRAND' # ['STRAND', 'STRIP']
    
    # More than one rendering engine is available
    has_multiple_engines: bool
    
    image_settings: ImageFormatSettings
    
    # When true the format is a movie
    is_movie_format: bool
    
    # Line thickness in pixels
    line_thickness: float
    
    # Line thickness mode for Freestyle line drawing
    line_thickness_mode: str = 'ABSOLUTE' # ['ABSOLUTE', 'RELATIVE']
    
    # Where to take the metadata from
    metadata_input: str = 'SCENE' # ['SCENE', 'STRIPS']
    
    # Time taken in frames between shutter open and close
    motion_blur_shutter: float
    
    # Curve defining the shutter’s openness over time
    motion_blur_shutter_curve: CurveMapping
    
    # Horizontal aspect ratio - for anamorphic or non-square pixel output
    pixel_aspect_x: float
    
    # Vertical aspect ratio - for anamorphic or non-square pixel output
    pixel_aspect_y: float
    
    # Pixel size for viewport rendering
    preview_pixel_size: str = 'AUTO' # ['AUTO', '1', '2', '4', '8']
    
    # Resolution to start rendering preview at, progressively increasing it to the full viewport size
    preview_start_resolution: int
    
    # Percentage scale for render resolution
    resolution_percentage: int
    
    # Number of horizontal pixels in the rendered image
    resolution_x: int
    
    # Number of vertical pixels in the rendered image
    resolution_y: int
    
    # Display method used in the sequencer view
    sequencer_gl_preview: str = 'SOLID' # ['WIREFRAME', 'SOLID', 'MATERIAL', 'RENDERED']
    
    # Global child particles percentage
    simplify_child_particles: float
    
    # Global child particles percentage during rendering
    simplify_child_particles_render: float
    
    # Simplify Grease Pencil drawing
    simplify_gpencil: bool
    
    # Use Antialiasing to smooth stroke edges
    simplify_gpencil_antialiasing: bool
    
    # Display modifiers
    simplify_gpencil_modifier: bool
    
    # Simplify Grease Pencil only during animation playback
    simplify_gpencil_onplay: bool
    
    # Display Shader Effects
    simplify_gpencil_shader_fx: bool
    
    # Display layer tint
    simplify_gpencil_tint: bool
    
    # Display fill strokes in the viewport
    simplify_gpencil_view_fill: bool
    
    # Global maximum subdivision level
    simplify_subdivision: int
    
    # Global maximum subdivision level during rendering
    simplify_subdivision_render: int
    
    # Resolution percentage of volume objects in viewport
    simplify_volumes: float
    
    # Color to use behind stamp text
    stamp_background: Tuple[float, float, float, float]
    
    # Size of the font used when rendering stamp text
    stamp_font_size: int
    
    # Color to use for stamp text
    stamp_foreground: Tuple[float, float, float, float]
    
    # Custom text to appear in the stamp note
    stamp_note_text: str
    
    stereo_views: List[SceneRenderView]
    
    # Maximum number of CPU cores to use simultaneously while rendering (for multi-core/CPU systems)
    threads: int
    
    # Determine the amount of render threads used
    threads_mode: str = 'AUTO' # ['AUTO', 'FIXED']
    
    # Horizontal tile size to use while rendering
    tile_x: int
    
    # Vertical tile size to use while rendering
    tile_y: int
    
    # Clear Images before baking
    use_bake_clear: bool
    
    # Calculate heights against unsubdivided low resolution mesh
    use_bake_lores_mesh: bool
    
    # Bake directly from multires object
    use_bake_multires: bool
    
    # Bake shading on the surface of selected objects to the active object
    use_bake_selected_to_active: bool
    
    # Use a user scale for the derivative map
    use_bake_user_scale: bool
    
    # Render a user-defined render region, within the frame size
    use_border: bool
    
    # Process the render result through the compositing pipeline, if compositing nodes are enabled
    use_compositing: bool
    
    # Crop the rendered frame to the defined render region size
    use_crop_to_border: bool
    
    # Add the file format extensions to the rendered file name (eg: filename + .jpg)
    use_file_extension: bool
    
    # Draw stylized strokes using Freestyle
    use_freestyle: bool
    
    # Save for every anti-aliasing sample the entire RenderLayer results (this solves anti-aliasing issues with compositing)
    use_full_sample: bool
    
    # Use high quality tangent space at the cost of lower performance
    use_high_quality_normals: bool
    
    # Lock interface during rendering in favor of giving more memory to the renderer
    use_lock_interface: bool
    
    # Use multi-sampled 3D scene motion blur
    use_motion_blur: bool
    
    # Use multiple views in the scene
    use_multiview: bool
    
    # Overwrite existing files while rendering
    use_overwrite: bool
    
    # Keep render data around for faster re-renders and animation renders, at the cost of increased memory usage
    use_persistent_data: bool
    
    # Create empty placeholder files while rendering frames (similar to Unix ‘touch’)
    use_placeholder: bool
    
    # Save render cache to EXR files (useful for heavy compositing, Note: affects indirectly rendered scenes)
    use_render_cache: bool
    
    # Save tiles for all RenderLayers and SceneNodes to files in the temp directory (saves memory, required for Full Sample)
    use_save_buffers: bool
    
    # Process the render (and composited) result through the video sequence editor pipeline, if sequencer strips exist
    use_sequencer: bool
    
    # Use workbench render settings from the sequencer scene, instead of each individual scene used in the strip
    use_sequencer_override_scene_strip: bool
    
    # Enable simplification of scene for quicker preview renders
    use_simplify: bool
    
    # Only render the active layer. Only affects rendering from the interface, ignored for rendering from command line
    use_single_layer: bool
    
    # Active render engine supports spherical stereo rendering
    use_spherical_stereo: bool
    
    # Render the stamp info text in the rendered image
    use_stamp: bool
    
    # Include the name of the active camera in image metadata
    use_stamp_camera: bool
    
    # Include the current date in image/video metadata
    use_stamp_date: bool
    
    # Include the .blend filename in image/video metadata
    use_stamp_filename: bool
    
    # Include the frame number in image metadata
    use_stamp_frame: bool
    
    # Include the rendered frame range in image/video metadata
    use_stamp_frame_range: bool
    
    # Include the hostname of the machine that rendered the frame
    use_stamp_hostname: bool
    
    # Display stamp labels (“Camera” in front of camera name, etc.)
    use_stamp_labels: bool
    
    # Include the active camera’s lens in image metadata
    use_stamp_lens: bool
    
    # Include the name of the last marker in image metadata
    use_stamp_marker: bool
    
    # Include the peak memory usage in image metadata
    use_stamp_memory: bool
    
    # Include a custom note in image/video metadata
    use_stamp_note: bool
    
    # Include the render time in image metadata
    use_stamp_render_time: bool
    
    # Include the name of the active scene in image/video metadata
    use_stamp_scene: bool
    
    # Include the name of the foreground sequence strip in image metadata
    use_stamp_sequencer_strip: bool
    
    # Include the rendered frame timecode as HH:MM:SS.FF in image metadata
    use_stamp_time: bool
    
    views: RenderViews
    
    views_format: str = 'STEREO_3D' # ['STEREO_3D', 'MULTIVIEW']

    def frame_path(self, *args, **kwargs) -> str:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...