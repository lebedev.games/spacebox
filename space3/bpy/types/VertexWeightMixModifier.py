from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class VertexWeightMixModifier(Modifier):
    
    # Default weight a vertex will have if it is not in the first A vgroup
    default_weight_a: float
    
    # Default weight a vertex will have if it is not in the second B vgroup
    default_weight_b: float
    
    # Invert vertex group mask influence
    invert_mask_vertex_group: bool
    
    # Invert the influence of vertex group A
    invert_vertex_group_a: bool
    
    # Invert the influence of vertex group B
    invert_vertex_group_b: bool
    
    # Global influence of current modifications on vgroup
    mask_constant: float
    
    # Which bone to take texture coordinates from
    mask_tex_map_bone: str
    
    # Which object to take texture coordinates from
    mask_tex_map_object: Object
    
    # Which texture coordinates to use for mapping
    mask_tex_mapping: str = 'LOCAL' # ['LOCAL', 'GLOBAL', 'OBJECT', 'UV']
    
    # Which texture channel to use for masking
    mask_tex_use_channel: str = 'INT' # ['INT', 'RED', 'GREEN', 'BLUE', 'HUE', 'SAT', 'VAL', 'ALPHA']
    
    # UV map name
    mask_tex_uv_layer: str
    
    # Masking texture
    mask_texture: Texture
    
    # Masking vertex group name
    mask_vertex_group: str
    
    # How weights from vgroup B affect weights of vgroup A
    mix_mode: str = 'SET' # ['SET', 'ADD', 'SUB', 'MUL', 'DIV', 'DIF', 'AVG']
    
    # Which vertices should be affected
    mix_set: str = 'AND' # ['ALL', 'A', 'B', 'OR', 'AND']
    
    # Normalize the resulting weights (otherwise they are only clamped within 0.0 to 1.0 range)
    normalize: bool
    
    # First vertex group name
    vertex_group_a: str
    
    # Second vertex group name
    vertex_group_b: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...