from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class KeyMaps(bpy_struct):

    def new(self, *args, **kwargs) -> KeyMap:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    def find(self, *args, **kwargs) -> KeyMap:
        ...

    def find_modal(self, *args, **kwargs) -> KeyMap:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...