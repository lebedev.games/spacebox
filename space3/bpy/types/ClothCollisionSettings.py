from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ClothCollisionSettings(bpy_struct):
    
    # Limit colliders to this Collection
    collection: Collection
    
    # How many collision iterations should be done. (higher is better quality but slower)
    collision_quality: int
    
    # Amount of velocity lost on collision
    damping: float
    
    # Minimum distance between collision objects before collision response takes effect
    distance_min: float
    
    # Friction force if a collision happened (higher = less movement)
    friction: float
    
    # Clamp collision impulses to avoid instability (0.0 to disable clamping)
    impulse_clamp: float
    
    # Minimum distance between cloth faces before collision response takes effect
    self_distance_min: float
    
    # Friction with self contact
    self_friction: float
    
    # Clamp collision impulses to avoid instability (0.0 to disable clamping)
    self_impulse_clamp: float
    
    # Enable collisions with other objects
    use_collision: bool
    
    # Enable self collisions
    use_self_collision: bool
    
    # Triangles with all vertices in this group are not used during object collisions
    vertex_group_object_collisions: str
    
    # Triangles with all vertices in this group are not used during self collisions
    vertex_group_self_collisions: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...