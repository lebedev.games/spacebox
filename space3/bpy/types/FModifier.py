from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class FModifier(bpy_struct):
    
    # F-Curve modifier will show settings in the editor
    active: bool
    
    # Number of frames from start frame for influence to take effect
    blend_in: float
    
    # Number of frames from end frame for influence to fade out
    blend_out: float
    
    # Frame that modifier’s influence ends (if Restrict Frame Range is in use)
    frame_end: float
    
    # Frame that modifier’s influence starts (if Restrict Frame Range is in use)
    frame_start: float
    
    # Amount of influence F-Curve Modifier will have when not fading in/out
    influence: float
    
    # F-Curve Modifier has invalid settings and will not be evaluated
    is_valid: bool
    
    # Enable F-Curve modifier evaluation
    mute: bool
    
    # F-Curve Modifier’s panel is expanded in UI
    show_expanded: bool
    
    # F-Curve Modifier Type
    type: str = 'NULL' # ['NULL', 'GENERATOR', 'FNGENERATOR', 'ENVELOPE', 'CYCLES', 'NOISE', 'LIMITS', 'STEPPED']
    
    # F-Curve Modifier’s effects will be tempered by a default factor
    use_influence: bool
    
    # F-Curve Modifier is only applied for the specified frame range to help mask off effects in order to chain them
    use_restricted_range: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...