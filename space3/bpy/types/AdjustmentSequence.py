from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class AdjustmentSequence(EffectSequence):
    
    # Animation end offset (trim end)
    animation_offset_end: int
    
    # Animation start offset (trim start)
    animation_offset_start: int
    
    input_count: int

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...