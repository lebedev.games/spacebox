from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MovieTrackingMarker(bpy_struct):
    
    # Marker position at frame in normalized coordinates
    co: Tuple[float, float]
    
    # Frame number marker is keyframed on
    frame: int
    
    # Whether the position of the marker is keyframed or tracked
    is_keyed: bool
    
    # Is marker muted for current frame
    mute: bool
    
    # Pattern area bounding box in normalized coordinates
    pattern_bound_box: List[float]
    
    # Array of coordinates which represents pattern’s corners in normalized coordinates relative to marker position
    pattern_corners: List[float]
    
    # Right-bottom corner of search area in normalized coordinates relative to marker position
    search_max: Tuple[float, float]
    
    # Left-bottom corner of search area in normalized coordinates relative to marker position
    search_min: Tuple[float, float]

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...