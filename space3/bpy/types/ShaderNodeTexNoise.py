from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ShaderNodeTexNoise(ShaderNode):
    
    # Color mapping settings
    color_mapping: ColorMapping
    
    # The dimensions of the space to evaluate the noise in
    noise_dimensions: str = '1D' # ['1D', '2D', '3D', '4D']
    
    # Texture coordinate mapping settings
    texture_mapping: TexMapping

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...