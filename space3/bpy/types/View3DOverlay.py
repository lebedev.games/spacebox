from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class View3DOverlay(bpy_struct):
    
    # Opacity when rendering transparent wires
    backwire_opacity: float
    
    # Limit the display of curve handles in edit mode
    display_handle: str = 'SELECTED' # ['NONE', 'SELECTED', 'ALL']
    
    # Strength of the fade effect
    fade_inactive_alpha: float
    
    # Fade layer opacity for Grease Pencil layers except the active one
    gpencil_fade_layer: float
    
    # Fade factor
    gpencil_fade_objects: float
    
    # Canvas grid opacity
    gpencil_grid_opacity: float
    
    # Vertex Paint mix factor
    gpencil_vertex_paint_opacity: float
    
    # Number of grid lines to display in perspective view
    grid_lines: int
    
    # Multiplier for the distance between 3D View grid lines
    grid_scale: float
    
    # Grid cell size scaled by scene unit system settings
    grid_scale_unit: float
    
    # Number of subdivisions between grid lines
    grid_subdivisions: int
    
    # Display size for normals in the 3D view
    normals_length: float
    
    sculpt_mode_face_sets_opacity: float
    
    sculpt_mode_mask_opacity: float
    
    # Show annotations for this view
    show_annotation: bool
    
    # Show the X axis line
    show_axis_x: bool
    
    # Show the Y axis line
    show_axis_y: bool
    
    # Show the Z axis line
    show_axis_z: bool
    
    # Display bones (disable to show motion paths only)
    show_bones: bool
    
    # Display 3D Cursor Overlay
    show_cursor: bool
    
    # Display 3D curve normals in editmode
    show_curve_normals: bool
    
    # Display weights created for the Bevel modifier
    show_edge_bevel_weight: bool
    
    # Display creases created for Subdivision Surface modifier
    show_edge_crease: bool
    
    # Display UV unwrapping seams
    show_edge_seams: bool
    
    # Display sharp edges, used with the Edge Split modifier
    show_edge_sharp: bool
    
    # Highlight selected edges
    show_edges: bool
    
    # Display selected edge angle, using global values when set in the transform panel
    show_extra_edge_angle: bool
    
    # Display selected edge lengths, using global values when set in the transform panel
    show_extra_edge_length: bool
    
    # Display the angles in the selected edges, using global values when set in the transform panel
    show_extra_face_angle: bool
    
    # Display the area of selected faces, using global values when set in the transform panel
    show_extra_face_area: bool
    
    # Display the index numbers of selected vertices, edges, and faces
    show_extra_indices: bool
    
    # Object details, including empty wire, cameras and other visual guides
    show_extras: bool
    
    # Display face center when face selection is enabled in solid shading modes
    show_face_center: bool
    
    # Display face normals as lines
    show_face_normals: bool
    
    # Show the Face Orientation Overlay
    show_face_orientation: bool
    
    # Highlight selected faces
    show_faces: bool
    
    # Fade inactive geometry using the viewport background color
    show_fade_inactive: bool
    
    # Show the ground plane grid
    show_floor: bool
    
    # Display Freestyle edge marks, used with the Freestyle renderer
    show_freestyle_edge_marks: bool
    
    # Display Freestyle face marks, used with the Freestyle renderer
    show_freestyle_face_marks: bool
    
    # Show HDRI preview spheres
    show_look_dev: bool
    
    # Show the Motion Paths Overlay
    show_motion_paths: bool
    
    # Show object center dots
    show_object_origins: bool
    
    # Show the object origin center dot for all (selected and unselected) objects
    show_object_origins_all: bool
    
    # Use hidden wireframe display
    show_occlude_wire: bool
    
    # Show the Onion Skinning Overlay
    show_onion_skins: bool
    
    # Show grid in orthographic side view
    show_ortho_grid: bool
    
    # Show an outline highlight around selected objects
    show_outline_selected: bool
    
    # Display overlays like gizmos and outlines
    show_overlays: bool
    
    # Use wireframe display in painting modes
    show_paint_wire: bool
    
    # Show dashed lines indicating parent or constraint relationships
    show_relationship_lines: bool
    
    # Display vertex-per-face normals as lines
    show_split_normals: bool
    
    # Display scene statistics overlay text
    show_stats: bool
    
    # Display statistical information about the mesh
    show_statvis: bool
    
    # Display overlay text
    show_text: bool
    
    # Display vertex normals as lines
    show_vertex_normals: bool
    
    # Display weights in editmode
    show_weight: bool
    
    # Show face edges wires
    show_wireframes: bool
    
    # Show contour lines formed by points with the same interpolated weight
    show_wpaint_contours: bool
    
    # Show the bone selection overlay
    show_xray_bone: bool
    
    # Opacity of the texture paint mode stencil mask overlay
    texture_paint_mode_opacity: float
    
    # Show Canvas grid in front
    use_gpencil_canvas_xray: bool
    
    # Show Edit Lines when editing strokes
    use_gpencil_edit_lines: bool
    
    # Fade Grease Pencil Objects, except the active one
    use_gpencil_fade_gp_objects: bool
    
    # Toggle fading of Grease Pencil layers except the active one
    use_gpencil_fade_layers: bool
    
    # Fade all viewport objects with a full color layer to improve visibility
    use_gpencil_fade_objects: bool
    
    # Display a grid over grease pencil paper
    use_gpencil_grid: bool
    
    # Show Edit Lines only in multiframe
    use_gpencil_multiedit_line_only: bool
    
    # Show ghosts of the keyframes before and after the current frame
    use_gpencil_onion_skin: bool
    
    # Show stroke drawing direction with a bigger green dot (start) and smaller red dot (end) points
    use_gpencil_show_directions: bool
    
    # Show material name assigned to each stroke
    use_gpencil_show_material_name: bool
    
    # Opacity for edit vertices
    vertex_opacity: float
    
    # Opacity of the texture paint mode stencil mask overlay
    vertex_paint_mode_opacity: float
    
    # Opacity of the weight paint mode overlay
    weight_paint_mode_opacity: float
    
    # Opacity of the displayed edges (1.0 for opaque)
    wireframe_opacity: float
    
    # Adjust the angle threshold for displaying edges (1.0 for all)
    wireframe_threshold: float
    
    # Opacity to use for bone selection
    xray_alpha_bone: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...