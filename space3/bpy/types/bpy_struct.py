from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class bpy_struct(object):
    
    # The <a class="reference internal" href="bpy.types.ID.html#bpy.types.ID" title="bpy.types.ID"><code class="xref py py-class docutils literal notranslate"><span class="pre">bpy.types.ID</span></code></a> object this datablock is from or None, (not available for all data types)
    id_data: Any

    def as_pointer(self, *args, **kwargs) -> None:
        ...

    def driver_add(self, *args, **kwargs) -> FCurve:
        ...

    def driver_remove(self, *args, **kwargs) -> bool:
        ...

    def get(self, *args, **kwargs) -> None:
        ...

    def is_property_hidden(self, *args, **kwargs) -> None:
        ...

    def is_property_overridable_library(self, *args, **kwargs) -> None:
        ...

    def is_property_readonly(self, *args, **kwargs) -> None:
        ...

    def is_property_set(self, *args, **kwargs) -> bool:
        ...

    def items(self, *args, **kwargs) -> None:
        ...

    def keyframe_delete(self, *args, **kwargs) -> bool:
        ...

    def keyframe_insert(self, *args, **kwargs) -> bool:
        ...

    def keys(self, *args, **kwargs) -> None:
        ...

    def path_from_id(self, *args, **kwargs) -> Any:
        ...

    def path_resolve(self, *args, **kwargs) -> None:
        ...

    def pop(self, *args, **kwargs) -> None:
        ...

    def property_overridable_library_set(self, *args, **kwargs) -> None:
        ...

    def property_unset(self, *args, **kwargs) -> None:
        ...

    def type_recast(self, *args, **kwargs) -> None:
        ...

    def values(self, *args, **kwargs) -> None:
        ...