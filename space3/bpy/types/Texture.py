from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Texture(ID):
    
    # Animation data for this data-block
    animation_data: AnimData
    
    color_ramp: ColorRamp
    
    # Adjust the contrast of the texture
    contrast: float
    
    factor_blue: float
    
    factor_green: float
    
    factor_red: float
    
    # Adjust the brightness of the texture
    intensity: float
    
    # Node tree for node-based textures
    node_tree: NodeTree
    
    # Adjust the saturation of colors in the texture
    saturation: float
    
    type: str = 'IMAGE' # ['NONE', 'BLEND', 'CLOUDS', 'DISTORTED_NOISE', 'IMAGE', 'MAGIC', 'MARBLE', 'MUSGRAVE', 'NOISE', 'STUCCI', 'VORONOI', 'WOOD']
    
    # Set negative texture RGB and intensity values to zero, for some uses like displacement this option can be disabled to get the full range
    use_clamp: bool
    
    # Map the texture intensity to the color ramp. Note that the alpha value is used for image textures, enable “Calculate Alpha” for images without an alpha channel
    use_color_ramp: bool
    
    # Make this a node-based texture
    use_nodes: bool
    
    # Show Alpha in Preview Render
    use_preview_alpha: bool
    
    # Materials that use this texture
    users_material: Any
    
    # Object modifiers that use this texture
    users_object_modifier: Any

    def evaluate(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...