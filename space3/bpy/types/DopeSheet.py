from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class DopeSheet(bpy_struct):
    
    # Collection that included object should be a member of
    filter_collection: Collection
    
    # F-Curve live filtering string
    filter_fcurve_name: str
    
    # Live filtering string
    filter_text: str
    
    # Include visualization of armature related animation data
    show_armatures: bool
    
    # Include visualization of cache file related animation data
    show_cache_files: bool
    
    # Include visualization of camera related animation data
    show_cameras: bool
    
    # Include visualization of curve related animation data
    show_curves: bool
    
    # Show options for whether channels related to certain types of data are included
    show_datablock_filters: bool
    
    # Collapse summary when shown, so all other channels get hidden (Dope Sheet editors only)
    show_expanded_summary: bool
    
    # Include visualization of Grease Pencil related animation data and frames
    show_gpencil: bool
    
    # Include visualization of hair related animation data
    show_hairs: bool
    
    # Include channels from objects/bone that are not visible
    show_hidden: bool
    
    # Include visualization of lattice related animation data
    show_lattices: bool
    
    # Include visualization of light related animation data
    show_lights: bool
    
    # Include visualization of Line Style related Animation data
    show_linestyles: bool
    
    # Include visualization of material related animation data
    show_materials: bool
    
    # Include visualization of mesh related animation data
    show_meshes: bool
    
    # Include visualization of metaball related animation data
    show_metaballs: bool
    
    # Include animation data-blocks with no NLA data (NLA editor only)
    show_missing_nla: bool
    
    # Include visualization of animation data related to data-blocks linked to modifiers
    show_modifiers: bool
    
    # Include visualization of movie clip related animation data
    show_movieclips: bool
    
    # Include visualization of node related animation data
    show_nodes: bool
    
    # Only include F-Curves and drivers that are disabled or have errors
    show_only_errors: bool
    
    # Only include channels relating to selected objects and data
    show_only_selected: bool
    
    # Include visualization of particle related animation data
    show_particles: bool
    
    # Include visualization of point cloud related animation data
    show_pointclouds: bool
    
    # Include visualization of scene related animation data
    show_scenes: bool
    
    # Include visualization of shape key related animation data
    show_shapekeys: bool
    
    # Include visualization of speaker related animation data
    show_speakers: bool
    
    # Display an additional ‘summary’ line (Dope Sheet editors only)
    show_summary: bool
    
    # Include visualization of texture related animation data
    show_textures: bool
    
    # Include visualization of object-level animation data (mostly transforms)
    show_transforms: bool
    
    # Include visualization of volume related animation data
    show_volumes: bool
    
    # Include visualization of world related animation data
    show_worlds: bool
    
    # ID-Block representing source data, usually ID_SCE (i.e. Scene)
    source: ID
    
    # Alphabetically sorts data-blocks - mainly objects in the scene (disable to increase viewport speed)
    use_datablock_sort: bool
    
    # Invert filter search
    use_filter_invert: bool
    
    use_multi_word_filter: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...