from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class WorkSpaceTool(bpy_struct):
    
    has_datablock: bool
    
    idname: str
    
    idname_fallback: str
    
    index: int
    
    mode: str = 'DEFAULT' # ['DEFAULT']
    
    space_type: str = 'EMPTY' # ['EMPTY', 'VIEW_3D', 'IMAGE_EDITOR', 'NODE_EDITOR', 'SEQUENCE_EDITOR', 'CLIP_EDITOR', 'DOPESHEET_EDITOR', 'GRAPH_EDITOR', 'NLA_EDITOR', 'TEXT_EDITOR', 'CONSOLE', 'INFO', 'TOPBAR', 'STATUSBAR', 'OUTLINER', 'PROPERTIES', 'FILE_BROWSER', 'SPREADSHEET', 'PREFERENCES']
    
    widget: str

    def setup(self, *args, **kwargs) -> None:
        ...

    def operator_properties(self, *args, **kwargs) -> OperatorProperties:
        ...

    def gizmo_group_properties(self, *args, **kwargs) -> GizmoGroupProperties:
        ...

    def refresh_from_context(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...