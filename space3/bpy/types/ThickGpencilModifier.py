from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ThickGpencilModifier(GpencilModifier):
    
    # Custom curve to apply effect
    curve: CurveMapping
    
    # Inverse filter
    invert_layer_pass: bool
    
    # Inverse filter
    invert_layers: bool
    
    # Inverse filter
    invert_material_pass: bool
    
    # Inverse filter
    invert_materials: bool
    
    # Inverse filter
    invert_vertex: bool
    
    # Layer name
    layer: str
    
    # Layer pass index
    layer_pass: int
    
    # Material used for filtering effect
    material: Material
    
    # Replace the stroke thickness
    normalize_thickness: bool
    
    # Pass index
    pass_index: int
    
    # Absolute thickness to apply everywhere
    thickness: int
    
    # Factor to multiply the thickness with
    thickness_factor: float
    
    # Use a custom curve to define thickness change along the strokes
    use_custom_curve: bool
    
    # Vertex group name for modulating the deform
    vertex_group: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...