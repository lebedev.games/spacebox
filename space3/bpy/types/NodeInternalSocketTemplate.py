from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class NodeInternalSocketTemplate(bpy_struct):
    
    # Identifier of the socket
    identifier: str
    
    # Name of the socket
    name: str
    
    # Data type of the socket
    type: str = 'VALUE' # ['CUSTOM', 'VALUE', 'INT', 'BOOLEAN', 'VECTOR', 'STRING', 'RGBA', 'SHADER', 'OBJECT', 'IMAGE', 'GEOMETRY', 'COLLECTION']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...