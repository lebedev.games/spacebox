from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class NlaTrack(bpy_struct):
    
    # NLA Track is active
    active: bool
    
    # NLA Track is evaluated itself (i.e. active Action and all other NLA Tracks in the same AnimData block are disabled)
    is_solo: bool
    
    # NLA Track is locked
    lock: bool
    
    # Disable NLA Track evaluation
    mute: bool
    
    name: str
    
    # NLA Track is selected
    select: bool
    
    # NLA Strips on this NLA-track
    strips: NlaStrips

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...