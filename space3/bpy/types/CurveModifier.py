from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CurveModifier(Modifier):
    
    # The axis that the curve deforms along
    deform_axis: str = 'POS_X' # ['POS_X', 'POS_Y', 'POS_Z', 'NEG_X', 'NEG_Y', 'NEG_Z']
    
    # Invert vertex group influence
    invert_vertex_group: bool
    
    # Curve object to deform with
    object: Object
    
    # Name of Vertex Group which determines influence of modifier per point
    vertex_group: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...