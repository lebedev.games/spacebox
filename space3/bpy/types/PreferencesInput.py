from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class PreferencesInput(bpy_struct):
    
    # Number of pixels to drag before a drag event is triggered for keyboard and other non mouse/tablet input (otherwise click events are detected)
    drag_threshold: int
    
    # Number of pixels to drag before a tweak/drag event is triggered for mouse/trackpad input (otherwise click events are detected)
    drag_threshold_mouse: int
    
    # Number of pixels to drag before a tweak/drag event is triggered for tablet input (otherwise click events are detected)
    drag_threshold_tablet: int
    
    # Invert the axis of mouse movement for zooming
    invert_mouse_zoom: bool
    
    # Swap the Mouse Wheel zoom direction
    invert_zoom_wheel: bool
    
    # Time/delay (in ms) for a double click
    mouse_double_click_time: int
    
    # Hold this modifier to emulate the middle mouse button
    mouse_emulate_3_button_modifier: str = 'ALT' # ['ALT', 'OSKEY']
    
    # Number of pixels to before the cursor is considered to have moved (used for cycling selected items on successive clicks)
    move_threshold: int
    
    # Which method to use for viewport navigation
    navigation_mode: str = 'WALK' # ['WALK', 'FLY']
    
    # Threshold of initial movement needed from the device’s rest position
    ndof_deadzone: float
    
    # Device up/down directly controls the Z position of the 3D viewport
    ndof_fly_helicopter: bool
    
    # Keep horizon level while flying with 3D Mouse
    ndof_lock_horizon: bool
    
    # Overall sensitivity of the 3D Mouse for orbiting
    ndof_orbit_sensitivity: float
    
    # Pan using up/down on the device (otherwise forward/backward)
    ndof_pan_yz_swap_axis: bool
    
    ndof_panx_invert_axis: bool
    
    ndof_pany_invert_axis: bool
    
    ndof_panz_invert_axis: bool
    
    ndof_rotx_invert_axis: bool
    
    ndof_roty_invert_axis: bool
    
    ndof_rotz_invert_axis: bool
    
    # Overall sensitivity of the 3D Mouse for panning
    ndof_sensitivity: float
    
    # Display the center and axis during rotation
    ndof_show_guide: bool
    
    # Navigation style in the viewport
    ndof_view_navigate_method: str = 'FREE' # ['FREE', 'ORBIT']
    
    # Rotation style in the viewport
    ndof_view_rotate_method: str = 'TRACKBALL' # ['TURNTABLE', 'TRACKBALL']
    
    # Zoom using opposite direction
    ndof_zoom_invert: bool
    
    # Adjusts softness of the low pressure response onset using a gamma curve
    pressure_softness: float
    
    # Raw input pressure value that is interpreted as 100% by Blender
    pressure_threshold_max: float
    
    # Select the tablet API to use for pressure sensitivity (may require restarting Blender for changes to take effect)
    tablet_api: str = 'AUTOMATIC' # ['AUTOMATIC', 'WINDOWS_INK', 'WINTAB']
    
    # Automatically switch between orthographic and perspective when changing from top/front/side views
    use_auto_perspective: bool
    
    # Moving things with a mouse drag confirms when releasing the button
    use_drag_immediately: bool
    
    # Main 1 to 0 keys act as the numpad ones (useful for laptops)
    use_emulate_numpad: bool
    
    # Let the mouse wrap around the view boundaries so mouse movements are not limited by the screen size (used by transform, dragging of UI controls, etc.)
    use_mouse_continuous: bool
    
    # Use the depth under the mouse to improve view pan/rotate/zoom functionality
    use_mouse_depth_navigate: bool
    
    # Emulate Middle Mouse with Alt+Left Mouse
    use_mouse_emulate_3_button: bool
    
    use_ndof: bool
    
    # When entering numbers while transforming, default to advanced mode for full math expression evaluation
    use_numeric_input_advanced: bool
    
    # Use selection as the pivot point
    use_rotate_around_active: bool
    
    # Zoom in towards the mouse pointer’s position in the 3D view, rather than the 2D window center
    use_zoom_to_mouse: bool
    
    # Orbit method in the viewport
    view_rotate_method: str = 'TURNTABLE' # ['TURNTABLE', 'TRACKBALL']
    
    # Scale trackball orbit sensitivity
    view_rotate_sensitivity_trackball: float
    
    # Rotation amount per pixel to control how fast the viewport orbits
    view_rotate_sensitivity_turntable: float
    
    # Axis of mouse movement to zoom in or out on
    view_zoom_axis: str = 'VERTICAL' # ['VERTICAL', 'HORIZONTAL']
    
    # Which style to use for viewport scaling
    view_zoom_method: str = 'DOLLY' # ['CONTINUE', 'DOLLY', 'SCALE']
    
    # Settings for walk navigation mode
    walk_navigation: WalkNavigation

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...