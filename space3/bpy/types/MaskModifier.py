from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MaskModifier(Modifier):
    
    # Armature to use as source of bones to mask
    armature: Object
    
    # Use vertices that are not part of region defined
    invert_vertex_group: bool
    
    mode: str = 'VERTEX_GROUP' # ['VERTEX_GROUP', 'ARMATURE']
    
    # Weights over this threshold remain
    threshold: float
    
    # Vertex group name
    vertex_group: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...