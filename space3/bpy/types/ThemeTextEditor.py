from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ThemeTextEditor(bpy_struct):
    
    cursor: Tuple[float, float, float]
    
    line_numbers: Tuple[float, float, float]
    
    line_numbers_background: Tuple[float, float, float]
    
    selected_text: Tuple[float, float, float]
    
    # Settings for space
    space: ThemeSpaceGeneric
    
    syntax_builtin: Tuple[float, float, float]
    
    syntax_comment: Tuple[float, float, float]
    
    syntax_numbers: Tuple[float, float, float]
    
    syntax_preprocessor: Tuple[float, float, float]
    
    syntax_reserved: Tuple[float, float, float]
    
    syntax_special: Tuple[float, float, float]
    
    syntax_string: Tuple[float, float, float]
    
    syntax_symbols: Tuple[float, float, float]

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...