from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CompositorNodeViewer(CompositorNode):
    
    center_x: float
    
    center_y: float
    
    # Tile order
    tile_order: str = 'CENTEROUT' # ['CENTEROUT', 'RANDOM', 'BOTTOMUP', 'RULE_OF_THIRDS']
    
    # Colors are treated alpha premultiplied, or colors output straight (alpha gets set to 1)
    use_alpha: bool

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    def update(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...