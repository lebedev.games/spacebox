from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CameraStereoData(bpy_struct):
    
    # The converge point for the stereo cameras (often the distance between a projector and the projection screen)
    convergence_distance: float
    
    convergence_mode: str = 'OFFAXIS' # ['OFFAXIS', 'PARALLEL', 'TOE']
    
    # Set the distance between the eyes - the stereo plane distance / 30 should be fine
    interocular_distance: float
    
    pivot: str = 'LEFT' # ['LEFT', 'RIGHT', 'CENTER']
    
    # Angle at which interocular distance starts to fade to 0
    pole_merge_angle_from: float
    
    # Angle at which interocular distance is 0
    pole_merge_angle_to: float
    
    # Fade interocular distance to 0 after the given cutoff angle
    use_pole_merge: bool
    
    # Render every pixel rotating the camera around the middle of the interocular distance
    use_spherical_stereo: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...