from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class PointCache(bpy_struct):
    
    # Compression method to be used
    compression: str = 'NO' # ['NO', 'LIGHT', 'HEAVY']
    
    # Cache file path
    filepath: str
    
    # Frame on which the simulation stops
    frame_end: int
    
    # Frame on which the simulation starts
    frame_start: int
    
    # Number of frames between cached frames
    frame_step: int
    
    # Index number of cache files
    index: int
    
    # Info on current cache status
    info: str
    
    is_baked: bool
    
    is_baking: bool
    
    is_frame_skip: bool
    
    is_outdated: bool
    
    # Cache name
    name: str
    
    point_caches: PointCaches
    
    # Save cache files to disk (.blend file must be saved first)
    use_disk_cache: bool
    
    # Read cache from an external location
    use_external: bool
    
    # Use this file’s path for the disk cache when library linked into another file (for local bakes per scene file, disable this option)
    use_library_path: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...