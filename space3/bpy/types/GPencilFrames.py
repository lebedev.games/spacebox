from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class GPencilFrames(bpy_struct):

    def new(self, *args, **kwargs) -> GPencilFrame:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    def copy(self, *args, **kwargs) -> GPencilFrame:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...