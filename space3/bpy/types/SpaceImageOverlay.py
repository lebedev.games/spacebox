from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SpaceImageOverlay(bpy_struct):
    
    # Display overlays like UV Maps and Metadata
    show_overlays: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...