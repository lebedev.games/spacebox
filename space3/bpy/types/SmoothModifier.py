from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SmoothModifier(Modifier):
    
    # Strength of modifier effect
    factor: float
    
    # Invert vertex group influence
    invert_vertex_group: bool
    
    iterations: int
    
    # Smooth object along X axis
    use_x: bool
    
    # Smooth object along Y axis
    use_y: bool
    
    # Smooth object along Z axis
    use_z: bool
    
    # Name of Vertex Group which determines influence of modifier per point
    vertex_group: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...