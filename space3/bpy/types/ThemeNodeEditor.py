from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ThemeNodeEditor(bpy_struct):
    
    attribute_node: Tuple[float, float, float]
    
    color_node: Tuple[float, float, float]
    
    converter_node: Tuple[float, float, float]
    
    distor_node: Tuple[float, float, float]
    
    filter_node: Tuple[float, float, float]
    
    frame_node: Tuple[float, float, float, float]
    
    geometry_node: Tuple[float, float, float]
    
    grid: Tuple[float, float, float]
    
    # Amount of grid lines displayed in the background
    grid_levels: int
    
    group_node: Tuple[float, float, float, float]
    
    group_socket_node: Tuple[float, float, float]
    
    input_node: Tuple[float, float, float]
    
    layout_node: Tuple[float, float, float]
    
    matte_node: Tuple[float, float, float]
    
    node_active: Tuple[float, float, float]
    
    node_backdrop: Tuple[float, float, float, float]
    
    node_selected: Tuple[float, float, float]
    
    # Curving of the noodle
    noodle_curving: int
    
    output_node: Tuple[float, float, float]
    
    pattern_node: Tuple[float, float, float]
    
    script_node: Tuple[float, float, float]
    
    selected_text: Tuple[float, float, float]
    
    shader_node: Tuple[float, float, float]
    
    # Settings for space
    space: ThemeSpaceGeneric
    
    # Settings for space list
    space_list: ThemeSpaceListGeneric
    
    texture_node: Tuple[float, float, float]
    
    vector_node: Tuple[float, float, float]
    
    wire: Tuple[float, float, float]
    
    wire_inner: Tuple[float, float, float]
    
    wire_select: Tuple[float, float, float]

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...