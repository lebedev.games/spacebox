from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ShrinkwrapModifier(Modifier):
    
    # Additional mesh target to shrink to
    auxiliary_target: Object
    
    # Stop vertices from projecting to a face on the target when facing towards/away
    cull_face: str = 'OFF' # ['OFF', 'FRONT', 'BACK']
    
    # Invert vertex group influence
    invert_vertex_group: bool
    
    # Distance to keep from the target
    offset: float
    
    # Limit the distance used for projection (zero disables)
    project_limit: float
    
    # Number of subdivisions that must be performed before extracting vertices’ positions and normals
    subsurf_levels: int
    
    # Mesh target to shrink to
    target: Object
    
    # When projecting in the negative direction invert the face cull mode
    use_invert_cull: bool
    
    # Allow vertices to move in the negative direction of axis
    use_negative_direction: bool
    
    # Allow vertices to move in the positive direction of axis
    use_positive_direction: bool
    
    use_project_x: bool
    
    use_project_y: bool
    
    use_project_z: bool
    
    # Vertex group name
    vertex_group: str
    
    wrap_method: str = 'NEAREST_SURFACEPOINT' # ['NEAREST_SURFACEPOINT', 'PROJECT', 'NEAREST_VERTEX', 'TARGET_PROJECT']
    
    # Select how vertices are constrained to the target surface
    wrap_mode: str = 'ON_SURFACE' # ['ON_SURFACE', 'INSIDE', 'OUTSIDE', 'OUTSIDE_SURFACE', 'ABOVE_SURFACE']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...