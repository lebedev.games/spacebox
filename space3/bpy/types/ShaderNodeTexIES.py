from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ShaderNodeTexIES(ShaderNode):
    
    # IES light path
    filepath: str
    
    # Internal IES file
    ies: Text
    
    # Whether the IES file is loaded from disk or from a text data-block
    mode: str = 'INTERNAL' # ['INTERNAL', 'EXTERNAL']

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...