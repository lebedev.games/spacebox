from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class BrushGpencilSettings(bpy_struct):
    
    # Amount of smoothing while drawing
    active_smooth_factor: float
    
    # Direction of the stroke at which brush gives maximal thickness (0° for horizontal)
    angle: float
    
    # Reduce brush thickness by this factor when stroke is perpendicular to ‘Angle’ direction
    angle_factor: float
    
    aspect: Tuple[float, float]
    
    # Preselected mode when using this brush
    brush_draw_mode: str = 'ACTIVE' # ['ACTIVE', 'MATERIAL', 'VERTEXCOLOR']
    
    # Curve used for the jitter effect
    curve_jitter: CurveMapping
    
    # Curve used for modulating effect
    curve_random_hue: CurveMapping
    
    # Curve used for modulating effect
    curve_random_pressure: CurveMapping
    
    # Curve used for modulating effect
    curve_random_saturation: CurveMapping
    
    # Curve used for modulating effect
    curve_random_strength: CurveMapping
    
    # Curve used for modulating effect
    curve_random_uv: CurveMapping
    
    # Curve used for modulating effect
    curve_random_value: CurveMapping
    
    # Curve used for the sensitivity
    curve_sensitivity: CurveMapping
    
    # Curve used for the strength
    curve_strength: CurveMapping
    
    direction: str = 'ADD' # ['ADD', 'SUBTRACT']
    
    # Eraser Mode
    eraser_mode: str = 'SOFT' # ['SOFT', 'HARD', 'STROKE']
    
    # Amount of erasing for strength
    eraser_strength_factor: float
    
    # Amount of erasing for thickness
    eraser_thickness_factor: float
    
    # Strokes end extension for closing gaps, use zero to disable
    extend_stroke_factor: float
    
    # Direction of the fill
    fill_direction: str = 'NORMAL' # ['NORMAL', 'INVERT']
    
    # Mode to draw boundary limits
    fill_draw_mode: str = 'BOTH' # ['BOTH', 'STROKE', 'CONTROL']
    
    # Factor for fill boundary accuracy, higher values are more accurate but slower
    fill_factor: float
    
    # Layers used as boundaries
    fill_layer_mode: str = 'VISIBLE' # ['VISIBLE', 'ACTIVE', 'ABOVE', 'BELOW', 'ALL_ABOVE', 'ALL_BELOW']
    
    # Size in pixels to consider the leak closed
    fill_leak: int
    
    # Number of simplify steps (large values reduce fill accuracy)
    fill_simplify_level: int
    
    # Threshold to consider color transparent for filling
    fill_threshold: float
    
    gpencil_paint_icon: str = 'PENCIL' # ['PENCIL', 'PEN', 'INK', 'INKNOISE', 'BLOCK', 'MARKER', 'AIRBRUSH', 'CHISEL', 'FILL', 'SOFT', 'HARD', 'STROKE']
    
    gpencil_sculpt_icon: str = 'SMOOTH' # ['SMOOTH', 'THICKNESS', 'STRENGTH', 'RANDOMIZE', 'GRAB', 'PUSH', 'TWIST', 'PINCH', 'CLONE']
    
    gpencil_vertex_icon: str = 'DRAW' # ['DRAW', 'BLUR', 'AVERAGE', 'SMEAR', 'REPLACE']
    
    gpencil_weight_icon: str = 'DRAW' # ['DRAW']
    
    # Gradient from the center of Dot and Box strokes (set to 1 for a solid stroke)
    hardness: float
    
    # Generate intermediate points for very fast mouse movements. Set to 0 to disable
    input_samples: int
    
    # Material used for strokes drawn using this brush
    material: Material
    
    # Jitter factor for new strokes
    pen_jitter: float
    
    # Amount of smoothing to apply after finish newly created strokes, to reduce jitter/noise
    pen_smooth_factor: float
    
    # Number of times to smooth newly created strokes
    pen_smooth_steps: int
    
    # Color strength for new strokes (affect alpha factor of color)
    pen_strength: float
    
    # Number of times to subdivide newly created strokes, for less jagged strokes
    pen_subdivision_steps: int
    
    # Pin the mode to the brush
    pin_draw_mode: bool
    
    # Random factor to modify original hue
    random_hue_factor: float
    
    # Randomness factor for pressure in new strokes
    random_pressure: float
    
    # Random factor to modify original saturation
    random_saturation_factor: float
    
    # Randomness factor strength in new strokes
    random_strength: float
    
    # Random factor to modify original value
    random_value_factor: float
    
    # Show transparent lines to use as boundary for filling
    show_fill: bool
    
    # Show help lines for filling to see boundaries
    show_fill_boundary: bool
    
    # Show help lines for stroke extension
    show_fill_extend: bool
    
    # Do not display fill color while drawing the stroke
    show_lasso: bool
    
    # Factor of Simplify using adaptive algorithm
    simplify_factor: float
    
    # Use this brush when enable eraser with fast switch key
    use_default_eraser: bool
    
    # The brush affects the position of the point
    use_edit_position: bool
    
    # Affect pressure values as well when smoothing strokes
    use_edit_pressure: bool
    
    # The brush affects the color strength of the point
    use_edit_strength: bool
    
    # The brush affects the thickness of the point
    use_edit_thickness: bool
    
    # The brush affects the UV rotation of the point
    use_edit_uv: bool
    
    # Fill only visible areas in viewport
    use_fill_limit: bool
    
    # Use tablet pressure for jitter
    use_jitter_pressure: bool
    
    # Keep material assigned to brush
    use_material_pin: bool
    
    # Erase only strokes visible and not occluded
    use_occlude_eraser: bool
    
    # Use tablet pressure
    use_pressure: bool
    
    # Use pressure to modulate randomness
    use_random_press_hue: bool
    
    # Use pressure to modulate randomness
    use_random_press_radius: bool
    
    # Use pressure to modulate randomness
    use_random_press_sat: bool
    
    # Use pressure to modulate randomness
    use_random_press_strength: bool
    
    # Use pressure to modulate randomness
    use_random_press_uv: bool
    
    # Use pressure to modulate randomness
    use_random_press_val: bool
    
    # Additional post processing options for new strokes
    use_settings_postprocess: bool
    
    # Random brush settings
    use_settings_random: bool
    
    # Draw lines with a delay to allow smooth strokes. Press Shift key to override while drawing
    use_settings_stabilizer: bool
    
    # Use tablet pressure for color strength
    use_strength_pressure: bool
    
    # Use randomness at stroke level
    use_stroke_random_hue: bool
    
    # Use randomness at stroke level
    use_stroke_random_radius: bool
    
    # Use randomness at stroke level
    use_stroke_random_sat: bool
    
    # Use randomness at stroke level
    use_stroke_random_strength: bool
    
    # Use randomness at stroke level
    use_stroke_random_uv: bool
    
    # Use randomness at stroke level
    use_stroke_random_val: bool
    
    # Trim intersecting stroke ends
    use_trim: bool
    
    # Random factor for autogenerated UV rotation
    uv_random: float
    
    # Factor used to mix vertex color to get final color
    vertex_color_factor: float
    
    # Defines how vertex color affect to the strokes
    vertex_mode: str = 'STROKE' # ['STROKE', 'FILL', 'BOTH']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...