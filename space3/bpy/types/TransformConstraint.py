from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class TransformConstraint(Constraint):
    
    # Top range of X axis source motion
    from_max_x: float
    
    # Top range of X axis source motion
    from_max_x_rot: float
    
    # Top range of X axis source motion
    from_max_x_scale: float
    
    # Top range of Y axis source motion
    from_max_y: float
    
    # Top range of Y axis source motion
    from_max_y_rot: float
    
    # Top range of Y axis source motion
    from_max_y_scale: float
    
    # Top range of Z axis source motion
    from_max_z: float
    
    # Top range of Z axis source motion
    from_max_z_rot: float
    
    # Top range of Z axis source motion
    from_max_z_scale: float
    
    # Bottom range of X axis source motion
    from_min_x: float
    
    # Bottom range of X axis source motion
    from_min_x_rot: float
    
    # Bottom range of X axis source motion
    from_min_x_scale: float
    
    # Bottom range of Y axis source motion
    from_min_y: float
    
    # Bottom range of Y axis source motion
    from_min_y_rot: float
    
    # Bottom range of Y axis source motion
    from_min_y_scale: float
    
    # Bottom range of Z axis source motion
    from_min_z: float
    
    # Bottom range of Z axis source motion
    from_min_z_rot: float
    
    # Bottom range of Z axis source motion
    from_min_z_scale: float
    
    # Specify the type of rotation channels to use
    from_rotation_mode: str = 'AUTO' # ['AUTO', 'XYZ', 'XZY', 'YXZ', 'YZX', 'ZXY', 'ZYX', 'QUATERNION', 'SWING_TWIST_X', 'SWING_TWIST_Y', 'SWING_TWIST_Z']
    
    # The transformation type to use from the target
    map_from: str = 'LOCATION' # ['LOCATION', 'ROTATION', 'SCALE']
    
    # The transformation type to affect of the constrained object
    map_to: str = 'LOCATION' # ['LOCATION', 'ROTATION', 'SCALE']
    
    # The source axis constrained object’s X axis uses
    map_to_x_from: str = 'X' # ['X', 'Y', 'Z']
    
    # The source axis constrained object’s Y axis uses
    map_to_y_from: str = 'X' # ['X', 'Y', 'Z']
    
    # The source axis constrained object’s Z axis uses
    map_to_z_from: str = 'X' # ['X', 'Y', 'Z']
    
    # Specify how to combine the new location with original
    mix_mode: str = 'ADD' # ['REPLACE', 'ADD']
    
    # Specify how to combine the new rotation with original
    mix_mode_rot: str = 'ADD' # ['REPLACE', 'ADD', 'BEFORE', 'AFTER']
    
    # Specify how to combine the new scale with original
    mix_mode_scale: str = 'REPLACE' # ['REPLACE', 'MULTIPLY']
    
    # Armature bone, mesh or lattice vertex group, …
    subtarget: str
    
    # Target object
    target: Object
    
    # Explicitly specify the output euler rotation order
    to_euler_order: str = 'AUTO' # ['AUTO', 'XYZ', 'XZY', 'YXZ', 'YZX', 'ZXY', 'ZYX']
    
    # Top range of X axis destination motion
    to_max_x: float
    
    # Top range of X axis destination motion
    to_max_x_rot: float
    
    # Top range of X axis destination motion
    to_max_x_scale: float
    
    # Top range of Y axis destination motion
    to_max_y: float
    
    # Top range of Y axis destination motion
    to_max_y_rot: float
    
    # Top range of Y axis destination motion
    to_max_y_scale: float
    
    # Top range of Z axis destination motion
    to_max_z: float
    
    # Top range of Z axis destination motion
    to_max_z_rot: float
    
    # Top range of Z axis destination motion
    to_max_z_scale: float
    
    # Bottom range of X axis destination motion
    to_min_x: float
    
    # Bottom range of X axis destination motion
    to_min_x_rot: float
    
    # Bottom range of X axis destination motion
    to_min_x_scale: float
    
    # Bottom range of Y axis destination motion
    to_min_y: float
    
    # Bottom range of Y axis destination motion
    to_min_y_rot: float
    
    # Bottom range of Y axis destination motion
    to_min_y_scale: float
    
    # Bottom range of Z axis destination motion
    to_min_z: float
    
    # Bottom range of Z axis destination motion
    to_min_z_rot: float
    
    # Bottom range of Z axis destination motion
    to_min_z_scale: float
    
    # Extrapolate ranges
    use_motion_extrapolate: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...