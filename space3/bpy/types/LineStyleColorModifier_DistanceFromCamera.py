from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class LineStyleColorModifier_DistanceFromCamera(LineStyleColorModifier):
    
    # Specify how the modifier value is blended into the base value
    blend: str = 'MIX' # ['MIX', 'DARKEN', 'MULTIPLY', 'BURN', 'LIGHTEN', 'SCREEN', 'DODGE', 'ADD', 'OVERLAY', 'SOFT_LIGHT', 'LINEAR_LIGHT', 'DIFFERENCE', 'SUBTRACT', 'DIVIDE', 'HUE', 'SATURATION', 'COLOR', 'VALUE']
    
    # Color ramp used to change line color
    color_ramp: ColorRamp
    
    # True if the modifier tab is expanded
    expanded: bool
    
    # Influence factor by which the modifier changes the property
    influence: float
    
    # Name of the modifier
    name: str
    
    # Upper bound of the input range the mapping is applied
    range_max: float
    
    # Lower bound of the input range the mapping is applied
    range_min: float
    
    # Type of the modifier
    type: str = 'ALONG_STROKE' # ['ALONG_STROKE', 'CREASE_ANGLE', 'CURVATURE_3D', 'DISTANCE_FROM_CAMERA', 'DISTANCE_FROM_OBJECT', 'MATERIAL', 'NOISE', 'TANGENT']
    
    # Enable or disable this modifier during stroke rendering
    use: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...