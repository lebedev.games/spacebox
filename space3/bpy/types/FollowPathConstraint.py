from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class FollowPathConstraint(Constraint):
    
    # Axis that points forward along the path
    forward_axis: str = 'FORWARD_X' # ['FORWARD_X', 'FORWARD_Y', 'FORWARD_Z', 'TRACK_NEGATIVE_X', 'TRACK_NEGATIVE_Y', 'TRACK_NEGATIVE_Z']
    
    # Offset from the position corresponding to the time frame
    offset: float
    
    # Percentage value defining target position along length of curve
    offset_factor: float
    
    # Target Curve object
    target: Object
    
    # Axis that points upward
    up_axis: str = 'UP_X' # ['UP_X', 'UP_Y', 'UP_Z']
    
    # Object will follow the heading and banking of the curve
    use_curve_follow: bool
    
    # Object is scaled by the curve radius
    use_curve_radius: bool
    
    # Object will stay locked to a single point somewhere along the length of the curve regardless of time
    use_fixed_location: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...