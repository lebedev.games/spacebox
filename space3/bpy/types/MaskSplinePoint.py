from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MaskSplinePoint(bpy_struct):
    
    # Coordinates of the control point
    co: Tuple[float, float]
    
    # Points defining feather
    feather_points: List[MaskSplinePointUW]
    
    # Coordinates of the first handle
    handle_left: Tuple[float, float]
    
    # Handle type
    handle_left_type: str = 'FREE' # ['AUTO', 'VECTOR', 'ALIGNED', 'ALIGNED_DOUBLESIDE', 'FREE']
    
    # Coordinates of the second handle
    handle_right: Tuple[float, float]
    
    # Handle type
    handle_right_type: str = 'FREE' # ['AUTO', 'VECTOR', 'ALIGNED', 'ALIGNED_DOUBLESIDE', 'FREE']
    
    # Handle type
    handle_type: str = 'FREE' # ['AUTO', 'VECTOR', 'ALIGNED', 'ALIGNED_DOUBLESIDE', 'FREE']
    
    parent: MaskParent
    
    # Selection status
    select: bool
    
    # Weight of the point
    weight: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...