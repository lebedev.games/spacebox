from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ImageTexture(Texture):
    
    # Distance between checker tiles
    checker_distance: float
    
    # Maximum X value to crop the image
    crop_max_x: float
    
    # Maximum Y value to crop the image
    crop_max_y: float
    
    # Minimum X value to crop the image
    crop_min_x: float
    
    # Minimum Y value to crop the image
    crop_min_y: float
    
    # How the image is extrapolated past its original bounds
    extension: str = 'REPEAT' # ['EXTEND', 'CLIP', 'CLIP_CUBE', 'REPEAT', 'CHECKER']
    
    # Maximum eccentricity (higher gives less blur at distant/oblique angles, but is also slower)
    filter_eccentricity: int
    
    # Maximum number of samples (higher gives less blur at distant/oblique angles, but is also slower)
    filter_lightprobes: int
    
    # Multiply the filter size used by MIP Map and Interpolation
    filter_size: float
    
    # Texture filter to use for sampling image
    filter_type: str = 'EWA' # ['BOX', 'EWA', 'FELINE', 'AREA']
    
    image: Image
    
    # Parameters defining which layer, pass and frame of the image is displayed
    image_user: ImageUser
    
    # Invert all the alpha values in the image
    invert_alpha: bool
    
    # Repetition multiplier in the X direction
    repeat_x: int
    
    # Repetition multiplier in the Y direction
    repeat_y: int
    
    # Use the alpha channel information in the image
    use_alpha: bool
    
    # Calculate an alpha channel based on RGB values in the image
    use_calculate_alpha: bool
    
    # Even checker tiles
    use_checker_even: bool
    
    # Odd checker tiles
    use_checker_odd: bool
    
    # Use Filter Size as a minimal filter value in pixels
    use_filter_size_min: bool
    
    # Flip the texture’s X and Y axis
    use_flip_axis: bool
    
    # Interpolate pixels using selected filter
    use_interpolation: bool
    
    # Use auto-generated MIP maps for the image
    use_mipmap: bool
    
    # Use Gauss filter to sample down MIP maps
    use_mipmap_gauss: bool
    
    # Mirror the image repetition on the X direction
    use_mirror_x: bool
    
    # Mirror the image repetition on the Y direction
    use_mirror_y: bool
    
    # Use image RGB values for normal mapping
    use_normal_map: bool
    
    # Materials that use this texture
    users_material: Any
    
    # Object modifiers that use this texture
    users_object_modifier: Any

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...