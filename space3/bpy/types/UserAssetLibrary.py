from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class UserAssetLibrary(bpy_struct):
    
    # Identifier (not necessarily unique) for the asset library
    name: str
    
    # Path to a directory with .blend files to use as an asset library
    path: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...