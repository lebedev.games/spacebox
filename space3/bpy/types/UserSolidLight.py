from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class UserSolidLight(bpy_struct):
    
    # Color of the light’s diffuse highlight
    diffuse_color: Tuple[float, float, float]
    
    # Direction that the light is shining
    direction: Tuple[float, float, float]
    
    # Smooth the lighting from this light
    smooth: float
    
    # Color of the light’s specular highlight
    specular_color: Tuple[float, float, float]
    
    # Enable this light in solid shading mode
    use: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...