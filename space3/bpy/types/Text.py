from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Text(ID):
    
    # Index of current character in current line, and also start index of character in selection if one exists
    current_character: int
    
    # Current line, and start line of selection if one exists
    current_line: TextLine
    
    # Index of current TextLine in TextLine collection
    current_line_index: int
    
    # Filename of the text file
    filepath: str
    
    # Use tabs or spaces for indentation
    indentation: str = 'TABS' # ['TABS', 'SPACES']
    
    # Text file has been edited since last save
    is_dirty: bool
    
    # Text file is in memory, without a corresponding file on disk
    is_in_memory: bool
    
    # Text file on disk is different than the one in memory
    is_modified: bool
    
    # Lines of text
    lines: List[TextLine]
    
    # Index of character after end of selection in the selection end line
    select_end_character: int
    
    # End line of selection
    select_end_line: TextLine
    
    # Index of last TextLine in selection
    select_end_line_index: int
    
    # Run this text as a script on loading, Text name must end with “.py”
    use_module: bool

    def clear(self, *args, **kwargs) -> None:
        ...

    def write(self, *args, **kwargs) -> None:
        ...

    def is_syntax_highlight_supported(self, *args, **kwargs) -> bool:
        ...

    def select_set(self, *args, **kwargs) -> None:
        ...

    def cursor_set(self, *args, **kwargs) -> None:
        ...

    def as_module(self, *args, **kwargs) -> None:
        ...

    def as_string(self, *args, **kwargs) -> None:
        ...

    def from_string(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...