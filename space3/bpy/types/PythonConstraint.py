from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class PythonConstraint(Constraint):
    
    # The linked Python script has thrown an error
    has_script_error: bool
    
    # Usually only 1 to 3 are needed
    target_count: int
    
    # Target Objects
    targets: List[ConstraintTarget]
    
    # The text object that contains the Python script
    text: Text
    
    # Use the targets indicated in the constraint panel
    use_targets: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...