from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ColorManagedInputColorspaceSettings(bpy_struct):
    
    # Treat image as non-color data without color management, like normal or displacement maps
    is_data: bool
    
    # Color space in the image file, to convert to and from when saving and loading the image
    name: str = 'NONE' # ['Filmic Log', 'Linear', 'Linear ACES', 'Non-Color', 'Raw', 'sRGB', 'XYZ']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...