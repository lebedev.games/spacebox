from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CompositorNodeEllipseMask(CompositorNode):
    
    # Height of the ellipse
    height: float
    
    mask_type: str = 'ADD' # ['ADD', 'SUBTRACT', 'MULTIPLY', 'NOT']
    
    # Rotation angle of the ellipse
    rotation: float
    
    # Width of the ellipse
    width: float
    
    # X position of the middle of the ellipse
    x: float
    
    # Y position of the middle of the ellipse
    y: float

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    def update(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...