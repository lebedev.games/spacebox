from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class BrushTextureSlot(TextureSlot):
    
    # Brush texture rotation
    angle: float
    
    has_random_texture_angle: bool
    
    has_texture_angle: bool
    
    has_texture_angle_source: bool
    
    map_mode: str = 'TILED' # ['VIEW_PLANE', 'AREA_PLANE', 'TILED', '3D', 'RANDOM', 'STENCIL']
    
    mask_map_mode: str = 'TILED' # ['VIEW_PLANE', 'TILED', 'RANDOM', 'STENCIL']
    
    # Brush texture random angle
    random_angle: float
    
    use_rake: bool
    
    use_random: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...