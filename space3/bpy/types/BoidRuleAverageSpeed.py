from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class BoidRuleAverageSpeed(BoidRule):
    
    # How much velocity’s z-component is kept constant
    level: float
    
    # Percentage of maximum speed
    speed: float
    
    # How fast velocity’s direction is randomized
    wander: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...