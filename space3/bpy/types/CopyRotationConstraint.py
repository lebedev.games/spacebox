from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CopyRotationConstraint(Constraint):
    
    # Explicitly specify the euler rotation order
    euler_order: str = 'AUTO' # ['AUTO', 'XYZ', 'XZY', 'YXZ', 'YZX', 'ZXY', 'ZYX']
    
    # Invert the X rotation
    invert_x: bool
    
    # Invert the Y rotation
    invert_y: bool
    
    # Invert the Z rotation
    invert_z: bool
    
    # Specify how the copied and existing rotations are combined
    mix_mode: str = 'REPLACE' # ['REPLACE', 'ADD', 'BEFORE', 'AFTER', 'OFFSET']
    
    # Armature bone, mesh or lattice vertex group, …
    subtarget: str
    
    # Target object
    target: Object
    
    # DEPRECATED: Add original rotation into copied rotation
    use_offset: bool
    
    # Copy the target’s X rotation
    use_x: bool
    
    # Copy the target’s Y rotation
    use_y: bool
    
    # Copy the target’s Z rotation
    use_z: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...