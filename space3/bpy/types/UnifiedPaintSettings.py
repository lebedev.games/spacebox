from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class UnifiedPaintSettings(bpy_struct):
    
    color: Tuple[float, float, float]
    
    secondary_color: Tuple[float, float, float]
    
    # Radius of the brush
    size: int
    
    # How powerful the effect of the brush is when applied
    strength: float
    
    # Radius of brush in Blender units
    unprojected_radius: float
    
    # Measure brush size relative to the view or the scene
    use_locked_size: str = 'VIEW' # ['VIEW', 'SCENE']
    
    # Instead of per-brush color, the color is shared across brushes
    use_unified_color: bool
    
    # Instead of per-brush radius, the radius is shared across brushes
    use_unified_size: bool
    
    # Instead of per-brush strength, the strength is shared across brushes
    use_unified_strength: bool
    
    # Instead of per-brush weight, the weight is shared across brushes
    use_unified_weight: bool
    
    # Weight to assign in vertex groups
    weight: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...