from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class DynamicPaintModifier(Modifier):
    
    brush_settings: DynamicPaintBrushSettings
    
    canvas_settings: DynamicPaintCanvasSettings
    
    ui_type: str = 'CANVAS' # ['CANVAS', 'BRUSH']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...