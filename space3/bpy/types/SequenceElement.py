from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SequenceElement(bpy_struct):
    
    # Name of the source file
    filename: str
    
    # Original image height
    orig_height: int
    
    # Original image width
    orig_width: int

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...