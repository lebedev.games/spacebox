from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ImageUser(bpy_struct):
    
    # Current frame number in image sequence or movie
    frame_current: int
    
    # Number of images of a movie to use
    frame_duration: int
    
    # Offset the number of the frame to use in the animation
    frame_offset: int
    
    # Global starting frame of the movie/sequence, assuming first picture has a #1
    frame_start: int
    
    # Layer in multilayer image
    multilayer_layer: int
    
    # Pass in multilayer image
    multilayer_pass: int
    
    # View in multilayer image
    multilayer_view: int
    
    # Tile in tiled image
    tile: int
    
    # Always refresh image on frame changes
    use_auto_refresh: bool
    
    # Cycle the images in the movie
    use_cyclic: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...