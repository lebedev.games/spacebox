from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class FluidEffectorSettings(bpy_struct):
    
    # Change type of effector in the simulation
    effector_type: str = 'COLLISION' # ['COLLISION', 'GUIDE']
    
    # How to create guiding velocities
    guide_mode: str = 'OVERRIDE' # ['MAXIMUM', 'MINIMUM', 'OVERRIDE', 'AVERAGED']
    
    # Number of additional samples to take between frames to improve quality of fast moving effector objects
    subframes: int
    
    # Additional distance around mesh surface to consider as effector
    surface_distance: float
    
    # Control when to apply the effector
    use_effector: bool
    
    # Treat this object as a planar, unclosed mesh
    use_plane_init: bool
    
    # Multiplier of obstacle velocity
    velocity_factor: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...