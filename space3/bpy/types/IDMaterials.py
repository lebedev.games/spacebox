from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class IDMaterials(bpy_struct, List[Material]):

    def append(self, *args, **kwargs) -> None:
        ...

    def pop(self, *args, **kwargs) -> Material:
        ...

    def clear(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...