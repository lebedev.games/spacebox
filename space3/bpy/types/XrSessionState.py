from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class XrSessionState(bpy_struct):
    
    # Last known location of the viewer pose (center between the eyes) in world space
    viewer_pose_location: Tuple[float, float, float]
    
    # Last known rotation of the viewer pose (center between the eyes) in world space
    viewer_pose_rotation: Tuple[float, float, float, float]

    @classmethod
    def is_running(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def reset_to_base_pose(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...