from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Struct(bpy_struct):
    
    # Struct definition this is derived from
    base: 'Struct'
    
    # Description of the Struct’s purpose
    description: str
    
    functions: List[Function]
    
    # Unique name used in the code and scripting
    identifier: str
    
    # Human readable name
    name: str
    
    # Property that gives the name of the struct
    name_property: StringProperty
    
    # Struct in which this struct is always nested, and to which it logically belongs
    nested: 'Struct'
    
    # Properties in the struct
    properties: List[Property]
    
    # Tags that properties can use to influence behavior
    property_tags: List[EnumPropertyItem]
    
    # Translation context of the struct’s name
    translation_context: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> 'Struct':
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...