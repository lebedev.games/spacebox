from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class NodeFrame(NodeInternal):
    
    # Font size to use for displaying the label
    label_size: int
    
    # Shrink the frame to minimal bounding box
    shrink: bool
    
    text: Text

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...