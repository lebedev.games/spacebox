from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class LoopColors(bpy_struct):
    
    # Active vertex color layer
    active: MeshLoopColorLayer
    
    # Active vertex color index
    active_index: int

    def new(self, *args, **kwargs) -> MeshLoopColorLayer:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...