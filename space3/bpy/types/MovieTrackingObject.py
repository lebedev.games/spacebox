from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MovieTrackingObject(bpy_struct):
    
    # Object is used for camera tracking
    is_camera: bool
    
    # First keyframe used for reconstruction initialization
    keyframe_a: int
    
    # Second keyframe used for reconstruction initialization
    keyframe_b: int
    
    # Unique name of object
    name: str
    
    # Collection of plane tracks in this tracking data object
    plane_tracks: MovieTrackingObjectPlaneTracks
    
    reconstruction: MovieTrackingReconstruction
    
    # Scale of object solution in camera space
    scale: float
    
    # Collection of tracks in this tracking data object
    tracks: MovieTrackingObjectTracks

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...