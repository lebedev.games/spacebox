from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class WoodTexture(Texture):
    
    # Size of derivative offset used for calculating normal
    nabla: float
    
    # Noise basis used for turbulence
    noise_basis: str = 'BLENDER_ORIGINAL' # ['BLENDER_ORIGINAL', 'ORIGINAL_PERLIN', 'IMPROVED_PERLIN', 'VORONOI_F1', 'VORONOI_F2', 'VORONOI_F3', 'VORONOI_F4', 'VORONOI_F2_F1', 'VORONOI_CRACKLE', 'CELL_NOISE']
    
    noise_basis_2: str = 'SIN' # ['SIN', 'SAW', 'TRI']
    
    # Scaling for noise input
    noise_scale: float
    
    noise_type: str = 'SOFT_NOISE' # ['SOFT_NOISE', 'HARD_NOISE']
    
    # Turbulence of the bandnoise and ringnoise types
    turbulence: float
    
    wood_type: str = 'BANDS' # ['BANDS', 'RINGS', 'BANDNOISE', 'RINGNOISE']
    
    # Materials that use this texture
    users_material: Any
    
    # Object modifiers that use this texture
    users_object_modifier: Any

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...