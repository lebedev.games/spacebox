from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class KeyingSetPaths(bpy_struct):
    
    # Active Keying Set used to insert/delete keyframes
    active: KeyingSetPath
    
    # Current Keying Set index
    active_index: int

    def add(self, *args, **kwargs) -> KeyingSetPath:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    def clear(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...