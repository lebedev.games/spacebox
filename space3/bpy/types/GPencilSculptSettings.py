from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class GPencilSculptSettings(bpy_struct):
    
    guide: GPencilSculptGuide
    
    # Threshold for stroke intersections
    intersection_threshold: float
    
    lock_axis: str = 'VIEW' # ['VIEW', 'AXIS_Y', 'AXIS_X', 'AXIS_Z', 'CURSOR']
    
    # Custom curve to control falloff of brush effect by Grease Pencil frames
    multiframe_falloff_curve: CurveMapping
    
    # Custom curve to control primitive thickness
    thickness_primitive_curve: CurveMapping
    
    # Use falloff effect when edit in multiframe mode to compute brush effect by frame
    use_multiframe_falloff: bool
    
    # Scale the stroke thickness when transforming strokes
    use_scale_thickness: bool
    
    # Use curve to define primitive stroke thickness
    use_thickness_curve: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...