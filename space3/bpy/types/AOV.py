from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class AOV(bpy_struct):
    
    # Is the name of the AOV conflicting
    is_valid: bool
    
    # Name of the AOV
    name: str
    
    # Data type of the AOV
    type: str = 'COLOR' # ['COLOR', 'VALUE']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...