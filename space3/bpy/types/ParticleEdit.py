from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ParticleEdit(bpy_struct):
    
    brush: ParticleBrush
    
    # How many keys to make new particles with
    default_key_count: int
    
    # How many steps to display the path with
    display_step: int
    
    # Distance to keep particles away from the emitter
    emitter_distance: float
    
    # How many frames to fade
    fade_frames: int
    
    # A valid edit mode exists
    is_editable: bool
    
    # Editing hair
    is_hair: bool
    
    # The edited object
    object: Object
    
    # Particle select and display mode
    select_mode: str = 'PATH' # ['PATH', 'POINT', 'TIP']
    
    # Outer shape to use for tools
    shape_object: Object
    
    # Display actual particles
    show_particles: bool
    
    tool: str = 'COMB' # ['COMB', 'SMOOTH', 'ADD', 'LENGTH', 'PUFF', 'CUT', 'WEIGHT']
    
    type: str = 'PARTICLES' # ['PARTICLES', 'SOFT_BODY', 'CLOTH']
    
    # Calculate point velocities automatically
    use_auto_velocity: bool
    
    # Interpolate new particles from the existing ones
    use_default_interpolate: bool
    
    # Keep paths from intersecting the emitter
    use_emitter_deflect: bool
    
    # Fade paths and keys further away from current frame
    use_fade_time: bool
    
    # Keep path lengths constant
    use_preserve_length: bool
    
    # Keep root keys unmodified
    use_preserve_root: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...