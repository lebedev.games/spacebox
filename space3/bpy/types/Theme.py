from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Theme(bpy_struct):
    
    bone_color_sets: List[ThemeBoneColorSet]
    
    clip_editor: ThemeClipEditor
    
    collection_color: List[ThemeCollectionColor]
    
    console: ThemeConsole
    
    dopesheet_editor: ThemeDopeSheet
    
    file_browser: ThemeFileBrowser
    
    graph_editor: ThemeGraphEditor
    
    image_editor: ThemeImageEditor
    
    info: ThemeInfo
    
    # Name of the theme
    name: str
    
    nla_editor: ThemeNLAEditor
    
    node_editor: ThemeNodeEditor
    
    outliner: ThemeOutliner
    
    preferences: ThemePreferences
    
    properties: ThemeProperties
    
    sequence_editor: ThemeSequenceEditor
    
    spreadsheet: ThemeSpreadsheet
    
    statusbar: ThemeStatusBar
    
    text_editor: ThemeTextEditor
    
    theme_area: str = 'USER_INTERFACE' # ['USER_INTERFACE', 'STYLE', 'BONE_COLOR_SETS', 'VIEW_3D', 'GRAPH_EDITOR', 'DOPESHEET_EDITOR', 'NLA_EDITOR', 'IMAGE_EDITOR', 'SEQUENCE_EDITOR', 'TEXT_EDITOR', 'NODE_EDITOR', 'PROPERTIES', 'OUTLINER', 'PREFERENCES', 'INFO', 'FILE_BROWSER', 'CONSOLE', 'CLIP_EDITOR', 'TOPBAR', 'STATUSBAR', 'SPREADSHEET']
    
    topbar: ThemeTopBar
    
    user_interface: ThemeUserInterface
    
    view_3d: ThemeView3D

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...