from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SpaceSequenceEditor(Space):
    
    # The channel number shown in the image preview. 0 is the result of all strips combined
    display_channel: int
    
    # View mode to use for displaying sequencer output
    display_mode: str = 'IMAGE' # ['IMAGE', 'WAVEFORM', 'VECTOR_SCOPE', 'HISTOGRAM']
    
    # Grease Pencil data for this Preview region
    grease_pencil: GreasePencil
    
    # Overlay display method
    overlay_type: str = 'RECTANGLE' # ['RECTANGLE', 'REFERENCE', 'CURRENT']
    
    # Channels of the preview to display
    preview_channels: str = 'COLOR' # ['COLOR_ALPHA', 'COLOR']
    
    # Display preview using full resolution or different proxy resolutions
    proxy_render_size: str = 'SCENE' # ['NONE', 'SCENE', 'PROXY_25', 'PROXY_50', 'PROXY_75', 'PROXY_100']
    
    # Show annotations for this view
    show_annotation: bool
    
    # Display result under strips
    show_backdrop: bool
    
    # Display strip opacity/volume curve
    show_fcurves: bool
    
    # Display frames rather than seconds
    show_frames: bool
    
    # If any exists, show markers in a separate row at the bottom of the editor
    show_markers: bool
    
    # Show metadata of first visible strip
    show_metadata: bool
    
    # Show overexposed areas with zebra stripes
    show_overexposed: int
    
    show_region_hud: bool
    
    show_region_tool_header: bool
    
    show_region_toolbar: bool
    
    show_region_ui: bool
    
    # Show TV title safe and action safe areas in preview
    show_safe_areas: bool
    
    # Show safe areas to fit content in a different aspect ratio
    show_safe_center: bool
    
    # Show timing in seconds not frames
    show_seconds: bool
    
    # Separate color channels in preview
    show_separate_color: bool
    
    show_strip_duration: bool
    
    show_strip_name: bool
    
    # Display strip in/out offsets
    show_strip_offset: bool
    
    show_strip_overlay: bool
    
    # Display path to source file, or name of source datablock
    show_strip_source: bool
    
    # Show preview of the transformed frames
    show_transform_preview: bool
    
    # Transform markers as well as strips
    use_marker_sync: bool
    
    # Use optimized files for faster scrubbing when available
    use_proxies: bool
    
    # Automatically zoom preview image to make it fully fit the region
    use_zoom_to_fit: bool
    
    # Type of the Sequencer view (sequencer, preview or both)
    view_type: str = 'SEQUENCER' # ['SEQUENCER', 'PREVIEW', 'SEQUENCER_PREVIEW']
    
    # How Waveforms are displayed
    waveform_display_type: str = 'DEFAULT_WAVEFORMS' # ['NO_WAVEFORMS', 'ALL_WAVEFORMS', 'DEFAULT_WAVEFORMS']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...

    def draw_handler_add(self, *args, **kwargs) -> Any:
        ...

    def draw_handler_remove(self, *args, **kwargs) -> None:
        ...