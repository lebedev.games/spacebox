from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Sequence(bpy_struct):
    
    # Percentage of how much the strip’s colors affect other strips
    blend_alpha: float
    
    # Method for controlling how the strip combines with other strips
    blend_type: str = 'REPLACE' # ['REPLACE', 'CROSS', 'DARKEN', 'MULTIPLY', 'BURN', 'LINEAR_BURN', 'LIGHTEN', 'SCREEN', 'DODGE', 'ADD', 'OVERLAY', 'SOFT_LIGHT', 'HARD_LIGHT', 'VIVID_LIGHT', 'LINEAR_LIGHT', 'PIN_LIGHT', 'DIFFERENCE', 'EXCLUSION', 'SUBTRACT', 'HUE', 'SATURATION', 'COLOR', 'VALUE', 'ALPHA_OVER', 'ALPHA_UNDER', 'GAMMA_CROSS', 'OVER_DROP']
    
    # Y position of the sequence strip
    channel: int
    
    # Custom fade value
    effect_fader: float
    
    # The length of the contents of this strip before the handles are applied
    frame_duration: int
    
    # The length of the contents of this strip after the handles are applied
    frame_final_duration: int
    
    # End frame displayed in the sequence editor after offsets are applied
    frame_final_end: int
    
    # Start frame displayed in the sequence editor after offsets are applied, setting this is equivalent to moving the handle, not the actual start frame
    frame_final_start: int
    
    frame_offset_end: int
    
    frame_offset_start: int
    
    # X position where the strip begins
    frame_start: int
    
    frame_still_end: int
    
    frame_still_start: int
    
    # Lock strip so that it cannot be transformed
    lock: bool
    
    # Modifiers affecting this strip
    modifiers: SequenceModifiers
    
    # Disable strip so that it cannot be viewed in the output
    mute: bool
    
    name: str
    
    # Override global cache settings
    override_cache_settings: bool
    
    select: bool
    
    select_left_handle: bool
    
    select_right_handle: bool
    
    # Multiply the current speed of the sequence with this number or remap current frame to this frame
    speed_factor: float
    
    type: str = 'IMAGE' # ['IMAGE', 'META', 'SCENE', 'MOVIE', 'MOVIECLIP', 'MASK', 'SOUND', 'CROSS', 'ADD', 'SUBTRACT', 'ALPHA_OVER', 'ALPHA_UNDER', 'GAMMA_CROSS', 'MULTIPLY', 'OVER_DROP', 'WIPE', 'GLOW', 'TRANSFORM', 'COLOR', 'SPEED', 'MULTICAM', 'ADJUSTMENT', 'GAUSSIAN_BLUR', 'TEXT', 'COLORMIX']
    
    # Cache intermediate composited images, for faster tweaking of stacked strips at the cost of memory usage
    use_cache_composite: bool
    
    # Cache preprocessed images, for faster tweaking of effects at the cost of memory usage
    use_cache_preprocessed: bool
    
    # Cache raw images read from disk, for faster tweaking of strip parameters at the cost of memory usage
    use_cache_raw: bool
    
    # Fade effect using the built-in default (usually make transition as long as effect strip)
    use_default_fade: bool
    
    # Calculate modifiers in linear space instead of sequencer’s space
    use_linear_modifiers: bool

    def update(self, *args, **kwargs) -> None:
        ...

    def strip_elem_from_frame(self, *args, **kwargs) -> SequenceElement:
        ...

    def swap(self, *args, **kwargs) -> None:
        ...

    def move_to_meta(self, *args, **kwargs) -> None:
        ...

    def invalidate_cache(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...