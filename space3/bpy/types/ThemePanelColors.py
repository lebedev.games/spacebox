from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ThemePanelColors(bpy_struct):
    
    back: Tuple[float, float, float, float]
    
    header: Tuple[float, float, float, float]
    
    sub_back: Tuple[float, float, float, float]

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...