from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class TexPaintSlot(bpy_struct):
    
    # Slot has a valid image and UV map
    is_valid: bool
    
    # Name of UV map
    uv_layer: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...