from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class FaceMap(bpy_struct):
    
    # Index number of the face map
    index: int
    
    # Face map name
    name: str
    
    # Face map selection state (for tools to use)
    select: bool

    def add(self, *args, **kwargs) -> None:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...