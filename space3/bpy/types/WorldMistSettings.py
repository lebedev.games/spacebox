from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class WorldMistSettings(bpy_struct):
    
    # Distance over which the mist effect fades in
    depth: float
    
    # Type of transition used to fade mist
    falloff: str = 'QUADRATIC' # ['QUADRATIC', 'LINEAR', 'INVERSE_QUADRATIC']
    
    # Control how much mist density decreases with height
    height: float
    
    # Overall minimum intensity of the mist effect
    intensity: float
    
    # Starting distance of the mist, measured from the camera
    start: float
    
    # Occlude objects with the environment color as they are further away
    use_mist: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...