from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class AnimData(bpy_struct):
    
    # Active Action for this data-block
    action: Action
    
    # Method used for combining Active Action’s result with result of NLA stack
    action_blend_type: str = 'REPLACE' # ['REPLACE', 'COMBINE', 'ADD', 'SUBTRACT', 'MULTIPLY']
    
    # Action to take for gaps past the Active Action’s range (when evaluating with NLA)
    action_extrapolation: str = 'HOLD' # ['NOTHING', 'HOLD', 'HOLD_FORWARD']
    
    # Amount the Active Action contributes to the result of the NLA stack
    action_influence: float
    
    # The Drivers/Expressions for this data-block
    drivers: AnimDataDrivers
    
    # NLA Tracks (i.e. Animation Layers)
    nla_tracks: NlaTracks
    
    # NLA stack is evaluated when evaluating this block
    use_nla: bool
    
    use_pin: bool
    
    # Whether to enable or disable tweak mode in NLA
    use_tweak_mode: bool

    def nla_tweak_strip_time_to_scene(self, *args, **kwargs) -> float:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...