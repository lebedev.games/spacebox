from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CompositorNodeImage(CompositorNode):
    
    # Number of images of a movie to use
    frame_duration: int
    
    # Offset the number of the frame to use in the animation
    frame_offset: int
    
    # Global starting frame of the movie/sequence, assuming first picture has a #1
    frame_start: int
    
    # True if this image has any named layer
    has_layers: bool
    
    # True if this image has multiple views
    has_views: bool
    
    image: Image
    
    layer: str = 'PLACEHOLDER' # ['PLACEHOLDER']
    
    # Always refresh image on frame changes
    use_auto_refresh: bool
    
    # Cycle the images in the movie
    use_cyclic: bool
    
    # Put node output buffer to straight alpha instead of premultiplied
    use_straight_alpha_output: bool
    
    view: str = 'ALL' # ['ALL']

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    def update(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...