from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Float2AttributeValue(bpy_struct):
    
    # 2D vector
    vector: Tuple[float, float]

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...