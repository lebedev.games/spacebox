from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class LatticePoint(bpy_struct):
    
    # Original undeformed location used to calculate the strength of the deform effect (edit/animate the Deformed Location instead)
    co: Tuple[float, float, float]
    
    co_deform: Tuple[float, float, float]
    
    # Weights for the vertex groups this point is member of
    groups: List[VertexGroupElement]
    
    # Selection status
    select: bool
    
    # Softbody goal weight
    weight_softbody: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...