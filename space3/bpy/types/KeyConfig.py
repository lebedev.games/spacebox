from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class KeyConfig(bpy_struct):
    
    # Indicates that a keyconfig was defined by the user
    is_user_defined: bool
    
    # Key maps configured as part of this configuration
    keymaps: KeyMaps
    
    # Name of the key configuration
    name: str
    
    preferences: KeyConfigPreferences

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...