from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CurveProfile(bpy_struct):
    
    # Profile control points
    points: CurveProfilePoints
    
    preset: str = 'LINE' # ['LINE', 'SUPPORTS', 'CORNICE', 'CROWN', 'STEPS']
    
    # Segments sampled from control points
    segments: List[CurveProfilePoint]
    
    # Force the path view to fit a defined boundary
    use_clip: bool
    
    # Sample edges with even lengths
    use_sample_even_lengths: bool
    
    # Sample edges with vector handles
    use_sample_straight_edges: bool

    def update(self, *args, **kwargs) -> None:
        ...

    def reset_view(self, *args, **kwargs) -> None:
        ...

    def initialize(self, *args, **kwargs) -> None:
        ...

    def evaluate(self, *args, **kwargs) -> Tuple[float, float]:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...