from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class BevelModifier(Modifier):
    
    # Affect edges or vertices
    affect: str = 'EDGES' # ['VERTICES', 'EDGES']
    
    # Angle above which to bevel edges
    angle_limit: float
    
    # The path for the custom profile
    custom_profile: CurveProfile
    
    # Whether to set face strength, and which faces to set it on
    face_strength_mode: str = 'FSTR_NONE' # ['FSTR_NONE', 'FSTR_NEW', 'FSTR_AFFECTED', 'FSTR_ALL']
    
    # Match normals of new faces to adjacent faces
    harden_normals: bool
    
    # Invert vertex group influence
    invert_vertex_group: bool
    
    limit_method: str = 'ANGLE' # ['NONE', 'ANGLE', 'WEIGHT', 'VGROUP']
    
    # Prefer sliding along edges to having even widths
    loop_slide: bool
    
    # Mark Seams along beveled edges
    mark_seam: bool
    
    # Mark beveled edges as sharp
    mark_sharp: bool
    
    # Material index of generated faces, -1 for automatic
    material: int
    
    # Pattern to use for inside of miters
    miter_inner: str = 'MITER_SHARP' # ['MITER_SHARP', 'MITER_ARC']
    
    # Pattern to use for outside of miters
    miter_outer: str = 'MITER_SHARP' # ['MITER_SHARP', 'MITER_PATCH', 'MITER_ARC']
    
    # What distance Width measures
    offset_type: str = 'OFFSET' # ['OFFSET', 'WIDTH', 'DEPTH', 'PERCENT', 'ABSOLUTE']
    
    # The profile shape (0.5 = round)
    profile: float
    
    # The type of shape used to rebuild a beveled section
    profile_type: str = 'SUPERELLIPSE' # ['SUPERELLIPSE', 'CUSTOM']
    
    # Number of segments for round edges/verts
    segments: int
    
    # Spread distance for inner miter arcs
    spread: float
    
    # Clamp the width to avoid overlap
    use_clamp_overlap: bool
    
    # Vertex group name
    vertex_group: str
    
    # The method to use to create the mesh at intersections
    vmesh_method: str = 'ADJ' # ['ADJ', 'CUTOFF']
    
    # Bevel amount
    width: float
    
    # Bevel amount for percentage method
    width_pct: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...