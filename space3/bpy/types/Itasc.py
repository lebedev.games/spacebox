from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Itasc(IKParam):
    
    # Singular value under which damping is progressively applied (higher values produce results with more stability, less reactivity)
    damping_epsilon: float
    
    # Maximum damping coefficient when singular value is nearly 0 (higher values produce results with more stability, less reactivity)
    damping_max: float
    
    # Feedback coefficient for error correction, average response time is 1/feedback
    feedback: float
    
    # Maximum number of iterations for convergence in case of reiteration
    iterations: int
    
    mode: str = 'ANIMATION' # ['ANIMATION', 'SIMULATION']
    
    # Precision of convergence in case of reiteration
    precision: float
    
    # Defines if the solver is allowed to reiterate (converge until precision is met) on none, first or all frames
    reiteration_method: str = 'NEVER' # ['NEVER', 'INITIAL', 'ALWAYS']
    
    # Solving method selection: automatic damping or manual damping
    solver: str = 'SDLS' # ['SDLS', 'DLS']
    
    # Divide the frame interval into this many steps
    step_count: int
    
    # Higher bound for timestep in second in case of automatic substeps
    step_max: float
    
    # Lower bound for timestep in second in case of automatic substeps
    step_min: float
    
    # Automatically determine the optimal number of steps for best performance/accuracy trade off
    use_auto_step: bool
    
    # Maximum joint velocity in radians/second
    velocity_max: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...