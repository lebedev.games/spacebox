from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class NodeSocketInterfaceStandard(NodeSocketInterface):
    
    # Data type
    type: str = 'VALUE' # ['CUSTOM', 'VALUE', 'INT', 'BOOLEAN', 'VECTOR', 'STRING', 'RGBA', 'SHADER', 'OBJECT', 'IMAGE', 'GEOMETRY', 'COLLECTION']

    def draw(self, *args, **kwargs) -> None:
        ...

    def draw_color(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...