from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Function(bpy_struct):
    
    # Description of the Function’s purpose
    description: str
    
    # Unique name used in the code and scripting
    identifier: str
    
    # Function is registered as callback as part of type registration
    is_registered: bool
    
    # Function is optionally registered as callback part of type registration
    is_registered_optional: bool
    
    # Parameters for the function
    parameters: List[Property]
    
    # Function does not pass its self as an argument (becomes a static method in python)
    use_self: bool
    
    # Function passes its self type as an argument (becomes a class method in python if use_self is false)
    use_self_type: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...