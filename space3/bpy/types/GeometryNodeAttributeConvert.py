from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class GeometryNodeAttributeConvert(GeometryNode):
    
    # The data type to save the result attribute with
    data_type: str = 'FLOAT' # ['FLOAT', 'INT', 'FLOAT_VECTOR', 'FLOAT_COLOR', 'BYTE_COLOR', 'STRING', 'BOOLEAN', 'FLOAT2']
    
    # The geometry domain to save the result attribute in
    domain: str = 'AUTO' # ['AUTO', 'POINT', 'EDGE', 'FACE', 'CORNER']

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...