from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class GizmoGroup(bpy_struct):
    
    bl_idname: str
    
    bl_label: str
    
    # Options for this operator type
    bl_options: Set[str] # {'3D', 'PERSISTENT', 'default {3D', 'SELECT', 'SHOW_MODAL_ALL', 'TOOL_INIT', 'VR_REDRAWS}', 'SCALE', 'DEPTH_3D'}
    
    bl_owner_id: str
    
    # The region where the panel is going to be used in
    bl_region_type: str = 'WINDOW' # ['WINDOW', 'HEADER', 'CHANNELS', 'TEMPORARY', 'UI', 'TOOLS', 'TOOL_PROPS', 'PREVIEW', 'HUD', 'NAVIGATION_BAR', 'EXECUTE', 'FOOTER', 'TOOL_HEADER']
    
    # The space where the panel is going to be used in
    bl_space_type: str = 'EMPTY' # ['EMPTY', 'VIEW_3D', 'IMAGE_EDITOR', 'NODE_EDITOR', 'SEQUENCE_EDITOR', 'CLIP_EDITOR', 'DOPESHEET_EDITOR', 'GRAPH_EDITOR', 'NLA_EDITOR', 'TEXT_EDITOR', 'CONSOLE', 'INFO', 'TOPBAR', 'STATUSBAR', 'OUTLINER', 'PROPERTIES', 'FILE_BROWSER', 'SPREADSHEET', 'PREFERENCES']
    
    # List of gizmos in the Gizmo Map
    gizmos: Gizmos
    
    # GizmoGroup has a set of reports (warnings and errors) from last execution
    has_reports: bool
    
    name: str

    @classmethod
    def poll(cls, *args, **kwargs) -> bool:
        ...

    @classmethod
    def setup_keymap(cls, *args, **kwargs) -> KeyMap:
        ...

    def setup(self, *args, **kwargs) -> None:
        ...

    def refresh(self, *args, **kwargs) -> None:
        ...

    def draw_prepare(self, *args, **kwargs) -> None:
        ...

    def invoke_prepare(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...