from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ShrinkwrapConstraint(Constraint):
    
    # Stop vertices from projecting to a face on the target when facing towards/away
    cull_face: str = 'OFF' # ['OFF', 'FRONT', 'BACK']
    
    # Distance to Target
    distance: float
    
    # Axis constrain to
    project_axis: str = 'POS_X' # ['POS_X', 'POS_Y', 'POS_Z', 'NEG_X', 'NEG_Y', 'NEG_Z']
    
    # Space for the projection axis
    project_axis_space: str = 'WORLD' # ['WORLD', 'CUSTOM', 'POSE', 'LOCAL_WITH_PARENT', 'LOCAL']
    
    # Limit the distance used for projection (zero disables)
    project_limit: float
    
    # Select type of shrinkwrap algorithm for target position
    shrinkwrap_type: str = 'NEAREST_SURFACE' # ['NEAREST_SURFACE', 'PROJECT', 'NEAREST_VERTEX', 'TARGET_PROJECT']
    
    # Target Mesh object
    target: Object
    
    # Axis that is aligned to the normal
    track_axis: str = 'TRACK_X' # ['TRACK_X', 'TRACK_Y', 'TRACK_Z', 'TRACK_NEGATIVE_X', 'TRACK_NEGATIVE_Y', 'TRACK_NEGATIVE_Z']
    
    # When projecting in the opposite direction invert the face cull mode
    use_invert_cull: bool
    
    # Project in both specified and opposite directions
    use_project_opposite: bool
    
    # Align the specified axis to the surface normal
    use_track_normal: bool
    
    # Select how to constrain the object to the target surface
    wrap_mode: str = 'ON_SURFACE' # ['ON_SURFACE', 'INSIDE', 'OUTSIDE', 'OUTSIDE_SURFACE', 'ABOVE_SURFACE']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...