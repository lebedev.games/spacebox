from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Region(bpy_struct):
    
    # Alignment of the region within the area
    alignment: str = 'NONE' # ['NONE', 'TOP', 'BOTTOM', 'LEFT', 'RIGHT', 'HORIZONTAL_SPLIT', 'VERTICAL_SPLIT', 'FLOAT', 'QUAD_SPLIT']
    
    # Region specific data (the type depends on the region type)
    data: AnyType
    
    # Region height
    height: int
    
    # Type of this region
    type: str = 'WINDOW' # ['WINDOW', 'HEADER', 'CHANNELS', 'TEMPORARY', 'UI', 'TOOLS', 'TOOL_PROPS', 'PREVIEW', 'HUD', 'NAVIGATION_BAR', 'EXECUTE', 'FOOTER', 'TOOL_HEADER']
    
    # 2D view of the region
    view2d: View2D
    
    # Region width
    width: int
    
    # The window relative vertical location of the region
    x: int
    
    # The window relative horizontal location of the region
    y: int

    def tag_redraw(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...