from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Light(ID):
    
    # Animation data for this data-block
    animation_data: AnimData
    
    # Light color
    color: Tuple[float, float, float]
    
    # Distance at which the light influence will be set to 0
    cutoff_distance: float
    
    # Cycles light settings
    cycles: Any
    
    # Diffuse reflection multiplier
    diffuse_factor: float
    
    # Falloff distance - the light is at half the original intensity at this point
    distance: float
    
    # Node tree for node based lights
    node_tree: NodeTree
    
    # Specular reflection multiplier
    specular_factor: float
    
    # Type of light
    type: str = 'POINT' # ['POINT', 'SUN', 'SPOT', 'AREA']
    
    # Use custom attenuation distance instead of global light threshold
    use_custom_distance: bool
    
    # Use shader nodes to render the light
    use_nodes: bool
    
    # Volume light multiplier
    volume_factor: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...