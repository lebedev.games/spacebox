from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SceneDisplay(bpy_struct):
    
    # Direction of the light for shadows and highlights
    light_direction: Tuple[float, float, float]
    
    # Attenuation constant
    matcap_ssao_attenuation: float
    
    # Distance of object that contribute to the Cavity/Edge effect
    matcap_ssao_distance: float
    
    # Number of samples
    matcap_ssao_samples: int
    
    # Method of anti-aliasing when rendering final image
    render_aa: str = '8' # ['OFF', 'FXAA', '5', '8', '11', '16', '32']
    
    # Shading settings for OpenGL render engine
    shading: View3DShading
    
    # Shadow factor hardness
    shadow_focus: float
    
    # Shadow termination angle
    shadow_shift: float
    
    # Method of anti-aliasing when rendering 3d viewport
    viewport_aa: str = 'FXAA' # ['OFF', 'FXAA', '5', '8', '11', '16', '32']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...