from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class FModifierEnvelope(FModifier):
    
    # Control points defining the shape of the envelope
    control_points: FModifierEnvelopeControlPoints
    
    # Upper distance from Reference Value for 1:1 default influence
    default_max: float
    
    # Lower distance from Reference Value for 1:1 default influence
    default_min: float
    
    # Value that envelope’s influence is centered around / based on
    reference_value: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...