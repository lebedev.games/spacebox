from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ClothModifier(Modifier):
    
    collision_settings: ClothCollisionSettings
    
    hair_grid_max: Tuple[float, float, float]
    
    hair_grid_min: Tuple[float, float, float]
    
    hair_grid_resolution: Tuple[int, int, int]
    
    point_cache: PointCache
    
    settings: ClothSettings
    
    solver_result: ClothSolverResult

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...