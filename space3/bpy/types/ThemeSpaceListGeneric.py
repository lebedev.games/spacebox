from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ThemeSpaceListGeneric(bpy_struct):
    
    list: Tuple[float, float, float]
    
    list_text: Tuple[float, float, float]
    
    list_text_hi: Tuple[float, float, float]
    
    list_title: Tuple[float, float, float]

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...