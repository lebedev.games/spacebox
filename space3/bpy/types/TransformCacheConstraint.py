from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class TransformCacheConstraint(Constraint):
    
    cache_file: CacheFile
    
    # Path to the object in the Alembic archive used to lookup the transform matrix
    object_path: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...