from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SplinePoint(bpy_struct):
    
    # Point coordinates
    co: Tuple[float, float, float, float]
    
    # Visibility status
    hide: bool
    
    # Radius for beveling
    radius: float
    
    # Selection status
    select: bool
    
    # Tilt in 3D View
    tilt: float
    
    # NURBS weight
    weight: float
    
    # Softbody goal weight
    weight_softbody: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...