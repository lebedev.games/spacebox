from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MovieTrackingMarkers(bpy_struct):

    def find_frame(self, *args, **kwargs) -> MovieTrackingMarker:
        ...

    def insert_frame(self, *args, **kwargs) -> MovieTrackingMarker:
        ...

    def delete_frame(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...