from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class BlendDataTexts(bpy_struct):

    def new(self, *args, **kwargs) -> Text:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    def load(self, *args, **kwargs) -> Text:
        ...

    def tag(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...