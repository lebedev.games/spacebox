from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MirrorModifier(Modifier):
    
    # Distance from the bisect plane within which vertices are removed
    bisect_threshold: float
    
    # Distance within which mirrored vertices are merged
    merge_threshold: float
    
    # Object to use as mirror
    mirror_object: Object
    
    # Amount to offset mirrored UVs flipping point from the 0.5 on the U axis
    mirror_offset_u: float
    
    # Amount to offset mirrored UVs flipping point from the 0.5 point on the V axis
    mirror_offset_v: float
    
    # Mirrored UV offset on the U axis
    offset_u: float
    
    # Mirrored UV offset on the V axis
    offset_v: float
    
    # Enable axis mirror
    use_axis: bool
    
    # Cuts the mesh across the mirror plane
    use_bisect_axis: bool
    
    # Flips the direction of the slice
    use_bisect_flip_axis: bool
    
    # Prevent vertices from going through the mirror during transform
    use_clip: bool
    
    # Merge vertices within the merge threshold
    use_mirror_merge: bool
    
    # Mirror the U texture coordinate around the flip offset point
    use_mirror_u: bool
    
    # Mirror the texture coordinate around each tile center
    use_mirror_udim: bool
    
    # Mirror the V texture coordinate around the flip offset point
    use_mirror_v: bool
    
    # Mirror vertex groups (e.g. .R-&gt;.L)
    use_mirror_vertex_groups: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...