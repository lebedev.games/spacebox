from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class OperatorFileListElement(PropertyGroup):
    
    # Name of a file or directory within a file list
    name: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...