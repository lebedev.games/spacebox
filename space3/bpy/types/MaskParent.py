from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MaskParent(bpy_struct):
    
    # ID-block to which masking element would be parented to or to its property
    id: ID
    
    # Type of ID-block that can be used
    id_type: str = 'MOVIECLIP' # ['MOVIECLIP']
    
    # Name of parent object in specified data-block to which parenting happens
    parent: str
    
    # Name of parent sub-object in specified data-block to which parenting happens
    sub_parent: str
    
    # Parent Type
    type: str = 'POINT_TRACK' # ['POINT_TRACK', 'PLANE_TRACK']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...