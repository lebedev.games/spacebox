from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MetaBallElements(bpy_struct):
    
    # Last selected element
    active: MetaElement

    def new(self, *args, **kwargs) -> MetaElement:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    def clear(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...