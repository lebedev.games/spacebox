from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MovieTrackingStabilization(bpy_struct):
    
    # Index of active track in rotation stabilization tracks list
    active_rotation_track_index: int
    
    # Index of active track in translation stabilization tracks list
    active_track_index: int
    
    # Reference point to anchor stabilization (other frames will be adjusted relative to this frame’s position)
    anchor_frame: int
    
    # Interpolation to use for sub-pixel shifts and rotations due to stabilization
    filter_type: str = 'NEAREST' # ['NEAREST', 'BILINEAR', 'BICUBIC']
    
    # Influence of stabilization algorithm on footage location
    influence_location: float
    
    # Influence of stabilization algorithm on footage rotation
    influence_rotation: float
    
    # Influence of stabilization algorithm on footage scale
    influence_scale: float
    
    # Collection of tracks used for 2D stabilization (translation)
    rotation_tracks: List[MovieTrackingTrack]
    
    # Limit the amount of automatic scaling
    scale_max: float
    
    # Show UI list of tracks participating in stabilization
    show_tracks_expanded: bool
    
    # Known relative offset of original shot, will be subtracted (e.g. for panning shot, can be animated)
    target_position: Tuple[float, float]
    
    # Rotation present on original shot, will be compensated (e.g. for deliberate tilting)
    target_rotation: float
    
    # Explicitly scale resulting frame to compensate zoom of original shot
    target_scale: float
    
    # Collection of tracks used for 2D stabilization (translation)
    tracks: List[MovieTrackingTrack]
    
    # Use 2D stabilization for footage
    use_2d_stabilization: bool
    
    # Automatically scale footage to cover unfilled areas when stabilizing
    use_autoscale: bool
    
    # Stabilize detected rotation around center of frame
    use_stabilize_rotation: bool
    
    # Compensate any scale changes relative to center of rotation
    use_stabilize_scale: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...