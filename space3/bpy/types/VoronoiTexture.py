from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class VoronoiTexture(Texture):
    
    color_mode: str = 'INTENSITY' # ['INTENSITY', 'POSITION', 'POSITION_OUTLINE', 'POSITION_OUTLINE_INTENSITY']
    
    # Algorithm used to calculate distance of sample points to feature points
    distance_metric: str = 'DISTANCE' # ['DISTANCE', 'DISTANCE_SQUARED', 'MANHATTAN', 'CHEBYCHEV', 'MINKOVSKY_HALF', 'MINKOVSKY_FOUR', 'MINKOVSKY']
    
    # Minkowski exponent
    minkovsky_exponent: float
    
    # Size of derivative offset used for calculating normal
    nabla: float
    
    # Scales the intensity of the noise
    noise_intensity: float
    
    # Scaling for noise input
    noise_scale: float
    
    # Voronoi feature weight 1
    weight_1: float
    
    # Voronoi feature weight 2
    weight_2: float
    
    # Voronoi feature weight 3
    weight_3: float
    
    # Voronoi feature weight 4
    weight_4: float
    
    # Materials that use this texture
    users_material: Any
    
    # Object modifiers that use this texture
    users_object_modifier: Any

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...