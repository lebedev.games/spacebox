from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CurveMapping(bpy_struct):
    
    # For RGB curves, the color that black is mapped to
    black_level: Tuple[float, float, float]
    
    clip_max_x: float
    
    clip_max_y: float
    
    clip_min_x: float
    
    clip_min_y: float
    
    curves: List[CurveMap]
    
    # Extrapolate the curve or extend it horizontally
    extend: str = 'HORIZONTAL' # ['HORIZONTAL', 'EXTRAPOLATED']
    
    # Tone of the curve
    tone: str = 'STANDARD' # ['STANDARD', 'FILMLIKE']
    
    # Force the curve view to fit a defined boundary
    use_clip: bool
    
    # For RGB curves, the color that white is mapped to
    white_level: Tuple[float, float, float]

    def update(self, *args, **kwargs) -> None:
        ...

    def reset_view(self, *args, **kwargs) -> None:
        ...

    def initialize(self, *args, **kwargs) -> None:
        ...

    def evaluate(self, *args, **kwargs) -> float:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...