from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class FieldSettings(bpy_struct):
    
    # Affect particle’s location
    apply_to_location: bool
    
    # Affect particle’s dynamic rotation
    apply_to_rotation: bool
    
    # Maximum distance for the field to work
    distance_max: float
    
    # Minimum distance for the field’s fall-off
    distance_min: float
    
    # How quickly strength falls off with distance from the force field
    falloff_power: float
    
    falloff_type: str = 'SPHERE' # ['SPHERE', 'TUBE', 'CONE']
    
    # Convert effector force into air flow velocity
    flow: float
    
    # Amount of clumping
    guide_clump_amount: float
    
    # Shape of clumping
    guide_clump_shape: float
    
    # Guide-free time from particle life’s end
    guide_free: float
    
    # The amplitude of the offset
    guide_kink_amplitude: float
    
    # Which axis to use for offset
    guide_kink_axis: str = 'X' # ['X', 'Y', 'Z']
    
    # The frequency of the offset (1/total length)
    guide_kink_frequency: float
    
    # Adjust the offset to the beginning/end
    guide_kink_shape: float
    
    # Type of periodic offset on the curve
    guide_kink_type: str = 'NONE' # ['NONE', 'CURL', 'RADIAL', 'WAVE', 'BRAID', 'ROTATION', 'ROLL']
    
    # The distance from which particles are affected fully
    guide_minimum: float
    
    # Damping of the harmonic force
    harmonic_damping: float
    
    # Inwards component of the vortex force
    inflow: float
    
    # Drag component proportional to velocity
    linear_drag: float
    
    # Amount of noise for the force strength
    noise: float
    
    # Drag component proportional to the square of velocity
    quadratic_drag: float
    
    # Radial falloff power (real gravitational falloff = 2)
    radial_falloff: float
    
    # Maximum radial distance for the field to work
    radial_max: float
    
    # Minimum radial distance for the field’s fall-off
    radial_min: float
    
    # Rest length of the harmonic force
    rest_length: float
    
    # Seed of the noise
    seed: int
    
    # Which direction is used to calculate the effector force
    shape: str = 'POINT' # ['POINT', 'LINE', 'PLANE', 'SURFACE', 'POINTS']
    
    # Size of the turbulence
    size: float
    
    # Select domain object of the smoke simulation
    source_object: Object
    
    # Strength of force field
    strength: float
    
    # Texture to use as force
    texture: Texture
    
    # How the texture effect is calculated (RGB and Curl need a RGB texture, else Gradient will be used instead)
    texture_mode: str = 'RGB' # ['RGB', 'GRADIENT', 'CURL']
    
    # Defines size of derivative offset used for calculating gradient and curl
    texture_nabla: float
    
    # Type of field
    type: str = 'NONE' # ['NONE', 'FORCE', 'WIND', 'VORTEX', 'MAGNET', 'HARMONIC', 'CHARGE', 'LENNARDJ', 'TEXTURE', 'GUIDE', 'BOID', 'TURBULENCE', 'DRAG', 'FLUID_FLOW']
    
    # Apply force only in 2D
    use_2d_force: bool
    
    # Force gets absorbed by collision objects
    use_absorption: bool
    
    # Use effector/global coordinates for turbulence
    use_global_coords: bool
    
    # Multiply force by 1/distance²
    use_gravity_falloff: bool
    
    # Based on distance/falloff it adds a portion of the entire path
    use_guide_path_add: bool
    
    # Use curve weights to influence the particle influence along the curve
    use_guide_path_weight: bool
    
    # Use a maximum distance for the field to work
    use_max_distance: bool
    
    # Use a minimum distance for the field’s fall-off
    use_min_distance: bool
    
    # Every point is effected by multiple springs
    use_multiple_springs: bool
    
    # Use object/global coordinates for texture
    use_object_coords: bool
    
    # Use a maximum radial distance for the field to work
    use_radial_max: bool
    
    # Use a minimum radial distance for the field’s fall-off
    use_radial_min: bool
    
    # Texture coordinates from root particle locations
    use_root_coords: bool
    
    # Adjust force strength based on smoke density
    use_smoke_density: bool
    
    # How much the force is reduced when acting parallel to a surface, e.g. cloth
    wind_factor: float
    
    # Effect in full or only positive/negative Z direction
    z_direction: str = 'BOTH' # ['BOTH', 'POSITIVE', 'NEGATIVE']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...