from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class AreaSpaces(bpy_struct):
    
    # Space currently being displayed in this area
    active: Space

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...