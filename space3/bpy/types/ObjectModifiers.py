from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ObjectModifiers(bpy_struct, List[Modifier]):
    
    # The active modifier in the list
    active: Modifier

    def new(self, *args, **kwargs) -> Modifier:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    def clear(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...