from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ImagePaint(Paint):
    
    # Image used as canvas
    canvas: Image
    
    # Image used as clone source
    clone_image: Image
    
    # Amount of dithering when painting on byte images
    dither: float
    
    # Texture filtering type
    interpolation: str = 'LINEAR' # ['LINEAR', 'CLOSEST']
    
    # Invert the stencil layer
    invert_stencil: bool
    
    # The mesh is missing materials
    missing_materials: bool
    
    # Image Painting does not have a stencil
    missing_stencil: bool
    
    # Image Painting does not have a texture to paint on
    missing_texture: bool
    
    # A UV layer is missing on the mesh
    missing_uvs: bool
    
    # Mode of operation for projection painting
    mode: str = 'MATERIAL' # ['MATERIAL', 'IMAGE']
    
    # Paint most on faces pointing towards the view according to this angle
    normal_angle: int
    
    # Size to capture the image for re-projecting
    screen_grab_size: Tuple[int, int]
    
    # Extend paint beyond the faces UVs to reduce seams (in pixels, slower)
    seam_bleed: int
    
    # Stencil color in the viewport
    stencil_color: Tuple[float, float, float]
    
    # Image used as stencil
    stencil_image: Image
    
    # Ignore faces pointing away from the view (faster)
    use_backface_culling: bool
    
    # Use another UV map as clone source, otherwise use the 3D cursor as the source
    use_clone_layer: bool
    
    # Paint most on faces pointing towards the view
    use_normal_falloff: bool
    
    # Only paint onto the faces directly under the brush (slower)
    use_occlude: bool
    
    # Set the mask layer from the UV map buttons
    use_stencil_layer: bool

    def detect_data(self, *args, **kwargs) -> bool:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...