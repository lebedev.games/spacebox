from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CopyScaleConstraint(Constraint):
    
    # Raise the target’s scale to the specified power
    power: float
    
    # Armature bone, mesh or lattice vertex group, …
    subtarget: str
    
    # Target object
    target: Object
    
    # Use addition instead of multiplication to combine scale (2.7 compatibility)
    use_add: bool
    
    # Redistribute the copied change in volume equally between the three axes of the owner
    use_make_uniform: bool
    
    # Combine original scale with copied scale
    use_offset: bool
    
    # Copy the target’s X scale
    use_x: bool
    
    # Copy the target’s Y scale
    use_y: bool
    
    # Copy the target’s Z scale
    use_z: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...