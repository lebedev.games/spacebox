from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Sculpt(Paint):
    
    # Maximum edge length for dynamic topology sculpting (as divisor of blender unit - higher value means smaller edge length)
    constant_detail_resolution: float
    
    # Maximum edge length for dynamic topology sculpting (in brush percenage)
    detail_percent: float
    
    # In dynamic-topology mode, how to add or remove mesh detail
    detail_refine_method: str = 'SUBDIVIDE' # ['SUBDIVIDE', 'COLLAPSE', 'SUBDIVIDE_COLLAPSE']
    
    # Maximum edge length for dynamic topology sculpting (in pixels)
    detail_size: float
    
    # In dynamic-topology mode, how mesh detail size is calculated
    detail_type_method: str = 'RELATIVE' # ['RELATIVE', 'CONSTANT', 'BRUSH', 'MANUAL']
    
    # Amount of gravity after each dab
    gravity: float
    
    # Object whose Z axis defines orientation of gravity
    gravity_object: Object
    
    # Disallow changes to the X axis of vertices
    lock_x: bool
    
    # Disallow changes to the Y axis of vertices
    lock_y: bool
    
    # Disallow changes to the Z axis of vertices
    lock_z: bool
    
    # Number of times to copy strokes across the surface
    radial_symmetry: Tuple[int, int, int]
    
    # Show Face Sets as overlay on object
    show_face_sets: bool
    
    # Show mask as overlay on object
    show_mask: bool
    
    # Source and destination for symmetrize operator
    symmetrize_direction: str = 'NEGATIVE_X' # ['NEGATIVE_X', 'POSITIVE_X', 'NEGATIVE_Y', 'POSITIVE_Y', 'NEGATIVE_Z', 'POSITIVE_Z']
    
    # Do not affect non manifold boundary edges
    use_automasking_boundary_edges: bool
    
    # Do not affect vertices that belong to a Face Set boundary
    use_automasking_boundary_face_sets: bool
    
    # Affect only vertices that share Face Sets with the active vertex
    use_automasking_face_sets: bool
    
    # Affect only vertices connected to the active vertex under the brush
    use_automasking_topology: bool
    
    # Use only deformation modifiers (temporary disable all constructive modifiers except multi-resolution)
    use_deform_only: bool
    
    # Show faces in dynamic-topology mode with smooth shading rather than flat shaded
    use_smooth_shading: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...