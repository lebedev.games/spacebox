from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class StudioLight(bpy_struct):
    
    # Studio light image file has separate “diffuse” and “specular” passes
    has_specular_highlight_pass: bool
    
    index: int
    
    is_user_defined: bool
    
    # Color of the ambient light that uniformly lit the scene
    light_ambient: Tuple[float, float, float]
    
    name: str
    
    path: str
    
    # Path where the irradiance cache is stored
    path_irr_cache: str
    
    # Path where the spherical harmonics cache is stored
    path_sh_cache: str
    
    # Lights user to display objects in solid draw mode
    solid_lights: List[UserSolidLight]
    
    spherical_harmonics_coefficients: List[float]
    
    type: str = 'STUDIO' # ['STUDIO', 'WORLD', 'MATCAP']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...