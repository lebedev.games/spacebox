from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class View3DShading(bpy_struct):
    
    # Name of the active Shader AOV
    aov_name: str
    
    # Color for custom background color
    background_color: Tuple[float, float, float]
    
    # Way to display the background
    background_type: str = 'THEME' # ['THEME', 'WORLD', 'VIEWPORT']
    
    # Factor for the cavity ridges
    cavity_ridge_factor: float
    
    # Way to display the cavity shading
    cavity_type: str = 'SCREEN' # ['WORLD', 'SCREEN', 'BOTH']
    
    # Factor for the cavity valleys
    cavity_valley_factor: float
    
    # Color Type
    color_type: str = 'MATERIAL' # ['MATERIAL', 'SINGLE', 'OBJECT', 'RANDOM', 'VERTEX', 'TEXTURE']
    
    # Factor for the curvature ridges
    curvature_ridge_factor: float
    
    # Factor for the curvature valleys
    curvature_valley_factor: float
    
    cycles: Any
    
    # Lighting Method for Solid/Texture Viewport Shading
    light: str = 'STUDIO' # ['STUDIO', 'MATCAP', 'FLAT']
    
    # Color for object outline
    object_outline_color: Tuple[float, float, float]
    
    # Render Pass to show in the viewport
    render_pass: str = 'COMBINED' # ['COMBINED', 'EMISSION', 'ENVIRONMENT', 'AO', 'SHADOW', 'DIFFUSE_LIGHT', 'DIFFUSE_COLOR', 'SPECULAR_LIGHT', 'SPECULAR_COLOR', 'VOLUME_LIGHT', 'BLOOM', 'NORMAL', 'MIST', 'AOV']
    
    # Selected StudioLight
    selected_studio_light: StudioLight
    
    # Darkness of shadows
    shadow_intensity: float
    
    # Use back face culling to hide the back side of faces
    show_backface_culling: bool
    
    # Show Cavity
    show_cavity: bool
    
    # Show Object Outline
    show_object_outline: bool
    
    # Show Shadow
    show_shadows: bool
    
    # Render specular highlights
    show_specular_highlight: bool
    
    # Show whole scene transparent
    show_xray: bool
    
    # Show whole scene transparent
    show_xray_wireframe: bool
    
    # Color for single color mode
    single_color: Tuple[float, float, float]
    
    # Studio lighting setup
    studio_light: str = 'DEFAULT' # ['DEFAULT']
    
    # Show the studiolight in the background
    studiolight_background_alpha: float
    
    # Blur the studiolight in the background
    studiolight_background_blur: float
    
    # Strength of the studiolight
    studiolight_intensity: float
    
    # Rotation of the studiolight around the Z-Axis
    studiolight_rotate_z: float
    
    # Method to display/shade objects in the 3D View
    type: str = 'SOLID' # ['WIREFRAME', 'SOLID', 'MATERIAL', 'RENDERED']
    
    # Use depth of field on viewport using the values from the active camera
    use_dof: bool
    
    # Render lights and light probes of the scene
    use_scene_lights: bool
    
    # Render lights and light probes of the scene
    use_scene_lights_render: bool
    
    # Use scene world for lighting
    use_scene_world: bool
    
    # Use scene world for lighting
    use_scene_world_render: bool
    
    # Make the HDR rotation fixed and not follow the camera
    use_studiolight_view_rotation: bool
    
    # Make the lighting fixed and not follow the camera
    use_world_space_lighting: bool
    
    # Color Type
    wireframe_color_type: str = 'MATERIAL' # ['MATERIAL', 'SINGLE', 'OBJECT', 'RANDOM', 'VERTEX', 'TEXTURE']
    
    # Amount of alpha to use
    xray_alpha: float
    
    # Amount of alpha to use
    xray_alpha_wireframe: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...