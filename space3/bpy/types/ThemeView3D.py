from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ThemeView3D(bpy_struct):
    
    act_spline: Tuple[float, float, float]
    
    # Shade for bones corresponding to a locked weight group during painting
    bone_locked_weight: Tuple[float, float, float, float]
    
    bone_pose: Tuple[float, float, float]
    
    bone_pose_active: Tuple[float, float, float]
    
    bone_solid: Tuple[float, float, float]
    
    bundle_solid: Tuple[float, float, float]
    
    camera: Tuple[float, float, float]
    
    camera_path: Tuple[float, float, float]
    
    clipping_border_3d: Tuple[float, float, float, float]
    
    edge_bevel: Tuple[float, float, float]
    
    edge_crease: Tuple[float, float, float]
    
    edge_facesel: Tuple[float, float, float]
    
    edge_seam: Tuple[float, float, float]
    
    edge_select: Tuple[float, float, float]
    
    edge_sharp: Tuple[float, float, float]
    
    editmesh_active: Tuple[float, float, float, float]
    
    empty: Tuple[float, float, float]
    
    extra_edge_angle: Tuple[float, float, float]
    
    extra_edge_len: Tuple[float, float, float]
    
    extra_face_angle: Tuple[float, float, float]
    
    extra_face_area: Tuple[float, float, float]
    
    face: Tuple[float, float, float, float]
    
    face_back: Tuple[float, float, float, float]
    
    face_dot: Tuple[float, float, float]
    
    face_front: Tuple[float, float, float, float]
    
    face_select: Tuple[float, float, float, float]
    
    facedot_size: int
    
    frame_current: Tuple[float, float, float]
    
    freestyle_edge_mark: Tuple[float, float, float]
    
    freestyle_face_mark: Tuple[float, float, float, float]
    
    gp_vertex: Tuple[float, float, float]
    
    gp_vertex_select: Tuple[float, float, float]
    
    gp_vertex_size: int
    
    grid: Tuple[float, float, float, float]
    
    handle_align: Tuple[float, float, float]
    
    handle_auto: Tuple[float, float, float]
    
    handle_free: Tuple[float, float, float]
    
    handle_sel_align: Tuple[float, float, float]
    
    handle_sel_auto: Tuple[float, float, float]
    
    handle_sel_free: Tuple[float, float, float]
    
    handle_sel_vect: Tuple[float, float, float]
    
    handle_vect: Tuple[float, float, float]
    
    lastsel_point: Tuple[float, float, float]
    
    light: Tuple[float, float, float, float]
    
    normal: Tuple[float, float, float]
    
    nurb_sel_uline: Tuple[float, float, float]
    
    nurb_sel_vline: Tuple[float, float, float]
    
    nurb_uline: Tuple[float, float, float]
    
    nurb_vline: Tuple[float, float, float]
    
    object_active: Tuple[float, float, float]
    
    # Diameter in pixels for object/light origin display
    object_origin_size: int
    
    object_selected: Tuple[float, float, float]
    
    outline_width: int
    
    paint_curve_handle: Tuple[float, float, float, float]
    
    paint_curve_pivot: Tuple[float, float, float, float]
    
    skin_root: Tuple[float, float, float]
    
    # Settings for space
    space: ThemeSpaceGradient
    
    speaker: Tuple[float, float, float]
    
    split_normal: Tuple[float, float, float]
    
    # Color for indicating Grease Pencil keyframes
    text_grease_pencil: Tuple[float, float, float]
    
    # Color for indicating object keyframes
    text_keyframe: Tuple[float, float, float]
    
    transform: Tuple[float, float, float]
    
    vertex: Tuple[float, float, float]
    
    vertex_active: Tuple[float, float, float]
    
    vertex_bevel: Tuple[float, float, float]
    
    vertex_normal: Tuple[float, float, float]
    
    vertex_select: Tuple[float, float, float]
    
    vertex_size: int
    
    vertex_unreferenced: Tuple[float, float, float]
    
    view_overlay: Tuple[float, float, float]
    
    wire: Tuple[float, float, float]
    
    # Color for wireframe when in edit mode, but edge selection is active
    wire_edit: Tuple[float, float, float]

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...