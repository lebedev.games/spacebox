from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class NodeGroupOutput(NodeInternal):
    
    # Interface socket data
    interface: PropertyGroup
    
    # True if this node is used as the active group output
    is_active_output: bool

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...