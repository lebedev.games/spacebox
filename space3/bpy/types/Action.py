from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Action(ID):
    
    # The individual F-Curves that make up the action
    fcurves: ActionFCurves
    
    # The final frame range of all F-Curves within this action
    frame_range: Tuple[float, float]
    
    # Convenient groupings of F-Curves
    groups: ActionGroups
    
    # Type of ID block that action can be used on - DO NOT CHANGE UNLESS YOU KNOW WHAT YOU ARE DOING
    id_root: str = 'ACTION' # ['ACTION', 'ARMATURE', 'BRUSH', 'CAMERA', 'CACHEFILE', 'CURVE', 'FONT', 'GREASEPENCIL', 'COLLECTION', 'IMAGE', 'KEY', 'LIGHT', 'LIBRARY', 'LINESTYLE', 'LATTICE', 'MASK', 'MATERIAL', 'META', 'MESH', 'MOVIECLIP', 'NODETREE', 'OBJECT', 'PAINTCURVE', 'PALETTE', 'PARTICLE', 'LIGHT_PROBE', 'SCENE', 'SIMULATION', 'SOUND', 'SPEAKER', 'TEXT', 'TEXTURE', 'HAIR', 'POINTCLOUD', 'VOLUME', 'WINDOWMANAGER', 'WORLD', 'WORKSPACE']
    
    # Markers specific to this action, for labeling poses
    pose_markers: ActionPoseMarkers

    def flip_with_pose(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...