from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class BlendTexture(Texture):
    
    # Style of the color blending
    progression: str = 'LINEAR' # ['LINEAR', 'QUADRATIC', 'EASING', 'DIAGONAL', 'SPHERICAL', 'QUADRATIC_SPHERE', 'RADIAL']
    
    # Flip the texture’s X and Y axis
    use_flip_axis: str = 'HORIZONTAL' # ['HORIZONTAL', 'VERTICAL']
    
    # Materials that use this texture
    users_material: Any
    
    # Object modifiers that use this texture
    users_object_modifier: Any

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...