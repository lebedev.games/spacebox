from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class FCurve(bpy_struct):
    
    # Index to the specific property affected by F-Curve if applicable
    array_index: int
    
    # Algorithm used to compute automatic handles
    auto_smoothing: str = 'NONE' # ['NONE', 'CONT_ACCEL']
    
    # Color of the F-Curve in the Graph Editor
    color: Tuple[float, float, float]
    
    # Method used to determine color of F-Curve in Graph Editor
    color_mode: str = 'AUTO_RAINBOW' # ['AUTO_RAINBOW', 'AUTO_RGB', 'AUTO_YRGB', 'CUSTOM']
    
    # RNA Path to property affected by F-Curve
    data_path: str
    
    # Channel Driver (only set for Driver F-Curves)
    driver: Driver
    
    # Method used for evaluating value of F-Curve outside first and last keyframes
    extrapolation: str = 'CONSTANT' # ['CONSTANT', 'LINEAR']
    
    # Action Group that this F-Curve belongs to
    group: ActionGroup
    
    # F-Curve and its keyframes are hidden in the Graph Editor graphs
    hide: bool
    
    # True if the curve contributes no animation due to lack of keyframes or useful modifiers, and should be deleted
    is_empty: bool
    
    # False when F-Curve could not be evaluated in past, so should be skipped when evaluating
    is_valid: bool
    
    # User-editable keyframes
    keyframe_points: FCurveKeyframePoints
    
    # F-Curve’s settings cannot be edited
    lock: bool
    
    # Modifiers affecting the shape of the F-Curve
    modifiers: FCurveModifiers
    
    # Disable F-Curve Modifier evaluation
    mute: bool
    
    # Sampled animation data
    sampled_points: List[FCurveSample]
    
    # F-Curve is selected for editing
    select: bool

    def evaluate(self, *args, **kwargs) -> float:
        ...

    def update(self, *args, **kwargs) -> None:
        ...

    def range(self, *args, **kwargs) -> None:
        ...

    def update_autoflags(self, *args, **kwargs) -> None:
        ...

    def convert_to_samples(self, *args, **kwargs) -> None:
        ...

    def convert_to_keyframes(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...