from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ThemeWidgetStateColors(bpy_struct):
    
    blend: float
    
    inner_anim: Tuple[float, float, float]
    
    inner_anim_sel: Tuple[float, float, float]
    
    inner_changed: Tuple[float, float, float]
    
    inner_changed_sel: Tuple[float, float, float]
    
    inner_driven: Tuple[float, float, float]
    
    inner_driven_sel: Tuple[float, float, float]
    
    inner_key: Tuple[float, float, float]
    
    inner_key_sel: Tuple[float, float, float]
    
    inner_overridden: Tuple[float, float, float]
    
    inner_overridden_sel: Tuple[float, float, float]

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...