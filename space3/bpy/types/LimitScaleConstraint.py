from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class LimitScaleConstraint(Constraint):
    
    # Highest X value to allow
    max_x: float
    
    # Highest Y value to allow
    max_y: float
    
    # Highest Z value to allow
    max_z: float
    
    # Lowest X value to allow
    min_x: float
    
    # Lowest Y value to allow
    min_y: float
    
    # Lowest Z value to allow
    min_z: float
    
    # Use the maximum X value
    use_max_x: bool
    
    # Use the maximum Y value
    use_max_y: bool
    
    # Use the maximum Z value
    use_max_z: bool
    
    # Use the minimum X value
    use_min_x: bool
    
    # Use the minimum Y value
    use_min_y: bool
    
    # Use the minimum Z value
    use_min_z: bool
    
    # Transform tools are affected by this constraint as well
    use_transform_limit: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...