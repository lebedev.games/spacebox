from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SceneObjects(bpy_struct, List[Object]):

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...