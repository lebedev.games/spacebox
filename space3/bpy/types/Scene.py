from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Scene(ID):
    
    # Active movie clip used for constraints and viewport drawing
    active_clip: MovieClip
    
    # Animation data for this data-block
    animation_data: AnimData
    
    # Distance model for distance attenuation calculation
    audio_distance_model: str = 'NONE' # ['NONE', 'INVERSE', 'INVERSE_CLAMPED', 'LINEAR', 'LINEAR_CLAMPED', 'EXPONENT', 'EXPONENT_CLAMPED']
    
    # Pitch factor for Doppler effect calculation
    audio_doppler_factor: float
    
    # Speed of sound for Doppler effect calculation
    audio_doppler_speed: float
    
    # Audio volume
    audio_volume: float
    
    # Background set scene
    background_set: 'Scene'
    
    # Active camera, used for rendering the scene
    camera: Object
    
    # Scene master collection that objects and other collections in the scene
    collection: Collection
    
    cursor: View3DCursor
    
    # Cycles render settings
    cycles: Any
    
    # Cycles hair rendering settings
    cycles_curves: Any
    
    # Scene display settings for 3D viewport
    display: SceneDisplay
    
    # Settings of device saved image would be displayed on
    display_settings: ColorManagedDisplaySettings
    
    # Eevee settings for the scene
    eevee: SceneEEVEE
    
    # Current frame, to update animation data from python frame_set() instead
    frame_current: int
    
    # Current frame with subframe and time remapping applied
    frame_current_final: float
    
    # Final frame of the playback/rendering range
    frame_end: int
    
    frame_float: float
    
    # Alternative end frame for UI playback
    frame_preview_end: int
    
    # Alternative start frame for UI playback
    frame_preview_start: int
    
    # First frame of the playback/rendering range
    frame_start: int
    
    # Number of frames to skip forward while rendering/playing back each frame
    frame_step: int
    
    frame_subframe: float
    
    # Constant acceleration in a given direction
    gravity: Tuple[float, float, float]
    
    # Grease Pencil data-block used for annotations in the 3D view
    grease_pencil: GreasePencil
    
    # Grease Pencil settings for the scene
    grease_pencil_settings: SceneGpencil
    
    # Whether there is any action referenced by NLA being edited (strictly read-only)
    is_nla_tweakmode: bool
    
    # Absolute Keying Sets for this Scene
    keying_sets: KeyingSets
    
    # All Keying Sets available for use (Builtins and Absolute Keying Sets for this Scene)
    keying_sets_all: KeyingSetsAll
    
    # Don’t allow frame to be selected with mouse outside of frame range
    lock_frame_selection_to_range: bool
    
    # Compositing node tree
    node_tree: NodeTree
    
    objects: SceneObjects
    
    render: RenderSettings
    
    rigidbody_world: RigidBodyWorld
    
    safe_areas: DisplaySafeAreas
    
    sequence_editor: SequenceEditor
    
    # Settings of color space sequencer is working in
    sequencer_colorspace_settings: ColorManagedSequencerColorspaceSettings
    
    # Consider keyframes for active object and/or its selected bones only (in timeline and when jumping between keyframes)
    show_keys_from_selected_only: bool
    
    # Show current scene subframe and allow set it using interface tools
    show_subframe: bool
    
    # How to sync playback
    sync_mode: str = 'AUDIO_SYNC' # ['NONE', 'FRAME_DROP', 'AUDIO_SYNC']
    
    # Markers used in all timelines for the current scene
    timeline_markers: TimelineMarkers
    
    tool_settings: ToolSettings
    
    transform_orientation_slots: List[TransformOrientationSlot]
    
    # Unit editing settings
    unit_settings: UnitSettings
    
    # Play back of audio from Sequence Editor will be muted
    use_audio: bool
    
    # Play audio from Sequence Editor while scrubbing
    use_audio_scrub: bool
    
    # Use global gravity for all dynamics
    use_gravity: bool
    
    # Enable the compositing node tree
    use_nodes: bool
    
    # Use an alternative start/end frame range for animation playback and view renders
    use_preview_range: bool
    
    # User defined note for the render stamping
    use_stamp_note: str
    
    view_layers: ViewLayers
    
    # Color management settings applied on image before saving
    view_settings: ColorManagedViewSettings
    
    # World used for rendering the scene
    world: World

    def statistics(self, *args, **kwargs) -> str:
        ...

    def frame_set(self, *args, **kwargs) -> None:
        ...

    def uvedit_aspect(self, *args, **kwargs) -> Tuple[float, float]:
        ...

    def ray_cast(self, *args, **kwargs) -> None:
        ...

    def sequence_editor_create(self, *args, **kwargs) -> None:
        ...

    def sequence_editor_clear(self, *args, **kwargs) -> None:
        ...

    def alembic_export(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...