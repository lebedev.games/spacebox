from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class FModifierStepped(FModifier):
    
    # Frame that modifier’s influence ends (if applicable)
    frame_end: float
    
    # Reference number of frames before frames get held (use to get hold for ‘1-3’ vs ‘5-7’ holding patterns)
    frame_offset: float
    
    # Frame that modifier’s influence starts (if applicable)
    frame_start: float
    
    # Number of frames to hold each value
    frame_step: float
    
    # Restrict modifier to only act before its ‘end’ frame
    use_frame_end: bool
    
    # Restrict modifier to only act after its ‘start’ frame
    use_frame_start: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...