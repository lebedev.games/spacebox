from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class AnimVizMotionPaths(bpy_struct):
    
    # When calculating Bone Paths, use Head or Tips
    bake_location: str = 'TAILS' # ['HEADS', 'TAILS']
    
    # Number of frames to show after the current frame (only for ‘Around Current Frame’ Onion-skinning method)
    frame_after: int
    
    # Number of frames to show before the current frame (only for ‘Around Current Frame’ Onion-skinning method)
    frame_before: int
    
    # End frame of range of paths to display/calculate (not for ‘Around Current Frame’ Onion-skinning method)
    frame_end: int
    
    # Starting frame of range of paths to display/calculate (not for ‘Around Current Frame’ Onion-skinning method)
    frame_start: int
    
    # Number of frames between paths shown (not for ‘On Keyframes’ Onion-skinning method)
    frame_step: int
    
    # Are there any bone paths that will need updating (read-only)
    has_motion_paths: bool
    
    # Show frame numbers on Motion Paths
    show_frame_numbers: bool
    
    # For bone motion paths, search whole Action for keyframes instead of in group with matching name only (is slower)
    show_keyframe_action_all: bool
    
    # Emphasize position of keyframes on Motion Paths
    show_keyframe_highlight: bool
    
    # Show frame numbers of Keyframes on Motion Paths
    show_keyframe_numbers: bool
    
    # Type of range to show for Motion Paths
    type: str = 'RANGE' # ['CURRENT_FRAME', 'RANGE']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...