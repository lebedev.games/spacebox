from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class GPencilStrokePoint(bpy_struct):
    
    co: Tuple[float, float, float]
    
    # Pressure of tablet at point when drawing it
    pressure: float
    
    # Point is selected for viewport editing
    select: bool
    
    # Color intensity (alpha factor)
    strength: float
    
    # Internal UV factor
    uv_factor: float
    
    # Internal UV factor for filling
    uv_fill: Tuple[float, float]
    
    # Internal UV factor for dot mode
    uv_rotation: float
    
    # Color used to mix with point color to get final color
    vertex_color: Tuple[float, float, float, float]

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...