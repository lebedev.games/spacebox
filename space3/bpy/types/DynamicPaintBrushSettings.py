from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class DynamicPaintBrushSettings(bpy_struct):
    
    # Proximity falloff is applied inside the volume
    invert_proximity: bool
    
    # Paint alpha
    paint_alpha: float
    
    # Color of the paint
    paint_color: Tuple[float, float, float]
    
    # Maximum distance from brush to mesh surface to affect paint
    paint_distance: float
    
    # Color ramp used to define proximity falloff
    paint_ramp: ColorRamp
    
    paint_source: str = 'VOLUME' # ['PARTICLE_SYSTEM', 'POINT', 'DISTANCE', 'VOLUME_DISTANCE', 'VOLUME']
    
    # Paint wetness, visible in wetmap (some effects only affect wet paint)
    paint_wetness: float
    
    # The particle system to paint with
    particle_system: ParticleSystem
    
    # Proximity falloff type
    proximity_falloff: str = 'CONSTANT' # ['SMOOTH', 'CONSTANT', 'RAMP']
    
    # Ray direction to use for projection (if brush object is located in that direction it’s painted)
    ray_direction: str = 'CANVAS' # ['CANVAS', 'BRUSH', 'Z_AXIS']
    
    # Smooth falloff added after solid radius
    smooth_radius: float
    
    # Smudge effect strength
    smudge_strength: float
    
    # Radius that will be painted solid
    solid_radius: float
    
    # Only increase alpha value if paint alpha is higher than existing
    use_absolute_alpha: bool
    
    # Negate influence inside the volume
    use_negative_volume: bool
    
    # Erase / remove paint instead of adding it
    use_paint_erase: bool
    
    # Use radius from particle settings
    use_particle_radius: bool
    
    # Brush is projected to canvas from defined direction within brush proximity
    use_proximity_project: bool
    
    # Only read color ramp alpha
    use_proximity_ramp_alpha: bool
    
    # Make this brush to smudge existing paint as it moves
    use_smudge: bool
    
    # Multiply brush influence by velocity color ramp alpha
    use_velocity_alpha: bool
    
    # Replace brush color by velocity color ramp
    use_velocity_color: bool
    
    # Multiply brush intersection depth (displace, waves) by velocity ramp alpha
    use_velocity_depth: bool
    
    # Velocity considered as maximum influence (Blender units per frame)
    velocity_max: float
    
    # Color ramp used to define brush velocity effect
    velocity_ramp: ColorRamp
    
    # Maximum level of surface intersection used to influence waves (use 0.0 to disable)
    wave_clamp: float
    
    # Multiplier for wave influence of this brush
    wave_factor: float
    
    wave_type: str = 'DEPTH' # ['CHANGE', 'DEPTH', 'FORCE', 'REFLECT']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...