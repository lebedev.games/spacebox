from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class VolumeDisplay(bpy_struct):
    
    # Thickness of volume display in the viewport
    density: float
    
    # Interpolation method to use for volumes in solid mode
    interpolation_method: str = 'LINEAR' # ['LINEAR', 'CUBIC', 'CLOSEST']
    
    slice_axis: str = 'AUTO' # ['AUTO', 'X', 'Y', 'Z']
    
    # Position of the slice
    slice_depth: float
    
    # Perform a single slice of the domain object
    use_slice: bool
    
    # Amount of detail for wireframe display
    wireframe_detail: str = 'COARSE' # ['COARSE', 'FINE']
    
    # Type of wireframe display
    wireframe_type: str = 'NONE' # ['NONE', 'BOUNDS', 'BOXES', 'POINTS']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...