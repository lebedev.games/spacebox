from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class GPencilEditCurve(bpy_struct):
    
    # Curve data points
    curve_points: List[GPencilEditCurvePoint]
    
    # Curve is selected for viewport editing
    select: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...