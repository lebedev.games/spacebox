from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class VolumeDisplaceModifier(Modifier):
    
    # Strength of the displacement
    strength: float
    
    texture: Texture
    
    texture_map_mode: str = 'LOCAL' # ['LOCAL', 'GLOBAL', 'OBJECT']
    
    # Object to use for texture mapping
    texture_map_object: Object
    
    # Subtracted from the texture color to get a displacement vector
    texture_mid_level: Tuple[float, float, float]
    
    # Smaller values result in better performance but might cut off the volume
    texture_sample_radius: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...