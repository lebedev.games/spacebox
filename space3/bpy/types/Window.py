from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Window(bpy_struct):
    
    # Window height
    height: int
    
    # Active workspace and scene follow this window
    parent: 'Window'
    
    # Active scene to be edited in the window
    scene: Scene
    
    # Active workspace screen showing in the window
    screen: Screen
    
    # Settings for stereo 3D display
    stereo_3d_display: Stereo3dDisplay
    
    # The active workspace view layer showing in the window
    view_layer: ViewLayer
    
    # Window width
    width: int
    
    # Active workspace showing in the window
    workspace: WorkSpace
    
    # Horizontal location of the window
    x: int
    
    # Vertical location of the window
    y: int

    def cursor_warp(self, *args, **kwargs) -> None:
        ...

    def cursor_set(self, *args, **kwargs) -> None:
        ...

    def cursor_modal_set(self, *args, **kwargs) -> None:
        ...

    def cursor_modal_restore(self, *args, **kwargs) -> None:
        ...

    def event_simulate(self, *args, **kwargs) -> Event:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...