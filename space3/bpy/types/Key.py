from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Key(ID):
    
    # Animation data for this data-block
    animation_data: AnimData
    
    # Evaluation time for absolute shape keys
    eval_time: float
    
    # Shape keys
    key_blocks: List[ShapeKey]
    
    reference_key: ShapeKey
    
    # Make shape keys relative, otherwise play through shapes as a sequence using the evaluation time
    use_relative: bool
    
    # Data-block using these shape keys
    user: ID

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...