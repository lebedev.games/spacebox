from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class GlowSequence(EffectSequence):
    
    # Radius of glow effect
    blur_radius: float
    
    # Brightness multiplier
    boost_factor: float
    
    # Brightness limit of intensity
    clamp: float
    
    # First input for the effect strip
    input_1: Sequence
    
    input_count: int
    
    # Accuracy of the blur effect
    quality: int
    
    # Minimum intensity to trigger a glow
    threshold: float
    
    # Show the glow buffer only
    use_only_boost: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...