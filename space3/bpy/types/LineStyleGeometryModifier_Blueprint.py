from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class LineStyleGeometryModifier_Blueprint(LineStyleGeometryModifier):
    
    # Amount of backbone stretching
    backbone_length: float
    
    # True if the modifier tab is expanded
    expanded: bool
    
    # Name of the modifier
    name: str
    
    # Randomness of the backbone stretching
    random_backbone: int
    
    # Randomness of the center
    random_center: int
    
    # Randomness of the radius
    random_radius: int
    
    # Number of rounds in contour strokes
    rounds: int
    
    # Select the shape of blueprint contour strokes
    shape: str = 'CIRCLES' # ['CIRCLES', 'ELLIPSES', 'SQUARES']
    
    # Type of the modifier
    type: str = '2D_OFFSET' # ['2D_OFFSET', '2D_TRANSFORM', 'BACKBONE_STRETCHER', 'BEZIER_CURVE', 'BLUEPRINT', 'GUIDING_LINES', 'PERLIN_NOISE_1D', 'PERLIN_NOISE_2D', 'POLYGONIZATION', 'SAMPLING', 'SIMPLIFICATION', 'SINUS_DISPLACEMENT', 'SPATIAL_NOISE', 'TIP_REMOVER']
    
    # Enable or disable this modifier during stroke rendering
    use: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...