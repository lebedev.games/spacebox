from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class GPencilFrame(bpy_struct):
    
    # The frame on which this sketch appears
    frame_number: int
    
    # Frame is being edited (painted on)
    is_edited: bool
    
    # Type of keyframe
    keyframe_type: str = 'KEYFRAME' # ['KEYFRAME', 'BREAKDOWN', 'MOVING_HOLD', 'EXTREME', 'JITTER']
    
    # Frame is selected for editing in the Dope Sheet
    select: bool
    
    # Freehand curves defining the sketch on this frame
    strokes: GPencilStrokes

    def clear(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...