from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SpaceClipEditor(Space):
    
    # Where the annotation comes from
    annotation_source: str = 'CLIP' # ['CLIP', 'TRACK']
    
    # Movie clip displayed and edited in this space
    clip: MovieClip
    
    # Parameters defining which frame of the movie clip is displayed
    clip_user: MovieClipUser
    
    # Lock viewport to selected markers during playback
    lock_selection: bool
    
    # Lock curves view to time cursor during playback and tracking
    lock_time_cursor: bool
    
    # Mask displayed and edited in this space
    mask: Mask
    
    # Display type for mask splines
    mask_display_type: str = 'OUTLINE' # ['OUTLINE', 'DASH', 'BLACK', 'WHITE']
    
    # Overlay mode of rasterized mask
    mask_overlay_mode: str = 'ALPHACHANNEL' # ['ALPHACHANNEL', 'COMBINED']
    
    # Editing context being displayed
    mode: str = 'TRACKING' # ['TRACKING', 'MASK']
    
    # Length of displaying path, in frames
    path_length: int
    
    # Pivot center for rotation/scaling
    pivot_point: str = 'BOUNDING_BOX_CENTER' # ['BOUNDING_BOX_CENTER', 'CURSOR', 'INDIVIDUAL_ORIGINS', 'MEDIAN_POINT']
    
    # Scopes to visualize movie clip statistics
    scopes: MovieClipScopes
    
    # Show annotations for this view
    show_annotation: bool
    
    # Show blue channel in the frame
    show_blue_channel: bool
    
    # Show projection of 3D markers into footage
    show_bundles: bool
    
    # Show disabled tracks from the footage
    show_disabled: bool
    
    # Show filters for graph editor
    show_filters: bool
    
    # Show curve for per-frame average error (camera motion should be solved first)
    show_graph_frames: bool
    
    # Include channels from objects/bone that aren’t visible
    show_graph_hidden: bool
    
    # Only include channels relating to selected objects and data
    show_graph_only_selected: bool
    
    # Display the reprojection error curve for selected tracks
    show_graph_tracks_error: bool
    
    # Display the speed curves (in “x” direction red, in “y” direction green) for the selected tracks
    show_graph_tracks_motion: bool
    
    # Show green channel in the frame
    show_green_channel: bool
    
    # Show grid showing lens distortion
    show_grid: bool
    
    # Show pattern boundbox for markers
    show_marker_pattern: bool
    
    # Show search boundbox for markers
    show_marker_search: bool
    
    show_mask_overlay: bool
    
    show_mask_smooth: bool
    
    # Show metadata of clip
    show_metadata: bool
    
    # Show track names and status
    show_names: bool
    
    # Show red channel in the frame
    show_red_channel: bool
    
    show_region_hud: bool
    
    show_region_toolbar: bool
    
    show_region_ui: bool
    
    # Show timing in seconds not frames
    show_seconds: bool
    
    # Show stable footage in editor (if stabilization is enabled)
    show_stable: bool
    
    # Show markers in a more compact manner
    show_tiny_markers: bool
    
    # Show path of how track moves
    show_track_path: bool
    
    # Display frame in grayscale mode
    use_grayscale_preview: bool
    
    # Use manual calibration helpers
    use_manual_calibration: bool
    
    # Mute footage and show black background instead
    use_mute_footage: bool
    
    # Type of the clip editor view
    view: str = 'CLIP' # ['CLIP', 'GRAPH', 'DOPESHEET']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...

    def draw_handler_add(self, *args, **kwargs) -> Any:
        ...

    def draw_handler_remove(self, *args, **kwargs) -> None:
        ...