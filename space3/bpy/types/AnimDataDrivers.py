from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class AnimDataDrivers(bpy_struct):

    def new(self, *args, **kwargs) -> FCurve:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    def from_existing(self, *args, **kwargs) -> FCurve:
        ...

    def find(self, *args, **kwargs) -> FCurve:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...