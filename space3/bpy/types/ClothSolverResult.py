from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ClothSolverResult(bpy_struct):
    
    # Average error during substeps
    avg_error: float
    
    # Average iterations during substeps
    avg_iterations: float
    
    # Maximum error during substeps
    max_error: float
    
    # Maximum iterations during substeps
    max_iterations: int
    
    # Minimum error during substeps
    min_error: float
    
    # Minimum iterations during substeps
    min_iterations: int
    
    # Status of the solver iteration
    status: Set[str] # {'default {SUCCESS', 'NUMERICAL_ISSUE', 'INVALID_INPUT}', 'NO_CONVERGENCE', 'SUCCESS'}

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...