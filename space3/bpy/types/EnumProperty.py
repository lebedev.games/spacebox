from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class EnumProperty(Property):
    
    # Default value for this enum
    default: str = 'DUMMY' # ['DUMMY']
    
    # Default value for this enum
    default_flag: Set[str] # {'DUMMY}', 'default {'}
    
    # Possible values for the property
    enum_items: List[EnumPropertyItem]
    
    # Possible values for the property (never calls optional dynamic generation of those)
    enum_items_static: List[EnumPropertyItem]

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...