from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class UILayout(bpy_struct):
    
    # When true, buttons defined in popups will be activated on first display (use so you can type into a field without having to click on it first)
    activate_init: bool
    
    active: bool
    
    # When true, an operator button defined after this will be activated when pressing return(use with popup dialogs)
    active_default: bool
    
    alert: bool
    
    alignment: str = 'EXPAND' # ['EXPAND', 'LEFT', 'CENTER', 'RIGHT']
    
    direction: str = 'HORIZONTAL' # ['HORIZONTAL', 'VERTICAL']
    
    emboss: str = 'NORMAL' # ['NORMAL', 'NONE', 'PULLDOWN_MENU', 'RADIAL_MENU', 'UI_EMBOSS_NONE_OR_STATUS']
    
    # When false, this (sub)layout is grayed out
    enabled: bool
    
    operator_context: str = 'INVOKE_DEFAULT' # ['INVOKE_DEFAULT', 'INVOKE_REGION_WIN', 'INVOKE_REGION_CHANNELS', 'INVOKE_REGION_PREVIEW', 'INVOKE_AREA', 'INVOKE_SCREEN', 'EXEC_DEFAULT', 'EXEC_REGION_WIN', 'EXEC_REGION_CHANNELS', 'EXEC_REGION_PREVIEW', 'EXEC_AREA', 'EXEC_SCREEN']
    
    # Scale factor along the X for items in this (sub)layout
    scale_x: float
    
    # Scale factor along the Y for items in this (sub)layout
    scale_y: float
    
    # Fixed size along the X for items in this (sub)layout
    ui_units_x: float
    
    # Fixed size along the Y for items in this (sub)layout
    ui_units_y: float
    
    use_property_decorate: bool
    
    use_property_split: bool

    def row(self, *args, **kwargs) -> 'UILayout':
        ...

    def column(self, *args, **kwargs) -> 'UILayout':
        ...

    def column_flow(self, *args, **kwargs) -> 'UILayout':
        ...

    def grid_flow(self, *args, **kwargs) -> 'UILayout':
        ...

    def box(self, *args, **kwargs) -> None:
        ...

    def split(self, *args, **kwargs) -> 'UILayout':
        ...

    def menu_pie(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def icon(cls, *args, **kwargs) -> int:
        ...

    @classmethod
    def enum_item_name(cls, *args, **kwargs) -> str:
        ...

    @classmethod
    def enum_item_description(cls, *args, **kwargs) -> str:
        ...

    @classmethod
    def enum_item_icon(cls, *args, **kwargs) -> int:
        ...

    def prop(self, *args, **kwargs) -> None:
        ...

    def props_enum(self, *args, **kwargs) -> None:
        ...

    def prop_menu_enum(self, *args, **kwargs) -> None:
        ...

    def prop_with_popover(self, *args, **kwargs) -> None:
        ...

    def prop_with_menu(self, *args, **kwargs) -> None:
        ...

    def prop_tabs_enum(self, *args, **kwargs) -> None:
        ...

    def prop_enum(self, *args, **kwargs) -> None:
        ...

    def prop_search(self, *args, **kwargs) -> None:
        ...

    def prop_decorator(self, *args, **kwargs) -> None:
        ...

    def operator(self, *args, **kwargs) -> OperatorProperties:
        ...

    def operator_menu_hold(self, *args, **kwargs) -> OperatorProperties:
        ...

    def operator_enum(self, *args, **kwargs) -> None:
        ...

    def operator_menu_enum(self, *args, **kwargs) -> None:
        ...

    def label(self, *args, **kwargs) -> None:
        ...

    def menu(self, *args, **kwargs) -> None:
        ...

    def menu_contents(self, *args, **kwargs) -> None:
        ...

    def popover(self, *args, **kwargs) -> None:
        ...

    def popover_group(self, *args, **kwargs) -> None:
        ...

    def separator(self, *args, **kwargs) -> None:
        ...

    def separator_spacer(self, *args, **kwargs) -> None:
        ...

    def context_pointer_set(self, *args, **kwargs) -> None:
        ...

    def template_header(self, *args, **kwargs) -> None:
        ...

    def template_ID(self, *args, **kwargs) -> None:
        ...

    def template_ID_preview(self, *args, **kwargs) -> None:
        ...

    def template_any_ID(self, *args, **kwargs) -> None:
        ...

    def template_ID_tabs(self, *args, **kwargs) -> None:
        ...

    def template_search(self, *args, **kwargs) -> None:
        ...

    def template_search_preview(self, *args, **kwargs) -> None:
        ...

    def template_path_builder(self, *args, **kwargs) -> None:
        ...

    def template_modifiers(self, *args, **kwargs) -> None:
        ...

    def template_constraints(self, *args, **kwargs) -> None:
        ...

    def template_grease_pencil_modifiers(self, *args, **kwargs) -> None:
        ...

    def template_shaderfx(self, *args, **kwargs) -> None:
        ...

    def template_greasepencil_color(self, *args, **kwargs) -> None:
        ...

    def template_constraint_header(self, *args, **kwargs) -> None:
        ...

    def template_preview(self, *args, **kwargs) -> None:
        ...

    def template_curve_mapping(self, *args, **kwargs) -> None:
        ...

    def template_curveprofile(self, *args, **kwargs) -> None:
        ...

    def template_color_ramp(self, *args, **kwargs) -> None:
        ...

    def template_icon(self, *args, **kwargs) -> None:
        ...

    def template_icon_view(self, *args, **kwargs) -> None:
        ...

    def template_histogram(self, *args, **kwargs) -> None:
        ...

    def template_waveform(self, *args, **kwargs) -> None:
        ...

    def template_vectorscope(self, *args, **kwargs) -> None:
        ...

    def template_layers(self, *args, **kwargs) -> None:
        ...

    def template_color_picker(self, *args, **kwargs) -> None:
        ...

    def template_palette(self, *args, **kwargs) -> None:
        ...

    def template_image_layers(self, *args, **kwargs) -> None:
        ...

    def template_image(self, *args, **kwargs) -> None:
        ...

    def template_image_settings(self, *args, **kwargs) -> None:
        ...

    def template_image_stereo_3d(self, *args, **kwargs) -> None:
        ...

    def template_image_views(self, *args, **kwargs) -> None:
        ...

    def template_movieclip(self, *args, **kwargs) -> None:
        ...

    def template_track(self, *args, **kwargs) -> None:
        ...

    def template_marker(self, *args, **kwargs) -> None:
        ...

    def template_movieclip_information(self, *args, **kwargs) -> None:
        ...

    def template_list(self, *args, **kwargs) -> None:
        ...

    def template_running_jobs(self, *args, **kwargs) -> None:
        ...

    def template_operator_search(self, *args, **kwargs) -> None:
        ...

    def template_menu_search(self, *args, **kwargs) -> None:
        ...

    def template_header_3D_mode(self, *args, **kwargs) -> None:
        ...

    def template_edit_mode_selection(self, *args, **kwargs) -> None:
        ...

    def template_reports_banner(self, *args, **kwargs) -> None:
        ...

    def template_input_status(self, *args, **kwargs) -> None:
        ...

    def template_node_link(self, *args, **kwargs) -> None:
        ...

    def template_node_view(self, *args, **kwargs) -> None:
        ...

    def template_texture_user(self, *args, **kwargs) -> None:
        ...

    def template_keymap_item_properties(self, *args, **kwargs) -> None:
        ...

    def template_component_menu(self, *args, **kwargs) -> None:
        ...

    def template_colorspace_settings(self, *args, **kwargs) -> None:
        ...

    def template_colormanaged_view_settings(self, *args, **kwargs) -> None:
        ...

    def template_node_socket(self, *args, **kwargs) -> None:
        ...

    def template_cache_file(self, *args, **kwargs) -> None:
        ...

    def template_recent_files(self, *args, **kwargs) -> int:
        ...

    def template_file_select_path(self, *args, **kwargs) -> None:
        ...

    def template_event_from_keymap_item(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...