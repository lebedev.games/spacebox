from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class VertexGroup(bpy_struct):
    
    # Index number of the vertex group
    index: int
    
    # Maintain the relative weights for the group
    lock_weight: bool
    
    # Vertex group name
    name: str

    def add(self, *args, **kwargs) -> None:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    def weight(self, *args, **kwargs) -> float:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...