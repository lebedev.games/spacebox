from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MovieClipUser(bpy_struct):
    
    # Current frame number in movie or image sequence
    frame_current: int
    
    # Display preview using full resolution or different proxy resolutions
    proxy_render_size: str = 'FULL' # ['PROXY_25', 'PROXY_50', 'PROXY_75', 'PROXY_100', 'FULL']
    
    # Render preview using undistorted proxy
    use_render_undistorted: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...