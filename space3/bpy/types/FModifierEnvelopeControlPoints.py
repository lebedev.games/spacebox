from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class FModifierEnvelopeControlPoints(bpy_struct):

    def add(self, *args, **kwargs) -> FModifierEnvelopeControlPoint:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...