from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class HookModifier(Modifier):
    
    # Center of the hook, used for falloff and display
    center: Tuple[float, float, float]
    
    # Custom falloff curve
    falloff_curve: CurveMapping
    
    # If not zero, the distance from the hook where influence ends
    falloff_radius: float
    
    falloff_type: str = 'SMOOTH' # ['NONE', 'CURVE', 'SMOOTH', 'SPHERE', 'ROOT', 'INVERSE_SQUARE', 'SHARP', 'LINEAR', 'CONSTANT']
    
    # Invert vertex group influence
    invert_vertex_group: bool
    
    # Reverse the transformation between this object and its target
    matrix_inverse: List[float]
    
    # Parent Object for hook, also recalculates and clears offset
    object: Object
    
    # Relative force of the hook
    strength: float
    
    # Name of Parent Bone for hook (if applicable), also recalculates and clears offset
    subtarget: str
    
    # Compensate for non-uniform object scale
    use_falloff_uniform: bool
    
    # Name of Vertex Group which determines influence of modifier per point
    vertex_group: str
    
    # Indices of vertices bound to the modifier. For bezier curves, handles count as additional vertices
    vertex_indices: Tuple[int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int]

    def vertex_indices_set(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...