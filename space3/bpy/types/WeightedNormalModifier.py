from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class WeightedNormalModifier(Modifier):
    
    # Invert vertex group influence
    invert_vertex_group: bool
    
    # Keep sharp edges as computed for default split normals, instead of setting a single weighted normal for each vertex
    keep_sharp: bool
    
    # Weighted vertex normal mode to use
    mode: str = 'FACE_AREA' # ['FACE_AREA', 'CORNER_ANGLE', 'FACE_AREA_WITH_ANGLE']
    
    # Threshold value for different weights to be considered equal
    thresh: float
    
    # Use influence of face for weighting
    use_face_influence: bool
    
    # Vertex group name for modifying the selected areas
    vertex_group: str
    
    # Corrective factor applied to faces’ weights, 50 is neutral, lower values increase weight of weak faces, higher values increase weight of strong faces
    weight: int

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...