from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ParticleSettingsTextureSlots(bpy_struct):

    @classmethod
    def add(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def create(cls, *args, **kwargs) -> ParticleSettingsTextureSlot:
        ...

    @classmethod
    def clear(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...