from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ShaderFxWave(ShaderFx):
    
    # Amplitude of Wave
    amplitude: float
    
    # Direction of the wave
    orientation: str = 'HORIZONTAL' # ['HORIZONTAL', 'VERTICAL']
    
    # Period of Wave
    period: float
    
    # Phase Shift of Wave
    phase: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...