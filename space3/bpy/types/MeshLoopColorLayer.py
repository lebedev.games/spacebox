from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MeshLoopColorLayer(bpy_struct):
    
    # Sets the layer as active for display and editing
    active: bool
    
    # Sets the layer as active for rendering
    active_render: bool
    
    data: List[MeshLoopColor]
    
    # Name of Vertex color layer
    name: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...