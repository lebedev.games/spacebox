from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MeshUVLoopLayer(bpy_struct):
    
    # Set the map as active for display and editing
    active: bool
    
    # Set the map as active for cloning
    active_clone: bool
    
    # Set the map as active for rendering
    active_render: bool
    
    data: List[MeshUVLoop]
    
    # Name of UV map
    name: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...