from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MovieTrackingSettings(bpy_struct):
    
    # Cleanup action to execute
    clean_action: str = 'SELECT' # ['SELECT', 'DELETE_TRACK', 'DELETE_SEGMENTS']
    
    # Effect on tracks which have a larger re-projection error
    clean_error: float
    
    # Effect on tracks which are tracked less than the specified amount of frames
    clean_frames: int
    
    # Default minimum value of correlation between matched pattern and reference that is still treated as successful tracking
    default_correlation_min: float
    
    # Every tracking cycle, this number of frames are tracked
    default_frames_limit: int
    
    # Default distance from image boundary at which marker stops tracking
    default_margin: int
    
    # Default motion model to use for tracking
    default_motion_model: str = 'Loc' # ['Perspective', 'Affine', 'LocRotScale', 'LocScale', 'LocRot', 'Loc']
    
    # Track pattern from given frame when tracking marker to next frame
    default_pattern_match: str = 'KEYFRAME' # ['KEYFRAME', 'PREV_FRAME']
    
    # Size of pattern area for newly created tracks
    default_pattern_size: int
    
    # Size of search area for newly created tracks
    default_search_size: int
    
    # Influence of newly created track on a final solution
    default_weight: float
    
    # Distance between two bundles used for scene scaling
    distance: float
    
    # Distance between two bundles used for object scaling
    object_distance: float
    
    # Refine focal length during camera solving
    refine_intrinsics_focal_length: bool
    
    # Refine principal point during camera solving
    refine_intrinsics_principal_point: bool
    
    # Refine radial coefficients of distortion model during camera solving
    refine_intrinsics_radial_distortion: bool
    
    # Refine tangential coefficients of distortion model during camera solving
    refine_intrinsics_tangential_distortion: bool
    
    # Limit speed of tracking to make visual feedback easier (this does not affect the tracking quality)
    speed: str = 'FASTEST' # ['FASTEST', 'DOUBLE', 'REALTIME', 'HALF', 'QUARTER']
    
    # Use blue channel from footage for tracking
    use_default_blue_channel: bool
    
    # Use a brute-force translation-only initialization when tracking
    use_default_brute: bool
    
    # Use green channel from footage for tracking
    use_default_green_channel: bool
    
    # Use a grease pencil data-block as a mask to use only specified areas of pattern when tracking
    use_default_mask: bool
    
    # Normalize light intensities while tracking (slower)
    use_default_normalization: bool
    
    # Use red channel from footage for tracking
    use_default_red_channel: bool
    
    # Automatically select keyframes when solving camera/object motion
    use_keyframe_selection: bool
    
    # Use special solver to track a stable camera position, such as a tripod
    use_tripod_solver: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...