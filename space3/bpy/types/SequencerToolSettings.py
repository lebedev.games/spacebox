from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SequencerToolSettings(bpy_struct):
    
    # Scale fit method
    fit_method: str = 'FIT' # ['FIT', 'FILL', 'STRETCH', 'ORIGINAL']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...