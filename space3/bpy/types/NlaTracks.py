from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class NlaTracks(bpy_struct):
    
    # Active NLA Track
    active: NlaTrack

    def new(self, *args, **kwargs) -> NlaTrack:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...