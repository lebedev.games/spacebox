from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class FluidModifier(Modifier):
    
    domain_settings: FluidDomainSettings
    
    effector_settings: FluidEffectorSettings
    
    flow_settings: FluidFlowSettings
    
    fluid_type: str = 'NONE' # ['NONE', 'DOMAIN', 'FLOW', 'EFFECTOR']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...