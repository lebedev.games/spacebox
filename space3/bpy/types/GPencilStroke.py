from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class GPencilStroke(bpy_struct):
    
    aspect: Tuple[float, float]
    
    bound_box_max: Tuple[float, float, float]
    
    bound_box_min: Tuple[float, float, float]
    
    # Coordinate space that stroke is in
    display_mode: str = 'SCREEN' # ['SCREEN', '3DSPACE', '2DSPACE', '2DIMAGE']
    
    # Temporary data for Edit Curve
    edit_curve: GPencilEditCurve
    
    # Stroke end extreme cap style
    end_cap_mode: str = 'ROUND' # ['ROUND', 'FLAT']
    
    # Amount of gradient along section of stroke
    hardness: float
    
    # Stroke has Curve data to edit shape
    has_edit_curve: bool
    
    # Special stroke to use as boundary for filling areas
    is_nofill_stroke: bool
    
    # Thickness of stroke (in pixels)
    line_width: int
    
    # Index of material used in this stroke
    material_index: int
    
    # Stroke data points
    points: GPencilStrokePoints
    
    # Stroke is selected for viewport editing
    select: bool
    
    # Index of selection used for interpolation
    select_index: int
    
    # Stroke start extreme cap style
    start_cap_mode: str = 'ROUND' # ['ROUND', 'FLAT']
    
    # Triangulation data for HQ fill
    triangles: List[GPencilTriangle]
    
    # Enable cyclic drawing, closing the stroke
    use_cyclic: bool
    
    # Rotation of the UV
    uv_rotation: float
    
    # Scale of the UV
    uv_scale: float
    
    # Translation of default UV position
    uv_translation: Tuple[float, float]
    
    # Color used to mix with fill color to get final color
    vertex_color_fill: Tuple[float, float, float, float]

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...