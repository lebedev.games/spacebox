from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ShaderNodeTexWave(ShaderNode):
    
    bands_direction: str = 'X' # ['X', 'Y', 'Z', 'DIAGONAL']
    
    # Color mapping settings
    color_mapping: ColorMapping
    
    rings_direction: str = 'X' # ['X', 'Y', 'Z', 'SPHERICAL']
    
    # Texture coordinate mapping settings
    texture_mapping: TexMapping
    
    wave_profile: str = 'SIN' # ['SIN', 'SAW', 'TRI']
    
    wave_type: str = 'BANDS' # ['BANDS', 'RINGS']

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...