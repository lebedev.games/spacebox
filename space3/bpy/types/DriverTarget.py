from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class DriverTarget(bpy_struct):
    
    # Name of PoseBone to use as target
    bone_target: str
    
    # RNA Path (from ID-block) to property used
    data_path: str
    
    # ID-block that the specific property used can be found from (id_type property must be set first)
    id: ID
    
    # Type of ID-block that can be used
    id_type: str = 'OBJECT' # ['ACTION', 'ARMATURE', 'BRUSH', 'CAMERA', 'CACHEFILE', 'CURVE', 'FONT', 'GREASEPENCIL', 'COLLECTION', 'IMAGE', 'KEY', 'LIGHT', 'LIBRARY', 'LINESTYLE', 'LATTICE', 'MASK', 'MATERIAL', 'META', 'MESH', 'MOVIECLIP', 'NODETREE', 'OBJECT', 'PAINTCURVE', 'PALETTE', 'PARTICLE', 'LIGHT_PROBE', 'SCENE', 'SIMULATION', 'SOUND', 'SPEAKER', 'TEXT', 'TEXTURE', 'HAIR', 'POINTCLOUD', 'VOLUME', 'WINDOWMANAGER', 'WORLD', 'WORKSPACE']
    
    # Mode for calculating rotation channel values
    rotation_mode: str = 'AUTO' # ['AUTO', 'XYZ', 'XZY', 'YXZ', 'YZX', 'ZXY', 'ZYX', 'QUATERNION', 'SWING_TWIST_X', 'SWING_TWIST_Y', 'SWING_TWIST_Z']
    
    # Space in which transforms are used
    transform_space: str = 'WORLD_SPACE' # ['WORLD_SPACE', 'TRANSFORM_SPACE', 'LOCAL_SPACE']
    
    # Driver variable type
    transform_type: str = 'LOC_X' # ['LOC_X', 'LOC_Y', 'LOC_Z', 'ROT_X', 'ROT_Y', 'ROT_Z', 'ROT_W', 'SCALE_X', 'SCALE_Y', 'SCALE_Z', 'SCALE_AVG']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...