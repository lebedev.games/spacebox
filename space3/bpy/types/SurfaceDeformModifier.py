from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SurfaceDeformModifier(Modifier):
    
    # Controls how much nearby polygons influence deformation
    falloff: float
    
    # Invert vertex group influence
    invert_vertex_group: bool
    
    # Whether geometry has been bound to target mesh
    is_bound: bool
    
    # Strength of modifier deformations
    strength: float
    
    # Mesh object to deform with
    target: Object
    
    # Vertex group name for selecting/weighting the affected areas
    vertex_group: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...