from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class LineStyleGeometryModifier_2DOffset(LineStyleGeometryModifier):
    
    # Displacement that is applied from the end of the stroke
    end: float
    
    # True if the modifier tab is expanded
    expanded: bool
    
    # Name of the modifier
    name: str
    
    # Displacement that is applied from the beginning of the stroke
    start: float
    
    # Type of the modifier
    type: str = '2D_OFFSET' # ['2D_OFFSET', '2D_TRANSFORM', 'BACKBONE_STRETCHER', 'BEZIER_CURVE', 'BLUEPRINT', 'GUIDING_LINES', 'PERLIN_NOISE_1D', 'PERLIN_NOISE_2D', 'POLYGONIZATION', 'SAMPLING', 'SIMPLIFICATION', 'SINUS_DISPLACEMENT', 'SPATIAL_NOISE', 'TIP_REMOVER']
    
    # Enable or disable this modifier during stroke rendering
    use: bool
    
    # Displacement that is applied to the X coordinates of stroke vertices
    x: float
    
    # Displacement that is applied to the Y coordinates of stroke vertices
    y: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...