from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Pose(bpy_struct):
    
    # Animation data for this data-block
    animation_visualization: AnimViz
    
    # Groups of the bones
    bone_groups: BoneGroups
    
    # Individual pose bones for the armature
    bones: List[PoseBone]
    
    # Parameters for IK solver
    ik_param: IKParam
    
    # Selection of IK solver for IK chain
    ik_solver: str = 'LEGACY' # ['LEGACY', 'ITASC']
    
    # Add temporary IK constraints while grabbing bones in Pose Mode
    use_auto_ik: bool
    
    # Apply relative transformations in X-mirror mode (not supported with Auto IK)
    use_mirror_relative: bool
    
    # Apply changes to matching bone on opposite side of X-Axis
    use_mirror_x: bool

    @classmethod
    def apply_pose_from_action(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...