from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class LineStyleGeometryModifier_2DTransform(LineStyleGeometryModifier):
    
    # Rotation angle
    angle: float
    
    # True if the modifier tab is expanded
    expanded: bool
    
    # Name of the modifier
    name: str
    
    # Pivot of scaling and rotation operations
    pivot: str = 'CENTER' # ['CENTER', 'START', 'END', 'PARAM', 'ABSOLUTE']
    
    # Pivot in terms of the stroke point parameter u (0 &lt;= u &lt;= 1)
    pivot_u: float
    
    # 2D X coordinate of the absolute pivot
    pivot_x: float
    
    # 2D Y coordinate of the absolute pivot
    pivot_y: float
    
    # Scaling factor that is applied along the X axis
    scale_x: float
    
    # Scaling factor that is applied along the Y axis
    scale_y: float
    
    # Type of the modifier
    type: str = '2D_OFFSET' # ['2D_OFFSET', '2D_TRANSFORM', 'BACKBONE_STRETCHER', 'BEZIER_CURVE', 'BLUEPRINT', 'GUIDING_LINES', 'PERLIN_NOISE_1D', 'PERLIN_NOISE_2D', 'POLYGONIZATION', 'SAMPLING', 'SIMPLIFICATION', 'SINUS_DISPLACEMENT', 'SPATIAL_NOISE', 'TIP_REMOVER']
    
    # Enable or disable this modifier during stroke rendering
    use: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...