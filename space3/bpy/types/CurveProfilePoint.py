from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CurveProfilePoint(bpy_struct):
    
    # Path interpolation at this point
    handle_type_1: str = 'FREE' # ['AUTO', 'VECTOR', 'FREE', 'ALIGN']
    
    # Path interpolation at this point
    handle_type_2: str = 'FREE' # ['AUTO', 'VECTOR', 'FREE', 'ALIGN']
    
    # X/Y coordinates of the path point
    location: Tuple[float, float]
    
    # Selection state of the path point
    select: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...