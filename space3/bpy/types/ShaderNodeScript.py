from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ShaderNodeScript(ShaderNode):
    
    # Compile bytecode for shader script node
    bytecode: str
    
    # Hash of compile bytecode, for quick equality checking
    bytecode_hash: str
    
    # Shader script path
    filepath: str
    
    mode: str = 'INTERNAL' # ['INTERNAL', 'EXTERNAL']
    
    # Internal shader script to define the shader
    script: Text
    
    # Automatically update the shader when the .osl file changes (external scripts only)
    use_auto_update: bool

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...