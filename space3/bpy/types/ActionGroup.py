from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ActionGroup(bpy_struct):
    
    # F-Curves in this group
    channels: List[FCurve]
    
    # Custom color set to use
    color_set: str = 'DEFAULT' # ['DEFAULT', 'THEME01', 'THEME02', 'THEME03', 'THEME04', 'THEME05', 'THEME06', 'THEME07', 'THEME08', 'THEME09', 'THEME10', 'THEME11', 'THEME12', 'THEME13', 'THEME14', 'THEME15', 'THEME16', 'THEME17', 'THEME18', 'THEME19', 'THEME20', 'CUSTOM']
    
    # Copy of the colors associated with the group’s color set
    colors: ThemeBoneColorSet
    
    # Color set is user-defined instead of a fixed theme color set
    is_custom_color_set: bool
    
    # Action group is locked
    lock: bool
    
    name: str
    
    # Action group is selected
    select: bool
    
    # Action group is expanded except in graph editor
    show_expanded: bool
    
    # Action group is expanded in graph editor
    show_expanded_graph: bool
    
    use_pin: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...