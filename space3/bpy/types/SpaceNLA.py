from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SpaceNLA(Space):
    
    # Automatic time snapping settings for transformations
    auto_snap: str = 'NONE' # ['NONE', 'STEP', 'TIME_STEP', 'FRAME', 'SECOND', 'MARKER']
    
    # Settings for filtering animation data
    dopesheet: DopeSheet
    
    # Show action-local markers on the strips, useful when synchronizing timing across strips
    show_local_markers: bool
    
    # If any exists, show markers in a separate row at the bottom of the editor
    show_markers: bool
    
    show_region_ui: bool
    
    # Show timing in seconds not frames
    show_seconds: bool
    
    # Show influence F-Curves on strips
    show_strip_curves: bool
    
    # When transforming strips, changes to the animation data are flushed to other views
    use_realtime_update: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...

    def draw_handler_add(self, *args, **kwargs) -> Any:
        ...

    def draw_handler_remove(self, *args, **kwargs) -> None:
        ...