from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class PivotConstraint(Constraint):
    
    # Target along length of bone: Head is 0, Tail is 1
    head_tail: float
    
    # Offset of pivot from target (when set), or from owner’s location (when Fixed Position is off), or the absolute pivot point
    offset: Tuple[float, float, float]
    
    # Rotation range on which pivoting should occur
    rotation_range: str = 'NX' # ['ALWAYS_ACTIVE', 'NX', 'NY', 'NZ', 'X', 'Y', 'Z']
    
    subtarget: str
    
    # Target Object, defining the position of the pivot when defined
    target: Object
    
    # Follow shape of B-Bone segments when calculating Head/Tail position
    use_bbone_shape: bool
    
    # Offset will be an absolute point in space instead of relative to the target
    use_relative_location: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...