from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class FModifierLimits(FModifier):
    
    # Highest X value to allow
    max_x: float
    
    # Highest Y value to allow
    max_y: float
    
    # Lowest X value to allow
    min_x: float
    
    # Lowest Y value to allow
    min_y: float
    
    # Use the maximum X value
    use_max_x: bool
    
    # Use the maximum Y value
    use_max_y: bool
    
    # Use the minimum X value
    use_min_x: bool
    
    # Use the minimum Y value
    use_min_y: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...