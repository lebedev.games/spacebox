from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class PoseBone(bpy_struct):
    
    # X-axis handle offset for start of the B-Bone’s curve, adjusts curvature
    bbone_curveinx: float
    
    # Y-axis handle offset for start of the B-Bone’s curve, adjusts curvature
    bbone_curveiny: float
    
    # X-axis handle offset for end of the B-Bone’s curve, adjusts curvature
    bbone_curveoutx: float
    
    # Y-axis handle offset for end of the B-Bone’s curve, adjusts curvature
    bbone_curveouty: float
    
    # Bone that serves as the end handle for the B-Bone curve
    bbone_custom_handle_end: 'PoseBone'
    
    # Bone that serves as the start handle for the B-Bone curve
    bbone_custom_handle_start: 'PoseBone'
    
    # Length of first Bezier Handle (for B-Bones only)
    bbone_easein: float
    
    # Length of second Bezier Handle (for B-Bones only)
    bbone_easeout: float
    
    # Roll offset for the start of the B-Bone, adjusts twist
    bbone_rollin: float
    
    # Roll offset for the end of the B-Bone, adjusts twist
    bbone_rollout: float
    
    # X-axis scale factor for start of the B-Bone, adjusts thickness (for tapering effects)
    bbone_scaleinx: float
    
    # Y-axis scale factor for start of the B-Bone, adjusts thickness (for tapering effects)
    bbone_scaleiny: float
    
    # X-axis scale factor for end of the B-Bone, adjusts thickness (for tapering effects)
    bbone_scaleoutx: float
    
    # Y-axis scale factor for end of the B-Bone, adjusts thickness (for tapering effects)
    bbone_scaleouty: float
    
    # Bone associated with this PoseBone
    bone: Bone
    
    # Bone group this pose channel belongs to
    bone_group: BoneGroup
    
    # Bone group this pose channel belongs to (0 means no group)
    bone_group_index: int
    
    # Child of this pose bone
    child: 'PoseBone'
    
    # Constraints that act on this pose channel
    constraints: PoseBoneConstraints
    
    # Object that defines custom display shape for this bone
    custom_shape: Object
    
    # Adjust the size of the custom shape
    custom_shape_scale: float
    
    # Bone that defines the display transform of this custom shape
    custom_shape_transform: 'PoseBone'
    
    # Location of head of the channel’s bone
    head: Tuple[float, float, float]
    
    # Weight of scale constraint for IK
    ik_linear_weight: float
    
    # Maximum angles for IK Limit
    ik_max_x: float
    
    # Maximum angles for IK Limit
    ik_max_y: float
    
    # Maximum angles for IK Limit
    ik_max_z: float
    
    # Minimum angles for IK Limit
    ik_min_x: float
    
    # Minimum angles for IK Limit
    ik_min_y: float
    
    # Minimum angles for IK Limit
    ik_min_z: float
    
    # Weight of rotation constraint for IK
    ik_rotation_weight: float
    
    # IK stiffness around the X axis
    ik_stiffness_x: float
    
    # IK stiffness around the Y axis
    ik_stiffness_y: float
    
    # IK stiffness around the Z axis
    ik_stiffness_z: float
    
    # Allow scaling of the bone for IK
    ik_stretch: float
    
    # Is part of an IK chain
    is_in_ik_chain: bool
    
    # Length of the bone
    length: float
    
    location: Tuple[float, float, float]
    
    # Disallow movement around the X axis
    lock_ik_x: bool
    
    # Disallow movement around the Y axis
    lock_ik_y: bool
    
    # Disallow movement around the Z axis
    lock_ik_z: bool
    
    # Lock editing of location when transforming
    lock_location: bool
    
    # Lock editing of rotation when transforming
    lock_rotation: bool
    
    # Lock editing of ‘angle’ component of four-component rotations when transforming
    lock_rotation_w: bool
    
    # Lock editing of four component rotations by components (instead of as Eulers)
    lock_rotations_4d: bool
    
    # Lock editing of scale when transforming
    lock_scale: bool
    
    # Final 4x4 matrix after constraints and drivers are applied (object space)
    matrix: List[float]
    
    # Alternative access to location/scale/rotation relative to the parent and own rest bone
    matrix_basis: List[float]
    
    # 4x4 matrix, before constraints
    matrix_channel: List[float]
    
    # Motion Path for this element
    motion_path: MotionPath
    
    name: str
    
    # Parent of this pose bone
    parent: 'PoseBone'
    
    # Angle of Rotation for Axis-Angle rotation representation
    rotation_axis_angle: Tuple[float, float, float, float]
    
    # Rotation in Eulers
    rotation_euler: Tuple[float, float, float]
    
    rotation_mode: str = 'QUATERNION' # ['QUATERNION', 'XYZ', 'XZY', 'YXZ', 'YZX', 'ZXY', 'ZYX', 'AXIS_ANGLE']
    
    # Rotation in Quaternions
    rotation_quaternion: Tuple[float, float, float, float]
    
    scale: Tuple[float, float, float]
    
    # Location of tail of the channel’s bone
    tail: Tuple[float, float, float]
    
    # Scale the custom object by the bone length
    use_custom_shape_bone_size: bool
    
    # Limit movement around the X axis
    use_ik_limit_x: bool
    
    # Limit movement around the Y axis
    use_ik_limit_y: bool
    
    # Limit movement around the Z axis
    use_ik_limit_z: bool
    
    # Apply channel size as IK constraint if stretching is enabled
    use_ik_linear_control: bool
    
    # Apply channel rotation as IK constraint
    use_ik_rotation_control: bool
    
    # The name of this bone before any ‘.’ character
    basename: Any
    
    # The midpoint between the head and the tail.
    center: Any
    
    # (readonly)
    children: Any
    
    # A list of all children from this bone.
    children_recursive: Any
    
    children_recursive_basename: Any
    
    # A list of parents, starting with the immediate parent
    parent_recursive: Any
    
    vector: Any
    
    # Vector pointing down the x-axis of the bone.
    x_axis: Any
    
    # Vector pointing down the y-axis of the bone.
    y_axis: Any
    
    # Vector pointing down the z-axis of the bone.
    z_axis: Any

    def evaluate_envelope(self, *args, **kwargs) -> float:
        ...

    def bbone_segment_matrix(self, *args, **kwargs) -> List[float]:
        ...

    def compute_bbone_handles(self, *args, **kwargs) -> None:
        ...

    def parent_index(self, *args, **kwargs) -> None:
        ...

    def translate(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...