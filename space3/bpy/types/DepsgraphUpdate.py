from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class DepsgraphUpdate(bpy_struct):
    
    # Updated data-block
    id: ID
    
    # Object geometry is updated
    is_updated_geometry: bool
    
    # Object shading is updated
    is_updated_shading: bool
    
    # Object transformation is updated
    is_updated_transform: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...