from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CompositorNodeScale(CompositorNode):
    
    # How the image fits in the camera frame
    frame_method: str = 'STRETCH' # ['STRETCH', 'FIT', 'CROP']
    
    # Offset image horizontally (factor of image size)
    offset_x: float
    
    # Offset image vertically (factor of image size)
    offset_y: float
    
    # Coordinate space to scale relative to
    space: str = 'RELATIVE' # ['RELATIVE', 'ABSOLUTE', 'SCENE_SIZE', 'RENDER_SIZE']

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    def update(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...