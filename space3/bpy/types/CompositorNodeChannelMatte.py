from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CompositorNodeChannelMatte(CompositorNode):
    
    color_space: str = 'RGB' # ['RGB', 'HSV', 'YUV', 'YCC']
    
    # Limit by this channel’s value
    limit_channel: str = 'R' # ['R', 'G', 'B']
    
    # Values higher than this setting are 100% opaque
    limit_max: float
    
    # Algorithm to use to limit channel
    limit_method: str = 'SINGLE' # ['SINGLE', 'MAX']
    
    # Values lower than this setting are 100% keyed
    limit_min: float
    
    # Channel used to determine matte
    matte_channel: str = 'R' # ['R', 'G', 'B']

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    def update(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...