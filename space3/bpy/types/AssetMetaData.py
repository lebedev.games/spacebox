from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class AssetMetaData(bpy_struct):
    
    # Index of the tag set for editing
    active_tag: int
    
    # A description of the asset to be displayed for the user
    description: str
    
    # Custom tags (name tokens) for the asset, used for filtering and general asset management
    tags: AssetTags

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...