from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Attribute(bpy_struct):
    
    # Type of data stored in attribute
    data_type: str = 'FLOAT' # ['FLOAT', 'INT', 'FLOAT_VECTOR', 'FLOAT_COLOR', 'BYTE_COLOR', 'STRING', 'BOOLEAN', 'FLOAT2']
    
    # Domain of the Attribute
    domain: str = 'POINT' # ['POINT', 'EDGE', 'FACE', 'CORNER', 'CURVE']
    
    # Name of the Attribute
    name: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...