from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Scopes(bpy_struct):
    
    # Proportion of original image source pixel lines to sample
    accuracy: float
    
    # Histogram for viewing image statistics
    histogram: Histogram
    
    # Sample every pixel of the image
    use_full_resolution: bool
    
    # Opacity of the points
    vectorscope_alpha: float
    
    # Opacity of the points
    waveform_alpha: float
    
    waveform_mode: str = 'LUMA' # ['LUMA', 'PARADE', 'YCBCR601', 'YCBCR709', 'YCBCRJPG', 'RGB']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...