from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class KeyMapItems(bpy_struct):

    def new(self, *args, **kwargs) -> KeyMapItem:
        ...

    def new_modal(self, *args, **kwargs) -> KeyMapItem:
        ...

    def new_from_item(self, *args, **kwargs) -> KeyMapItem:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    def from_id(self, *args, **kwargs) -> KeyMapItem:
        ...

    def find_from_operator(self, *args, **kwargs) -> None:
        ...

    def match_event(self, *args, **kwargs) -> KeyMapItem:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...