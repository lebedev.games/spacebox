from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ParticleSettings(ID):
    
    active_instanceweight: ParticleDupliWeight
    
    active_instanceweight_index: int
    
    # Active texture slot being displayed
    active_texture: Texture
    
    # Index of active texture slot
    active_texture_index: int
    
    # How many degrees path has to curve to make another render segment
    adaptive_angle: int
    
    # How many pixels path has to cover to make another render segment
    adaptive_pixel: int
    
    # Angular velocity amount (in radians per second)
    angular_velocity_factor: float
    
    # What axis is used to change particle rotation with time
    angular_velocity_mode: str = 'VELOCITY' # ['NONE', 'VELOCITY', 'HORIZONTAL', 'VERTICAL', 'GLOBAL_X', 'GLOBAL_Y', 'GLOBAL_Z', 'RAND']
    
    # Animation data for this data-block
    animation_data: AnimData
    
    # Apply effectors to children
    apply_effector_to_children: bool
    
    apply_guide_to_children: bool
    
    # Random stiffness of hairs
    bending_random: float
    
    boids: BoidSettings
    
    # Threshold of branching
    branch_threshold: float
    
    # Amount of random, erratic particle movement
    brownian_factor: float
    
    # Length of child paths
    child_length: float
    
    # Amount of particles left untouched by child path length
    child_length_threshold: float
    
    # Number of children per parent
    child_nbr: int
    
    # Create parting in the children based on parent strands
    child_parting_factor: float
    
    # Maximum root to tip angle (tip distance/root distance for long hair)
    child_parting_max: float
    
    # Minimum root to tip angle (tip distance/root distance for long hair)
    child_parting_min: float
    
    # Radius of children around parent
    child_radius: float
    
    # Roundness of children around parent
    child_roundness: float
    
    # A multiplier for the child particle size
    child_size: float
    
    # Random variation to the size of the child particles
    child_size_random: float
    
    # Create child particles
    child_type: str = 'NONE' # ['NONE', 'SIMPLE', 'INTERPOLATED']
    
    # Curve defining clump tapering
    clump_curve: CurveMapping
    
    # Amount of clumping
    clump_factor: float
    
    # Size of clump noise
    clump_noise_size: float
    
    # Shape of clumping
    clump_shape: float
    
    # Limit colliders to this collection
    collision_collection: Collection
    
    # Maximum length of the particle color vector
    color_maximum: float
    
    # Total number of particles
    count: int
    
    # The relative distance a particle can move before requiring more subframes (target Courant number); 0.01 to 0.3 is the recommended range
    courant_target: float
    
    # Calculate children that suit long hair well
    create_long_hair_children: bool
    
    # Amount of damping
    damping: float
    
    # Display additional particle data as a color
    display_color: str = 'MATERIAL' # ['NONE', 'MATERIAL', 'VELOCITY', 'ACCELERATION']
    
    # How particles are displayed in viewport
    display_method: str = 'RENDER' # ['NONE', 'RENDER', 'DOT', 'CIRC', 'CROSS', 'AXIS']
    
    # Percentage of particles to display in 3D view
    display_percentage: int
    
    # Size of particles on viewport
    display_size: float
    
    # How many steps paths are displayed with (power of 2)
    display_step: int
    
    # How to distribute particles on selected element
    distribution: str = 'JIT' # ['JIT', 'RAND', 'GRID']
    
    # Amount of air drag
    drag_factor: float
    
    # Hair stiffness for effectors
    effect_hair: float
    
    # How many particles are effectors (0 is all particles)
    effector_amount: int
    
    effector_weights: EffectorWeights
    
    # Where to emit particles from
    emit_from: str = 'FACE' # ['VERT', 'FACE', 'VOLUME']
    
    # Give the starting velocity a random variation
    factor_random: float
    
    fluid: SPHFluidSettings
    
    force_field_1: FieldSettings
    
    force_field_2: FieldSettings
    
    # Frame number to stop emitting particles
    frame_end: float
    
    # Frame number to start emitting particles
    frame_start: float
    
    # Add random offset to the grid locations
    grid_random: float
    
    # The resolution of the particle grid
    grid_resolution: int
    
    # Length of the hair
    hair_length: float
    
    # Number of hair segments
    hair_step: int
    
    # Create the grid in a hexagonal pattern
    hexagonal_grid: bool
    
    # Show objects in this collection in place of particles
    instance_collection: Collection
    
    # Show this object in place of particles
    instance_object: Object
    
    # Weights for all of the objects in the instance collection
    instance_weights: List[ParticleDupliWeight]
    
    # Algorithm used to calculate physics, from the fastest to the most stable and accurate: Midpoint, Euler, Verlet, RK4
    integrator: str = 'MIDPOINT' # ['EULER', 'VERLET', 'MIDPOINT', 'RK4']
    
    # Invert what is considered object and what is not
    invert_grid: bool
    
    # Particles were created by a fluid simulation
    is_fluid: bool
    
    # Amount of jitter applied to the sampling
    jitter_factor: float
    
    # Number of times the keys are looped
    keyed_loops: int
    
    keys_step: int
    
    # Type of periodic offset on the path
    kink: str = 'NO' # ['NO', 'CURL', 'RADIAL', 'WAVE', 'BRAID', 'SPIRAL']
    
    # The amplitude of the offset
    kink_amplitude: float
    
    # How much clump affects kink amplitude
    kink_amplitude_clump: float
    
    # Random variation of the amplitude
    kink_amplitude_random: float
    
    # Which axis to use for offset
    kink_axis: str = 'Z' # ['X', 'Y', 'Z']
    
    # Random variation of the orientation
    kink_axis_random: float
    
    # Extra steps for resolution of special kink features
    kink_extra_steps: int
    
    # How flat the hairs are
    kink_flat: float
    
    # The frequency of the offset (1/total length)
    kink_frequency: float
    
    # Adjust the offset to the beginning/end
    kink_shape: float
    
    # Give path length a random variation
    length_random: float
    
    # Life span of the particles
    lifetime: float
    
    # Give the particle life a random variation
    lifetime_random: float
    
    # Length of the line’s head
    line_length_head: float
    
    # Length of the line’s tail
    line_length_tail: float
    
    # Constrain boids to a surface
    lock_boids_to_surface: bool
    
    # Mass of the particles
    mass: float
    
    # Index of material slot used for rendering particles
    material: int
    
    # Material slot used for rendering particles
    material_slot: str = 'DUMMY' # ['DUMMY']
    
    # Let the surface normal give the particle a starting velocity
    normal_factor: float
    
    # Let the emitter object orientation give the particle a starting velocity
    object_align_factor: Tuple[float, float, float]
    
    # Let the object give the particle a starting velocity
    object_factor: float
    
    # Let the target particle give the particle a starting velocity
    particle_factor: float
    
    # The size of the particles
    particle_size: float
    
    # End time of path
    path_end: float
    
    # Starting time of path
    path_start: float
    
    # Rotation around the chosen orientation axis
    phase_factor: float
    
    # Randomize rotation around the chosen orientation axis
    phase_factor_random: float
    
    # Particle physics type
    physics_type: str = 'NEWTON' # ['NO', 'NEWTON', 'KEYED', 'BOIDS', 'FLUID']
    
    # Multiplier of diameter properties
    radius_scale: float
    
    # The event of target particles to react on
    react_event: str = 'DEATH' # ['DEATH', 'COLLIDE', 'NEAR']
    
    # Let the vector away from the target particle’s location give the particle a starting velocity
    reactor_factor: float
    
    # How many steps paths are rendered with (power of 2)
    render_step: int
    
    # How particles are rendered
    render_type: str = 'HALO' # ['NONE', 'HALO', 'LINE', 'PATH', 'OBJECT', 'COLLECTION']
    
    # Number of children per parent for rendering
    rendered_child_count: int
    
    # Strand diameter width at the root
    root_radius: float
    
    # Randomize particle orientation
    rotation_factor_random: float
    
    # Particle orientation axis (does not affect Explode modifier’s results)
    rotation_mode: str = 'VEL' # ['NONE', 'NOR', 'NOR_TAN', 'VEL', 'GLOB_X', 'GLOB_Y', 'GLOB_Z', 'OB_X', 'OB_Y', 'OB_Z']
    
    # Amount of location dependent roughness
    roughness_1: float
    
    # Size of location dependent roughness
    roughness_1_size: float
    
    # Amount of random roughness
    roughness_2: float
    
    # Size of random roughness
    roughness_2_size: float
    
    # Amount of particles left untouched by random roughness
    roughness_2_threshold: float
    
    # Curve defining roughness
    roughness_curve: CurveMapping
    
    # Shape of endpoint roughness
    roughness_end_shape: float
    
    # Amount of endpoint roughness
    roughness_endpoint: float
    
    # Strand shape parameter
    shape: float
    
    # Show guide hairs
    show_guide_hairs: bool
    
    # Show hair simulation grid
    show_hair_grid: bool
    
    # Display boid health
    show_health: bool
    
    # Show particle number
    show_number: bool
    
    # Show particle size
    show_size: bool
    
    # Show particles before they are emitted
    show_unborn: bool
    
    # Show particle velocity
    show_velocity: bool
    
    # Give the particle size a random variation
    size_random: float
    
    # Subframes to simulate for improved stability and finer granularity simulations (dt = timestep / (subframes + 1))
    subframes: int
    
    # Let the surface tangent give the particle a starting velocity
    tangent_factor: float
    
    # Rotate the surface tangent
    tangent_phase: float
    
    # Texture slots defining the mapping and influence of textures
    texture_slots: ParticleSettingsTextureSlots
    
    # A multiplier for physics timestep (1.0 means one frame = 1/25 seconds)
    time_tweak: float
    
    # The simulation timestep per frame (seconds per frame)
    timestep: float
    
    # Strand diameter width at the tip
    tip_radius: float
    
    # Number of trail particles
    trail_count: int
    
    # Number of turns around parent along the strand
    twist: float
    
    # Curve defining twist
    twist_curve: CurveMapping
    
    # Particle type
    type: str = 'EMITTER' # ['EMITTER', 'HAIR']
    
    # Path timing is in absolute frames
    use_absolute_path_time: bool
    
    # Automatically set the number of subframes
    use_adaptive_subframes: bool
    
    # Use full physics calculations for growing hair
    use_advanced_hair: bool
    
    # Set tip radius to zero
    use_close_tip: bool
    
    # Use a curve to define clump tapering
    use_clump_curve: bool
    
    # Create random clumps around the parent
    use_clump_noise: bool
    
    # Use object multiple times in the same collection
    use_collection_count: bool
    
    # Pick objects from collection randomly
    use_collection_pick_random: bool
    
    # Show particles after they have died
    use_dead: bool
    
    # Particles die when they collide with a deflector object
    use_die_on_collision: bool
    
    # Particle rotations are affected by collisions and effectors
    use_dynamic_rotation: bool
    
    # Emit in random order of elements
    use_emit_random: bool
    
    # Use even distribution from faces based on face areas or edge lengths
    use_even_distribution: bool
    
    # Use object’s global coordinates for duplication
    use_global_instance: bool
    
    # Interpolate hair using B-Splines
    use_hair_bspline: bool
    
    # Emit particles from mesh with modifiers applied (must use same subdivision surface level for viewport and render for correct results)
    use_modifier_stack: bool
    
    # Multiply mass by particle size
    use_multiply_size_mass: bool
    
    # Render parent particles
    use_parent_particles: bool
    
    # React multiple times
    use_react_multiple: bool
    
    # Give birth to unreacted particles eventually
    use_react_start_end: bool
    
    # Regrow hair for each frame
    use_regrow_hair: bool
    
    # Display steps of the particle path
    use_render_adaptive: bool
    
    # Use object’s rotation for duplication (global x-axis is aligned particle rotation axis)
    use_rotation_instance: bool
    
    # Calculate particle rotations
    use_rotations: bool
    
    # Use a curve to define roughness
    use_roughness_curve: bool
    
    # Use object’s scale for duplication
    use_scale_instance: bool
    
    # Particle effectors affect themselves
    use_self_effect: bool
    
    # Use particle’s size in deflection
    use_size_deflect: bool
    
    # Use the strand primitive for rendering
    use_strand_primitive: bool
    
    # Use a curve to define twist
    use_twist_curve: bool
    
    # Multiply line length by particle speed
    use_velocity_length: bool
    
    # Use whole collection at once
    use_whole_collection: bool
    
    # Emission locations per face (0 = automatic)
    userjit: int
    
    # Relative amount of virtual parents
    virtual_parents: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...