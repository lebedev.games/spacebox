from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class LatticeGpencilModifier(GpencilModifier):
    
    # Inverse filter
    invert_layer_pass: bool
    
    # Inverse filter
    invert_layers: bool
    
    # Inverse filter
    invert_material_pass: bool
    
    # Inverse filter
    invert_materials: bool
    
    # Inverse filter
    invert_vertex: bool
    
    # Layer name
    layer: str
    
    # Layer pass index
    layer_pass: int
    
    # Material used for filtering effect
    material: Material
    
    # Lattice object to deform with
    object: Object
    
    # Pass index
    pass_index: int
    
    # Strength of modifier effect
    strength: float
    
    # Vertex group name for modulating the deform
    vertex_group: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...