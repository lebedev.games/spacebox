from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class FluidFlowSettings(bpy_struct):
    
    density: float
    
    # Name of vertex group which determines surface emission rate
    density_vertex_group: str
    
    # Change flow behavior in the simulation
    flow_behavior: str = 'GEOMETRY' # ['INFLOW', 'OUTFLOW', 'GEOMETRY']
    
    # Change how fluid is emitted
    flow_source: str = 'NONE' # ['NONE']
    
    # Change type of fluid in the simulation
    flow_type: str = 'SMOKE' # ['SMOKE', 'BOTH', 'FIRE', 'LIQUID']
    
    fuel_amount: float
    
    # Texture that controls emission strength
    noise_texture: Texture
    
    # Particle size in simulation cells
    particle_size: float
    
    # Particle systems emitted from the object
    particle_system: ParticleSystem
    
    # Color of smoke
    smoke_color: Tuple[float, float, float]
    
    # Number of additional samples to take between frames to improve quality of fast moving flows
    subframes: int
    
    # Controls fluid emission from the mesh surface (higher value results in emission further away from the mesh surface
    surface_distance: float
    
    # Temperature difference to ambient temperature
    temperature: float
    
    # Texture mapping type
    texture_map_type: str = 'AUTO' # ['AUTO', 'UV']
    
    # Z-offset of texture mapping
    texture_offset: float
    
    # Size of texture mapping
    texture_size: float
    
    # Only allow given density value in emitter area and will not add up
    use_absolute: bool
    
    # Control when to apply fluid flow
    use_inflow: bool
    
    # Fluid has some initial velocity when it is emitted
    use_initial_velocity: bool
    
    # Set particle size in simulation cells or use nearest cell
    use_particle_size: bool
    
    # Treat this object as a planar and unclosed mesh. Fluid will only be emitted from the mesh surface and based on the surface emission value
    use_plane_init: bool
    
    # Use a texture to control emission strength
    use_texture: bool
    
    # UV map name
    uv_layer: str
    
    # Additional initial velocity in X, Y and Z direction (added to source velocity)
    velocity_coord: Tuple[float, float, float]
    
    # Multiplier of source velocity passed to fluid (source velocity is non-zero only if object is moving)
    velocity_factor: float
    
    # Amount of normal directional velocity
    velocity_normal: float
    
    # Amount of random velocity
    velocity_random: float
    
    # Controls fluid emission from within the mesh (higher value results in greater emissions from inside the mesh)
    volume_density: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...