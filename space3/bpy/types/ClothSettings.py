from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ClothSettings(bpy_struct):
    
    # Air has normally some thickness which slows falling things down
    air_damping: float
    
    # Amount of damping in bending behavior
    bending_damping: float
    
    # Physical model for simulating bending forces
    bending_model: str = 'ANGULAR' # ['ANGULAR', 'LINEAR']
    
    # How much the material resists bending
    bending_stiffness: float
    
    # Maximum bending stiffness value
    bending_stiffness_max: float
    
    collider_friction: float
    
    # Amount of damping in compression behavior
    compression_damping: float
    
    # How much the material resists compression
    compression_stiffness: float
    
    # Maximum compression stiffness value
    compression_stiffness_max: float
    
    # Influence of target density on the simulation
    density_strength: float
    
    # Maximum density of hair
    density_target: float
    
    effector_weights: EffectorWeights
    
    # Density (kg/l) of the fluid contained inside the object, used to create a hydrostatic pressure gradient simulating the weight of the internal fluid, or buoyancy from the surrounding fluid if negative
    fluid_density: float
    
    # Default Goal (vertex target position) value, when no Vertex Group used
    goal_default: float
    
    # Goal (vertex target position) friction
    goal_friction: float
    
    # Goal maximum, vertex group weights are scaled to match this range
    goal_max: float
    
    # Goal minimum, vertex group weights are scaled to match this range
    goal_min: float
    
    # Goal (vertex target position) spring stiffness
    goal_spring: float
    
    # Gravity or external force vector
    gravity: Tuple[float, float, float]
    
    # How much the material resists compression
    internal_compression_stiffness: float
    
    # Maximum compression stiffness value
    internal_compression_stiffness_max: float
    
    internal_friction: float
    
    # How much the rays used to connect the internal points can diverge from the vertex normal
    internal_spring_max_diversion: float
    
    # The maximum length an internal spring can have during creation. If the distance between internal points is greater than this, no internal spring will be created between these points. A length of zero means that there is no length limit
    internal_spring_max_length: float
    
    # Require the points the internal springs connect to have opposite normal directions
    internal_spring_normal_check: bool
    
    # How much the material resists stretching
    internal_tension_stiffness: float
    
    # Maximum tension stiffness value
    internal_tension_stiffness_max: float
    
    # The mass of each vertex on the cloth material
    mass: float
    
    # Pin (vertex target position) spring stiffness
    pin_stiffness: float
    
    # Ambient pressure (kPa) that balances out between the inside and outside of the object when it has the target volume
    pressure_factor: float
    
    # Quality of the simulation in steps per frame (higher is better quality but slower)
    quality: int
    
    # Shape key to use the rest spring lengths from
    rest_shape_key: ShapeKey
    
    # Maximum sewing force
    sewing_force_max: float
    
    # Amount of damping in shearing behavior
    shear_damping: float
    
    # How much the material resists shearing
    shear_stiffness: float
    
    # Maximum shear scaling value
    shear_stiffness_max: float
    
    # Max amount to shrink cloth by
    shrink_max: float
    
    # Factor by which to shrink cloth
    shrink_min: float
    
    # The mesh volume where the inner/outer pressure will be the same. If set to zero the change in volume will not affect pressure
    target_volume: float
    
    # Amount of damping in stretching behavior
    tension_damping: float
    
    # How much the material resists stretching
    tension_stiffness: float
    
    # Maximum tension stiffness value
    tension_stiffness_max: float
    
    # Cloth speed is multiplied by this value
    time_scale: float
    
    # The uniform pressure that is constantly applied to the mesh, in units of Pressure Scale. Can be negative
    uniform_pressure_force: float
    
    # Make simulation respect deformations in the base mesh
    use_dynamic_mesh: bool
    
    # Simulate an internal volume structure by creating springs connecting the opposite sides of the mesh
    use_internal_springs: bool
    
    # Simulate pressure inside a closed cloth mesh
    use_pressure: bool
    
    # Use the Target Volume parameter as the initial volume, instead of calculating it from the mesh itself
    use_pressure_volume: bool
    
    # Pulls loose edges together
    use_sewing_springs: bool
    
    # Vertex group for fine control over bending stiffness
    vertex_group_bending: str
    
    # Vertex group for fine control over the internal spring stiffness
    vertex_group_intern: str
    
    # Vertex Group for pinning of vertices
    vertex_group_mass: str
    
    # Vertex Group for where to apply pressure. Zero weight means no pressure while a weight of one means full pressure. Faces with a vertex that has zero weight will be excluded from the volume calculation
    vertex_group_pressure: str
    
    # Vertex group for fine control over shear stiffness
    vertex_group_shear_stiffness: str
    
    # Vertex Group for shrinking cloth
    vertex_group_shrink: str
    
    # Vertex group for fine control over structural stiffness
    vertex_group_structural_stiffness: str
    
    # Size of the voxel grid cells for interaction effects
    voxel_cell_size: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...