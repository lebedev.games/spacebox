from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MotionPathVert(bpy_struct):
    
    co: Tuple[float, float, float]
    
    # Path point is selected for editing
    select: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...