from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class BlendData(bpy_struct):
    
    # Action data-blocks
    actions: BlendDataActions
    
    # Armature data-blocks
    armatures: BlendDataArmatures
    
    # Brush data-blocks
    brushes: BlendDataBrushes
    
    # Cache Files data-blocks
    cache_files: BlendDataCacheFiles
    
    # Camera data-blocks
    cameras: BlendDataCameras
    
    # Collection data-blocks
    collections: BlendDataCollections
    
    # Curve data-blocks
    curves: BlendDataCurves
    
    # Path to the .blend file
    filepath: str
    
    # Vector font data-blocks
    fonts: BlendDataFonts
    
    # Grease Pencil data-blocks
    grease_pencils: BlendDataGreasePencils
    
    # Image data-blocks
    images: BlendDataImages
    
    # Have recent edits been saved to disk
    is_dirty: bool
    
    # Has the current session been saved to disk as a .blend file
    is_saved: bool
    
    # Lattice data-blocks
    lattices: BlendDataLattices
    
    # Library data-blocks
    libraries: BlendDataLibraries
    
    # Light Probe data-blocks
    lightprobes: BlendDataProbes
    
    # Light data-blocks
    lights: BlendDataLights
    
    # Line Style data-blocks
    linestyles: BlendDataLineStyles
    
    # Masks data-blocks
    masks: BlendDataMasks
    
    # Material data-blocks
    materials: BlendDataMaterials
    
    # Mesh data-blocks
    meshes: BlendDataMeshes
    
    # Metaball data-blocks
    metaballs: BlendDataMetaBalls
    
    # Movie Clip data-blocks
    movieclips: BlendDataMovieClips
    
    # Node group data-blocks
    node_groups: BlendDataNodeTrees
    
    # Object data-blocks
    objects: BlendDataObjects
    
    # Paint Curves data-blocks
    paint_curves: BlendDataPaintCurves
    
    # Palette data-blocks
    palettes: BlendDataPalettes
    
    # Particle data-blocks
    particles: BlendDataParticles
    
    # Scene data-blocks
    scenes: BlendDataScenes
    
    # Screen data-blocks
    screens: BlendDataScreens
    
    # Shape Key data-blocks
    shape_keys: List[Key]
    
    # Sound data-blocks
    sounds: BlendDataSounds
    
    # Speaker data-blocks
    speakers: BlendDataSpeakers
    
    # Text data-blocks
    texts: BlendDataTexts
    
    # Texture data-blocks
    textures: BlendDataTextures
    
    # Automatically pack all external data into .blend file
    use_autopack: bool
    
    # File format version the .blend file was saved with
    version: Tuple[int, int, int]
    
    # Volume data-blocks
    volumes: BlendDataVolumes
    
    # Window manager data-blocks
    window_managers: BlendDataWindowManagers
    
    # Workspace data-blocks
    workspaces: BlendDataWorkSpaces
    
    # World data-blocks
    worlds: BlendDataWorlds

    def batch_remove(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...

    def orphans_purge(self, *args, **kwargs) -> None:
        ...

    def temp_data(self, *args, **kwargs) -> 'BlendData':
        ...

    def user_map(self, *args, **kwargs) -> Any:
        ...