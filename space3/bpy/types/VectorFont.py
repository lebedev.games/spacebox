from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class VectorFont(ID):
    
    filepath: str
    
    packed_file: PackedFile

    def pack(self, *args, **kwargs) -> None:
        ...

    def unpack(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...