from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MeshLoop(bpy_struct):
    
    # Bitangent vector of this vertex for this polygon (must be computed beforehand using calc_tangents, use it only if really needed, slower access than bitangent_sign)
    bitangent: Tuple[float, float, float]
    
    # Sign of the bitangent vector of this vertex for this polygon (must be computed beforehand using calc_tangents, bitangent = bitangent_sign * cross(normal, tangent))
    bitangent_sign: float
    
    # Edge index
    edge_index: int
    
    # Index of this loop
    index: int
    
    # Local space unit length split normal vector of this vertex for this polygon (must be computed beforehand using calc_normals_split or calc_tangents)
    normal: Tuple[float, float, float]
    
    # Local space unit length tangent vector of this vertex for this polygon (must be computed beforehand using calc_tangents)
    tangent: Tuple[float, float, float]
    
    # Vertex index
    vertex_index: int

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...