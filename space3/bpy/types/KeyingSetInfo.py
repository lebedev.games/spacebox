from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class KeyingSetInfo(bpy_struct):
    
    # A short description of the keying set
    bl_description: str
    
    # If this is set, the Keying Set gets a custom ID, otherwise it takes the name of the class used to define the Keying Set (for example, if the class name is “BUILTIN_KSI_location”, and bl_idname is not set by the script, then bl_idname = “BUILTIN_KSI_location”)
    bl_idname: str
    
    bl_label: str
    
    # Keying Set options to use when inserting keyframes
    bl_options: Set[str] # {'INSERTKEY_VISUAL', 'default {INSERTKEY_NEEDED', 'INSERTKEY_NEEDED', 'INSERTKEY_XYZ_TO_RGB}'}

    def poll(self, *args, **kwargs) -> bool:
        ...

    def iterator(self, *args, **kwargs) -> None:
        ...

    def generate(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...