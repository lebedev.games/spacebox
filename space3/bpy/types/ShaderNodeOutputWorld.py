from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ShaderNodeOutputWorld(ShaderNode):
    
    # True if this node is used as the active output
    is_active_output: bool
    
    # Which renderer and viewport shading types to use the shaders for
    target: str = 'ALL' # ['ALL', 'EEVEE', 'CYCLES']

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...