from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class View3DCursor(bpy_struct):
    
    location: Tuple[float, float, float]
    
    # Matrix combining location and rotation of the cursor
    matrix: List[float]
    
    # Angle of Rotation for Axis-Angle rotation representation
    rotation_axis_angle: Tuple[float, float, float, float]
    
    # 3D rotation
    rotation_euler: Tuple[float, float, float]
    
    rotation_mode: str = 'XYZ' # ['QUATERNION', 'XYZ', 'XZY', 'YXZ', 'YZX', 'ZXY', 'ZYX', 'AXIS_ANGLE']
    
    # Rotation in quaternions (keep normalized)
    rotation_quaternion: Tuple[float, float, float, float]

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...