from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ActionPoseMarkers(bpy_struct):
    
    # Active pose marker for this action
    active: TimelineMarker
    
    # Index of active pose marker
    active_index: int

    def new(self, *args, **kwargs) -> TimelineMarker:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...