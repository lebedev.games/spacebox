from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class OpacityGpencilModifier(GpencilModifier):
    
    # Custom curve to apply effect
    curve: CurveMapping
    
    # Factor of Opacity
    factor: float
    
    # Factor of stroke hardness
    hardness: float
    
    # Inverse filter
    invert_layer_pass: bool
    
    # Inverse filter
    invert_layers: bool
    
    # Inverse filter
    invert_material_pass: bool
    
    # Inverse filter
    invert_materials: bool
    
    # Inverse filter
    invert_vertex: bool
    
    # Layer name
    layer: str
    
    # Layer pass index
    layer_pass: int
    
    # Material used for filtering effect
    material: Material
    
    # Set what colors of the stroke are affected
    modify_color: str = 'BOTH' # ['BOTH', 'STROKE', 'FILL', 'HARDNESS']
    
    # Replace the stroke opacity
    normalize_opacity: bool
    
    # Pass index
    pass_index: int
    
    # Use a custom curve to define opacity effect along the strokes
    use_custom_curve: bool
    
    # Vertex group name for modulating the deform
    vertex_group: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...