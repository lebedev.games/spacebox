from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ShaderFxBlur(ShaderFx):
    
    # Rotation of the effect
    rotation: float
    
    # Number of Blur Samples (zero, disable blur)
    samples: int
    
    # Factor of Blur
    size: Tuple[float, float]
    
    # Blur using camera depth of field
    use_dof_mode: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...