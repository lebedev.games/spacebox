from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class GreasePencilLayers(bpy_struct):
    
    # Active grease pencil layer
    active: GPencilLayer
    
    # Index of active grease pencil layer
    active_index: int
    
    # Note/Layer to add annotation strokes to
    active_note: str = 'DEFAULT' # ['DEFAULT']

    def new(self, *args, **kwargs) -> GPencilLayer:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    def move(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...