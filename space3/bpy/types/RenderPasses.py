from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class RenderPasses(bpy_struct):

    def find_by_type(self, *args, **kwargs) -> RenderPass:
        ...

    def find_by_name(self, *args, **kwargs) -> RenderPass:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...