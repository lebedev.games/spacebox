from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class FCurveModifiers(bpy_struct):
    
    # Active F-Curve Modifier
    active: FModifier

    def new(self, *args, **kwargs) -> FModifier:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...