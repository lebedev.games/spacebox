from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MeshDeformModifier(Modifier):
    
    # Invert vertex group influence
    invert_vertex_group: bool
    
    # Whether geometry has been bound to control cage
    is_bound: bool
    
    # Mesh object to deform with
    object: Object
    
    # The grid size for binding
    precision: int
    
    # Recompute binding dynamically on top of other deformers (slower and more memory consuming)
    use_dynamic_bind: bool
    
    # Vertex group name
    vertex_group: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...