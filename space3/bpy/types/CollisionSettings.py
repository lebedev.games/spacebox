from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CollisionSettings(bpy_struct):
    
    # How much of effector force gets lost during collision with this object (in percent)
    absorption: float
    
    # Friction for cloth collisions
    cloth_friction: float
    
    # Amount of damping during collision
    damping: float
    
    # Amount of damping during particle collision
    damping_factor: float
    
    # Random variation of damping
    damping_random: float
    
    # Amount of friction during particle collision
    friction_factor: float
    
    # Random variation of friction
    friction_random: float
    
    # Chance that the particle will pass through the mesh
    permeability: float
    
    # Amount of stickiness to surface collision
    stickiness: float
    
    # Inner face thickness (only used by softbodies)
    thickness_inner: float
    
    # Outer face thickness
    thickness_outer: float
    
    # Enable this objects as a collider for physics systems
    use: bool
    
    # Cloth collision acts with respect to the collider normals (improves penetration recovery)
    use_culling: bool
    
    # Cloth collision impulses act in the direction of the collider normals (more reliable in some cases)
    use_normal: bool
    
    # Kill collided particles
    use_particle_kill: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...