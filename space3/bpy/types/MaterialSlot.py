from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MaterialSlot(bpy_struct):
    
    # Link material to object or the object’s data
    link: str = 'DATA' # ['OBJECT', 'DATA']
    
    # Material data-block used by this material slot
    material: Material
    
    # Material slot name
    name: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...