from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MovieClipProxy(bpy_struct):
    
    # Build proxy resolution 100% of the original footage dimension
    build_100: bool
    
    # Build proxy resolution 25% of the original footage dimension
    build_25: bool
    
    # Build proxy resolution 50% of the original footage dimension
    build_50: bool
    
    # Build proxy resolution 75% of the original footage dimension
    build_75: bool
    
    # Build free run time code index
    build_free_run: bool
    
    # Build free run time code index using Record Date/Time
    build_free_run_rec_date: bool
    
    # Build record run time code index
    build_record_run: bool
    
    # Build proxy resolution 100% of the original undistorted footage dimension
    build_undistorted_100: bool
    
    # Build proxy resolution 25% of the original undistorted footage dimension
    build_undistorted_25: bool
    
    # Build proxy resolution 50% of the original undistorted footage dimension
    build_undistorted_50: bool
    
    # Build proxy resolution 75% of the original undistorted footage dimension
    build_undistorted_75: bool
    
    # Location to store the proxy files
    directory: str
    
    # JPEG quality of proxy images
    quality: int
    
    timecode: str = 'NONE' # ['NONE', 'RECORD_RUN', 'FREE_RUN', 'FREE_RUN_REC_DATE', 'FREE_RUN_NO_GAPS']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...