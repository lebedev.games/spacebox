from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ShaderFx(bpy_struct):
    
    # Effect name
    name: str
    
    # Set effect expansion in the user interface
    show_expanded: bool
    
    # Display effect in Edit mode
    show_in_editmode: bool
    
    # Use effect during render
    show_render: bool
    
    # Display effect in viewport
    show_viewport: bool
    
    type: str = 'FX_BLUR' # ['FX_BLUR', 'FX_COLORIZE', 'FX_FLIP', 'FX_GLOW', 'FX_PIXEL', 'FX_RIM', 'FX_SHADOW', 'FX_SWIRL', 'FX_WAVE']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...