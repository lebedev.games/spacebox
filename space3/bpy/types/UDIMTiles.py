from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class UDIMTiles(bpy_struct):
    
    # Active Image Tile
    active: UDIMTile
    
    # Active index in tiles array
    active_index: int

    def new(self, *args, **kwargs) -> UDIMTile:
        ...

    def get(self, *args, **kwargs) -> UDIMTile:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...