from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class OperatorStrokeElement(PropertyGroup):
    
    is_start: bool
    
    location: Tuple[float, float, float]
    
    mouse: Tuple[float, float]
    
    mouse_event: Tuple[float, float]
    
    pen_flip: bool
    
    # Tablet pressure
    pressure: float
    
    # Brush size in screen space
    size: float
    
    time: float
    
    x_tilt: float
    
    y_tilt: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...