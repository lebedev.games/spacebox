from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class WhiteBalanceModifier(SequenceModifier):
    
    # This color defines white in the strip
    white_value: Tuple[float, float, float]

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...