from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SequencerTonemapModifierData(SequenceModifier):
    
    # If 0, global; if 1, based on pixel intensity
    adaptation: float
    
    # Set to 0 to use estimate from input image
    contrast: float
    
    # If 0, same for all channels; if 1, each independent
    correction: float
    
    # If not used, set to 1
    gamma: float
    
    # If less than zero, darkens image; otherwise, makes it brighter
    intensity: float
    
    # The value the average luminance is mapped to
    key: float
    
    # Normally always 1, but can be used as an extra control to alter the brightness curve
    offset: float
    
    # Tone mapping algorithm
    tonemap_type: str = 'RH_SIMPLE' # ['RD_PHOTORECEPTOR', 'RH_SIMPLE']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...