from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Object(ID):
    
    # Active material being displayed
    active_material: Material
    
    # Index of active material slot
    active_material_index: int
    
    # Current shape key
    active_shape_key: ShapeKey
    
    # Current shape key index
    active_shape_key_index: int
    
    # Animation data for this data-block
    animation_data: AnimData
    
    # Animation data for this data-block
    animation_visualization: AnimViz
    
    # Object’s bounding box in object-space coordinates, all values are -1.0 when not available
    bound_box: List[float]
    
    # Settings for using the object as a collider in physics simulation
    collision: CollisionSettings
    
    # Object color and alpha, used when faces have the ObColor mode enabled
    color: Tuple[float, float, float, float]
    
    # Constraints affecting the transformation of the object
    constraints: ObjectConstraints
    
    # Cycles object settings
    cycles: Any
    
    # Cycles visibility settings
    cycles_visibility: Any
    
    # Object data
    data: ID
    
    # Extra translation added to the location of the object
    delta_location: Tuple[float, float, float]
    
    # Extra rotation added to the rotation of the object (when using Euler rotations)
    delta_rotation_euler: Tuple[float, float, float]
    
    # Extra rotation added to the rotation of the object (when using Quaternion rotations)
    delta_rotation_quaternion: Tuple[float, float, float, float]
    
    # Extra scaling added to the scale of the object
    delta_scale: Tuple[float, float, float]
    
    dimensions: Tuple[float, float, float]
    
    # Object display settings for 3D viewport
    display: ObjectDisplay
    
    # Object boundary display type
    display_bounds_type: str = 'BOX' # ['BOX', 'SPHERE', 'CYLINDER', 'CONE', 'CAPSULE']
    
    # How to display object in viewport
    display_type: str = 'TEXTURED' # ['BOUNDS', 'WIRE', 'SOLID', 'TEXTURED']
    
    # Size of display for empties in the viewport
    empty_display_size: float
    
    # Viewport display style for empties
    empty_display_type: str = 'PLAIN_AXES' # ['PLAIN_AXES', 'ARROWS', 'SINGLE_ARROW', 'CIRCLE', 'CUBE', 'SPHERE', 'CONE', 'IMAGE']
    
    # Determine which other objects will occlude the image
    empty_image_depth: str = 'DEFAULT' # ['DEFAULT', 'FRONT', 'BACK']
    
    # Origin offset distance
    empty_image_offset: Tuple[float, float]
    
    # Show front/back side
    empty_image_side: str = 'DOUBLE_SIDED' # ['DOUBLE_SIDED', 'FRONT', 'BACK']
    
    # Maps of faces of the object
    face_maps: FaceMaps
    
    # Settings for using the object as a field in physics simulation
    field: FieldSettings
    
    # Modifiers affecting the data of the grease pencil object
    grease_pencil_modifiers: ObjectGpencilModifiers
    
    # Globally disable in renders
    hide_render: bool
    
    # Disable selection in viewport
    hide_select: bool
    
    # Globally disable in viewports
    hide_viewport: bool
    
    # Parameters defining which layer, pass and frame of the image is displayed
    image_user: ImageUser
    
    # Instance an existing collection
    instance_collection: Collection
    
    # Scale the face instance objects
    instance_faces_scale: float
    
    # If not None, object instancing method to use
    instance_type: str = 'NONE' # ['NONE', 'VERTS', 'FACES', 'COLLECTION']
    
    # Object comes from a instancer
    is_from_instancer: bool
    
    # Object comes from a background set
    is_from_set: bool
    
    is_instancer: bool
    
    # Line art settings for the object
    lineart: ObjectLineArt
    
    # Location of the object
    location: Tuple[float, float, float]
    
    # Lock editing of location when transforming
    lock_location: bool
    
    # Lock editing of rotation when transforming
    lock_rotation: bool
    
    # Lock editing of ‘angle’ component of four-component rotations when transforming
    lock_rotation_w: bool
    
    # Lock editing of four component rotations by components (instead of as Eulers)
    lock_rotations_4d: bool
    
    # Lock editing of scale when transforming
    lock_scale: bool
    
    # Material slots in the object
    material_slots: List[MaterialSlot]
    
    # Matrix access to location, rotation and scale (including deltas), before constraints and parenting are applied
    matrix_basis: List[float]
    
    matrix_local: List[float]
    
    # Inverse of object’s parent matrix at time of parenting
    matrix_parent_inverse: List[float]
    
    # Worldspace transformation matrix
    matrix_world: List[float]
    
    # Object interaction mode
    mode: str = 'OBJECT' # ['OBJECT', 'EDIT', 'POSE', 'SCULPT', 'VERTEX_PAINT', 'WEIGHT_PAINT', 'TEXTURE_PAINT', 'PARTICLE_EDIT', 'EDIT_GPENCIL', 'SCULPT_GPENCIL', 'PAINT_GPENCIL', 'WEIGHT_GPENCIL', 'VERTEX_GPENCIL']
    
    # Modifiers affecting the geometric data of the object
    modifiers: ObjectModifiers
    
    # Motion Path for this element
    motion_path: MotionPath
    
    # Parent object
    parent: 'Object'
    
    # Name of parent bone in case of a bone parenting relation
    parent_bone: str
    
    # Type of parent relation
    parent_type: str = 'OBJECT' # ['OBJECT', 'ARMATURE', 'LATTICE', 'VERTEX', 'VERTEX_3', 'BONE']
    
    # Indices of vertices in case of a vertex parenting relation
    parent_vertices: Tuple[int, int, int]
    
    # Particle systems emitted from the object
    particle_systems: ParticleSystems
    
    # Index number for the “Object Index” render pass
    pass_index: int
    
    # Current pose for armatures
    pose: Pose
    
    # Action used as a pose library for armatures
    pose_library: Action
    
    # Library object this proxy object controls
    proxy: 'Object'
    
    # Library collection duplicator object this proxy object controls
    proxy_collection: 'Object'
    
    # Settings for rigid body simulation
    rigid_body: RigidBodyObject
    
    # Constraint constraining rigid bodies
    rigid_body_constraint: RigidBodyConstraint
    
    # Angle of Rotation for Axis-Angle rotation representation
    rotation_axis_angle: Tuple[float, float, float, float]
    
    # Rotation in Eulers
    rotation_euler: Tuple[float, float, float]
    
    rotation_mode: str = 'XYZ' # ['QUATERNION', 'XYZ', 'XZY', 'YXZ', 'YZX', 'ZXY', 'ZYX', 'AXIS_ANGLE']
    
    # Rotation in Quaternions
    rotation_quaternion: Tuple[float, float, float, float]
    
    # Scaling of the object
    scale: Tuple[float, float, float]
    
    # Effects affecting display of object
    shader_effects: ObjectShaderFx
    
    # Display all edges for mesh objects
    show_all_edges: bool
    
    # Display the object’s origin and axes
    show_axis: bool
    
    # Display the object’s bounds
    show_bounds: bool
    
    # Only display the image when it is aligned with the view axis
    show_empty_image_only_axis_aligned: bool
    
    # Display image in orthographic mode
    show_empty_image_orthographic: bool
    
    # Display image in perspective mode
    show_empty_image_perspective: bool
    
    # Make the object display in front of others
    show_in_front: bool
    
    # Make instancer visible when rendering
    show_instancer_for_render: bool
    
    # Make instancer visible in the viewport
    show_instancer_for_viewport: bool
    
    # Display the object’s name
    show_name: bool
    
    # Always show the current shape for this object
    show_only_shape_key: bool
    
    # Display the object’s texture space
    show_texture_space: bool
    
    # Display material transparency in the object
    show_transparent: bool
    
    # Display the object’s wireframe over solid shading
    show_wire: bool
    
    # Settings for soft body simulation
    soft_body: SoftBodySettings
    
    # Axis that points in the ‘forward’ direction (applies to Instance Vertices when Align to Vertex Normal is enabled)
    track_axis: str = 'POS_X' # ['POS_X', 'POS_Y', 'POS_Z', 'NEG_X', 'NEG_Y', 'NEG_Z']
    
    # Type of object
    type: str = 'EMPTY' # ['MESH', 'CURVE', 'SURFACE', 'META', 'FONT', 'HAIR', 'POINTCLOUD', 'VOLUME', 'GPENCIL', 'ARMATURE', 'LATTICE', 'EMPTY', 'LIGHT', 'LIGHT_PROBE', 'CAMERA', 'SPEAKER']
    
    # Axis that points in the upward direction (applies to Instance Vertices when Align to Vertex Normal is enabled)
    up_axis: str = 'X' # ['X', 'Y', 'Z']
    
    # View Lock 3D viewport camera transformation affects the object’s parent instead
    use_camera_lock_parent: bool
    
    use_dynamic_topology_sculpting: bool
    
    # Use alpha blending instead of alpha test (can produce sorting artifacts)
    use_empty_image_alpha: bool
    
    # Lights affect grease pencil object
    use_grease_pencil_lights: bool
    
    # Scale instance based on face size
    use_instance_faces_scale: bool
    
    # Rotate instance according to vertex normal
    use_instance_vertices_rotation: bool
    
    # Enable mesh symmetry in the X axis
    use_mesh_mirror_x: bool
    
    # Enable mesh symmetry in the Y axis
    use_mesh_mirror_y: bool
    
    # Enable mesh symmetry in the Z axis
    use_mesh_mirror_z: bool
    
    # Apply shape keys in edit mode (for meshes only)
    use_shape_key_edit_mode: bool
    
    # Vertex groups of the object
    vertex_groups: VertexGroups
    
    # All the children of this object.
    children: Any
    
    # The collections this object is in.
    users_collection: Any
    
    # The scenes this object is in.
    users_scene: Any

    def select_get(self, *args, **kwargs) -> bool:
        ...

    def select_set(self, *args, **kwargs) -> None:
        ...

    def hide_get(self, *args, **kwargs) -> bool:
        ...

    def hide_set(self, *args, **kwargs) -> None:
        ...

    def visible_get(self, *args, **kwargs) -> bool:
        ...

    def holdout_get(self, *args, **kwargs) -> bool:
        ...

    def indirect_only_get(self, *args, **kwargs) -> bool:
        ...

    def local_view_get(self, *args, **kwargs) -> bool:
        ...

    def local_view_set(self, *args, **kwargs) -> None:
        ...

    def visible_in_viewport_get(self, *args, **kwargs) -> bool:
        ...

    def convert_space(self, *args, **kwargs) -> List[float]:
        ...

    def calc_matrix_camera(self, *args, **kwargs) -> List[float]:
        ...

    def camera_fit_coords(self, *args, **kwargs) -> None:
        ...

    def to_mesh(self, *args, **kwargs) -> Mesh:
        ...

    def to_mesh_clear(self, *args, **kwargs) -> None:
        ...

    def to_curve(self, *args, **kwargs) -> Curve:
        ...

    def to_curve_clear(self, *args, **kwargs) -> None:
        ...

    def find_armature(self, *args, **kwargs) -> None:
        ...

    def shape_key_add(self, *args, **kwargs) -> ShapeKey:
        ...

    def shape_key_remove(self, *args, **kwargs) -> None:
        ...

    def shape_key_clear(self, *args, **kwargs) -> None:
        ...

    def ray_cast(self, *args, **kwargs) -> None:
        ...

    def closest_point_on_mesh(self, *args, **kwargs) -> None:
        ...

    def is_modified(self, *args, **kwargs) -> bool:
        ...

    def is_deform_modified(self, *args, **kwargs) -> bool:
        ...

    def update_from_editmode(self, *args, **kwargs) -> None:
        ...

    def cache_release(self, *args, **kwargs) -> None:
        ...

    def generate_gpencil_strokes(self, *args, **kwargs) -> bool:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...