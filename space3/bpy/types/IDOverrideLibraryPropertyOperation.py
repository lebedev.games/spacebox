from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class IDOverrideLibraryPropertyOperation(bpy_struct):
    
    # Optional flags (NOT USED)
    flag: str = 'MANDATORY' # ['MANDATORY', 'LOCKED']
    
    # What override operation is performed
    operation: str = 'REPLACE' # ['NOOP', 'REPLACE', 'DIFF_ADD', 'DIFF_SUB', 'FACT_MULTIPLY', 'INSERT_AFTER', 'INSERT_BEFORE']
    
    # Used to handle insertions into collection
    subitem_local_index: int
    
    # Used to handle insertions into collection
    subitem_local_name: str
    
    # Used to handle insertions into collection
    subitem_reference_index: int
    
    # Used to handle insertions into collection
    subitem_reference_name: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...