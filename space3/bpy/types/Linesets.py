from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Linesets(bpy_struct):
    
    # Active line set being displayed
    active: FreestyleLineSet
    
    # Index of active line set slot
    active_index: int

    def new(self, *args, **kwargs) -> FreestyleLineSet:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...