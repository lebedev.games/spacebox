from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CompositorNodeBlur(CompositorNode):
    
    # Type of aspect correction to use
    aspect_correction: str = 'NONE' # ['NONE', 'Y', 'X']
    
    factor: float
    
    factor_x: float
    
    factor_y: float
    
    filter_type: str = 'FLAT' # ['FLAT', 'TENT', 'QUAD', 'CUBIC', 'GAUSS', 'FAST_GAUSS', 'CATROM', 'MITCH']
    
    size_x: int
    
    size_y: int
    
    # Use circular filter (slower)
    use_bokeh: bool
    
    # Extend bounds of the input image to fully fit blurred image
    use_extended_bounds: bool
    
    # Apply filter on gamma corrected values
    use_gamma_correction: bool
    
    # Use relative (percent) values to define blur radius
    use_relative: bool
    
    # Support variable blur per pixel when using an image for size input
    use_variable_size: bool

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    def update(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...