from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ShaderNodeTexSky(ShaderNode):
    
    # Density of air molecules
    air_density: float
    
    # Height from sea level
    altitude: float
    
    # Color mapping settings
    color_mapping: ColorMapping
    
    # Density of dust molecules and water droplets
    dust_density: float
    
    # Ground color that is subtly reflected in the sky
    ground_albedo: float
    
    # Density of ozone layer
    ozone_density: float
    
    # Which sky model should be used
    sky_type: str = 'PREETHAM' # ['PREETHAM', 'HOSEK_WILKIE', 'NISHITA']
    
    # Direction from where the sun is shining
    sun_direction: Tuple[float, float, float]
    
    # Include the sun itself in the output
    sun_disc: bool
    
    # Sun angle from horizon
    sun_elevation: float
    
    # Strength of sun
    sun_intensity: float
    
    # Rotation of sun around zenith
    sun_rotation: float
    
    # Size of sun disc
    sun_size: float
    
    # Texture coordinate mapping settings
    texture_mapping: TexMapping
    
    # Atmospheric turbidity
    turbidity: float

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...