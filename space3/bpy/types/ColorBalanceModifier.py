from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ColorBalanceModifier(SequenceModifier):
    
    color_balance: SequenceColorBalanceData
    
    # Multiply the intensity of each pixel
    color_multiply: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...