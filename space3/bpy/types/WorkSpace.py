from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class WorkSpace(ID):
    
    # Switch to this object mode when activating the workspace
    object_mode: str = 'OBJECT' # ['OBJECT', 'EDIT', 'POSE', 'SCULPT', 'VERTEX_PAINT', 'WEIGHT_PAINT', 'TEXTURE_PAINT', 'PARTICLE_EDIT', 'EDIT_GPENCIL', 'SCULPT_GPENCIL', 'PAINT_GPENCIL', 'VERTEX_GPENCIL', 'WEIGHT_GPENCIL']
    
    owner_ids: wmOwnerIDs
    
    # Screen layouts of a workspace
    screens: List[Screen]
    
    tools: wmTools
    
    # Filter the UI by tags
    use_filter_by_owner: bool

    @classmethod
    def status_text_set_internal(cls, *args, **kwargs) -> None:
        ...

    def status_text_set(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...