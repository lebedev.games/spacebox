from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class WireframeModifier(Modifier):
    
    # Crease weight (if active)
    crease_weight: float
    
    # Invert vertex group influence
    invert_vertex_group: bool
    
    # Offset material index of generated faces
    material_offset: int
    
    # Offset the thickness from the center
    offset: float
    
    # Thickness factor
    thickness: float
    
    # Thickness factor to use for zero vertex group influence
    thickness_vertex_group: float
    
    # Support face boundaries
    use_boundary: bool
    
    # Crease hub edges for improved subdivision surface
    use_crease: bool
    
    # Scale the offset to give more even thickness
    use_even_offset: bool
    
    # Scale the offset by surrounding geometry
    use_relative_offset: bool
    
    # Remove original geometry
    use_replace: bool
    
    # Vertex group name for selecting the affected areas
    vertex_group: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...