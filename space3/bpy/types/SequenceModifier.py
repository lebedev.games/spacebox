from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SequenceModifier(bpy_struct):
    
    # Mask ID used as mask input for the modifier
    input_mask_id: Mask
    
    # Strip used as mask input for the modifier
    input_mask_strip: Sequence
    
    # Type of input data used for mask
    input_mask_type: str = 'STRIP' # ['STRIP', 'ID']
    
    # Time to use for the Mask animation
    mask_time: str = 'RELATIVE' # ['RELATIVE', 'ABSOLUTE']
    
    # Mute this modifier
    mute: bool
    
    name: str
    
    # Mute expanded settings for the modifier
    show_expanded: bool
    
    type: str = 'COLOR_BALANCE' # ['COLOR_BALANCE', 'CURVES', 'HUE_CORRECT', 'BRIGHT_CONTRAST', 'MASK', 'WHITE_BALANCE', 'TONEMAP']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...