from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ThemeConsole(bpy_struct):
    
    cursor: Tuple[float, float, float]
    
    line_error: Tuple[float, float, float]
    
    line_info: Tuple[float, float, float]
    
    line_input: Tuple[float, float, float]
    
    line_output: Tuple[float, float, float]
    
    select: Tuple[float, float, float, float]
    
    # Settings for space
    space: ThemeSpaceGeneric

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...