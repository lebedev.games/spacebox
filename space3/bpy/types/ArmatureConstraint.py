from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ArmatureConstraint(Constraint):
    
    # Target Bones
    targets: ArmatureConstraintTargets
    
    # Multiply weights by envelope for all bones, instead of acting like Vertex Group based blending. The specified weights are still used, and only the listed bones are considered
    use_bone_envelopes: bool
    
    # Use the current bone location for envelopes and choosing B-Bone segments instead of rest position
    use_current_location: bool
    
    # Deform rotation interpolation with quaternions
    use_deform_preserve_volume: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...