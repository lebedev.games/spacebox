from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Histogram(bpy_struct):
    
    # Channels to display in the histogram
    mode: str = 'LUMA' # ['LUMA', 'RGB', 'R', 'G', 'B', 'A']
    
    # Display lines rather than filled shapes
    show_line: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...