from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class PreferencesExperimental(bpy_struct):
    
    # Enable library overrides automatic resync detection and process on file load. Disable when dealing with older .blend files that need manual Resync (Enforce) handling
    override_auto_resync: bool
    
    # Enable Asset Browser editor and operators to manage data-blocks as asset
    use_asset_browser: bool
    
    # Enable Cycles debugging options for developers
    use_cycles_debug: bool
    
    # Enable the new hair type in the ui
    use_new_hair_type: bool
    
    # Enable the new point cloud type in the ui
    use_new_point_cloud_type: bool
    
    # Enable library override template in the python API
    use_override_templates: bool
    
    # Support for pen tablet tilt events in Sculpt Mode
    use_sculpt_tools_tilt: bool
    
    # Use the new Vertex Painting system
    use_sculpt_vertex_colors: bool
    
    # Use legacy undo (slower than the new default one, but may be more stable in some cases)
    use_undo_legacy: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...