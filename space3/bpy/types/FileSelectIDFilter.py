from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class FileSelectIDFilter(bpy_struct):
    
    # Show animation data
    category_animation: bool
    
    # Show worlds, lights, cameras and speakers
    category_environment: bool
    
    # Show meshes, curves, lattice, armatures and metaballs data
    category_geometry: bool
    
    # Show images, movie clips, sounds and masks
    category_image: bool
    
    # Show other data types
    category_misc: bool
    
    # Show objects and collections
    category_object: bool
    
    # Show scenes
    category_scene: bool
    
    # Show materials, nodetrees, textures and Freestyle’s linestyles
    category_shading: bool
    
    # Show Action data-blocks
    filter_action: bool
    
    # Show Armature data-blocks
    filter_armature: bool
    
    # Show Brushes data-blocks
    filter_brush: bool
    
    # Show Cache File data-blocks
    filter_cachefile: bool
    
    # Show Camera data-blocks
    filter_camera: bool
    
    # Show Curve data-blocks
    filter_curve: bool
    
    # Show Font data-blocks
    filter_font: bool
    
    # Show Grease pencil data-blocks
    filter_grease_pencil: bool
    
    # Show Collection data-blocks
    filter_group: bool
    
    # Show/hide Hair data-blocks
    filter_hair: bool
    
    # Show Image data-blocks
    filter_image: bool
    
    # Show Lattice data-blocks
    filter_lattice: bool
    
    # Show Light data-blocks
    filter_light: bool
    
    # Show Light Probe data-blocks
    filter_light_probe: bool
    
    # Show Freestyle’s Line Style data-blocks
    filter_linestyle: bool
    
    # Show Mask data-blocks
    filter_mask: bool
    
    # Show Material data-blocks
    filter_material: bool
    
    # Show Mesh data-blocks
    filter_mesh: bool
    
    # Show Metaball data-blocks
    filter_metaball: bool
    
    # Show Movie Clip data-blocks
    filter_movie_clip: bool
    
    # Show Node Tree data-blocks
    filter_node_tree: bool
    
    # Show Object data-blocks
    filter_object: bool
    
    # Show Paint Curve data-blocks
    filter_paint_curve: bool
    
    # Show Palette data-blocks
    filter_palette: bool
    
    # Show Particle Settings data-blocks
    filter_particle_settings: bool
    
    # Show/hide Point Cloud data-blocks
    filter_pointcloud: bool
    
    # Show Scene data-blocks
    filter_scene: bool
    
    # Show Simulation data-blocks
    filter_simulation: bool
    
    # Show Sound data-blocks
    filter_sound: bool
    
    # Show Speaker data-blocks
    filter_speaker: bool
    
    # Show Text data-blocks
    filter_text: bool
    
    # Show Texture data-blocks
    filter_texture: bool
    
    # Show/hide Volume data-blocks
    filter_volume: bool
    
    # Show workspace data-blocks
    filter_work_space: bool
    
    # Show World data-blocks
    filter_world: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...