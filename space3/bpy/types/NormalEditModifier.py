from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class NormalEditModifier(Modifier):
    
    # Invert vertex group influence
    invert_vertex_group: bool
    
    # How much of generated normals to mix with exiting ones
    mix_factor: float
    
    # Maximum angle between old and new normals
    mix_limit: float
    
    # How to mix generated normals with existing ones
    mix_mode: str = 'COPY' # ['COPY', 'ADD', 'SUB', 'MUL']
    
    # How to affect (generate) normals
    mode: str = 'RADIAL' # ['RADIAL', 'DIRECTIONAL']
    
    # Do not flip polygons when their normals are not consistent with their newly computed custom vertex normals
    no_polynors_fix: bool
    
    # Offset from object’s center
    offset: Tuple[float, float, float]
    
    # Target object used to affect normals
    target: Object
    
    # Use same direction for all normals, from origin to target’s center (Directional mode only)
    use_direction_parallel: bool
    
    # Vertex group name for selecting/weighting the affected areas
    vertex_group: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...