from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ActionConstraint(Constraint):
    
    # The constraining action
    action: Action
    
    # Interpolates between Action Start and End frames
    eval_time: float
    
    # Last frame of the Action to use
    frame_end: int
    
    # First frame of the Action to use
    frame_start: int
    
    # Maximum value for target channel range
    max: float
    
    # Minimum value for target channel range
    min: float
    
    # Specify how existing transformations and the action channels are combined
    mix_mode: str = 'AFTER_FULL' # ['BEFORE', 'AFTER', 'AFTER_FULL']
    
    # Armature bone, mesh or lattice vertex group, …
    subtarget: str
    
    # Target object
    target: Object
    
    # Transformation channel from the target that is used to key the Action
    transform_channel: str = 'ROTATION_X' # ['LOCATION_X', 'LOCATION_Y', 'LOCATION_Z', 'ROTATION_X', 'ROTATION_Y', 'ROTATION_Z', 'SCALE_X', 'SCALE_Y', 'SCALE_Z']
    
    # Bones only: apply the object’s transformation channels of the action to the constrained bone, instead of bone’s channels
    use_bone_object_action: bool
    
    # Interpolate between Action Start and End frames, with the Evaluation Time slider instead of the Target object/bone
    use_eval_time: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...