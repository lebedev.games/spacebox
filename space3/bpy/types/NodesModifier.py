from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class NodesModifier(Modifier):
    
    # Node group that controls what this modifier does
    node_group: NodeTree

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...