from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Paint(bpy_struct):
    
    # Active Brush
    brush: Brush
    
    # Editable cavity curve
    cavity_curve: CurveMapping
    
    # Average multiple input samples together to smooth the brush stroke
    input_samples: int
    
    # Active Palette
    palette: Palette
    
    show_brush: bool
    
    show_brush_on_surface: bool
    
    # For multires, show low resolution while navigating the view
    show_low_resolution: bool
    
    # Stride at which tiled strokes are copied
    tile_offset: Tuple[float, float, float]
    
    # Tile along X axis
    tile_x: bool
    
    # Tile along Y axis
    tile_y: bool
    
    # Tile along Z axis
    tile_z: bool
    
    tool_slots: List[PaintToolSlot]
    
    # Mask painting according to mesh geometry cavity
    use_cavity: bool
    
    # Update the geometry when it enters the view, providing faster view navigation
    use_sculpt_delay_updates: bool
    
    # Reduce the strength of the brush where it overlaps symmetrical daubs
    use_symmetry_feather: bool
    
    # Mirror brush across the X axis
    use_symmetry_x: bool
    
    # Mirror brush across the Y axis
    use_symmetry_y: bool
    
    # Mirror brush across the Z axis
    use_symmetry_z: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...