from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class LineStyleThicknessModifier_Calligraphy(LineStyleThicknessModifier):
    
    # Specify how the modifier value is blended into the base value
    blend: str = 'MIX' # ['MIX', 'ADD', 'SUBTRACT', 'MULTIPLY', 'DIVIDE', 'DIFFERENCE', 'MINIMUM', 'MAXIMUM']
    
    # True if the modifier tab is expanded
    expanded: bool
    
    # Influence factor by which the modifier changes the property
    influence: float
    
    # Name of the modifier
    name: str
    
    # Angle of the main direction
    orientation: float
    
    # Maximum thickness in the main direction
    thickness_max: float
    
    # Minimum thickness in the direction perpendicular to the main direction
    thickness_min: float
    
    # Type of the modifier
    type: str = 'ALONG_STROKE' # ['ALONG_STROKE', 'CALLIGRAPHY', 'CREASE_ANGLE', 'CURVATURE_3D', 'DISTANCE_FROM_CAMERA', 'DISTANCE_FROM_OBJECT', 'MATERIAL', 'NOISE', 'TANGENT']
    
    # Enable or disable this modifier during stroke rendering
    use: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...