from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class GpencilModifier(bpy_struct):
    
    # Modifier name
    name: str
    
    # Set modifier expanded in the user interface
    show_expanded: bool
    
    # Display modifier in Edit mode
    show_in_editmode: bool
    
    # Use modifier during render
    show_render: bool
    
    # Display modifier in viewport
    show_viewport: bool
    
    type: str = 'GP_ARRAY' # ['GP_ARRAY', 'GP_BUILD', 'GP_LINEART', 'GP_MIRROR', 'GP_MULTIPLY', 'GP_SIMPLIFY', 'GP_SUBDIV', 'GP_ARMATURE', 'GP_HOOK', 'GP_LATTICE', 'GP_NOISE', 'GP_OFFSET', 'GP_SMOOTH', 'GP_THICK', 'GP_TIME', 'GP_COLOR', 'GP_OPACITY', 'GP_TEXTURE', 'GP_TINT']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...