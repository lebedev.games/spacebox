from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class UI_UL_list(UIList):

    def filter_items_by_name(self, *args, **kwargs) -> None:
        ...

    def sort_items_helper(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...