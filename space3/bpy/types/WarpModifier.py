from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class WarpModifier(Modifier):
    
    # Bone to transform from
    bone_from: str
    
    # Bone defining offset
    bone_to: str
    
    # Custom falloff curve
    falloff_curve: CurveMapping
    
    # Radius to apply
    falloff_radius: float
    
    falloff_type: str = 'SMOOTH' # ['NONE', 'CURVE', 'SMOOTH', 'SPHERE', 'ROOT', 'INVERSE_SQUARE', 'SHARP', 'LINEAR', 'CONSTANT']
    
    # Invert vertex group influence
    invert_vertex_group: bool
    
    # Object to transform from
    object_from: Object
    
    # Object to transform to
    object_to: Object
    
    strength: float
    
    texture: Texture
    
    texture_coords: str = 'LOCAL' # ['LOCAL', 'GLOBAL', 'OBJECT', 'UV']
    
    # Bone to set the texture coordinates
    texture_coords_bone: str
    
    # Object to set the texture coordinates
    texture_coords_object: Object
    
    # Preserve volume when rotations are used
    use_volume_preserve: bool
    
    # UV map name
    uv_layer: str
    
    # Vertex group name for modulating the deform
    vertex_group: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...