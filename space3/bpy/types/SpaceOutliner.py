from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SpaceOutliner(Space):
    
    # Type of information to display
    display_mode: str = 'SCENES' # ['SCENES', 'VIEW_LAYER', 'SEQUENCE', 'LIBRARIES', 'DATA_API', 'LIBRARY_OVERRIDES', 'ORPHAN_DATA']
    
    # Data-block type to show
    filter_id_type: str = 'ACTION' # ['ACTION', 'ARMATURE', 'BRUSH', 'CAMERA', 'CACHEFILE', 'CURVE', 'FONT', 'GREASEPENCIL', 'COLLECTION', 'IMAGE', 'KEY', 'LIGHT', 'LIBRARY', 'LINESTYLE', 'LATTICE', 'MASK', 'MATERIAL', 'META', 'MESH', 'MOVIECLIP', 'NODETREE', 'OBJECT', 'PAINTCURVE', 'PALETTE', 'PARTICLE', 'LIGHT_PROBE', 'SCENE', 'SIMULATION', 'SOUND', 'SPEAKER', 'TEXT', 'TEXTURE', 'HAIR', 'POINTCLOUD', 'VOLUME', 'WINDOWMANAGER', 'WORLD', 'WORKSPACE']
    
    # Invert the object state filter
    filter_invert: bool
    
    filter_state: str = 'ALL' # ['ALL', 'VISIBLE', 'SELECTED', 'ACTIVE', 'SELECTABLE']
    
    # Live search filtering string
    filter_text: str
    
    # Show the mode column for mode toggle and activation
    show_mode_column: bool
    
    # Exclude from view layer
    show_restrict_column_enable: bool
    
    # Temporarily hide in viewport
    show_restrict_column_hide: bool
    
    # Holdout
    show_restrict_column_holdout: bool
    
    # Indirect only
    show_restrict_column_indirect_only: bool
    
    # Globally disable in renders
    show_restrict_column_render: bool
    
    # Selectable
    show_restrict_column_select: bool
    
    # Globally disable in viewports
    show_restrict_column_viewport: bool
    
    # Only use case sensitive matches of search string
    use_filter_case_sensitive: bool
    
    # Show children
    use_filter_children: bool
    
    # Show collections
    use_filter_collection: bool
    
    # Only use complete matches of search string
    use_filter_complete: bool
    
    # Show only data-blocks of one type
    use_filter_id_type: bool
    
    # For libraries with overrides created, show the overridden values
    use_filter_lib_override: bool
    
    # For libraries with overrides created, show the overridden values that are defined/controlled automatically (e.g. to make users of an overridden data-block point to the override data, not the original linked data)
    use_filter_lib_override_system: bool
    
    # Show objects
    use_filter_object: bool
    
    # Show armature objects
    use_filter_object_armature: bool
    
    # Show camera objects
    use_filter_object_camera: bool
    
    # Show what is inside the objects elements
    use_filter_object_content: bool
    
    # Show empty objects
    use_filter_object_empty: bool
    
    # Show light objects
    use_filter_object_light: bool
    
    # Show mesh objects
    use_filter_object_mesh: bool
    
    # Show curves, lattices, light probes, fonts, …
    use_filter_object_others: bool
    
    use_sort_alpha: bool
    
    # Sync outliner selection with other editors
    use_sync_select: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...

    def draw_handler_add(self, *args, **kwargs) -> Any:
        ...

    def draw_handler_remove(self, *args, **kwargs) -> None:
        ...