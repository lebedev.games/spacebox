from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class LayerCollection(bpy_struct):
    
    # Child layer collections
    children: List[LayerCollection]
    
    # Collection this layer collection is wrapping
    collection: Collection
    
    # Exclude from view layer
    exclude: bool
    
    # Temporarily hide in viewport
    hide_viewport: bool
    
    # Mask out objects in collection from view layer
    holdout: bool
    
    # Objects in collection only contribute indirectly (through shadows and reflections) in the view layer
    indirect_only: bool
    
    # Whether this collection is visible for the view layer, take into account the collection parent
    is_visible: bool
    
    # Name of this view layer (same as its collection one)
    name: str

    def visible_get(self, *args, **kwargs) -> bool:
        ...

    def has_objects(self, *args, **kwargs) -> bool:
        ...

    def has_selected_objects(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...