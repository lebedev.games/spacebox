from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Brush(ID):
    
    # Ratio between the brush radius and the radius that is going to be used to sample the area center
    area_radius_factor: float
    
    # Amount of smoothing to automatically apply to each stroke
    auto_smooth_factor: float
    
    # Distance where boundary edge automasking is going to protect vertices from the fully masked edge
    automasking_boundary_edges_propagation_steps: int
    
    # Brush blending mode
    blend: str = 'MIX' # ['MIX', 'DARKEN', 'MUL', 'COLORBURN', 'LINEARBURN', 'LIGHTEN', 'SCREEN', 'COLORDODGE', 'ADD', 'OVERLAY', 'SOFTLIGHT', 'HARDLIGHT', 'VIVIDLIGHT', 'LINEARLIGHT', 'PINLIGHT', 'DIFFERENCE', 'EXCLUSION', 'SUB', 'HUE', 'SATURATION', 'COLOR', 'LUMINOSITY', 'ERASE_ALPHA', 'ADD_ALPHA']
    
    # Radius of kernel used for soften and sharpen in pixels
    blur_kernel_radius: int
    
    blur_mode: str = 'GAUSSIAN' # ['BOX', 'GAUSSIAN']
    
    # Deformation type that is used in the brush
    boundary_deform_type: str = 'BEND' # ['BEND', 'EXPAND', 'INFLATE', 'GRAB', 'TWIST', 'SMOOTH']
    
    # How the brush falloff is applied across the boundary
    boundary_falloff_type: str = 'CONSTANT' # ['CONSTANT', 'RADIUS', 'LOOP', 'LOOP_INVERT']
    
    # Offset of the boundary origin in relation to the brush radius
    boundary_offset: float
    
    # Brush’s capabilities
    brush_capabilities: BrushCapabilities
    
    # Opacity of clone image display
    clone_alpha: float
    
    # Image for clone tool
    clone_image: Image
    
    clone_offset: Tuple[float, float]
    
    # How much the cloth preserves the original shape, acting as a soft body
    cloth_constraint_softbody_strength: float
    
    # How much the applied forces are propagated through the cloth
    cloth_damping: float
    
    # Deformation type that is used in the brush
    cloth_deform_type: str = 'DRAG' # ['DRAG', 'PUSH', 'PINCH_POINT', 'PINCH_PERPENDICULAR', 'INFLATE', 'GRAB', 'EXPAND', 'SNAKE_HOOK']
    
    # Shape used in the brush to apply force to the cloth
    cloth_force_falloff_type: str = 'RADIAL' # ['RADIAL', 'PLANE']
    
    # Mass of each simulation particle
    cloth_mass: float
    
    # Area to apply deformation falloff to the effects of the simulation
    cloth_sim_falloff: float
    
    # Factor added relative to the size of the radius to limit the cloth simulation effects
    cloth_sim_limit: float
    
    # Part of the mesh that is going to be simulated when the stroke is active
    cloth_simulation_area_type: str = 'LOCAL' # ['LOCAL', 'GLOBAL', 'DYNAMIC']
    
    color: Tuple[float, float, float]
    
    # Use single color or gradient when painting
    color_type: str = 'COLOR' # ['COLOR', 'GRADIENT']
    
    # How much the crease brush pinches
    crease_pinch_factor: float
    
    # Color of cursor when adding
    cursor_color_add: Tuple[float, float, float, float]
    
    # Color of cursor when subtracting
    cursor_color_subtract: Tuple[float, float, float, float]
    
    cursor_overlay_alpha: int
    
    # Editable falloff curve
    curve: CurveMapping
    
    curve_preset: str = 'CUSTOM' # ['CUSTOM', 'SMOOTH', 'SMOOTHER', 'SPHERE', 'ROOT', 'SHARP', 'LIN', 'POW4', 'INVSQUARE', 'CONSTANT']
    
    # Ratio of samples in a cycle that the brush is enabled
    dash_ratio: float
    
    # Length of a dash cycle measured in stroke samples
    dash_samples: int
    
    # How the deformation of the brush will affect the object
    deform_target: str = 'GEOMETRY' # ['GEOMETRY', 'CLOTH_SIM']
    
    # Amount of random elements that are going to be affected by the brush
    density: float
    
    direction: str = 'ADD' # ['ADD', 'SUBTRACT']
    
    # Maximum distance to search for disconnected loose parts in the mesh
    disconnected_distance_max: float
    
    # Deformation type that is used in the brush
    elastic_deform_type: str = 'GRAB' # ['GRAB', 'GRAB_BISCALE', 'GRAB_TRISCALE', 'SCALE', 'TWIST']
    
    # Poisson ratio for elastic deformation. Higher values preserve volume more, but also lead to more bulging
    elastic_deform_volume_preservation: float
    
    # Paint most on faces pointing towards the view according to this angle
    falloff_angle: float
    
    # Use projected or spherical falloff
    falloff_shape: str = 'SPHERE' # ['SPHERE', 'PROJECTED']
    
    # Threshold above which filling is not propagated
    fill_threshold: float
    
    # Amount of paint that is applied per stroke sample
    flow: float
    
    gpencil_sculpt_tool: str = 'SMOOTH' # ['SMOOTH', 'THICKNESS', 'STRENGTH', 'RANDOMIZE', 'GRAB', 'PUSH', 'TWIST', 'PINCH', 'CLONE']
    
    gpencil_settings: BrushGpencilSettings
    
    gpencil_tool: str = 'DRAW' # ['DRAW', 'FILL', 'ERASE', 'TINT']
    
    gpencil_vertex_tool: str = 'DRAW' # ['DRAW', 'BLUR', 'AVERAGE', 'SMEAR', 'REPLACE']
    
    gpencil_weight_tool: str = 'WEIGHT' # ['WEIGHT']
    
    # Spacing before brush gradient goes full circle
    grad_spacing: int
    
    gradient: ColorRamp
    
    gradient_fill_mode: str = 'LINEAR' # ['LINEAR', 'RADIAL']
    
    gradient_stroke_mode: str = 'PRESSURE' # ['PRESSURE', 'SPACING_REPEAT', 'SPACING_CLAMP']
    
    # How close the brush falloff starts from the edge of the brush
    hardness: float
    
    # Affectable height of brush (layer height for layer tool, i.e.)
    height: float
    
    # File path to brush icon
    icon_filepath: str
    
    image_paint_capabilities: BrushCapabilitiesImagePaint
    
    image_tool: str = 'DRAW' # ['DRAW', 'SOFTEN', 'SMEAR', 'CLONE', 'FILL', 'MASK']
    
    # Invert the modulation of pressure in density
    invert_density_pressure: bool
    
    # Invert the modulation of pressure in flow
    invert_flow_pressure: bool
    
    # Invert the modulation of pressure in hardness
    invert_hardness_pressure: bool
    
    # Use Scrape or Fill tool when inverting this brush instead of inverting its displacement direction
    invert_to_scrape_fill: bool
    
    # Invert the modulation of pressure in wet mix
    invert_wet_mix_pressure: bool
    
    # Invert the modulation of pressure in wet persistence
    invert_wet_persistence_pressure: bool
    
    # Jitter the position of the brush while painting
    jitter: float
    
    # Jitter the position of the brush in pixels while painting
    jitter_absolute: int
    
    # Jitter in screen space or relative to brush size
    jitter_unit: str = 'BRUSH' # ['VIEW', 'BRUSH']
    
    mask_overlay_alpha: int
    
    # Dimensions of mask stencil in viewport
    mask_stencil_dimension: Tuple[float, float]
    
    # Position of mask stencil in viewport
    mask_stencil_pos: Tuple[float, float]
    
    mask_texture: Texture
    
    mask_texture_slot: BrushTextureSlot
    
    mask_tool: str = 'DRAW' # ['DRAW', 'SMOOTH']
    
    # Angle between the planes of the crease
    multiplane_scrape_angle: float
    
    # Ratio between the brush radius and the radius that is going to be used to sample the normal
    normal_radius_factor: float
    
    # How much grab will pull vertexes out of surface during a grab
    normal_weight: float
    
    # Active paint curve
    paint_curve: PaintCurve
    
    # Adjust plane on which the brush acts towards or away from the object surface
    plane_offset: float
    
    # If a vertex is further away from offset plane than this, then it is not affected
    plane_trim: float
    
    # Deformation type that is used in the brush
    pose_deform_type: str = 'ROTATE_TWIST' # ['ROTATE_TWIST', 'SCALE_TRANSLATE', 'SQUASH_STRETCH']
    
    # Number of segments of the inverse kinematics chain that will deform the mesh
    pose_ik_segments: int
    
    # Offset of the pose origin in relation to the brush radius
    pose_offset: float
    
    # Method to set the rotation origins for the segments of the brush
    pose_origin_type: str = 'TOPOLOGY' # ['TOPOLOGY', 'FACE_SETS', 'FACE_SETS_FK']
    
    # Smooth iterations applied after calculating the pose factor of each vertex
    pose_smooth_iterations: int
    
    # How much grab will follow cursor rotation
    rake_factor: float
    
    # Interval between paints for Airbrush
    rate: float
    
    sculpt_capabilities: BrushCapabilitiesSculpt
    
    sculpt_plane: str = 'AREA' # ['AREA', 'VIEW', 'X', 'Y', 'Z']
    
    sculpt_tool: str = 'DRAW' # ['DRAW', 'DRAW_SHARP', 'CLAY', 'CLAY_STRIPS', 'CLAY_THUMB', 'LAYER', 'INFLATE', 'BLOB', 'CREASE', 'SMOOTH', 'FLATTEN', 'FILL', 'SCRAPE', 'MULTIPLANE_SCRAPE', 'PINCH', 'GRAB', 'ELASTIC_DEFORM', 'SNAKE_HOOK', 'THUMB', 'POSE', 'NUDGE', 'ROTATE', 'TOPOLOGY', 'BOUNDARY', 'CLOTH', 'SIMPLIFY', 'MASK', 'DISPLACEMENT_ERASER', 'DISPLACEMENT_SMEAR', 'PAINT', 'SMEAR', 'DRAW_FACE_SETS']
    
    secondary_color: Tuple[float, float, float]
    
    # Threshold below which, no sharpening is done
    sharp_threshold: float
    
    # Preview the scrape planes in the cursor during the stroke
    show_multiplane_scrape_planes_preview: bool
    
    # Radius of the brush in pixels
    size: int
    
    # Deformation type that is used in the brush
    slide_deform_type: str = 'DRAG' # ['DRAG', 'PINCH', 'EXPAND']
    
    # Deformation type that is used in the brush
    smear_deform_type: str = 'DRAG' # ['DRAG', 'PINCH', 'EXPAND']
    
    # Deformation type that is used in the brush
    smooth_deform_type: str = 'LAPLACIAN' # ['LAPLACIAN', 'SURFACE']
    
    # Higher values give a smoother stroke
    smooth_stroke_factor: float
    
    # Minimum distance from last point before stroke continues
    smooth_stroke_radius: int
    
    # Deformation type that is used in the brush
    snake_hook_deform_type: str = 'FALLOFF' # ['FALLOFF', 'ELASTIC']
    
    # Spacing between brush daubs as a percentage of brush diameter
    spacing: int
    
    # Dimensions of stencil in viewport
    stencil_dimension: Tuple[float, float]
    
    # Position of stencil in viewport
    stencil_pos: Tuple[float, float]
    
    # How powerful the effect of the brush is when applied
    strength: float
    
    stroke_method: str = 'DOTS' # ['DOTS', 'DRAG_DOT', 'SPACE', 'AIRBRUSH', 'ANCHORED', 'LINE', 'CURVE']
    
    # How much the position of each individual vertex influences the final result
    surface_smooth_current_vertex: float
    
    # Number of smoothing iterations per brush step
    surface_smooth_iterations: int
    
    # How much of the original shape is preserved when smoothing
    surface_smooth_shape_preservation: float
    
    texture: Texture
    
    texture_overlay_alpha: int
    
    # Value added to texture samples
    texture_sample_bias: float
    
    texture_slot: BrushTextureSlot
    
    # How much the tilt of the pen will affect the brush
    tilt_strength_factor: float
    
    # Roundness of the brush tip
    tip_roundness: float
    
    # Scale of the brush tip in the X axis
    tip_scale_x: float
    
    # Automatically align edges to the brush direction to generate cleaner topology and define sharp features. Best used on low-poly meshes as it has a performance impact
    topology_rake_factor: float
    
    # Radius of brush in Blender units
    unprojected_radius: float
    
    # Accumulate stroke daubs on top of each other
    use_accumulate: bool
    
    # Space daubs according to surface orientation instead of screen space
    use_adaptive_space: bool
    
    # Keep applying paint effect while holding mouse (spray)
    use_airbrush: bool
    
    # When this is disabled, lock alpha while painting
    use_alpha: bool
    
    # Keep the brush anchored to the initial location
    use_anchor: bool
    
    # Do not affect non manifold boundary edges
    use_automasking_boundary_edges: bool
    
    # Do not affect vertices that belong to a Face Set boundary
    use_automasking_boundary_face_sets: bool
    
    # Affect only vertices that share Face Sets with the active vertex
    use_automasking_face_sets: bool
    
    # Affect only vertices connected to the active vertex under the brush
    use_automasking_topology: bool
    
    # Collide with objects during the simulation
    use_cloth_collision: bool
    
    # Lock the position of the vertices in the simulation falloff area to avoid artifacts and create a softer transition with unaffected areas
    use_cloth_pin_simulation_boundary: bool
    
    # Affect only topologically connected elements
    use_connected_only: bool
    
    # Show cursor in viewport
    use_cursor_overlay: bool
    
    # Don’t show overlay during a stroke
    use_cursor_overlay_override: bool
    
    # Define the stroke curve with a bezier curve. Dabs are separated according to spacing
    use_curve: bool
    
    # Set the brush icon from an image file
    use_custom_icon: bool
    
    # Use pressure to modulate density
    use_density_pressure: bool
    
    # Drag anchor brush from edge-to-edge
    use_edge_to_edge: bool
    
    # Use pressure to modulate flow
    use_flow_pressure: bool
    
    # Brush only affects vertexes that face the viewer
    use_frontface: bool
    
    # Blend brush influence by how much they face the front
    use_frontface_falloff: bool
    
    # Apply the maximum grab strength to the active vertex instead of the cursor location
    use_grab_active_vertex: bool
    
    # Grabs trying to automask the silhouette of the object
    use_grab_silhouette: bool
    
    # Use pressure to modulate hardness
    use_hardness_pressure: bool
    
    # Lighter pressure causes more smoothing to be applied
    use_inverse_smooth_pressure: bool
    
    # Draw a line with dabs separated according to spacing
    use_line: bool
    
    # Measure brush size relative to the view or the scene
    use_locked_size: str = 'VIEW' # ['VIEW', 'SCENE']
    
    # The angle between the planes changes during the stroke to fit the surface under the cursor
    use_multiplane_scrape_dynamic: bool
    
    # Enable tablet pressure sensitivity for offset
    use_offset_pressure: bool
    
    # When locked keep using normal of surface where stroke was initiated
    use_original_normal: bool
    
    # When locked keep using the plane origin of surface where stroke was initiated
    use_original_plane: bool
    
    # Smooths the edges of the strokes
    use_paint_antialiasing: bool
    
    # Use this brush in grease pencil drawing mode
    use_paint_grease_pencil: bool
    
    # Use this brush in texture paint mode
    use_paint_image: bool
    
    # Use this brush in sculpt mode
    use_paint_sculpt: bool
    
    # Use this brush in UV sculpt mode
    use_paint_uv_sculpt: bool
    
    # Use this brush in vertex paint mode
    use_paint_vertex: bool
    
    # Use this brush in weight paint mode
    use_paint_weight: bool
    
    # Sculpt on a persistent layer of the mesh
    use_persistent: bool
    
    # Enable Plane Trim
    use_plane_trim: bool
    
    # Keep the position of the last segment in the IK chain fixed
    use_pose_ik_anchored: bool
    
    # Do not rotate the segment when using the scale deform mode
    use_pose_lock_rotation: bool
    
    # Enable tablet pressure sensitivity for area radius
    use_pressure_area_radius: bool
    
    # Enable tablet pressure sensitivity for jitter
    use_pressure_jitter: bool
    
    # Pen pressure makes texture influence smaller
    use_pressure_masking: str = 'NONE' # ['NONE', 'RAMP', 'CUTOFF']
    
    # Enable tablet pressure sensitivity for size
    use_pressure_size: bool
    
    # Enable tablet pressure sensitivity for spacing
    use_pressure_spacing: bool
    
    # Enable tablet pressure sensitivity for strength
    use_pressure_strength: bool
    
    # Show texture in viewport
    use_primary_overlay: bool
    
    # Don’t show overlay during a stroke
    use_primary_overlay_override: bool
    
    # Allow a single dot to be carefully positioned
    use_restore_mesh: bool
    
    # Calculate the brush spacing using view or scene distance
    use_scene_spacing: str = 'VIEW' # ['VIEW', 'SCENE']
    
    # Show texture in viewport
    use_secondary_overlay: bool
    
    # Don’t show overlay during a stroke
    use_secondary_overlay_override: bool
    
    # Brush lags behind mouse and follows a smoother path
    use_smooth_stroke: bool
    
    # Limit brush application to the distance specified by spacing
    use_space: bool
    
    # Automatically adjust strength to give consistent results for different spacings
    use_space_attenuation: bool
    
    # Use this brush in grease pencil vertex color mode
    use_vertex_grease_pencil: bool
    
    # Use pressure to modulate wet mix
    use_wet_mix_pressure: bool
    
    # Use pressure to modulate wet persistence
    use_wet_persistence_pressure: bool
    
    uv_sculpt_tool: str = 'GRAB' # ['GRAB', 'RELAX', 'PINCH']
    
    vertex_paint_capabilities: BrushCapabilitiesVertexPaint
    
    vertex_tool: str = 'DRAW' # ['DRAW', 'BLUR', 'AVERAGE', 'SMEAR']
    
    # Vertex weight when brush is applied
    weight: float
    
    weight_paint_capabilities: BrushCapabilitiesWeightPaint
    
    weight_tool: str = 'DRAW' # ['DRAW', 'BLUR', 'AVERAGE', 'SMEAR']
    
    # Amount of paint that is picked from the surface into the brush color
    wet_mix: float
    
    # Ratio between the brush radius and the radius that is going to be used to sample the color to blend in wet paint
    wet_paint_radius_factor: float
    
    # Amount of wet paint that stays in the brush after applying paint to the surface
    wet_persistence: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...