from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class DisplaceModifier(Modifier):
    
    direction: str = 'NORMAL' # ['X', 'Y', 'Z', 'NORMAL', 'CUSTOM_NORMAL', 'RGB_TO_XYZ']
    
    # Invert vertex group influence
    invert_vertex_group: bool
    
    # Material value that gives no displacement
    mid_level: float
    
    space: str = 'LOCAL' # ['LOCAL', 'GLOBAL']
    
    # Amount to displace geometry
    strength: float
    
    texture: Texture
    
    texture_coords: str = 'LOCAL' # ['LOCAL', 'GLOBAL', 'OBJECT', 'UV']
    
    # Bone to set the texture coordinates
    texture_coords_bone: str
    
    # Object to set the texture coordinates
    texture_coords_object: Object
    
    # UV map name
    uv_layer: str
    
    # Name of Vertex Group which determines influence of modifier per point
    vertex_group: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...