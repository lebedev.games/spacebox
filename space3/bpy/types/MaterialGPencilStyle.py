from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MaterialGPencilStyle(bpy_struct):
    
    # Defines how align Dots and Boxes with drawing path and object rotation
    alignment_mode: str = 'PATH' # ['PATH', 'OBJECT', 'FIXED']
    
    # Additional rotation applied to dots and square strokes
    alignment_rotation: float
    
    color: Tuple[float, float, float, float]
    
    # Color for filling region bounded by each stroke
    fill_color: Tuple[float, float, float, float]
    
    fill_image: Image
    
    # Select style used to fill strokes
    fill_style: str = 'SOLID' # ['SOLID', 'GRADIENT', 'TEXTURE']
    
    # Flip filling colors
    flip: bool
    
    # Display strokes using this color when showing onion skins
    ghost: bool
    
    # Select type of gradient used to fill strokes
    gradient_type: str = 'LINEAR' # ['LINEAR', 'RADIAL']
    
    # Set color Visibility
    hide: bool
    
    # True when opacity of fill is set high enough to be visible
    is_fill_visible: bool
    
    # True when opacity of stroke is set high enough to be visible
    is_stroke_visible: bool
    
    # Protect color from further editing and/or frame changes
    lock: bool
    
    # Color for mixing with primary filling color
    mix_color: Tuple[float, float, float, float]
    
    # Mix Factor
    mix_factor: float
    
    # Mix Stroke Factor
    mix_stroke_factor: float
    
    # Select line type for strokes
    mode: str = 'LINE' # ['LINE', 'DOTS', 'BOX']
    
    # Index number for the “Color Index” pass
    pass_index: int
    
    # Texture Pixel Size factor along the stroke
    pixel_size: float
    
    # Show stroke fills of this material
    show_fill: bool
    
    # Show stroke lines of this material
    show_stroke: bool
    
    stroke_image: Image
    
    # Select style used to draw strokes
    stroke_style: str = 'SOLID' # ['SOLID', 'TEXTURE']
    
    # Texture Orientation Angle
    texture_angle: float
    
    # Do not repeat texture and clamp to one instance only
    texture_clamp: bool
    
    # Shift Texture in 2d Space
    texture_offset: Tuple[float, float]
    
    # Scale Factor for Texture
    texture_scale: Tuple[float, float]
    
    # Remove the color from underneath this stroke by using it as a mask
    use_fill_holdout: bool
    
    # Disable stencil and overlap self intersections with alpha materials
    use_overlap_strokes: bool
    
    # Remove the color from underneath this stroke by using it as a mask
    use_stroke_holdout: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...