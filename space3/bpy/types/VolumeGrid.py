from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class VolumeGrid(bpy_struct):
    
    # Number of dimensions of the grid data type
    channels: int
    
    # Data type of voxel values
    data_type: str = 'UNKNOWN' # ['BOOLEAN', 'FLOAT', 'DOUBLE', 'INT', 'INT64', 'MASK', 'STRING', 'VECTOR_FLOAT', 'VECTOR_DOUBLE', 'VECTOR_INT', 'POINTS', 'UNKNOWN']
    
    # Grid tree is loaded in memory
    is_loaded: bool
    
    # Transformation matrix from voxel index to object space
    matrix_object: List[float]
    
    # Volume grid name
    name: str

    def load(self, *args, **kwargs) -> None:
        ...

    def unload(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...