from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ShaderFxRim(ShaderFx):
    
    # Number of pixels for blurring rim (set to 0 to disable)
    blur: Tuple[int, int]
    
    # Color that must be kept
    mask_color: Tuple[float, float, float]
    
    # Blend mode
    mode: str = 'NORMAL' # ['NORMAL', 'OVERLAY', 'ADD', 'SUBTRACT', 'MULTIPLY', 'DIVIDE']
    
    # Offset of the rim
    offset: Tuple[int, int]
    
    # Color used for Rim
    rim_color: Tuple[float, float, float]
    
    # Number of Blur Samples (zero, disable blur)
    samples: int

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...