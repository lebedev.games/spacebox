from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class UnitSettings(bpy_struct):
    
    # Unit that will be used to display length values
    length_unit: str = 'DEFAULT' # ['DEFAULT']
    
    # Unit that will be used to display mass values
    mass_unit: str = 'DEFAULT' # ['DEFAULT']
    
    # Scale to use when converting between blender units and dimensions. When working at microscopic or astronomical scale, a small or large unit scale respectively can be used to avoid numerical precision problems
    scale_length: float
    
    # The unit system to use for user interface controls
    system: str = 'NONE' # ['NONE', 'METRIC', 'IMPERIAL']
    
    # Unit to use for displaying/editing rotation values
    system_rotation: str = 'DEGREES' # ['DEGREES', 'RADIANS']
    
    # Unit that will be used to display temperature values
    temperature_unit: str = 'DEFAULT' # ['DEFAULT']
    
    # Unit that will be used to display time values
    time_unit: str = 'DEFAULT' # ['DEFAULT']
    
    # Display units in pairs (e.g. 1m 0cm)
    use_separate: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...