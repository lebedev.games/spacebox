from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class bpy_prop_collection(bpy_struct):

    def find(self, *args, **kwargs) -> Any:
        ...

    def foreach_get(self, *args, **kwargs) -> None:
        ...

    def foreach_set(self, *args, **kwargs) -> None:
        ...

    def get(self, *args, **kwargs) -> None:
        ...

    def items(self, *args, **kwargs) -> None:
        ...

    def keys(self, *args, **kwargs) -> None:
        ...

    def values(self, *args, **kwargs) -> None:
        ...