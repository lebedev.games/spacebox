from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ParticleBrush(bpy_struct):
    
    # Particle count
    count: int
    
    curve: CurveMapping
    
    length_mode: str = 'GROW' # ['GROW', 'SHRINK']
    
    puff_mode: str = 'ADD' # ['ADD', 'SUB']
    
    # Radius of the brush in pixels
    size: int
    
    # Brush steps
    steps: int
    
    # Brush strength
    strength: float
    
    # Apply puff to unselected end-points (helps maintain hair volume when puffing root)
    use_puff_volume: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...