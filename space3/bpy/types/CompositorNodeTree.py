from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CompositorNodeTree(NodeTree):
    
    # Max size of a tile (smaller values gives better distribution of multiple threads, but more overhead)
    chunk_size: str = '32' # ['32', '64', '128', '256', '512', '1024']
    
    # Quality when editing
    edit_quality: str = 'HIGH' # ['HIGH', 'MEDIUM', 'LOW']
    
    # Quality when rendering
    render_quality: str = 'HIGH' # ['HIGH', 'MEDIUM', 'LOW']
    
    # Enable buffering of group nodes
    use_groupnode_buffer: bool
    
    # Enable GPU calculations
    use_opencl: bool
    
    # Use two pass execution during editing: first calculate fast nodes, second pass calculate all nodes
    use_two_pass: bool
    
    # Use boundaries for viewer nodes and composite backdrop
    use_viewer_border: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...