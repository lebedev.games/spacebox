from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Menu(bpy_struct):
    
    bl_description: str
    
    # If this is set, the menu gets a custom ID, otherwise it takes the name of the class used to define the menu (for example, if the class name is “OBJECT_MT_hello”, and bl_idname is not set by the script, then bl_idname = “OBJECT_MT_hello”)
    bl_idname: str
    
    # The menu label
    bl_label: str
    
    bl_owner_id: str
    
    bl_translation_context: str
    
    # Defines the structure of the menu in the UI
    layout: UILayout

    @classmethod
    def poll(cls, *args, **kwargs) -> bool:
        ...

    def draw(self, *args, **kwargs) -> None:
        ...

    def draw_preset(self, *args, **kwargs) -> None:
        ...

    def path_menu(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...