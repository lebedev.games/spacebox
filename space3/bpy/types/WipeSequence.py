from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class WipeSequence(EffectSequence):
    
    # Edge angle
    angle: float
    
    # Width of the blur edge, in percentage relative to the image size
    blur_width: float
    
    # Wipe direction
    direction: str = 'OUT' # ['OUT', 'IN']
    
    # First input for the effect strip
    input_1: Sequence
    
    # Second input for the effect strip
    input_2: Sequence
    
    input_count: int
    
    transition_type: str = 'SINGLE' # ['SINGLE', 'DOUBLE', 'IRIS', 'CLOCK']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...