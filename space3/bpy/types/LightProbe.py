from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class LightProbe(ID):
    
    # Animation data for this data-block
    animation_data: AnimData
    
    # Probe clip end, beyond which objects will not appear in reflections
    clip_end: float
    
    # Probe clip start, below which objects will not appear in reflections
    clip_start: float
    
    # Control how fast the probe influence decreases
    falloff: float
    
    # Number of sample along the x axis of the volume
    grid_resolution_x: int
    
    # Number of sample along the y axis of the volume
    grid_resolution_y: int
    
    # Number of sample along the z axis of the volume
    grid_resolution_z: int
    
    # Influence distance of the probe
    influence_distance: float
    
    # Type of influence volume
    influence_type: str = 'ELIPSOID' # ['ELIPSOID', 'BOX']
    
    # Modify the intensity of the lighting captured by this probe
    intensity: float
    
    # Invert visibility collection
    invert_visibility_collection: bool
    
    # Lowest corner of the parallax bounding box
    parallax_distance: float
    
    # Type of parallax volume
    parallax_type: str = 'ELIPSOID' # ['ELIPSOID', 'BOX']
    
    # Show the clipping distances in the 3D view
    show_clip: bool
    
    # Show captured lighting data into the 3D view for debugging purpose
    show_data: bool
    
    # Show the influence volume in the 3D view
    show_influence: bool
    
    # Show the parallax correction volume in the 3D view
    show_parallax: bool
    
    # Type of light probe
    type: str = 'CUBEMAP' # ['CUBEMAP', 'PLANAR', 'GRID']
    
    # Enable custom settings for the parallax correction volume
    use_custom_parallax: bool
    
    # Bias for reducing light-bleed on variance shadow maps
    visibility_bleed_bias: float
    
    # Filter size of the visibility blur
    visibility_blur: float
    
    # Bias for reducing self shadowing
    visibility_buffer_bias: float
    
    # Restrict objects visible for this probe
    visibility_collection: Collection

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...