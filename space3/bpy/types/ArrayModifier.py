from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ArrayModifier(Modifier):
    
    # Value for the distance between arrayed items
    constant_offset_displace: Tuple[float, float, float]
    
    # Number of duplicates to make
    count: int
    
    # Curve object to fit array length to
    curve: Object
    
    # Mesh object to use as an end cap
    end_cap: Object
    
    # Length to fit array within
    fit_length: float
    
    # Array length calculation method
    fit_type: str = 'FIXED_COUNT' # ['FIXED_COUNT', 'FIT_LENGTH', 'FIT_CURVE']
    
    # Limit below which to merge vertices
    merge_threshold: float
    
    # Use the location and rotation of another object to determine the distance and rotational change between arrayed items
    offset_object: Object
    
    # Amount to offset array UVs on the U axis
    offset_u: float
    
    # Amount to offset array UVs on the V axis
    offset_v: float
    
    # The size of the geometry will determine the distance between arrayed items
    relative_offset_displace: Tuple[float, float, float]
    
    # Mesh object to use as a start cap
    start_cap: Object
    
    # Add a constant offset
    use_constant_offset: bool
    
    # Merge vertices in adjacent duplicates
    use_merge_vertices: bool
    
    # Merge vertices in first and last duplicates
    use_merge_vertices_cap: bool
    
    # Add another object’s transformation to the total offset
    use_object_offset: bool
    
    # Add an offset relative to the object’s bounding box
    use_relative_offset: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...