from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MovieReconstructedCamera(bpy_struct):
    
    # Average error of reconstruction
    average_error: float
    
    # Frame number marker is keyframed on
    frame: int
    
    # Worldspace transformation matrix
    matrix: List[float]

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...