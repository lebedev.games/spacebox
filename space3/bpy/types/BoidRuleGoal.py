from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class BoidRuleGoal(BoidRule):
    
    # Goal object
    object: Object
    
    # Predict target movement
    use_predict: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...