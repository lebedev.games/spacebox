from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ColorManagedSequencerColorspaceSettings(bpy_struct):
    
    # Color space that the sequencer operates in
    name: str = 'NONE' # ['Filmic Log', 'Linear', 'Linear ACES', 'Non-Color', 'Raw', 'sRGB', 'XYZ']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...