from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MeshSkinVertex(bpy_struct):
    
    # Radius of the skin
    radius: Tuple[float, float]
    
    # If vertex has multiple adjacent edges, it is hulled to them directly
    use_loose: bool
    
    # Vertex is a root for rotation calculations and armature generation, setting this flag does not clear other roots in the same mesh island
    use_root: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...