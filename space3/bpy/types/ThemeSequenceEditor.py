from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ThemeSequenceEditor(bpy_struct):
    
    active_strip: Tuple[float, float, float]
    
    audio_strip: Tuple[float, float, float]
    
    color_strip: Tuple[float, float, float]
    
    draw_action: Tuple[float, float, float]
    
    effect_strip: Tuple[float, float, float]
    
    frame_current: Tuple[float, float, float]
    
    grid: Tuple[float, float, float]
    
    image_strip: Tuple[float, float, float]
    
    keyframe: Tuple[float, float, float]
    
    mask_strip: Tuple[float, float, float]
    
    meta_strip: Tuple[float, float, float]
    
    metadatabg: Tuple[float, float, float]
    
    metadatatext: Tuple[float, float, float]
    
    movie_strip: Tuple[float, float, float]
    
    movieclip_strip: Tuple[float, float, float]
    
    preview_back: Tuple[float, float, float]
    
    # Color of preview range overlay
    preview_range: Tuple[float, float, float, float]
    
    # Overlay color on every other row
    row_alternate: Tuple[float, float, float, float]
    
    scene_strip: Tuple[float, float, float]
    
    selected_strip: Tuple[float, float, float]
    
    # Settings for space
    space: ThemeSpaceGeneric
    
    text_strip: Tuple[float, float, float]
    
    time_marker_line: Tuple[float, float, float, float]
    
    time_marker_line_selected: Tuple[float, float, float, float]
    
    time_scrub_background: Tuple[float, float, float, float]
    
    window_sliders: Tuple[float, float, float]

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...