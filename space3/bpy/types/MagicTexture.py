from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MagicTexture(Texture):
    
    # Depth of the noise
    noise_depth: int
    
    # Turbulence of the noise
    turbulence: float
    
    # Materials that use this texture
    users_material: Any
    
    # Object modifiers that use this texture
    users_object_modifier: Any

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...