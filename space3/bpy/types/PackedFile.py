from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class PackedFile(bpy_struct):
    
    # Raw data (bytes, exact content of the embedded file)
    data: str
    
    # Size of packed file in bytes
    size: int

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...