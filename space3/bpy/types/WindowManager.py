from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class WindowManager(ID):
    
    # Filter add-ons by category
    addon_filter: str
    
    # Search within the selected filter
    addon_search: str
    
    # Display support level
    addon_support: Set[str] # {'COMMUNITY', 'OFFICIAL', 'default {COMMUNITY', 'TESTING}'}
    
    # If true, the interface is currently locked by a running job and data shouldn’t be modified from application timers. Otherwise, the running job might conflict with the handler causing unexpected results or even crashes
    is_interface_locked: bool
    
    # Registered key configurations
    keyconfigs: KeyConfigurations
    
    # Operator registry
    operators: List[Operator]
    
    # Name for new preset
    preset_name: str
    
    # Open windows
    windows: List[Window]
    
    xr_session_settings: XrSessionSettings
    
    # Runtime state information about the VR session
    xr_session_state: XrSessionState

    @classmethod
    def fileselect_add(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def modal_handler_add(cls, *args, **kwargs) -> bool:
        ...

    def event_timer_add(self, *args, **kwargs) -> None:
        ...

    def event_timer_remove(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def gizmo_group_type_ensure(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def gizmo_group_type_unlink_delayed(cls, *args, **kwargs) -> None:
        ...

    def progress_begin(self, *args, **kwargs) -> None:
        ...

    def progress_update(self, *args, **kwargs) -> None:
        ...

    def progress_end(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def invoke_props_popup(cls, *args, **kwargs) -> Set[str]:
        ...

    @classmethod
    def invoke_props_dialog(cls, *args, **kwargs) -> Set[str]:
        ...

    @classmethod
    def invoke_search_popup(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def invoke_popup(cls, *args, **kwargs) -> Set[str]:
        ...

    @classmethod
    def invoke_confirm(cls, *args, **kwargs) -> Set[str]:
        ...

    @classmethod
    def popmenu_begin__internal(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def popmenu_end__internal(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def popover_begin__internal(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def popover_end__internal(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def piemenu_begin__internal(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def piemenu_end__internal(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def operator_properties_last(cls, *args, **kwargs) -> OperatorProperties:
        ...

    def print_undo_steps(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def tag_script_reload(cls, *args, **kwargs) -> None:
        ...

    def popover(self, *args, **kwargs) -> None:
        ...

    def popup_menu(self, *args, **kwargs) -> None:
        ...

    def popup_menu_pie(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...

    def draw_cursor_add(self, *args, **kwargs) -> None:
        ...

    def draw_cursor_remove(self, *args, **kwargs) -> None:
        ...