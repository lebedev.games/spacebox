from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class VolumeRender(bpy_struct):
    
    # Value under which voxels are considered empty space to optimize rendering
    clipping: float
    
    # Specify volume density and step size  in object or world space
    space: str = 'OBJECT' # ['OBJECT', 'WORLD']
    
    # Distance between volume samples. Lower values render more detail at the cost of performance. If set to zero, the step size is automatically determined based on voxel size
    step_size: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...