from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ID(bpy_struct):
    
    # Additional data for an asset data-block
    asset_data: AssetMetaData
    
    # This data-block is not an independent one, but is actually a sub-data of another ID (typical example: root node trees or master collections)
    is_embedded_data: bool
    
    # Whether this ID is runtime-only, evaluated data-block, or actual data from .blend file
    is_evaluated: bool
    
    # Is this ID block linked indirectly
    is_library_indirect: bool
    
    # Library file the data-block is linked from
    library: Library
    
    # Unique data-block ID name
    name: str
    
    # Unique data-block ID name, including library one is any
    name_full: str
    
    # Actual data-block from .blend file (Main database) that generated that evaluated one
    original: 'ID'
    
    # Library override data
    override_library: IDOverrideLibrary
    
    # Preview image and icon of this data-block (None if not supported for this type of data)
    preview: ImagePreview
    
    # Tools can use this to tag data for their own purposes (initial state is undefined)
    tag: bool
    
    # Save this data-block even if it has no users
    use_fake_user: bool
    
    # Number of times this data-block is referenced
    users: int

    def evaluated_get(self, *args, **kwargs) -> 'ID':
        ...

    def copy(self, *args, **kwargs) -> None:
        ...

    def override_create(self, *args, **kwargs) -> 'ID':
        ...

    def override_template_create(self, *args, **kwargs) -> None:
        ...

    def user_clear(self, *args, **kwargs) -> None:
        ...

    def user_remap(self, *args, **kwargs) -> None:
        ...

    def make_local(self, *args, **kwargs) -> 'ID':
        ...

    def user_of_id(self, *args, **kwargs) -> int:
        ...

    def animation_data_create(self, *args, **kwargs) -> None:
        ...

    def animation_data_clear(self, *args, **kwargs) -> None:
        ...

    def update_tag(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...