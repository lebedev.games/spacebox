from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class FluidDomainSettings(bpy_struct):
    
    # Margin added around fluid to minimize boundary interference
    adapt_margin: int
    
    # Minimum amount of fluid a cell can contain before it is considered empty
    adapt_threshold: float
    
    # Maximum number of additional cells
    additional_res: int
    
    # Buoyant force based on smoke density (higher value results in faster rising smoke)
    alpha: float
    
    # Buoyant force based on smoke heat (higher value results in faster rising smoke)
    beta: float
    
    # Speed of the burning reaction (higher value results in smaller flames)
    burning_rate: float
    
    # Select the file format to be used for caching volumetric data
    cache_data_format: str = 'NONE' # ['NONE']
    
    # Directory that contains fluid cache files
    cache_directory: str
    
    # Frame on which the simulation stops. This is the last frame that will be baked
    cache_frame_end: int
    
    # Frame offset that is used when loading the simulation from the cache. It is not considered when baking the simulation, only when loading it
    cache_frame_offset: int
    
    cache_frame_pause_data: int
    
    cache_frame_pause_guide: int
    
    cache_frame_pause_mesh: int
    
    cache_frame_pause_noise: int
    
    cache_frame_pause_particles: int
    
    # Frame on which the simulation starts. This is the first frame that will be baked
    cache_frame_start: int
    
    # Select the file format to be used for caching surface data
    cache_mesh_format: str = 'NONE' # ['NONE']
    
    # Select the file format to be used for caching noise data
    cache_noise_format: str = 'NONE' # ['NONE']
    
    # Select the file format to be used for caching particle data
    cache_particle_format: str = 'NONE' # ['NONE']
    
    # Additional data will be saved so that the bake jobs can be resumed after pausing. Because more data will be written to disk it is recommended to avoid enabling this option when baking at high resolutions
    cache_resumable: bool
    
    # Change the cache type of the simulation
    cache_type: str = 'REPLAY' # ['REPLAY', 'MODULAR', 'ALL']
    
    # Cell Size
    cell_size: Tuple[float, float, float]
    
    # Maximal velocity per cell (higher value results in greater timesteps)
    cfl_condition: float
    
    # Value under which voxels are considered empty space to optimize rendering
    clipping: float
    
    # Smoke color grid
    color_grid: Tuple[float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float]
    
    color_ramp: ColorRamp
    
    # Simulation field to color map
    color_ramp_field: str = 'NONE' # ['NONE']
    
    # Multiplier for scaling the selected field to color map
    color_ramp_field_scale: float
    
    # Delete fluid inside obstacles
    delete_in_obstacle: bool
    
    # Smoke density grid
    density_grid: Tuple[float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float]
    
    # Interpolation method to use for smoke/fire volumes in solid mode
    display_interpolation: str = 'LINEAR' # ['LINEAR', 'CUBIC', 'CLOSEST']
    
    # Thickness of smoke display in the viewport
    display_thickness: float
    
    # Determine how quickly the smoke dissolves (lower value makes smoke disappear faster)
    dissolve_speed: int
    
    # Smoke Grid Resolution
    domain_resolution: Tuple[int, int, int]
    
    # Change domain type of the simulation
    domain_type: str = 'GAS' # ['GAS', 'LIQUID']
    
    # Limit effectors to this collection
    effector_group: Collection
    
    effector_weights: EffectorWeights
    
    # Generate and export Mantaflow script from current domain settings during bake. This is only needed if you plan to analyze the cache (e.g. view grids, velocity vectors, particles) in Mantaflow directly (outside of Blender) after baking the simulation
    export_manta_script: bool
    
    # Smoke flame grid
    flame_grid: Tuple[float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float]
    
    # Minimum temperature of the flames (higher value results in faster rising flames)
    flame_ignition: float
    
    # Maximum temperature of the flames (higher value results in faster rising flames)
    flame_max_temp: float
    
    # Amount of smoke created by burning fuel
    flame_smoke: float
    
    # Color of smoke emitted from burning fuel
    flame_smoke_color: Tuple[float, float, float]
    
    # Additional vorticity for the flames
    flame_vorticity: float
    
    # PIC/FLIP Ratio. A value of 1.0 will result in a completely FLIP based simulation. Use a lower value for simulations which should produce smaller splashes
    flip_ratio: float
    
    # Limit fluid objects to this collection
    fluid_group: Collection
    
    # Limit forces to this collection
    force_collection: Collection
    
    # Determines how far apart fluid and obstacle are (higher values will result in fluid being further away from obstacles, smaller values will let fluid move towards the inside of obstacles)
    fractions_distance: float
    
    # Determines how much fluid is allowed in an obstacle cell (higher values will tag a boundary cell as an obstacle easier and reduce the boundary smoothening effect)
    fractions_threshold: float
    
    # Gravity in X, Y and Z direction
    gravity: Tuple[float, float, float]
    
    # Cell type to be highlighted
    gridlines_cell_filter: str = 'NONE' # ['NONE', 'FLUID', 'OBSTACLE', 'EMPTY', 'INFLOW', 'OUTFLOW']
    
    # Simulation field to color map onto gridlines
    gridlines_color_field: str = 'NONE' # ['NONE', 'FLAGS', 'RANGE']
    
    # Lower bound of the highlighting range
    gridlines_lower_bound: float
    
    # Color used to highlight the range
    gridlines_range_color: Tuple[float, float, float, float]
    
    # Upper bound of the highlighting range
    gridlines_upper_bound: float
    
    # Guiding weight (higher value results in greater lag)
    guide_alpha: float
    
    # Guiding size (higher value results in larger vortices)
    guide_beta: int
    
    # Use velocities from this object for the guiding effect (object needs to have fluid modifier and be of type domain))
    guide_parent: Object
    
    # Choose where to get guiding velocities from
    guide_source: str = 'DOMAIN' # ['DOMAIN', 'EFFECTOR']
    
    # Guiding velocity factor (higher value results in greater guiding velocities)
    guide_vel_factor: float
    
    has_cache_baked_any: bool
    
    has_cache_baked_data: bool
    
    has_cache_baked_guide: bool
    
    has_cache_baked_mesh: bool
    
    has_cache_baked_noise: bool
    
    has_cache_baked_particles: bool
    
    # Smoke heat grid
    heat_grid: Tuple[float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float]
    
    # Method for sampling the high resolution flow
    highres_sampling: str = 'FULLSAMPLE' # ['FULLSAMPLE', 'LINEAR', 'NEAREST']
    
    is_cache_baking_any: bool
    
    is_cache_baking_data: bool
    
    is_cache_baking_guide: bool
    
    is_cache_baking_mesh: bool
    
    is_cache_baking_noise: bool
    
    is_cache_baking_particles: bool
    
    # Lower mesh concavity bound (high values tend to smoothen and fill out concave regions)
    mesh_concave_lower: float
    
    # Upper mesh concavity bound (high values tend to smoothen and fill out concave regions)
    mesh_concave_upper: float
    
    # Which particle level set generator to use
    mesh_generator: str = 'IMPROVED' # ['IMPROVED', 'UNION']
    
    # Particle radius factor (higher value results in larger (meshed) particles). Needs to be adjusted after changing the mesh scale
    mesh_particle_radius: float
    
    # The mesh simulation is scaled up by this factor (compared to the base resolution of the domain). For best meshing, it is recommended to adjust the mesh particle radius alongside this value
    mesh_scale: int
    
    # Negative mesh smoothening
    mesh_smoothen_neg: int
    
    # Positive mesh smoothening
    mesh_smoothen_pos: int
    
    # Vertices of the fluid mesh generated by simulation
    mesh_vertices: List[FluidDomainVertexVelocity]
    
    # Scale of noise (higher value results in larger vortices)
    noise_pos_scale: float
    
    # The noise simulation is scaled up by this factor (compared to the base resolution of the domain)
    noise_scale: int
    
    # Strength of noise
    noise_strength: float
    
    # Animation time of noise
    noise_time_anim: float
    
    # Noise method which is used during the high-res simulation
    noise_type: str = 'NOISEWAVE' # ['NOISEWAVE']
    
    # Compression method to be used
    openvdb_cache_compress_type: str = 'BLOSC' # ['ZIP', 'BLOSC', 'NONE']
    
    # Bit depth for fluid particles and grids (lower bit values reduce file size)
    openvdb_data_depth: str = 'NONE' # ['NONE']
    
    # Particle (narrow) band width (higher value results in thicker band and more particles)
    particle_band_width: float
    
    # Maximum number of particles per cell (ensures that each cell has at most this amount of particles)
    particle_max: int
    
    # Minimum number of particles per cell (ensures that each cell has at least this amount of particles)
    particle_min: int
    
    # Particle number factor (higher value results in more particles)
    particle_number: int
    
    # Particle radius factor. Increase this value if the simulation appears to leak volume, decrease it if the simulation seems to gain volume
    particle_radius: float
    
    # Randomness factor for particle sampling
    particle_randomness: float
    
    # The particle simulation is scaled up by this factor (compared to the base resolution of the domain)
    particle_scale: int
    
    # Resolution used for the fluid domain. Value corresponds to the longest domain side (resolution for other domain sides is calculated automatically)
    resolution_max: int
    
    # Show gridlines
    show_gridlines: bool
    
    # Visualize vector fields
    show_velocity: bool
    
    # Change the underlying simulation method
    simulation_method: str = 'FLIP' # ['FLIP', 'APIC']
    
    slice_axis: str = 'AUTO' # ['AUTO', 'X', 'Y', 'Z']
    
    # Position of the slice
    slice_depth: float
    
    # How many slices per voxel should be generated
    slice_per_voxel: float
    
    # How particles that left the domain are treated
    sndparticle_boundary: str = 'DELETE' # ['DELETE', 'PUSHOUT']
    
    # Amount of buoyancy force that rises bubbles (high value results in bubble movement mainly upwards)
    sndparticle_bubble_buoyancy: float
    
    # Amount of drag force that moves bubbles along with the fluid (high value results in bubble movement mainly along with the fluid)
    sndparticle_bubble_drag: float
    
    # Determines which particle systems are created from secondary particles
    sndparticle_combined_export: str = 'OFF' # ['OFF', 'SPRAY_FOAM', 'SPRAY_BUBBLES', 'FOAM_BUBBLES', 'SPRAY_FOAM_BUBBLES']
    
    # Highest possible particle lifetime
    sndparticle_life_max: float
    
    # Lowest possible particle lifetime
    sndparticle_life_min: float
    
    # Upper clamping threshold that indicates the fluid speed where cells no longer emit more particles (higher value results in generally less particles)
    sndparticle_potential_max_energy: float
    
    # Upper clamping threshold for marking fluid cells where air is trapped (higher value results in less marked cells)
    sndparticle_potential_max_trappedair: float
    
    # Upper clamping threshold for marking fluid cells as wave crests (higher value results in less marked cells)
    sndparticle_potential_max_wavecrest: float
    
    # Lower clamping threshold that indicates the fluid speed where cells start to emit particles (lower values result in generally more particles)
    sndparticle_potential_min_energy: float
    
    # Lower clamping threshold for marking fluid cells where air is trapped (lower value results in more marked cells)
    sndparticle_potential_min_trappedair: float
    
    # Lower clamping threshold for marking fluid cells as wave crests (lower value results in more marked cells)
    sndparticle_potential_min_wavecrest: float
    
    # Radius to compute potential for each cell (higher values are slower but create smoother potential grids)
    sndparticle_potential_radius: int
    
    # Maximum number of particles generated per trapped air cell per frame
    sndparticle_sampling_trappedair: int
    
    # Maximum number of particles generated per wave crest cell per frame
    sndparticle_sampling_wavecrest: int
    
    # Radius to compute position update for each particle (higher values are slower but particles move less chaotic)
    sndparticle_update_radius: int
    
    # Start point
    start_point: Tuple[float, float, float]
    
    # Surface tension of liquid (higher value results in greater hydrophobic behavior)
    surface_tension: float
    
    # Maximum number of fluid particles that are allowed in this simulation
    sys_particle_maximum: int
    
    # Smoke temperature grid, range 0 to 1 represents 0 to 1000K
    temperature_grid: Tuple[float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float]
    
    # Adjust simulation speed
    time_scale: float
    
    # Maximum number of simulation steps to perform for one frame
    timesteps_max: int
    
    # Minimum number of simulation steps to perform for one frame
    timesteps_min: int
    
    # Adapt simulation resolution and size to fluid
    use_adaptive_domain: bool
    
    use_adaptive_timesteps: bool
    
    # Create bubble particle system
    use_bubble_particles: bool
    
    # Enable collisions with back domain border
    use_collision_border_back: bool
    
    # Enable collisions with bottom domain border
    use_collision_border_bottom: bool
    
    # Enable collisions with front domain border
    use_collision_border_front: bool
    
    # Enable collisions with left domain border
    use_collision_border_left: bool
    
    # Enable collisions with right domain border
    use_collision_border_right: bool
    
    # Enable collisions with top domain border
    use_collision_border_top: bool
    
    # Render a simulation field while mapping its voxels values to the colors of a ramp or using a predefined color code
    use_color_ramp: bool
    
    # Enable fluid diffusion settings (e.g. viscosity, surface tension)
    use_diffusion: bool
    
    # Let smoke disappear over time
    use_dissolve_smoke: bool
    
    # Dissolve smoke in a logarithmic fashion. Dissolves quickly at first, but lingers longer
    use_dissolve_smoke_log: bool
    
    # Create liquid particle system
    use_flip_particles: bool
    
    # Create foam particle system
    use_foam_particles: bool
    
    # Fractional obstacles improve and smoothen the fluid-obstacle boundary
    use_fractions: bool
    
    # Enable fluid guiding
    use_guide: bool
    
    # Enable fluid mesh (using amplification)
    use_mesh: bool
    
    # Enable fluid noise (using amplification)
    use_noise: bool
    
    # Perform a single slice of the domain object
    use_slice: bool
    
    # Caches velocities of mesh vertices. These will be used (automatically) when rendering with motion blur enabled
    use_speed_vectors: bool
    
    # Create spray particle system
    use_spray_particles: bool
    
    # Create tracer particle system
    use_tracer_particles: bool
    
    # Enable fluid viscosity settings
    use_viscosity: bool
    
    vector_display_type: str = 'NEEDLE' # ['NEEDLE', 'STREAMLINE', 'MAC']
    
    # Vector field to be represented by the display vectors
    vector_field: str = 'FLUID_VELOCITY' # ['FLUID_VELOCITY', 'GUIDE_VELOCITY', 'FORCE']
    
    # Multiplier for scaling the vectors
    vector_scale: float
    
    # Scale vectors with their magnitudes
    vector_scale_with_magnitude: bool
    
    # Show X-component of MAC Grid
    vector_show_mac_x: bool
    
    # Show Y-component of MAC Grid
    vector_show_mac_y: bool
    
    # Show Z-component of MAC Grid
    vector_show_mac_z: bool
    
    # Smoke velocity grid
    velocity_grid: Tuple[float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float]
    
    # Viscosity setting: value that is multiplied by 10 to the power of (exponent*-1)
    viscosity_base: float
    
    # Negative exponent for the viscosity value (to simplify entering small values e.g. 5*10^-6)
    viscosity_exponent: int
    
    # Viscosity of liquid (higher values result in more viscous fluids, a value of 0 will still apply some viscosity)
    viscosity_value: float
    
    # Amount of turbulence and rotation in smoke
    vorticity: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...