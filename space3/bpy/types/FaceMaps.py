from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class FaceMaps(bpy_struct):
    
    # Face maps of the object
    active: FaceMap
    
    # Active index in face map array
    active_index: int

    def new(self, *args, **kwargs) -> FaceMap:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    def clear(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...