from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class BoidRuleAvoidCollision(BoidRule):
    
    # Time to look ahead in seconds
    look_ahead: float
    
    # Avoid collision with other boids
    use_avoid: bool
    
    # Avoid collision with deflector objects
    use_avoid_collision: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...