from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MovieTrackingObjects(bpy_struct):
    
    # Active object in this tracking data object
    active: MovieTrackingObject

    def new(self, *args, **kwargs) -> MovieTrackingObject:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...