from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SequenceTransform(bpy_struct):
    
    # Move along X axis
    offset_x: int
    
    # Move along Y axis
    offset_y: int
    
    # Rotate around image center
    rotation: float
    
    # Scale along X axis
    scale_x: float
    
    # Scale along Y axis
    scale_y: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...