from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class NodeOutputFileSlotFile(bpy_struct):
    
    format: ImageFormatSettings
    
    # Subpath used for this slot
    path: str
    
    # Apply render part of display transform when saving byte image
    save_as_render: bool
    
    use_node_format: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...