from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CacheFile(ID):
    
    # Animation data for this data-block
    animation_data: AnimData
    
    # Path to external displacements file
    filepath: str
    
    forward_axis: str = 'POS_X' # ['POS_X', 'POS_Y', 'POS_Z', 'NEG_X', 'NEG_Y', 'NEG_Z']
    
    # The time to use for looking up the data in the cache file, or to determine which file to use in a file sequence
    frame: float
    
    # Subtracted from the current frame to use for looking up the data in the cache file, or to determine which file to use in a file sequence
    frame_offset: float
    
    # Whether the cache is separated in a series of files
    is_sequence: bool
    
    # Paths of the objects inside the Alembic archive
    object_paths: AlembicObjectPaths
    
    # Whether to use a custom frame for looking up data in the cache file, instead of using the current scene frame
    override_frame: bool
    
    # Value by which to enlarge or shrink the object with respect to the world’s origin (only applicable through a Transform Cache constraint)
    scale: float
    
    up_axis: str = 'POS_X' # ['POS_X', 'POS_Y', 'POS_Z', 'NEG_X', 'NEG_Y', 'NEG_Z']
    
    # Name of the Alembic attribute used for generating motion blur data
    velocity_name: str
    
    # Define how the velocity vectors are interpreted with regard to time, ‘frame’ means the delta time is 1 frame, ‘second’ means the delta time is 1 / FPS
    velocity_unit: str = 'FRAME' # ['SECOND', 'FRAME']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...