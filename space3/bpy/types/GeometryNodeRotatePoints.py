from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class GeometryNodeRotatePoints(GeometryNode):
    
    input_type_angle: str = 'ATTRIBUTE' # ['ATTRIBUTE', 'FLOAT']
    
    input_type_axis: str = 'ATTRIBUTE' # ['ATTRIBUTE', 'VECTOR']
    
    input_type_rotation: str = 'ATTRIBUTE' # ['ATTRIBUTE', 'VECTOR']
    
    # Base orientation of the points
    space: str = 'OBJECT' # ['OBJECT', 'POINT']
    
    # Method used to describe the rotation
    type: str = 'EULER' # ['AXIS_ANGLE', 'EULER']

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...