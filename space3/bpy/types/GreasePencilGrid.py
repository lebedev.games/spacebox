from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class GreasePencilGrid(bpy_struct):
    
    # Color for grid lines
    color: Tuple[float, float, float]
    
    # Number of subdivisions in each side of symmetry line
    lines: int
    
    # Offset of the canvas
    offset: Tuple[float, float]
    
    # Grid scale
    scale: Tuple[float, float]

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...