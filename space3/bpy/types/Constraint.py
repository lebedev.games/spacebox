from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Constraint(bpy_struct):
    
    # Constraint is the one being edited
    active: bool
    
    # Amount of residual error in Blender space unit for constraints that work on position
    error_location: float
    
    # Amount of residual error in radians for constraints that work on orientation
    error_rotation: float
    
    # Amount of influence constraint will have on the final solution
    influence: float
    
    # Constraint was added in this proxy instance (i.e. did not belong to source Armature)
    is_proxy_local: bool
    
    # Constraint has valid settings and can be evaluated
    is_valid: bool
    
    # Enable/Disable Constraint
    mute: bool
    
    # Constraint name
    name: str
    
    # Space that owner is evaluated in
    owner_space: str = 'WORLD' # ['WORLD', 'CUSTOM', 'POSE', 'LOCAL_WITH_PARENT', 'LOCAL']
    
    # Constraint’s panel is expanded in UI
    show_expanded: bool
    
    # Object for Custom Space
    space_object: Object
    
    # Armature bone, mesh or lattice vertex group, …
    space_subtarget: str
    
    # Space that target is evaluated in
    target_space: str = 'WORLD' # ['WORLD', 'CUSTOM', 'POSE', 'LOCAL_WITH_PARENT', 'LOCAL']
    
    type: str = 'CAMERA_SOLVER' # ['CAMERA_SOLVER', 'FOLLOW_TRACK', 'OBJECT_SOLVER', 'COPY_LOCATION', 'COPY_ROTATION', 'COPY_SCALE', 'COPY_TRANSFORMS', 'LIMIT_DISTANCE', 'LIMIT_LOCATION', 'LIMIT_ROTATION', 'LIMIT_SCALE', 'MAINTAIN_VOLUME', 'TRANSFORM', 'TRANSFORM_CACHE', 'CLAMP_TO', 'DAMPED_TRACK', 'IK', 'LOCKED_TRACK', 'SPLINE_IK', 'STRETCH_TO', 'TRACK_TO', 'ACTION', 'ARMATURE', 'CHILD_OF', 'FLOOR', 'FOLLOW_PATH', 'PIVOT', 'SHRINKWRAP']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...