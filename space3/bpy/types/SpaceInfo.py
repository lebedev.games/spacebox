from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SpaceInfo(Space):
    
    # Display debug reporting info
    show_report_debug: bool
    
    # Display error text
    show_report_error: bool
    
    # Display general information
    show_report_info: bool
    
    # Display the operator log
    show_report_operator: bool
    
    # Display warnings
    show_report_warning: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...

    def draw_handler_add(self, *args, **kwargs) -> Any:
        ...

    def draw_handler_remove(self, *args, **kwargs) -> None:
        ...