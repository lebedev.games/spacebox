from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MovieTrackingReconstruction(bpy_struct):
    
    # Average error of reconstruction
    average_error: float
    
    # Collection of solved cameras
    cameras: MovieTrackingReconstructedCameras
    
    # Is tracking data contains valid reconstruction information
    is_valid: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...