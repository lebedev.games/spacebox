from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class BoidRuleFollowLeader(BoidRule):
    
    # Distance behind leader to follow
    distance: float
    
    # Follow this object instead of a boid
    object: Object
    
    # How many boids in a line
    queue_count: int
    
    # Follow leader in a line
    use_line: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...