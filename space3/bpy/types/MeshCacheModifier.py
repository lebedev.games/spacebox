from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MeshCacheModifier(Modifier):
    
    cache_format: str = 'MDD' # ['MDD', 'PC2']
    
    deform_mode: str = 'OVERWRITE' # ['OVERWRITE', 'INTEGRATE']
    
    # Evaluation time in seconds
    eval_factor: float
    
    # The frame to evaluate (starting at 0)
    eval_frame: float
    
    # Evaluation time in seconds
    eval_time: float
    
    # Influence of the deformation
    factor: float
    
    # Path to external displacements file
    filepath: str
    
    flip_axis: Set[str] # {'Z}', 'X', 'default {X', 'Y'}
    
    forward_axis: str = 'POS_Y' # ['POS_X', 'POS_Y', 'POS_Z', 'NEG_X', 'NEG_Y', 'NEG_Z']
    
    # Evaluation time in seconds
    frame_scale: float
    
    # Add this to the start frame
    frame_start: float
    
    interpolation: str = 'LINEAR' # ['NONE', 'LINEAR']
    
    play_mode: str = 'SCENE' # ['SCENE', 'CUSTOM']
    
    # Method to control playback time
    time_mode: str = 'FRAME' # ['FRAME', 'TIME', 'FACTOR']
    
    up_axis: str = 'POS_Z' # ['POS_X', 'POS_Y', 'POS_Z', 'NEG_X', 'NEG_Y', 'NEG_Z']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...