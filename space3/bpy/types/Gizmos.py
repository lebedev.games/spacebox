from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Gizmos(bpy_struct):

    def new(self, *args, **kwargs) -> Gizmo:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    def clear(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...