from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CastModifier(Modifier):
    
    # Target object shape
    cast_type: str = 'SPHERE' # ['SPHERE', 'CYLINDER', 'CUBOID']
    
    factor: float
    
    # Invert vertex group influence
    invert_vertex_group: bool
    
    # Control object: if available, its location determines the center of the effect
    object: Object
    
    # Only deform vertices within this distance from the center of the effect (leave as 0 for infinite.)
    radius: float
    
    # Size of projection shape (leave as 0 for auto)
    size: float
    
    # Use radius as size of projection shape (0 = auto)
    use_radius_as_size: bool
    
    # Use object transform to control projection shape
    use_transform: bool
    
    use_x: bool
    
    use_y: bool
    
    use_z: bool
    
    # Vertex group name
    vertex_group: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...