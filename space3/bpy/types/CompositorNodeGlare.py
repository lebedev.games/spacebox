from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CompositorNodeGlare(CompositorNode):
    
    # Streak angle offset
    angle_offset: float
    
    # Amount of Color Modulation, modulates colors of streaks and ghosts for a spectral dispersion effect
    color_modulation: float
    
    # Streak fade-out factor
    fade: float
    
    glare_type: str = 'SIMPLE_STAR' # ['GHOSTS', 'STREAKS', 'FOG_GLOW', 'SIMPLE_STAR']
    
    iterations: int
    
    # -1 is original image only, 0 is exact 50/50 mix, 1 is processed image only
    mix: float
    
    # If not set to high quality, the effect will be applied to a low-res copy of the source image
    quality: str = 'HIGH' # ['HIGH', 'MEDIUM', 'LOW']
    
    # Glow/glare size (not actual size; relative to initial size of bright area of pixels)
    size: int
    
    # Total number of streaks
    streaks: int
    
    # The glare filter will only be applied to pixels brighter than this value
    threshold: float
    
    # Simple star filter: add 45 degree rotation offset
    use_rotate_45: bool

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    def update(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...