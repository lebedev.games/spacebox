from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ArmatureEditBones(bpy_struct):
    
    # Armatures active edit bone
    active: EditBone

    def new(self, *args, **kwargs) -> EditBone:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...