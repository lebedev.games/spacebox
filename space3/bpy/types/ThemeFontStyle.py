from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ThemeFontStyle(bpy_struct):
    
    # Which style to use for font kerning
    font_kerning_style: str = 'UNFITTED' # ['UNFITTED', 'FITTED']
    
    # Font size in points
    points: int
    
    # Shadow size (0, 3 and 5 supported)
    shadow: int
    
    shadow_alpha: float
    
    # Shadow offset in pixels
    shadow_offset_x: int
    
    # Shadow offset in pixels
    shadow_offset_y: int
    
    # Shadow color in gray value
    shadow_value: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...