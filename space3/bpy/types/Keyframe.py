from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Keyframe(bpy_struct):
    
    # Amount to boost elastic bounces for ‘elastic’ easing
    amplitude: float
    
    # Amount of overshoot for ‘back’ easing
    back: float
    
    # Coordinates of the control point
    co: Tuple[float, float]
    
    # Coordinates of the control point. Note: Changing this value also updates the handles similar to using the graph editor transform operator
    co_ui: Tuple[float, float]
    
    # Which ends of the segment between this and the next keyframe easing interpolation is applied to
    easing: str = 'AUTO' # ['AUTO', 'EASE_IN', 'EASE_OUT', 'EASE_IN_OUT']
    
    # Coordinates of the left handle (before the control point)
    handle_left: Tuple[float, float]
    
    # Handle types
    handle_left_type: str = 'FREE' # ['FREE', 'ALIGNED', 'VECTOR', 'AUTO', 'AUTO_CLAMPED']
    
    # Coordinates of the right handle (after the control point)
    handle_right: Tuple[float, float]
    
    # Handle types
    handle_right_type: str = 'FREE' # ['FREE', 'ALIGNED', 'VECTOR', 'AUTO', 'AUTO_CLAMPED']
    
    # Interpolation method to use for segment of the F-Curve from this Keyframe until the next Keyframe
    interpolation: str = 'CONSTANT' # ['CONSTANT', 'LINEAR', 'BEZIER', 'SINE', 'QUAD', 'CUBIC', 'QUART', 'QUINT', 'EXPO', 'CIRC', 'BACK', 'BOUNCE', 'ELASTIC']
    
    # Time between bounces for elastic easing
    period: float
    
    # Control point selection status
    select_control_point: bool
    
    # Left handle selection status
    select_left_handle: bool
    
    # Right handle selection status
    select_right_handle: bool
    
    # Type of keyframe (for visual purposes only)
    type: str = 'KEYFRAME' # ['KEYFRAME', 'BREAKDOWN', 'MOVING_HOLD', 'EXTREME', 'JITTER']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...