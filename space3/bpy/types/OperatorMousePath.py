from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class OperatorMousePath(PropertyGroup):
    
    # Mouse location
    loc: Tuple[float, float]
    
    # Time of mouse location
    time: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...