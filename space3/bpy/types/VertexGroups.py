from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class VertexGroups(bpy_struct, List[VertexGroup]):
    
    # Vertex groups of the object
    active: VertexGroup
    
    # Active index in vertex group array
    active_index: int

    def new(self, *args, **kwargs) -> VertexGroup:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    def clear(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...