from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class NodeTreeInputs(bpy_struct):

    def new(self, *args, **kwargs) -> NodeSocketInterface:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    def clear(self, *args, **kwargs) -> None:
        ...

    def move(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...