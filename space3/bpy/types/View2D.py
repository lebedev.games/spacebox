from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class View2D(bpy_struct):

    def region_to_view(self, *args, **kwargs) -> Tuple[float, float]:
        ...

    def view_to_region(self, *args, **kwargs) -> Tuple[int, int]:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...