from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class PreferencesView(bpy_struct):
    
    # Different styles of displaying the color picker widget
    color_picker_type: str = 'CIRCLE_HSV' # ['CIRCLE_HSV', 'CIRCLE_HSL', 'SQUARE_SV', 'SQUARE_HS', 'SQUARE_HV']
    
    # How factor values are displayed
    factor_display_type: str = 'FACTOR' # ['FACTOR', 'PERCENTAGE']
    
    # Default location where the File Editor will be displayed in
    filebrowser_display_type: str = 'WINDOW' # ['SCREEN', 'WINDOW']
    
    # Path to interface font
    font_path_ui: str
    
    # Path to interface monospaced Font
    font_path_ui_mono: str
    
    # Diameter of the gizmo
    gizmo_size: int
    
    # The Navigate Gizmo size
    gizmo_size_navigate_v3d: int
    
    # Default header position for new space-types
    header_align: str = 'NONE' # ['NONE', 'TOP', 'BOTTOM']
    
    # Language used for translation
    language: str = 'DEFAULT' # ['DEFAULT']
    
    # Diameter of the HDRI preview spheres
    lookdev_sphere_size: int
    
    # Brightness of the icon
    mini_axis_brightness: int
    
    # The axes icon’s size
    mini_axis_size: int
    
    # Show a small rotating 3D axes in the top right corner of the 3D View
    mini_axis_type: str = 'GIZMO' # ['NONE', 'MINIMAL', 'GIZMO']
    
    # Time delay in 1/10 seconds before automatically opening sub level menus
    open_sublevel_delay: int
    
    # Time delay in 1/10 seconds before automatically opening top level menus
    open_toplevel_delay: int
    
    # Time needed to fully animate the pie to unfolded state (in 1/100ths of sec)
    pie_animation_timeout: int
    
    # Pie menus will use the initial mouse position as center for this amount of time (in 1/100ths of sec)
    pie_initial_timeout: int
    
    # Distance threshold after which selection is made (zero to disable)
    pie_menu_confirm: int
    
    # Pie menu size in pixels
    pie_menu_radius: int
    
    # Distance from center needed before a selection can be made
    pie_menu_threshold: int
    
    # Pie menu button held longer than this will dismiss menu on release.(in 1/100ths of sec)
    pie_tap_timeout: int
    
    # Default location where rendered images will be displayed in
    render_display_type: str = 'WINDOW' # ['NONE', 'SCREEN', 'AREA', 'WINDOW']
    
    # Rotation step for numerical pad keys (2 4 6 8)
    rotation_angle: float
    
    # Only show enabled add-ons. Un-check to see all installed add-ons
    show_addons_enabled_only: bool
    
    # Use a column layout for toolbox
    show_column_layout: bool
    
    # Show options for developers (edit source in context menu, geometry indices)
    show_developer_ui: bool
    
    # Use transform gizmos by default
    show_gizmo: bool
    
    # Split and join editors by dragging from corners
    show_layout_ui: bool
    
    # Show navigation controls in 2D and 3D views which do not have scroll bars
    show_navigate_ui: bool
    
    # Display objects name and frame number in 3D view
    show_object_info: bool
    
    # Show the frames per second screen refresh rate, while animation is played back
    show_playback_fps: bool
    
    # Display splash screen on startup
    show_splash: bool
    
    # Show Blender memory usage
    show_statusbar_memory: bool
    
    # Show scene statistics
    show_statusbar_stats: bool
    
    # Show Blender version string
    show_statusbar_version: bool
    
    # Show GPU video memory usage
    show_statusbar_vram: bool
    
    # Display tooltips (when off hold Alt to force display)
    show_tooltips: bool
    
    # Show Python references in tooltips
    show_tooltips_python: bool
    
    # Show the name of the view’s direction in each 3D View
    show_view_name: bool
    
    # Time to animate the view in milliseconds, zero to disable
    smooth_view: int
    
    # Method for making user interface text render sharp
    text_hinting: str = 'AUTO' # ['AUTO', 'NONE', 'SLIGHT', 'FULL']
    
    # Format of Time Codes displayed when not displaying timing in terms of frames
    timecode_style: str = 'MINIMAL' # ['MINIMAL', 'SMPTE', 'SMPTE_COMPACT', 'MILLISECONDS', 'SECONDS_ONLY']
    
    # Changes the thickness of widget outlines, lines and dots in the interface
    ui_line_width: str = 'AUTO' # ['THIN', 'AUTO', 'THICK']
    
    # Changes the size of the fonts and widgets in the interface
    ui_scale: float
    
    # Otherwise menus, etc will always be top to bottom, left to right, no matter opening direction
    use_directional_menus: bool
    
    # Open menu buttons and pulldowns automatically when the mouse is hovering
    use_mouse_over_open: bool
    
    # Ask for confirmation when quitting with unsaved changes
    use_save_prompt: bool
    
    # Smooth jagged edges of user interface text
    use_text_antialiasing: bool
    
    # Translate all labels in menus, buttons and panels (note that this might make it hard to follow tutorials or the manual)
    use_translate_interface: bool
    
    # Translate the names of new data-blocks (objects, materials…)
    use_translate_new_dataname: bool
    
    # Translate the descriptions when hovering UI elements (recommended)
    use_translate_tooltips: bool
    
    # Enable color range used for weight visualization in weight painting mode
    use_weight_color_range: bool
    
    # Minimum number of pixels between each gridline in 2D Viewports
    view2d_grid_spacing_min: int
    
    # Keyframes around cursor that we zoom around
    view_frame_keyframes: int
    
    # Seconds around cursor that we zoom around
    view_frame_seconds: float
    
    # How zooming to frame focuses around current frame
    view_frame_type: str = 'KEEP_RANGE' # ['KEEP_RANGE', 'SECONDS', 'KEYFRAMES']
    
    # Color range used for weight visualization in weight painting mode
    weight_color_range: ColorRamp

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...