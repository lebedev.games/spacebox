from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Collection(ID):
    
    # Objects that are in this collection and its child collections
    all_objects: List[Object]
    
    # Collections that are immediate children of this collection
    children: CollectionChildren
    
    # Color tag for a collection
    color_tag: str = 'NONE' # ['NONE', 'COLOR_01', 'COLOR_02', 'COLOR_03', 'COLOR_04', 'COLOR_05', 'COLOR_06', 'COLOR_07', 'COLOR_08']
    
    # Globally disable in renders
    hide_render: bool
    
    # Disable selection in viewport
    hide_select: bool
    
    # Globally disable in viewports
    hide_viewport: bool
    
    # Offset from the origin to use when instancing
    instance_offset: Tuple[float, float, float]
    
    # How to use this collection in line art
    lineart_usage: str = 'INCLUDE' # ['INCLUDE', 'OCCLUSION_ONLY', 'EXCLUDE', 'INTERSECTION_ONLY', 'NO_INTERSECTION']
    
    # Objects that are directly in this collection
    objects: CollectionObjects
    
    # The collection instance objects this collection is used in
    users_dupli_group: Any

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...