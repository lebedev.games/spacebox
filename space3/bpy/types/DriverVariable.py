from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class DriverVariable(bpy_struct):
    
    # Is this a valid name for a driver variable
    is_name_valid: bool
    
    # Name to use in scripted expressions/functions (no spaces or dots are allowed, and must start with a letter)
    name: str
    
    # Sources of input data for evaluating this variable
    targets: List[DriverTarget]
    
    # Driver variable type
    type: str = 'SINGLE_PROP' # ['SINGLE_PROP', 'TRANSFORMS', 'ROTATION_DIFF', 'LOC_DIFF']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...