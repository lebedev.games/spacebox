from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ShapeKeyBezierPoint(bpy_struct):
    
    co: Tuple[float, float, float]
    
    handle_left: Tuple[float, float, float]
    
    handle_right: Tuple[float, float, float]
    
    # Radius for beveling
    radius: float
    
    # Tilt in 3D View
    tilt: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...