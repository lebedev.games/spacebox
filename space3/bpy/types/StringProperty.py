from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class StringProperty(Property):
    
    # String default value
    default: str
    
    # Maximum length of the string, 0 means unlimited
    length_max: int

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...