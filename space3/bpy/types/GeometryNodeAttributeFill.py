from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class GeometryNodeAttributeFill(GeometryNode):
    
    # Type of data stored in attribute
    data_type: str = 'FLOAT' # ['FLOAT', 'INT', 'FLOAT_VECTOR', 'FLOAT_COLOR', 'BYTE_COLOR', 'STRING', 'BOOLEAN', 'FLOAT2']
    
    domain: str = 'AUTO' # ['AUTO', 'POINT', 'EDGE', 'FACE', 'CORNER']

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...