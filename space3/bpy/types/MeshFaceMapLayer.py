from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MeshFaceMapLayer(bpy_struct):
    
    data: List[MeshFaceMap]
    
    # Name of face map layer
    name: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...