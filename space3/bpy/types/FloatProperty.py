from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class FloatProperty(Property):
    
    # Length of each dimension of the array
    array_dimensions: Tuple[int, int, int]
    
    # Maximum length of the array, 0 means unlimited
    array_length: int
    
    # Default value for this number
    default: float
    
    # Default value for this array
    default_array: Tuple[float, float, float]
    
    # Maximum value used by buttons
    hard_max: float
    
    # Minimum value used by buttons
    hard_min: float
    
    is_array: bool
    
    # Number of digits after the dot used by buttons
    precision: int
    
    # Maximum value used by buttons
    soft_max: float
    
    # Minimum value used by buttons
    soft_min: float
    
    # Step size used by number buttons, for floats 1/100th of the step size
    step: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...