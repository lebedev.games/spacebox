from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Gizmo(bpy_struct):
    
    alpha: float
    
    alpha_highlight: float
    
    bl_idname: str
    
    color: Tuple[float, float, float]
    
    color_highlight: Tuple[float, float, float]
    
    # Gizmo group this gizmo is a member of
    group: GizmoGroup
    
    hide: bool
    
    # Ignore the key-map for this gizmo
    hide_keymap: bool
    
    hide_select: bool
    
    is_highlight: bool
    
    is_modal: bool
    
    line_width: float
    
    matrix_basis: List[float]
    
    matrix_offset: List[float]
    
    matrix_space: List[float]
    
    matrix_world: List[float]
    
    properties: GizmoProperties
    
    scale_basis: float
    
    select: bool
    
    # Depth bias used for selection
    select_bias: float
    
    use_draw_hover: bool
    
    # Show while dragging
    use_draw_modal: bool
    
    # Scale the offset matrix (use to apply screen-space offset)
    use_draw_offset_scale: bool
    
    # Use scale when calculating the matrix
    use_draw_scale: bool
    
    # Show an indicator for the current value while dragging
    use_draw_value: bool
    
    # When highlighted, do not pass events through to be handled by other keymaps
    use_event_handle_all: bool
    
    use_grab_cursor: bool
    
    # Merge active tool properties on activation (does not overwrite existing)
    use_operator_tool_properties: bool
    
    # Don’t write into the depth buffer
    use_select_background: bool
    
    # Use tooltips when hovering over this gizmo
    use_tooltip: bool

    def draw(self, *args, **kwargs) -> None:
        ...

    def draw_select(self, *args, **kwargs) -> None:
        ...

    def test_select(self, *args, **kwargs) -> int:
        ...

    def modal(self, *args, **kwargs) -> Set[str]:
        ...

    def setup(self, *args, **kwargs) -> None:
        ...

    def invoke(self, *args, **kwargs) -> None:
        ...

    def exit(self, *args, **kwargs) -> None:
        ...

    def select_refresh(self, *args, **kwargs) -> None:
        ...

    def draw_preset_box(self, *args, **kwargs) -> None:
        ...

    def draw_preset_arrow(self, *args, **kwargs) -> None:
        ...

    def draw_preset_circle(self, *args, **kwargs) -> None:
        ...

    def draw_preset_facemap(self, *args, **kwargs) -> None:
        ...

    def target_set_prop(self, *args, **kwargs) -> None:
        ...

    def target_set_operator(self, *args, **kwargs) -> OperatorProperties:
        ...

    def target_is_valid(self, *args, **kwargs) -> None:
        ...

    def draw_custom_shape(self, *args, **kwargs) -> None:
        ...

    def new_custom_shape(self, *args, **kwargs) -> Any:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...