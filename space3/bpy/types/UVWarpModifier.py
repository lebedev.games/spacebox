from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class UVWarpModifier(Modifier):
    
    # Pole axis for rotation
    axis_u: str = 'X' # ['X', 'Y', 'Z']
    
    # Pole axis for rotation
    axis_v: str = 'Y' # ['X', 'Y', 'Z']
    
    # Bone defining offset
    bone_from: str
    
    # Bone defining offset
    bone_to: str
    
    # Center point for rotate/scale
    center: Tuple[float, float]
    
    # Invert vertex group influence
    invert_vertex_group: bool
    
    # Object defining offset
    object_from: Object
    
    # Object defining offset
    object_to: Object
    
    # 2D Offset for the warp
    offset: Tuple[float, float]
    
    # 2D Rotation for the warp
    rotation: float
    
    # 2D Scale for the warp
    scale: Tuple[float, float]
    
    # UV Layer name
    uv_layer: str
    
    # Vertex group name
    vertex_group: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...