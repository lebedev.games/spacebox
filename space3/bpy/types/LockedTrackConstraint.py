from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class LockedTrackConstraint(Constraint):
    
    # Target along length of bone: Head is 0, Tail is 1
    head_tail: float
    
    # Axis that points upward
    lock_axis: str = 'LOCK_X' # ['LOCK_X', 'LOCK_Y', 'LOCK_Z']
    
    # Armature bone, mesh or lattice vertex group, …
    subtarget: str
    
    # Target object
    target: Object
    
    # Axis that points to the target object
    track_axis: str = 'TRACK_X' # ['TRACK_X', 'TRACK_Y', 'TRACK_Z', 'TRACK_NEGATIVE_X', 'TRACK_NEGATIVE_Y', 'TRACK_NEGATIVE_Z']
    
    # Follow shape of B-Bone segments when calculating Head/Tail position
    use_bbone_shape: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...