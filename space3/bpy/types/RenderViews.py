from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class RenderViews(bpy_struct):
    
    # Active Render View
    active: SceneRenderView
    
    # Active index in render view array
    active_index: int

    def new(self, *args, **kwargs) -> SceneRenderView:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...