from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class RenderResult(bpy_struct):
    
    layers: List[RenderLayer]
    
    resolution_x: int
    
    resolution_y: int
    
    views: List[RenderView]

    def load_from_file(self, *args, **kwargs) -> None:
        ...

    def stamp_data_add_field(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...