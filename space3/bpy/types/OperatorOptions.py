from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class OperatorOptions(bpy_struct):
    
    # True when the cursor is grabbed
    is_grab_cursor: bool
    
    # True when invoked (even if only the execute callbacks available)
    is_invoke: bool
    
    # True when run from the ‘Adjust Last Operation’ panel
    is_repeat: bool
    
    # True when run from the operator ‘Repeat Last’
    is_repeat_last: bool
    
    # Enable to use the region under the cursor for modal execution
    use_cursor_region: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...