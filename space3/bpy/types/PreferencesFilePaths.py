from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class PreferencesFilePaths(bpy_struct):
    
    # Path to a custom animation/frame sequence player
    animation_player: str
    
    # Preset configs for external animation players
    animation_player_preset: str = 'INTERNAL' # ['INTERNAL', 'DJV', 'FRAMECYCLER', 'RV', 'MPLAYER', 'CUSTOM']
    
    asset_libraries: List[UserAssetLibrary]
    
    # The time (in minutes) to wait between automatic temporary saves
    auto_save_time: int
    
    # The default directory to search for loading fonts
    font_directory: str
    
    # Hide recent locations in the file selector
    hide_recent_locations: bool
    
    # Hide system bookmarks in the file selector
    hide_system_bookmarks: bool
    
    # The path to the ‘/branches’ directory of your local svn-translation copy, to allow translating from the UI
    i18n_branches_directory: str
    
    # Path to an image editor
    image_editor: str
    
    # Maximum number of recently opened files to remember
    recent_files: int
    
    # Where to cache raw render results
    render_cache_directory: str
    
    # The default directory for rendering output, for new scenes
    render_output_directory: str
    
    # The number of old versions to maintain in the current directory, when manually saving
    save_version: int
    
    # Alternate script path, matching the default layout with subdirectories: startup, add-ons and modules (requires restart)
    script_directory: str
    
    # Hide files and data-blocks if their name start with a dot (.*)
    show_hidden_files_datablocks: bool
    
    # The default directory to search for sounds
    sound_directory: str
    
    # The directory for storing temporary save files
    temporary_directory: str
    
    # The default directory to search for textures
    texture_directory: str
    
    use_auto_save_temporary_files: bool
    
    # Enable file compression when saving .blend files
    use_file_compression: bool
    
    # Display only files with extensions in the image select window
    use_filter_files: bool
    
    # Load user interface setup when loading .blend files
    use_load_ui: bool
    
    # Default relative path option for the file selector, when no path is defined yet
    use_relative_paths: bool
    
    # Enables automatic saving of preview images in the .blend file as well as a thumbnail of the .blend
    use_save_preview_images: bool
    
    # Allow any .blend file to run scripts automatically (unsafe with blend files from an untrusted source)
    use_scripts_auto_execute: bool
    
    # Automatically convert all new tabs into spaces for new and loaded text files
    use_tabs_as_spaces: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...