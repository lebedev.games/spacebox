from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SequencesMeta(bpy_struct):

    def new_clip(self, *args, **kwargs) -> Sequence:
        ...

    def new_mask(self, *args, **kwargs) -> Sequence:
        ...

    def new_scene(self, *args, **kwargs) -> Sequence:
        ...

    def new_image(self, *args, **kwargs) -> Sequence:
        ...

    def new_movie(self, *args, **kwargs) -> Sequence:
        ...

    def new_sound(self, *args, **kwargs) -> Sequence:
        ...

    def new_meta(self, *args, **kwargs) -> Sequence:
        ...

    def new_effect(self, *args, **kwargs) -> Sequence:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...