from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CompositorNodeColorBalance(CompositorNode):
    
    correction_method: str = 'LIFT_GAMMA_GAIN' # ['LIFT_GAMMA_GAIN', 'OFFSET_POWER_SLOPE']
    
    # Correction for highlights
    gain: Tuple[float, float, float]
    
    # Correction for midtones
    gamma: Tuple[float, float, float]
    
    # Correction for shadows
    lift: Tuple[float, float, float]
    
    # Correction for entire tonal range
    offset: Tuple[float, float, float]
    
    # Support negative color by using this as the RGB basis
    offset_basis: float
    
    # Correction for midtones
    power: Tuple[float, float, float]
    
    # Correction for highlights
    slope: Tuple[float, float, float]

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    def update(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...