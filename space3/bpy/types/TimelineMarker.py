from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class TimelineMarker(bpy_struct):
    
    # Camera that becomes active on this frame
    camera: Object
    
    # The frame on which the timeline marker appears
    frame: int
    
    name: str
    
    # Marker selection state
    select: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...