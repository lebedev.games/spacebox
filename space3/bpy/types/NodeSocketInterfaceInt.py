from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class NodeSocketInterfaceInt(NodeSocketInterfaceStandard):
    
    # Input value used for unconnected socket
    default_value: int
    
    # Maximum value
    max_value: int
    
    # Minimum value
    min_value: int

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...