from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ParticleHairKey(bpy_struct):
    
    # Location of the hair key in object space
    co: Tuple[float, float, float]
    
    # Location of the hair key in its local coordinate system, relative to the emitting face
    co_local: Tuple[float, float, float]
    
    # Relative time of key over hair length
    time: float
    
    # Weight for cloth simulation
    weight: float

    def co_object(self, *args, **kwargs) -> Tuple[float, float, float]:
        ...

    def co_object_set(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...