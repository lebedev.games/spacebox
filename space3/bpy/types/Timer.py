from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Timer(bpy_struct):
    
    # Time since last step in seconds
    time_delta: float
    
    # Time since last step in seconds
    time_duration: float
    
    time_step: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...