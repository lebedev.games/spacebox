from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CompositorNodeStabilize(CompositorNode):
    
    clip: MovieClip
    
    # Method to use to filter stabilization
    filter_type: str = 'NEAREST' # ['NEAREST', 'BILINEAR', 'BICUBIC']
    
    # Invert stabilization to re-introduce motion to the frame
    invert: bool

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    def update(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...