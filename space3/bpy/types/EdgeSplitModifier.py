from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class EdgeSplitModifier(Modifier):
    
    # Angle above which to split edges
    split_angle: float
    
    # Split edges with high angle between faces
    use_edge_angle: bool
    
    # Split edges that are marked as sharp
    use_edge_sharp: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...