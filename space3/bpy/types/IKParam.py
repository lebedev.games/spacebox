from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class IKParam(bpy_struct):
    
    # IK solver for which these parameters are defined
    ik_solver: str = 'LEGACY' # ['LEGACY', 'ITASC']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...