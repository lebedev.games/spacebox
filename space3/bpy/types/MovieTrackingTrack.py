from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MovieTrackingTrack(bpy_struct):
    
    # Average error of re-projection
    average_error: float
    
    # Position of bundle reconstructed from this track
    bundle: Tuple[float, float, float]
    
    # Color of the track in the Movie Clip Editor and the 3D viewport after a solve
    color: Tuple[float, float, float]
    
    # Minimal value of correlation between matched pattern and reference that is still treated as successful tracking
    correlation_min: float
    
    # Every tracking cycle, this number of frames are tracked
    frames_limit: int
    
    # Grease pencil data for this track
    grease_pencil: GreasePencil
    
    # True if track has a valid bundle
    has_bundle: bool
    
    # Track is hidden
    hide: bool
    
    # Track is locked and all changes to it are disabled
    lock: bool
    
    # Distance from image boundary at which marker stops tracking
    margin: int
    
    # Collection of markers in track
    markers: MovieTrackingMarkers
    
    # Default motion model to use for tracking
    motion_model: str = 'Loc' # ['Perspective', 'Affine', 'LocRotScale', 'LocScale', 'LocRot', 'Loc']
    
    # Unique name of track
    name: str
    
    # Offset of track from the parenting point
    offset: Tuple[float, float]
    
    # Track pattern from given frame when tracking marker to next frame
    pattern_match: str = 'KEYFRAME' # ['KEYFRAME', 'PREV_FRAME']
    
    # Track is selected
    select: bool
    
    # Track’s anchor point is selected
    select_anchor: bool
    
    # Track’s pattern area is selected
    select_pattern: bool
    
    # Track’s search area is selected
    select_search: bool
    
    # Apply track’s mask on displaying preview
    use_alpha_preview: bool
    
    # Use blue channel from footage for tracking
    use_blue_channel: bool
    
    # Use a brute-force translation only pre-track before refinement
    use_brute: bool
    
    # Use custom color instead of theme-defined
    use_custom_color: bool
    
    # Display what the tracking algorithm sees in the preview
    use_grayscale_preview: bool
    
    # Use green channel from footage for tracking
    use_green_channel: bool
    
    # Use a grease pencil data-block as a mask to use only specified areas of pattern when tracking
    use_mask: bool
    
    # Normalize light intensities while tracking. Slower
    use_normalization: bool
    
    # Use red channel from footage for tracking
    use_red_channel: bool
    
    # Influence of this track on a final solution
    weight: float
    
    # Influence of this track on 2D stabilization
    weight_stab: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...