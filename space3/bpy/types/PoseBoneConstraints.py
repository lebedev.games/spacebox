from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class PoseBoneConstraints(bpy_struct):
    
    # Active PoseChannel constraint
    active: Constraint

    def new(self, *args, **kwargs) -> Constraint:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    def move(self, *args, **kwargs) -> None:
        ...

    def copy(self, *args, **kwargs) -> Constraint:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...