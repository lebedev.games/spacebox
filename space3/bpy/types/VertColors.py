from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class VertColors(bpy_struct):
    
    # Active sculpt vertex color layer
    active: MeshVertColorLayer
    
    # Active sculpt vertex color index
    active_index: int

    def new(self, *args, **kwargs) -> MeshVertColorLayer:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...