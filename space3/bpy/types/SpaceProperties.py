from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SpaceProperties(Space):
    
    context: str = 'RENDER' # ['TOOL', 'SCENE', 'RENDER', 'OUTPUT', 'VIEW_LAYER', 'WORLD', 'COLLECTION', 'OBJECT', 'CONSTRAINT', 'MODIFIER', 'DATA', 'BONE', 'BONE_CONSTRAINT', 'MATERIAL', 'TEXTURE', 'PARTICLES', 'PHYSICS', 'SHADERFX']
    
    # Change to the corresponding tab when outliner data icons are clicked
    outliner_sync: str = 'AUTO' # ['ALWAYS', 'NEVER', 'AUTO']
    
    pin_id: ID
    
    # Live search filtering string
    search_filter: str
    
    # Whether or not each visible tab has a search result
    tab_search_results: bool
    
    # Use the pinned context
    use_pin_id: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...

    def draw_handler_add(self, *args, **kwargs) -> Any:
        ...

    def draw_handler_remove(self, *args, **kwargs) -> None:
        ...