from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class GPencilEditCurvePoint(bpy_struct):
    
    # Coordinates of the control point
    co: Tuple[float, float, float]
    
    # Coordinates of the first handle
    handle_left: Tuple[float, float, float]
    
    # Coordinates of the second handle
    handle_right: Tuple[float, float, float]
    
    # Visibility status
    hide: bool
    
    # Index of the corresponding grease pencil stroke point
    point_index: int
    
    # Pressure of the grease pencil stroke point
    pressure: float
    
    # Control point selection status
    select_control_point: bool
    
    # Handle 1 selection status
    select_left_handle: bool
    
    # Handle 2 selection status
    select_right_handle: bool
    
    # Color intensity (alpha factor) of the grease pencil stroke point
    strength: float
    
    # Internal UV factor
    uv_factor: float
    
    # Internal UV factor for dot mode
    uv_rotation: float
    
    # Vertex color of the grease pencil stroke point
    vertex_color: Tuple[float, float, float, float]

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...