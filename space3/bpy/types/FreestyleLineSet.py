from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class FreestyleLineSet(bpy_struct):
    
    # A collection of objects based on which feature edges are selected
    collection: Collection
    
    # Specify either inclusion or exclusion of feature edges belonging to a collection of objects
    collection_negation: str = 'INCLUSIVE' # ['INCLUSIVE', 'EXCLUSIVE']
    
    # Specify a logical combination of selection conditions on feature edge types
    edge_type_combination: str = 'OR' # ['OR', 'AND']
    
    # Specify either inclusion or exclusion of feature edges selected by edge types
    edge_type_negation: str = 'INCLUSIVE' # ['INCLUSIVE', 'EXCLUSIVE']
    
    # Exclude border edges
    exclude_border: bool
    
    # Exclude contours
    exclude_contour: bool
    
    # Exclude crease edges
    exclude_crease: bool
    
    # Exclude edge marks
    exclude_edge_mark: bool
    
    # Exclude external contours
    exclude_external_contour: bool
    
    # Exclude edges at material boundaries
    exclude_material_boundary: bool
    
    # Exclude ridges and valleys
    exclude_ridge_valley: bool
    
    # Exclude silhouette edges
    exclude_silhouette: bool
    
    # Exclude suggestive contours
    exclude_suggestive_contour: bool
    
    # Specify a feature edge selection condition based on face marks
    face_mark_condition: str = 'ONE' # ['ONE', 'BOTH']
    
    # Specify either inclusion or exclusion of feature edges selected by face marks
    face_mark_negation: str = 'INCLUSIVE' # ['INCLUSIVE', 'EXCLUSIVE']
    
    # Line style settings
    linestyle: FreestyleLineStyle
    
    # Line set name
    name: str
    
    # Last QI value of the QI range
    qi_end: int
    
    # First QI value of the QI range
    qi_start: int
    
    # Select border edges (open mesh edges)
    select_border: bool
    
    # Select feature edges based on a collection of objects
    select_by_collection: bool
    
    # Select feature edges based on edge types
    select_by_edge_types: bool
    
    # Select feature edges by face marks
    select_by_face_marks: bool
    
    # Select feature edges by image border (less memory consumption)
    select_by_image_border: bool
    
    # Select feature edges based on visibility
    select_by_visibility: bool
    
    # Select contours (outer silhouettes of each object)
    select_contour: bool
    
    # Select crease edges (those between two faces making an angle smaller than the Crease Angle)
    select_crease: bool
    
    # Select edge marks (edges annotated by Freestyle edge marks)
    select_edge_mark: bool
    
    # Select external contours (outer silhouettes of occluding and occluded objects)
    select_external_contour: bool
    
    # Select edges at material boundaries
    select_material_boundary: bool
    
    # Select ridges and valleys (boundary lines between convex and concave areas of surface)
    select_ridge_valley: bool
    
    # Select silhouettes (edges at the boundary of visible and hidden faces)
    select_silhouette: bool
    
    # Select suggestive contours (almost silhouette/contour edges)
    select_suggestive_contour: bool
    
    # Enable or disable this line set during stroke rendering
    show_render: bool
    
    # Determine how to use visibility for feature edge selection
    visibility: str = 'VISIBLE' # ['VISIBLE', 'HIDDEN', 'RANGE']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...