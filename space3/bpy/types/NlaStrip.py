from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class NlaStrip(bpy_struct):
    
    # Action referenced by this strip
    action: Action
    
    # Last frame from action to use
    action_frame_end: float
    
    # First frame from action to use
    action_frame_start: float
    
    # NLA Strip is active
    active: bool
    
    # Number of frames at start of strip to fade in influence
    blend_in: float
    
    blend_out: float
    
    # Method used for combining strip’s result with accumulated result
    blend_type: str = 'REPLACE' # ['REPLACE', 'COMBINE', 'ADD', 'SUBTRACT', 'MULTIPLY']
    
    # Action to take for gaps past the strip extents
    extrapolation: str = 'HOLD' # ['NOTHING', 'HOLD', 'HOLD_FORWARD']
    
    # F-Curves for controlling the strip’s influence and timing
    fcurves: NlaStripFCurves
    
    frame_end: float
    
    frame_start: float
    
    # Amount the strip contributes to the current result
    influence: float
    
    # Modifiers affecting all the F-Curves in the referenced Action
    modifiers: List[FModifier]
    
    # Disable NLA Strip evaluation
    mute: bool
    
    name: str
    
    # Number of times to repeat the action range
    repeat: float
    
    # Scaling factor for action
    scale: float
    
    # NLA Strip is selected
    select: bool
    
    # Frame of referenced Action to evaluate
    strip_time: float
    
    # NLA Strips that this strip acts as a container for (if it is of type Meta)
    strips: List[NlaStrip]
    
    # Type of NLA Strip
    type: str = 'CLIP' # ['CLIP', 'TRANSITION', 'META', 'SOUND']
    
    # Influence setting is controlled by an F-Curve rather than automatically determined
    use_animated_influence: bool
    
    # Strip time is controlled by an F-Curve rather than automatically determined
    use_animated_time: bool
    
    # Cycle the animated time within the action start and end
    use_animated_time_cyclic: bool
    
    # Number of frames for Blending In/Out is automatically determined from overlapping strips
    use_auto_blend: bool
    
    # NLA Strip is played back in reverse order (only when timing is automatically determined)
    use_reverse: bool
    
    # Update range of frames referenced from action after tweaking strip and its keyframes
    use_sync_length: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...