from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ObjectBase(bpy_struct):
    
    # Temporarily hide in viewport
    hide_viewport: bool
    
    # Object this base links to
    object: Object
    
    # Object base selection state
    select: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...