from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class PathCompareCollection(bpy_struct):

    @classmethod
    def new(cls, *args, **kwargs) -> PathCompare:
        ...

    @classmethod
    def remove(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...