from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SequenceProxy(bpy_struct):
    
    # Build 100% proxy resolution
    build_100: bool
    
    # Build 25% proxy resolution
    build_25: bool
    
    # Build 50% proxy resolution
    build_50: bool
    
    # Build 75% proxy resolution
    build_75: bool
    
    # Build free run time code index
    build_free_run: bool
    
    # Build free run time code index using Record Date/Time
    build_free_run_rec_date: bool
    
    # Build record run time code index
    build_record_run: bool
    
    # Location to store the proxy files
    directory: str
    
    # Location of custom proxy file
    filepath: str
    
    # JPEG Quality of proxies to build
    quality: int
    
    # Method for reading the inputs timecode
    timecode: str = 'NONE' # ['NONE', 'RECORD_RUN', 'FREE_RUN', 'FREE_RUN_REC_DATE', 'RECORD_RUN_NO_GAPS']
    
    # Overwrite existing proxy files when building
    use_overwrite: bool
    
    # Use a custom directory to store data
    use_proxy_custom_directory: bool
    
    # Use a custom file to read proxy data from
    use_proxy_custom_file: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...