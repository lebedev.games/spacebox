from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CompositorNodeColorCorrection(CompositorNode):
    
    # Blue channel active
    blue: bool
    
    # Green channel active
    green: bool
    
    # Highlights contrast
    highlights_contrast: float
    
    # Highlights gain
    highlights_gain: float
    
    # Highlights gamma
    highlights_gamma: float
    
    # Highlights lift
    highlights_lift: float
    
    # Highlights saturation
    highlights_saturation: float
    
    # Master contrast
    master_contrast: float
    
    # Master gain
    master_gain: float
    
    # Master gamma
    master_gamma: float
    
    # Master lift
    master_lift: float
    
    # Master saturation
    master_saturation: float
    
    # Midtones contrast
    midtones_contrast: float
    
    # End of midtones
    midtones_end: float
    
    # Midtones gain
    midtones_gain: float
    
    # Midtones gamma
    midtones_gamma: float
    
    # Midtones lift
    midtones_lift: float
    
    # Midtones saturation
    midtones_saturation: float
    
    # Start of midtones
    midtones_start: float
    
    # Red channel active
    red: bool
    
    # Shadows contrast
    shadows_contrast: float
    
    # Shadows gain
    shadows_gain: float
    
    # Shadows gamma
    shadows_gamma: float
    
    # Shadows lift
    shadows_lift: float
    
    # Shadows saturation
    shadows_saturation: float

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    def update(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...