from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Mask(ID):
    
    # Index of active layer in list of all mask’s layers
    active_layer_index: int
    
    # Animation data for this data-block
    animation_data: AnimData
    
    # Final frame of the mask (used for sequencer)
    frame_end: int
    
    # First frame of the mask (used for sequencer)
    frame_start: int
    
    # Collection of layers which defines this mask
    layers: MaskLayers

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...