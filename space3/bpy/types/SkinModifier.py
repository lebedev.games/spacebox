from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SkinModifier(Modifier):
    
    # Smooth complex geometry around branches
    branch_smoothing: float
    
    # Output faces with smooth shading rather than flat shaded
    use_smooth_shade: bool
    
    # Avoid making unsymmetrical quads across the X axis
    use_x_symmetry: bool
    
    # Avoid making unsymmetrical quads across the Y axis
    use_y_symmetry: bool
    
    # Avoid making unsymmetrical quads across the Z axis
    use_z_symmetry: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...