from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class VertexPaint(Paint):
    
    # Number of times to copy strokes across the surface
    radial_symmetry: Tuple[int, int, int]
    
    # Restrict painting to vertices in the group
    use_group_restrict: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...