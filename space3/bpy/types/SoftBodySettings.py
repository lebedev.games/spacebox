from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SoftBodySettings(bpy_struct):
    
    # Make edges ‘sail’
    aero: int
    
    # Method of calculating aerodynamic interaction
    aerodynamics_type: str = 'SIMPLE' # ['SIMPLE', 'LIFT_FORCE']
    
    # Blending to inelastic collision
    ball_damp: float
    
    # Absolute ball size or factor if not manually adjusted
    ball_size: float
    
    # Ball inflating pressure
    ball_stiff: float
    
    # Bending Stiffness
    bend: float
    
    # ‘Viscosity’ inside collision target
    choke: int
    
    # Limit colliders to this collection
    collision_collection: Collection
    
    # Choose Collision Type
    collision_type: str = 'MANUAL' # ['MANUAL', 'AVERAGE', 'MINIMAL', 'MAXIMAL', 'MINMAX']
    
    # Edge spring friction
    damping: float
    
    effector_weights: EffectorWeights
    
    # The Runge-Kutta ODE solver error limit, low value gives more precision, high values speed
    error_threshold: float
    
    # General media friction for point movements
    friction: float
    
    # Fuzziness while on collision, high values make collision handling faster but less stable
    fuzzy: int
    
    # Default Goal (vertex target position) value
    goal_default: float
    
    # Goal (vertex target position) friction
    goal_friction: float
    
    # Goal maximum, vertex weights are scaled to match this range
    goal_max: float
    
    # Goal minimum, vertex weights are scaled to match this range
    goal_min: float
    
    # Goal (vertex target position) spring stiffness
    goal_spring: float
    
    # Apply gravitation to point movement
    gravity: float
    
    # Location of center of mass
    location_mass_center: Tuple[float, float, float]
    
    # General Mass value
    mass: float
    
    # Permanent deform
    plastic: int
    
    # Edge spring stiffness when longer than rest length
    pull: float
    
    # Edge spring stiffness when shorter than rest length
    push: float
    
    # Estimated rotation matrix
    rotation_estimate: List[float]
    
    # Estimated scale matrix
    scale_estimate: List[float]
    
    # Shear Stiffness
    shear: float
    
    # Tweak timing for physics to control frequency and speed
    speed: float
    
    # Alter spring length to shrink/blow up (unit %) 0 to disable
    spring_length: int
    
    # Maximal # solver steps/frame
    step_max: int
    
    # Minimal # solver steps/frame
    step_min: int
    
    # Use velocities for automagic step sizes
    use_auto_step: bool
    
    # Turn on SB diagnose console prints
    use_diagnose: bool
    
    # Edges collide too
    use_edge_collision: bool
    
    # Use Edges as springs
    use_edges: bool
    
    # Store the estimated transforms in the soft body settings
    use_estimate_matrix: bool
    
    # Faces collide too, can be very slow
    use_face_collision: bool
    
    # Define forces for vertices to stick to animated position
    use_goal: bool
    
    # Enable naive vertex ball self collision
    use_self_collision: bool
    
    # Add diagonal springs on 4-gons
    use_stiff_quads: bool
    
    # Control point weight values
    vertex_group_goal: str
    
    # Control point mass values
    vertex_group_mass: str
    
    # Control point spring strength values
    vertex_group_spring: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...