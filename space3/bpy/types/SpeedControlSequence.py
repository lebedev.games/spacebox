from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SpeedControlSequence(EffectSequence):
    
    # First input for the effect strip
    input_1: Sequence
    
    input_count: int
    
    # Multiply the resulting speed after the speed factor
    multiply_speed: float
    
    # Interpret the value as speed instead of a frame number
    use_as_speed: bool
    
    # Do crossfade blending between current and next frame
    use_frame_interpolate: bool
    
    # Scale values from 0.0 to 1.0 to target sequence length
    use_scale_to_length: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...