from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class EffectorWeights(bpy_struct):
    
    # All effector’s weight
    all: float
    
    # Use force fields when growing hair
    apply_to_hair_growing: bool
    
    # Boid effector weight
    boid: float
    
    # Charge effector weight
    charge: float
    
    # Limit effectors to this collection
    collection: Collection
    
    # Curve guide effector weight
    curve_guide: float
    
    # Drag effector weight
    drag: float
    
    # Force effector weight
    force: float
    
    # Global gravity weight
    gravity: float
    
    # Harmonic effector weight
    harmonic: float
    
    # Lennard-Jones effector weight
    lennardjones: float
    
    # Magnetic effector weight
    magnetic: float
    
    # Fluid Flow effector weight
    smokeflow: float
    
    # Texture effector weight
    texture: float
    
    # Turbulence effector weight
    turbulence: float
    
    # Vortex effector weight
    vortex: float
    
    # Wind effector weight
    wind: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...