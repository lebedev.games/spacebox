from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class GPencilTriangle(bpy_struct):
    
    # First triangle vertex index
    v1: int
    
    # Second triangle vertex index
    v2: int
    
    # Third triangle vertex index
    v3: int

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...