from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class TransformSequence(EffectSequence):
    
    # First input for the effect strip
    input_1: Sequence
    
    input_count: int
    
    # Method to determine how missing pixels are created
    interpolation: str = 'NONE' # ['NONE', 'BILINEAR', 'BICUBIC']
    
    # Degrees to rotate the input
    rotation_start: float
    
    # Amount to scale the input in the X axis
    scale_start_x: float
    
    # Amount to scale the input in the Y axis
    scale_start_y: float
    
    # Amount to move the input on the X axis
    translate_start_x: float
    
    # Amount to move the input on the Y axis
    translate_start_y: float
    
    # Unit of measure to translate the input
    translation_unit: str = 'PIXELS' # ['PIXELS', 'PERCENT']
    
    # Scale uniformly, preserving aspect ratio
    use_uniform_scale: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...