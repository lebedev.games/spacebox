from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class BlendDataMaterials(bpy_struct):

    def new(self, *args, **kwargs) -> Material:
        ...

    def create_gpencil_data(self, *args, **kwargs) -> None:
        ...

    def remove_gpencil_data(self, *args, **kwargs) -> None:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    def tag(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...