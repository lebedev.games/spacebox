from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ThemeDopeSheet(bpy_struct):
    
    active_channels_group: Tuple[float, float, float, float]
    
    channel_group: Tuple[float, float, float, float]
    
    channels: Tuple[float, float, float, float]
    
    channels_selected: Tuple[float, float, float, float]
    
    dopesheet_channel: Tuple[float, float, float, float]
    
    dopesheet_subchannel: Tuple[float, float, float, float]
    
    frame_current: Tuple[float, float, float]
    
    grid: Tuple[float, float, float]
    
    # Color of lines showing non-bezier interpolation modes
    interpolation_line: Tuple[float, float, float, float]
    
    # Color of Keyframe
    keyframe: Tuple[float, float, float]
    
    # Color of keyframe border
    keyframe_border: Tuple[float, float, float, float]
    
    # Color of selected keyframe border
    keyframe_border_selected: Tuple[float, float, float, float]
    
    # Color of breakdown keyframe
    keyframe_breakdown: Tuple[float, float, float]
    
    # Color of selected breakdown keyframe
    keyframe_breakdown_selected: Tuple[float, float, float]
    
    # Color of extreme keyframe
    keyframe_extreme: Tuple[float, float, float]
    
    # Color of selected extreme keyframe
    keyframe_extreme_selected: Tuple[float, float, float]
    
    # Color of jitter keyframe
    keyframe_jitter: Tuple[float, float, float]
    
    # Color of selected jitter keyframe
    keyframe_jitter_selected: Tuple[float, float, float]
    
    # Color of moving hold keyframe
    keyframe_movehold: Tuple[float, float, float]
    
    # Color of selected moving hold keyframe
    keyframe_movehold_selected: Tuple[float, float, float]
    
    # Scale factor for adjusting the height of keyframes
    keyframe_scale_factor: float
    
    # Color of selected keyframe
    keyframe_selected: Tuple[float, float, float]
    
    long_key: Tuple[float, float, float, float]
    
    long_key_selected: Tuple[float, float, float, float]
    
    # Color of preview range overlay
    preview_range: Tuple[float, float, float, float]
    
    # Settings for space
    space: ThemeSpaceGeneric
    
    # Settings for space list
    space_list: ThemeSpaceListGeneric
    
    # Color of summary channel
    summary: Tuple[float, float, float, float]
    
    time_marker_line: Tuple[float, float, float, float]
    
    time_marker_line_selected: Tuple[float, float, float, float]
    
    time_scrub_background: Tuple[float, float, float, float]
    
    value_sliders: Tuple[float, float, float]
    
    view_sliders: Tuple[float, float, float]

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...