from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ThemeGraphEditor(bpy_struct):
    
    active_channels_group: Tuple[float, float, float]
    
    channel_group: Tuple[float, float, float]
    
    channels_region: Tuple[float, float, float]
    
    dopesheet_channel: Tuple[float, float, float]
    
    dopesheet_subchannel: Tuple[float, float, float]
    
    frame_current: Tuple[float, float, float]
    
    grid: Tuple[float, float, float]
    
    handle_align: Tuple[float, float, float]
    
    handle_auto: Tuple[float, float, float]
    
    handle_auto_clamped: Tuple[float, float, float]
    
    handle_free: Tuple[float, float, float]
    
    handle_sel_align: Tuple[float, float, float]
    
    handle_sel_auto: Tuple[float, float, float]
    
    handle_sel_auto_clamped: Tuple[float, float, float]
    
    handle_sel_free: Tuple[float, float, float]
    
    handle_sel_vect: Tuple[float, float, float]
    
    handle_vect: Tuple[float, float, float]
    
    handle_vertex: Tuple[float, float, float]
    
    handle_vertex_select: Tuple[float, float, float]
    
    handle_vertex_size: int
    
    lastsel_point: Tuple[float, float, float]
    
    # Color of preview range overlay
    preview_range: Tuple[float, float, float, float]
    
    # Settings for space
    space: ThemeSpaceGeneric
    
    # Settings for space list
    space_list: ThemeSpaceListGeneric
    
    time_marker_line: Tuple[float, float, float, float]
    
    time_marker_line_selected: Tuple[float, float, float, float]
    
    time_scrub_background: Tuple[float, float, float, float]
    
    vertex: Tuple[float, float, float]
    
    vertex_active: Tuple[float, float, float]
    
    vertex_bevel: Tuple[float, float, float]
    
    vertex_select: Tuple[float, float, float]
    
    vertex_size: int
    
    vertex_unreferenced: Tuple[float, float, float]
    
    window_sliders: Tuple[float, float, float]

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...