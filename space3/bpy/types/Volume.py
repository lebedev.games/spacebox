from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Volume(ID):
    
    # Animation data for this data-block
    animation_data: AnimData
    
    # Volume display settings for 3D viewport
    display: VolumeDisplay
    
    # Volume file used by this Volume data-block
    filepath: str
    
    # Number of frames of the sequence to use
    frame_duration: int
    
    # Offset the number of the frame to use in the animation
    frame_offset: int
    
    # Global starting frame of the sequence, assuming first has a #1
    frame_start: int
    
    # 3D volume grids
    grids: VolumeGrids
    
    # Whether the cache is separated in a series of files
    is_sequence: bool
    
    materials: IDMaterials
    
    packed_file: PackedFile
    
    # Volume render settings for 3D viewport
    render: VolumeRender
    
    # Sequence playback mode
    sequence_mode: str = 'CLIP' # ['CLIP', 'EXTEND', 'REPEAT', 'PING_PONG']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...