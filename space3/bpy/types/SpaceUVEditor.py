from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SpaceUVEditor(bpy_struct):
    
    # Type of stretch to display
    display_stretch_type: str = 'ANGLE' # ['ANGLE', 'AREA']
    
    # Display style for UV edges
    edge_display_type: str = 'OUTLINE' # ['OUTLINE', 'DASH', 'BLACK', 'WHITE']
    
    # Constraint to stay within the image bounds while editing
    lock_bounds: bool
    
    # Snap UVs to pixels while editing
    pixel_snap_mode: str = 'DISABLED' # ['DISABLED', 'CORNER', 'CENTER']
    
    # Display faces over the image
    show_faces: bool
    
    # Display metadata properties of the image
    show_metadata: bool
    
    # Display edges after modifiers are applied
    show_modified_edges: bool
    
    # Display UV coordinates in pixels rather than from 0.0 to 1.0
    show_pixel_coords: bool
    
    # Display faces colored according to the difference in shape between UVs and their 3D coordinates (blue for low distortion, red for high distortion)
    show_stretch: bool
    
    # Display overlay of texture paint uv layer
    show_texpaint: bool
    
    # Method for extending UV vertex selection
    sticky_select_mode: str = 'SHARED_LOCATION' # ['DISABLED', 'SHARED_LOCATION', 'SHARED_VERTEX']
    
    # How many tiles will be shown in the background
    tile_grid_shape: Tuple[int, int]
    
    # Continuously unwrap the selected UV island while transforming pinned vertices
    use_live_unwrap: bool
    
    # Opacity of UV overlays
    uv_opacity: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...