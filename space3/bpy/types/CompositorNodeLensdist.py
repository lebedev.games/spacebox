from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CompositorNodeLensdist(CompositorNode):
    
    # For positive distortion factor only: scale image such that black areas are not visible
    use_fit: bool
    
    # Enable/disable jittering (faster, but also noisier)
    use_jitter: bool
    
    # Enable/disable projector mode (the effect is applied in horizontal direction only)
    use_projector: bool

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    def update(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...