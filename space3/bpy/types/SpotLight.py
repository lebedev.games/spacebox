from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SpotLight(Light):
    
    # Constant distance attenuation coefficient
    constant_coefficient: float
    
    # Bias to avoid self shadowing
    contact_shadow_bias: float
    
    # World space distance in which to search for screen space occluder
    contact_shadow_distance: float
    
    # Pixel thickness used to detect occlusion
    contact_shadow_thickness: float
    
    # The energy this light would emit over its entire area if it wasn’t limited by the spot angle
    energy: float
    
    # Custom light falloff curve
    falloff_curve: CurveMapping
    
    # Intensity Decay with distance
    falloff_type: str = 'INVERSE_SQUARE' # ['CONSTANT', 'INVERSE_LINEAR', 'INVERSE_SQUARE', 'INVERSE_COEFFICIENTS', 'CUSTOM_CURVE', 'LINEAR_QUADRATIC_WEIGHTED']
    
    # Linear distance attenuation
    linear_attenuation: float
    
    # Linear distance attenuation coefficient
    linear_coefficient: float
    
    # Quadratic distance attenuation
    quadratic_attenuation: float
    
    # Quadratic distance attenuation coefficient
    quadratic_coefficient: float
    
    # Bias for reducing self shadowing
    shadow_buffer_bias: float
    
    # Shadow map clip start, below which objects will not generate shadows
    shadow_buffer_clip_start: float
    
    # Number of shadow buffer samples
    shadow_buffer_samples: int
    
    # Resolution of the shadow buffer, higher values give crisper shadows but use more memory
    shadow_buffer_size: int
    
    # Color of shadows cast by the light
    shadow_color: Tuple[float, float, float]
    
    # Light size for ray shadow sampling (Raytraced shadows)
    shadow_soft_size: float
    
    # Display transparent cone in 3D view to visualize which objects are contained in it
    show_cone: bool
    
    # The softness of the spotlight edge
    spot_blend: float
    
    # Angle of the spotlight beam
    spot_size: float
    
    # Use screen space raytracing to have correct shadowing near occluder, or for small features that does not appear in shadow maps
    use_contact_shadow: bool
    
    use_shadow: bool
    
    # Cast a square spot light shape
    use_square: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...