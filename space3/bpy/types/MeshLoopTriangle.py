from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MeshLoopTriangle(bpy_struct):
    
    # Area of this triangle
    area: float
    
    # Index of this loop triangle
    index: int
    
    # Indices of mesh loops that make up the triangle
    loops: Tuple[int, int, int]
    
    material_index: int
    
    # Local space unit length normal vector for this triangle
    normal: Tuple[float, float, float]
    
    # Index of mesh polygon that the triangle is a part of
    polygon_index: int
    
    # Local space unit length split normals vectors of the vertices of this triangle (must be computed beforehand using calc_normals_split or calc_tangents)
    split_normals: List[float]
    
    use_smooth: bool
    
    # Indices of triangle vertices
    vertices: Tuple[int, int, int]
    
    # The midpoint of the face.
    center: Any
    
    # (readonly)
    edge_keys: Any

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...