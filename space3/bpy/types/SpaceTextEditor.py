from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SpaceTextEditor(Space):
    
    # Text to search for with the find tool
    find_text: str
    
    # Font size to use for displaying the text
    font_size: int
    
    # Column number to show right margin at
    margin_column: int
    
    # Text to replace selected text with using the replace tool
    replace_text: str
    
    # Highlight the current line
    show_line_highlight: bool
    
    # Show line numbers next to the text
    show_line_numbers: bool
    
    # Show right margin
    show_margin: bool
    
    show_region_footer: bool
    
    show_region_ui: bool
    
    # Syntax highlight for scripting
    show_syntax_highlight: bool
    
    # Wrap words if there is not enough horizontal space
    show_word_wrap: bool
    
    # Number of spaces to display tabs with
    tab_width: int
    
    # Text displayed and edited in this space
    text: Text
    
    # Top line visible
    top: int
    
    # Search in all text data-blocks, instead of only the active one
    use_find_all: bool
    
    # Search again from the start of the file when reaching the end
    use_find_wrap: bool
    
    # Run python while editing
    use_live_edit: bool
    
    # Search string is sensitive to uppercase and lowercase letters
    use_match_case: bool
    
    # Overwrite characters when typing rather than inserting them
    use_overwrite: bool
    
    # Amount of lines that can be visible in current editor
    visible_lines: int

    def is_syntax_highlight_supported(self, *args, **kwargs) -> bool:
        ...

    def region_location_from_cursor(self, *args, **kwargs) -> Tuple[int, int]:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...

    def draw_handler_add(self, *args, **kwargs) -> Any:
        ...

    def draw_handler_remove(self, *args, **kwargs) -> None:
        ...