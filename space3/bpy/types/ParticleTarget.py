from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ParticleTarget(bpy_struct):
    
    alliance: str = 'NEUTRAL' # ['FRIEND', 'NEUTRAL', 'ENEMY']
    
    duration: float
    
    # Keyed particles target is valid
    is_valid: bool
    
    # Particle target name
    name: str
    
    # The object that has the target particle system (empty if same object)
    object: Object
    
    # The index of particle system on the target object
    system: int
    
    time: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...