from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SplineIKConstraint(Constraint):
    
    # Factor between volume variation and stretching
    bulge: float
    
    # Maximum volume stretching factor
    bulge_max: float
    
    # Minimum volume stretching factor
    bulge_min: float
    
    # Strength of volume stretching clamping
    bulge_smooth: float
    
    # How many bones are included in the chain
    chain_count: int
    
    # (EXPERIENCED USERS ONLY) The relative positions of the joints along the chain, as percentages
    joint_bindings: Tuple[float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float]
    
    # Curve that controls this relationship
    target: Object
    
    # Use upper limit for volume variation
    use_bulge_max: bool
    
    # Use lower limit for volume variation
    use_bulge_min: bool
    
    # Offset the entire chain relative to the root joint
    use_chain_offset: bool
    
    # Average radius of the endpoints is used to tweak the X and Z Scaling of the bones, on top of XZ Scale mode
    use_curve_radius: bool
    
    # Ignore the relative lengths of the bones when fitting to the curve
    use_even_divisions: bool
    
    # Apply volume preservation over the original scaling
    use_original_scale: bool
    
    # Method used for determining the scaling of the X and Z axes of the bones
    xz_scale_mode: str = 'NONE' # ['NONE', 'BONE_ORIGINAL', 'INVERSE_PRESERVE', 'VOLUME_PRESERVE']
    
    # Method used for determining the scaling of the Y axis of the bones, on top of the shape and scaling of the curve itself
    y_scale_mode: str = 'NONE' # ['NONE', 'FIT_CURVE', 'BONE_ORIGINAL']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...