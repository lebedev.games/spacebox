from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CompositorNodeSunBeams(CompositorNode):
    
    # Length of rays as a factor of the image size
    ray_length: float
    
    # Source point of rays as a factor of the image width and height
    source: Tuple[float, float]

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    def update(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...