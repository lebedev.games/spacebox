from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SubdivGpencilModifier(GpencilModifier):
    
    # Inverse filter
    invert_layer_pass: bool
    
    # Inverse filter
    invert_layers: bool
    
    # Inverse filter
    invert_material_pass: bool
    
    # Inverse filter
    invert_materials: bool
    
    # Layer name
    layer: str
    
    # Layer pass index
    layer_pass: int
    
    # Number of subdivisions
    level: int
    
    # Material used for filtering effect
    material: Material
    
    # Pass index
    pass_index: int
    
    # Select type of subdivision algorithm
    subdivision_type: str = 'CATMULL_CLARK' # ['CATMULL_CLARK', 'SIMPLE']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...