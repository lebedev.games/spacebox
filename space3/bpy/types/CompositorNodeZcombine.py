from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CompositorNodeZcombine(CompositorNode):
    
    # Take alpha channel into account when doing the Z operation
    use_alpha: bool
    
    # Anti-alias the z-buffer to try to avoid artifacts, mostly useful for Blender renders
    use_antialias_z: bool

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    def update(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...