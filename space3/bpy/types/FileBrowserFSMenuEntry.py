from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class FileBrowserFSMenuEntry(bpy_struct):
    
    icon: int
    
    # Whether this path is currently reachable
    is_valid: bool
    
    name: str
    
    path: str
    
    # Whether this path is saved in bookmarks, or generated from OS
    use_save: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...