from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CorrectiveSmoothModifier(Modifier):
    
    # Smooth factor effect
    factor: float
    
    # Invert vertex group influence
    invert_vertex_group: bool
    
    is_bind: bool
    
    iterations: int
    
    # Select the source of rest positions
    rest_source: str = 'ORCO' # ['ORCO', 'BIND']
    
    # Compensate for scale applied by other modifiers
    scale: float
    
    # Method used for smoothing
    smooth_type: str = 'SIMPLE' # ['SIMPLE', 'LENGTH_WEIGHTED']
    
    # Apply smoothing without reconstructing the surface
    use_only_smooth: bool
    
    # Excludes boundary vertices from being smoothed
    use_pin_boundary: bool
    
    # Name of Vertex Group which determines influence of modifier per point
    vertex_group: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...