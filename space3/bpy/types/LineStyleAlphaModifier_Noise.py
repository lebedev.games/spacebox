from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class LineStyleAlphaModifier_Noise(LineStyleAlphaModifier):
    
    # Amplitude of the noise
    amplitude: float
    
    # Specify how the modifier value is blended into the base value
    blend: str = 'MIX' # ['MIX', 'ADD', 'SUBTRACT', 'MULTIPLY', 'DIVIDE', 'DIFFERENCE', 'MINIMUM', 'MAXIMUM']
    
    # Curve used for the curve mapping
    curve: CurveMapping
    
    # True if the modifier tab is expanded
    expanded: bool
    
    # Influence factor by which the modifier changes the property
    influence: float
    
    # Invert the fade-out direction of the linear mapping
    invert: bool
    
    # Select the mapping type
    mapping: str = 'LINEAR' # ['LINEAR', 'CURVE']
    
    # Name of the modifier
    name: str
    
    # Period of the noise
    period: float
    
    # Seed for the noise generation
    seed: int
    
    # Type of the modifier
    type: str = 'ALONG_STROKE' # ['ALONG_STROKE', 'CREASE_ANGLE', 'CURVATURE_3D', 'DISTANCE_FROM_CAMERA', 'DISTANCE_FROM_OBJECT', 'MATERIAL', 'NOISE', 'TANGENT']
    
    # Enable or disable this modifier during stroke rendering
    use: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...