from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class SubsurfModifier(Modifier):
    
    # Controls how open boundaries are smoothed
    boundary_smooth: str = 'ALL' # ['PRESERVE_CORNERS', 'ALL']
    
    # Number of subdivisions to perform
    levels: int
    
    # Accuracy of vertex positions, lower value is faster but less precise
    quality: int
    
    # Number of subdivisions to perform when rendering
    render_levels: int
    
    # Skip displaying interior subdivided edges
    show_only_control_edges: bool
    
    # Select type of subdivision algorithm
    subdivision_type: str = 'CATMULL_CLARK' # ['CATMULL_CLARK', 'SIMPLE']
    
    # Use mesh edge crease information to sharpen edges
    use_creases: bool
    
    # Interpolates existing custom normals to resulting mesh
    use_custom_normals: bool
    
    # Place vertices at the surface that would be produced with infinite levels of subdivision (smoothest possible shape)
    use_limit_surface: bool
    
    # Controls how smoothing is applied to UVs
    uv_smooth: str = 'PRESERVE_BOUNDARIES' # ['NONE', 'PRESERVE_CORNERS', 'PRESERVE_CORNERS_AND_JUNCTIONS', 'PRESERVE_CORNERS_JUNCTIONS_AND_CONCAVE', 'PRESERVE_BOUNDARIES', 'SMOOTH_ALL']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...