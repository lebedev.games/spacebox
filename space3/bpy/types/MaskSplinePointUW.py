from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MaskSplinePointUW(bpy_struct):
    
    # Selection status
    select: bool
    
    # U coordinate of point along spline segment
    u: float
    
    # Weight of feather point
    weight: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...