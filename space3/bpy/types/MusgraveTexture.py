from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MusgraveTexture(Texture):
    
    # Highest fractal dimension
    dimension_max: float
    
    # The gain multiplier
    gain: float
    
    # Gap between successive frequencies
    lacunarity: float
    
    # Fractal noise algorithm
    musgrave_type: str = 'MULTIFRACTAL' # ['MULTIFRACTAL', 'RIDGED_MULTIFRACTAL', 'HYBRID_MULTIFRACTAL', 'FBM', 'HETERO_TERRAIN']
    
    # Size of derivative offset used for calculating normal
    nabla: float
    
    # Noise basis used for turbulence
    noise_basis: str = 'BLENDER_ORIGINAL' # ['BLENDER_ORIGINAL', 'ORIGINAL_PERLIN', 'IMPROVED_PERLIN', 'VORONOI_F1', 'VORONOI_F2', 'VORONOI_F3', 'VORONOI_F4', 'VORONOI_F2_F1', 'VORONOI_CRACKLE', 'CELL_NOISE']
    
    # Intensity of the noise
    noise_intensity: float
    
    # Scaling for noise input
    noise_scale: float
    
    # Number of frequencies used
    octaves: float
    
    # The fractal offset
    offset: float
    
    # Materials that use this texture
    users_material: Any
    
    # Object modifiers that use this texture
    users_object_modifier: Any

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...