from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class GeometryNodeTriangulate(GeometryNode):
    
    # Method for splitting the n-gons into triangles
    ngon_method: str = 'BEAUTY' # ['BEAUTY', 'CLIP']
    
    # Method for splitting the quads into triangles
    quad_method: str = 'SHORTEST_DIAGONAL' # ['BEAUTY', 'FIXED', 'FIXED_ALTERNATE', 'SHORTEST_DIAGONAL']

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...