from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CopyTransformsConstraint(Constraint):
    
    # Target along length of bone: Head is 0, Tail is 1
    head_tail: float
    
    # Specify how the copied and existing transformations are combined
    mix_mode: str = 'REPLACE' # ['REPLACE', 'BEFORE', 'AFTER']
    
    # Armature bone, mesh or lattice vertex group, …
    subtarget: str
    
    # Target object
    target: Object
    
    # Follow shape of B-Bone segments when calculating Head/Tail position
    use_bbone_shape: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...