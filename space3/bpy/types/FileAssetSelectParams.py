from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class FileAssetSelectParams(FileSelectParams):
    
    # Determine which kind of assets to display
    asset_category: str = 'SCENES' # ['SCENES', 'ANIMATIONS', 'OBJECTS_AND_COLLECTIONS', 'GEOMETRY', 'SHADING', 'IMAGES_AND_SOUNDS', 'ENVIRONMENTS', 'MISC']
    
    asset_library: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...