from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ThemeImageEditor(bpy_struct):
    
    edge_select: Tuple[float, float, float]
    
    editmesh_active: Tuple[float, float, float, float]
    
    face: Tuple[float, float, float, float]
    
    face_back: Tuple[float, float, float, float]
    
    face_dot: Tuple[float, float, float]
    
    face_front: Tuple[float, float, float, float]
    
    face_select: Tuple[float, float, float, float]
    
    facedot_size: int
    
    frame_current: Tuple[float, float, float]
    
    freestyle_face_mark: Tuple[float, float, float, float]
    
    grid: Tuple[float, float, float, float]
    
    handle_align: Tuple[float, float, float]
    
    handle_auto: Tuple[float, float, float]
    
    handle_auto_clamped: Tuple[float, float, float]
    
    handle_free: Tuple[float, float, float]
    
    handle_sel_align: Tuple[float, float, float]
    
    handle_sel_auto: Tuple[float, float, float]
    
    handle_sel_auto_clamped: Tuple[float, float, float]
    
    handle_sel_free: Tuple[float, float, float]
    
    handle_vertex: Tuple[float, float, float]
    
    handle_vertex_select: Tuple[float, float, float]
    
    handle_vertex_size: int
    
    metadatabg: Tuple[float, float, float]
    
    metadatatext: Tuple[float, float, float]
    
    paint_curve_handle: Tuple[float, float, float, float]
    
    paint_curve_pivot: Tuple[float, float, float, float]
    
    preview_stitch_active: Tuple[float, float, float, float]
    
    preview_stitch_edge: Tuple[float, float, float, float]
    
    preview_stitch_face: Tuple[float, float, float, float]
    
    preview_stitch_stitchable: Tuple[float, float, float, float]
    
    preview_stitch_unstitchable: Tuple[float, float, float, float]
    
    preview_stitch_vert: Tuple[float, float, float, float]
    
    scope_back: Tuple[float, float, float, float]
    
    # Settings for space
    space: ThemeSpaceGeneric
    
    uv_shadow: Tuple[float, float, float, float]
    
    vertex: Tuple[float, float, float]
    
    vertex_active: Tuple[float, float, float]
    
    vertex_bevel: Tuple[float, float, float]
    
    vertex_select: Tuple[float, float, float]
    
    vertex_size: int
    
    vertex_unreferenced: Tuple[float, float, float]
    
    wire_edit: Tuple[float, float, float]

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...