from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class OffsetGpencilModifier(GpencilModifier):
    
    # Inverse filter
    invert_layer_pass: bool
    
    # Inverse filter
    invert_layers: bool
    
    # Inverse filter
    invert_material_pass: bool
    
    # Inverse filter
    invert_materials: bool
    
    # Inverse filter
    invert_vertex: bool
    
    # Layer name
    layer: str
    
    # Layer pass index
    layer_pass: int
    
    # Values for change location
    location: Tuple[float, float, float]
    
    # Material used for filtering effect
    material: Material
    
    # Pass index
    pass_index: int
    
    # Values for changes in rotation
    rotation: Tuple[float, float, float]
    
    # Values for changes in scale
    scale: Tuple[float, float, float]
    
    # Vertex group name for modulating the deform
    vertex_group: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...