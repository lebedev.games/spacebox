from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class TextCharacterFormat(bpy_struct):
    
    # Spacing between characters
    kerning: int
    
    material_index: int
    
    use_bold: bool
    
    use_italic: bool
    
    use_small_caps: bool
    
    use_underline: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...