from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class TextureSlot(bpy_struct):
    
    # Mode used to apply the texture
    blend_type: str = 'MIX' # ['MIX', 'DARKEN', 'MULTIPLY', 'LIGHTEN', 'SCREEN', 'ADD', 'OVERLAY', 'SOFT_LIGHT', 'LINEAR_LIGHT', 'DIFFERENCE', 'SUBTRACT', 'DIVIDE', 'HUE', 'SATURATION', 'COLOR', 'VALUE']
    
    # Default color for textures that don’t return RGB or when RGB to intensity is enabled
    color: Tuple[float, float, float]
    
    # Value to use for Ref, Spec, Amb, Emit, Alpha, RayMir, TransLu and Hard
    default_value: float
    
    # Texture slot name
    name: str
    
    # Fine tune of the texture mapping X, Y and Z locations
    offset: Tuple[float, float, float]
    
    # Which output node to use, for node-based textures
    output_node: str = 'DUMMY' # ['DUMMY']
    
    # Set scaling for the texture’s X, Y and Z sizes
    scale: Tuple[float, float, float]
    
    # Texture data-block used by this texture slot
    texture: Texture

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...