from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class BakeSettings(bpy_struct):
    
    # Inflate the active object by the specified distance for baking. This helps matching to points nearer to the outside of the selected object meshes
    cage_extrusion: float
    
    # Object to use as cage instead of calculating the cage from the active object with cage extrusion
    cage_object: Object
    
    # Image filepath to use when saving externally
    filepath: str
    
    # Vertical dimension of the baking map
    height: int
    
    image_settings: ImageFormatSettings
    
    # Extends the baked result as a post process filter
    margin: int
    
    # The maximum ray distance for matching points between the active and selected objects. If zero, there is no limit
    max_ray_distance: float
    
    # Axis to bake in blue channel
    normal_b: str = 'POS_X' # ['POS_X', 'POS_Y', 'POS_Z', 'NEG_X', 'NEG_Y', 'NEG_Z']
    
    # Axis to bake in green channel
    normal_g: str = 'POS_X' # ['POS_X', 'POS_Y', 'POS_Z', 'NEG_X', 'NEG_Y', 'NEG_Z']
    
    # Axis to bake in red channel
    normal_r: str = 'POS_X' # ['POS_X', 'POS_Y', 'POS_Z', 'NEG_X', 'NEG_Y', 'NEG_Z']
    
    # Choose normal space for baking
    normal_space: str = 'OBJECT' # ['OBJECT', 'TANGENT']
    
    # Passes to include in the active baking pass
    pass_filter: Set[str] # {'DIRECT', 'default {', 'TRANSMISSION}', 'DIFFUSE', 'INDIRECT', 'AO', 'EMIT', 'COLOR', 'GLOSSY', 'NONE'}
    
    # Where to save baked image textures
    save_mode: str = 'INTERNAL' # ['INTERNAL', 'EXTERNAL']
    
    # Where to output the baked map
    target: str = 'IMAGE_TEXTURES' # ['IMAGE_TEXTURES', 'VERTEX_COLORS']
    
    # Automatically name the output file with the pass type (external only)
    use_automatic_name: bool
    
    # Cast rays to active object from a cage
    use_cage: bool
    
    # Clear Images before baking (internal only)
    use_clear: bool
    
    # Add ambient occlusion contribution
    use_pass_ambient_occlusion: bool
    
    # Color the pass
    use_pass_color: bool
    
    # Add diffuse contribution
    use_pass_diffuse: bool
    
    # Add direct lighting contribution
    use_pass_direct: bool
    
    # Add emission contribution
    use_pass_emit: bool
    
    # Add glossy contribution
    use_pass_glossy: bool
    
    # Add indirect lighting contribution
    use_pass_indirect: bool
    
    # Add transmission contribution
    use_pass_transmission: bool
    
    # Bake shading on the surface of selected objects to the active object
    use_selected_to_active: bool
    
    # Split external images per material (external only)
    use_split_materials: bool
    
    # Horizontal dimension of the baking map
    width: int

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...