from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class VolumeToMeshModifier(Modifier):
    
    # Reduces the final face count by simplifying geometry where detail is not needed
    adaptivity: float
    
    # Grid in the volume object that is converted to a mesh
    grid_name: str
    
    # Object
    object: Object
    
    # Mode for how the desired voxel size is specified
    resolution_mode: str = 'GRID' # ['GRID', 'VOXEL_AMOUNT', 'VOXEL_SIZE']
    
    # Voxels with a larger value are inside the generated mesh
    threshold: float
    
    # Output faces with smooth shading rather than flat shaded
    use_smooth_shade: bool
    
    # Approximate number of voxels along one axis
    voxel_amount: int
    
    # Smaller values result in a higher resolution output
    voxel_size: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...