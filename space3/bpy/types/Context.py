from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Context(bpy_struct):
    
    area: Area
    
    blend_data: BlendData
    
    collection: Collection
    
    engine: str
    
    gizmo_group: GizmoGroup
    
    layer_collection: LayerCollection
    
    mode: str = 'EDIT_MESH' # ['EDIT_MESH', 'EDIT_CURVE', 'EDIT_SURFACE', 'EDIT_TEXT', 'EDIT_ARMATURE', 'EDIT_METABALL', 'EDIT_LATTICE', 'POSE', 'SCULPT', 'PAINT_WEIGHT', 'PAINT_VERTEX', 'PAINT_TEXTURE', 'PARTICLE', 'OBJECT', 'PAINT_GPENCIL', 'EDIT_GPENCIL', 'SCULPT_GPENCIL', 'WEIGHT_GPENCIL', 'VERTEX_GPENCIL']
    
    preferences: Preferences
    
    region: Region
    
    region_data: RegionView3D
    
    scene: Scene
    
    screen: Screen
    
    space_data: Space
    
    tool_settings: ToolSettings
    
    view_layer: ViewLayer
    
    window: Window
    
    window_manager: WindowManager
    
    workspace: WorkSpace

    def evaluated_depsgraph_get(self, *args, **kwargs) -> None:
        ...

    def copy(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...