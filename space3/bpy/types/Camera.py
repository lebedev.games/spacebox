from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Camera(ID):
    
    # Camera lens field of view
    angle: float
    
    # Camera lens horizontal field of view
    angle_x: float
    
    # Camera lens vertical field of view
    angle_y: float
    
    # Animation data for this data-block
    animation_data: AnimData
    
    # List of background images
    background_images: CameraBackgroundImages
    
    # Camera far clipping distance
    clip_end: float
    
    # Camera near clipping distance
    clip_start: float
    
    # Cycles camera settings
    cycles: Any
    
    # Apparent size of the Camera object in the 3D View
    display_size: float
    
    dof: CameraDOFSettings
    
    # Perspective Camera lens value in millimeters
    lens: float
    
    # Unit to edit lens in for the user interface
    lens_unit: str = 'MILLIMETERS' # ['MILLIMETERS', 'FOV']
    
    # Orthographic Camera scale (similar to zoom)
    ortho_scale: float
    
    # Opacity (alpha) of the darkened overlay in Camera view
    passepartout_alpha: float
    
    # Method to fit image and field of view angle inside the sensor
    sensor_fit: str = 'AUTO' # ['AUTO', 'HORIZONTAL', 'VERTICAL']
    
    # Vertical size of the image sensor area in millimeters
    sensor_height: float
    
    # Horizontal size of the image sensor area in millimeters
    sensor_width: float
    
    # Camera horizontal shift
    shift_x: float
    
    # Camera vertical shift
    shift_y: float
    
    # Display reference images behind objects in the 3D View
    show_background_images: bool
    
    # Display center composition guide inside the camera view
    show_composition_center: bool
    
    # Display diagonal center composition guide inside the camera view
    show_composition_center_diagonal: bool
    
    # Display golden ratio composition guide inside the camera view
    show_composition_golden: bool
    
    # Display golden triangle A composition guide inside the camera view
    show_composition_golden_tria_a: bool
    
    # Display golden triangle B composition guide inside the camera view
    show_composition_golden_tria_b: bool
    
    # Display harmony A composition guide inside the camera view
    show_composition_harmony_tri_a: bool
    
    # Display harmony B composition guide inside the camera view
    show_composition_harmony_tri_b: bool
    
    # Display rule of thirds composition guide inside the camera view
    show_composition_thirds: bool
    
    # Display the clipping range and focus point on the camera
    show_limits: bool
    
    # Display a line from the Camera to indicate the mist area
    show_mist: bool
    
    # Show the active Camera’s name in Camera view
    show_name: bool
    
    # Show a darkened overlay outside the image area in Camera view
    show_passepartout: bool
    
    # Show TV title safe and action safe areas in Camera view
    show_safe_areas: bool
    
    # Show safe areas to fit content in a different aspect ratio
    show_safe_center: bool
    
    # Show sensor size (film gate) in Camera view
    show_sensor: bool
    
    stereo: CameraStereoData
    
    # Camera types
    type: str = 'PERSP' # ['PERSP', 'ORTHO', 'PANO']

    def view_frame(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...