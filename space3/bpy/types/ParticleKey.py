from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ParticleKey(bpy_struct):
    
    # Key angular velocity
    angular_velocity: Tuple[float, float, float]
    
    # Key location
    location: Tuple[float, float, float]
    
    # Key rotation quaternion
    rotation: Tuple[float, float, float, float]
    
    # Time of key over the simulation
    time: float
    
    # Key velocity
    velocity: Tuple[float, float, float]

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...