from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class GPencilSculptGuide(bpy_struct):
    
    # Direction of lines
    angle: float
    
    # Angle snapping
    angle_snap: float
    
    # Custom reference point for guides
    location: Tuple[float, float, float]
    
    # Object used for reference point
    reference_object: Object
    
    # Type of speed guide
    reference_point: str = 'CURSOR' # ['CURSOR', 'CUSTOM', 'OBJECT']
    
    # Guide spacing
    spacing: float
    
    # Type of speed guide
    type: str = 'CIRCULAR' # ['CIRCULAR', 'RADIAL', 'PARALLEL', 'GRID', 'ISO']
    
    # Enable speed guides
    use_guide: bool
    
    # Enable snapping to guides angle or spacing options
    use_snapping: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...