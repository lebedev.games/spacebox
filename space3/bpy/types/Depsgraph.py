from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Depsgraph(bpy_struct):
    
    # All evaluated data-blocks
    ids: List[ID]
    
    # Evaluation mode
    mode: str = 'VIEWPORT' # ['VIEWPORT', 'RENDER']
    
    # All object instances to display or render (Warning: Only use this as an iterator, never as a sequence, and do not keep any references to its items)
    object_instances: List[DepsgraphObjectInstance]
    
    # Evaluated objects in the dependency graph
    objects: List[Object]
    
    # Original scene dependency graph is built for
    scene: Scene
    
    # Original scene dependency graph is built for
    scene_eval: Scene
    
    # Updates to data-blocks
    updates: List[DepsgraphUpdate]
    
    # Original view layer dependency graph is built for
    view_layer: ViewLayer
    
    # Original view layer dependency graph is built for
    view_layer_eval: ViewLayer

    def debug_relations_graphviz(self, *args, **kwargs) -> None:
        ...

    def debug_stats_gnuplot(self, *args, **kwargs) -> None:
        ...

    def debug_tag_update(self, *args, **kwargs) -> None:
        ...

    def debug_stats(self, *args, **kwargs) -> None:
        ...

    def update(self, *args, **kwargs) -> None:
        ...

    def id_eval_get(self, *args, **kwargs) -> ID:
        ...

    def id_type_updated(self, *args, **kwargs) -> bool:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...