from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ShaderNodeTexVoronoi(ShaderNode):
    
    # Color mapping settings
    color_mapping: ColorMapping
    
    distance: str = 'EUCLIDEAN' # ['EUCLIDEAN', 'MANHATTAN', 'CHEBYCHEV', 'MINKOWSKI']
    
    feature: str = 'F1' # ['F1', 'F2', 'SMOOTH_F1', 'DISTANCE_TO_EDGE', 'N_SPHERE_RADIUS']
    
    # Texture coordinate mapping settings
    texture_mapping: TexMapping
    
    voronoi_dimensions: str = '1D' # ['1D', '2D', '3D', '4D']

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...