from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Nodes(bpy_struct):
    
    # Active node in this tree
    active: Node

    def new(self, *args, **kwargs) -> Node:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    def clear(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...