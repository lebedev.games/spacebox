from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ParticleSettingsTextureSlot(TextureSlot):
    
    # Amount texture affects child clump
    clump_factor: float
    
    # Amount texture affects particle damping
    damp_factor: float
    
    # Amount texture affects particle density
    density_factor: float
    
    # Amount texture affects particle force fields
    field_factor: float
    
    # Amount texture affects particle gravity
    gravity_factor: float
    
    # Amount texture affects child kink amplitude
    kink_amp_factor: float
    
    # Amount texture affects child kink frequency
    kink_freq_factor: float
    
    # Amount texture affects child hair length
    length_factor: float
    
    # Amount texture affects particle life time
    life_factor: float
    
    mapping: str = 'FLAT' # ['FLAT', 'CUBE', 'TUBE', 'SPHERE']
    
    mapping_x: str = 'X' # ['NONE', 'X', 'Y', 'Z']
    
    mapping_y: str = 'Y' # ['NONE', 'X', 'Y', 'Z']
    
    mapping_z: str = 'Z' # ['NONE', 'X', 'Y', 'Z']
    
    # Object to use for mapping with Object texture coordinates
    object: Object
    
    # Amount texture affects child roughness
    rough_factor: float
    
    # Amount texture affects physical particle size
    size_factor: float
    
    # Texture coordinates used to map the texture onto the background
    texture_coords: str = 'UV' # ['GLOBAL', 'OBJECT', 'UV', 'ORCO', 'STRAND']
    
    # Amount texture affects particle emission time
    time_factor: float
    
    # Amount texture affects child twist
    twist_factor: float
    
    # Affect the child clumping
    use_map_clump: bool
    
    # Affect the particle velocity damping
    use_map_damp: bool
    
    # Affect the density of the particles
    use_map_density: bool
    
    # Affect the particle force fields
    use_map_field: bool
    
    # Affect the particle gravity
    use_map_gravity: bool
    
    # Affect the child kink amplitude
    use_map_kink_amp: bool
    
    # Affect the child kink frequency
    use_map_kink_freq: bool
    
    # Affect the child hair length
    use_map_length: bool
    
    # Affect the life time of the particles
    use_map_life: bool
    
    # Affect the child rough
    use_map_rough: bool
    
    # Affect the particle size
    use_map_size: bool
    
    # Affect the emission time of the particles
    use_map_time: bool
    
    # Affect the child twist
    use_map_twist: bool
    
    # Affect the particle initial velocity
    use_map_velocity: bool
    
    # UV map to use for mapping with UV texture coordinates
    uv_layer: str
    
    # Amount texture affects particle initial velocity
    velocity_factor: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...