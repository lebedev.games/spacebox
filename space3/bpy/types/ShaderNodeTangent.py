from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ShaderNodeTangent(ShaderNode):
    
    # Axis for radial tangents
    axis: str = 'X' # ['X', 'Y', 'Z']
    
    # Method to use for the tangent
    direction_type: str = 'RADIAL' # ['RADIAL', 'UV_MAP']
    
    # UV Map for tangent generated from UV
    uv_map: str

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...