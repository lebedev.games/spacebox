from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class BoneGroups(bpy_struct):
    
    # Active bone group for this pose
    active: BoneGroup
    
    # Active index in bone groups array
    active_index: int

    def new(self, *args, **kwargs) -> BoneGroup:
        ...

    def remove(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...