from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class FreestyleLineStyle(ID):
    
    # Active texture slot being displayed
    active_texture: Texture
    
    # Index of active texture slot
    active_texture_index: int
    
    # Base alpha transparency, possibly modified by alpha transparency modifiers
    alpha: float
    
    # List of alpha transparency modifiers
    alpha_modifiers: LineStyleAlphaModifiers
    
    # Maximum 2D angle for splitting chains
    angle_max: float
    
    # Minimum 2D angle for splitting chains
    angle_min: float
    
    # Animation data for this data-block
    animation_data: AnimData
    
    # Select the shape of both ends of strokes
    caps: str = 'BUTT' # ['BUTT', 'ROUND', 'SQUARE']
    
    # Chain count for the selection of first N chains
    chain_count: int
    
    # Select the way how feature edges are jointed to form chains
    chaining: str = 'PLAIN' # ['PLAIN', 'SKETCHY']
    
    # Base line color, possibly modified by line color modifiers
    color: Tuple[float, float, float]
    
    # List of line color modifiers
    color_modifiers: LineStyleColorModifiers
    
    # Length of the 1st dash for dashed lines
    dash1: int
    
    # Length of the 2nd dash for dashed lines
    dash2: int
    
    # Length of the 3rd dash for dashed lines
    dash3: int
    
    # Length of the 1st gap for dashed lines
    gap1: int
    
    # Length of the 2nd gap for dashed lines
    gap2: int
    
    # Length of the 3rd gap for dashed lines
    gap3: int
    
    # List of stroke geometry modifiers
    geometry_modifiers: LineStyleGeometryModifiers
    
    # Select the way how the sort key is computed for each chain
    integration_type: str = 'MEAN' # ['MEAN', 'MIN', 'MAX', 'FIRST', 'LAST']
    
    # Maximum curvilinear 2D length for the selection of chains
    length_max: float
    
    # Minimum curvilinear 2D length for the selection of chains
    length_min: float
    
    # If true, chains of feature edges are split at material boundaries
    material_boundary: bool
    
    # Node tree for node-based shaders
    node_tree: NodeTree
    
    # Select the property panel to be shown
    panel: str = 'STROKES' # ['STROKES', 'COLOR', 'ALPHA', 'THICKNESS', 'GEOMETRY', 'TEXTURE']
    
    # Number of rounds in a sketchy multiple touch
    rounds: int
    
    # Select the sort key to determine the stacking order of chains
    sort_key: str = 'DISTANCE_FROM_CAMERA' # ['DISTANCE_FROM_CAMERA', '2D_LENGTH', 'PROJECTED_X', 'PROJECTED_Y']
    
    # Select the sort order
    sort_order: str = 'DEFAULT' # ['DEFAULT', 'REVERSE']
    
    # Length of the 1st dash for splitting
    split_dash1: int
    
    # Length of the 2nd dash for splitting
    split_dash2: int
    
    # Length of the 3rd dash for splitting
    split_dash3: int
    
    # Length of the 1st gap for splitting
    split_gap1: int
    
    # Length of the 2nd gap for splitting
    split_gap2: int
    
    # Length of the 3rd gap for splitting
    split_gap3: int
    
    # Curvilinear 2D length for chain splitting
    split_length: float
    
    # Texture slots defining the mapping and influence of textures
    texture_slots: LineStyleTextureSlots
    
    # Spacing for textures along stroke length
    texture_spacing: float
    
    # Base line thickness, possibly modified by line thickness modifiers
    thickness: float
    
    # List of line thickness modifiers
    thickness_modifiers: LineStyleThicknessModifiers
    
    # Thickness position of silhouettes and border edges (applicable when plain chaining is used with the Same Object option)
    thickness_position: str = 'CENTER' # ['CENTER', 'INSIDE', 'OUTSIDE', 'RELATIVE']
    
    # A number between 0 (inside) and 1 (outside) specifying the relative position of stroke thickness
    thickness_ratio: float
    
    # Split chains at points with angles larger than the maximum 2D angle
    use_angle_max: bool
    
    # Split chains at points with angles smaller than the minimum 2D angle
    use_angle_min: bool
    
    # Enable the selection of first N chains
    use_chain_count: bool
    
    # Enable chaining of feature edges
    use_chaining: bool
    
    # Enable or disable dashed line
    use_dashed_line: bool
    
    # Enable the selection of chains by a maximum 2D length
    use_length_max: bool
    
    # Enable the selection of chains by a minimum 2D length
    use_length_min: bool
    
    # Use shader nodes for the line style
    use_nodes: bool
    
    # If true, only feature edges of the same object are joined
    use_same_object: bool
    
    # Arrange the stacking order of strokes
    use_sorting: bool
    
    # Enable chain splitting by curvilinear 2D length
    use_split_length: bool
    
    # Enable chain splitting by dashed line patterns
    use_split_pattern: bool
    
    # Enable or disable textured strokes
    use_texture: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...