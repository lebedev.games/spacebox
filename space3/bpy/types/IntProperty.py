from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class IntProperty(Property):
    
    # Length of each dimension of the array
    array_dimensions: Tuple[int, int, int]
    
    # Maximum length of the array, 0 means unlimited
    array_length: int
    
    # Default value for this number
    default: int
    
    # Default value for this array
    default_array: Tuple[int, int, int]
    
    # Maximum value used by buttons
    hard_max: int
    
    # Minimum value used by buttons
    hard_min: int
    
    is_array: bool
    
    # Maximum value used by buttons
    soft_max: int
    
    # Minimum value used by buttons
    soft_min: int
    
    # Step size used by number buttons, for floats 1/100th of the step size
    step: int

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...