from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class LimitRotationConstraint(Constraint):
    
    # Highest X value to allow
    max_x: float
    
    # Highest Y value to allow
    max_y: float
    
    # Highest Z value to allow
    max_z: float
    
    # Lowest X value to allow
    min_x: float
    
    # Lowest Y value to allow
    min_y: float
    
    # Lowest Z value to allow
    min_z: float
    
    # Use the minimum X value
    use_limit_x: bool
    
    # Use the minimum Y value
    use_limit_y: bool
    
    # Use the minimum Z value
    use_limit_z: bool
    
    # Transform tools are affected by this constraint as well
    use_transform_limit: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...