from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ChildOfConstraint(Constraint):
    
    # Transformation matrix to apply before
    inverse_matrix: List[float]
    
    # Set to true to request recalculation of the inverse matrix
    set_inverse_pending: bool
    
    # Armature bone, mesh or lattice vertex group, …
    subtarget: str
    
    # Target object
    target: Object
    
    # Use X Location of Parent
    use_location_x: bool
    
    # Use Y Location of Parent
    use_location_y: bool
    
    # Use Z Location of Parent
    use_location_z: bool
    
    # Use X Rotation of Parent
    use_rotation_x: bool
    
    # Use Y Rotation of Parent
    use_rotation_y: bool
    
    # Use Z Rotation of Parent
    use_rotation_z: bool
    
    # Use X Scale of Parent
    use_scale_x: bool
    
    # Use Y Scale of Parent
    use_scale_y: bool
    
    # Use Z Scale of Parent
    use_scale_z: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...