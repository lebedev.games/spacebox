from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CurveMapPoint(bpy_struct):
    
    # Curve interpolation at this point: Bezier or vector
    handle_type: str = 'AUTO' # ['AUTO', 'AUTO_CLAMPED', 'VECTOR']
    
    # X/Y coordinates of the curve point
    location: Tuple[float, float]
    
    # Selection state of the curve point
    select: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...