from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CompositorNodeTrackPos(CompositorNode):
    
    clip: MovieClip
    
    # Frame to be used for relative position
    frame_relative: int
    
    # Which marker position to use for output
    position: str = 'ABSOLUTE' # ['ABSOLUTE', 'RELATIVE_START', 'RELATIVE_FRAME', 'ABSOLUTE_FRAME']
    
    track_name: str
    
    tracking_object: str

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    def update(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...