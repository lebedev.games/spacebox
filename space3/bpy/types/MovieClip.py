from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MovieClip(ID):
    
    # Animation data for this data-block
    animation_data: AnimData
    
    # Input color space settings
    colorspace_settings: ColorManagedInputColorspaceSettings
    
    # Display Aspect for this clip, does not affect rendering
    display_aspect: Tuple[float, float]
    
    # Filename of the movie or sequence file
    filepath: str
    
    # Detected frame rate of the movie clip in frames per second
    fps: float
    
    # Detected duration of movie clip in frames
    frame_duration: int
    
    # Offset of footage first frame relative to its file name (affects only how footage is loading, does not change data associated with a clip)
    frame_offset: int
    
    # Global scene frame number at which this movie starts playing (affects all data associated with a clip)
    frame_start: int
    
    # Grease pencil data for this movie clip
    grease_pencil: GreasePencil
    
    proxy: MovieClipProxy
    
    # Width and height in pixels, zero when image data cant be loaded
    size: Tuple[int, int]
    
    # Where the clip comes from
    source: str = 'SEQUENCE' # ['SEQUENCE', 'MOVIE']
    
    tracking: MovieTracking
    
    # Use a preview proxy and/or timecode index for this clip
    use_proxy: bool
    
    # Create proxy images in a custom directory (default is movie location)
    use_proxy_custom_directory: bool

    def metadata(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...