from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class NoiseGpencilModifier(GpencilModifier):
    
    # Custom curve to apply effect
    curve: CurveMapping
    
    # Amount of noise to apply
    factor: float
    
    # Amount of noise to apply to opacity
    factor_strength: float
    
    # Amount of noise to apply to thickness
    factor_thickness: float
    
    # Amount of noise to apply uv rotation
    factor_uvs: float
    
    # Inverse filter
    invert_layer_pass: bool
    
    # Inverse filter
    invert_layers: bool
    
    # Inverse filter
    invert_material_pass: bool
    
    # Inverse filter
    invert_materials: bool
    
    # Inverse filter
    invert_vertex: bool
    
    # Layer name
    layer: str
    
    # Layer pass index
    layer_pass: int
    
    # Material used for filtering effect
    material: Material
    
    # Offset the noise along the strokes
    noise_offset: float
    
    # Scale the noise frequency
    noise_scale: float
    
    # Pass index
    pass_index: int
    
    # Use random values over time
    random: bool
    
    # Random seed
    seed: int
    
    # Number of frames before recalculate random values again
    step: int
    
    # Use a custom curve to define noise effect along the strokes
    use_custom_curve: bool
    
    # Vertex group name for modulating the deform
    vertex_group: str

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...