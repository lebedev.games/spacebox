from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ColorManagedViewSettings(bpy_struct):
    
    # Color curve mapping applied before display transform
    curve_mapping: CurveMapping
    
    # Exposure (stops) applied before display transform
    exposure: float
    
    # Amount of gamma modification applied after display transform
    gamma: float
    
    # Additional transform applied before view transform for artistic needs
    look: str = 'NONE' # ['NONE']
    
    # Use RGB curved for pre-display transformation
    use_curve_mapping: bool
    
    # View used when converting image to a display space
    view_transform: str = 'NONE' # ['NONE']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...