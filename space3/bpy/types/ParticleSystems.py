from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ParticleSystems(bpy_struct):
    
    # Active particle system being displayed
    active: ParticleSystem
    
    # Index of active particle system slot
    active_index: int

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...