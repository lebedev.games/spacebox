from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class KeyingSetsAll(bpy_struct):
    
    # Active Keying Set used to insert/delete keyframes
    active: KeyingSet
    
    # Current Keying Set index (negative for ‘builtin’ and positive for ‘absolute’)
    active_index: int

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...