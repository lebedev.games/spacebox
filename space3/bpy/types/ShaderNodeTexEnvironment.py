from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ShaderNodeTexEnvironment(ShaderNode):
    
    # Color mapping settings
    color_mapping: ColorMapping
    
    image: Image
    
    # Parameters defining which layer, pass and frame of the image is displayed
    image_user: ImageUser
    
    # Texture interpolation
    interpolation: str = 'Linear' # ['Linear', 'Closest', 'Cubic', 'Smart']
    
    # Projection of the input image
    projection: str = 'EQUIRECTANGULAR' # ['EQUIRECTANGULAR', 'MIRROR_BALL']
    
    # Texture coordinate mapping settings
    texture_mapping: TexMapping

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...