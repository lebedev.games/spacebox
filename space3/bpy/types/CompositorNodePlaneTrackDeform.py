from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class CompositorNodePlaneTrackDeform(CompositorNode):
    
    clip: MovieClip
    
    # Number of motion blur samples
    motion_blur_samples: int
    
    # Exposure for motion blur as a factor of FPS
    motion_blur_shutter: float
    
    plane_track_name: str
    
    tracking_object: str
    
    # Use multi-sampled motion blur of the mask
    use_motion_blur: bool

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    def update(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...