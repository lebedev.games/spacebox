from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class DecimateModifier(Modifier):
    
    # Only dissolve angles below this (planar only)
    angle_limit: float
    
    decimate_type: str = 'COLLAPSE' # ['COLLAPSE', 'UNSUBDIV', 'DISSOLVE']
    
    # Limit merging geometry
    delimit: Set[str] # {'default {NORMAL', 'SHARP', 'NORMAL', 'SEAM', 'MATERIAL', 'UV}'}
    
    # The current number of faces in the decimated mesh
    face_count: int
    
    # Invert vertex group influence (collapse only)
    invert_vertex_group: bool
    
    # Number of times reduce the geometry (unsubdivide only)
    iterations: int
    
    # Ratio of triangles to reduce to (collapse only)
    ratio: float
    
    # Axis of symmetry
    symmetry_axis: str = 'X' # ['X', 'Y', 'Z']
    
    # Keep triangulated faces resulting from decimation (collapse only)
    use_collapse_triangulate: bool
    
    # Dissolve all vertices in between face boundaries (planar only)
    use_dissolve_boundaries: bool
    
    # Maintain symmetry on an axis
    use_symmetry: bool
    
    # Vertex group name (collapse only)
    vertex_group: str
    
    # Vertex group strength
    vertex_group_factor: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...