from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ShaderNodeVectorTransform(ShaderNode):
    
    # Space to convert from
    convert_from: str = 'WORLD' # ['WORLD', 'OBJECT', 'CAMERA']
    
    # Space to convert to
    convert_to: str = 'WORLD' # ['WORLD', 'OBJECT', 'CAMERA']
    
    vector_type: str = 'VECTOR' # ['POINT', 'VECTOR', 'NORMAL']

    @classmethod
    def is_registered_node_type(cls, *args, **kwargs) -> None:
        ...

    @classmethod
    def input_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def output_template(cls, *args, **kwargs) -> NodeInternalSocketTemplate:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...