from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class UDIMTile(bpy_struct):
    
    # Tile label
    label: str
    
    # Number of the position that this tile covers
    number: int

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...