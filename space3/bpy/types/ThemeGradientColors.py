from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ThemeGradientColors(bpy_struct):
    
    # Type of background in the 3D viewport
    background_type: str = 'SINGLE_COLOR' # ['SINGLE_COLOR', 'LINEAR', 'RADIAL']
    
    gradient: Tuple[float, float, float]
    
    high_gradient: Tuple[float, float, float]

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...