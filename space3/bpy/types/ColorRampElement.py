from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ColorRampElement(bpy_struct):
    
    # Set alpha of selected color stop
    alpha: float
    
    # Set color of selected color stop
    color: Tuple[float, float, float, float]
    
    # Set position of selected color stop
    position: float

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...