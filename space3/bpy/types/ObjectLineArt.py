from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ObjectLineArt(bpy_struct):
    
    # Angles smaller than this will be treated as creases
    crease_threshold: float
    
    # How to use this object in line art calculation
    usage: str = 'INHERIT' # ['INHERIT', 'INCLUDE', 'OCCLUSION_ONLY', 'EXCLUDE', 'INTERSECTION_ONLY', 'NO_INTERSECTION']
    
    # Use this object’s crease setting to overwrite scene global
    use_crease_override: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...