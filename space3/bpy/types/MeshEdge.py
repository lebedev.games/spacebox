from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class MeshEdge(bpy_struct):
    
    # Weight used by the Bevel modifier
    bevel_weight: float
    
    # Weight used by the Subdivision Surface modifier for creasing
    crease: float
    
    hide: bool
    
    # Index of this edge
    index: int
    
    # Loose edge
    is_loose: bool
    
    select: bool
    
    # Sharp edge for the Edge Split modifier
    use_edge_sharp: bool
    
    # Edge mark for Freestyle line rendering
    use_freestyle_mark: bool
    
    # Seam edge for UV unwrapping
    use_seam: bool
    
    # Vertex indices
    vertices: Tuple[int, int]
    
    # (readonly)
    key: Any

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...