from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ToolSettings(bpy_struct):
    
    annotation_stroke_placement_image_editor: str = 'VIEW' # ['CURSOR', 'VIEW', 'SURFACE']
    
    annotation_stroke_placement_sequencer_preview: str = 'VIEW' # ['CURSOR', 'VIEW', 'SURFACE']
    
    annotation_stroke_placement_view2d: str = 'VIEW' # ['CURSOR', 'VIEW', 'SURFACE']
    
    # How annotation strokes are orientated in 3D space
    annotation_stroke_placement_view3d: str = 'CURSOR' # ['CURSOR', 'VIEW', 'SURFACE']
    
    # Thickness of annotation strokes
    annotation_thickness: int
    
    # Mode of automatic keyframe insertion for Objects, Bones and Masks
    auto_keying_mode: str = 'ADD_REPLACE_KEYS' # ['ADD_REPLACE_KEYS', 'REPLACE_KEYS']
    
    curve_paint_settings: CurvePaintSettings
    
    # Used for defining a profile’s path
    custom_bevel_profile_preset: CurveProfile
    
    # Threshold distance for Auto Merge
    double_threshold: float
    
    # Settings for Grease Pencil Interpolation tools
    gpencil_interpolate: GPencilInterpolateSettings
    
    gpencil_paint: GpPaint
    
    # Settings for stroke sculpting tools and brushes
    gpencil_sculpt: GPencilSculptSettings
    
    gpencil_sculpt_paint: GpSculptPaint
    
    gpencil_selectmode_edit: str = 'POINT' # ['POINT', 'STROKE', 'SEGMENT']
    
    gpencil_stroke_placement_view3d: str = 'ORIGIN' # ['ORIGIN', 'CURSOR', 'SURFACE', 'STROKE']
    
    gpencil_stroke_snap_mode: str = 'NONE' # ['NONE', 'ENDS', 'FIRST']
    
    gpencil_vertex_paint: GpVertexPaint
    
    gpencil_weight_paint: GpWeightPaint
    
    image_paint: ImagePaint
    
    # Type of keyframes to create when inserting keyframes
    keyframe_type: str = 'KEYFRAME' # ['KEYFRAME', 'BREAKDOWN', 'MOVING_HOLD', 'EXTREME', 'JITTER']
    
    # Prevent marker editing
    lock_markers: bool
    
    # Restrict select to the current mode
    lock_object_mode: bool
    
    # Which mesh elements selection works on
    mesh_select_mode: bool
    
    # Normal Vector used to copy, add or multiply
    normal_vector: Tuple[float, float, float]
    
    particle_edit: ParticleEdit
    
    # Falloff type for proportional editing mode
    proportional_edit_falloff: str = 'SMOOTH' # ['SMOOTH', 'SPHERE', 'ROOT', 'INVERSE_SQUARE', 'SHARP', 'LINEAR', 'CONSTANT', 'RANDOM']
    
    # Display size for proportional editing circle
    proportional_size: float
    
    sculpt: Sculpt
    
    sequencer_tool_settings: SequencerToolSettings
    
    # Display only faces with the currently displayed image assigned
    show_uv_local_view: bool
    
    # Type of element to snap to
    snap_elements: Set[str] # {'VERTEX', 'EDGE_MIDPOINT', 'EDGE', 'FACE', 'EDGE_PERPENDICULAR}', 'VOLUME', 'INCREMENT', 'default {INCREMENT'}
    
    # Type of element to snap to
    snap_node_element: str = 'GRID' # ['GRID', 'NODE_X', 'NODE_Y', 'NODE_XY']
    
    # Which part to snap onto the target
    snap_target: str = 'CLOSEST' # ['CLOSEST', 'CENTER', 'MEDIAN', 'ACTIVE']
    
    # Type of element to snap to
    snap_uv_element: str = 'INCREMENT' # ['INCREMENT', 'VERTEX']
    
    statvis: MeshStatVis
    
    # Pivot center for rotation/scaling
    transform_pivot_point: str = 'MEDIAN_POINT' # ['BOUNDING_BOX_CENTER', 'CURSOR', 'INDIVIDUAL_ORIGINS', 'MEDIAN_POINT', 'ACTIVE_ELEMENT']
    
    unified_paint_settings: UnifiedPaintSettings
    
    # Ensure all bone-deforming vertex groups add up to 1.0 while weight painting
    use_auto_normalize: bool
    
    # Changing edge seams recalculates UV unwrap
    use_edge_path_live_unwrap: bool
    
    # Join by distance last drawn stroke with previous strokes in the active layer
    use_gpencil_automerge_strokes: bool
    
    # When creating new frames, the strokes from the previous/active frame are included as the basis for the new one
    use_gpencil_draw_additive: bool
    
    # When draw new strokes, the new stroke is drawn below of all strokes in the layer
    use_gpencil_draw_onback: bool
    
    # Only sculpt selected stroke points
    use_gpencil_select_mask_point: bool
    
    # Only sculpt selected stroke points between other strokes
    use_gpencil_select_mask_segment: bool
    
    # Only sculpt selected stroke
    use_gpencil_select_mask_stroke: bool
    
    # Only use the first and last parts of the stroke for snapping
    use_gpencil_stroke_endpoints: bool
    
    # Show compact list of color instead of thumbnails
    use_gpencil_thumbnail_list: bool
    
    # Only paint selected stroke points
    use_gpencil_vertex_select_mask_point: bool
    
    # Only paint selected stroke points between other strokes
    use_gpencil_vertex_select_mask_segment: bool
    
    # Only paint selected stroke
    use_gpencil_vertex_select_mask_stroke: bool
    
    # When creating new strokes, the weight data is added according to the current vertex group and weight, if no vertex group selected, weight is not added
    use_gpencil_weight_data_add: bool
    
    # For channels with cyclic extrapolation, keyframe insertion is automatically remapped inside the cycle time range, and keeps ends in sync
    use_keyframe_cycle_aware: bool
    
    # Automatic keyframe insertion for Objects, Bones and Masks
    use_keyframe_insert_auto: bool
    
    # Automatic keyframe insertion using active Keying Set only
    use_keyframe_insert_keyingset: bool
    
    # Display bone-deforming groups as if all locked deform groups were deleted, and the remaining ones were re-normalized
    use_lock_relative: bool
    
    # Automatically merge vertices moved to the same location
    use_mesh_automerge: bool
    
    # Automatically split edges and faces
    use_mesh_automerge_and_split: bool
    
    # Paint across the weights of all selected bones, maintaining their relative influence
    use_multipaint: bool
    
    # Proportional editing in action editor
    use_proportional_action: bool
    
    # Proportional Editing using connected geometry only
    use_proportional_connected: bool
    
    # Proportional edit mode
    use_proportional_edit: bool
    
    # Proportional editing mask mode
    use_proportional_edit_mask: bool
    
    # Proportional editing object mode
    use_proportional_edit_objects: bool
    
    # Proportional editing in FCurve editor
    use_proportional_fcurve: bool
    
    # Proportional Editing using screen space locations
    use_proportional_projected: bool
    
    # Add a new NLA Track + Strip for every loop/pass made over the animation to allow non-destructive tweaking
    use_record_with_nla: bool
    
    # Snap during transform
    use_snap: bool
    
    # Align rotation with the snapping target
    use_snap_align_rotation: bool
    
    # Exclude back facing geometry from snapping
    use_snap_backface_culling: bool
    
    # Absolute grid alignment while translating (based on the pivot center)
    use_snap_grid_absolute: bool
    
    # Consider objects as whole when finding volume center
    use_snap_peel_object: bool
    
    # Project individual elements on the surface of other objects
    use_snap_project: bool
    
    # Rotate is affected by the snapping settings
    use_snap_rotate: bool
    
    # Scale is affected by snapping settings
    use_snap_scale: bool
    
    # Snap onto itself (Edit Mode Only)
    use_snap_self: bool
    
    # Move is affected by snapping settings
    use_snap_translate: bool
    
    # Correct data such as UV’s and vertex colors when transforming
    use_transform_correct_face_attributes: bool
    
    # During the Face Attributes correction, merge attributes connected to the same vertex
    use_transform_correct_keep_connected: bool
    
    # Transform object origins, while leaving the shape in place
    use_transform_data_origin: bool
    
    # Only transform object locations, without affecting rotation or scaling
    use_transform_pivot_point_align: bool
    
    # Transform the parents, leaving the children in place
    use_transform_skip_children: bool
    
    # Keep UV and edit mode mesh selection in sync
    use_uv_select_sync: bool
    
    # Algorithm used for UV relaxation
    uv_relax_method: str = 'LAPLACIAN' # ['LAPLACIAN', 'HC']
    
    uv_sculpt: UvSculpt
    
    # Brush operates on all islands
    uv_sculpt_all_islands: bool
    
    # Disable editing of boundary edges
    uv_sculpt_lock_borders: bool
    
    # UV selection and display mode
    uv_select_mode: str = 'VERTEX' # ['VERTEX', 'EDGE', 'FACE', 'ISLAND']
    
    # Filter Vertex groups for Display
    vertex_group_subset: str = 'ALL' # ['ALL', 'BONE_DEFORM', 'OTHER_DEFORM']
    
    # Display unweighted vertices
    vertex_group_user: str = 'NONE' # ['NONE', 'ACTIVE', 'ALL']
    
    # Weight to assign in vertex groups
    vertex_group_weight: float
    
    vertex_paint: VertexPaint
    
    weight_paint: VertexPaint
    
    # Action when dragging in the viewport
    workspace_tool_type: str = 'FALLBACK' # ['DEFAULT', 'FALLBACK']

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...