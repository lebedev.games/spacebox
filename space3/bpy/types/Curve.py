from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class Curve(ID):
    
    # Animation data for this data-block
    animation_data: AnimData
    
    # Radius of the bevel geometry, not including extrusion
    bevel_depth: float
    
    # Define where along the spline the curve geometry ends (0 for the beginning, 1 for the end)
    bevel_factor_end: float
    
    # Determine how the geometry end factor is mapped to a spline
    bevel_factor_mapping_end: str = 'RESOLUTION' # ['RESOLUTION', 'SEGMENTS', 'SPLINE']
    
    # Determine how the geometry start factor is mapped to a spline
    bevel_factor_mapping_start: str = 'RESOLUTION' # ['RESOLUTION', 'SEGMENTS', 'SPLINE']
    
    # Define where along the spline the curve geometry starts (0 for the beginning, 1 for the end)
    bevel_factor_start: float
    
    # Determine how to build the curve’s bevel geometry
    bevel_mode: str = 'ROUND' # ['ROUND', 'OBJECT', 'PROFILE']
    
    # The name of the Curve object that defines the bevel shape
    bevel_object: Object
    
    # The path for the curve’s custom profile
    bevel_profile: CurveProfile
    
    # The number of segments in each quarter-circle of the bevel
    bevel_resolution: int
    
    # Cycles mesh settings
    cycles: Any
    
    # Select 2D or 3D curve type
    dimensions: str = '2D' # ['2D', '3D']
    
    # Parametric position along the length of the curve that Objects ‘following’ it should be at (position is evaluated by dividing by the ‘Path Length’ value)
    eval_time: float
    
    # Length of the depth added in the local Z direction along the curve, perpendicular to its normals
    extrude: float
    
    # Mode of filling curve
    fill_mode: str = 'FULL' # ['FULL', 'BACK', 'FRONT', 'HALF']
    
    # True when used in editmode
    is_editmode: bool
    
    materials: IDMaterials
    
    # Distance to move the curve parallel to its normals
    offset: float
    
    # The number of frames that are needed to traverse the path, defining the maximum value for the ‘Evaluation Time’ setting
    path_duration: int
    
    # Surface resolution in U direction used while rendering (zero uses preview resolution)
    render_resolution_u: int
    
    # Surface resolution in V direction used while rendering (zero uses preview resolution)
    render_resolution_v: int
    
    # Number of computed points in the U direction between every pair of control points
    resolution_u: int
    
    # The number of computed points in the V direction between every pair of control points
    resolution_v: int
    
    shape_keys: Key
    
    # Collection of splines in this curve data object
    splines: CurveSplines
    
    # Curve object name that defines the taper (width)
    taper_object: Object
    
    # Determine how the effective radius of the spline point is computed when a taper object is specified
    taper_radius_mode: str = 'OVERRIDE' # ['OVERRIDE', 'MULTIPLY', 'ADD']
    
    # Texture space location
    texspace_location: Tuple[float, float, float]
    
    # Texture space size
    texspace_size: Tuple[float, float, float]
    
    # The type of tilt calculation for 3D Curves
    twist_mode: str = 'MINIMUM' # ['Z_UP', 'MINIMUM', 'TANGENT']
    
    # Smoothing iteration for tangents
    twist_smooth: float
    
    # Adjust active object’s texture space automatically when transforming object
    use_auto_texspace: bool
    
    # Option for curve-deform: Use the mesh bounds to clamp the deformation
    use_deform_bounds: bool
    
    # Fill caps for beveled curves
    use_fill_caps: bool
    
    # Fill curve after applying shape keys and all modifiers
    use_fill_deform: bool
    
    # Map effect of the taper object to the beveled part of the curve
    use_map_taper: bool
    
    # Enable the curve to become a translation path
    use_path: bool
    
    # Clamp the curve path children so they can’t travel past the start/end point of the curve
    use_path_clamp: bool
    
    # Make curve path children to rotate along the path
    use_path_follow: bool
    
    # Option for paths and curve-deform: apply the curve radius with path following it and deforming
    use_radius: bool
    
    # Option for curve-deform: make deformed child to stretch along entire path
    use_stretch: bool

    def transform(self, *args, **kwargs) -> None:
        ...

    def validate_material_indices(self, *args, **kwargs) -> None:
        ...

    def update_gpu_tag(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...