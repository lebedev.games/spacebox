from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class DynamicPaintSurface(bpy_struct):
    
    # Only use brush objects from this collection
    brush_collection: Collection
    
    # Adjust influence brush objects have on this surface
    brush_influence_scale: float
    
    # Adjust radius of proximity brushes or particles for this surface
    brush_radius_scale: float
    
    # The wetness level when colors start to shift to the background
    color_dry_threshold: float
    
    # How fast colors get mixed within wet paint
    color_spread_speed: float
    
    # Maximum level of depth intersection in object space (use 0.0 to disable)
    depth_clamp: float
    
    # Strength of displace when applied to the mesh
    displace_factor: float
    
    displace_type: str = 'DISPLACE' # ['DISPLACE', 'DEPTH']
    
    # Approximately in how many frames should dissolve happen
    dissolve_speed: int
    
    # How much surface acceleration affects dripping
    drip_acceleration: float
    
    # How much surface velocity affects dripping
    drip_velocity: float
    
    # Approximately in how many frames should drying happen
    dry_speed: int
    
    effect_ui: str = 'SPREAD' # ['SPREAD', 'DRIP', 'SHRINK']
    
    effector_weights: EffectorWeights
    
    # Simulation end frame
    frame_end: int
    
    # Simulation start frame
    frame_start: int
    
    # Do extra frames between scene frames to ensure smooth motion
    frame_substeps: int
    
    image_fileformat: str = 'PNG' # ['PNG', 'OPENEXR']
    
    # Directory to save the textures
    image_output_path: str
    
    # Output image resolution
    image_resolution: int
    
    # Initial color of the surface
    init_color: Tuple[float, float, float, float]
    
    init_color_type: str = 'NONE' # ['NONE', 'COLOR', 'TEXTURE', 'VERTEX_COLOR']
    
    init_layername: str
    
    init_texture: Texture
    
    # Toggle whether surface is processed or ignored
    is_active: bool
    
    is_cache_user: bool
    
    # Surface name
    name: str
    
    # Name used to save output from this surface
    output_name_a: str
    
    # Name used to save output from this surface
    output_name_b: str
    
    point_cache: PointCache
    
    # How fast shrink effect moves on the canvas surface
    shrink_speed: float
    
    # How fast spread effect moves on the canvas surface
    spread_speed: float
    
    # Surface Format
    surface_format: str = 'VERTEX' # ['VERTEX', 'IMAGE']
    
    # Surface Type
    surface_type: str = 'PAINT' # ['PAINT']
    
    # Use 5x multisampling to smooth paint edges
    use_antialiasing: bool
    
    # Enable to make surface changes disappear over time
    use_dissolve: bool
    
    # Use logarithmic dissolve (makes high values to fade faster than low values)
    use_dissolve_log: bool
    
    # Process drip effect (drip wet paint to gravity direction)
    use_drip: bool
    
    # Use logarithmic drying (makes high values to dry faster than low values)
    use_dry_log: bool
    
    # Enable to make surface wetness dry over time
    use_drying: bool
    
    # New displace is added cumulatively on top of existing
    use_incremental_displace: bool
    
    # Save this output layer
    use_output_a: bool
    
    # Save this output layer
    use_output_b: bool
    
    # Multiply color by alpha (recommended for Blender input)
    use_premultiply: bool
    
    # Process shrink effect (shrink paint areas)
    use_shrink: bool
    
    # Process spread effect (spread wet paint around surface)
    use_spread: bool
    
    # Pass waves through mesh edges
    use_wave_open_border: bool
    
    # UV map name
    uv_layer: str
    
    # Wave damping factor
    wave_damping: float
    
    # Limit maximum steepness of wave slope between simulation points (use higher values for smoother waves at expense of reduced detail)
    wave_smoothness: float
    
    # Wave propagation speed
    wave_speed: float
    
    # Spring force that pulls water level back to zero
    wave_spring: float
    
    # Wave time scaling factor
    wave_timescale: float

    def output_exists(self, *args, **kwargs) -> None:
        ...

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...