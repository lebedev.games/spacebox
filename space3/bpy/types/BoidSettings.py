from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class BoidSettings(bpy_struct):
    
    # Accuracy of attack
    accuracy: float
    
    active_boid_state: BoidRule
    
    active_boid_state_index: int
    
    # Boid will fight this times stronger enemy
    aggression: float
    
    # Maximum acceleration in air (relative to maximum speed)
    air_acc_max: float
    
    # Maximum angular velocity in air (relative to 180 degrees)
    air_ave_max: float
    
    # Radius of boids personal space in air (% of particle size)
    air_personal_space: float
    
    # Maximum speed in air
    air_speed_max: float
    
    # Minimum speed in air (relative to maximum speed)
    air_speed_min: float
    
    # Amount of rotation around velocity vector on turns
    bank: float
    
    # Initial boid health when born
    health: float
    
    # Boid height relative to particle size
    height: float
    
    # Maximum acceleration on land (relative to maximum speed)
    land_acc_max: float
    
    # Maximum angular velocity on land (relative to 180 degrees)
    land_ave_max: float
    
    # Maximum speed for jumping
    land_jump_speed: float
    
    # Radius of boids personal space on land (% of particle size)
    land_personal_space: float
    
    # How smoothly the boids land
    land_smooth: float
    
    # Maximum speed on land
    land_speed_max: float
    
    # How strong a force must be to start effecting a boid on land
    land_stick_force: float
    
    # Amount of rotation around side vector
    pitch: float
    
    # Maximum distance from which a boid can attack
    range: float
    
    states: List[BoidState]
    
    # Maximum caused damage on attack per second
    strength: float
    
    # Allow boids to climb goal objects
    use_climb: bool
    
    # Allow boids to move in air
    use_flight: bool
    
    # Allow boids to move on land
    use_land: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...