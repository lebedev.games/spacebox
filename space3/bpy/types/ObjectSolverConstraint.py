from typing import Any
from typing import List
from typing import Set
from typing import Tuple

from . import *


class ObjectSolverConstraint(Constraint):
    
    # Camera to which motion is parented (if empty active scene camera is used)
    camera: Object
    
    # Movie Clip to get tracking data from
    clip: MovieClip
    
    # Movie tracking object to follow
    object: str
    
    # Set to true to request recalculation of the inverse matrix
    set_inverse_pending: bool
    
    # Use active clip defined in scene
    use_active_clip: bool

    @classmethod
    def bl_rna_get_subclass(cls, *args, **kwargs) -> Struct:
        ...

    @classmethod
    def bl_rna_get_subclass_py(cls, *args, **kwargs) -> Any:
        ...