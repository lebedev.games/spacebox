use serde::{Deserialize, Serialize};
use universe::{
    BedId, BridgeId, CharacterId, ColonyId, ConstructionId, CropId, DeckId, GroupId, PlanetId,
    PlanterId, PropId, SpaceshipId, StorageId, SystemId, WorkbenchId,
};

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(tag = "$type")]
pub enum Disappearance {
    ColonyVanished { id: ColonyId },
    SystemVanished { id: SystemId },
    DeckVanished { id: DeckId },
    SpaceshipVanished { id: SpaceshipId },
    PlanetVanished { id: PlanetId },
    GroupVanished2 { id: GroupId },
    CharacterVanished { id: CharacterId },
    CropVanished { id: CropId },
    StorageVanished { id: StorageId },
    BridgeVanished { id: BridgeId },
    WorkbenchVanished { id: WorkbenchId },
    PropVanished { id: PropId },
    PlanterVanished { id: PlanterId },
    BedVanished { id: BedId },
    ConstructionVanished { id: ConstructionId },
}
