use serde::{Deserialize, Serialize};
use universe::CharacterId;

use crate::game::Action;
use crate::game::ActionError;

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(tag = "$type")]
pub enum Game {
    ActionFailed {
        character: CharacterId,
        action: Action,
        error: ActionError,
    },
    GameLoaded,
    GameStatusChanged {
        started: bool,
    },
    PlayerListUpdated {
        players: Vec<String>,
        origins: Vec<String>,
    },
}
