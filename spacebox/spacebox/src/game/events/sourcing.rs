use std::collections::HashSet;

pub use farming::Farming;
pub use inventory::Inventory;
pub use navigation::Navigation;
pub use physics::{Physics, SpaceId};
use piloting::Piloting;
use serde::{Deserialize, Serialize};
use universe::{CharacterId, GroupId, Universe};

use crate::game::Action;
use crate::game::ActionError;
use crate::game::{Appearance, Disappearance, Game};

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(tag = "$domain")]
pub enum Event {
    Game(Game),
    Universe(Universe),
    Physics(Physics),
    Piloting(Piloting),
    Farming(Farming),
    Inventory(Inventory),
    Navigation(Navigation),
    Appearance(Appearance),
    Disappearance(Disappearance),
}

#[derive(Debug, Clone)]
pub struct Record {
    pub source: Source,
    pub events: Vec<Event>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Source {
    pub space: Option<SpaceId>,
    pub groups: Option<HashSet<GroupId>>,
}

impl Source {
    pub fn new() -> Self {
        Source {
            space: None,
            groups: None,
        }
    }

    pub fn only(group: GroupId) -> Self {
        let mut source = Self::new();
        source.groups = Some([group].iter().cloned().collect());
        source
    }

    pub fn of(groups: HashSet<GroupId>) -> Self {
        let mut source = Self::new();
        source.groups = Some(groups);
        source
    }

    pub fn at(space: SpaceId) -> Self {
        let mut source = Self::new();
        source.space = Some(space);
        source
    }
}

/// Subjects are Pull collections of game events.
///
/// What is Pull? In Pull systems, the Consumer determines when it receives data
/// from the data Producer. The Producer itself is unaware of when the data will
/// be delivered to the Consumer.
pub struct Subject {
    pub source: Source,
    pub records: Vec<Record>,
}

impl Subject {
    pub fn events_count(&self) -> usize {
        self.records.iter().map(|record| record.events.len()).sum()
    }

    pub fn records_count(&self) -> usize {
        self.records.len()
    }

    pub fn at(space: SpaceId) -> Self {
        Subject {
            source: Source::at(space),
            records: vec![],
        }
    }

    pub fn only(group: GroupId) -> Self {
        Subject {
            source: Source::only(group),
            records: vec![],
        }
    }

    pub fn of(groups: HashSet<GroupId>) -> Self {
        Subject {
            source: Source::of(groups),
            records: vec![],
        }
    }

    pub fn new() -> Self {
        Subject {
            source: Source::new(),
            records: vec![],
        }
    }

    pub fn nothing() -> Self {
        Subject {
            source: Source::new(),
            records: vec![],
        }
    }

    pub fn occur<T>(mut self, events: Vec<T>) -> Self
    where
        T: Into<Event>,
    {
        self.publish(events, self.source.clone());
        self
    }

    pub fn push<T>(&mut self, event: T)
    where
        T: Into<Event>,
    {
        self.publish(vec![event.into()], self.source.clone());
    }

    pub fn push_at<T>(&mut self, event: T, source: Source)
    where
        T: Into<Event>,
    {
        self.publish(vec![event.into()], source);
    }

    pub fn join(&mut self, other: Subject) {
        for record in other.records {
            self.records.push(record);
        }
    }

    pub fn publish<T>(&mut self, events: Vec<T>, source: Source)
    where
        T: Into<Event>,
    {
        self.records.push(Record {
            source,
            events: events.into_iter().map(|event| event.into()).collect(),
        });
    }
}

impl From<Physics> for Event {
    fn from(event: Physics) -> Self {
        Event::Physics(event)
    }
}

impl From<Universe> for Event {
    fn from(event: Universe) -> Self {
        Event::Universe(event)
    }
}

impl From<Piloting> for Event {
    fn from(event: Piloting) -> Self {
        Event::Piloting(event)
    }
}

impl From<Game> for Event {
    fn from(event: Game) -> Self {
        Event::Game(event)
    }
}

impl From<Farming> for Event {
    fn from(event: Farming) -> Self {
        Event::Farming(event)
    }
}

impl From<Inventory> for Event {
    fn from(event: Inventory) -> Self {
        Event::Inventory(event)
    }
}

impl From<Navigation> for Event {
    fn from(event: Navigation) -> Self {
        Event::Navigation(event)
    }
}

impl From<Appearance> for Event {
    fn from(event: Appearance) -> Self {
        Event::Appearance(event)
    }
}

impl From<Disappearance> for Event {
    fn from(event: Disappearance) -> Self {
        Event::Disappearance(event)
    }
}
