use std::collections::{HashMap, HashSet};

use farming::Plant;
use physics::{Barrier, BarrierOrigin, Body, BodyOrigin, Space, SpaceId, SpaceOrigin};
use universe::{
    Bed, Bridge, Character, Colonization, Construction, Crop, GroupId, Planet, Planter, Prop,
    Spaceship, Storage, Workbench,
};

use crate::game::ActionError;
use crate::game::Appearance::{
    BedAppeared, BridgeAppeared, CharacterAppeared, ColonyAppeared, ConstructionAppeared,
    CropAppeared, DeckAppeared, GroupAppeared, PlanetAppeared, PlanterAppeared, PropAppeared,
    SpaceshipAppeared, StorageAppeared, SystemAppeared, WorkbenchAppeared,
};
use crate::game::Disappearance::{
    BedVanished, BridgeVanished, CharacterVanished, ColonyVanished, ConstructionVanished,
    CropVanished, DeckVanished, GroupVanished2, PlanetVanished, PlanterVanished, PropVanished,
    SpaceshipVanished, StorageVanished, SystemVanished, WorkbenchVanished,
};
use crate::game::Master;
use crate::game::{Appearance, Disappearance, Event};

#[derive(Default)]
pub struct Counter {
    pub mapping: HashMap<SpaceId, HashMap<GroupId, usize>>,
    pub mapping_inverted: HashMap<GroupId, HashMap<SpaceId, usize>>,
}

impl Counter {
    pub fn new() -> Self {
        Counter {
            mapping: Default::default(),
            mapping_inverted: Default::default(),
        }
    }

    pub fn get_space_groups(&self, space: SpaceId) -> HashSet<GroupId> {
        match self.mapping.get(&space) {
            Some(groups) => groups.keys().cloned().collect(),
            None => HashSet::new(),
        }
    }

    pub fn get_group_spaces(&self, group: GroupId) -> HashSet<SpaceId> {
        match self.mapping_inverted.get(&group) {
            Some(spaces) => spaces.keys().cloned().collect(),
            None => HashSet::new(),
        }
    }

    pub fn increment(&mut self, space: SpaceId, group: GroupId) {
        match self.mapping.get_mut(&space) {
            Some(mapping) => match mapping.get(&group).cloned() {
                Some(counter) => {
                    mapping.insert(group, counter + 1);
                }
                None => {
                    mapping.insert(group, 1);
                }
            },
            None => {
                self.mapping
                    .insert(space, [(group, 1)].iter().cloned().collect());
            }
        };

        match self.mapping_inverted.get_mut(&group) {
            Some(mapping_inverted) => match mapping_inverted.get(&space).cloned() {
                Some(counter) => {
                    mapping_inverted.insert(space, counter + 1);
                }
                None => {
                    mapping_inverted.insert(space, 1);
                }
            },
            None => {
                self.mapping_inverted
                    .insert(group, [(space, 1)].iter().cloned().collect());
            }
        }
    }

    pub fn decrement(&mut self, space: SpaceId, group: GroupId) {
        match self.mapping.get_mut(&space) {
            Some(mapping) => match mapping.get(&group).cloned() {
                Some(counter) => {
                    if counter > 1 {
                        mapping.insert(group, counter - 1);
                    } else {
                        mapping.remove(&group);
                    }

                    if mapping.is_empty() {
                        self.mapping.remove(&space);
                    }
                }
                None => {
                    error!(
                        "Unable to decrement counter of {:?} in {:?}, group not found",
                        group, space
                    )
                }
            },
            None => {
                error!(
                    "Unable to decrement counter of {:?} in {:?}, space not found",
                    group, space
                )
            }
        }

        match self.mapping_inverted.get_mut(&group) {
            Some(mapping_inverted) => {
                match mapping_inverted.get(&space).cloned() {
                    Some(counter) => {
                        if counter > 1 {
                            mapping_inverted.insert(space, counter - 1);
                        } else {
                            mapping_inverted.remove(&space);
                        }
                    }
                    None => {
                        error!("Unable to decrement counter of {:?} in {:?}, (inverted) space not found", group, space)
                    }
                }
            }
            None => error!(
                "Unable to decrement counter of {:?} in {:?}, (inverted) group not found",
                group, space
            ),
        }
    }
}

/// Represents reference counting of observers of player groups.
/// The main idea is that groups are reclaimed as soon as they can no longer
/// be referenced, and in an incremental fashion, without hard methods
/// for space groups calculation in place.
#[derive(Default)]
pub struct Observing {
    pub characters: Counter,
    pub links: HashMap<SpaceId, HashSet<SpaceId>>,
}

impl Observing {
    pub fn get_links(&self, space: SpaceId) -> HashSet<SpaceId> {
        match self.links.get(&space) {
            None => HashSet::new(),
            Some(spaces) => spaces.clone(),
        }
    }

    pub fn append_link(&mut self, space: SpaceId, other: SpaceId) {
        match self.links.get_mut(&space) {
            None => {
                self.links.insert(space, [other].iter().cloned().collect());
            }
            Some(spaces) => {
                spaces.insert(other);
                let groups = self.get_space_groups(space);
                for group in groups {
                    self.characters.increment(other, group);
                }
            }
        };
    }

    pub fn remove_link(&mut self, space: SpaceId, other: SpaceId) {
        match self.links.get_mut(&space) {
            None => {}
            Some(spaces) => {
                spaces.remove(&other);
                let groups = self.get_space_groups(space);
                for group in groups {
                    self.characters.decrement(other, group);
                }
            }
        };
    }

    pub fn append_observer(&mut self, space: SpaceId, group: GroupId) {
        self.characters.increment(space, group);

        if let Some(spaces) = self.links.get(&space) {
            for other in spaces {
                self.characters.increment(*other, group);
            }
        }
    }

    pub fn remove_observer(&mut self, space: SpaceId, group: GroupId) {
        self.characters.decrement(space, group);

        if let Some(spaces) = self.links.get(&space) {
            for space in spaces {
                self.characters.decrement(*space, group);
            }
        }
    }

    pub fn get_space_groups(&self, space: SpaceId) -> HashSet<GroupId> {
        self.characters.get_space_groups(space)
    }

    pub fn get_group_spaces(&self, group: GroupId) -> HashSet<SpaceId> {
        self.characters.get_group_spaces(group)
    }
}

impl Observing {
    pub fn new() -> Self {
        Observing {
            characters: Counter::new(),
            links: Default::default(),
        }
    }
}

impl Master {
    pub fn observe_character(character: &Character, body: &Body) -> Vec<Appearance> {
        vec![CharacterAppeared {
            id: character.id,
            key: character.key.clone(),
            body: character.body,
            group: character.group,
            space: character.space(body),
            container: character.container,
            task: character.task.clone(),
            position: body.position.into(),
            health: character.health,
            health_limit: character.health_limit,
            energy: character.energy,
            energy_limit: character.energy_limit,
            stress: character.stress,
            stress_limit: character.stress_limit,
        }]
    }

    pub fn observe_spaceship(spaceship: &Spaceship, body: &Body) -> Vec<Appearance> {
        vec![SpaceshipAppeared {
            id: spaceship.id,
            key: spaceship.key.clone(),
            body: spaceship.body,
            spacecraft: spaceship.spacecraft,
            space: spaceship.space(body),
            position: body.position.into(),
        }]
    }

    pub fn observe_planet(
        planet: &Planet,
        body: &Body,
        colonizations: Vec<&Colonization>,
    ) -> Vec<Appearance> {
        vec![PlanetAppeared {
            id: planet.id,
            key: planet.key.clone(),
            body: planet.body,
            space: planet.space(body),
            position: body.position.into(),
            colonies: colonizations
                .iter()
                .map(|colonization| colonization.colony)
                .collect(),
        }]
    }

    pub fn observe_construction(construction: &Construction, barrier: &Barrier) -> Vec<Appearance> {
        vec![ConstructionAppeared {
            id: construction.id,
            space: construction.space(barrier),
            key: construction.key.clone(),
            progress: construction.progress,
            project: construction.project.clone(),
            barrier: construction.barrier,
            position: barrier.position.into(),
            rotation: barrier.rotation,
        }]
    }

    pub fn observe_prop(prop: &Prop, barrier: &Barrier) -> Vec<Appearance> {
        vec![PropAppeared {
            id: prop.id,
            key: prop.key.clone(),
            barrier: prop.barrier,
            obstacle: prop.obstacle,
            space: prop.space(barrier),
            position: barrier.position.into(),
            rotation: barrier.rotation,
        }]
    }

    pub fn observe_crop(crop: &Crop, barrier: &Barrier, plant: &Plant) -> Vec<Appearance> {
        vec![CropAppeared {
            id: crop.id,
            key: crop.key.clone(),
            plant: crop.plant,
            state: plant.state.clone(),
            barrier: crop.barrier,
            position: barrier.position.into(),
            space: barrier.space,
            place: plant.place,
            rotation: barrier.rotation,
        }]
    }

    pub fn observe_storage(storage: &Storage, barrier: &Barrier) -> Vec<Appearance> {
        vec![StorageAppeared {
            id: storage.id,
            key: storage.key.clone(),
            barrier: storage.barrier,
            container: storage.container,
            space: storage.space(barrier),
            position: barrier.position.into(),
            rotation: barrier.rotation,
            entry: storage.entry(barrier).into(),
        }]
    }

    pub fn observe_workbench(workbench: &Workbench, barrier: &Barrier) -> Vec<Appearance> {
        vec![WorkbenchAppeared {
            id: workbench.id,
            key: workbench.key.clone(),
            barrier: workbench.barrier,
            space: workbench.space(barrier),
            position: barrier.position.into(),
            rotation: barrier.rotation,
            entry: workbench.entry(barrier).into(),
        }]
    }

    pub fn observe_bridge(bridge: &Bridge, barrier: &Barrier) -> Vec<Appearance> {
        vec![BridgeAppeared {
            id: bridge.id,
            key: bridge.key.clone(),
            barrier: bridge.barrier,
            space: bridge.space(barrier),
            position: barrier.position.into(),
            rotation: barrier.rotation,
            entry: bridge.entry(barrier).into(),
            spaceship: bridge.spaceship,
        }]
    }

    pub fn observe_bed(bed: &Bed, barrier: &Barrier) -> Vec<Appearance> {
        vec![BedAppeared {
            id: bed.id,
            key: bed.key.clone(),
            barrier: bed.barrier,
            space: bed.space(barrier),
            position: barrier.position.into(),
            rotation: barrier.rotation,
            entry: bed.entry(barrier).into(),
            user: bed.user,
        }]
    }

    pub fn observe_planter(planter: &Planter, barrier: &Barrier) -> Vec<Appearance> {
        vec![PlanterAppeared {
            id: planter.id,
            key: planter.key.clone(),
            barrier: planter.barrier,
            space: planter.space(barrier),
            position: barrier.position.into(),
            rotation: barrier.rotation,
            entry: planter.entry(barrier).into(),
        }]
    }

    pub fn vanish_space(&self, space: SpaceId) -> Result<Vec<Disappearance>, ActionError> {
        let appearance = self.observe_space(space)?;
        Ok(self.vanish(&appearance))
    }

    pub fn observe_space(&self, space: SpaceId) -> Result<Vec<Appearance>, ActionError> {
        let universe = &self.universe;
        let physics = &self.physics;
        let farming = &self.farming;

        let space = self.physics.get_space(space)?;

        let mut events = Vec::new();

        match space.origin {
            SpaceOrigin::Deck => {
                let deck = universe.get_deck_by_space(space.id)?;
                events.push(DeckAppeared {
                    id: deck.id,
                    key: deck.key.clone(),
                    space: deck.space,
                    mesh: deck.mesh,
                    farmland: deck.farmland,
                    parent: space.parent,
                    spaceship: deck.spaceship,
                });
            }
            SpaceOrigin::Colony => {
                let colony = universe.get_colony_by_space(space.id)?;
                events.push(ColonyAppeared {
                    id: colony.id,
                    key: colony.key.clone(),
                    space: colony.space,
                    mesh: colony.mesh,
                    farmland: colony.farmland,
                });
            }
            SpaceOrigin::System => {
                let system = universe.get_system_by_space(space.id)?;
                events.push(SystemAppeared {
                    id: system.id,
                    key: system.key.clone(),
                    space: system.space,
                });
            }
        }

        for body in physics.get_space_bodies(space.id) {
            match body.origin {
                BodyOrigin::Character => {
                    let character = universe.get_character(body.id.0.into())?;
                    events.extend(Master::observe_character(character, body));
                }
                BodyOrigin::Spaceship => {
                    let spaceship = universe.get_spaceship(body.id.0.into())?;
                    events.extend(Master::observe_spaceship(spaceship, body));
                }
                BodyOrigin::Planet => {
                    let planet = universe.get_planet(body.id.0.into())?;
                    let colonizations = universe.get_planet_colonizations(planet.id);
                    events.extend(Master::observe_planet(planet, body, colonizations));
                }
            }
        }

        for barrier in physics.get_space_barriers(space.id) {
            let id = barrier.id.0;
            match barrier.origin {
                BarrierOrigin::Prop => {
                    let prop = universe.get_prop(id.into())?;
                    events.extend(Master::observe_prop(prop, barrier));
                }
                BarrierOrigin::Crop => {
                    let crop = universe.get_crop(id.into())?;
                    let plant = farming.get_plant(id.into())?;
                    events.extend(Master::observe_crop(crop, barrier, plant));
                }
                BarrierOrigin::Workbench => {
                    let workbench = universe.get_workbench(id.into())?;
                    events.extend(Master::observe_workbench(workbench, barrier));
                }
                BarrierOrigin::Storage => {
                    let storage = universe.get_storage(id)?;
                    events.extend(Master::observe_storage(storage, barrier));
                }
                BarrierOrigin::Construction => {
                    let construction = universe.get_construction(id.into())?;
                    events.extend(Master::observe_construction(construction, barrier));
                }
                BarrierOrigin::Bed => {
                    let bed = universe.get_bed(id.into())?;
                    events.extend(Master::observe_bed(bed, barrier));
                }
                BarrierOrigin::Planter => {
                    let planter = universe.get_planter(id.into())?;
                    events.extend(Master::observe_planter(planter, barrier));
                }
                BarrierOrigin::Bridge => {
                    let bridge = universe.get_bridge(id.into())?;
                    events.extend(Master::observe_bridge(bridge, barrier));
                }
            }
        }

        for child in physics.get_child_spaces(space.id) {
            events.extend(self.observe_space(child)?);
        }

        Ok(events)
    }

    pub fn vanish(&self, appearance: &Vec<Appearance>) -> Vec<Disappearance> {
        let events = appearance.iter().map(|event| match event.clone() {
            ColonyAppeared { id, .. } => ColonyVanished { id },
            SystemAppeared { id, .. } => SystemVanished { id },
            DeckAppeared { id, .. } => DeckVanished { id },
            PlanetAppeared { id, .. } => PlanetVanished { id },
            SpaceshipAppeared { id, .. } => SpaceshipVanished { id },
            GroupAppeared { id, .. } => GroupVanished2 { id },
            CharacterAppeared { id, .. } => CharacterVanished { id },
            CropAppeared { id, .. } => CropVanished { id },
            StorageAppeared { id, .. } => StorageVanished { id },
            BridgeAppeared { id, .. } => BridgeVanished { id },
            WorkbenchAppeared { id, .. } => WorkbenchVanished { id },
            PropAppeared { id, .. } => PropVanished { id },
            PlanterAppeared { id, .. } => PlanterVanished { id },
            BedAppeared { id, .. } => BedVanished { id },
            ConstructionAppeared { id, .. } => ConstructionVanished { id },
        });

        events.collect()
    }

    pub fn inspect_game(&self, group: GroupId) -> Result<Vec<Event>, ActionError> {
        let universe = &self.universe;
        let physics = &self.physics;
        let inventory = &self.inventory;

        let spaces = self.observing.get_group_spaces(group);
        let mut sorted_spaces = Vec::with_capacity(spaces.len());
        for space in spaces {
            sorted_spaces.push(physics.get_space(space)?);
        }
        sorted_spaces.sort_by(|a, b| {
            let a = get_inspection_order(a);
            let b = get_inspection_order(b);
            a.cmp(&b)
        });

        let mut events = vec![];

        for group in universe.state.groups.iter() {
            events.push(
                GroupAppeared {
                    id: group.id,
                    name: group.name.clone(),
                }
                .into(),
            );
        }

        for space in sorted_spaces {
            events.extend(
                self.observe_space(space.id)?
                    .into_iter()
                    .map(|event| event.into()),
            );
        }

        for character in &universe.state.characters {
            if character.group == group {
                let container = character.container;
                events.extend(
                    inventory
                        .open_container(container)?
                        .into_iter()
                        .map(|event| event.into()),
                );
            }
        }

        Ok(events)
    }
}

#[inline]
pub fn get_inspection_order(space: &Space) -> usize {
    match space.origin {
        SpaceOrigin::System => 1,
        SpaceOrigin::Colony => 2,
        SpaceOrigin::Deck => 3,
    }
}
