use farming::FarmingError;
use inventory::InventoryError;
use navigation::NavigationError;
use physics::PhysicsError;
use piloting::PilotingError;
use serde::{Deserialize, Serialize};
use universe::UniverseError;

use crate::game::PlayerId;
use crate::persistence::DataError;

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(tag = "$domain")]
pub enum ActionError {
    Universe(UniverseError),
    Piloting(PilotingError),
    Physics(PhysicsError),
    Navigation(NavigationError),
    Farming(FarmingError),
    Inventory(InventoryError),
    Obsolete(String),
}

#[derive(Debug)]
pub enum GameError {
    PlayerNotFound(PlayerId),
    Data(DataError),
    Navigation(NavigationError),
    ActionError(ActionError),
}

impl From<ActionError> for GameError {
    fn from(error: ActionError) -> Self {
        GameError::ActionError(error)
    }
}

impl From<NavigationError> for GameError {
    fn from(error: NavigationError) -> Self {
        GameError::Navigation(error)
    }
}

impl From<DataError> for GameError {
    fn from(error: DataError) -> Self {
        GameError::Data(error)
    }
}

impl From<InventoryError> for ActionError {
    fn from(error: InventoryError) -> Self {
        ActionError::Inventory(error)
    }
}

impl From<FarmingError> for ActionError {
    fn from(error: FarmingError) -> Self {
        ActionError::Farming(error)
    }
}

impl From<PilotingError> for ActionError {
    fn from(error: PilotingError) -> Self {
        ActionError::Piloting(error)
    }
}

impl From<UniverseError> for ActionError {
    fn from(error: UniverseError) -> Self {
        ActionError::Universe(error)
    }
}

impl From<PhysicsError> for ActionError {
    fn from(error: PhysicsError) -> Self {
        ActionError::Physics(error)
    }
}

impl From<NavigationError> for ActionError {
    fn from(error: NavigationError) -> Self {
        ActionError::Navigation(error)
    }
}

impl From<String> for ActionError {
    fn from(error: String) -> Self {
        ActionError::Obsolete(error)
    }
}

impl From<&str> for ActionError {
    fn from(error: &str) -> Self {
        ActionError::Obsolete(error.into())
    }
}
