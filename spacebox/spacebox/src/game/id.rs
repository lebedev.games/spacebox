pub struct IdGenerator {
    counter: usize,
}

impl IdGenerator {
    pub fn new(counter: usize) -> Self {
        IdGenerator { counter }
    }

    pub fn last(&self) -> usize {
        self.counter
    }

    pub fn generate_one(&mut self) -> usize {
        self.counter += 1;
        self.counter
    }

    pub fn generate_many<T>(&mut self, count: usize) -> Vec<T>
    where
        T: From<usize>,
    {
        let mut values = Vec::new();
        for _ in 0..count {
            values.push(self.generate_one().into())
        }
        values
    }
}
