use crate::game::ActionError;
use crate::game::Subject;

pub enum Process {
    Progress(Subject),
    Finished(Subject),
    Terminated(ActionError),
}
