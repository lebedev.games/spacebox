use farming::handlers::{Harvesting, HarvestingProgress, HarvestingResult};
use universe::{CharacterId, CropId, Time};

use crate::game::ActionError;
use crate::game::Disappearance::CropVanished;
use crate::game::Event::Disappearance;
use crate::game::Subject;
use crate::game::{Master, Process};

impl Master {
    pub fn run_crop_harvesting(
        &mut self,
        character: CharacterId,
        crop: CropId,
        elapsed_time: Time,
    ) -> Result<Process, ActionError> {
        let universe = &self.universe;
        let farming = &self.farming;

        let crop = universe.get_crop(crop)?;
        let crop_id = crop.id;

        match farming.update_harvesting(crop.plant, elapsed_time.in_days())? {
            Harvesting::Progress(progress) => {
                let progress = continue_crop_harvesting(self, character, crop_id, progress)?;
                Ok(Process::Progress(progress))
            }
            Harvesting::Result(result) => {
                let result = complete_crop_harvesting(self, character, crop_id, result)?;
                Ok(Process::Finished(result))
            }
        }
    }
}

fn continue_crop_harvesting(
    master: &mut Master,
    character: CharacterId,
    _crop: CropId,
    progress: HarvestingProgress,
) -> Result<Subject, ActionError> {
    let universe = &mut master.universe;
    let farming = &mut master.farming;
    let physics = &master.physics;

    let character = universe.get_character(character)?;
    let body = physics.get_body(character.body)?;
    let space = character.space(body);

    let harvesting = Subject::at(space).occur(farming.continue_harvesting(progress));

    Ok(harvesting)
}

fn complete_crop_harvesting(
    master: &mut Master,
    character: CharacterId,
    crop: CropId,
    result: HarvestingResult,
) -> Result<Subject, ActionError> {
    let universe = &mut master.universe;
    let farming = &mut master.farming;
    let inventory = &mut master.inventory;
    let physics = &master.physics;

    let character = universe.get_character(character)?;
    let body = physics.get_body(character.body)?;
    let space = character.space(body);
    let crop = universe.get_crop(crop)?;
    let crop_type = universe.get_crop_type(&crop.key)?;

    let quantity = (crop_type.production * result.quality) as usize;
    let production = inventory.prepare_items(
        &crop_type.consumable,
        character.container,
        master.id.generate_many(quantity),
    )?;

    let dead_crop = if result.plant_is_dead {
        let dead_crop = universe.prepare_delete_crop_operation(crop.id)?;
        Some(dead_crop)
    } else {
        None
    };

    let mut harvesting = Subject::at(space)
        .occur(farming.complete_harvesting(result))
        .occur(inventory.create_items(production));

    if let Some(dead_crop) = dead_crop {
        harvesting.push(Disappearance(CropVanished { id: dead_crop.crop }));
        universe.delete_crop(dead_crop);
    }

    Ok(harvesting)
}
