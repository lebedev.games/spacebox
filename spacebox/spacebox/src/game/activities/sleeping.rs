use universe::Universe::CharacterEnergyRestored;
use universe::{BedId, CharacterId, Time};

use crate::game::ActionError;
use crate::game::Subject;
use crate::game::{Master, Process};

impl Master {
    pub fn begin_sleep(&mut self, character: CharacterId, bed: BedId) -> Result<Time, ActionError> {
        let universe = &mut self.universe;
        let _character = universe.get_character_mut(character)?;
        let _prop = universe.get_bed(bed)?;

        let time_to_go_bed = Time::from_minutes(1.0);

        Ok(time_to_go_bed)
    }

    pub fn run_sleep(
        &mut self,
        character: CharacterId,
        _bed: BedId,
        time: Time,
    ) -> Result<Process, ActionError> {
        let universe = &mut self.universe;
        let physics = &self.physics;

        let character = universe.get_character_mut(character)?;
        let body = physics.get_body(character.body)?;
        let space = character.space(body);

        // full energy restore by hour
        let amount = time.in_hours() * character.energy_limit;
        let amount = amount.min(character.energy_limit - character.energy);
        character.energy += amount;

        let sleeping = Subject::new().occur(vec![CharacterEnergyRestored {
            id: character.id,
            amount,
            energy: character.energy,
            space,
        }]);

        Ok(Process::Progress(sleeping))
    }

    pub fn end_sleep(&mut self, character: CharacterId, bed: BedId) -> Result<Time, ActionError> {
        let universe = &mut self.universe;
        let _character = universe.get_character_mut(character)?;
        let _prop = universe.get_bed(bed)?;

        let time_to_wake_up = Time::from_minutes(3.0);

        Ok(time_to_wake_up)
    }

    pub fn finish_sleep(
        &mut self,
        character: CharacterId,
        bed: BedId,
    ) -> Result<Subject, ActionError> {
        let universe = &mut self.universe;
        let physics = &self.physics;

        let character = universe.get_character(character)?;
        let body = physics.get_body(character.body)?;
        let space = character.space(body);

        let operation = universe.prepare_free_bed(bed)?;
        Ok(Subject::at(space).occur(universe.free_bed(operation)))
    }
}
