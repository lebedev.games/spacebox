use universe::{CharacterId, StorageId, Time};

use crate::game::ActionError;
use crate::game::Master;
use crate::game::Subject;

impl Master {
    pub fn end_storage_usage(
        &mut self,
        _character: CharacterId,
        _storage: StorageId,
    ) -> Result<Time, ActionError> {
        let time_to_out = Time::instant();
        Ok(time_to_out)
    }

    pub fn finish_storage_usage(
        &mut self,
        character: CharacterId,
        storage: StorageId,
    ) -> Result<Subject, ActionError> {
        let universe = &self.universe;
        let inventory = &mut self.inventory;

        let character = universe.get_character(character)?;
        let storage = universe.get_storage(storage)?;
        let group = character.group;
        let closing = inventory.close_container(storage.container)?;

        Ok(Subject::only(group).occur(closing))
    }
}
