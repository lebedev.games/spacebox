use farming::handlers::MakeLandInfertileOperation;
use farming::Place;
use inventory::Container;
use navigation::{CreateObstacleOperation, PathPoint};
use physics::commands::CreateBarrierOperation;
use physics::{is_rotated_horizontally, rotate_relative_point, BarrierOrigin, SpaceId, Vector};
use universe::Universe::{ConstructionComplete, ConstructionProgressed};
use universe::{
    Bed, BedKey, Bridge, BridgeKey, CharacterId, ConstructionId, ConstructionProject, Planter,
    PlanterKey, Prop, PropKey, Storage, StorageKey, Time, Workbench, WorkbenchKey,
};

use crate::game::ActionError;
use crate::game::Subject;
use crate::game::{Master, Process};

impl Master {
    pub fn run_building(
        &mut self,
        character: CharacterId,
        construction: ConstructionId,
        elapsed_time: Time,
    ) -> Result<Process, ActionError> {
        let universe = &self.universe;

        let construction = universe.get_construction(construction)?;
        let construction_type = universe.get_construction_type(&construction.key)?;
        let construction_id = construction.id;
        let elapsed_time = elapsed_time.in_days();

        if construction.progress + elapsed_time < construction_type.days_to_construct {
            let progress = continue_building(self, character, construction_id, elapsed_time)?;
            Ok(Process::Progress(progress))
        } else {
            let result = complete_building(self, character, construction_id)?;
            Ok(Process::Finished(result))
        }
    }

    pub fn create_prop(
        &self,
        id: usize,
        key: &PropKey,
        space: SpaceId,
        position: Vector,
        rotation: f32,
    ) -> Result<BuildOperation, ActionError> {
        let universe = &self.universe;
        let physics = &self.physics;
        let navigation = &self.navigation;
        let farming = &self.farming;

        let target_space = physics.get_space(space)?;
        let mesh = self.get_space_mesh(target_space)?;

        let prop = universe.get_prop_type(key)?;
        let barrier = physics.prepare_barrier(
            space,
            id.into(),
            &prop.barrier,
            BarrierOrigin::Prop,
            position.into(),
            rotation,
        )?;
        let obstacle = navigation.prepare_create_obstacle_operation(
            mesh,
            &prop.obstacle,
            id.into(),
            point(position),
            is_rotated_horizontally(rotation),
        )?;
        let farmland = farming.prepare_make_land_infertile_operation(
            space.0.into(),
            place(position),
            barrier.barrier.size.x as usize,
            barrier.barrier.size.y as usize,
        )?;
        let prop = Prop {
            id: id.into(),
            key: key.clone(),
            barrier: id.into(),
            obstacle: id.into(),
        };
        Ok(BuildOperation::Prop {
            prop,
            obstacle,
            barrier,
            farmland,
        })
    }

    pub fn create_planter(
        &self,
        id: usize,
        key: &PlanterKey,
        space: SpaceId,
        position: Vector,
        rotation: f32,
    ) -> Result<BuildOperation, ActionError> {
        let universe = &self.universe;
        let physics = &self.physics;
        let navigation = &self.navigation;
        let farming = &self.farming;

        let target_space = physics.get_space(space)?;
        let mesh = self.get_space_mesh(target_space)?;

        let planter = universe.get_planter_type(key)?;
        let barrier = physics.prepare_barrier(
            space,
            id.into(),
            &planter.barrier,
            BarrierOrigin::Planter,
            position.into(),
            rotation,
        )?;
        let obstacle = navigation.prepare_create_obstacle_operation(
            mesh,
            &planter.obstacle,
            id.into(),
            point(position),
            is_rotated_horizontally(rotation),
        )?;
        let farmland = farming.prepare_make_land_infertile_operation(
            space.0.into(),
            place(position),
            barrier.barrier.size.x as usize,
            barrier.barrier.size.y as usize,
        )?;
        let relative_entry = rotate_relative_point(planter.relative_entry.into(), rotation).into();
        let planter = Planter {
            id: id.into(),
            key: key.clone(),
            barrier: id.into(),
            obstacle: id.into(),
            relative_entry,
        };
        Ok(BuildOperation::Planter {
            planter,
            barrier,
            obstacle,
            farmland,
        })
    }

    pub fn create_storage(
        &self,
        id: usize,
        key: &StorageKey,
        space: SpaceId,
        position: Vector,
        rotation: f32,
    ) -> Result<BuildOperation, ActionError> {
        let universe = &self.universe;
        let physics = &self.physics;
        let navigation = &self.navigation;
        let farming = &self.farming;

        let target_space = physics.get_space(space)?;
        let mesh = self.get_space_mesh(target_space)?;

        let storage = universe.get_storage_type(key)?;
        let barrier = physics.prepare_barrier(
            space,
            id.into(),
            &storage.barrier,
            BarrierOrigin::Storage,
            position.into(),
            rotation,
        )?;
        let obstacle = navigation.prepare_create_obstacle_operation(
            mesh,
            &storage.obstacle,
            id.into(),
            point(position),
            is_rotated_horizontally(rotation),
        )?;
        let container = Container {
            id: id.into(),
            key: storage.container.clone(),
            size: 100,
            slots: vec![],
        };
        let farmland = farming.prepare_make_land_infertile_operation(
            space.0.into(),
            place(position),
            barrier.barrier.size.x as usize,
            barrier.barrier.size.y as usize,
        )?;
        let relative_entry = rotate_relative_point(storage.relative_entry.into(), rotation).into();
        let storage = Storage {
            id: id.into(),
            key: key.clone(),
            barrier: id.into(),
            obstacle: id.into(),
            container: id.into(),
            relative_entry,
        };

        Ok(BuildOperation::Storage {
            storage,
            barrier,
            obstacle,
            container,
            farmland,
        })
    }

    pub fn create_bed(
        &self,
        id: usize,
        key: &BedKey,
        space: SpaceId,
        position: Vector,
        rotation: f32,
    ) -> Result<BuildOperation, ActionError> {
        let universe = &self.universe;
        let physics = &self.physics;
        let navigation = &self.navigation;
        let farming = &self.farming;

        let target_space = physics.get_space(space)?;
        let mesh = self.get_space_mesh(target_space)?;

        let bed = universe.get_bed_type(key)?;
        let barrier = physics.prepare_barrier(
            space,
            id.into(),
            &bed.barrier,
            BarrierOrigin::Bed,
            position.into(),
            rotation,
        )?;
        let obstacle = navigation.prepare_create_obstacle_operation(
            mesh,
            &bed.obstacle,
            id.into(),
            point(position),
            is_rotated_horizontally(rotation),
        )?;
        let farmland = farming.prepare_make_land_infertile_operation(
            space.0.into(),
            place(position),
            barrier.barrier.size.x as usize,
            barrier.barrier.size.y as usize,
        )?;
        let relative_entry = rotate_relative_point(bed.relative_entry.into(), rotation).into();
        let bed = Bed {
            id: id.into(),
            key: key.clone(),
            barrier: id.into(),
            obstacle: id.into(),
            relative_entry,
            user: None,
        };
        Ok(BuildOperation::Bed {
            bed,
            obstacle,
            barrier,
            farmland,
        })
    }

    pub fn create_workbench(
        &self,
        id: usize,
        key: &WorkbenchKey,
        space: SpaceId,
        position: Vector,
        rotation: f32,
    ) -> Result<BuildOperation, ActionError> {
        let universe = &self.universe;
        let physics = &self.physics;
        let navigation = &self.navigation;
        let farming = &self.farming;

        let target_space = physics.get_space(space)?;
        let mesh = self.get_space_mesh(target_space)?;

        let workbench = universe.get_workbench_type(key)?;
        let barrier = physics.prepare_barrier(
            space,
            id.into(),
            &workbench.barrier,
            BarrierOrigin::Workbench,
            position.into(),
            rotation,
        )?;
        let obstacle = navigation.prepare_create_obstacle_operation(
            mesh,
            &workbench.obstacle,
            id.into(),
            point(position),
            is_rotated_horizontally(rotation),
        )?;
        let farmland = farming.prepare_make_land_infertile_operation(
            space.0.into(),
            place(position),
            barrier.barrier.size.x as usize,
            barrier.barrier.size.y as usize,
        )?;
        let relative_entry =
            rotate_relative_point(workbench.relative_entry.into(), rotation).into();
        let workbench = Workbench {
            id: id.into(),
            key: key.clone(),
            barrier: id.into(),
            obstacle: id.into(),
            relative_entry,
        };
        Ok(BuildOperation::Workbench {
            workbench,
            barrier,
            obstacle,
            farmland,
        })
    }

    pub fn create_bridge(
        &self,
        id: usize,
        key: &BridgeKey,
        space: SpaceId,
        position: Vector,
        rotation: f32,
    ) -> Result<BuildOperation, ActionError> {
        let universe = &self.universe;
        let physics = &self.physics;
        let navigation = &self.navigation;
        let farming = &self.farming;

        let target_space = physics.get_space(space)?;
        let mesh = self.get_space_mesh(target_space)?;

        let bridge = universe.get_bridge_type(key)?;
        let barrier = physics.prepare_barrier(
            space,
            id.into(),
            &bridge.barrier,
            BarrierOrigin::Bridge,
            position.into(),
            rotation,
        )?;
        let obstacle = navigation.prepare_create_obstacle_operation(
            mesh,
            &bridge.obstacle,
            id.into(),
            point(position),
            is_rotated_horizontally(rotation),
        )?;
        let farmland = farming.prepare_make_land_infertile_operation(
            space.0.into(),
            place(position),
            barrier.barrier.size.x as usize,
            barrier.barrier.size.y as usize,
        )?;
        let spaceship = self.determine_spaceship_by_space(target_space);
        let relative_entry = rotate_relative_point(bridge.relative_entry.into(), rotation).into();
        let bridge = Bridge {
            id: id.into(),
            key: key.clone(),
            barrier: id.into(),
            obstacle: id.into(),
            relative_entry,
            spaceship,
        };
        Ok(BuildOperation::Bridge {
            bridge,
            barrier,
            obstacle,
            farmland,
        })
    }

    pub fn apply_construction_complete(&mut self, operation: BuildOperation) -> Subject {
        let universe = &mut self.universe;
        let physics = &mut self.physics;
        let navigation = &mut self.navigation;
        let inventory = &mut self.inventory;
        let farming = &mut self.farming;

        match operation {
            BuildOperation::Prop {
                prop,
                obstacle,
                barrier,
                farmland,
            } => {
                let space = barrier.barrier.space;
                let events = Master::observe_prop(&prop, &barrier.barrier);
                universe.state.props.push(prop);
                Subject::at(space)
                    .occur(physics.create_barrier(barrier))
                    .occur(navigation.create_obstacle(obstacle))
                    .occur(events)
                    .occur(farming.make_land_infertile(farmland))
            }
            BuildOperation::Storage {
                storage,
                obstacle,
                barrier,
                container,
                farmland,
            } => {
                let space = barrier.barrier.space;
                let events = Master::observe_storage(&storage, &barrier.barrier);
                inventory.state.containers.push(container);
                universe.state.storages.push(storage);
                Subject::at(space)
                    .occur(physics.create_barrier(barrier))
                    .occur(navigation.create_obstacle(obstacle))
                    .occur(events)
                    .occur(farming.make_land_infertile(farmland))
            }
            BuildOperation::Workbench {
                workbench,
                obstacle,
                barrier,
                farmland,
            } => {
                let space = barrier.barrier.space;
                let events = Master::observe_workbench(&workbench, &barrier.barrier);
                universe.state.workbenches.push(workbench);
                Subject::at(space)
                    .occur(physics.create_barrier(barrier))
                    .occur(navigation.create_obstacle(obstacle))
                    .occur(events)
                    .occur(farming.make_land_infertile(farmland))
            }
            BuildOperation::Bridge {
                bridge,
                obstacle,
                barrier,
                farmland,
            } => {
                let space = barrier.barrier.space;
                let events = Master::observe_bridge(&bridge, &barrier.barrier);
                universe.state.bridges.push(bridge);
                Subject::at(space)
                    .occur(physics.create_barrier(barrier))
                    .occur(navigation.create_obstacle(obstacle))
                    .occur(events)
                    .occur(farming.make_land_infertile(farmland))
            }
            BuildOperation::Bed {
                bed,
                obstacle,
                barrier,
                farmland,
            } => {
                let space = barrier.barrier.space;
                let events = Master::observe_bed(&bed, &barrier.barrier);
                universe.state.beds.push(bed);
                Subject::at(space)
                    .occur(physics.create_barrier(barrier))
                    .occur(navigation.create_obstacle(obstacle))
                    .occur(events)
                    .occur(farming.make_land_infertile(farmland))
            }
            BuildOperation::Planter {
                planter,
                obstacle,
                barrier,
                farmland,
            } => {
                let space = barrier.barrier.space;
                let events = Master::observe_planter(&planter, &barrier.barrier);
                universe.state.planters.push(planter);
                Subject::at(space)
                    .occur(physics.create_barrier(barrier))
                    .occur(navigation.create_obstacle(obstacle))
                    .occur(events)
                    .occur(farming.make_land_infertile(farmland))
            }
        }
    }
}

fn continue_building(
    master: &mut Master,
    character: CharacterId,
    construction: ConstructionId,
    elapsed_time: f32,
) -> Result<Subject, ActionError> {
    let physics = &master.physics;
    let universe = &mut master.universe;

    let character = universe.get_character(character)?;
    let body = physics.get_body(character.body)?;
    let space = character.space(body);
    let construction = universe.get_construction_mut(construction)?;
    construction.progress = construction.progress + elapsed_time;

    let building = Subject::at(space).occur(vec![ConstructionProgressed {
        id: construction.id,
        progress: construction.progress,
        amount: elapsed_time,
        space,
    }]);

    Ok(building)
}

pub enum BuildOperation {
    Prop {
        prop: Prop,
        obstacle: CreateObstacleOperation,
        barrier: CreateBarrierOperation,
        farmland: MakeLandInfertileOperation,
    },
    Bed {
        bed: Bed,
        obstacle: CreateObstacleOperation,
        barrier: CreateBarrierOperation,
        farmland: MakeLandInfertileOperation,
    },
    Storage {
        storage: Storage,
        container: Container,
        obstacle: CreateObstacleOperation,
        barrier: CreateBarrierOperation,
        farmland: MakeLandInfertileOperation,
    },
    Workbench {
        workbench: Workbench,
        obstacle: CreateObstacleOperation,
        barrier: CreateBarrierOperation,
        farmland: MakeLandInfertileOperation,
    },
    Bridge {
        bridge: Bridge,
        obstacle: CreateObstacleOperation,
        barrier: CreateBarrierOperation,
        farmland: MakeLandInfertileOperation,
    },
    Planter {
        planter: Planter,
        obstacle: CreateObstacleOperation,
        barrier: CreateBarrierOperation,
        farmland: MakeLandInfertileOperation,
    },
}

fn prepare_construction_complete(
    master: &Master,
    id: usize,
    project: ConstructionProject,
    space: SpaceId,
    position: Vector,
    rotation: f32,
) -> Result<BuildOperation, ActionError> {
    let physics = &master.physics;

    let space = physics.get_space(space)?;
    let position = position.into();

    match &project {
        ConstructionProject::PropProject { key } => {
            master.create_prop(id, key, space.id, position, rotation)
        }
        ConstructionProject::StorageProject { key } => {
            master.create_storage(id, key, space.id, position, rotation)
        }
        ConstructionProject::WorkbenchProject { key } => {
            master.create_workbench(id, key, space.id, position, rotation)
        }
        ConstructionProject::PlanterProject { key } => {
            master.create_planter(id, key, space.id, position, rotation)
        }
        ConstructionProject::BedProject { key } => {
            master.create_bed(id, key, space.id, position, rotation)
        }
        ConstructionProject::BridgeProject { key } => {
            master.create_bridge(id, key, space.id, position, rotation)
        }
    }
}

fn remove_construction(
    master: &mut Master,
    construction: ConstructionId,
    space: SpaceId,
) -> Result<Subject, ActionError> {
    let universe = &master.universe;
    let physics = &master.physics;

    let construction_index = universe.index_construction(construction)?;
    let construction = &universe.state.constructions[construction_index];
    let id = construction.id;
    let barrier = construction.barrier;
    let obstacle = construction.obstacle;
    let target_space = physics.get_space(space)?;
    let mesh = master.get_space_mesh(target_space)?;

    let universe = &mut master.universe;
    let physics = &mut master.physics;
    let navigation = &mut master.navigation;

    let barrier_index = physics.index_barrier(barrier)?;
    let obstacle_remove = navigation.prepare_obstacle_remove_operation(mesh, obstacle)?;

    physics.state.barriers.remove(barrier_index);
    navigation.remove_obstacle(obstacle_remove);
    universe.state.constructions.remove(construction_index);

    let removing = Subject::at(space).occur(vec![ConstructionComplete { id, space }]);

    Ok(removing)
}

fn complete_building(
    master: &mut Master,
    _character: CharacterId,
    construction: ConstructionId,
) -> Result<Subject, ActionError> {
    let id = master.id.generate_one();
    let universe = &master.universe;
    let physics = &master.physics;

    let construction = universe.get_construction(construction)?;
    let barrier = physics.get_barrier(construction.barrier)?;
    let space = construction.space(barrier);
    let project = construction.project.clone();
    let barrier = physics.get_barrier(construction.barrier)?;
    let position = barrier.position;
    let rotation = barrier.rotation;
    let construction_id = construction.id;

    let mut subject = Subject::at(space);

    subject.join(remove_construction(master, construction_id, space)?);
    let operation = prepare_construction_complete(master, id, project, space, position, rotation)?;
    subject.join(master.apply_construction_complete(operation));

    Ok(subject)
}

fn place(position: Vector) -> Place {
    Place {
        x: position.x as usize,
        y: position.y as usize,
    }
}

fn point(position: Vector) -> PathPoint {
    PathPoint {
        x: position.x,
        y: position.y,
    }
}
