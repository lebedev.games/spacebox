use serde::{Deserialize, Serialize};

use crate::game::Operation;
use crate::game::PlayerId;
use crate::game::{Action, Instruction, Interaction};
use universe::CharacterId;

#[derive(Debug, Clone)]
pub struct TrustedMessage(pub PlayerId, pub Message);

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(tag = "$type")]
pub enum Message {
    Instruction(Instruction),
    Interaction(Interaction),
    Action {
        character: CharacterId,
        action: Action,
    },
    Operation(Operation),
}

pub enum Response {
    Events,
}
