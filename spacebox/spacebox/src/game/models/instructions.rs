use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(tag = "$type")]
pub enum Instruction {
    Load,
    Save,
    Pause,
    Play { speed: f32 },
}
