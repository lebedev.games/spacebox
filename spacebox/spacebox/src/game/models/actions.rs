use inventory::ItemId;
use serde::{Deserialize, Serialize};
use universe::{
    BedId, BridgeId, ColonyId, ConstructionId, Coordinates, CropId, DeckId, PlanterId, StorageId,
    WorkbenchId,
};

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(tag = "$type")]
pub enum Action {
    MoveCharacter {
        position: Coordinates,
    },
    ConsumeItem {
        consumable: ItemId,
    },
    DesignBuilding {
        recipe: ItemId,
        position: Coordinates,
        rotation: f32,
    },
    Build {
        construction: ConstructionId,
    },
    UseWorkbench {
        workbench: WorkbenchId,
    },
    UseStorage {
        storage: StorageId,
    },
    UsePlanter {
        planter: PlanterId,
    },
    UseBridge {
        bridge: BridgeId,
    },
    UseBed {
        bed: BedId,
    },
    CancelActivity,
    BuildSpaceship {
        position: Coordinates,
    },
    MoveSpaceship {
        position: Coordinates,
    },
    LaunchSpaceship,
    LandSpaceship {
        colony: ColonyId,
        position: Coordinates,
    },
    UndockSpaceship,
    DockSpaceship {
        deck: DeckId,
        position: Coordinates,
    },
    PlantCrop {
        seed: ItemId,
        position: Coordinates,
        rotation: f32,
    },
    HarvestCrop {
        crop: CropId,
    },
    TakeItem {
        item: ItemId,
    },
    StoreItem {
        item: ItemId,
    },
}
