use std::sync::{Arc, RwLock};

use serde::{Deserialize, Serialize};
use universe::GroupId;

use crate::game::{Action, Master};
use crate::game::{Event, TrustedMessage};

#[derive(Debug, Clone)]
pub enum PlayerStatus {
    Active,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq, Hash)]
pub struct PlayerId(pub String);

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq)]
pub enum PlayerRole {
    Player,
    Host,
    Editor,
}

pub enum Response {
    Events(Vec<Event>),
    Notification,
}

#[derive(Debug, Clone)]
pub struct Player {
    pub id: PlayerId,
    pub group: Option<GroupId>,
    pub origin: String,
    pub status: PlayerStatus,
    pub events: Vec<Event>,
    pub observer: bool,
    pub backend: usize,
    pub role: PlayerRole,
}

impl Player {
    pub fn is_host(&self) -> bool {
        self.role == PlayerRole::Host || self.role == PlayerRole::Editor
    }

    pub fn is_editor(&self) -> bool {
        self.role == PlayerRole::Editor
    }
}

pub trait PlayerBackend {
    fn reload_game(&mut self);
    fn accept_players(&mut self) -> Vec<PlayerId>;
    fn reject_players(&mut self) -> Vec<PlayerId>;
    fn send_events(&mut self, player: &PlayerId, events: Vec<Event>);
    fn send_inside_data(&mut self, master: Arc<RwLock<Master>>);
    fn receive_players_messages(&mut self) -> Vec<TrustedMessage>;
}
