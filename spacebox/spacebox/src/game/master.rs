use std::fs;
use std::path::PathBuf;
use std::time::{Duration, Instant};

use farming::{Farming, FarmingDomain};
use inventory::InventoryDomain;
use navigation::{MeshId, NavigationDomain, PathPoint};
use physics::Physics::BodyPositionUpdated;
use physics::{BarrierOrigin, Physics, PhysicsDomain, Space, SpaceId, SpaceOrigin, Vector};
use piloting::PilotingDomain;
use serde::{Deserialize, Serialize};
use universe::Activity::{
    Building, BuildingDesign, CropHarvesting, Movement, Planting, Sleeping, SpaceshipCommand,
    StorageUsage,
};
use universe::Task::{Beginning, Ending, Idle, PendingBeginning, PendingEnding, Running};
use universe::Universe::{
    ActivityBegun, ActivityEnded, ActivityStarted, ActivityStopped, ActivityTerminated,
};
use universe::{
    Activity, Bed, Bridge, Character, CharacterId, CharacterKey, Crop, GroupId, Prop, Spaceship,
    SpaceshipId, Task, Time, Universe, UniverseDomain, UniverseError,
};

use crate::game::actions::consume_item;
use crate::game::id::IdGenerator;
use crate::game::observing::Observing;
use crate::game::Action::{
    Build, BuildSpaceship, CancelActivity, ConsumeItem, DesignBuilding, DockSpaceship, HarvestCrop,
    LandSpaceship, LaunchSpaceship, MoveCharacter, MoveSpaceship, PlantCrop, StoreItem, TakeItem,
    UndockSpaceship, UseBed, UseBridge, UsePlanter, UseStorage, UseWorkbench,
};
use crate::game::{
    Action, Database, Instruction, Interaction, Message, Operation, Process, TrustedMessage,
};
use crate::game::{ActionError, GameError};
use crate::game::{Game, Source, Subject};
use crate::game::{Player, PlayerId, PlayerRole, PlayerStatus};
use crate::persistence::DataError;

#[derive(Default, Debug, Clone, Serialize, Deserialize)]
pub struct Metrics {
    pub game_update_domain_updates_time: f32,
    pub game_update_events_publish_time: f32,
    pub game_update_events_count: usize,
    pub game_update_event_records_count: usize,
    pub messages_handles_time: f32,
    pub messages_events_publish_time: f32,
    pub messages_events_count: usize,
    pub messages_event_records_count: usize,
    pub messages_count: usize,
}

pub struct Rules {
    pub usage_distance: f32,
}

#[derive(Debug, Clone)]
pub enum GameStatus {
    PendingLoad { source: String },
    Running,
    Paused,
}

pub struct Master {
    pub database_path: PathBuf,

    pub shared_database: Database,
    pub universe: UniverseDomain,
    pub physics: PhysicsDomain,
    pub navigation: NavigationDomain,
    pub piloting: PilotingDomain,
    pub farming: FarmingDomain,
    pub inventory: InventoryDomain,

    pub rules: Rules,

    pub players: Vec<Player>,
    pub time: Time,
    pub status: GameStatus,
    pub id: IdGenerator,
    pub metrics: Metrics,
    pub observing: Observing,
}

impl Master {
    pub fn new(database_path: PathBuf) -> Self {
        Master {
            database_path,
            shared_database: Database::default(),
            universe: UniverseDomain::default(),
            physics: PhysicsDomain::default(),
            navigation: NavigationDomain::default(),
            piloting: PilotingDomain::default(),
            farming: FarmingDomain::default(),
            inventory: InventoryDomain::default(),
            rules: Rules {
                usage_distance: 2.0,
            },
            players: vec![],
            time: Time::default(),
            status: GameStatus::Paused,
            id: IdGenerator::new(1),
            metrics: Metrics::default(),
            observing: Observing::new(),
        }
    }

    pub fn get_space_beds(&self, space: SpaceId) -> Result<Vec<&Bed>, ActionError> {
        let mut beds = vec![];
        for barrier in self.physics.state.barriers.iter() {
            if barrier.space == space && barrier.origin == BarrierOrigin::Bed {
                beds.push(self.universe.get_bed(barrier.id.0.into())?);
            }
        }
        Ok(beds)
    }

    pub fn get_space_props(&self, space: SpaceId) -> Result<Vec<&Prop>, ActionError> {
        let mut props = vec![];
        for barrier in self.physics.state.barriers.iter() {
            if barrier.space == space && barrier.origin == BarrierOrigin::Prop {
                props.push(self.universe.get_prop(barrier.id.0.into())?);
            }
        }
        Ok(props)
    }

    pub fn get_space_crops(&self, space: SpaceId) -> Result<Vec<&Crop>, ActionError> {
        let mut crops = vec![];
        for barrier in self.physics.state.barriers.iter() {
            if barrier.space == space && barrier.origin == BarrierOrigin::Crop {
                crops.push(self.universe.get_crop(barrier.id.0.into())?);
            }
        }
        Ok(crops)
    }

    pub fn spawn_player_character(
        &mut self,
        key: CharacterKey,
        group: GroupId,
        space: SpaceId,
        position: Vector,
    ) -> Result<Subject, ActionError> {
        let id = self.id.generate_one();
        let universe = &self.universe;
        let physics = &self.physics;
        let inventory = &self.inventory;
        let navigation = &self.navigation;

        let space = physics.get_space(space)?;
        let mesh = self.get_space_mesh(space)?;
        let character = universe.get_character_type(&key)?;
        let container = inventory.prepare_container(&character.container, id.into())?;
        let navigator = navigation.prepare_navigator(
            &character.navigator,
            id.into(),
            mesh,
            PathPoint::new(position.x, position.y),
        )?;
        let body = physics.prepare_body(space.id, id.into(), &character.body, position)?;

        let character = Character {
            id: id.into(),
            key: key.clone(),
            group,
            body: id.into(),
            task: Task::Idle,
            sensor: id.into(),
            health: character.health_limit,
            health_limit: character.health_limit,
            energy: character.energy_limit,
            energy_limit: character.energy_limit,
            stress: 0.0,
            stress_limit: character.stress_limit,
            container: id.into(),
            navigator: id.into(),
        };

        let my_spaces = self.observing.get_group_spaces(group);

        let mut events = Vec::new();

        for parent in self.observing.get_links(space.id) {
            if !my_spaces.contains(&parent) {
                events.extend(self.observe_space(parent)?);
            }
        }

        if !my_spaces.contains(&space.id) {
            events.extend(self.observe_space(space.id)?);
        }

        events.extend(Master::observe_character(&character, &body.body));
        let observing = &mut self.observing;
        observing.append_observer(space.id, group);

        let space = space.id;

        let universe = &mut self.universe;
        let physics = &mut self.physics;
        let inventory = &mut self.inventory;
        let navigation = &mut self.navigation;

        universe.state.characters.push(character);
        physics.create_body(body);
        inventory.create_container(container);
        navigation.create_navigator(navigator);

        Ok(Subject::at(space).occur(events))
    }

    pub fn resume_game(&mut self) -> Result<(), GameError> {
        info!("Game resumed...");
        Ok(())
    }

    pub fn save_game(&self, destination: &str) -> Result<(), GameError> {
        info!("Save game '{}'", destination);

        let dir = self.database_path.join(destination);
        if dir.exists() {
            info!("Overwrite existing game save directory");
            fs::remove_dir_all(dir).map_err(DataError::Io)?;
        } else {
            info!("Create game save directory");
            fs::create_dir_all(dir).map_err(DataError::Io)?;
        }

        self.save_state(destination)?;
        self.save_types(destination)?;
        Ok(())
    }

    pub fn load_game(&mut self, source: &str) -> Result<(), GameError> {
        info!("Load game '{}'", source);

        self.shared_database = Database::default();
        self.universe = UniverseDomain::default();
        self.physics = PhysicsDomain::default();
        self.piloting = PilotingDomain::default();
        self.farming = FarmingDomain::default();
        self.inventory = InventoryDomain::default();
        self.navigation = NavigationDomain::default();
        self.update_database(source);

        // info!("Recalculate mesh {:?} on navigation state load", mesh.id);
        self.observing = self.reload_observing()?;

        Ok(())
    }

    fn reload_observing(&self) -> Result<Observing, ActionError> {
        let mut observing = Observing::new();
        for deck in self.universe.state.decks.iter() {
            let spaceship = self.universe.get_spaceship(deck.spaceship)?;
            if spaceship.landing.is_none() {
                let body = self.physics.get_body(spaceship.body)?;
                let system_space = body.space;
                observing.append_link(deck.space, system_space);
            }
        }
        for character in self.universe.state.characters.iter() {
            let body = self.physics.get_body(character.body)?;
            let space = self.physics.get_space(body.space)?;
            observing.append_observer(space.id, character.group);
        }
        Ok(observing)
    }

    pub fn handle_messages(&mut self, messages: Vec<TrustedMessage>) {
        let instant = Instant::now();
        self.metrics.messages_count = messages.len();
        let mut players_act = Subject::new();
        for trusted_message in messages {
            let (p, message) = (trusted_message.0, trusted_message.1);

            let player = match self
                .players
                .iter()
                .find(|player| &player.id == &p)
                .ok_or(GameError::PlayerNotFound(p.clone()))
            {
                Ok(player) => player,
                Err(_) => {
                    error!("Unable to handle player {:?} message, player not found", p);
                    continue;
                }
            };

            self.handle_message(&player.id.clone(), message.clone(), &mut players_act);
            /*
            if let Err(error) = self.handle_message(role, message.clone(), &mut players_act) {
                self.notify_player_game_error(player, message, error);
            }*/
        }
        self.metrics.messages_handles_time = instant.elapsed().as_secs_f32();
        self.metrics.messages_events_count = players_act.events_count();
        self.metrics.messages_event_records_count = players_act.records_count();
        let instant = Instant::now();
        self.publish(players_act);
        self.metrics.messages_events_publish_time = instant.elapsed().as_secs_f32();
    }

    pub fn handle_message(&mut self, player: &PlayerId, message: Message, stream: &mut Subject) {
        match message {
            Message::Instruction(instruction) => {
                // self.handle_instruction(player, instruction);
            }
            Message::Interaction(interaction) => {
                // self.handle_interaction(player, interaction);
            }
            Message::Action { character, action } => {
                match self.handle_action(character, action.clone()) {
                    Ok(events) => stream.join(events),
                    Err(error) => {
                        self.notify_player_action_error(player, character, action, error);
                    }
                }
            }
            Message::Operation(operation) => {
                // if !player.is_editor() {}
                // self.handle_operation(player, operation);
            }
        }
    }

    pub fn handle_instruction(&mut self, host: &Player, instruction: Instruction) {
        match instruction {
            Instruction::Load => {}
            Instruction::Save => {}
            Instruction::Pause => {}
            Instruction::Play { .. } => {}
        }
    }

    pub fn handle_interaction(&mut self, player: &Player, interaction: Interaction) {
        match interaction {
            Interaction::InspectGame => {} /*
                                           if let Some(group) = self.get_player_mut(player)?.group {
                                                   match self.inspect_game(group) {
                                                       Ok(events) => {
                                                           event_stream.publish(events, Source::only(group));
                                                       }
                                                       Err(error) => {
                                                           let player = self.get_player_mut(player)?;
                                                           error!(
                                                               "Unable to inspect game by player {:?}, {:?}",
                                                               player.id, error
                                                           );
                                                           let event = Game::MessageFailed {
                                                               error: format!("{:?}", error),
                                                           };
                                                           player.events.push(event.into());
                                                       }
                                                   }
                                               }
                                            */
        }
    }

    pub fn handle_operation(&mut self, editor: &Player, operation: Operation) {}

    fn notify_player_game_error(&mut self, id: PlayerId, message: Message, error: GameError) {
        unimplemented!()
        /*match self.players.iter_mut().find(|player| player.id == id) {
            Some(player) => {
                error!(
                    "Unable to handle player {:?} message {:?}, {:?}",
                    id, message, error
                );
                let event = Game::MessageFailed {
                    error: format!("{:?}", error),
                };
                player.events.push(event.into());
            }
            None => error!(
                "Unable to notify player {:?} game error, player not found",
                id
            ),
        }*/
    }

    fn notify_player_action_error(
        &mut self,
        id: &PlayerId,
        character: CharacterId,
        action: Action,
        error: ActionError,
    ) {
        match self.players.iter_mut().find(|player| &player.id == id) {
            Some(player) => {
                error!(
                    "Unable to handle player {:?} action {:?}, {:?}",
                    player.id, action, error
                );
                let event = Game::ActionFailed {
                    character,
                    action,
                    error,
                };
                player.events.push(event.into())
            }
            None => error!(
                "Unable to notify player {:?} action error, player not found",
                id
            ),
        }
    }

    pub fn accept_players(&mut self, players: Vec<PlayerId>, backend: usize) {
        for id in players {
            info!("Accept new player {:?} at backend {}", &id, backend);
            self.players.push(Player {
                id,
                group: None,
                origin: "".to_string(),
                status: PlayerStatus::Active,
                events: vec![],
                observer: false,
                backend,
                role: PlayerRole::Player,
            });
        }
    }

    fn get_player_mut(&mut self, id: &PlayerId) -> Result<&mut Player, GameError> {
        self.players
            .iter_mut()
            .find(|player| &player.id == id)
            .ok_or(GameError::PlayerNotFound(id.clone()))
    }

    pub fn publish(&mut self, subject: Subject) {
        for index in 0..self.players.len() {
            let group = self.players[index].group;
            let group = match group {
                Some(group) => group,
                None => {
                    continue;
                }
            };

            let observed_spaces = self.observing.get_group_spaces(group);
            let player = &mut self.players[index];

            for record in subject.records.iter() {
                let observable = if let Some(groups) = &record.source.groups {
                    // direct events
                    // no need to check presence spaces (or nested space's relations)
                    groups.contains(&group)
                } else if let Some(space) = &record.source.space {
                    observed_spaces.contains(&space)
                } else {
                    // is global events
                    true
                };
                if observable {
                    player.events.extend(record.events.clone());
                }
            }
        }
    }

    pub fn cancel_task(&mut self, character: CharacterId) -> Result<Subject, ActionError> {
        let character = self.universe.get_character_mut(character)?;

        character.task = match character.task {
            Idle => Idle,
            PendingBeginning { .. } => Idle,
            Beginning { time, activity } => Ending { time, activity },
            Running { activity } => PendingEnding { activity },
            PendingEnding { activity } => PendingEnding { activity },
            Ending { time, activity } => Ending { time, activity },
        };

        Ok(Subject::nothing())
    }

    pub fn update_activities(&mut self, elapsed_time: Time) {
        let mut update = Subject::new();
        for i in 0..self.universe.state.characters.len() {
            loop {
                let character = &mut self.universe.state.characters[i];
                let character_id = character.id;
                let character_task = character.task;

                let task =
                    self.update_task(character_id, character_task, elapsed_time, &mut update);
                let character = &mut self.universe.state.characters[i];
                character.task = task;

                // instant activities state fast forward
                match task {
                    PendingBeginning { .. } | PendingEnding { .. } => continue,
                    Beginning { time, .. } | Ending { time, .. } => {
                        if time.is_instant() {
                            continue;
                        } else {
                            break;
                        }
                    }
                    _ => break,
                }
            }
        }
        self.publish(update);
    }

    pub fn update_task(
        &mut self,
        character: CharacterId,
        task: Task,
        elapsed_time: Time,
        update: &mut Subject,
    ) -> Task {
        match task {
            Idle => Idle,
            PendingBeginning { activity } => match self.begin_activity(character, activity) {
                Ok(time) => {
                    update.push(ActivityBegun {
                        character,
                        activity,
                        time,
                    });
                    Beginning { time, activity }
                }
                Err(error) => {
                    error!("Unable to begin task {:?} correctly, {:?}", activity, error);
                    update.push(ActivityTerminated {
                        character,
                        activity,
                    });
                    Idle
                }
            },
            Beginning { time, activity } => {
                if elapsed_time >= time {
                    update.push(ActivityStarted {
                        character,
                        activity,
                    });
                    Running { activity }
                } else {
                    let time = time - elapsed_time;
                    Beginning { time, activity }
                }
            }
            Running { activity } => match self.run_activity(character, activity, elapsed_time) {
                Process::Progress(events) => {
                    update.join(events);
                    Running { activity }
                }
                Process::Finished(events) => {
                    update.join(events);
                    PendingEnding { activity }
                }
                Process::Terminated(error) => {
                    error!("Unable to run task {:?} correctly, {:?}", activity, error);
                    update.push(ActivityTerminated {
                        character,
                        activity,
                    });
                    Idle
                }
            },
            PendingEnding { activity } => match self.end_activity(character, activity) {
                Ok(time) => {
                    update.push(ActivityStopped {
                        character,
                        activity,
                        time,
                    });
                    Ending { time, activity }
                }
                Err(error) => {
                    error!(
                        "Unable to cancel task {:?} correctly, {:?}",
                        activity, error
                    );
                    update.push(ActivityTerminated {
                        character,
                        activity,
                    });
                    Idle
                }
            },
            Ending { time, activity } => {
                if elapsed_time >= time {
                    update.push(ActivityEnded {
                        character,
                        activity,
                    });
                    match self.finish_activity(character, activity) {
                        Ok(finish) => update.join(finish),
                        Err(error) => {
                            error!(
                                "Unable to finish task {:?} correctly, {:?}",
                                activity, error
                            )
                        }
                    }
                    Idle
                } else {
                    let time = time - elapsed_time;
                    Ending { time, activity }
                }
            }
        }
    }

    pub fn run_activity(
        &mut self,
        character: CharacterId,
        activity: Activity,
        elapsed_time: Time,
    ) -> Process {
        let processing = match activity {
            Movement => Ok(Process::Progress(Subject::nothing())),
            CropHarvesting { crop } => self.run_crop_harvesting(character, crop, elapsed_time),
            Sleeping { bed } => self.run_sleep(character, bed, elapsed_time),
            BuildingDesign { .. } => Ok(Process::Progress(Subject::nothing())),
            Building { construction } => self.run_building(character, construction, elapsed_time),
            StorageUsage { .. } => Ok(Process::Progress(Subject::nothing())),
            Planting { .. } => Ok(Process::Progress(Subject::nothing())),
            SpaceshipCommand { .. } => Ok(Process::Progress(Subject::nothing())),
        };

        match processing {
            Ok(process) => process,
            Err(error) => Process::Terminated(error),
        }
    }

    pub fn begin_activity(
        &mut self,
        character: CharacterId,
        activity: Activity,
    ) -> Result<Time, ActionError> {
        match activity {
            Sleeping { bed } => self.begin_sleep(character, bed),
            CropHarvesting { .. } => Ok(Time::instant()),
            Movement => Ok(Time::instant()),
            BuildingDesign { .. } => Ok(Time::instant()),
            Building { .. } => Ok(Time::instant()),
            StorageUsage { .. } => Ok(Time::instant()),
            Planting { .. } => Ok(Time::instant()),
            SpaceshipCommand { .. } => Ok(Time::instant()),
        }
    }

    pub fn end_activity(
        &mut self,
        character: CharacterId,
        activity: Activity,
    ) -> Result<Time, ActionError> {
        match activity {
            Sleeping { bed } => self.end_sleep(character, bed),
            CropHarvesting { .. } => Ok(Time::instant()),
            Movement => Ok(Time::instant()),
            BuildingDesign { .. } => Ok(Time::instant()),
            Building { .. } => Ok(Time::instant()),
            StorageUsage { storage } => self.end_storage_usage(character, storage),
            Planting { .. } => Ok(Time::instant()),
            SpaceshipCommand { .. } => Ok(Time::instant()),
        }
    }

    pub fn finish_activity(
        &mut self,
        character: CharacterId,
        activity: Activity,
    ) -> Result<Subject, ActionError> {
        match activity {
            Sleeping { bed } => self.finish_sleep(character, bed),
            Movement => Ok(Subject::nothing()),
            CropHarvesting { .. } => Ok(Subject::nothing()),
            BuildingDesign { .. } => Ok(Subject::nothing()),
            Building { .. } => Ok(Subject::nothing()),
            StorageUsage { storage } => self.finish_storage_usage(character, storage),
            Planting { .. } => Ok(Subject::nothing()),
            SpaceshipCommand { .. } => Ok(Subject::nothing()),
        }
    }

    fn handle_action(
        &mut self,
        character: CharacterId,
        action: Action,
    ) -> Result<Subject, ActionError> {
        match action {
            PlantCrop {
                seed,
                position,
                rotation,
            } => self.plant_crop(character, seed, position, rotation),
            HarvestCrop { crop } => self.harvest_crop(character, crop),
            MoveCharacter { position } => self.move_character(character, position),
            MoveSpaceship { position } => self.move_spaceship(character, position),
            LaunchSpaceship => self.launch_spaceship(character),
            LandSpaceship { position, colony } => self.land_spaceship(character, colony, position),
            CancelActivity => self.cancel_task(character),
            BuildSpaceship { .. } => unimplemented!(),
            ConsumeItem { consumable } => consume_item(self, character, consumable),
            UseWorkbench { workbench } => self.use_workbench(character, workbench),
            UseStorage { storage } => self.use_storage(character, storage),
            UseBed { bed } => self.use_bed(character, bed),
            UsePlanter { planter } => self.use_planter(character, planter),
            UseBridge { bridge } => self.use_bridge(character, bridge),
            DesignBuilding {
                recipe,
                position,
                rotation,
            } => self.design_building(character, recipe, position, rotation),
            Build { construction } => self.build(character, construction),
            TakeItem { item } => self.take_item(character, item),
            StoreItem { item } => self.store_item(character, item),
            UndockSpaceship => self.undock_spaceship(character),
            DockSpaceship { deck, position } => self.dock_spaceship(character, deck, position),
        }
    }

    pub fn update_game(&mut self, elapsed_real_time: Duration) {
        // 1 game minute = 1 real second
        // 60 game minutes = 1 real minute
        // 1 game hour = 100 real seconds (1.6 minutes)
        // 9 game hours = 15 real minutes
        // 0.9 game days = 15 real minutes
        // 1 game day = 1000 real seconds (16.6 minutes)
        let elapsed_real_time = elapsed_real_time.as_secs_f32();
        let elapsed_time = Time::from_minutes(elapsed_real_time);

        let instant = Instant::now();
        let mut game_update = Subject::new();

        self.time.increment(&elapsed_time);

        for event in self.physics.update(elapsed_real_time) {
            if let BodyPositionUpdated { body, position, .. } = event {
                let navigator = body.0.into();
                let location = PathPoint::new(position.x, position.y);
                self.navigation.update_navigator(navigator, location);
            }
            match event.get_space() {
                Some(space) => game_update.push_at(event, Source::at(space)),
                None => game_update.push_at(event, Source::new()),
            }
        }

        for event in self.farming.update(elapsed_time.in_days()) {
            match event.get_space() {
                Some(space) => game_update.push_at(event, Source::at(space)),
                None => game_update.push_at(event, Source::new()),
            }
        }

        game_update.push_at(
            Universe::TimeUpdated {
                ms: self.time.in_millis(),
            },
            Source::new(),
        );

        self.update_activities(elapsed_time);

        self.metrics.game_update_domain_updates_time = instant.elapsed().as_secs_f32();
        self.metrics.game_update_events_count = game_update.events_count();
        self.metrics.game_update_event_records_count = game_update.records_count();
        let instant = Instant::now();
        self.publish(game_update);
        self.metrics.game_update_events_publish_time = instant.elapsed().as_secs_f32();
    }

    pub fn determine_spaceship_by_space(&self, space: &Space) -> Option<SpaceshipId> {
        if space.origin == SpaceOrigin::Deck {
            match self.universe.get_deck_by_space(space.id) {
                Ok(deck) => return Some(deck.spaceship),
                Err(error) => {
                    error!(
                        "Unable to determine spaceship of {:?}, {:?}",
                        space.id, error
                    )
                }
            }
        }

        None
    }

    pub fn get_controlled_spaceship(&self, bridge: &Bridge) -> Result<&Spaceship, ActionError> {
        let spaceship = bridge
            .spaceship
            .ok_or(UniverseError::BridgeHasNoSpaceshipControl)?;
        let spaceship = self.universe.get_spaceship(spaceship)?;

        Ok(spaceship)
    }

    pub fn get_space_mesh(&self, space: &Space) -> Result<MeshId, ActionError> {
        let universe = &self.universe;

        match space.origin {
            SpaceOrigin::Deck => {
                let deck = universe.get_deck_by_space(space.id)?;
                Ok(deck.mesh)
            }
            SpaceOrigin::Colony => {
                let colony = universe.get_colony_by_space(space.id)?;
                Ok(colony.mesh)
            }
            SpaceOrigin::System => Err(UniverseError::ConstructionSpaceInvalid.into()),
        }
    }
}

trait EventInSpace {
    fn get_space(&self) -> Option<SpaceId>;
}

impl EventInSpace for Physics {
    fn get_space(&self) -> Option<SpaceId> {
        match self {
            Physics::SpaceConnected { parent, .. } => Some(*parent),
            Physics::SpaceDisconnected { space } => Some(*space),
            Physics::BodyMotionSignalReceived { .. } => None,
            Physics::BodyMotionSignalUpdated { .. } => None,
            Physics::BodyMotionSignalLost { .. } => None,
            Physics::BodySpaceUpdated { space, .. } => Some(*space),
            Physics::BodyPositionUpdated { space, .. } => Some(*space),
            Physics::BarrierSpaceUpdated { space, .. } => Some(*space),
        }
    }
}

impl EventInSpace for Farming {
    fn get_space(&self) -> Option<SpaceId> {
        match self {
            Farming::PlantGrowthProgressed { farmland, .. } => Some(farmland.0.into()),
            Farming::PlantDestroyed { farmland, .. } => Some(farmland.0.into()),
            Farming::PlantMaturityProgressed { farmland, .. } => Some(farmland.0.into()),
            Farming::PlantHarvestingProgressed { farmland, .. } => Some(farmland.0.into()),
            Farming::LandChanged { farmland, .. } => Some(farmland.0.into()),
        }
    }
}
