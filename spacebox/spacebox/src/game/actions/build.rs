use universe::Activity::Building;
use universe::{CharacterId, ConstructionId};

use crate::game::ActionError;
use crate::game::Master;
use crate::game::Subject;

impl Master {
    pub fn build(
        &mut self,
        character: CharacterId,
        construction: ConstructionId,
    ) -> Result<Subject, ActionError> {
        let universe = &mut self.universe;
        let physics = &self.physics;

        let activity = Building { construction };
        let character = universe.get_character(character)?;
        let construction = universe.get_construction(construction)?;
        let barrier = physics.get_barrier(construction.barrier)?;
        let space = construction.space(barrier);
        physics.ensure_distance(character.body, construction.barrier, 5.0)?;
        let task = universe.prepare_task_start(character.id, activity)?;

        let building = Subject::at(space).occur(universe.start_task(task));
        Ok(building)
    }
}
