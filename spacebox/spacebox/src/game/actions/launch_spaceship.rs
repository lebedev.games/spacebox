use physics::Physics::BodySpaceUpdated;
use physics::{BodyOrigin, Vector};
use universe::CharacterId;

use crate::game::ActionError;
use crate::game::Disappearance::SpaceshipVanished;
use crate::game::Master;
use crate::game::{Source, Subject};

impl Master {
    pub fn launch_spaceship(&mut self, character: CharacterId) -> Result<Subject, ActionError> {
        let physics = &self.physics;
        let universe = &self.universe;
        let observing = &self.observing;

        let character = universe.get_character(character)?;
        let body = physics.get_body(character.body)?;
        let space = character.space(body);
        let bridge = character.task.ensure_spaceship_command()?;
        let bridge = universe.get_bridge(bridge)?;
        let spaceship = self.get_controlled_spaceship(bridge)?;
        let spaceship_index = self.universe.index_spaceship(spaceship.id)?;
        let body_index = self.physics.index_body(spaceship.body)?;
        let landing = spaceship.ensure_landing()?;
        let bounds_min = landing.bounds_min.into();
        let bounds_max = landing.bounds_max.into();
        let colony = universe.get_colony_by_space(space)?;
        let colony_space = colony.space;
        let colony_mesh = colony_space.0.into();
        let planet = universe.get_planet_by_colony(colony.id)?;
        let planet_body = physics.get_body(planet.body)?;
        let system_space = planet.space(planet_body);
        let body_position = physics.get_body(planet.body)?.position;
        let deck = universe.get_spaceship_deck(spaceship.id)?;
        let deck_space = deck.space;
        let deck_mesh = deck.mesh;
        let offset = -landing.point;

        let system_create = self.observe_space(system_space)?;
        let system_observers = &observing.get_space_groups(system_space);
        let colony_observers_before = &observing.get_space_groups(colony_space);

        let physics = &mut self.physics;
        let universe = &mut self.universe;

        // 0. operate spaceship launch
        let spaceship = &mut universe.state.spaceships[spaceship_index];
        spaceship.landing = None;
        let body = &mut physics.state.bodies[body_index];
        body.space = system_space;
        body.position = body_position;
        body.velocity = Vector::ZERO;
        body.steering = None;
        self.observing.append_link(deck_space, system_space);
        let spaceship_create = Master::observe_spaceship(spaceship, body);
        let spaceship_delete = vec![SpaceshipVanished { id: spaceship.id }];
        let spaceship_update = vec![BodySpaceUpdated {
            body: body.id,
            space: system_space,
            position: body_position,
            velocity: Vector::ZERO,
            offset: Vector::ZERO,
        }];

        let space_extraction = physics.prepare_extract_space_operation(
            colony_space,
            bounds_min,
            bounds_max,
            deck_space,
        )?;

        let observing = &mut self.observing;

        // update observers
        let mut deck_navigators = vec![];
        let mut deck_obstacles = vec![];
        let source = &physics.state.spaces[space_extraction.source_space_index];
        let target = &physics.state.spaces[space_extraction.target_space_index];
        for body in space_extraction.bodies.iter().cloned() {
            let body = &physics.state.bodies[body];
            if body.origin == BodyOrigin::Character {
                let character = universe.get_character(body.id.0.into())?;
                deck_navigators.push(character.navigator);
                let group = character.group;
                observing.remove_observer(source.id, group);
                observing.append_observer(target.id, group);
            }
        }
        for barrier in space_extraction.barriers.iter().cloned() {
            let barrier = &physics.state.barriers[barrier];
            let obstacle = barrier.id.0.into();
            deck_obstacles.push(obstacle);
        }
        //

        let navigation = &self.navigation;
        let mesh_extraction = navigation.prepare_extract_mesh_operation(
            colony_mesh,
            deck_mesh,
            offset.into(),
            deck_navigators,
            deck_obstacles,
        )?;

        let navigation = &mut self.navigation;
        navigation.extract_mesh(mesh_extraction);

        let deck_update = physics.extract_space(space_extraction);
        let deck_observers = &observing.get_space_groups(deck_space);
        let colony_observers = &observing.get_space_groups(colony_space);
        let system_observers_after = &observing.get_space_groups(system_space);

        let deck_delete = self.vanish_space(deck_space)?;
        let colony_delete = self.vanish_space(colony_space)?;

        let mut events = Subject::new();

        events.publish(system_create, Source::of(deck_observers - system_observers));
        events.publish(
            spaceship_create,
            Source::of(system_observers - colony_observers_before),
        );
        events.publish(spaceship_update, Source::at(system_space));
        events.publish(
            spaceship_delete,
            Source::of(colony_observers - system_observers_after),
        );
        events.publish(deck_update, Source::at(deck_space));
        events.publish(deck_delete, Source::of(colony_observers - deck_observers));
        events.publish(colony_delete, Source::of(deck_observers - colony_observers));

        Ok(events)
    }
}
