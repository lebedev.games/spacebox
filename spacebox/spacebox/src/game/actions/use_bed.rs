use universe::Activity::Sleeping;
use universe::{BedId, CharacterId};

use crate::game::ActionError;
use crate::game::Master;
use crate::game::Subject;

impl Master {
    pub fn use_bed(&mut self, character: CharacterId, bed: BedId) -> Result<Subject, ActionError> {
        let universe = &mut self.universe;
        let physics = &self.physics;

        let activity = Sleeping { bed };
        let character = universe.get_character(character)?;
        let body = physics.get_body(character.body)?;
        let space = character.space(body);
        let bed = universe.get_bed(bed)?;
        physics.ensure_distance(character.body, bed.barrier, self.rules.usage_distance)?;
        let bed_usage = universe.prepare_use_bed_operation(character.id, bed.id)?;
        let sleep_starting = universe.prepare_task_start(character.id, activity)?;

        Ok(Subject::at(space)
            .occur(universe.start_task(sleep_starting))
            .occur(universe.use_bed(bed_usage)))
    }
}
