use physics::SteeringBehaviour::MoveTowards;
use universe::{CharacterId, Coordinates};

use crate::game::ActionError;
use crate::game::Master;
use crate::game::Subject;

impl Master {
    pub fn move_spaceship(
        &mut self,
        character: CharacterId,
        position: Coordinates,
    ) -> Result<Subject, ActionError> {
        let universe = &self.universe;

        let character = universe.get_character(character)?;
        let bridge = character.task.ensure_spaceship_command()?;
        let bridge = universe.get_bridge(bridge)?;
        let spaceship = self.get_controlled_spaceship(bridge)?;
        let spaceship = spaceship.body;

        let physics = &mut self.physics;

        physics.set_body_steering_behaviour(
            spaceship,
            MoveTowards {
                destination: position.into(),
            },
        )?;

        Ok(Subject::nothing())
    }
}
