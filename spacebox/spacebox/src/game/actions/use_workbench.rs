use universe::Activity::BuildingDesign;
use universe::{CharacterId, WorkbenchId};

use crate::game::ActionError;
use crate::game::Master;
use crate::game::Subject;

impl Master {
    pub fn use_workbench(
        &mut self,
        character: CharacterId,
        workbench: WorkbenchId,
    ) -> Result<Subject, ActionError> {
        let universe = &mut self.universe;
        let physics = &self.physics;

        let activity = BuildingDesign { workbench };
        let character = universe.get_character(character)?;
        let body = physics.get_body(character.body)?;
        let space = character.space(body);
        let workbench = universe.get_workbench(workbench)?;
        physics.ensure_distance(character.body, workbench.barrier, self.rules.usage_distance)?;
        let task = universe.prepare_task_start(character.id, activity)?;

        let usage = Subject::at(space).occur(universe.start_task(task));
        Ok(usage)
    }
}
