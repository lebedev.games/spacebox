use inventory::ItemId;
use navigation::CreateObstacleOperation;
use physics::commands::CreateBarrierOperation;
use physics::{is_rotated_horizontally, BarrierOrigin, SpaceId};
use universe::{CharacterId, Construction, ConstructionKey, ConstructionProject, Coordinates};

use crate::game::ActionError;
use crate::game::Master;
use crate::game::Subject;

pub struct CreateConstructionOperation {
    space: SpaceId,
    construction: Construction,
    barrier: CreateBarrierOperation,
    obstacle: CreateObstacleOperation,
}

impl Master {
    pub fn design_building(
        &mut self,
        character: CharacterId,
        recipe: ItemId,
        position: Coordinates,
        rotation: f32,
    ) -> Result<Subject, ActionError> {
        let universe = &self.universe;
        let physics = &self.physics;
        let inventory = &self.inventory;

        let character = universe.get_character(character)?;
        let workbench = character.task.ensure_building_design()?;
        let workbench = universe.get_workbench(workbench)?;
        let barrier = physics.get_barrier(workbench.barrier)?;
        let space = workbench.space(barrier);
        let recipe = inventory.get_item(recipe)?;
        let recipe = recipe.ensure_building_recipe()?.clone();
        let construction = ConstructionKey(recipe.0);

        let id = self.id.generate_one();

        let construction =
            self.prepare_construction(id, construction, space, position, rotation)?;

        Ok(self.create_construction(construction))
    }

    pub fn prepare_construction(
        &self,
        id: usize,
        construction: ConstructionKey,
        space: SpaceId,
        position: Coordinates,
        rotation: f32,
    ) -> Result<CreateConstructionOperation, ActionError> {
        let universe = &self.universe;
        let physics = &self.physics;
        let navigation = &self.navigation;

        let construction = universe.get_construction_type(&construction)?;
        let space = physics.get_space(space)?;

        let (barrier, obstacle) = match &construction.project {
            ConstructionProject::PropProject { key } => {
                let prop = universe.get_prop_type(&key)?;
                (&prop.barrier, &prop.obstacle)
            }
            ConstructionProject::StorageProject { key } => {
                let storage = universe.get_storage_type(&key)?;
                (&storage.barrier, &storage.obstacle)
            }
            ConstructionProject::WorkbenchProject { key } => {
                let workbench = universe.get_workbench_type(&key)?;
                (&workbench.barrier, &workbench.obstacle)
            }
            ConstructionProject::PlanterProject { key } => {
                let planter = universe.get_planter_type(&key)?;
                (&planter.barrier, &planter.obstacle)
            }
            ConstructionProject::BedProject { key } => {
                let bed = universe.get_bed_type(&key)?;
                (&bed.barrier, &bed.obstacle)
            }
            ConstructionProject::BridgeProject { key } => {
                let bridge = universe.get_bridge_type(&key)?;
                (&bridge.barrier, &bridge.obstacle)
            }
        };
        let mesh = self.get_space_mesh(space)?;
        let barrier = physics.prepare_barrier(
            space.id,
            id.into(),
            barrier,
            BarrierOrigin::Construction,
            position.into(),
            rotation,
        )?;
        let obstacle = navigation.prepare_create_obstacle_operation(
            mesh,
            obstacle,
            id.into(),
            position.into(),
            is_rotated_horizontally(rotation),
        )?;
        let key = construction.key.clone();
        let project = construction.project.clone();
        let space = space.id;

        let construction = Construction {
            id: id.into(),
            key,
            barrier: id.into(),
            obstacle: id.into(),
            project,
            progress: 0.0,
        };

        Ok(CreateConstructionOperation {
            construction,
            barrier,
            obstacle,
            space,
        })
    }

    pub fn create_construction(&mut self, operation: CreateConstructionOperation) -> Subject {
        let universe = &mut self.universe;
        let physics = &mut self.physics;
        let navigation = &mut self.navigation;

        let construction = operation.construction;
        let barrier = operation.barrier;
        let obstacle = operation.obstacle;

        let events = Master::observe_construction(&construction, &barrier.barrier);
        universe.state.constructions.push(construction);

        Subject::at(operation.space)
            .occur(physics.create_barrier(barrier))
            .occur(navigation.create_obstacle(obstacle))
            .occur(events)
    }
}
