use farming::handlers::PlantOperation;
use farming::PlantKind;
use inventory::ItemId;
use physics::commands::CreateBarrierOperation;
use physics::{BarrierOrigin, SpaceId, SpaceOrigin};
use universe::{CharacterId, Coordinates, Crop, CropKey};

use crate::game::ActionError;
use crate::game::Master;
use crate::game::Subject;

pub struct CreateCropOperation {
    space: SpaceId,
    crop: Crop,
    plant: PlantOperation,
    barrier: CreateBarrierOperation,
}

impl Master {
    pub fn plant_crop(
        &mut self,
        character: CharacterId,
        seed: ItemId,
        position: Coordinates,
        rotation: f32,
    ) -> Result<Subject, ActionError> {
        let id = self.id.generate_one();
        let universe = &self.universe;
        let physics = &self.physics;
        let inventory = &self.inventory;

        let character = universe.get_character(character)?;
        let planter = character.task.ensure_planting()?;
        let planter = universe.get_planter(planter)?;
        let barrier = physics.get_barrier(planter.barrier)?;
        let space = barrier.space;
        let seed = inventory.get_item(seed)?;
        let crop_key = seed.ensure_seed()?;
        let crop_key = CropKey(crop_key.0);

        let item = inventory.prepare_remove_item_operation(seed.id)?;
        let operation = self.create_crop(id, &crop_key, space, position, rotation)?;

        let inventory = &mut self.inventory;

        let mut process = Subject::at(space).occur(inventory.remove_item(item));

        process.join(self.apply_create_crop(operation));

        Ok(process)
    }

    pub fn create_crop(
        &self,
        id: usize,
        crop: &CropKey,
        space: SpaceId,
        position: Coordinates,
        rotation: f32,
    ) -> Result<CreateCropOperation, ActionError> {
        let universe = &self.universe;
        let farming = &self.farming;
        let physics = &self.physics;

        let crop = universe.get_crop_type(crop)?;
        let space = physics.get_space(space)?;
        let farmland = match space.origin {
            SpaceOrigin::Deck => universe.get_deck_by_space(space.id)?.farmland,
            SpaceOrigin::Colony => universe.get_colony_by_space(space.id)?.farmland,
            SpaceOrigin::System => {
                return Err(ActionError::Obsolete(format!(
                    "Unable to plant crop {:?} on system",
                    &crop.key
                )))
            }
        };
        let plant = farming.prepare_plant(
            farmland,
            id.into(),
            &crop.plant,
            PlantKind::Crop,
            position.into(),
            0.0,
        )?;

        let barrier = physics.prepare_barrier(
            space.id,
            id.into(),
            &crop.barrier,
            BarrierOrigin::Crop,
            position.into(),
            rotation,
        )?;
        let crop = Crop {
            id: id.into(),
            key: crop.key.clone(),
            plant: id.into(),
            barrier: id.into(),
        };
        Ok(CreateCropOperation {
            crop,
            plant,
            barrier,
            space: space.id,
        })
    }

    pub fn apply_create_crop(&mut self, operation: CreateCropOperation) -> Subject {
        let universe = &mut self.universe;
        let farming = &mut self.farming;
        let physics = &mut self.physics;

        let events = Master::observe_crop(
            &operation.crop,
            &operation.barrier.barrier,
            &operation.plant.plant,
        );
        universe.state.crops.push(operation.crop);

        Subject::at(operation.space)
            .occur(physics.create_barrier(operation.barrier))
            .occur(farming.plant(operation.plant))
            .occur(events)
    }
}
