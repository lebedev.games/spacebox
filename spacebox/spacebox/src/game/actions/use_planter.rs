use universe::Activity::Planting;
use universe::{CharacterId, PlanterId};

use crate::game::ActionError;
use crate::game::Master;
use crate::game::Subject;

impl Master {
    pub fn use_planter(
        &mut self,
        character: CharacterId,
        planter: PlanterId,
    ) -> Result<Subject, ActionError> {
        let universe = &mut self.universe;
        let physics = &self.physics;

        let activity = Planting { planter };
        let character = universe.get_character(character)?;
        let body = physics.get_body(character.body)?;
        let space = character.space(body);
        let planter = universe.get_planter(planter)?;
        physics.ensure_distance(character.body, planter.barrier, self.rules.usage_distance)?;
        let task = universe.prepare_task_start(character.id, activity)?;

        let usage = Subject::at(space).occur(universe.start_task(task));
        Ok(usage)
    }
}
