use universe::Activity::SpaceshipCommand;
use universe::{BridgeId, CharacterId};

use crate::game::ActionError;
use crate::game::Master;
use crate::game::Subject;

impl Master {
    pub fn use_bridge(
        &mut self,
        character: CharacterId,
        bridge: BridgeId,
    ) -> Result<Subject, ActionError> {
        let universe = &mut self.universe;
        let physics = &self.physics;

        let activity = SpaceshipCommand { bridge };
        let character = universe.get_character(character)?;
        let body = physics.get_body(character.body)?;
        let space = character.space(body);
        let bridge = universe.get_bridge(bridge)?;
        physics.ensure_distance(character.body, bridge.barrier, self.rules.usage_distance)?;
        let task = universe.prepare_task_start(character.id, activity)?;

        let usage = Subject::at(space).occur(universe.start_task(task));
        Ok(usage)
    }
}
