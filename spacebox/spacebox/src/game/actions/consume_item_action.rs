use inventory::{Effect, ItemId};
use physics::{PhysicsDomain, SpaceId};
use universe::{CharacterId, Universe, UniverseDomain};

use crate::game::ActionError;
use crate::game::Master;
use crate::game::Subject;

pub fn consume_item(
    master: &mut Master,
    character: CharacterId,
    consumable: ItemId,
) -> Result<Subject, ActionError> {
    let physics = &master.physics;
    let universe = &mut master.universe;
    let inventory = &mut master.inventory;

    let character = universe.get_character(character)?;
    let body = physics.get_body(character.body)?;
    let space = character.space(body);
    let consumable = inventory.get_item(consumable)?;
    let effects = consumable.ensure_consumable()?;

    let consumed_item = inventory.prepare_remove_item_operation(consumable.id)?;
    let effects = prepare_effects_apply(universe, physics, character.id, &effects)?;

    let consuming = Subject::at(space)
        .occur(inventory.remove_item(consumed_item))
        .occur(apply_effects(universe, effects));

    Ok(consuming)
}

pub struct ApplyEffectsOperation {
    pub space: SpaceId,
    pub character_index: usize,
    pub effects: Vec<Effect>,
}

pub fn prepare_effects_apply(
    universe: &UniverseDomain,
    physics: &PhysicsDomain,
    character: CharacterId,
    effects: &Vec<Effect>,
) -> Result<ApplyEffectsOperation, ActionError> {
    let character_index = universe.index_character(character)?;
    let character = &universe.state.characters[character_index];
    let body = physics.get_body(character.body)?;
    let space = character.space(body);

    Ok(ApplyEffectsOperation {
        character_index,
        effects: effects.clone(),
        space,
    })
}

pub fn apply_effects(
    universe: &mut UniverseDomain,
    operation: ApplyEffectsOperation,
) -> Vec<Universe> {
    let character = &mut universe.state.characters[operation.character_index];
    let effects = operation.effects;

    let mut events = Vec::new();
    for effect in effects {
        match effect {
            Effect::RestoreEnergy { amount } => {
                let amount = amount.min(character.energy_limit - character.energy);
                character.energy += amount;
                events.push(Universe::CharacterEnergyRestored {
                    id: character.id,
                    amount,
                    energy: character.energy,
                    space: operation.space,
                })
            }
            Effect::ReduceEnergy { amount } => {
                let amount = amount.min(character.energy);
                character.energy -= amount;
                events.push(Universe::CharacterEnergyReduced {
                    id: character.id,
                    amount,
                    energy: character.energy,
                    space: operation.space,
                })
            }
        }
    }
    events
}
