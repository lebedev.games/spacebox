use universe::CharacterId;

use crate::game::ActionError;
use crate::game::Master;
use crate::game::Subject;

impl Master {
    pub fn undock_spaceship(&mut self, _character: CharacterId) -> Result<Subject, ActionError> {
        unimplemented!()
    }
}
