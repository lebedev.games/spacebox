use universe::Activity::CropHarvesting;
use universe::{CharacterId, CropId};

use crate::game::ActionError;
use crate::game::Master;
use crate::game::Subject;

impl Master {
    pub fn harvest_crop(
        &mut self,
        character: CharacterId,
        crop: CropId,
    ) -> Result<Subject, ActionError> {
        let universe = &mut self.universe;
        let physics = &self.physics;

        let activity = CropHarvesting { crop };
        let character = universe.get_character(character)?;
        let body = physics.get_body(character.body)?;
        let space = character.space(body);
        let crop = universe.get_crop(crop)?;
        physics.ensure_distance(character.body, crop.barrier, self.rules.usage_distance)?;
        let task = universe.prepare_task_start(character.id, activity)?;

        let building = Subject::at(space).occur(universe.start_task(task));
        Ok(building)
    }
}
