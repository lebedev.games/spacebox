use physics::{SteeringBehaviour, Vector};
use universe::{CharacterId, Coordinates};

use crate::game::ActionError;
use crate::game::Master;
use crate::game::Subject;

impl Master {
    pub fn move_character(
        &mut self,
        character: CharacterId,
        position: Coordinates,
    ) -> Result<Subject, ActionError> {
        let universe = &self.universe;
        let navigation = &mut self.navigation;
        let physics = &mut self.physics;

        let character = universe.get_character(character)?;
        let body = physics.get_body(character.body)?;
        let space = character.space(body);

        let route = navigation.prepare_navigate_operation(character.navigator, position.into())?;
        // skip first point (current body position)
        // workaround for bad follow path steering behaviour
        let path: Vec<Vector> = route
            .path
            .iter()
            .skip(1)
            .map(|point| Vector {
                x: point.x,
                y: point.y,
            })
            .collect();
        let behaviour = SteeringBehaviour::FollowPath { path };

        physics.set_body_steering_behaviour(character.body, behaviour)?;

        let movement = Subject::at(space).occur(navigation.navigate(route));

        Ok(movement)
    }
}
