use universe::Activity::StorageUsage;
use universe::{CharacterId, StorageId};

use crate::game::ActionError;
use crate::game::Master;
use crate::game::{Source, Subject};

impl Master {
    pub fn use_storage(
        &mut self,
        character: CharacterId,
        storage: StorageId,
    ) -> Result<Subject, ActionError> {
        let universe = &mut self.universe;
        let physics = &self.physics;
        let inventory = &self.inventory;

        let activity = StorageUsage { storage };
        let character = universe.get_character(character)?;
        let body = physics.get_body(character.body)?;
        let space = character.space(body);
        let group = character.group;
        let storage = universe.get_storage(storage)?;
        physics.ensure_distance(character.body, storage.barrier, self.rules.usage_distance)?;
        let container_inspect = inventory.open_container(storage.container)?;
        let task = universe.prepare_task_start(character.id, activity)?;

        let mut usage = Subject::new();
        usage.publish(universe.start_task(task), Source::at(space));
        usage.publish(container_inspect, Source::only(group));

        Ok(usage)
    }
}
