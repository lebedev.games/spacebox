use physics::Physics::BodySpaceUpdated;
use physics::{BodyOrigin, Vector};
use universe::{CharacterId, ColonyId, Coordinates, Landing};

use crate::game::ActionError;
use crate::game::Disappearance::SpaceshipVanished;
use crate::game::Master;
use crate::game::{Source, Subject};

impl Master {
    pub fn land_spaceship(
        &mut self,
        character: CharacterId,
        colony: ColonyId,
        landing_point: Coordinates,
    ) -> Result<Subject, ActionError> {
        let physics = &self.physics;
        let universe = &self.universe;
        let navigation = &self.navigation;
        let observing = &self.observing;

        let character = universe.get_character(character)?;

        let bridge = character.task.ensure_spaceship_command()?;
        let bridge = universe.get_bridge(bridge)?;
        let spaceship = self.get_controlled_spaceship(bridge)?;
        let spaceship_index = universe.index_spaceship(spaceship.id)?;
        let body_index = physics.index_body(spaceship.body)?;
        let body = &physics.state.bodies[body_index];
        let system_space = spaceship.space(body);
        let deck = universe.get_spaceship_deck(spaceship.id)?;
        let deck_space = deck.space;
        let deck_mesh = deck.mesh;
        let deck_size = navigation.get_mesh(deck_mesh)?.size.into();
        let colony = universe.get_colony(colony)?;
        let colony_space = colony.space;
        let colony_mesh = colony.mesh;

        let landing = Landing {
            bounds_min: landing_point,
            bounds_max: landing_point + deck_size,
            point: landing_point,
        };

        let deck_create = self.observe_space(deck_space)?;
        let colony_create = self.observe_space(colony_space)?;

        let deck_observers = &observing.get_space_groups(deck_space);
        let colony_observers = &observing.get_space_groups(colony_space);
        let system_observers = &observing.get_space_groups(system_space);

        let space_translation = physics.prepare_translate_space_operation(
            deck_space,
            colony_space,
            landing_point.into(),
        )?;

        let mesh_translation = navigation.prepare_translate_mesh_operation(
            deck_mesh,
            colony_mesh,
            landing_point.into(),
        )?;

        let physics = &mut self.physics;
        let universe = &mut self.universe;
        let navigation = &mut self.navigation;
        let observing = &mut self.observing;

        // update observers
        let source = &physics.state.spaces[space_translation.source_space_index];
        let target = &physics.state.spaces[space_translation.target_space_index];
        for body in space_translation.bodies.iter().cloned() {
            let body = &physics.state.bodies[body];
            if body.origin == BodyOrigin::Character {
                let character = universe.get_character(body.id.0.into())?;
                let group = character.group;
                observing.remove_observer(source.id, group);
                observing.append_observer(target.id, group);
            }
        }
        //

        let deck_update = physics.translate_space(space_translation);
        navigation.translate_mesh(mesh_translation);

        // 5. operate spaceship landing
        let spaceship = &mut universe.state.spaceships[spaceship_index];
        spaceship.landing = Some(landing);
        let body = &mut physics.state.bodies[body_index];
        body.space = colony_space;
        body.position = landing_point.into();
        body.velocity = Vector::ZERO;
        body.steering = None;
        observing.remove_link(deck_space, system_space);

        let spaceship_create = Master::observe_spaceship(spaceship, body);
        let spaceship_update = vec![BodySpaceUpdated {
            body: body.id,
            space: colony_space,
            position: landing_point.into(),
            velocity: Vector::ZERO,
            offset: Vector::ZERO,
        }];
        let spaceship_delete = vec![SpaceshipVanished { id: spaceship.id }];
        let system_delete = self.vanish_space(system_space)?;

        let system_observers_after = &self.observing.get_space_groups(system_space);
        let colony_observers_after = &self.observing.get_space_groups(colony_space);

        let mut events = Subject::new();

        events.publish(colony_create, Source::of(deck_observers - colony_observers));
        events.publish(
            spaceship_create,
            Source::of(colony_observers - system_observers),
        );
        events.publish(spaceship_update, Source::at(colony_space));
        events.publish(
            spaceship_delete,
            Source::of(system_observers_after - colony_observers_after),
        );
        events.publish(deck_create, Source::of(colony_observers - deck_observers));
        events.publish(deck_update, Source::at(colony_space));
        events.publish(
            system_delete,
            Source::of(deck_observers - system_observers_after),
        );

        Ok(events)
    }
}
