use universe::{CharacterId, Coordinates, DeckId};

use crate::game::ActionError;
use crate::game::Master;
use crate::game::Subject;

impl Master {
    pub fn dock_spaceship(
        &mut self,
        _character: CharacterId,
        _deck: DeckId,
        _position: Coordinates,
    ) -> Result<Subject, ActionError> {
        unimplemented!()
    }
}
