use std::collections::HashSet;

use inventory::ItemId;
use universe::{Activity, CharacterId, Task};

use crate::game::ActionError;
use crate::game::Master;
use crate::game::Subject;

impl Master {
    pub fn store_item(
        &mut self,
        character: CharacterId,
        item: ItemId,
    ) -> Result<Subject, ActionError> {
        let universe = &self.universe;
        let inventory = &self.inventory;

        let character = universe.get_character(character)?;
        let storage = character.task.ensure_storage_usage()?;
        let storage = universe.get_storage(storage)?;

        let mut observers = HashSet::new();
        for character in &universe.state.characters {
            let activity = match character.task {
                Task::PendingBeginning { activity, .. } => activity,
                Task::Beginning { activity, .. } => activity,
                Task::Running { activity } => activity,
                _ => continue,
            };
            match activity {
                Activity::StorageUsage { storage: s } => {
                    if s == storage.id {
                        observers.insert(character.group);
                    }
                }
                _ => continue,
            }
        }

        let transfer =
            inventory.prepare_move_item_operation(character.container, item, storage.container)?;

        let inventory = &mut self.inventory;

        let take = Subject::of(observers).occur(inventory.move_item(transfer));

        Ok(take)
    }
}
