use std::ffi::OsStr;
use std::path::PathBuf;
use std::rc::Rc;

use farming::FarmingDatabase;
use glob::glob;
use inventory::InventoryDatabase;
use navigation::NavigationDatabase;
use notify::DebouncedEvent;
use physics::PhysicsDatabase;
use piloting::PilotingDatabase;
use universe::UniverseDatabase;

use crate::game::Master;
use crate::persistence::{read_file_quietly, DataError, DataPath};

#[derive(Default, Clone)]
pub struct Database {
    pub universe: Rc<UniverseDatabase>,
    pub physics: Rc<PhysicsDatabase>,
    pub navigation: Rc<NavigationDatabase>,
    pub piloting: Rc<PilotingDatabase>,
    pub farming: Rc<FarmingDatabase>,
    pub inventory: Rc<InventoryDatabase>,
}

#[derive(Debug)]
pub enum Change {
    Update,
}

#[derive(Debug)]
pub struct ChangeEvent {
    pub path: String,
    pub change: Change,
    pub route: Vec<String>,
}

pub fn detect_changes(
    channel: &Vec<DebouncedEvent>,
    database_absolute_path: &PathBuf,
) -> Vec<ChangeEvent> {
    let mut changes = Vec::new();
    for event in channel {
        let operations = match event {
            DebouncedEvent::Create(path) => vec![(path, Change::Update)],
            DebouncedEvent::Write(path) => vec![(path, Change::Update)],
            // DebouncedEvent::Remove(path) => vec![(path, Change::Remove)],
            DebouncedEvent::Rename(_, destination) => {
                vec![(destination, Change::Update)]
            }
            _ => continue,
        };

        for (path, change) in operations {
            let extension = path.extension().and_then(OsStr::to_str).unwrap_or("err");
            if extension != "yaml" {
                continue;
            }

            let relative_path = path.strip_prefix(&database_absolute_path).unwrap();
            let route: Vec<String> = relative_path
                .components()
                .map(|component| component.as_os_str().to_str().unwrap_or(".err").to_string())
                .skip(1) // save name
                .collect();

            changes.push(ChangeEvent {
                path: path.display().to_string(),
                change,
                route,
            })
        }
    }

    changes
}

impl Master {
    pub fn update_database(&mut self, source: &str) {
        let path = self.database_path.join(source);
        info!("Update database from {}", path.display());
        let files = match glob(&format!("{}/**/*.yaml", path.display())) {
            Ok(files) => files,
            Err(error) => {
                error!("Unable to update database, {:?}", error);
                return;
            }
        };
        let files = files
            .flat_map(|file| file)
            .map(|file| DebouncedEvent::Create(file))
            .collect();
        let changes = detect_changes(&files, &self.database_path);
        info!("Update changes: {}", changes.len());
        self.handle_changes(changes);
    }

    pub fn handle_changes(&mut self, events: Vec<ChangeEvent>) {
        self.close_database();
        for event in events {
            let route: Vec<&str> = event.route.iter().map(|component| &component[..]).collect();
            match event.change {
                Change::Update => self.update_collection(event.path, route),
            }
        }
        self.share_database();
    }

    pub fn close_database(&mut self) {
        self.universe.database = Default::default();
        self.physics.database = Default::default();
        self.navigation.database = Default::default();
        self.piloting.database = Default::default();
        self.farming.database = Default::default();
        self.inventory.database = Default::default();
    }

    pub fn share_database(&mut self) {
        let shared = &self.shared_database;

        self.universe.database = shared.universe.clone();
        self.physics.database = shared.physics.clone();
        self.navigation.database = shared.navigation.clone();
        self.piloting.database = shared.piloting.clone();
        self.farming.database = shared.farming.clone();
        self.inventory.database = shared.inventory.clone();
    }

    pub fn update_collection(&mut self, path: String, route: Vec<&str>) {
        match &route[..] {
            ["types", domain, collection] => self.update_types(
                domain,
                collection.strip_suffix(".yaml").unwrap_or(collection),
                path,
            ),
            ["state", domain, collection] => self.update_state(
                domain,
                collection.strip_suffix(".yaml").unwrap_or(collection),
                path,
            ),
            _ => error!(
                "Unable to upsert data, route {:?} not implemented yet",
                route
            ),
        }
    }

    pub fn save_types(&self, destination: &str) -> Result<(), DataError> {
        let root = &DataPath::new(&self.database_path).join(destination);
        info!("Save types to {}", root.display());

        let data = root.join("types").join("universe");
        let state = &self.shared_database.universe;
        println!("characters {:?}", &state.characters);
        data.write("systems.yaml", &state.systems)?;
        data.write("planets.yaml", &state.planets)?;
        data.write("spaceships.yaml", &state.spaceships)?;
        data.write("stations.yaml", &state.stations)?;
        data.write("decks.yaml", &state.decks)?;
        data.write("colonies.yaml", &state.colonies)?;
        data.write("characters.yaml", &state.characters)?;
        data.write("props.yaml", &state.props)?;
        data.write("crops.yaml", &state.crops)?;
        data.write("trees.yaml", &state.trees)?;
        data.write("storages.yaml", &state.storages)?;
        data.write("workbenches.yaml", &state.workbenches)?;
        data.write("constructions.yaml", &state.constructions)?;
        data.write("beds.yaml", &state.beds)?;
        data.write("planters.yaml", &state.planters)?;
        data.write("bridges.yaml", &state.bridges)?;

        let data = root.join("types").join("farming");
        let state = &self.shared_database.farming;
        data.write("farmlands.yaml", &state.farmlands)?;
        data.write("plants.yaml", &state.plants)?;

        let data = root.join("types").join("inventory");
        let state = &self.shared_database.inventory;
        data.write("containers.yaml", &state.containers)?;
        data.write("items.yaml", &state.items)?;

        let data = root.join("types").join("navigation");
        let state = &self.shared_database.navigation;
        data.write("meshes.yaml", &state.meshes)?;
        data.write("navigators.yaml", &state.navigators)?;
        data.write("obstacles.yaml", &state.obstacles)?;

        let data = root.join("types").join("piloting");
        let state = &self.shared_database.piloting;
        data.write("spacecrafts.yaml", &state.spacecrafts)?;

        let data = root.join("types").join("physics");
        let state = &self.shared_database.physics;
        data.write("spaces.yaml", &state.spaces)?;
        data.write("bodies.yaml", &state.bodies)?;
        data.write("barriers.yaml", &state.barriers)?;
        data.write("portals.yaml", &state.portals)?;

        Ok(())
    }

    pub fn update_types(&mut self, domain: &str, collection: &str, path: String) {
        let farming = match Rc::get_mut(&mut self.shared_database.farming) {
            Some(farming) => farming,
            None => {
                error!(
                    "Unable to update {} domain types, farming database referenced",
                    domain
                );
                return;
            }
        };
        let inventory = match Rc::get_mut(&mut self.shared_database.inventory) {
            Some(inventory) => inventory,
            None => {
                error!(
                    "Unable to update {} domain types, inventory database referenced",
                    domain
                );
                return;
            }
        };
        let navigation = match Rc::get_mut(&mut self.shared_database.navigation) {
            Some(navigation) => navigation,
            None => {
                error!(
                    "Unable to update {} domain types, navigation database referenced",
                    domain
                );
                return;
            }
        };
        let physics = match Rc::get_mut(&mut self.shared_database.physics) {
            Some(physics) => physics,
            None => {
                error!(
                    "Unable to update {} domain types, physics database referenced",
                    domain
                );
                return;
            }
        };
        let piloting = match Rc::get_mut(&mut self.shared_database.piloting) {
            Some(piloting) => piloting,
            None => {
                error!(
                    "Unable to update {} domain types, piloting database referenced",
                    domain
                );
                return;
            }
        };
        let universe = match Rc::get_mut(&mut self.shared_database.universe) {
            Some(universe) => universe,
            None => {
                error!(
                    "Unable to update {} domain types, universe database referenced",
                    domain
                );
                return;
            }
        };

        match domain {
            "farming" => match collection {
                "plants" => farming.plants = read_file_quietly(path),
                "farmlands" => farming.farmlands = read_file_quietly(path),
                collection => error!(
                    "Unable to update {} domain types, {} not implemented yet",
                    domain, collection,
                ),
            },
            "inventory" => match collection {
                "items" => inventory.items = read_file_quietly(path),
                "containers" => inventory.containers = read_file_quietly(path),
                collection => error!(
                    "Unable to update {} domain types, {} not implemented yet",
                    domain, collection,
                ),
            },
            "navigation" => match collection {
                "obstacles" => navigation.obstacles = read_file_quietly(path),
                "meshes" => navigation.meshes = read_file_quietly(path),
                "navigators" => navigation.navigators = read_file_quietly(path),
                collection => error!(
                    "Unable to update {} domain types, {} not implemented yet",
                    domain, collection,
                ),
            },
            "physics" => match collection {
                "bodies" => physics.bodies = read_file_quietly(path),
                "barriers" => physics.barriers = read_file_quietly(path),
                "portals" => physics.portals = read_file_quietly(path),
                "spaces" => physics.spaces = read_file_quietly(path),
                collection => error!(
                    "Unable to update {} domain types, {} not implemented yet",
                    domain, collection,
                ),
            },
            "piloting" => match collection {
                "spacecrafts" => piloting.spacecrafts = read_file_quietly(path),
                collection => error!(
                    "Unable to update {} domain types, {} not implemented yet",
                    domain, collection,
                ),
            },
            "universe" => match collection {
                "beds" => universe.beds = read_file_quietly(path),
                "bridges" => universe.bridges = read_file_quietly(path),
                "characters" => universe.characters = read_file_quietly(path),
                "colonies" => universe.colonies = read_file_quietly(path),
                "constructions" => universe.constructions = read_file_quietly(path),
                "crops" => universe.crops = read_file_quietly(path),
                "decks" => universe.decks = read_file_quietly(path),
                "planets" => universe.planets = read_file_quietly(path),
                "planters" => universe.planters = read_file_quietly(path),
                "props" => universe.props = read_file_quietly(path),
                "spaceships" => universe.spaceships = read_file_quietly(path),
                "stations" => universe.stations = read_file_quietly(path),
                "storages" => universe.storages = read_file_quietly(path),
                "systems" => universe.systems = read_file_quietly(path),
                "trees" => universe.trees = read_file_quietly(path),
                "workbenches" => universe.workbenches = read_file_quietly(path),
                collection => error!(
                    "Unable to update {} domain types, {} not implemented yet",
                    domain, collection,
                ),
            },
            domain => error!(
                "Unable to update types, {} domain not implemented yet",
                domain
            ),
        }
    }

    pub fn save_state(&self, destination: &str) -> Result<(), DataError> {
        let root = &DataPath::new(&self.database_path).join(destination);
        info!("Save state to {}", root.display());

        let data = root.join("state").join("universe");
        let state = &self.universe.state;
        data.write("systems.yaml", &state.systems)?;
        data.write("planets.yaml", &state.planets)?;
        data.write("spaceships.yaml", &state.spaceships)?;
        data.write("stations.yaml", &state.stations)?;
        data.write("decks.yaml", &state.decks)?;
        data.write("colonizations.yaml", &state.colonizations)?;
        data.write("colonies.yaml", &state.colonies)?;
        data.write("characters.yaml", &state.characters)?;
        data.write("props.yaml", &state.props)?;
        data.write("crops.yaml", &state.crops)?;
        data.write("trees.yaml", &state.trees)?;
        data.write("groups.yaml", &state.groups)?;
        data.write("permissions.yaml", &state.permissions)?;
        data.write("storages.yaml", &state.storages)?;
        data.write("workbenches.yaml", &state.workbenches)?;
        data.write("constructions.yaml", &state.constructions)?;
        data.write("beds.yaml", &state.beds)?;
        data.write("planters.yaml", &state.planters)?;
        data.write("bridges.yaml", &state.bridges)?;

        let data = root.join("state").join("farming");
        let state = &self.farming.state;
        data.write("farmlands.yaml", &state.farmlands)?;
        data.write("plants.yaml", &state.plants)?;

        let data = root.join("state").join("inventory");
        let state = &self.inventory.state;
        data.write("containers.yaml", &state.containers)?;
        data.write("items.yaml", &state.items)?;

        let data = root.join("state").join("navigation");
        let state = &self.navigation.state;
        data.write("meshes.yaml", &state.meshes)?;
        data.write("navigators.yaml", &state.navigators)?;

        let data = root.join("state").join("piloting");
        let state = &self.piloting.state;
        data.write("spacecrafts.yaml", &state.spacecrafts)?;

        let data = root.join("state").join("physics");
        let state = &self.physics.state;
        data.write("spaces.yaml", &state.spaces)?;
        data.write("bodies.yaml", &state.bodies)?;
        data.write("barriers.yaml", &state.barriers)?;
        data.write("portals.yaml", &state.portals)?;

        Ok(())
    }

    pub fn update_state(&mut self, domain: &str, collection: &str, path: String) {
        let universe = &mut self.universe.state;
        let farming = &mut self.farming.state;
        let inventory = &mut self.inventory.state;
        let navigation = &mut self.navigation.state;
        let physics = &mut self.physics.state;
        let piloting = &mut self.piloting.state;

        match domain {
            "farming" => match collection {
                "farmlands" => farming.farmlands = read_file_quietly(path),
                "plants" => farming.plants = read_file_quietly(path),
                collection => error!(
                    "Unable to update {} domain state, {} not implemented yet",
                    domain, collection,
                ),
            },
            "inventory" => match collection {
                "items" => inventory.items = read_file_quietly(path),
                "containers" => inventory.containers = read_file_quietly(path),
                collection => error!(
                    "Unable to update {} domain state, {} not implemented yet",
                    domain, collection,
                ),
            },
            "navigation" => match collection {
                "navigators" => navigation.navigators = read_file_quietly(path),
                "meshes" => navigation.meshes = read_file_quietly(path),
                collection => error!(
                    "Unable to update {} domain state, {} not implemented yet",
                    domain, collection,
                ),
            },
            "physics" => match collection {
                "bodies" => physics.bodies = read_file_quietly(path),
                "barriers" => physics.barriers = read_file_quietly(path),
                "spaces" => physics.spaces = read_file_quietly(path),
                "portals" => physics.portals = read_file_quietly(path),
                "sensors" => physics.sensors = read_file_quietly(path),
                collection => error!(
                    "Unable to update {} domain state, {} not implemented yet",
                    domain, collection,
                ),
            },
            "piloting" => match collection {
                "spacecrafts" => piloting.spacecrafts = read_file_quietly(path),
                collection => error!(
                    "Unable to update {} domain state, {} not implemented yet",
                    domain, collection,
                ),
            },
            "universe" => match collection {
                "beds" => universe.beds = read_file_quietly(path),
                "bridges" => universe.bridges = read_file_quietly(path),
                "characters" => universe.characters = read_file_quietly(path),
                "colonies" => universe.colonies = read_file_quietly(path),
                "constructions" => universe.constructions = read_file_quietly(path),
                "crops" => universe.crops = read_file_quietly(path),
                "decks" => universe.decks = read_file_quietly(path),
                "planets" => universe.planets = read_file_quietly(path),
                "planters" => universe.planters = read_file_quietly(path),
                "props" => universe.props = read_file_quietly(path),
                "spaceships" => universe.spaceships = read_file_quietly(path),
                "stations" => universe.stations = read_file_quietly(path),
                "storages" => universe.storages = read_file_quietly(path),
                "systems" => universe.systems = read_file_quietly(path),
                "trees" => universe.trees = read_file_quietly(path),
                "workbenches" => universe.workbenches = read_file_quietly(path),
                "groups" => universe.groups = read_file_quietly(path),
                "permissions" => universe.permissions = read_file_quietly(path),
                "colonizations" => universe.colonizations = read_file_quietly(path),
                collection => error!(
                    "Unable to update {} domain state, {} not implemented yet",
                    domain, collection,
                ),
            },
            domain => error!(
                "Unable to update state, {} domain not implemented yet",
                domain
            ),
        }
    }
}
