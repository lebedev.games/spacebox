use serde::{Deserialize, Serialize};

use crate::game::{Message, TrustedMessage};
use crate::game::{PlayerBackend, PlayerId, PlayerRole};
use std::collections::HashMap;
use std::fmt::Debug;
use std::io::{BufReader, Read, Write};
use std::net::{SocketAddr, TcpListener, TcpStream, ToSocketAddrs};
use std::sync::mpsc::{channel, Receiver, RecvTimeoutError, Sender};
use std::sync::{Arc, RwLock};
use std::time::Duration;
use std::{io, thread};

use crate::game::Master;
use crate::game::{Event, Game};

#[derive(Debug, Clone, Deserialize)]
#[serde(tag = "$type")]
pub enum Inbound {
    Authorization { player: PlayerId, role: PlayerRole },
    Message { message: Message },
    Heartbeat,
}

#[derive(Debug, Clone, Serialize)]
#[serde(tag = "$type")]
pub enum Outbound {
    Events { events: Vec<Event> },
    Heartbeat,
}

#[derive(Debug, Clone)]
pub struct Authorization {
    pub player: PlayerId,
    pub role: PlayerRole,
}

pub struct NetworkPlayer {
    pub id: PlayerId,
    pub role: PlayerRole,
    pub outgoing_events: Sender<Vec<Event>>,
}

pub struct PlayerNetworkingBackend {
    pub players_authorization: Receiver<NetworkPlayer>,
    pub players_incoming_messages: Receiver<TrustedMessage>,
    pub players: HashMap<PlayerId, NetworkPlayer>,
    pub players_disconnected: Vec<PlayerId>,
}

impl PlayerBackend for PlayerNetworkingBackend {
    fn reload_game(&mut self) {
        /*
        info!("Reload network backend");
        let players: Vec<PlayerId> = self.players.keys().map(|key| key.clone()).collect();
        for player in players {
            self.send_events(&player, vec![Game::GameLoaded.into()]);
        }*/
    }

    fn receive_players_messages(&mut self) -> Vec<TrustedMessage> {
        let mut messages = Vec::new();
        while let Ok(message) = self.players_incoming_messages.try_recv() {
            messages.push(message);
        }
        messages
    }

    fn accept_players(&mut self) -> Vec<PlayerId> {
        let mut players = Vec::new();
        while let Ok(player) = self.players_authorization.try_recv() {
            players.push(player.id.clone());
            self.players.insert(player.id.clone(), player);
        }
        players
    }

    fn reject_players(&mut self) -> Vec<PlayerId> {
        let players = self.players_disconnected.clone();
        self.players_disconnected = Vec::new();
        for id in players.iter() {
            self.players.remove(id);
        }
        players
    }

    fn send_events(&mut self, id: &PlayerId, events: Vec<Event>) {
        match self.players.get(id) {
            Some(player) => {
                if let Err(_) = player.outgoing_events.send(events) {
                    error!(
                        "Unable to send player {:?} events, network connection lost",
                        id
                    );
                    self.players_disconnected.push(id.clone());
                }
            }
            None => error!("Unable to send player {:?} events, player not found", id),
        }
    }

    fn send_inside_data(&mut self, _: Arc<RwLock<Master>>) {}
}

impl PlayerNetworkingBackend {
    pub fn startup<A: 'static + ToSocketAddrs + Send + Debug>(address: A) -> Self {
        let (players_incoming_messages_sender, players_incoming_messages) = channel();
        let (players_authorization_sender, players_authorization) = channel();

        thread::spawn(move || {
            serve_network_players(
                address,
                players_authorization_sender,
                players_incoming_messages_sender,
            );
        });

        PlayerNetworkingBackend {
            players_authorization,
            players_incoming_messages,
            players: HashMap::new(),
            players_disconnected: Vec::new(),
        }
    }
}

pub fn serve_network_players<A: ToSocketAddrs + Debug>(
    address: A,
    players_authorization: Sender<NetworkPlayer>,
    players_incoming_messages: Sender<TrustedMessage>,
) {
    info!("Serve network player connections on {:?}", address);
    let listener = match TcpListener::bind(address) {
        Ok(listener) => listener,
        Err(error) => {
            error!(
                "Unable to server network players because of listener, {:?}",
                error
            );
            return;
        }
    };

    for stream in listener.incoming() {
        let stream = match stream {
            Ok(stream) => stream,
            Err(error) => {
                error!(
                    "Unable to server network players because of incoming stream, {:?}",
                    error
                );
                return;
            }
        };

        stream
            .set_read_timeout(Some(Duration::from_secs(5)))
            .unwrap();

        stream
            .set_write_timeout(Some(Duration::from_secs(5)))
            .unwrap();

        let mut reader = BufReader::new(stream.try_clone().unwrap());

        // authorization blocks new player connections, should be super fast
        let authorization = match authorize_player(&mut reader, stream.peer_addr()) {
            Some(authorization) => authorization,
            None => {
                info!(
                    "Player connection peer={:?} unauthorized",
                    stream.peer_addr()
                );
                continue;
            }
        };

        let (outgoing_events, outgoing_events_receiver) = channel();
        let players_incoming_messages = players_incoming_messages.clone();

        let player = NetworkPlayer {
            id: authorization.player,
            role: authorization.role,
            outgoing_events,
        };

        let trusted_player_id = player.id.clone();
        thread::spawn(move || {
            send_player_events(trusted_player_id, outgoing_events_receiver, stream)
        });

        let trusted_player_id = player.id.clone();
        thread::spawn(move || {
            receive_player_messages(trusted_player_id, players_incoming_messages, reader)
        });

        players_authorization.send(player).unwrap();
    }
}

fn authorize_player(
    reader: &mut BufReader<TcpStream>,
    peer: io::Result<SocketAddr>,
) -> Option<Authorization> {
    info!("Begin player authorization from {:?}", peer);

    let mut header_buffer = [0_u8; 2];
    if let Err(error) = reader.read_exact(&mut header_buffer) {
        error!(
            "Unable to authorize player because of header read, {}",
            error
        );
        return None;
    }
    let body_length = u16::from_be_bytes(header_buffer) as usize;

    let mut body_buffer = vec![0_u8; body_length];
    if let Err(error) = reader.read_exact(&mut body_buffer) {
        error!("Unable to authorize player because of body read, {}", error);
        return None;
    }

    let data = match std::str::from_utf8(&body_buffer) {
        Ok(data) => data,
        Err(error) => {
            error!(
                "Unable to authorize player because of body encoding, {}",
                error
            );
            return None;
        }
    };

    match serde_json::from_str(data) {
        Ok(message) => match message {
            Inbound::Authorization { player, role } => {
                let authorization = Authorization { player, role };
                info!(
                    "End player authorization from {:?}, {:?}",
                    peer, &authorization
                );
                Some(authorization)
            }
            _ => {
                error!(
                    "Unable to authorize player because of unexpected inbound, {:?}",
                    message
                );
                None
            }
        },
        Err(error) => {
            error!(
                "Unable to authorize player because of deserialization, {}",
                error
            );
            None
        }
    }
}

fn receive_player_messages(
    player: PlayerId,
    inbox: Sender<TrustedMessage>,
    mut reader: BufReader<TcpStream>,
) {
    info!("Begin player {:?} messages receiving thread", &player);

    loop {
        let mut header_buffer = [0_u8; 2];
        if let Err(error) = reader.read_exact(&mut header_buffer) {
            error!(
                "Unable to receive player message because of header read, {}",
                error
            );
            break;
        }
        let body_length = u16::from_be_bytes(header_buffer) as usize;

        let mut body_buffer = vec![0; body_length];
        if let Err(error) = reader.read_exact(body_buffer.as_mut_slice()) {
            error!(
                "Unable to receive player message because of body read, {}",
                error
            );
            break;
        }

        let inbound: Inbound = match serde_json::from_slice(body_buffer.as_slice()) {
            Ok(inbound) => inbound,
            Err(error) => {
                error!(
                    "Unable to receive player message because of deserialization, {}",
                    error
                );
                break;
            }
        };

        let message = match inbound {
            Inbound::Authorization { .. } => continue,
            Inbound::Heartbeat => continue,
            Inbound::Message { message } => message,
        };

        if inbox.send(TrustedMessage(player.clone(), message)).is_err() {
            break;
        }
    }

    info!("End player {:?} messages receiving thread", &player);
}

fn send_player_events(
    player: PlayerId,
    player_events: Receiver<Vec<Event>>,
    mut writer: TcpStream,
) {
    let interval = Duration::from_secs(1);
    info!(
        "Begin player {:?} events sending thread, heartbeat: {:?}",
        &player, &interval
    );

    loop {
        let outbound = match player_events.recv_timeout(interval) {
            Ok(events) => Outbound::Events { events },
            Err(receiving) => match receiving {
                RecvTimeoutError::Timeout => Outbound::Heartbeat,
                RecvTimeoutError::Disconnected => break,
            },
        };

        match serde_json::to_vec(&outbound) {
            Ok(body) => {
                let header = (body.len() as u16).to_be_bytes();

                if let Err(error) = writer.write_all(&header) {
                    error!(
                        "Unable to send player events because of header write, {}",
                        error
                    );
                    break;
                }

                if let Err(error) = writer.write_all(&body) {
                    error!(
                        "Unable to send player events because of body write, {}",
                        error
                    );
                    break;
                }
            }
            Err(error) => {
                error!(
                    "Unable to send player events because of serialization, {}",
                    error
                )
            }
        }
    }

    info!("End player {:?} events sending thread", &player);
}
