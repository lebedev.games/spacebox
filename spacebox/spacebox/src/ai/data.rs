use crate::ai::database::AiDatabase;

use crate::persistence::{DataError, DataPath};

pub fn load_ai_database(_data: &DataPath) -> Result<AiDatabase, DataError> {
    Ok(AiDatabase {
        modules: vec![],
        profiles: vec![],
    })
    /*
    let module_entries: Vec<ModuleDefinitionData> = data.read("modules.yaml")?;
    let definitions_prop_related: Vec<DefinitionData<PropRelatedDecision>> =
        data.read("definitions/prop_related.yaml")?;
    let mut modules = vec![];
    for module in module_entries {
        let prop_related_entries: Vec<DefinitionData<PropRelatedDecision>> = definitions_prop_related.iter().filter(|def| def.mo)
        let mut prop_related = vec![];
        for definition in prop_related_entries {
            let considerations: Vec<Consideration<PropRelatedInput>> = load_from_query(
                connection,
                "ai.character_considerations_prop_related",
                "where definition = ?",
                [definition.key],
            )?;
            prop_related.push(Definition { considerations, decision: definition.decision })
        }

        let crop_related_entries: Vec<DefinitionData<CropRelatedDecision>> = load_from_query(
            connection,
            "ai.character_definitions_crop_related",
            "where module = ?",
            [&module.key.0],
        )?;
        let mut crop_related = vec![];
        for definition in crop_related_entries {
            let considerations: Vec<Consideration<CropRelatedInput>> = load_from_query(
                connection,
                "ai.character_considerations_crop_related",
                "where definition = ?",
                [definition.key],
            )?;
            crop_related.push(Definition { considerations, decision: definition.decision })
        }

        let bed_related_entries: Vec<DefinitionData<BedRelatedDecision>> = load_from_query(
            connection,
            "ai.character_definitions_bed_related",
            "where module = ?",
            [&module.key.0],
        )?;
        let mut bed_related = vec![];
        for definition in bed_related_entries {
            let considerations: Vec<Consideration<BedRelatedInput>> = load_from_query(
                connection,
                "ai.character_considerations_bed_related",
                "where definition = ?",
                [definition.key],
            )?;
            bed_related.push(Definition { considerations, decision: definition.decision })
        }

        modules.push(ModuleDefinition {
            key: module.key.clone(),
            priority: module.priority,
            prop_related,
            crop_related,
            bed_related,
        })
    }
    let profiles = load_from(connection, "ai.character_agents")?;

    Ok(AiDatabase { modules, profiles })*/
}
