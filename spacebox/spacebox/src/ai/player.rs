use std::collections::HashMap;
use std::sync::mpsc::{channel, Receiver, Sender};
use std::sync::{Arc, RwLock};
use std::time::Instant;

use crate::game::{Message, TrustedMessage};
use serde::{Deserialize, Serialize};
use universe::{CharacterId, GroupId};

use crate::ai::character::execute;
use crate::ai::data::load_ai_database;
use crate::ai::database::AiDatabase;
use crate::ai::metrics::AiMetrics;
use crate::ai::models::{CharacterAgent, ModuleDefinition, PlayerAgent};
use crate::game::Master;
use crate::game::{Appearance, Event, Game};
use crate::game::{PlayerBackend, PlayerId};
use crate::persistence::DataPath;

pub struct MultithreadingAiBackend {
    pub database: AiDatabase,
    pub counter: usize,
    pub players_authorization: Receiver<PlayerAgent>,
    pub players_authorization_trigger: Sender<PlayerAgent>,
    pub players_incoming_messages: Receiver<TrustedMessage>,
    pub players_incoming_messages_trigger: Sender<TrustedMessage>,
    pub players: HashMap<PlayerId, PlayerAgent>,
    pub players_disconnected: Vec<PlayerId>,
    pub characters: Vec<CharacterAgent>,
    pub metrics: AiMetrics,
}

impl PlayerBackend for MultithreadingAiBackend {
    fn reload_game(&mut self) {
        info!("Reload AI backend");
        self.players = HashMap::new();
        self.characters = vec![];
        self.metrics = AiMetrics::default();
    }

    fn receive_players_messages(&mut self) -> Vec<TrustedMessage> {
        let mut messages = Vec::new();
        while let Ok(message) = self.players_incoming_messages.try_recv() {
            messages.push(message);
        }
        messages
    }

    fn accept_players(&mut self) -> Vec<PlayerId> {
        let mut players = Vec::new();
        while let Ok(player) = self.players_authorization.try_recv() {
            players.push(player.id.clone());
            self.players.insert(player.id.clone(), player);
        }
        players
    }

    fn reject_players(&mut self) -> Vec<PlayerId> {
        vec![]
    }

    fn send_events(&mut self, _player: &PlayerId, events: Vec<Event>) {
        self.metrics.events_consumed.increase(events.len());

        for event in events {
            match event {
                Event::Game(event) => match event {
                    Game::GameLoaded => {
                        info!("Game loaded, recreate AI units");
                        self.players = HashMap::new();
                        self.characters = vec![];
                    }
                    _ => {}
                },
                Event::Appearance(event) => match event {
                    Appearance::CharacterAppeared { id, group, key, .. } => {
                        if let Some(player) = self.get_player_by_group(group) {
                            let player = player.id.clone();
                            info!(
                                "Create new character {:?} AI agent of player {:?} group {:?}",
                                id, &player, group
                            );
                            self.characters.push(CharacterAgent {
                                id,
                                player,
                                group,
                                key,
                                last_update: Instant::now(),
                                last_decision: None,
                            })
                        }
                    }
                    _ => {}
                },
                Event::Disappearance(_) => {}
                Event::Universe(_) => {}
                Event::Physics(_) => {}
                Event::Piloting(_) => {}
                Event::Farming(_) => {}
                Event::Inventory(_) => {}
                Event::Navigation(_) => {}
            }
        }
    }

    fn send_inside_data(&mut self, master: Arc<RwLock<Master>>) {
        let master = match master.read() {
            Ok(master) => master,
            Err(error) => {
                error!("Unable to get inside information, {}", error);
                return;
            }
        };

        for group in master.universe.state.groups.iter() {
            /*if self.get_player_by_group(group.id).is_none() && !group.playable {
                self.counter += 1;
                let id = format!("agent:{}+{}", group.name, self.counter);
                let id = PlayerId(id);
                let player = PlayerAgent {
                    id: id.clone(),
                    group: group.id,
                };
                if self.players_authorization_trigger.send(player).is_err() {
                    error!("Unable to authorize player, ai backend tear down")
                }
                let message = TrustedMessage(
                    id.clone(),
                    JoinGame {
                        group: Some(group.id),
                        observer: false,
                    },
                );
                if self
                    .players_incoming_messages_trigger
                    .send(message)
                    .is_err()
                {
                    error!("Unable to set up ai player")
                }
                let message = TrustedMessage(id.clone(), InspectGame);
                if self
                    .players_incoming_messages_trigger
                    .send(message)
                    .is_err()
                {
                    error!("Unable to set up ai player")
                }
            }*/
        }

        self.metrics.player_agents.set(self.players.len());
        self.metrics.character_agents.set(self.characters.len());

        for agent in self.characters.iter_mut() {
            let character = match master.universe.get_character(agent.id) {
                Ok(character) => character,
                Err(error) => {
                    error!("Unable to update agent, {:?}", error);
                    continue;
                }
            };

            let profile = match self
                .database
                .profiles
                .iter()
                .find(|x| x.key == character.key)
            {
                Some(profile) => profile,
                None => {
                    debug!(
                        "Unable to update character {:?} {:?} agent, AI profile not found",
                        character.id, &character.key
                    );
                    continue;
                }
            };

            let modules: Vec<&ModuleDefinition> = self
                .database
                .modules
                .iter()
                .filter(|module| profile.modules.contains(&module.key))
                .collect();

            let decision = match MultithreadingAiBackend::make_decision(
                &mut self.metrics,
                character,
                &master,
                modules,
            ) {
                Ok(decision) => decision,
                Err(error) => {
                    error!("Unable to make decision, {:?}", error);
                    None
                }
            };

            agent.last_decision = decision.clone();

            if let Some(decision) = decision {
                match execute(character, decision, &master) {
                    Ok(Some(action)) => {
                        let player = agent.player.clone();
                        let message = TrustedMessage(
                            player,
                            Message::Action {
                                character: character.id,
                                action,
                            },
                        );
                        self.metrics.messages_sent.increment();
                        if let Err(error) = self.players_incoming_messages_trigger.send(message) {
                            error!("Unable to send message, {:?}", error)
                        }
                    }
                    Ok(None) => {}
                    Err(error) => error!("Unable to execute {:?}", error),
                }
            }

            let last_update = Instant::now();
            agent.last_update = last_update;
        }
    }
}

impl MultithreadingAiBackend {
    pub fn get_player_by_group(&self, group: GroupId) -> Option<&PlayerAgent> {
        self.players.values().find(|player| player.group == group)
    }

    pub fn get_character_agent(&self, id: CharacterId) -> Result<&CharacterAgent, AiError> {
        self.characters
            .iter()
            .find(|character| character.id == id)
            .ok_or(AiError::CharacterAgentNotFound(id))
    }

    pub fn startup(data_path: &str) -> Self {
        let (players_incoming_messages_trigger, players_incoming_messages) = channel();
        let (players_authorization_trigger, players_authorization) = channel();

        let data = DataPath::new(data_path);
        let database = match load_ai_database(&data) {
            Ok(database) => database,
            Err(error) => panic!("Unable to startup ai backend, {:?}", error),
        };

        MultithreadingAiBackend {
            database,
            counter: 0,
            players_authorization,
            players_authorization_trigger,
            players_incoming_messages,
            players_incoming_messages_trigger,
            players: HashMap::new(),
            players_disconnected: Vec::new(),
            characters: vec![],
            metrics: AiMetrics::default(),
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(tag = "$type")]
pub enum AiError {
    CharacterAgentNotFound(CharacterId),
}
