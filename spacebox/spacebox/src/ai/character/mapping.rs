pub trait Normalized {
    fn normalize(&self) -> f32;
}

impl Normalized for bool {
    fn normalize(&self) -> f32 {
        if *self {
            1.0
        } else {
            0.0
        }
    }
}
