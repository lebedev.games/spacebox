use navigation::RouteStatus::{RouteComplete, RouteInProgress, RouteNotStarted};
use universe::Activity::Sleeping;
use universe::TaskStatus::{OtherEnding, OtherRunning, TaskCanBeStarted, TaskInProgress};
use universe::{Bed, BedId, Character};

use crate::ai::character::{ExecutionError, Normalized};
use crate::ai::models::{BedRelatedDecision, BedRelatedInput};
use crate::game::Action::{CancelActivity, MoveCharacter, UseBed};
use crate::game::ActionError;
use crate::game::{Action, Master};

pub fn map_bed_input(
    input: BedRelatedInput,
    character: &Character,
    bed: &Bed,
    master: &Master,
) -> Result<f32, ActionError> {
    let physics = &master.physics;
    let navigation = &master.navigation;

    let body = physics.get_body(character.body)?;
    let barrier = physics.get_barrier(bed.barrier)?;

    let input = match input {
        BedRelatedInput::MyHealth => character.health / character.health_limit,
        BedRelatedInput::MyEnergy => character.energy / character.energy_limit,
        BedRelatedInput::MyStress => character.stress / character.stress_limit,
        BedRelatedInput::DistanceToBed => {
            let body = physics.get_body(character.body)?;
            body.position.distance(bed.entry_position(barrier)) / 100.0
        }
        BedRelatedInput::BedInUse => bed.user.is_some().normalize(),
        BedRelatedInput::CanBeReached => {
            let mesh = body.space.0.into();
            navigation
                .can_be_reached(mesh, bed.entry(barrier))
                .normalize()
        }
    };

    Ok(input)
}

pub fn execute_bed_related_decision(
    character: &Character,
    bed: BedId,
    decision: BedRelatedDecision,
    master: &Master,
) -> Result<Option<Action>, ExecutionError> {
    let universe = &master.universe;
    let physics = &master.physics;
    let navigation = &master.navigation;
    let bed = universe.get_bed(bed)?;
    let barrier = physics.get_barrier(bed.barrier)?;

    let action = match decision {
        BedRelatedDecision::GoToBed => match character.validate_task(Sleeping { bed: bed.id }) {
            OtherRunning => CancelActivity,
            TaskCanBeStarted => {
                match navigation.validate_route(character.navigator, bed.entry(barrier))? {
                    RouteComplete => UseBed { bed: bed.id },
                    RouteInProgress => return Ok(None),
                    RouteNotStarted => MoveCharacter {
                        position: bed.entry(barrier).into(),
                    },
                }
            }
            TaskInProgress => return Ok(None),
            OtherEnding => return Ok(None),
        },
    };

    Ok(Some(action))
}
