use farming::PlantState;
use navigation::PathPoint;
use navigation::RouteStatus::{RouteComplete, RouteInProgress, RouteNotStarted};
use universe::Activity::CropHarvesting;
use universe::TaskStatus::{OtherEnding, OtherRunning, TaskCanBeStarted, TaskInProgress};
use universe::{Character, Crop, CropId};

use crate::ai::character::ExecutionError;
use crate::ai::models::{CropRelatedDecision, CropRelatedInput};
use crate::game::Action::{CancelActivity, HarvestCrop, MoveCharacter};
use crate::game::ActionError;
use crate::game::{Action, Master};

pub fn map_crop_input(
    input: CropRelatedInput,
    character: &Character,
    crop: &Crop,
    master: &Master,
) -> Result<f32, ActionError> {
    let farming = &master.farming;
    let physics = &master.physics;

    let input = match input {
        CropRelatedInput::MyHealth => character.health / character.health_limit,
        CropRelatedInput::MyEnergy => character.energy / character.energy_limit,
        CropRelatedInput::MyStress => character.stress / character.stress_limit,
        CropRelatedInput::DistanceToCrop => {
            let char = physics.get_body(character.body)?;
            let crop = physics.get_barrier(crop.barrier)?;
            char.position.distance(crop.position) / 100.0
        }
        CropRelatedInput::CropReadyToHarvest => {
            let plant = farming.get_plant(crop.plant)?;
            match plant.state {
                PlantState::Harvest { .. } => 1.0,
                _ => 0.0,
            }
        }
    };

    Ok(input)
}

pub fn execute_crop_related_decision(
    character: &Character,
    crop: CropId,
    decision: CropRelatedDecision,
    master: &Master,
) -> Result<Option<Action>, ExecutionError> {
    let universe = &master.universe;
    let physics = &master.physics;
    let navigation = &master.navigation;

    let crop = universe.get_crop(crop)?;
    let crop_position = physics.get_barrier(crop.barrier)?.position;

    let action = match decision {
        CropRelatedDecision::HarvestCrop => {
            match character.validate_task(CropHarvesting { crop: crop.id }) {
                OtherRunning => CancelActivity,
                TaskCanBeStarted => {
                    match navigation.validate_route(
                        character.navigator,
                        PathPoint::new(crop_position.x, crop_position.y),
                    )? {
                        RouteComplete => HarvestCrop { crop: crop.id },
                        RouteInProgress => {
                            return Ok(None);
                        }
                        RouteNotStarted => MoveCharacter {
                            position: crop_position.into(),
                        },
                    }
                }
                TaskInProgress => return Ok(None),
                OtherEnding => return Ok(None),
            }
        }
    };

    Ok(Some(action))
}
