use universe::{Character, Prop, PropId};

use crate::ai::character::ExecutionError;
use crate::ai::models::{PropRelatedDecision, PropRelatedInput};
use crate::game::ActionError;
use crate::game::{Action, Master};

pub fn map_prop_input(
    input: PropRelatedInput,
    character: &Character,
    _prop: &Prop,
    _master: &Master,
) -> Result<f32, ActionError> {
    let input = match input {
        PropRelatedInput::MyHealth => character.health / character.health_limit,
        PropRelatedInput::MyEnergy => character.energy / character.energy_limit,
        PropRelatedInput::MyStress => character.stress / character.stress_limit,
    };

    Ok(input)
}

pub fn execute_prop_related_decision(
    _character: &Character,
    _prop: PropId,
    decision: PropRelatedDecision,
    _master: &Master,
) -> Result<Option<Action>, ExecutionError> {
    match decision {
        PropRelatedDecision::Nothing => Ok(None),
    }
}
