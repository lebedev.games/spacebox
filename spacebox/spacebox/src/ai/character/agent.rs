use navigation::NavigationError;
use physics::PhysicsError;
use universe::{Character, UniverseError};

use crate::ai::character::{
    execute_bed_related_decision, execute_crop_related_decision, execute_prop_related_decision,
};
use crate::ai::models::CharacterDecision;
use crate::game::ActionError;
use crate::game::{Action, Master};

pub fn execute(
    character: &Character,
    decision: CharacterDecision,
    master: &Master,
) -> Result<Option<Action>, ExecutionError> {
    match decision {
        CharacterDecision::Prop { prop, decision } => {
            execute_prop_related_decision(character, prop, decision, master)
        }
        CharacterDecision::Bed { bed, decision } => {
            execute_bed_related_decision(character, bed, decision, master)
        }
        CharacterDecision::Crop { crop, decision } => {
            execute_crop_related_decision(character, crop, decision, master)
        }
    }
}

#[derive(Debug)]
pub enum ExecutionError {
    Action(ActionError),
}

impl From<ActionError> for ExecutionError {
    fn from(error: ActionError) -> Self {
        Self::Action(error)
    }
}

impl From<UniverseError> for ExecutionError {
    fn from(error: UniverseError) -> Self {
        Self::Action(ActionError::Universe(error))
    }
}

impl From<NavigationError> for ExecutionError {
    fn from(error: NavigationError) -> Self {
        Self::Action(ActionError::Navigation(error))
    }
}

impl From<PhysicsError> for ExecutionError {
    fn from(error: PhysicsError) -> Self {
        Self::Action(ActionError::Physics(error))
    }
}
