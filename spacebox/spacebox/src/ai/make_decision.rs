use universe::Character;

use crate::ai::character::{map_bed_input, map_crop_input, map_prop_input};
use crate::ai::models::{CharacterDecision, ModuleDefinition};
use crate::ai::{AiMetrics, MultithreadingAiBackend};
use crate::game::ActionError;
use crate::game::Master;

impl MultithreadingAiBackend {
    pub fn make_decision(
        metrics: &mut AiMetrics,
        character: &Character,
        master: &Master,
        modules: Vec<&ModuleDefinition>,
    ) -> Result<Option<CharacterDecision>, ActionError> {
        let mut best = None;
        let mut best_utility = 0.0;

        metrics.processed_modules.increase(modules.len());

        if modules.len() == 0 {
            debug!(
                "Unable to make decision for character {:?} {:?}, no AI modules",
                character.id, character.key
            );
        }

        let body = master.physics.get_body(character.body)?;
        let space = character.space(body);

        for module in modules {
            metrics
                .processed_definitions
                .increase(module.prop_related.len());
            for definition in &module.prop_related {
                for prop in master.get_space_props(space)? {
                    let mut utility = 1.0;

                    metrics
                        .processed_considerations
                        .increase(definition.considerations.len());
                    for consideration in &definition.considerations {
                        let input =
                            match map_prop_input(consideration.input, character, prop, master) {
                                Ok(input) => input,
                                Err(error) => {
                                    error!(
                                        "Unable to map input {:?}, {:?}",
                                        consideration.input, error
                                    );
                                    0.0
                                }
                            };
                        let score = consideration.curve.evaluate(input);
                        utility *= score;
                    }
                    if utility > best_utility {
                        best = Some(CharacterDecision::Prop {
                            prop: prop.id,
                            decision: definition.decision,
                        });
                        best_utility = utility;
                    }
                }
            }

            metrics
                .processed_definitions
                .increase(module.crop_related.len());
            for definition in &module.crop_related {
                for crop in master.get_space_crops(space)? {
                    let mut utility = 1.0;

                    metrics
                        .processed_considerations
                        .increase(definition.considerations.len());
                    for consideration in &definition.considerations {
                        let input =
                            match map_crop_input(consideration.input, character, crop, master) {
                                Ok(input) => input,
                                Err(error) => {
                                    error!(
                                        "Unable to map input {:?}, {:?}",
                                        consideration.input, error
                                    );
                                    0.0
                                }
                            };
                        let score = consideration.curve.evaluate(input);
                        utility *= score;
                    }
                    if utility > best_utility {
                        best = Some(CharacterDecision::Crop {
                            crop: crop.id,
                            decision: definition.decision,
                        });
                        best_utility = utility;
                    }
                }
            }

            metrics
                .processed_definitions
                .increase(module.bed_related.len());
            for definition in &module.bed_related {
                for bed in &master.get_space_beds(space)? {
                    let mut utility = 1.0;

                    metrics
                        .processed_considerations
                        .increase(definition.considerations.len());
                    for consideration in &definition.considerations {
                        let input = match map_bed_input(consideration.input, character, bed, master)
                        {
                            Ok(input) => input,
                            Err(error) => {
                                error!(
                                    "Unable to map input {:?}, {:?}",
                                    consideration.input, error
                                );
                                0.0
                            }
                        };
                        let score = consideration.curve.evaluate(input);
                        utility *= score;
                    }
                    if utility > best_utility {
                        best = Some(CharacterDecision::Bed {
                            bed: bed.id,
                            decision: definition.decision,
                        });
                        best_utility = utility;
                    }
                }
            }
        }

        Ok(best)
    }
}
