#[cfg(test)]
mod tests {
    use env_logger::Env;
    use log::LevelFilter;

    use crate::ai::data::load_ai_database;
    use crate::persistence::DataPath;

    #[test]
    fn test_persistence() {
        let _ = env_logger::Builder::from_env(Env::default())
            .filter_level(LevelFilter::Debug)
            .try_init();

        let connection = DataPath::new("./database/game");
        load_ai_database(&connection).unwrap();
        /*
        let database = AiDatabase {
            modules: vec![ModuleDefinition {
                key: ModuleKey("Default Behaviour".into()),
                priority: 1.0,
                self_related: vec![Definition {
                    considerations: vec![Consideration {
                        input: SelfRelatedInput::MyHealth,
                        curve: Curve { values: vec![0.0] },
                    }],
                    decision: SelfRelatedDecision::EatFood,
                }],
                prop_related: vec![Definition {
                    considerations: vec![Consideration {
                        input: PropRelatedInput::MyHealth,
                        curve: Curve { values: vec![0.0] },
                    }],
                    decision: PropRelatedDecision::DropUselessGoods,
                }],
                bed_related: vec![Definition {
                    considerations: vec![Consideration {
                        input: BedRelatedInput::MyHealth,
                        curve: Curve { values: vec![0.0] },
                    }],
                    decision: BedRelatedDecision::GoToBed,
                }],
            }],
            profiles: vec![CharacterAgentProfile {
                key: CharacterKey("boris".into()),
                modules: vec![ModuleKey("A".into()), ModuleKey("B".into())],
            }],
        };
        */
    }
}
