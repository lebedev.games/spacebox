use crate::ai::models::{CharacterAgentProfile, ModuleDefinition};

#[derive(Debug, Default)]
pub struct AiDatabase {
    pub modules: Vec<ModuleDefinition>,
    pub profiles: Vec<CharacterAgentProfile>,
}
