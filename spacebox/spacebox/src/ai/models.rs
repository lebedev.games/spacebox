use std::time::Instant;

use serde::{Deserialize, Serialize};
use universe::{BedId, CharacterId, CharacterKey, CropId, GroupId, PropId};

use crate::game::PlayerId;

#[derive(Debug, Serialize, Deserialize)]
pub struct Curve {
    pub values: Vec<f32>,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum PropRelatedInput {
    MyHealth,
    MyEnergy,
    MyStress,
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub enum PropRelatedDecision {
    Nothing,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum BedRelatedInput {
    MyHealth,
    MyEnergy,
    MyStress,
    DistanceToBed,
    BedInUse,
    CanBeReached,
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub enum BedRelatedDecision {
    GoToBed,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum CropRelatedInput {
    MyHealth,
    MyEnergy,
    MyStress,
    DistanceToCrop,
    CropReadyToHarvest,
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub enum CropRelatedDecision {
    HarvestCrop,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum CharacterDecision {
    Prop {
        prop: PropId,
        decision: PropRelatedDecision,
    },
    Bed {
        bed: BedId,
        decision: BedRelatedDecision,
    },
    Crop {
        crop: CropId,
        decision: CropRelatedDecision,
    },
}

#[derive(Debug)]
pub struct CharacterAgent {
    pub id: CharacterId,
    pub key: CharacterKey,
    pub player: PlayerId,
    pub group: GroupId,
    pub last_update: Instant,
    pub last_decision: Option<CharacterDecision>,
}

#[derive(Debug)]
pub struct PlayerAgent {
    pub id: PlayerId,
    pub group: GroupId,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Consideration<I> {
    pub input: I,
    pub curve: Curve,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct DefinitionData<D> {
    pub key: String,
    pub decision: D,
}

#[derive(Debug)]
pub struct Definition<I, D> {
    pub considerations: Vec<Consideration<I>>,
    pub decision: D,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct ModuleKey(pub String);

#[derive(Debug, Serialize, Deserialize)]
pub struct ModuleDefinitionData {
    pub key: ModuleKey,
    pub priority: f32,
}

#[derive(Debug)]
pub struct ModuleDefinition {
    pub key: ModuleKey,
    pub priority: f32,
    pub prop_related: Vec<Definition<PropRelatedInput, PropRelatedDecision>>,
    pub crop_related: Vec<Definition<CropRelatedInput, CropRelatedDecision>>,
    pub bed_related: Vec<Definition<BedRelatedInput, BedRelatedDecision>>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct CharacterAgentProfile {
    pub key: CharacterKey,
    pub modules: Vec<ModuleKey>,
}

impl Curve {
    pub fn evaluate(&self, time: f32) -> f32 {
        let values = &self.values;

        if time <= 0.0 {
            return values[0];
        }
        if time >= 1.0 {
            return values[values.len() - 1];
        }

        let steps = (values.len() - 1) as f32;
        let step = time * steps;
        let next = step + 1.0;

        // round
        let step = step as usize;
        let next = next as usize;

        let step_time = 1.0 / steps;
        let delta = time - step_time * (step as f32);

        let interpolation = values[step] + (values[next] - values[step]) * delta / step_time;

        // clamp
        interpolation.max(0.0).min(1.0)
    }
}
