### Models

![./readme/fig1.png](readme/fig1.png) 

#### Decision

An individual behaviour of agent related to game entity.

#### Consideration

Each of decisions has one or many considerations. 
Consideration represents as response curve, the mathematical model which 
evaluates utility of decision. 

#### Input

Each consideration has input related to game entity. 
Because response curve x range runs from 0 to 1, input value should be normalized.
 
 