use serde::{Deserialize, Serialize};

#[derive(Default, Debug, Clone, Copy, Deserialize, Serialize)]
pub struct Counter(pub usize);

impl Counter {
    pub fn increment(&mut self) {
        self.increase(1)
    }

    pub fn increase(&mut self, value: usize) {
        self.0 += value;
    }
}

#[derive(Default, Debug, Clone, Copy, Deserialize, Serialize)]
pub struct Gauge(pub i64);

impl Gauge {
    pub fn set(&mut self, value: usize) {
        self.0 = value as i64;
    }
}

#[derive(Default, Debug, Clone, Deserialize, Serialize)]
pub struct AiMetrics {
    pub events_consumed: Counter,
    pub player_agents: Gauge,
    pub character_agents: Gauge,
    pub messages_sent: Counter,
    pub processed_modules: Counter,
    pub processed_considerations: Counter,
    pub processed_definitions: Counter,
}
