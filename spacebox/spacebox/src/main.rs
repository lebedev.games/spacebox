#[macro_use]
extern crate log;

use std::fs;
use std::sync::mpsc::channel;
use std::sync::{Arc, RwLock};
use std::thread::sleep;
use std::time::{Duration, Instant};

use log::LevelFilter;
use log4rs::append::console::ConsoleAppender;
use log4rs::append::file::FileAppender;
use log4rs::config::{Appender, Root};
use log4rs::Config;
use notify::{watcher, RecursiveMode, Watcher};

use crate::ai::MultithreadingAiBackend;
use crate::game::Game;
use crate::game::PlayerBackend;
use crate::game::{detect_changes, GameStatus, Master};
use crate::network::PlayerNetworkingBackend;

mod ai;
mod game;
mod network;
mod persistence;

fn define_version() {
    let version = match fs::read_to_string("./VERSION") {
        Ok(version) => version,
        Err(_) => "undefined".into(),
    };
    let build = match fs::read_to_string("./BUILD") {
        Ok(build) => build,
        Err(_) => "undefined".into(),
    };
    info!("Server version: {}, build: {}", version, build);
}

fn main() {
    let stdout = ConsoleAppender::builder().build();
    let file = FileAppender::builder().build("spacebox.log").unwrap();
    let config = Config::builder()
        .appender(Appender::builder().build("stdout", Box::new(stdout)))
        .appender(Appender::builder().build("file", Box::new(file)))
        .build(
            Root::builder()
                .appender("stdout")
                .appender("file")
                .build(LevelFilter::Info),
        )
        .unwrap();
    log4rs::init_config(config).unwrap();

    define_version();
    let database_path = "../../database";
    let database_path = fs::canonicalize(database_path).unwrap();

    let interval = Duration::from_secs_f32(0.5);
    info!(
        "Watch file changes in {} with interval {:?}",
        database_path.display(),
        &interval
    );
    let (changes_sender, watched_files) = channel();
    let mut watcher = watcher(changes_sender, interval).unwrap();
    watcher
        .watch(&database_path, RecursiveMode::Recursive)
        .unwrap();

    let master = Master::new(database_path.clone());
    let shared_master = Arc::new(RwLock::new(master));

    let player_networking = &mut PlayerNetworkingBackend::startup("0.0.0.0:8087");
    let multithreading_ai =
        &mut MultithreadingAiBackend::startup(&database_path.display().to_string());

    let mut time = Instant::now();

    let mut host = None;

    loop {
        player_networking.send_inside_data(shared_master.clone());
        multithreading_ai.send_inside_data(shared_master.clone());

        let elapsed_time = time.elapsed();
        time = Instant::now();

        let mut master = shared_master.write().unwrap();

        let changes = detect_changes(&watched_files.try_iter().collect(), &master.database_path);
        if !changes.is_empty() {
            master.handle_changes(changes);
        }

        // remove players left game
        let mut rejected = vec![];
        let network_players = player_networking.reject_players();
        if let Some(player) = host.as_ref() {
            if network_players.contains(player) {
                info!("Host disconnected, wait new one");
                host = None;
            }
        }
        rejected.extend(network_players);
        rejected.extend(multithreading_ai.reject_players());
        master
            .players
            .retain(|player| !rejected.contains(&player.id));

        // add new players
        let new_network_players = player_networking.accept_players();
        if host.is_none() && !new_network_players.is_empty() {
            let player = new_network_players[0].clone();
            info!("Now host of this server is {:?}", player);
            host = Some(player);
        }
        master.accept_players(new_network_players, 0);
        master.accept_players(multithreading_ai.accept_players(), 1);

        // update game
        let mut messages = vec![];
        messages.extend(player_networking.receive_players_messages());
        messages.extend(multithreading_ai.receive_players_messages());

        master.handle_messages(messages);

        match master.status.clone() {
            GameStatus::PendingLoad { source } => match master.load_game(&source) {
                Ok(_) => {
                    player_networking.reload_game();
                    multithreading_ai.reload_game();
                    master.status = GameStatus::Running;
                }
                Err(error) => {
                    error!("Unable to load game from {}, {:?}", source, error);
                    master.status = GameStatus::Running;
                    master.update_game(elapsed_time);
                }
            },
            GameStatus::Running => master.update_game(elapsed_time),
            GameStatus::Paused => {}
        }

        for mut player in master.players.iter_mut() {
            let mut events = player.events.clone();
            let events_estimated_count = events.len();
            player.events = Vec::with_capacity(events_estimated_count);

            match player.backend {
                0 => player_networking.send_events(&player.id, events),
                _ => multithreading_ai.send_events(&player.id, events),
            }
        }

        sleep(Duration::from_millis(20));
    }

    info!("Server stopped successfully!")
}
