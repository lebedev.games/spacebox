use navigation::RouteStatus::{RouteComplete, RouteInProgress, RouteNotStarted};
use physics::Physics::BodyPositionUpdated;
use spacebox::game::Event::{Physics, Universe};
use universe::Universe::TimeUpdated;

mod testing;

pub use testing::*;

#[test]
fn test_move_from_spaceship() {
    GameTestScenario::new()
        // Arrange
        .given_system_type("system")
        .given_system("system", "Solar")
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_spaceship_type("shuttle")
        .given_spaceship_deck("shuttle", "Donkey", "Solar", (1000, 1000))
        .given_player("Alice")
        .given_character_type("human")
        .given_character("Alice", "human", "runner", "on Donkey", (5, 5))
        .given_spaceship_landing("runner", "bridge of Donkey", "at Mars", (27, 27))
        // Act
        .when_player_move_character("Alice", "runner", (22, 32))
        .when_update_game_for(4, real_seconds())
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![
                Physics(BodyPositionUpdated {
                    body: it.body("runner"),
                    space: it.space("at Mars"),
                    position: (27, 32).into(),
                    velocity: (-5, 0).into(),
                }),
                Universe(TimeUpdated { ms: 100000 }),
                Physics(BodyPositionUpdated {
                    body: it.body("runner"),
                    space: it.space("at Mars"),
                    position: (22, 32).into(),
                    velocity: (-5, 0).into(),
                }),
                Universe(TimeUpdated { ms: 200000 }),
                Physics(BodyPositionUpdated {
                    body: it.body("runner"),
                    space: it.space("at Mars"),
                    position: (22, 32).into(),
                    velocity: (0, 0).into(),
                }),
                Universe(TimeUpdated { ms: 300000 }),
                Universe(TimeUpdated { ms: 400000 }),
            ]
        });
}

#[test]
fn test_route_on_movement_start() {
    GameTestScenario::new()
        // Arrange
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_player("Alice")
        .given_character_type("human")
        .given_character("Alice", "human", "runner", "at Mars", (100, 100))
        // Act
        .when_player_move_character("Alice", "runner", (125, 100))
        // Assert
        .then_body_position_should_be("runner", (100, 100))
        .then_navigator_location_should_be("runner", (100, 100))
        .then_navigator_path_should_be_some("runner")
        .then_route_status_should_be("runner", (125, 100), RouteInProgress);
}

#[test]
fn test_other_route_on_movement_start() {
    GameTestScenario::new()
        // Arrange
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_player("Alice")
        .given_character_type("human")
        .given_character("Alice", "human", "runner", "at Mars", (100, 100))
        // Act
        .when_player_move_character("Alice", "runner", (125, 100))
        // Assert
        .then_body_position_should_be("runner", (100, 100))
        .then_navigator_location_should_be("runner", (100, 100))
        .then_navigator_path_should_be_some("runner")
        .then_route_status_should_be("runner", (200, 200), RouteNotStarted);
}

#[test]
fn test_route_on_movement_complete_after_some_time() {
    GameTestScenario::new()
        // Arrange
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_player("Alice")
        .given_character_type("human")
        .given_character("Alice", "human", "runner", "at Mars", (100, 100))
        // Act
        .when_player_move_character("Alice", "runner", (110, 100))
        .when_update_game_for(4, real_seconds())
        // Assert
        .then_body_position_should_be("runner", (110, 100))
        .then_navigator_location_should_be("runner", (110, 100))
        .then_route_status_should_be("runner", (110, 100), RouteComplete)
        .then_navigator_path_should_be_some("runner")
        .then_player_events_should_be("Alice", |it| {
            vec![
                Physics(BodyPositionUpdated {
                    body: it.body("runner"),
                    space: it.space("at Mars"),
                    position: (105, 100).into(),
                    velocity: (5, 0).into(),
                }),
                Universe(TimeUpdated { ms: 100000 }),
                Physics(BodyPositionUpdated {
                    body: it.body("runner"),
                    space: it.space("at Mars"),
                    position: (110, 100).into(),
                    velocity: (5, 0).into(),
                }),
                Universe(TimeUpdated { ms: 200000 }),
                Physics(BodyPositionUpdated {
                    body: it.body("runner"),
                    space: it.space("at Mars"),
                    position: (110, 100).into(),
                    velocity: (0, 0).into(),
                }),
                Universe(TimeUpdated { ms: 300000 }),
                Universe(TimeUpdated { ms: 400000 }),
            ]
        });
}

#[test]
fn test_route_on_movement_complete() {
    GameTestScenario::new()
        // Arrange
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_player("Alice")
        .given_character_type("human")
        .given_character("Alice", "human", "runner", "at Mars", (100, 100))
        // Act
        .when_player_move_character("Alice", "runner", (125, 100))
        .when_update_game_for(5, real_seconds())
        // Assert
        .then_body_position_should_be("runner", (125, 100))
        .then_navigator_location_should_be("runner", (125, 100))
        .then_route_status_should_be("runner", (125, 100), RouteComplete)
        .then_navigator_path_should_be_some("runner");
}

#[test]
fn test_route_on_diagonal_movement_complete() {
    GameTestScenario::new()
        // Arrange
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_player("Alice")
        .given_character_type("human")
        .given_character("Alice", "human", "runner", "at Mars", (95, 35))
        // Act
        .when_player_move_character("Alice", "runner", (115, 15))
        .when_update_game_for(6, real_seconds())
        // Assert
        .then_body_position_should_be("runner", (115, 15))
        .then_navigator_location_should_be("runner", (115, 15))
        .then_route_status_should_be("runner", (115, 15), RouteComplete)
        .then_navigator_path_should_be_some("runner");
}

#[test]
fn test_regular_character_movement() {
    GameTestScenario::new()
        // Arrange
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_player("Alice")
        .given_character_type("human")
        .given_character("Alice", "human", "farmer", "at Mars", (100, 100))
        // Act
        .when_player_move_character("Alice", "farmer", (150, 100))
        .when_update_game_for(3, real_seconds())
        // Assert
        .then_body_position_should_be("farmer", (115, 100))
        .then_navigator_location_should_be("farmer", (115, 100))
        .then_route_status_should_be("farmer", (150, 100), RouteInProgress)
        .then_player_events_should_be("Alice", |it| {
            vec![
                Physics(BodyPositionUpdated {
                    body: it.body("farmer"),
                    space: it.space("at Mars"),
                    position: (105, 100).into(),
                    velocity: (5, 0).into(),
                }),
                Universe(TimeUpdated { ms: 100000 }),
                Physics(BodyPositionUpdated {
                    body: it.body("farmer"),
                    space: it.space("at Mars"),
                    position: (110, 100).into(),
                    velocity: (5, 0).into(),
                }),
                Universe(TimeUpdated { ms: 200000 }),
                Physics(BodyPositionUpdated {
                    body: it.body("farmer"),
                    space: it.space("at Mars"),
                    position: (115, 100).into(),
                    velocity: (5, 0).into(),
                }),
                Universe(TimeUpdated { ms: 300000 }),
            ]
        });
}
