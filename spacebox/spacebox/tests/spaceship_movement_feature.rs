use physics::Physics::BodyPositionUpdated;
use spacebox::game::Event::{Physics, Universe};
use universe::Universe::TimeUpdated;

mod testing;

pub use testing::*;

#[test]
fn test_regular_spaceship_movement() {
    GameTestScenario::new()
        // Arrange
        .given_player("Alice")
        .given_system_type("system")
        .given_system("system", "Solar")
        .given_spaceship_type("battleship")
        .given_spaceship_deck("battleship", "Fighter", "Solar", (100, 100))
        .given_character_type("human")
        .given_character("Alice", "human", "ranger", "on Fighter", (15, 10))
        .given_character_command("ranger", "bridge of Fighter")
        // Act
        .when_player_move_spaceship("Alice", "ranger", (100, 500))
        .when_update_game_for(3, real_seconds())
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![
                Physics(BodyPositionUpdated {
                    body: it.body("Fighter"),
                    space: it.space("Solar"),
                    position: (100, 200).into(),
                    velocity: (0, 100).into(),
                }),
                Universe(TimeUpdated { ms: 100000 }),
                Physics(BodyPositionUpdated {
                    body: it.body("Fighter"),
                    space: it.space("Solar"),
                    position: (100, 300).into(),
                    velocity: (0, 100).into(),
                }),
                Universe(TimeUpdated { ms: 200000 }),
                Physics(BodyPositionUpdated {
                    body: it.body("Fighter"),
                    space: it.space("Solar"),
                    position: (100, 400).into(),
                    velocity: (0, 100).into(),
                }),
                Universe(TimeUpdated { ms: 300000 }),
            ]
        });
}
