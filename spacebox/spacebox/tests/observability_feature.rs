use physics::Physics::{
    BarrierSpaceUpdated, BodyPositionUpdated, BodySpaceUpdated, SpaceConnected, SpaceDisconnected,
};
use physics::SpaceRelation;
use spacebox::game::Appearance::{
    BridgeAppeared, CharacterAppeared, ColonyAppeared, DeckAppeared, PlanetAppeared,
    SpaceshipAppeared, SystemAppeared,
};
use spacebox::game::Disappearance::{
    BridgeVanished, CharacterVanished, ColonyVanished, DeckVanished, PlanetVanished,
    SpaceshipVanished, SystemVanished,
};
use spacebox::game::Event::{Appearance, Disappearance, Physics, Universe};
use universe::Activity::SpaceshipCommand;
use universe::Task::Idle;
use universe::Universe::TimeUpdated;
use universe::{CharacterKey, ColonyKey, Task};

mod testing;

pub use testing::*;

fn tiny_solar_system(scenario: GameTestScenario) -> GameTestScenario {
    scenario
        .given_system_type("system")
        .given_system("system", "Solar")
        .given_planet_type("planet")
        .given_planet("planet", "Mars", "Solar", (1400, 1000))
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_colonization("Mars", "at Mars")
}

#[test]
fn test_spaceship_movement_events_in_same_planet_system() {
    GameTestScenario::new()
        // Arrange
        .given_players(&["Alice", "Boris", "Carol"])
        .given_system_type("system")
        .given_system("system", "Solar")
        .given_spaceship_type("battleship")
        .given_spaceship_deck("battleship", "Alph", "Solar", (100, 100))
        .given_spaceship_deck("battleship", "Beta", "Solar", (200, 100))
        .given_spaceship_deck("battleship", "Gamm", "Solar", (300, 100))
        .given_character_type("human")
        .given_character("Alice", "human", "ranger", "on Alph", (15, 10))
        .given_character("Boris", "human", "farmer", "on Beta", (15, 10))
        .given_character("Carol", "human", "fisher", "on Gamm", (15, 10))
        .given_character_command("ranger", "bridge of Alph")
        // Act
        .when_player_move_spaceship("Alice", "ranger", (100, 500))
        .when_update_game_for_one_second()
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![
                Physics(BodyPositionUpdated {
                    body: it.body("Alph"),
                    space: it.space("Solar"),
                    position: (100, 200).into(),
                    velocity: (0, 100).into(),
                }),
                Universe(TimeUpdated { ms: 100000 }),
            ]
        })
        .then_player_events_should_be("Boris", |it| {
            vec![
                Physics(BodyPositionUpdated {
                    body: it.body("Alph"),
                    space: it.space("Solar"),
                    position: (100, 200).into(),
                    velocity: (0, 100).into(),
                }),
                Universe(TimeUpdated { ms: 100000 }),
            ]
        })
        .then_player_events_should_be("Carol", |it| {
            vec![
                Physics(BodyPositionUpdated {
                    body: it.body("Alph"),
                    space: it.space("Solar"),
                    position: (100, 200).into(),
                    velocity: (0, 100).into(),
                }),
                Universe(TimeUpdated { ms: 100000 }),
            ]
        });
}

#[test]
fn test_land_from_system() {
    GameTestScenario::new()
        .background(tiny_solar_system)
        // Arrange
        .given_player("Alice")
        .given_spaceship_type("battleship")
        .given_spaceship_deck("battleship", "Fighter", "Solar", (100, 100))
        .given_character_type("human")
        .given_character("Alice", "human", "ranger", "on Fighter", (50, 51))
        .given_character_command("ranger", "bridge of Fighter")
        // Act
        .when_player_land_spaceship("Alice", "ranger", "at Mars", (200, 200))
        // Assert
        .then_player_observers_should_be("Alice", "at Mars", 1)
        .then_player_observers_should_be("Alice", "Solar", 0)
        .then_player_events_should_be("Alice", |it| {
            vec![
                Appearance(ColonyAppeared {
                    id: it.colony("at Mars"),
                    key: "colony".into(),
                    space: it.space("at Mars"),
                    mesh: it.mesh("at Mars"),
                    farmland: it.farmland("at Mars"),
                }),
                Physics(BodySpaceUpdated {
                    body: it.body("Fighter"),
                    space: it.space("at Mars"),
                    position: (200, 200).into(),
                    velocity: (0, 0).into(),
                    offset: (0, 0).into(),
                }),
                Physics(SpaceConnected {
                    space: it.space("on Fighter"),
                    parent: it.space("at Mars"),
                    connection_point: (200, 200).into(),
                }),
                Physics(BodySpaceUpdated {
                    body: it.body("ranger"),
                    space: it.space("at Mars"),
                    position: (250, 251).into(),
                    velocity: (0, 0).into(),
                    offset: (200, 200).into(),
                }),
                Physics(BarrierSpaceUpdated {
                    barrier: it.barrier("bridge of Fighter"),
                    space: it.space("at Mars"),
                    position: (250, 250).into(),
                    offset: (200, 200).into(),
                }),
                Disappearance(SystemVanished {
                    id: it.system("Solar"),
                }),
                Disappearance(PlanetVanished {
                    id: it.planet("Mars"),
                }),
            ]
        });
}

#[test]
fn test_land_from_system_with_teammate() {
    GameTestScenario::new()
        .background(tiny_solar_system)
        // Arrange
        .given_player("Alice")
        .given_spaceship_type("battleship")
        .given_spaceship_deck("battleship", "Fighter", "Solar", (100, 100))
        .given_spaceship_deck("battleship", "Shuttle", "Solar", (200, 200))
        .given_character_type("human")
        .given_character("Alice", "human", "ranger", "on Fighter", (50, 51))
        .given_character("Alice", "human", "runner", "on Shuttle", (50, 51))
        .given_character_command("ranger", "bridge of Fighter")
        // Act
        .when_player_land_spaceship("Alice", "ranger", "at Mars", (200, 200))
        // Assert
        .then_player_observers_should_be("Alice", "at Mars", 1)
        .then_player_observers_should_be("Alice", "Solar", 1)
        .then_player_events_should_be("Alice", |it| {
            vec![
                Appearance(ColonyAppeared {
                    id: it.colony("at Mars"),
                    key: "colony".into(),
                    space: it.space("at Mars"),
                    mesh: it.mesh("at Mars"),
                    farmland: it.farmland("at Mars"),
                }),
                Physics(BodySpaceUpdated {
                    body: it.body("Fighter"),
                    space: it.space("at Mars"),
                    position: (200, 200).into(),
                    velocity: (0, 0).into(),
                    offset: (0, 0).into(),
                }),
                Physics(SpaceConnected {
                    space: it.space("on Fighter"),
                    parent: it.space("at Mars"),
                    connection_point: (200, 200).into(),
                }),
                Physics(BodySpaceUpdated {
                    body: it.body("ranger"),
                    space: it.space("at Mars"),
                    position: (250, 251).into(),
                    velocity: (0, 0).into(),
                    offset: (200, 200).into(),
                }),
                Physics(BarrierSpaceUpdated {
                    barrier: it.barrier("bridge of Fighter"),
                    space: it.space("at Mars"),
                    position: (250, 250).into(),
                    offset: (200, 200).into(),
                }),
            ]
        });
}

#[test]
fn test_land_on_colony_with_teammate() {
    GameTestScenario::new()
        .background(tiny_solar_system)
        // Arrange
        .given_player("Alice")
        .given_spaceship_deck("battleship", "Fighter", "Solar", (100, 100))
        .given_character_type("human")
        .given_character("Alice", "human", "ranger", "on Fighter", (50, 51))
        .given_character("Alice", "human", "fisher", "at Mars", (70, 50))
        .given_character_command("ranger", "bridge of Fighter")
        // Act
        .when_player_land_spaceship("Alice", "ranger", "at Mars", (200, 200))
        // Assert
        .then_player_observers_should_be("Alice", "at Mars", 2)
        .then_player_observers_should_be("Alice", "Solar", 0)
        .then_player_events_should_be("Alice", |it| {
            vec![
                Physics(BodySpaceUpdated {
                    body: it.body("Fighter"),
                    space: it.space("at Mars"),
                    position: (200, 200).into(),
                    velocity: (0, 0).into(),
                    offset: (0, 0).into(),
                }),
                Physics(SpaceConnected {
                    space: it.space("on Fighter"),
                    parent: it.space("at Mars"),
                    connection_point: (200, 200).into(),
                }),
                Physics(BodySpaceUpdated {
                    body: it.body("ranger"),
                    space: it.space("at Mars"),
                    position: (250, 251).into(),
                    velocity: (0, 0).into(),
                    offset: (200, 200).into(),
                }),
                Physics(BarrierSpaceUpdated {
                    barrier: it.barrier("bridge of Fighter"),
                    space: it.space("at Mars"),
                    position: (250, 250).into(),
                    offset: (200, 200).into(),
                }),
                Disappearance(SystemVanished {
                    id: it.system("Solar"),
                }),
                Disappearance(PlanetVanished {
                    id: it.planet("Mars"),
                }),
            ]
        });
}

#[test]
fn test_land_on_colony_other_player_spaceship() {
    GameTestScenario::new()
        // Arrange
        .given_players(&["Alice", "Boris"])
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_system_type("system")
        .given_system("system", "Solar")
        .given_spaceship_type("battleship")
        .given_spaceship_deck("battleship", "Fighter", "Solar", (100, 100))
        .given_character_type("human")
        .given_character("Alice", "human", "ranger", "on Fighter", (50, 51))
        .given_character("Boris", "human", "farmer", "at Mars", (70, 50))
        .given_character_command("ranger", "bridge of Fighter")
        // Act
        .when_player_land_spaceship("Alice", "ranger", "at Mars", (200, 200))
        // Assert
        .then_player_events_should_be("Boris", |it| {
            vec![
                Appearance(SpaceshipAppeared {
                    id: it.spaceship("Fighter"),
                    key: "battleship".into(),
                    body: it.body("Fighter"),
                    spacecraft: it.spacecraft("Fighter"),
                    space: it.space("at Mars"),
                    position: (200, 200).into(),
                }),
                Physics(BodySpaceUpdated {
                    body: it.body("Fighter"),
                    space: it.space("at Mars"),
                    position: (200, 200).into(),
                    velocity: (0, 0).into(),
                    offset: (0, 0).into(),
                }),
                Appearance(DeckAppeared {
                    id: it.deck("on Fighter"),
                    key: "battleship".into(),
                    space: it.space("on Fighter"),
                    mesh: it.mesh("on Fighter"),
                    farmland: it.farmland("on Fighter"),
                    parent: None,
                    spaceship: it.spaceship("Fighter"),
                }),
                Appearance(CharacterAppeared {
                    id: it.character("ranger"),
                    key: "human".into(),
                    body: it.body("ranger"),
                    group: it.group("Alice"),
                    space: it.space("on Fighter"),
                    container: it.container("ranger"),
                    task: Task::Running {
                        activity: SpaceshipCommand {
                            bridge: it.bridge("bridge of Fighter"),
                        },
                    },
                    position: (50, 51).into(),
                    health: 100.0,
                    health_limit: 100.0,
                    energy: 100.0,
                    energy_limit: 100.0,
                    stress: 0.0,
                    stress_limit: 100.0,
                }),
                Appearance(BridgeAppeared {
                    id: it.bridge("bridge of Fighter"),
                    key: "bridge".into(),
                    barrier: it.barrier("bridge of Fighter"),
                    space: it.space("on Fighter"),
                    position: (50, 50).into(),
                    rotation: 0.0,
                    entry: (50, 50).into(),
                    spaceship: Some(it.spaceship("Fighter")),
                }),
                Physics(SpaceConnected {
                    space: it.space("on Fighter"),
                    parent: it.space("at Mars"),
                    connection_point: (200, 200).into(),
                }),
                Physics(BodySpaceUpdated {
                    body: it.body("ranger"),
                    space: it.space("at Mars"),
                    position: (250, 251).into(),
                    velocity: (0, 0).into(),
                    offset: (200, 200).into(),
                }),
                Physics(BarrierSpaceUpdated {
                    barrier: it.barrier("bridge of Fighter"),
                    space: it.space("at Mars"),
                    position: (250, 250).into(),
                    offset: (200, 200).into(),
                }),
            ]
        });
}

#[test]
fn test_land_on_colony() {
    GameTestScenario::new()
        .background(tiny_solar_system)
        // Arrange
        .given_players(&["Alice", "Boris"])
        .given_spaceship_type("battleship")
        .given_spaceship_deck("battleship", "Fighter", "Solar", (100, 100))
        .given_character_type("human")
        .given_character("Alice", "human", "ranger", "on Fighter", (50, 51))
        .given_character("Boris", "human", "farmer", "at Mars", (70, 50))
        .given_character_command("ranger", "bridge of Fighter")
        // Act
        .when_player_land_spaceship("Alice", "ranger", "at Mars", (200, 200))
        // Assert
        .then_player_observers_should_be("Alice", "at Mars", 1)
        .then_player_observers_should_be("Alice", "Solar", 0)
        .then_player_events_should_be("Alice", |it| {
            vec![
                Appearance(ColonyAppeared {
                    id: it.colony("at Mars"),
                    key: "colony".into(),
                    space: it.space("at Mars"),
                    mesh: it.mesh("at Mars"),
                    farmland: it.farmland("at Mars"),
                }),
                Appearance(CharacterAppeared {
                    id: it.character("farmer"),
                    key: "human".into(),
                    body: it.body("farmer"),
                    group: it.group("Boris"),
                    space: it.space("at Mars"),
                    container: it.container("farmer"),
                    task: Task::Idle,
                    position: (70, 50).into(),
                    health: 100.0,
                    health_limit: 100.0,
                    energy: 100.0,
                    energy_limit: 100.0,
                    stress: 0.0,
                    stress_limit: 100.0,
                }),
                Physics(BodySpaceUpdated {
                    body: it.body("Fighter"),
                    space: it.space("at Mars"),
                    position: (200, 200).into(),
                    velocity: (0, 0).into(),
                    offset: (0, 0).into(),
                }),
                Physics(SpaceConnected {
                    space: it.space("on Fighter"),
                    parent: it.space("at Mars"),
                    connection_point: (200, 200).into(),
                }),
                Physics(BodySpaceUpdated {
                    body: it.body("ranger"),
                    space: it.space("at Mars"),
                    position: (250, 251).into(),
                    velocity: (0, 0).into(),
                    offset: (200, 200).into(),
                }),
                Physics(BarrierSpaceUpdated {
                    barrier: it.barrier("bridge of Fighter"),
                    space: it.space("at Mars"),
                    position: (250, 250).into(),
                    offset: (200, 200).into(),
                }),
                Disappearance(SystemVanished {
                    id: it.system("Solar"),
                }),
                Disappearance(PlanetVanished {
                    id: it.planet("Mars"),
                }),
            ]
        });
}

#[test]
fn test_launch_to_system_with_teammate() {
    GameTestScenario::new()
        .background(tiny_solar_system)
        // Arrange
        .given_player("Alice")
        .given_spaceship_type("battleship")
        .given_spaceship_deck("battleship", "Alpha", "Solar", (1500, 1000))
        .given_spaceship_deck("battleship", "Betha", "Solar", (1000, 2000))
        .given_character_type("human")
        .given_character("Alice", "human", "ranger", "on Alpha", (15, 10))
        .given_character("Alice", "human", "runner", "on Betha", (60, 50))
        .given_spaceship_landing("ranger", "bridge of Alpha", "at Mars", (100, 100))
        .given_character_command("ranger", "bridge of Alpha")
        // Act
        .when_player_launch_spaceship("Alice", "ranger")
        // Assert
        .then_player_observers_should_be("Alice", "at Mars", 0)
        .then_player_observers_should_be("Alice", "Solar", 2)
        .then_player_events_should_be("Alice", |it| {
            vec![
                Physics(BodySpaceUpdated {
                    body: it.body("Alpha"),
                    space: it.space("Solar"),
                    position: (1400, 1000).into(),
                    velocity: (0, 0).into(),
                    offset: (0, 0).into(),
                }),
                Physics(SpaceDisconnected {
                    space: it.space("on Alpha"),
                }),
                Physics(BodySpaceUpdated {
                    body: it.body("ranger"),
                    space: it.space("on Alpha"),
                    position: (15, 10).into(),
                    velocity: (0, 0).into(),
                    offset: (-100, -100).into(),
                }),
                Physics(BarrierSpaceUpdated {
                    barrier: it.barrier("bridge of Alpha"),
                    space: it.space("on Alpha"),
                    position: (50, 50).into(),
                    offset: (-100, -100).into(),
                }),
                Disappearance(ColonyVanished {
                    id: it.colony("at Mars"),
                }),
            ]
        });
}

#[test]
fn test_launch_to_system_other_player_spaceship() {
    GameTestScenario::new()
        .background(tiny_solar_system)
        // Arrange
        .given_players(&["Alice", "Boris"])
        .given_spaceship_type("battleship")
        .given_spaceship_deck("battleship", "Alpha", "Solar", (1500, 1000))
        .given_spaceship_deck("battleship", "Betha", "Solar", (1000, 2000))
        .given_character_type("human")
        .given_character("Alice", "human", "ranger", "on Alpha", (15, 10))
        .given_character("Boris", "human", "runner", "on Betha", (60, 50))
        .given_spaceship_landing("ranger", "bridge of Alpha", "at Mars", (100, 100))
        .given_character_command("ranger", "bridge of Alpha")
        // Act
        .when_player_launch_spaceship("Alice", "ranger")
        // Assert
        .then_player_events_should_be("Boris", |it| {
            vec![
                Appearance(SpaceshipAppeared {
                    id: it.spaceship("Alpha"),
                    key: "battleship".into(),
                    body: it.body("Alpha"),
                    spacecraft: it.spacecraft("Alpha"),
                    space: it.space("Solar"),
                    position: (1400, 1000).into(),
                }),
                Physics(BodySpaceUpdated {
                    body: it.body("Alpha"),
                    space: it.space("Solar"),
                    position: (1400, 1000).into(),
                    velocity: (0, 0).into(),
                    offset: (0, 0).into(),
                }),
            ]
        });
}

#[test]
fn test_launch_to_system() {
    GameTestScenario::new()
        .background(tiny_solar_system)
        // Arrange
        .given_players(&["Alice", "Boris"])
        .given_spaceship_type("battleship")
        .given_spaceship_deck("battleship", "Alpha", "Solar", (1500, 1000))
        .given_spaceship_deck("battleship", "Betha", "Solar", (1000, 2000))
        .given_character_type("human")
        .given_character("Alice", "human", "ranger", "on Alpha", (15, 10))
        .given_character("Boris", "human", "runner", "on Betha", (60, 50))
        .given_spaceship_landing("ranger", "bridge of Alpha", "at Mars", (100, 100))
        .given_character_command("ranger", "bridge of Alpha")
        // Act
        .when_player_launch_spaceship("Alice", "ranger")
        // Assert
        .then_player_observers_should_be("Alice", "at Mars", 0)
        .then_player_observers_should_be("Alice", "Solar", 1)
        .then_player_events_should_be("Alice", |it| {
            vec![
                Appearance(SystemAppeared {
                    id: it.system("Solar"),
                    key: "system".into(),
                    space: it.space("Solar"),
                }),
                Appearance(PlanetAppeared {
                    id: it.planet("Mars"),
                    key: "planet".into(),
                    body: it.body("Mars"),
                    space: it.space("Solar"),
                    position: (1400, 1000).into(),
                    colonies: vec![it.colony("at Mars")],
                }),
                Appearance(SpaceshipAppeared {
                    id: it.spaceship("Betha"),
                    key: "battleship".into(),
                    body: it.body("Betha"),
                    spacecraft: it.spacecraft("Betha"),
                    space: it.space("Solar"),
                    position: (1000, 2000).into(),
                }),
                Physics(BodySpaceUpdated {
                    body: it.body("Alpha"),
                    space: it.space("Solar"),
                    position: (1400, 1000).into(),
                    velocity: (0, 0).into(),
                    offset: (0, 0).into(),
                }),
                Physics(SpaceDisconnected {
                    space: it.space("on Alpha"),
                }),
                Physics(BodySpaceUpdated {
                    body: it.body("ranger"),
                    space: it.space("on Alpha"),
                    position: (15, 10).into(),
                    velocity: (0, 0).into(),
                    offset: (-100, -100).into(),
                }),
                Physics(BarrierSpaceUpdated {
                    barrier: it.barrier("bridge of Alpha"),
                    space: it.space("on Alpha"),
                    position: (50, 50).into(),
                    offset: (-100, -100).into(),
                }),
                Disappearance(ColonyVanished {
                    id: it.colony("at Mars"),
                }),
            ]
        });
}

#[test]
fn test_launch_from_colony_with_teammate() {
    GameTestScenario::new()
        .background(tiny_solar_system)
        // Arrange
        .given_players(&["Alice", "Boris"])
        .given_spaceship_type("battleship")
        .given_spaceship_deck("battleship", "Fighter", "Solar", (1000, 1000))
        .given_character_type("human")
        .given_character("Alice", "human", "ranger", "on Fighter", (15, 10))
        .given_character("Alice", "human", "farmer", "at Mars", (60, 50))
        .given_character("Boris", "human", "fisher", "at Mars", (70, 50))
        .given_spaceship_landing("ranger", "bridge of Fighter", "at Mars", (100, 100))
        .given_character_command("ranger", "bridge of Fighter")
        // Act
        .when_player_launch_spaceship("Alice", "ranger")
        // Assert
        .then_player_observers_should_be("Alice", "at Mars", 1)
        .then_player_observers_should_be("Alice", "Solar", 1)
        .then_player_events_should_be("Alice", |it| {
            vec![
                Appearance(SystemAppeared {
                    id: it.system("Solar"),
                    key: "system".into(),
                    space: it.space("Solar"),
                }),
                Appearance(PlanetAppeared {
                    id: it.planet("Mars"),
                    key: "planet".into(),
                    body: it.body("Mars"),
                    space: it.space("Solar"),
                    position: (1400, 1000).into(),
                    colonies: vec![it.colony("at Mars")],
                }),
                Physics(BodySpaceUpdated {
                    body: it.body("Fighter"),
                    space: it.space("Solar"),
                    position: (1400, 1000).into(),
                    velocity: (0, 0).into(),
                    offset: (0, 0).into(),
                }),
                Physics(SpaceDisconnected {
                    space: it.space("on Fighter"),
                }),
                Physics(BodySpaceUpdated {
                    body: it.body("ranger"),
                    space: it.space("on Fighter"),
                    position: (15, 10).into(),
                    velocity: (0, 0).into(),
                    offset: (-100, -100).into(),
                }),
                Physics(BarrierSpaceUpdated {
                    barrier: it.barrier("bridge of Fighter"),
                    space: it.space("on Fighter"),
                    position: (50, 50).into(),
                    offset: (-100, -100).into(),
                }),
            ]
        });
}

#[test]
fn test_launch_from_colony_other_player_spaceship() {
    GameTestScenario::new()
        .background(tiny_solar_system)
        // Arrange
        .given_players(&["Alice", "Boris"])
        .given_spaceship_type("battleship")
        .given_spaceship_deck("battleship", "Fighter", "Solar", (100, 100))
        .given_character_type("human")
        .given_character("Alice", "human", "ranger", "on Fighter", (15, 10))
        .given_character("Boris", "human", "fisher", "at Mars", (70, 50))
        .given_spaceship_landing("ranger", "bridge of Fighter", "at Mars", (100, 100))
        .given_character_command("ranger", "bridge of Fighter")
        // Act
        .when_player_launch_spaceship("Alice", "ranger")
        // Assert
        .then_player_events_should_be("Boris", |it| {
            vec![
                Disappearance(SpaceshipVanished {
                    id: it.spaceship("Fighter"),
                }),
                Disappearance(DeckVanished {
                    id: it.deck("on Fighter"),
                }),
                Disappearance(CharacterVanished {
                    id: it.character("ranger"),
                }),
                Disappearance(BridgeVanished {
                    id: it.bridge("bridge of Fighter"),
                }),
            ]
        });
}

#[test]
fn test_launch_from_colony() {
    GameTestScenario::new()
        .background(tiny_solar_system)
        // Arrange
        .given_players(&["Alice", "Boris"])
        .given_spaceship_type("battleship")
        .given_spaceship_deck("battleship", "Fighter", "Solar", (100, 100))
        .given_character_type("human")
        .given_character("Alice", "human", "ranger", "on Fighter", (15, 10))
        .given_character("Boris", "human", "fisher", "at Mars", (70, 50))
        .given_spaceship_landing("ranger", "bridge of Fighter", "at Mars", (100, 100))
        .given_character_command("ranger", "bridge of Fighter")
        // Act
        .when_player_launch_spaceship("Alice", "ranger")
        // Assert
        .then_player_observers_should_be("Alice", "at Mars", 0)
        .then_player_observers_should_be("Alice", "Solar", 1)
        .then_player_events_should_be("Alice", |it| {
            vec![
                Appearance(SystemAppeared {
                    id: it.system("Solar"),
                    key: "system".into(),
                    space: it.space("Solar"),
                }),
                Appearance(PlanetAppeared {
                    id: it.planet("Mars"),
                    key: "planet".into(),
                    body: it.body("Mars"),
                    space: it.space("Solar"),
                    position: (1400, 1000).into(),
                    colonies: vec![it.colony("at Mars")],
                }),
                Physics(BodySpaceUpdated {
                    body: it.body("Fighter"),
                    space: it.space("Solar"),
                    position: (1400, 1000).into(),
                    velocity: (0, 0).into(),
                    offset: (0, 0).into(),
                }),
                Physics(SpaceDisconnected {
                    space: it.space("on Fighter"),
                }),
                Physics(BodySpaceUpdated {
                    body: it.body("ranger"),
                    space: it.space("on Fighter"),
                    position: (15, 10).into(),
                    velocity: (0, 0).into(),
                    offset: (-100, -100).into(),
                }),
                Physics(BarrierSpaceUpdated {
                    barrier: it.barrier("bridge of Fighter"),
                    space: it.space("on Fighter"),
                    position: (50, 50).into(),
                    offset: (-100, -100).into(),
                }),
                Disappearance(ColonyVanished {
                    id: it.colony("at Mars"),
                }),
                Disappearance(CharacterVanished {
                    id: it.character("fisher"),
                }),
            ]
        });
}

#[test]
fn test_body_movement_events_on_spaceship_landed_on_colony() {
    GameTestScenario::new()
        // Arrange
        .given_players(&["Alice", "Boris"])
        .given_colony_type("colony", (400, 400))
        .given_system_type("system")
        .given_system("system", "Solar")
        .given_colony("colony", "at Mars")
        .given_spaceship_deck("battleship", "Enterprise", "Solar", (100, 100))
        .given_character_type("human")
        .given_character("Alice", "human", "ranger", "on Enterprise", (10, 10))
        .given_character("Boris", "human", "farmer", "at Mars", (70, 50))
        .given_spaceship_landing("ranger", "bridge of Enterprise", "at Mars", (30, 80))
        // Act
        .when_character_move_towards("Alice", "ranger", (50, 90))
        .when_update_game_for_one_second()
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![
                Physics(BodyPositionUpdated {
                    body: it.body("ranger"),
                    space: it.space("at Mars"),
                    position: (45, 90).into(),
                    velocity: (5, 0).into(),
                }),
                Universe(TimeUpdated { ms: 100000 }),
            ]
        })
        .then_player_events_should_be("Boris", |it| {
            vec![
                Physics(BodyPositionUpdated {
                    body: it.body("ranger"),
                    space: it.space("at Mars"),
                    position: (45, 90).into(),
                    velocity: (5, 0).into(),
                }),
                Universe(TimeUpdated { ms: 100000 }),
            ]
        });
}

#[test]
fn test_look_around_when_character_spawned_in_same_space() {
    GameTestScenario::new()
        // Arrange
        .given_player("Alice")
        .given_player("Boris")
        .given_colony_type("colony", (100, 100))
        .given_colony("colony", "at Mars")
        .given_character_type("human")
        .given_character("Alice", "human", "ranger", "at Mars", (30, 50))
        .given_character("Boris", "human", "farmer", "at Mars", (70, 50))
        // Act
        .when_master_spawn_character("Alice", "human", "fisher", "at Mars", (10, 10))
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![Appearance(CharacterAppeared {
                id: it.character("fisher"),
                key: CharacterKey("human".into()),
                body: it.body("fisher"),
                group: it.group("Alice"),
                space: it.space("at Mars"),
                container: it.container("fisher"),
                task: Task::Idle,
                position: (10, 10).into(),
                health: 100.0,
                health_limit: 100.0,
                energy: 100.0,
                energy_limit: 100.0,
                stress: 0.0,
                stress_limit: 100.0,
            })]
        });
}

#[test]
fn test_look_around_when_character_spawned_in_spaceship() {
    GameTestScenario::new()
        .background(tiny_solar_system)
        // Arrange
        .given_player("Alice")
        .given_spaceship_type("battleship")
        .given_spaceship_deck("battleship", "Fighter", "Solar", (100, 100))
        .given_character_type("human")
        // Act
        .when_master_spawn_character("Alice", "human", "dancer", "on Fighter", (10, 10))
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![
                Appearance(SystemAppeared {
                    id: it.system("Solar"),
                    key: "system".into(),
                    space: it.space("Solar"),
                }),
                Appearance(PlanetAppeared {
                    id: it.planet("Mars"),
                    key: "planet".into(),
                    body: it.body("Mars"),
                    space: it.space("Solar"),
                    position: (1400, 1000).into(),
                    colonies: vec![it.colony("at Mars")],
                }),
                Appearance(SpaceshipAppeared {
                    id: it.spaceship("Fighter"),
                    key: "battleship".into(),
                    body: it.body("Fighter"),
                    spacecraft: it.spacecraft("Fighter"),
                    space: it.space("Solar"),
                    position: (100, 100).into(),
                }),
                Appearance(DeckAppeared {
                    id: it.deck("on Fighter"),
                    key: "battleship".into(),
                    space: it.space("on Fighter"),
                    mesh: it.mesh("on Fighter"),
                    farmland: it.farmland("on Fighter"),
                    parent: None,
                    spaceship: it.spaceship("Fighter"),
                }),
                Appearance(BridgeAppeared {
                    id: it.bridge("bridge of Fighter"),
                    key: "bridge".into(),
                    barrier: it.barrier("bridge of Fighter"),
                    space: it.space("on Fighter"),
                    position: (50, 50).into(),
                    rotation: 0.0,
                    entry: (50, 50).into(),
                    spaceship: Some(it.spaceship("Fighter")),
                }),
                Appearance(CharacterAppeared {
                    id: it.character("dancer"),
                    key: "human".into(),
                    body: it.body("dancer"),
                    group: it.group("Alice"),
                    space: it.space("on Fighter"),
                    container: it.container("dancer"),
                    task: Idle,
                    position: (10, 10).into(),
                    health: 100.0,
                    health_limit: 100.0,
                    energy: 100.0,
                    energy_limit: 100.0,
                    stress: 0.0,
                    stress_limit: 100.0,
                }),
            ]
        });
}

#[test]
fn test_look_around_when_character_spawned_in_landed_spaceship() {
    GameTestScenario::new()
        // Arrange
        .given_player("Alice")
        .given_player("Boris")
        .given_system_type("system")
        .given_system("system", "Solar")
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_spaceship_type("shuttle")
        .given_spaceship_deck("shuttle", "Donkey", "Solar", (1000, 1000))
        .given_character_type("human")
        .given_character("Boris", "human", "ranger", "on Donkey", (50, 51))
        .given_spaceship_landing("ranger", "bridge of Donkey", "at Mars", (100, 100))
        // Act
        .when_master_spawn_character("Alice", "human", "fisher", "at Mars", (105, 105))
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![
                Appearance(ColonyAppeared {
                    id: it.colony("at Mars"),
                    key: "colony".into(),
                    space: it.space("at Mars"),
                    mesh: it.mesh("at Mars"),
                    farmland: it.farmland("at Mars"),
                }),
                Appearance(SpaceshipAppeared {
                    id: it.spaceship("Donkey"),
                    key: "shuttle".into(),
                    body: it.body("Donkey"),
                    spacecraft: it.spacecraft("Donkey"),
                    space: it.space("at Mars"),
                    position: (100, 100).into(),
                }),
                Appearance(CharacterAppeared {
                    id: it.character("ranger"),
                    key: "human".into(),
                    body: it.body("ranger"),
                    group: it.group("Boris"),
                    space: it.space("at Mars"),
                    container: it.container("ranger"),
                    task: Task::Running {
                        activity: SpaceshipCommand {
                            bridge: it.bridge("bridge of Donkey"),
                        },
                    },
                    position: (150, 151).into(),
                    health: 100.0,
                    health_limit: 100.0,
                    energy: 100.0,
                    energy_limit: 100.0,
                    stress: 0.0,
                    stress_limit: 100.0,
                }),
                Appearance(BridgeAppeared {
                    id: it.bridge("bridge of Donkey"),
                    key: "bridge".into(),
                    barrier: it.barrier("bridge of Donkey"),
                    space: it.space("at Mars"),
                    position: (150, 150).into(),
                    rotation: 0.0,
                    entry: (150, 150).into(),
                    spaceship: Some(it.spaceship("Donkey")),
                }),
                Appearance(DeckAppeared {
                    id: it.deck("on Donkey"),
                    key: "shuttle".into(),
                    space: it.space("on Donkey"),
                    mesh: it.mesh("on Donkey"),
                    farmland: it.farmland("on Donkey"),
                    parent: Some(SpaceRelation {
                        space: it.space("at Mars"),
                        point: (100, 100).into(),
                    }),
                    spaceship: it.spaceship("Donkey"),
                }),
                Appearance(CharacterAppeared {
                    id: it.character("fisher"),
                    key: "human".into(),
                    body: it.body("fisher"),
                    group: it.group("Alice"),
                    space: it.space("at Mars"),
                    container: it.container("fisher"),
                    task: Task::Idle,
                    position: (105, 105).into(),
                    health: 100.0,
                    health_limit: 100.0,
                    energy: 100.0,
                    energy_limit: 100.0,
                    stress: 0.0,
                    stress_limit: 100.0,
                }),
            ]
        });
}

#[test]
fn test_look_around_when_character_spawned_in_new_space() {
    GameTestScenario::new()
        // Arrange
        .given_player("Alice")
        .given_player("Boris")
        .given_colony_type("colony", (100, 100))
        .given_colony("colony", "at Mars")
        .given_colony("colony", "at Earth")
        .given_character_type("human")
        .given_character("Alice", "human", "ranger", "at Mars", (30, 50))
        .given_character("Boris", "human", "farmer", "at Earth", (70, 50))
        // Act
        .when_master_spawn_character("Alice", "human", "fisher", "at Earth", (10, 10))
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![
                Appearance(ColonyAppeared {
                    id: it.colony("at Earth"),
                    key: ColonyKey("colony".into()),
                    space: it.space("at Earth"),
                    mesh: it.mesh("at Earth"),
                    farmland: it.farmland("at Earth"),
                }),
                Appearance(CharacterAppeared {
                    id: it.character("farmer"),
                    key: CharacterKey("human".into()),
                    body: it.body("farmer"),
                    group: it.group("Boris"),
                    space: it.space("at Earth"),
                    container: it.container("farmer"),
                    task: Task::Idle,
                    position: (70, 50).into(),
                    health: 100.0,
                    health_limit: 100.0,
                    energy: 100.0,
                    energy_limit: 100.0,
                    stress: 0.0,
                    stress_limit: 100.0,
                }),
                Appearance(CharacterAppeared {
                    id: it.character("fisher"),
                    key: CharacterKey("human".into()),
                    body: it.body("fisher"),
                    group: it.group("Alice"),
                    space: it.space("at Earth"),
                    container: it.container("fisher"),
                    task: Task::Idle,
                    position: (10, 10).into(),
                    health: 100.0,
                    health_limit: 100.0,
                    energy: 100.0,
                    energy_limit: 100.0,
                    stress: 0.0,
                    stress_limit: 100.0,
                }),
            ]
        });
}

#[test]
fn test_body_movement_events_in_different_spaces() {
    GameTestScenario::new()
        // Arrange
        .given_player("Alice")
        .given_player("Boris")
        .given_colony_type("colony", (100, 100))
        .given_colony("colony", "at Mars")
        .given_colony("colony", "at Earth")
        .given_character_type("human")
        .given_character("Alice", "human", "ranger", "at Mars", (30, 50))
        .given_character("Boris", "human", "farmer", "at Earth", (70, 50))
        // Act
        .when_character_move_towards("Alice", "ranger", (50, 50))
        .when_update_game_for_one_second()
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![
                Physics(BodyPositionUpdated {
                    body: it.body("ranger"),
                    space: it.space("at Mars"),
                    position: (35, 50).into(),
                    velocity: (5, 0).into(),
                }),
                Universe(TimeUpdated { ms: 100000 }),
            ]
        })
        .then_player_events_should_be("Boris", |_| vec![Universe(TimeUpdated { ms: 100000 })]);
}

#[test]
fn test_body_movement_events_in_same_space_with_many_observers() {
    GameTestScenario::new()
        // Arrange
        .given_player("Alice")
        .given_player("Boris")
        .given_colony_type("colony", (100, 100))
        .given_colony("colony", "at Mars")
        .given_character_type("human")
        .given_character("Alice", "human", "ranger", "at Mars", (30, 50))
        .given_character("Alice", "human", "fisher", "at Mars", (35, 55))
        .given_character("Boris", "human", "farmer", "at Mars", (70, 50))
        // Act
        .when_character_move_towards("Alice", "ranger", (50, 50))
        .when_update_game_for_one_second()
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![
                Physics(BodyPositionUpdated {
                    body: it.body("ranger"),
                    space: it.space("at Mars"),
                    position: (35, 50).into(),
                    velocity: (5, 0).into(),
                }),
                Universe(TimeUpdated { ms: 100000 }),
            ]
        })
        .then_player_events_should_be("Boris", |it| {
            vec![
                Physics(BodyPositionUpdated {
                    body: it.body("ranger"),
                    space: it.space("at Mars"),
                    position: (35, 50).into(),
                    velocity: (5, 0).into(),
                }),
                Universe(TimeUpdated { ms: 100000 }),
            ]
        });
}

#[test]
fn test_body_movement_events_in_same_space() {
    GameTestScenario::new()
        // Arrange
        .given_player("Alice")
        .given_player("Boris")
        .given_colony_type("colony", (100, 100))
        .given_colony("colony", "at Mars")
        .given_character_type("human")
        .given_character("Alice", "human", "ranger", "at Mars", (30, 50))
        .given_character("Boris", "human", "farmer", "at Mars", (70, 50))
        // Act
        .when_character_move_towards("Alice", "ranger", (50, 50))
        .when_update_game_for_one_second()
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![
                Physics(BodyPositionUpdated {
                    body: it.body("ranger"),
                    space: it.space("at Mars"),
                    position: (35, 50).into(),
                    velocity: (5, 0).into(),
                }),
                Universe(TimeUpdated { ms: 100000 }),
            ]
        })
        .then_player_events_should_be("Boris", |it| {
            vec![
                Physics(BodyPositionUpdated {
                    body: it.body("ranger"),
                    space: it.space("at Mars"),
                    position: (35, 50).into(),
                    velocity: (5, 0).into(),
                }),
                Universe(TimeUpdated { ms: 100000 }),
            ]
        });
}
