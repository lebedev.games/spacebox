use std::collections::HashMap;
use std::fs;
use std::path::Path;
use std::rc::Rc;

use farming::{
    FarmingDatabase, FarmingDomain, FarmingState, Farmland, FarmlandId, FarmlandKey, FarmlandType,
    Place, Plant, PlantId, PlantKey, PlantKind, PlantState, PlantType,
};
use inventory::{
    Container, ContainerId, ContainerKey, ContainerType, Effect, GenericKey, InventoryDatabase,
    InventoryDomain, InventoryState, Item, ItemFunction, ItemId, ItemKey, ItemType,
};
use navigation::{
    Mesh, MeshId, MeshKey, MeshType, NavigationDatabase, NavigationDomain, NavigationState,
    Navigator, NavigatorId, NavigatorKey, NavigatorType, Obstacle, ObstacleId, ObstacleKey,
    ObstacleType, PathPoint, Point,
};
use physics::{
    Barrier, BarrierId, BarrierKey, BarrierOrigin, BarrierType, Body, BodyId, BodyKey, BodyOrigin,
    BodyType, PhysicsDatabase, PhysicsDomain, PhysicsProperties, PhysicsState, Portal, PortalKey,
    PortalType, Segment, SensorId, Space, SpaceId, SpaceKey, SpaceOrigin, SpaceRelation, SpaceType,
    SteeringBehaviour, Vector,
};
use piloting::{
    CommanderId, PilotingDatabase, PilotingDomain, PilotingState, Spacecraft, SpacecraftId,
    SpacecraftKey, SpacecraftType,
};
use spacebox::game::IdGenerator;
use spacebox::game::Rules;
use spacebox::game::{Database, GameStatus, Master};
use universe::{
    Activity, Bed, BedId, BedKey, BedType, Bridge, BridgeId, BridgeKey, BridgeType, Character,
    CharacterId, CharacterKey, CharacterType, Colonization, Colony, ColonyId, ColonyKey,
    ColonyType, Construction, ConstructionId, ConstructionKey, ConstructionProject,
    ConstructionType, Crop, CropId, CropKey, CropType, Deck, DeckId, DeckKey, DeckType, Group,
    GroupId, Landing, Permission, Planet, PlanetId, PlanetKey, PlanetType, Planter, PlanterId,
    PlanterKey, PlanterType, Prop, PropId, PropKey, PropType, Securable, Spaceship, SpaceshipId,
    SpaceshipKey, SpaceshipType, Station, StationId, StationKey, StationType, Storage, StorageId,
    StorageKey, StorageType, System, SystemId, SystemKey, SystemType, Task, Tree, TreeId, TreeKey,
    TreeType, UniverseDatabase, UniverseDomain, UniverseState, Workbench, WorkbenchId,
    WorkbenchKey, WorkbenchType,
};

mod testing;

pub use testing::*;

#[test]
pub fn test_persistence() {
    GameTestScenario::new();

    let database_path = Path::new("./output");
    if !database_path.exists() {
        fs::create_dir_all(database_path).unwrap();
    }
    let database_path = fs::canonicalize(database_path).unwrap();

    let shared = Database {
        universe: Rc::new(UniverseDatabase {
            systems: vec![SystemType {
                key: SystemKey("key".into()),
                space: SpaceKey("space".into()),
                mesh: MeshKey("mesh".into()),
            }],
            planets: vec![PlanetType {
                key: PlanetKey("key".into()),
                body: BodyKey("body".into()),
            }],
            spaceships: vec![SpaceshipType {
                key: SpaceshipKey("key".into()),
                spacecraft: SpacecraftKey("spacecraft".into()),
                body: BodyKey("body".into()),
                navigator: NavigatorKey("navigator".into()),
            }],
            stations: vec![StationType {
                key: StationKey("key".into()),
                spacecraft: SpacecraftKey("spacecraft".into()),
                body: BodyKey("body".into()),
            }],
            decks: vec![DeckType {
                key: DeckKey("key".into()),
                space: SpaceKey("space".into()),
                farmland: FarmlandKey("farmland".into()),
                mesh: MeshKey("mesh".into()),
            }],
            colonies: vec![ColonyType {
                key: ColonyKey("key".into()),
                space: SpaceKey("space".into()),
                farmland: FarmlandKey("farmland".into()),
                mesh: MeshKey("mesh".into()),
            }],
            characters: vec![CharacterType {
                key: CharacterKey("key".into()),
                body: BodyKey("body".into()),
                health_limit: 1.0,
                energy_limit: 2.0,
                stress_limit: 3.0,
                container: ContainerKey("container".into()),
                navigator: NavigatorKey("navigator".into()),
            }],
            props: vec![PropType {
                key: PropKey("prop".into()),
                barrier: BarrierKey("barrier".into()),
                obstacle: ObstacleKey("obstacle".into()),
            }],
            crops: vec![CropType {
                key: CropKey("key".into()),
                plant: PlantKey("plant".into()),
                consumable: ItemKey("consumable".into()),
                barrier: BarrierKey("barrier".into()),
                production: 1.0,
            }],
            trees: vec![TreeType {
                key: TreeKey("key".into()),
                barrier: BarrierKey("barrier".into()),
                obstacle: ObstacleKey("obstacle".into()),
                plant: PlantKey("plant".into()),
            }],
            storages: vec![StorageType {
                key: StorageKey("key".into()),
                barrier: BarrierKey("barrier".into()),
                obstacle: ObstacleKey("obstacle".into()),
                container: ContainerKey("container".into()),
                relative_entry: (0, 0).into(),
            }],
            workbenches_fallback: Default::default(),
            workbenches: vec![WorkbenchType {
                key: WorkbenchKey("key".into()),
                barrier: BarrierKey("barrier".into()),
                obstacle: ObstacleKey("container".into()),
                relative_entry: (0, 0).into(),
            }],
            constructions: vec![ConstructionType {
                key: ConstructionKey("key".into()),
                project: ConstructionProject::PropProject {
                    key: PropKey("prop".into()),
                },
                days_to_construct: 0.0,
            }],
            beds: vec![BedType {
                key: BedKey("key".into()),
                barrier: BarrierKey("barrier".into()),
                obstacle: ObstacleKey("obstacle".into()),
                relative_entry: (0, 0).into(),
            }],
            planters: vec![PlanterType {
                key: PlanterKey("key".into()),
                barrier: BarrierKey("barrier".into()),
                obstacle: ObstacleKey("obstacle".into()),
                relative_entry: (0, 0).into(),
            }],
            bridges: vec![BridgeType {
                key: BridgeKey("key".into()),
                barrier: BarrierKey("barrier".into()),
                obstacle: ObstacleKey("obstacle".into()),
                relative_entry: (0, 0).into(),
            }],
        }),
        physics: Rc::new(PhysicsDatabase {
            spaces: vec![SpaceType {
                key: SpaceKey("key".into()),
            }],
            bodies: vec![BodyType {
                key: BodyKey("key".into()),
                origin: BodyOrigin::Character,
                speed: 0.0,
            }],
            barriers: vec![BarrierType {
                key: BarrierKey("key".into()),
                size: Vector::new(1.0, 2.0),
            }],
            sensors: vec![],
            portals: vec![PortalType {
                key: PortalKey("key".into()),
                offset: Vector::new(1.0, 2.0),
            }],
        }),
        navigation: Rc::new(NavigationDatabase {
            meshes: vec![MeshType {
                key: MeshKey("key".into()),
                size: Point::new(10, 10),
            }],
            obstacles: vec![ObstacleType {
                key: ObstacleKey("key".into()),
                size: Point::new(1, 1),
            }],
            navigators: vec![NavigatorType {
                key: NavigatorKey("key".into()),
            }],
        }),
        piloting: Rc::new(PilotingDatabase {
            spacecrafts: vec![SpacecraftType {
                key: SpacecraftKey("key".into()),
            }],
        }),
        farming: Rc::new(FarmingDatabase {
            farmlands: vec![FarmlandType {
                key: FarmlandKey("key".into()),
                size: Place::from((10, 10)),
                fertile: false,
            }],
            plants: vec![PlantType {
                key: PlantKey("key".into()),
                days_to_maturity: 2.0,
                days_to_harvest: 3.5,
                max_harvests: Some(4),
                water_per_day: 1.0,
                harvest_time: 2.0,
            }],
        }),
        inventory: Rc::new(InventoryDatabase {
            containers: vec![ContainerType {
                key: ContainerKey("key".into()),
                size: 100,
            }],
            items: vec![ItemType {
                key: ItemKey("key".into()),
                able_to_stack: false,
                weight: 1.0,
                functions: vec![
                    ItemFunction::Seed {
                        crop: GenericKey("garlic".into()),
                    },
                    ItemFunction::Consumable {
                        effects: vec![
                            Effect::RestoreEnergy { amount: 42.0 },
                            Effect::ReduceEnergy { amount: 42.0 },
                        ],
                    },
                ],
            }],
        }),
    };

    let mut master = Master {
        database_path,
        shared_database: shared.clone(),
        universe: UniverseDomain {
            database: shared.universe.clone(),
            state: UniverseState {
                systems: vec![System {
                    id: SystemId(1),
                    key: SystemKey("key".into()),
                    space: SpaceId(1),
                    mesh: MeshId(1),
                }],
                planets: vec![Planet {
                    id: PlanetId(1),
                    key: PlanetKey("key".into()),
                    body: BodyId(1),
                }],
                spaceships: vec![Spaceship {
                    id: SpaceshipId(1),
                    key: SpaceshipKey("key".into()),
                    spacecraft: SpacecraftId(1),
                    body: BodyId(1),
                    landing: Some(Landing {
                        bounds_min: (0, 0).into(),
                        bounds_max: (100, 100).into(),
                        point: (0, 0).into(),
                    }),
                    navigator: NavigatorId(1),
                }],
                stations: vec![Station {
                    id: StationId(1),
                    key: StationKey("key".into()),
                    spacecraft: SpacecraftId(1),
                    body: BodyId(1),
                }],
                decks: vec![Deck {
                    id: DeckId(1),
                    key: DeckKey("key".into()),
                    space: SpaceId(1),
                    farmland: FarmlandId(1),
                    mesh: MeshId(1),
                    spaceship: SpaceshipId(1),
                }],
                colonies: vec![Colony {
                    id: ColonyId(1),
                    key: ColonyKey("key".into()),
                    space: SpaceId(1),
                    farmland: FarmlandId(1),
                    mesh: MeshId(1),
                }],
                colonizations: vec![Colonization {
                    colony: ColonyId(1),
                    planet: PlanetId(2),
                }],
                characters: vec![Character {
                    id: CharacterId(1),
                    key: CharacterKey("key".into()),
                    group: GroupId(1),
                    body: BodyId(1),
                    task: Task::Running {
                        activity: Activity::SpaceshipCommand {
                            bridge: BridgeId(1),
                        },
                    },
                    sensor: SensorId(1),
                    container: ContainerId(1),
                    navigator: NavigatorId(1),
                    health: 0.0,
                    health_limit: 1.0,
                    energy: 2.0,
                    energy_limit: 3.0,
                    stress: 4.0,
                    stress_limit: 5.0,
                }],
                props: vec![Prop {
                    id: PropId(1),
                    key: PropKey("key".into()),
                    barrier: BarrierId(1),
                    obstacle: ObstacleId(1),
                }],
                crops: vec![Crop {
                    id: CropId(1),
                    key: CropKey("key".into()),
                    plant: PlantId(1),
                    barrier: BarrierId(1),
                }],
                trees: vec![Tree {
                    id: TreeId(1),
                    key: TreeKey("key".into()),
                    barrier: BarrierId(1),
                    obstacle: ObstacleId(1),
                    plant: PlantId(1),
                }],
                groups: vec![Group {
                    id: GroupId(1),
                    name: "group".to_string(),
                    playable: true,
                }],
                permissions: vec![Permission {
                    securable: Securable::Bed { id: BedId(1) },
                    principal: GroupId(1),
                }],
                storages: vec![Storage {
                    id: StorageId(1),
                    key: StorageKey("key".into()),
                    barrier: BarrierId(1),
                    obstacle: ObstacleId(1),
                    container: ContainerId(1),
                    relative_entry: (0, 0).into(),
                }],
                workbenches: vec![Workbench {
                    id: WorkbenchId(1),
                    key: WorkbenchKey("key".into()),
                    barrier: BarrierId(1),
                    obstacle: ObstacleId(1),
                    relative_entry: (0, 0).into(),
                }],
                constructions: vec![Construction {
                    id: ConstructionId(1),
                    key: ConstructionKey("key".into()),
                    project: ConstructionProject::WorkbenchProject {
                        key: WorkbenchKey("workbench".into()),
                    },
                    barrier: BarrierId(1),
                    obstacle: ObstacleId(1),
                    progress: 0.0,
                }],
                beds: vec![Bed {
                    id: BedId(1),
                    key: BedKey("key".into()),
                    barrier: BarrierId(1),
                    obstacle: ObstacleId(1),
                    user: None,
                    relative_entry: (0, 0).into(),
                }],
                planters: vec![Planter {
                    id: PlanterId(1),
                    key: PlanterKey("key".into()),
                    barrier: BarrierId(1),
                    obstacle: ObstacleId(1),
                    relative_entry: (0, 0).into(),
                }],
                bridges: vec![Bridge {
                    id: BridgeId(1),
                    key: BridgeKey("key".into()),
                    barrier: BarrierId(1),
                    obstacle: ObstacleId(1),
                    relative_entry: (0, 0).into(),
                    spaceship: Some(SpaceshipId(2)),
                }],
            },
        },
        physics: PhysicsDomain {
            properties: PhysicsProperties {
                force_limit: 0.0,
                time_simulation_scale: 0.0,
            },
            database: shared.physics.clone(),
            state: PhysicsState {
                spaces: vec![
                    Space {
                        id: SpaceId(1),
                        key: SpaceKey("key".into()),
                        origin: SpaceOrigin::Colony,
                        parent: Some(SpaceRelation {
                            space: SpaceId(2),
                            point: (0, 0).into(),
                        }),
                    },
                    Space {
                        id: SpaceId(2),
                        key: SpaceKey("key".into()),
                        origin: SpaceOrigin::Deck,
                        parent: None,
                    },
                ],
                bodies: vec![
                    Body {
                        id: BodyId(1),
                        key: BodyKey("key".into()),
                        space: SpaceId(1),
                        origin: BodyOrigin::Character,
                        position: Vector::ZERO,
                        velocity: Vector::ZERO,
                        speed: 1.0,
                        steering: None,
                    },
                    Body {
                        id: BodyId(2),
                        key: BodyKey("key".into()),
                        space: SpaceId(2),
                        origin: BodyOrigin::Character,
                        position: Vector::ZERO,
                        velocity: Vector::ZERO,
                        speed: 2.0,
                        steering: Some(SteeringBehaviour::FollowPath {
                            path: vec![Vector::new(1.0, 1.0), Vector::new(2.0, 2.0)],
                        }),
                    },
                ],
                barriers: vec![Barrier {
                    id: BarrierId(1),
                    key: BarrierKey("key".into()),
                    space: SpaceId(2),
                    position: Vector::ZERO,
                    rotation: 0.0,
                    size: Vector::ZERO,
                    origin: BarrierOrigin::Prop,
                }],
                sensors: vec![],
                portals: vec![Portal {
                    space: SpaceId(2),
                    target: Some(SpaceId(10)),
                    segment: Segment {
                        begin: Vector::new(1.0, 2.0),
                        end: Vector::new(2.0, 1.0),
                    },
                    offset: Vector::new(1.0, 2.0),
                }],
                signals: Default::default(),
            },
        },
        navigation: NavigationDomain {
            database: shared.navigation.clone(),
            state: NavigationState {
                meshes: vec![Mesh {
                    id: MeshId(1),
                    key: MeshKey("key".into()),
                    counter: 0,
                    size: Point::new(10, 10),
                    fragments: vec![],
                    links: vec![],
                    obstacles: vec![Obstacle {
                        id: ObstacleId(1),
                        key: ObstacleKey("key".into()),
                        location: PathPoint::new(1.0, 1.0),
                        rotation: false,
                        size: Point::new(2, 2),
                        mesh: MeshId(1),
                    }],
                }],
                navigators: vec![
                    Navigator {
                        id: NavigatorId(1),
                        key: NavigatorKey("key".into()),
                        location: PathPoint::new(100.0, 100.0),
                        mesh: MeshId(1),
                        path: None,
                    },
                    Navigator {
                        id: NavigatorId(2),
                        key: NavigatorKey("key".into()),
                        location: PathPoint::new(100.0, 100.0),
                        mesh: MeshId(1),
                        path: Some(vec![PathPoint::new(0.0, 0.0)]),
                    },
                ],
            },
        },
        piloting: PilotingDomain {
            database: shared.piloting.clone(),
            state: PilotingState {
                spacecrafts: vec![Spacecraft {
                    id: SpacecraftId(1),
                    key: SpacecraftKey("key".into()),
                    commander: Some(CommanderId(42)),
                    pilot: None,
                }],
            },
        },
        farming: FarmingDomain {
            database: shared.farming.clone(),
            state: FarmingState {
                farmlands: vec![Farmland {
                    id: FarmlandId(1),
                    key: FarmlandKey("key".into()),
                    mapping: HashMap::new(),
                    fertile: true,
                }],
                plants: vec![Plant {
                    id: PlantId(1),
                    key: PlantKey("key".into()),
                    kind: PlantKind::Crop,
                    state: PlantState::Maturity {
                        growth: 1.0,
                        watered: 2.0,
                    },
                    farmland: FarmlandId(1),
                    harvests: 0,
                    place: Place::from((1, 1)),
                }],
            },
        },
        inventory: InventoryDomain {
            database: shared.inventory.clone(),
            state: InventoryState {
                containers: vec![Container {
                    id: ContainerId(1),
                    key: ContainerKey("key".into()),
                    size: 100,
                    slots: vec![],
                }],
                items: vec![Item {
                    id: ItemId(1),
                    key: ItemKey("key".into()),
                    container: ContainerId(1),
                    weight: 2.0,
                    functions: vec![
                        ItemFunction::Seed {
                            crop: GenericKey("garlic".into()),
                        },
                        ItemFunction::Consumable {
                            effects: vec![
                                Effect::RestoreEnergy { amount: 42.0 },
                                Effect::ReduceEnergy { amount: 42.0 },
                            ],
                        },
                    ],
                }],
            },
        },
        rules: Rules {
            usage_distance: 1.0,
        },
        players: vec![],
        time: Default::default(),

        status: GameStatus::Running,
        id: IdGenerator::new(1),
        metrics: Default::default(),
        observing: Default::default(),
    };

    master.save_game("test").unwrap();
    master.load_game("test").unwrap();

    // assert_that!(equals!(master.universe.database.characters.len(), 1));
    // assert_that!(equals!(master.physics.database.bodies.len(), 1));
    // assert_that!(equals!(master.universe.state.characters.len(), 1));
    // assert_that!(equals!(master.physics.state.bodies.len(), 2));
}
