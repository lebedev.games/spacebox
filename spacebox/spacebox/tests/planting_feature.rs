use farming::Farming::{LandChanged, PlantGrowthProgressed};
use farming::FarmingError::{LandAlreadyCultivated, LandInfertile};
use farming::PlantState::PlantingState;
use farming::{Land, Plant, PlantId, PlantKind};
use inventory::Inventory::ItemRemoved;
use physics::PhysicsError::DistantObjectsIsTooFar;
use physics::{Barrier, BarrierId, BarrierOrigin};
use spacebox::game::Action::{PlantCrop, UsePlanter};
use spacebox::game::Appearance::CropAppeared;
use spacebox::game::Event::{Appearance, Farming, Game, Inventory, Universe};
use spacebox::game::Game::ActionFailed;
use universe::Activity::Planting;
use universe::Universe::{ActivityBegun, ActivityStarted, TimeUpdated};
use universe::UniverseError::CharacterInvalidTask;
use universe::{Task, Time};

mod testing;

pub use testing::*;

#[test]
fn test_regular_planting() {
    GameTestScenario::new()
        // Arrange
        .given_player("Alice")
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_planter_type("planter")
        .given_planter("planter", "garden", "at Mars", (101, 100))
        .given_character_type("human")
        .given_character("Alice", "human", "farmer", "at Mars", (100, 100))
        .given_crop_type("garlic")
        .given_item_seed_type("garlic seeds", "garlic")
        .given_item("garlic seeds", "own seeds", "farmer", 10)
        // Act
        .when_player_use_planter("Alice", "farmer", "garden")
        .when_update_game_for_one_second()
        .when_player_plant_crop("Alice", "farmer", "own seeds", (100, 105))
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![
                Universe(ActivityBegun {
                    character: it.character("farmer"),
                    activity: Planting {
                        planter: it.planter("garden"),
                    },
                    time: Time(0),
                }),
                Universe(ActivityStarted {
                    character: it.character("farmer"),
                    activity: Planting {
                        planter: it.planter("garden"),
                    },
                }),
                Universe(TimeUpdated { ms: 100000 }),
                Inventory(ItemRemoved {
                    id: it.item("own seeds"),
                    container: it.container("farmer"),
                }),
                Farming(LandChanged {
                    place: (100, 105).into(),
                    land: Land::Cultivated {
                        plant: it.last_id().into(),
                    },
                    farmland: it.farmland("at Mars"),
                }),
                Appearance(CropAppeared {
                    id: it.last_id().into(),
                    key: "garlic".into(),
                    barrier: it.last_id().into(),
                    plant: it.last_id().into(),
                    state: PlantingState {
                        growth: 0.0,
                        watered: 0.0,
                    },
                    place: (100, 105).into(),
                    space: it.space("at Mars"),
                    position: (100, 105).into(),
                    rotation: 0.0,
                }),
            ]
        })
        .then_plant_should_be(PlantId(6), |it| Plant {
            id: it.last_id().into(),
            key: "garlic".into(),
            kind: PlantKind::Crop,
            state: PlantingState {
                growth: 0.0,
                watered: 0.0,
            },
            farmland: it.farmland("at Mars"),
            harvests: 0,
            place: (100, 105).into(),
        })
        .then_barrier_should_be(BarrierId(6), |it| Barrier {
            id: it.last_id().into(),
            key: "garlic".into(),
            space: it.space("at Mars"),
            position: (100, 105).into(),
            rotation: 0.0,
            size: (0, 0).into(),
            origin: BarrierOrigin::Crop,
        });
}

#[test]
fn test_plant_crop_on_existing_crop() {
    GameTestScenario::new()
        // Arrange
        .given_player("Alice")
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_planter_type("planter")
        .given_planter("planter", "garden", "at Mars", (101, 100))
        .given_character_type("human")
        .given_character("Alice", "human", "farmer", "at Mars", (100, 100))
        .given_crop_type("garlic")
        .given_item_seed_type("garlic seeds", "garlic")
        .given_item("garlic seeds", "own seeds", "farmer", 10)
        .given_crop("garlic", "existing", "at Mars", (100, 105))
        // Act
        .when_player_use_planter("Alice", "farmer", "garden")
        .when_update_game_for(1, game_hours())
        .when_player_plant_crop("Alice", "farmer", "own seeds", (100, 105))
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![
                Universe(ActivityBegun {
                    character: it.character("farmer"),
                    activity: Planting {
                        planter: it.planter("garden"),
                    },
                    time: Time(0),
                }),
                Universe(ActivityStarted {
                    character: it.character("farmer"),
                    activity: Planting {
                        planter: it.planter("garden"),
                    },
                }),
                Farming(PlantGrowthProgressed {
                    id: it.plant("existing"),
                    farmland: it.farmland("at Mars"),
                    growth: 0.1,
                    watered: 0.0,
                }),
                Universe(TimeUpdated { ms: 10000000 }),
                Game(ActionFailed {
                    character: it.character("farmer"),
                    action: PlantCrop {
                        seed: it.item("own seeds"),
                        position: (100, 105).into(),
                        rotation: 0.0,
                    },
                    error: LandAlreadyCultivated {
                        place: (100, 105).into(),
                        plant: it.plant("existing"),
                    }
                    .into(),
                }),
            ]
        });
}

#[test]
fn test_plant_crop_on_existing_prop() {
    GameTestScenario::new()
        // Arrange
        .given_player("Alice")
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_planter_type("planter")
        .given_planter("planter", "garden", "at Mars", (101, 100))
        .given_character_type("human")
        .given_character("Alice", "human", "farmer", "at Mars", (100, 100))
        .given_crop_type("garlic")
        .given_item_seed_type("garlic seeds", "garlic")
        .given_item("garlic seeds", "own seeds", "farmer", 10)
        .given_prop_type("prop", (2, 2))
        .given_prop("prop", "cabinet", "at Mars", (100, 105))
        // Act
        .when_player_use_planter("Alice", "farmer", "garden")
        .when_update_game_for(1, game_hours())
        .when_player_plant_crop("Alice", "farmer", "own seeds", (100, 105))
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![
                Universe(ActivityBegun {
                    character: it.character("farmer"),
                    activity: Planting {
                        planter: it.planter("garden"),
                    },
                    time: Time(0),
                }),
                Universe(ActivityStarted {
                    character: it.character("farmer"),
                    activity: Planting {
                        planter: it.planter("garden"),
                    },
                }),
                Universe(TimeUpdated { ms: 10000000 }),
                Game(ActionFailed {
                    character: it.character("farmer"),
                    action: PlantCrop {
                        seed: it.item("own seeds"),
                        position: (100, 105).into(),
                        rotation: 0.0,
                    },
                    error: LandInfertile {
                        place: (100, 105).into(),
                    }
                    .into(),
                }),
            ]
        });
}

#[test]
fn test_use_planter_on_distance() {
    GameTestScenario::new()
        // Arrange
        .given_player("Alice")
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_planter_type("planter")
        .given_planter("planter", "garden", "at Mars", (200, 100))
        .given_character_type("human")
        .given_character("Alice", "human", "farmer", "at Mars", (100, 100))
        // Act
        .when_player_use_planter("Alice", "farmer", "garden")
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![Game(ActionFailed {
                character: it.character("farmer"),
                action: UsePlanter {
                    planter: it.planter("garden"),
                },
                error: DistantObjectsIsTooFar.into(),
            })]
        });
}

#[test]
fn test_plant_crop_out_of_planter() {
    GameTestScenario::new()
        // Arrange
        .given_player("Alice")
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_planter_type("planter")
        .given_planter("planter", "garden", "at Mars", (101, 100))
        .given_character_type("human")
        .given_character("Alice", "human", "farmer", "at Mars", (100, 100))
        .given_crop_type("garlic")
        .given_item_seed_type("garlic seeds", "garlic")
        .given_item("garlic seeds", "own seeds", "farmer", 10)
        // Act
        .when_player_plant_crop("Alice", "farmer", "own seeds", (100, 105))
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![Game(ActionFailed {
                character: it.character("farmer"),
                action: PlantCrop {
                    seed: it.item("own seeds"),
                    position: (100, 105).into(),
                    rotation: 0.0,
                },
                error: CharacterInvalidTask { task: Task::Idle }.into(),
            })]
        });
}
