use std::collections::HashMap;
use std::rc::Rc;
use std::time::Duration;

use env_logger::Env;
use farming::PlantState::Harvest;
use farming::{
    FarmingDatabase, Farmland, FarmlandId, FarmlandKey, FarmlandType, Plant, PlantId, PlantKind,
    PlantType,
};
use inventory::{
    ContainerId, ContainerType, GenericKey, InventoryDatabase, Item, ItemFunction, ItemId, ItemType,
};
use log::LevelFilter;
use navigation::{
    Fragment, FragmentId, Mesh, MeshId, MeshKey, MeshType, NavigationDatabase, Navigator,
    NavigatorId, NavigatorType, Obstacle, ObstacleId, ObstacleKey, ObstacleType, PathPoint, Point,
    RouteStatus,
};
use physics::{
    Barrier, BarrierId, BarrierKey, BarrierOrigin, BarrierType, Body, BodyId, BodyKey, BodyOrigin,
    BodyType, PhysicsDatabase, Space, SpaceId, SpaceKey, SpaceOrigin, SpaceType, SteeringBehaviour,
    Vector,
};
use piloting::{PilotingDatabase, Spacecraft, SpacecraftId, SpacecraftKey, SpacecraftType};
use spacebox::game::Action::{
    Build, CancelActivity, DesignBuilding, HarvestCrop, LandSpaceship, LaunchSpaceship,
    MoveCharacter, MoveSpaceship, PlantCrop, StoreItem, TakeItem, UseBed, UsePlanter, UseStorage,
    UseWorkbench,
};
use spacebox::game::Event;
use spacebox::game::Observing;
use spacebox::game::{Action, Interaction, Master, Message, TrustedMessage};
use spacebox::game::{Player, PlayerId, PlayerRole, PlayerStatus};
use testit::*;
use universe::{
    Activity, BedId, BedType, BridgeId, BridgeType, CharacterId, CharacterKey, CharacterType,
    Colonization, Colony, ColonyId, ColonyKey, ColonyType, ConstructionId, ConstructionProject,
    ConstructionType, Crop, CropId, CropType, Deck, DeckId, DeckKey, Group, GroupId, Landing,
    Planet, PlanetId, PlanetType, PlanterId, PlanterType, PropId, PropKey, PropType, Spaceship,
    SpaceshipId, SpaceshipKey, SpaceshipType, StorageId, StorageType, System, SystemId, SystemType,
    Task, UniverseDatabase, WorkbenchId, WorkbenchType,
};

pub fn real_seconds() -> Duration {
    Duration::from_secs_f32(1.0)
}

pub fn _game_minutes() -> Duration {
    Duration::from_secs_f32(1.0)
}

pub fn game_hours() -> Duration {
    Duration::from_secs_f32(100.0)
}

pub struct GameTestScenario {
    named_spaces: HashMap<String, SpaceId>,
    named_characters: HashMap<String, CharacterId>,
    named_bodies: HashMap<String, BodyId>,
    named_barriers: HashMap<String, BarrierId>,
    named_groups: HashMap<String, GroupId>,
    named_colonies: HashMap<String, ColonyId>,
    named_systems: HashMap<String, SystemId>,
    named_decks: HashMap<String, DeckId>,
    named_spaceships: HashMap<String, SpaceshipId>,
    named_spacecrafts: HashMap<String, SpacecraftId>,
    named_meshes: HashMap<String, MeshId>,
    named_props: HashMap<String, PropId>,
    named_farmlands: HashMap<String, FarmlandId>,
    named_containers: HashMap<String, ContainerId>,
    named_workbenches: HashMap<String, WorkbenchId>,
    named_constructions: HashMap<String, ConstructionId>,
    named_beds: HashMap<String, BedId>,
    named_planters: HashMap<String, PlanterId>,
    named_storages: HashMap<String, StorageId>,
    named_bridges: HashMap<String, BridgeId>,
    named_items: HashMap<String, ItemId>,
    named_crops: HashMap<String, CropId>,
    named_plants: HashMap<String, PlantId>,
    named_planets: HashMap<String, PlanetId>,
    named_navigators: HashMap<String, NavigatorId>,
    master: Master,
}

impl GameTestScenario {
    pub fn new() -> Self {
        let _ = env_logger::Builder::from_env(Env::default())
            .filter_level(LevelFilter::Debug)
            .try_init();

        GameTestScenario {
            named_spaces: HashMap::new(),
            named_characters: HashMap::new(),
            named_bodies: HashMap::new(),
            named_colonies: HashMap::new(),
            named_groups: HashMap::new(),
            named_systems: HashMap::new(),
            named_decks: HashMap::new(),
            named_spaceships: HashMap::new(),
            named_spacecrafts: HashMap::new(),
            named_props: HashMap::new(),
            named_meshes: HashMap::new(),
            master: Master::new("".into()),
            named_farmlands: HashMap::new(),
            named_containers: HashMap::new(),
            named_workbenches: HashMap::new(),
            named_constructions: HashMap::new(),
            named_beds: HashMap::new(),
            named_planters: HashMap::new(),
            named_storages: HashMap::new(),
            named_bridges: HashMap::new(),
            named_items: HashMap::new(),
            named_crops: HashMap::new(),
            named_plants: HashMap::new(),
            named_planets: HashMap::new(),
            named_navigators: HashMap::new(),
            named_barriers: HashMap::new(),
        }
    }

    pub fn body(&self, name: &str) -> BodyId {
        self.named_bodies
            .get(name.into())
            .expect(&format!("test named body '{}' not specified", name))
            .clone()
    }

    pub fn barrier(&self, name: &str) -> BarrierId {
        self.named_barriers
            .get(name.into())
            .expect(&format!("test named barrier '{}' not specified", name))
            .clone()
    }

    pub fn container(&self, name: &str) -> ContainerId {
        self.named_containers
            .get(name.into())
            .expect(&format!("test named container '{}' not specified", name))
            .clone()
    }

    pub fn group(&self, name: &str) -> GroupId {
        self.named_groups
            .get(name.into())
            .expect(&format!("test named group '{}' not specified", name))
            .clone()
    }

    pub fn plant(&self, name: &str) -> PlantId {
        self.named_plants
            .get(name.into())
            .expect(&format!("test named plant '{}' not specified", name))
            .clone()
    }

    pub fn spacecraft(&self, name: &str) -> SpacecraftId {
        self.named_spacecrafts
            .get(name.into())
            .expect(&format!("test named spacecraft '{}' not specified", name))
            .clone()
    }

    pub fn space(&self, name: &str) -> SpaceId {
        self.named_spaces
            .get(name.into())
            .expect(&format!("test named space '{}' not specified", name))
            .clone()
    }

    pub fn colony(&self, name: &str) -> ColonyId {
        self.named_colonies
            .get(name.into())
            .expect(&format!("test named colony '{}' not specified", name))
            .clone()
    }

    pub fn mesh(&self, name: &str) -> MeshId {
        self.named_meshes
            .get(name.into())
            .expect(&format!("test named mesh '{}' not specified", name))
            .clone()
    }

    pub fn navigator(&self, name: &str) -> NavigatorId {
        self.named_navigators
            .get(name.into())
            .expect(&format!("test named navigator '{}' not specified", name))
            .clone()
    }

    pub fn spaceship(&self, name: &str) -> SpaceshipId {
        self.named_spaceships
            .get(name.into())
            .expect(&format!("test named spaceship '{}' not specified", name))
            .clone()
    }

    pub fn construction(&self, name: &str) -> ConstructionId {
        self.named_constructions
            .get(name.into())
            .expect(&format!("test named construction '{}' not specified", name))
            .clone()
    }

    pub fn character(&self, name: &str) -> CharacterId {
        self.named_characters
            .get(name.into())
            .expect(&format!("test named character '{}' not specified", name))
            .clone()
    }

    pub fn deck(&self, name: &str) -> DeckId {
        self.named_decks
            .get(name.into())
            .expect(&format!("test named deck '{}' not specified", name))
            .clone()
    }

    pub fn crop(&self, name: &str) -> CropId {
        self.named_crops
            .get(name.into())
            .expect(&format!("test named crop '{}' not specified", name))
            .clone()
    }

    pub fn item(&self, name: &str) -> ItemId {
        self.named_items
            .get(name.into())
            .expect(&format!("test named item '{}' not specified", name))
            .clone()
    }

    pub fn planter(&self, name: &str) -> PlanterId {
        self.named_planters
            .get(name.into())
            .expect(&format!("test named planter '{}' not specified", name))
            .clone()
    }

    pub fn storage(&self, name: &str) -> StorageId {
        self.named_storages
            .get(name.into())
            .expect(&format!("test named storage '{}' not specified", name))
            .clone()
    }

    pub fn bridge(&self, name: &str) -> BridgeId {
        self.named_bridges
            .get(name.into())
            .expect(&format!("test named bridge '{}' not specified", name))
            .clone()
    }

    pub fn bed(&self, name: &str) -> BedId {
        self.named_beds
            .get(name.into())
            .expect(&format!("test named bed '{}' not specified", name))
            .clone()
    }

    pub fn workbench(&self, name: &str) -> WorkbenchId {
        self.named_workbenches
            .get(name.into())
            .expect(&format!("test named workbench '{}' not specified", name))
            .clone()
    }

    pub fn farmland(&self, name: &str) -> FarmlandId {
        self.named_farmlands
            .get(name.into())
            .expect(&format!("test named farmland '{}' not specified", name))
            .clone()
    }

    pub fn system(&self, name: &str) -> SystemId {
        self.named_systems
            .get(name.into())
            .expect(&format!("test named system '{}' not specified", name))
            .clone()
    }

    pub fn planet(&self, name: &str) -> PlanetId {
        self.named_planets
            .get(name.into())
            .expect(&format!("test named planet '{}' not specified", name))
            .clone()
    }

    pub fn player(&self, name: &str) -> PlayerId {
        PlayerId(name.into())
    }

    pub fn background<F>(self, factory: F) -> Self
    where
        F: Fn(Self) -> Self,
    {
        factory(self)
    }

    pub fn universe_database(&mut self) -> &mut UniverseDatabase {
        Rc::get_mut(&mut self.master.universe.database).unwrap()
    }

    pub fn navigation_database(&mut self) -> &mut NavigationDatabase {
        Rc::get_mut(&mut self.master.navigation.database).unwrap()
    }

    pub fn farming_database(&mut self) -> &mut FarmingDatabase {
        Rc::get_mut(&mut self.master.farming.database).unwrap()
    }

    pub fn physics_database(&mut self) -> &mut PhysicsDatabase {
        Rc::get_mut(&mut self.master.physics.database).unwrap()
    }

    pub fn piloting_database(&mut self) -> &mut PilotingDatabase {
        Rc::get_mut(&mut self.master.piloting.database).unwrap()
    }

    pub fn inventory_database(&mut self) -> &mut InventoryDatabase {
        Rc::get_mut(&mut self.master.inventory.database).unwrap()
    }

    pub fn given_characters_distribution(
        mut self,
        characters: usize,
        players: usize,
        spaces: usize,
    ) -> Self {
        for player in 0..players {
            self = self.given_player(&player.to_string());
        }
        self = self.given_colony_type("colony", (200, 200));
        for space in 0..spaces {
            self = self.given_colony("colony", &space.to_string());
        }
        self = self.given_character_type("human");
        for character in 0..characters {
            let player = character % players;
            let space = character % spaces;
            self = self.given_character(
                &player.to_string(),
                "human",
                &character.to_string(),
                &space.to_string(),
                (100, 100).into(),
            );
        }
        self
    }

    pub fn given_player(mut self, name: &str) -> Self {
        let group = self.master.players.len().into();
        self.master.players.push(Player {
            id: PlayerId(name.into()),
            group: Some(group),
            origin: name.into(),
            status: PlayerStatus::Active,
            events: vec![],
            observer: false,
            backend: 0,
            role: PlayerRole::Player,
        });
        self.master.universe.state.groups.push(Group {
            id: group,
            name: format!("Group of {}", name),
            playable: true,
        });
        self.named_groups.insert(name.into(), group);
        self
    }

    pub fn given_players(mut self, names: &[&str]) -> Self {
        for name in names {
            self = self.given_player(name);
        }
        self
    }

    pub fn given_system_type(mut self, key: &str) -> Self {
        self.navigation_database().meshes.push(MeshType {
            key: key.into(),
            size: (1000, 1000).into(),
        });
        let database = Rc::get_mut(&mut self.master.universe.database).unwrap();

        database.systems.push(SystemType {
            key: key.into(),
            space: key.into(),
            mesh: key.into(),
        });
        self.physics_database()
            .spaces
            .push(SpaceType { key: key.into() });
        self
    }

    pub fn given_system(mut self, key: &str, name: &str) -> Self {
        let id = self.master.id.generate_one();
        let mesh = self
            .master
            .navigation
            .prepare_mesh(id.into(), &key.into())
            .unwrap();
        self.master.navigation.state.meshes.push(mesh);
        self.master.universe.state.systems.push(System {
            id: id.into(),
            key: key.into(),
            space: id.into(),
            mesh: id.into(),
        });
        self.master.physics.state.spaces.push(Space {
            id: id.into(),
            key: key.into(),
            origin: SpaceOrigin::System,
            parent: None,
        });
        self.named_meshes.insert(name.into(), id.into());
        self.named_spaces.insert(name.into(), id.into());
        self.named_systems.insert(name.into(), id.into());
        self
    }

    pub fn given_planet_type(mut self, key: &str) -> Self {
        self.universe_database().planets.push(PlanetType {
            key: key.into(),
            body: key.into(),
        });
        self.physics_database().bodies.push(BodyType {
            key: key.into(),
            origin: BodyOrigin::Planet,
            speed: 0.0,
        });
        self
    }

    pub fn given_planet(
        mut self,
        key: &str,
        name: &str,
        space: &str,
        position: (i32, i32),
    ) -> Self {
        let id = self.master.id.generate_one();
        self.master.universe.state.planets.push(Planet {
            id: id.into(),
            key: key.into(),
            body: id.into(),
        });
        self.master.physics.state.bodies.push(Body {
            id: id.into(),
            key: key.into(),
            space: self.space(space),
            origin: BodyOrigin::Planet,
            position: position.into(),
            velocity: Vector::ZERO,
            speed: 0.0,
            steering: None,
        });
        self.named_planets.insert(name.into(), id.into());
        self.named_bodies.insert(name.into(), id.into());
        self
    }

    pub fn given_colony_type(mut self, key: &str, size: (usize, usize)) -> Self {
        self.universe_database().colonies.push(ColonyType {
            key: ColonyKey(key.into()),
            space: SpaceKey(key.into()),
            farmland: FarmlandKey(key.into()),
            mesh: MeshKey(key.into()),
        });
        self.navigation_database().meshes.push(MeshType {
            key: MeshKey(key.into()),
            size: size.into(),
        });
        self.farming_database().farmlands.push(FarmlandType {
            key: FarmlandKey(key.into()),
            size: size.into(),
            fertile: true,
        });
        self.physics_database().spaces.push(SpaceType {
            key: SpaceKey(key.into()),
        });
        self
    }

    pub fn given_colonization(mut self, planet: &str, colony: &str) -> Self {
        let planet = self.planet(planet);
        let colony = self.colony(colony);
        self.master
            .universe
            .state
            .colonizations
            .push(Colonization { colony, planet });
        self
    }

    pub fn given_colony(mut self, key: &str, name: &str) -> Self {
        let master = &mut self.master;
        let id = master.id.generate_one();
        let universe = &mut master.universe;
        let physics = &mut master.physics;
        let navigation = &mut master.navigation;
        let farming = &mut master.farming;

        let colony = universe.get_colony_type(key.into()).unwrap();
        let space = physics
            .prepare_space(id.into(), &colony.space, SpaceOrigin::Colony)
            .unwrap();
        let mesh = navigation.prepare_mesh(id.into(), &colony.mesh).unwrap();
        let farmland = farming
            .prepare_farmland(id.into(), &colony.farmland)
            .unwrap();

        let colony = Colony {
            id: id.into(),
            key: key.into(),
            space: space.id,
            mesh: mesh.id,
            farmland: farmland.id,
        };

        universe.state.colonies.push(colony);
        physics.create_space(space);
        navigation.create_mesh(mesh);
        farming.create_farmland(farmland);

        self.named_spaces.insert(name.into(), id.into());
        self.named_colonies.insert(name.into(), id.into());
        self.named_meshes.insert(name.into(), id.into());
        self.named_farmlands.insert(name.into(), id.into());
        self
    }

    pub fn given_spaceship_type(mut self, key: &str) -> Self {
        self.universe_database().spaceships.push(SpaceshipType {
            key: key.into(),
            spacecraft: key.into(),
            body: key.into(),
            navigator: key.into(),
        });
        self.navigation_database()
            .navigators
            .push(NavigatorType { key: key.into() });
        self.piloting_database().spacecrafts.push(SpacecraftType {
            key: SpacecraftKey(key.into()),
        });
        self.physics_database().bodies.push(BodyType {
            key: BodyKey(key.into()),
            origin: BodyOrigin::Spaceship,
            speed: 500.0,
        });
        self
    }

    pub fn given_construction_type(
        mut self,
        key: &str,
        days_to_construct: f32,
        project: ConstructionProject,
    ) -> Self {
        self.universe_database()
            .constructions
            .push(ConstructionType {
                key: key.into(),
                project,
                days_to_construct,
            });
        self
    }

    pub fn given_prop_construction_type(mut self, key: &str) -> Self {
        self.universe_database()
            .constructions
            .push(ConstructionType {
                key: key.into(),
                project: ConstructionProject::PropProject { key: key.into() },
                days_to_construct: 0.0,
            });
        self
    }

    pub fn given_construction(
        mut self,
        key: &str,
        name: &str,
        space: &str,
        position: (usize, usize),
    ) -> Self {
        let id = self.master.id.generate_one();
        let rotation = 0.0;
        let operation = self
            .master
            .prepare_construction(id, key.into(), self.space(space), position.into(), rotation)
            .unwrap();
        self.master.create_construction(operation);
        self.named_constructions.insert(name.into(), id.into());
        self
    }

    pub fn given_workbench_type(mut self, key: &str) -> Self {
        let size = (1, 1);
        self.universe_database().workbenches.push(WorkbenchType {
            key: key.into(),
            barrier: key.into(),
            obstacle: key.into(),
            relative_entry: (0, 0).into(),
        });
        self.physics_database().barriers.push(BarrierType {
            key: key.into(),
            size: size.into(),
        });
        self.navigation_database().obstacles.push(ObstacleType {
            key: key.into(),
            size: size.into(),
        });
        self
    }

    pub fn given_workbench(
        mut self,
        key: &str,
        name: &str,
        space: &str,
        position: (usize, usize),
    ) -> Self {
        let id = self.master.id.generate_one();
        let rotation = 0.0;
        let operation = self
            .master
            .create_workbench(
                id,
                &key.into(),
                self.space(space),
                position.into(),
                rotation,
            )
            .unwrap();
        self.master.apply_construction_complete(operation);
        self.named_workbenches.insert(name.into(), id.into());
        self
    }

    pub fn given_storage_type(mut self, key: &str) -> Self {
        let size = (1, 1);
        self.universe_database().storages.push(StorageType {
            key: key.into(),
            barrier: key.into(),
            obstacle: key.into(),
            container: key.into(),
            relative_entry: (0, 0).into(),
        });
        self.physics_database().barriers.push(BarrierType {
            key: key.into(),
            size: size.into(),
        });
        self.navigation_database().obstacles.push(ObstacleType {
            key: key.into(),
            size: size.into(),
        });
        self.inventory_database().containers.push(ContainerType {
            key: key.into(),
            size: 100,
        });
        self
    }

    pub fn given_storage(
        mut self,
        key: &str,
        name: &str,
        space: &str,
        position: (usize, usize),
    ) -> Self {
        let id = self.master.id.generate_one();
        let rotation = 0.0;
        let operation = self
            .master
            .create_storage(
                id,
                &key.into(),
                self.space(space),
                position.into(),
                rotation,
            )
            .unwrap();
        self.master.apply_construction_complete(operation);
        self.named_storages.insert(name.into(), id.into());
        self.named_containers.insert(name.into(), id.into());
        self
    }

    pub fn given_planter_type(mut self, key: &str) -> Self {
        let size = (1, 1);
        self.universe_database().planters.push(PlanterType {
            key: key.into(),
            barrier: key.into(),
            obstacle: key.into(),
            relative_entry: (0, 0).into(),
        });
        self.physics_database().barriers.push(BarrierType {
            key: key.into(),
            size: size.into(),
        });
        self.navigation_database().obstacles.push(ObstacleType {
            key: key.into(),
            size: size.into(),
        });
        self
    }

    pub fn given_planter(
        mut self,
        key: &str,
        name: &str,
        space: &str,
        position: (usize, usize),
    ) -> Self {
        let id = self.master.id.generate_one();
        let rotation = 0.0;
        let operation = self
            .master
            .create_planter(
                id,
                &key.into(),
                self.space(space),
                position.into(),
                rotation,
            )
            .unwrap();
        self.master.apply_construction_complete(operation);
        self.named_planters.insert(name.into(), id.into());
        self
    }

    pub fn given_bridge_type(mut self, key: &str) -> Self {
        let size = (1, 1);
        self.universe_database().bridges.push(BridgeType {
            key: key.into(),
            barrier: key.into(),
            obstacle: key.into(),
            relative_entry: (0, 0).into(),
        });
        self.physics_database().barriers.push(BarrierType {
            key: key.into(),
            size: size.into(),
        });
        self.navigation_database().obstacles.push(ObstacleType {
            key: key.into(),
            size: size.into(),
        });
        self
    }

    pub fn given_bridge(
        mut self,
        key: &str,
        name: &str,
        space: &str,
        position: (usize, usize),
    ) -> Self {
        let id = self.master.id.generate_one();
        let rotation = 0.0;
        let operation = self
            .master
            .create_bridge(
                id,
                &key.into(),
                self.space(space),
                position.into(),
                rotation,
            )
            .unwrap();
        self.master.apply_construction_complete(operation);
        self.named_bridges.insert(name.into(), id.into());
        self.named_barriers.insert(name.into(), id.into());
        self
    }

    pub fn given_prop_type(mut self, key: &str, size: (usize, usize)) -> Self {
        self.universe_database().props.push(PropType {
            key: PropKey(key.into()),
            barrier: BarrierKey(key.into()),
            obstacle: ObstacleKey(key.into()),
        });
        self.physics_database().barriers.push(BarrierType {
            key: BarrierKey(key.into()),
            size: size.into(),
        });
        self.navigation_database().obstacles.push(ObstacleType {
            key: ObstacleKey(key.into()),
            size: size.into(),
        });
        self
    }

    pub fn given_prop(
        mut self,
        key: &str,
        name: &str,
        space: &str,
        position: (usize, usize),
    ) -> Self {
        let id = self.master.id.generate_one();
        let rotation = 0.0;
        let operation = self
            .master
            .create_prop(
                id,
                &key.into(),
                self.space(space),
                position.into(),
                rotation,
            )
            .unwrap();
        self.master.apply_construction_complete(operation);
        self.named_props.insert(name.into(), id.into());
        self
    }

    pub fn given_bed_type(mut self, key: &str, size: (usize, usize)) -> Self {
        self.universe_database().beds.push(BedType {
            key: key.into(),
            barrier: key.into(),
            obstacle: key.into(),
            relative_entry: (0, 0).into(),
        });
        self.physics_database().barriers.push(BarrierType {
            key: key.into(),
            size: size.into(),
        });
        self.navigation_database().obstacles.push(ObstacleType {
            key: key.into(),
            size: size.into(),
        });
        self
    }

    pub fn given_bed(
        mut self,
        key: &str,
        name: &str,
        space: &str,
        position: (usize, usize),
    ) -> Self {
        let id = self.master.id.generate_one();
        let rotation = 0.0;
        let operation = self
            .master
            .create_bed(
                id,
                &key.into(),
                self.space(space),
                position.into(),
                rotation,
            )
            .unwrap();
        self.master.apply_construction_complete(operation);
        self.named_beds.insert(name.into(), id.into());
        self
    }

    pub fn given_spaceship_deck(
        mut self,
        key: &str,
        name: &str,
        system_space: &str,
        position: (usize, usize),
    ) -> Self {
        let id = self.master.id.generate_one();
        self.master.universe.state.spaceships.push(Spaceship {
            id: id.into(),
            key: SpaceshipKey(key.into()),
            spacecraft: id.into(),
            body: id.into(),
            landing: None,
            navigator: id.into(),
        });
        self.master.navigation.state.navigators.push(Navigator {
            id: id.into(),
            key: key.into(),
            location: PathPoint::new(position.0 as f32, position.1 as f32),
            mesh: self.mesh(system_space),
            path: None,
        });
        self.master.physics.state.bodies.push(Body {
            id: id.into(),
            key: BodyKey(key.into()),
            space: self.space(system_space),
            origin: BodyOrigin::Spaceship,
            position: position.into(),
            velocity: Vector::ZERO,
            speed: 100.0,
            steering: None,
        });
        self.master.piloting.state.spacecrafts.push(Spacecraft {
            id: id.into(),
            key: SpacecraftKey(key.into()),
            commander: None,
            pilot: None,
        });
        self.master.universe.state.decks.push(Deck {
            id: id.into(),
            key: DeckKey(key.into()),
            space: id.into(),
            farmland: id.into(),
            mesh: id.into(),
            spaceship: id.into(),
        });
        self.master.physics.state.spaces.push(Space {
            id: id.into(),
            key: SpaceKey(key.into()),
            origin: SpaceOrigin::Deck,
            parent: None,
        });
        self.master.farming.state.farmlands.push(Farmland {
            id: id.into(),
            key: key.into(),
            mapping: Default::default(),
            fertile: false,
        });
        self.navigation_database().meshes.push(MeshType {
            key: key.into(),
            size: Point::new(100, 100),
        });
        self.master.navigation.state.meshes.push(Mesh {
            id: id.into(),
            key: key.into(),
            counter: 0,
            size: Point::new(100, 100),
            fragments: vec![Fragment {
                id: FragmentId(0),
                position: (0, 0).into(),
                size: Point::new(100, 100),
                obstacle: None,
            }],
            links: vec![],
            obstacles: vec![],
        });
        self.named_spaceships.insert(name.into(), id.into());
        self.named_bodies.insert(name.into(), id.into());
        self.named_spacecrafts.insert(name.into(), id.into());
        self.named_spaces.insert(format!("on {}", name), id.into());
        self.named_decks.insert(format!("on {}", name), id.into());
        self.named_meshes.insert(format!("on {}", name), id.into());
        self.named_farmlands
            .insert(format!("on {}", name), id.into());

        self.master
            .observing
            .append_link(id.into(), self.space(system_space));

        self.given_bridge_type("bridge").given_bridge(
            "bridge",
            &format!("bridge of {}", name),
            &format!("on {}", name),
            (50, 50),
        )
    }

    pub fn given_crop_type_params(
        mut self,
        key: &str,
        growth_days: f32,
        harvest_time: f32,
    ) -> Self {
        self.universe_database().crops.push(CropType {
            key: key.into(),
            plant: key.into(),
            consumable: key.into(),
            barrier: key.into(),
            production: 1.0,
        });
        self.farming_database().plants.push(PlantType {
            key: key.into(),
            days_to_maturity: growth_days,
            days_to_harvest: growth_days,
            max_harvests: None,
            water_per_day: 0.0,
            harvest_time,
        });
        self.physics_database().barriers.push(BarrierType {
            key: key.into(),
            size: Vector::ZERO,
        });
        self.inventory_database().items.push(ItemType {
            key: key.into(),
            able_to_stack: true,
            weight: 0.0,
            functions: vec![ItemFunction::Consumable { effects: vec![] }],
        });
        self
    }

    pub fn given_crop_type(self, key: &str) -> Self {
        self.given_crop_type_params(key, 1.0, 0.5)
    }

    pub fn given_crop_ready_harvest(
        mut self,
        key: &str,
        name: &str,
        space: &str,
        position: (usize, usize),
    ) -> Self {
        let crop = self.master.universe.get_crop_type(&key.into()).unwrap();
        let plant = self.master.farming.get_plant_type(&crop.plant).unwrap();
        let barrier = self.master.physics.get_barrier_type(&crop.barrier).unwrap();
        let id = self.master.id.generate_one();
        let crop = Crop {
            id: id.into(),
            key: crop.key.clone(),
            plant: id.into(),
            barrier: id.into(),
        };
        let plant = Plant {
            id: id.into(),
            key: plant.key.clone(),
            kind: PlantKind::Crop,
            state: Harvest {
                time: 0.0,
                watered: 1.0,
            },
            farmland: self.farmland(space),
            harvests: 0,
            place: position.into(),
        };
        let barrier = Barrier {
            id: id.into(),
            key: barrier.key.clone(),
            space: self.space(space),
            position: position.into(),
            rotation: 0.0,
            size: barrier.size,
            origin: BarrierOrigin::Crop,
        };
        self.master.universe.state.crops.push(crop);
        self.master.farming.state.plants.push(plant);
        self.master.physics.state.barriers.push(barrier);
        self.named_crops.insert(name.into(), id.into());
        self.named_plants.insert(name.into(), id.into());
        self
    }

    pub fn given_crop(
        mut self,
        key: &str,
        name: &str,
        space: &str,
        position: (usize, usize),
    ) -> Self {
        let id = self.master.id.generate_one();
        let rotation = 0.0;
        let operation = self
            .master
            .create_crop(
                id,
                &key.into(),
                self.space(space),
                position.into(),
                rotation,
            )
            .unwrap();
        self.master.apply_create_crop(operation);
        self.named_crops.insert(name.into(), id.into());
        self.named_plants.insert(name.into(), id.into());
        self
    }

    pub fn given_item_type(mut self, key: &str, functions: Vec<ItemFunction>) -> Self {
        self.inventory_database().items.push(ItemType {
            key: key.into(),
            able_to_stack: true,
            weight: 0.0,
            functions,
        });
        self
    }

    pub fn given_item_seed_type(mut self, key: &str, crop: &str) -> Self {
        self.inventory_database().items.push(ItemType {
            key: key.into(),
            able_to_stack: true,
            weight: 0.0,
            functions: vec![ItemFunction::Seed {
                crop: GenericKey(crop.into()),
            }],
        });
        self
    }

    pub fn given_item_building_recipe_type(mut self, key: &str, construction: &str) -> Self {
        self.inventory_database().items.push(ItemType {
            key: key.into(),
            able_to_stack: true,
            weight: 0.0,
            functions: vec![ItemFunction::BuildingRecipe {
                construction: GenericKey(construction.into()),
            }],
        });
        self
    }

    pub fn given_item(mut self, key: &str, name: &str, container: &str, _quantity: usize) -> Self {
        let container = self.container(container);
        let item = self.master.inventory.get_item_type(&key.into()).unwrap();
        let id = self.master.id.generate_one();
        let item = Item {
            id: id.into(),
            key: item.key.clone(),
            container,
            weight: item.weight,
            functions: item.functions.clone(),
        };
        self.master.inventory.state.items.push(item);
        self.named_items.insert(name.into(), id.into());
        self
    }

    pub fn given_character_type(mut self, key: &str) -> Self {
        self.universe_database().characters.push(CharacterType {
            key: CharacterKey(key.into()),
            body: BodyKey(key.into()),
            health_limit: 100.0,
            energy_limit: 100.0,
            stress_limit: 100.0,
            container: key.into(),
            navigator: key.into(),
        });
        self.inventory_database().containers.push(ContainerType {
            key: key.into(),
            size: 100,
        });
        self.navigation_database()
            .navigators
            .push(NavigatorType { key: key.into() });
        self.physics_database().bodies.push(BodyType {
            key: key.into(),
            origin: BodyOrigin::Character,
            speed: 5.0,
        });
        self
    }

    pub fn given_character_energy(mut self, character: &str, energy: f32) -> Self {
        let character = self.character(character);
        let character = self.master.universe.get_character_mut(character).unwrap();
        character.energy = energy;
        self
    }

    pub fn given_character(
        mut self,
        player: &str,
        key: &str,
        name: &str,
        space: &str,
        position: (usize, usize),
    ) -> Self {
        self.master
            .spawn_player_character(
                CharacterKey(key.into()),
                self.group(player),
                self.space(space),
                position.into(),
            )
            .unwrap();
        self.named_characters
            .insert(name.into(), self.master.id.last().into());
        self.named_bodies
            .insert(name.into(), self.master.id.last().into());
        self.named_containers
            .insert(name.into(), self.master.id.last().into());
        self.named_navigators
            .insert(name.into(), self.master.id.last().into());
        self
    }

    pub fn given_character_command(mut self, character: &str, bridge: &str) -> Self {
        let character = self.character(character);
        let bridge = self.bridge(bridge);
        let character = self.master.universe.get_character_mut(character).unwrap();
        character.task = Task::Running {
            activity: Activity::SpaceshipCommand { bridge },
        };
        self
    }

    pub fn given_character_building_design(mut self, character: &str, workbench: &str) -> Self {
        let character = self.character(character);
        let workbench = self.workbench(workbench);
        let character = self.master.universe.get_character_mut(character).unwrap();
        character.task = Task::Running {
            activity: Activity::BuildingDesign { workbench },
        };
        self
    }

    pub fn given_character_storage_usage(mut self, character: &str, storage: &str) -> Self {
        let character = self.character(character);
        let storage = self.storage(storage);
        let character = self.master.universe.get_character_mut(character).unwrap();
        character.task = Task::Running {
            activity: Activity::StorageUsage { storage },
        };
        self
    }

    pub fn given_spaceship_landing(
        mut self,
        character: &str,
        bridge: &str,
        colony: &str,
        landing_point: (i32, i32),
    ) -> Self {
        self = self.given_character_command(character, bridge);
        let character = self.character(character);
        let colony = self.colony(colony);
        self.master
            .land_spaceship(character, colony, landing_point.into())
            .unwrap();
        self
    }

    pub fn when_player_land_spaceship(
        mut self,
        player: &str,
        character: &str,
        colony: &str,
        landing_place: (usize, usize),
    ) -> Self {
        let action = LandSpaceship {
            position: landing_place.into(),
            colony: self.colony(colony),
        };
        self.send_action(player, character, action);
        self
    }

    pub fn when_player_cancel_activity(mut self, player: &str, character: &str) -> Self {
        let action = CancelActivity;
        self.send_action(player, character, action);
        self
    }

    pub fn when_player_use_planter(mut self, player: &str, character: &str, planter: &str) -> Self {
        let action = UsePlanter {
            planter: self.planter(planter),
        };
        self.send_action(player, character, action);
        self
    }

    pub fn when_player_use_storage(mut self, player: &str, character: &str, storage: &str) -> Self {
        let action = UseStorage {
            storage: self.storage(storage),
        };
        self.send_action(player, character, action);
        self
    }

    pub fn when_player_use_workbench(
        mut self,
        player: &str,
        character: &str,
        workbench: &str,
    ) -> Self {
        let action = UseWorkbench {
            workbench: self.workbench(workbench),
        };
        self.send_action(player, character, action);
        self
    }

    pub fn when_player_use_bed(mut self, player: &str, character: &str, bed: &str) -> Self {
        let action = UseBed { bed: self.bed(bed) };
        self.send_action(player, character, action);
        self
    }

    pub fn when_player_take_item(mut self, player: &str, character: &str, item: &str) -> Self {
        let action = TakeItem {
            item: self.item(item),
        };
        self.send_action(player, character, action);
        self
    }

    pub fn when_player_store_item(mut self, player: &str, character: &str, item: &str) -> Self {
        let action = StoreItem {
            item: self.item(item),
        };
        self.send_action(player, character, action);
        self
    }

    fn send_action(&mut self, player: &str, character: &str, action: Action) {
        let message = Message::Action {
            character: self.character(character),
            action,
        };
        self.master
            .handle_messages(vec![TrustedMessage(self.player(player), message)]);
    }

    pub fn when_player_build(mut self, player: &str, character: &str, construction: &str) -> Self {
        let action = Build {
            construction: self.construction(construction),
        };
        self.send_action(player, character, action);
        self
    }

    pub fn when_player_harvest_crop(mut self, player: &str, character: &str, crop: &str) -> Self {
        let action = HarvestCrop {
            crop: self.crop(crop),
        };
        self.send_action(player, character, action);
        self
    }

    pub fn when_player_plant_crop(
        mut self,
        player: &str,
        character: &str,
        seed: &str,
        position: (usize, usize),
    ) -> Self {
        let action = PlantCrop {
            seed: self.item(seed),
            position: position.into(),
            rotation: 0.0,
        };
        self.send_action(player, character, action);
        self
    }

    pub fn when_player_design_building(
        mut self,
        player: &str,
        character: &str,
        recipe: &str,
        position: (usize, usize),
    ) -> Self {
        let action = DesignBuilding {
            recipe: self.item(recipe),
            position: position.into(),
            rotation: 0.0,
        };
        self.send_action(player, character, action);
        self
    }

    pub fn when_player_move_character(
        mut self,
        player: &str,
        character: &str,
        destination: (i32, i32),
    ) -> Self {
        let action = MoveCharacter {
            position: destination.into(),
        };
        self.send_action(player, character, action);
        self
    }

    pub fn when_character_move_towards(
        mut self,
        _player: &str,
        character: &str,
        destination: (usize, usize),
    ) -> Self {
        self.master
            .physics
            .set_body_steering_behaviour(
                self.body(character),
                SteeringBehaviour::MoveTowards {
                    destination: destination.into(),
                },
            )
            .unwrap();
        self
    }

    pub fn when_player_move_spaceship(
        mut self,
        player: &str,
        character: &str,
        destination: (usize, usize),
    ) -> Self {
        let action = MoveSpaceship {
            position: destination.into(),
        };
        self.send_action(player, character, action);
        self
    }

    pub fn when_player_launch_spaceship(mut self, player: &str, character: &str) -> Self {
        let action = LaunchSpaceship;
        self.send_action(player, character, action);
        self
    }

    pub fn when_all_players_inspect_game(mut self) -> Self {
        let messages = self
            .master
            .players
            .iter()
            .map(|player| {
                TrustedMessage(
                    player.id.clone(),
                    Message::Interaction(Interaction::InspectGame),
                )
            })
            .collect();
        self.master.handle_messages(messages);
        self
    }

    pub fn _when_player_inspect_game(mut self, player: &str) -> Self {
        let player = self.player(player);
        self.master.handle_messages(vec![TrustedMessage(
            player,
            Message::Interaction(Interaction::InspectGame),
        )]);
        self
    }

    pub fn when_move_all_characters(mut self) -> Self {
        let mut mapping: HashMap<GroupId, PlayerId> = HashMap::new();
        for player in self.master.players.iter() {
            if let Some(group) = player.group {
                mapping.insert(group, player.id.clone());
            }
        }

        let bodies: Vec<BodyId> = self
            .master
            .universe
            .state
            .characters
            .iter()
            .map(|character| character.body)
            .collect();

        for body in bodies {
            self.master
                .physics
                .set_body_steering_behaviour(
                    body,
                    SteeringBehaviour::MoveTowards {
                        destination: (100, 120).into(),
                    },
                )
                .unwrap();
        }
        self
    }

    pub fn when_update_game_for_one_second(self) -> Self {
        self.when_update_game_for(1, real_seconds())
    }

    pub fn when_update_game_for(mut self, iterations: usize, time_delta: Duration) -> Self {
        self.master.resume_game().unwrap();
        for _ in 0..iterations {
            self.master.update_game(time_delta);
        }
        self
    }

    pub fn when_master_spawn_character(
        mut self,
        player: &str,
        key: &str,
        name: &str,
        space: &str,
        position: (usize, usize),
    ) -> Self {
        let events = self
            .master
            .spawn_player_character(
                CharacterKey(key.into()),
                self.group(player),
                self.space(space),
                position.into(),
            )
            .unwrap();
        self.master.publish(events);
        self.named_characters
            .insert(name.into(), CharacterId(self.master.id.last()));
        self.named_bodies
            .insert(name.into(), BodyId(self.master.id.last()));
        self.named_containers
            .insert(name.into(), self.master.id.last().into());
        self
    }

    pub fn then_spaceship_landing_should_be<F>(self, spaceship: &str, landing_builder: F) -> Self
    where
        F: Fn(&Self) -> Option<Landing>,
    {
        let spaceship = self.spaceship(spaceship);
        let spaceship = self.master.universe.get_spaceship(spaceship).unwrap();
        assert_that!(equals!(spaceship.landing, landing_builder(&self)));
        self
    }

    pub fn then_obstacle_should_be<F>(self, mesh: &str, id: ObstacleId, obstacle_builder: F) -> Self
    where
        F: Fn(&Self) -> Obstacle,
    {
        let mesh = self.mesh(mesh);
        let mesh = self.master.navigation.get_mesh(mesh).unwrap();
        let obstacle = self.master.navigation.get_obstacle(mesh, id).unwrap();
        assert_that!(equals!(obstacle, &obstacle_builder(&self)));
        self
    }

    pub fn then_plant_should_be<F>(self, id: PlantId, plant_builder: F) -> Self
    where
        F: Fn(&Self) -> Plant,
    {
        let plant = self.master.farming.get_plant(id).unwrap();
        assert_that!(equals!(plant, &plant_builder(&self)));
        self
    }

    pub fn then_barrier_should_be<F>(self, id: BarrierId, barrier_builder: F) -> Self
    where
        F: Fn(&Self) -> Barrier,
    {
        let barrier = self.master.physics.get_barrier(id).unwrap();
        assert_that!(equals!(barrier, &barrier_builder(&self)));
        self
    }

    pub fn then_body_position_should_be(
        self,
        body: &str,
        expected_position: (usize, usize),
    ) -> Self {
        let body = self.body(body);
        let body = self.master.physics.get_body(body).unwrap();
        let expected_position: Vector = expected_position.into();
        assert_that!(equals!(body.position, expected_position));
        self
    }

    pub fn then_body_steering_should_be(
        self,
        body: &str,
        expected_steering: Option<SteeringBehaviour>,
    ) -> Self {
        let body = self.body(body);
        let body = self.master.physics.get_body(body).unwrap();
        assert_that!(equals!(body.steering, expected_steering));
        self
    }

    pub fn then_body_velocity_should_be(
        self,
        body: &str,
        expected_velocity: (usize, usize),
    ) -> Self {
        let body = self.body(body);
        let body = self.master.physics.get_body(body).unwrap();
        let expected_velocity: Vector = expected_velocity.into();
        assert_that!(equals!(body.velocity, expected_velocity));
        self
    }

    pub fn then_navigator_location_should_be(
        self,
        navigator: &str,
        expected_location: (usize, usize),
    ) -> Self {
        let navigator = self.navigator(navigator);
        let navigator = self.master.navigation.get_navigator(navigator).unwrap();
        let (x, y) = expected_location;
        let expected_location = PathPoint::new(x as f32, y as f32);
        assert_that!(equals!(navigator.location, expected_location));
        self
    }

    pub fn then_navigator_path_should_be_some(self, navigator: &str) -> Self {
        let navigator = self.navigator(navigator);
        let navigator = self.master.navigation.get_navigator(navigator).unwrap();
        assert_that!(is_true!(navigator.path.is_some()));
        self
    }

    pub fn then_route_status_should_be(
        self,
        navigator: &str,
        destination: (usize, usize),
        expected_status: RouteStatus,
    ) -> Self {
        let navigator = self.navigator(navigator);
        let (x, y) = destination;
        let destination = PathPoint::new(x as f32, y as f32);
        let status = self
            .master
            .navigation
            .validate_route(navigator, destination)
            .unwrap();
        assert_that!(equals!(status, expected_status));
        self
    }

    pub fn last_id(&self) -> usize {
        self.master.id.last()
    }

    pub fn then_player_observers_should_be(
        self,
        player: &str,
        space: &str,
        expected_observers: usize,
    ) -> Self {
        let player = self.player(player);
        let space = self.space(space);
        let player = self.master.players.iter().find(|x| x.id == player).unwrap();
        let group = player.group.unwrap();
        let observers = get_observers(&self.master.observing, group, space);
        assert_that!(equals!(observers, expected_observers));
        self
    }

    pub fn then_player_events_should_be<F>(self, player: &str, event_builder: F) -> Self
    where
        F: Fn(&Self) -> Vec<Event>,
    {
        let player = self
            .master
            .players
            .iter()
            .find(|p| p.id == self.player(player))
            .unwrap();
        assert_that!(collection_equals!(player.events, event_builder(&self)));
        self
    }

    pub fn then_update_events_publish_time_should_between(
        self,
        min_time: f32,
        max_time: f32,
    ) -> Self {
        let reference_time = measure_reference_time(1000, 100);
        let execution_time = self.master.metrics.game_update_events_publish_time / reference_time;

        println!(
            "game_update_event_records_count: {}",
            self.master.metrics.game_update_event_records_count
        );
        println!(
            "game_update_events_count: {}",
            self.master.metrics.game_update_events_count
        );
        println!(
            "game_update_domain_updates: {} seconds",
            self.master.metrics.game_update_domain_updates_time
        );
        println!(
            "game_update_events_publish: {} seconds",
            self.master.metrics.game_update_events_publish_time
        );
        println!("reference_time: {} seconds", reference_time);

        assert_that!(between!(execution_time, min_time, max_time));
        self
    }

    pub fn then_message_events_publish_time_should_between(
        self,
        min_time: f32,
        max_time: f32,
    ) -> Self {
        let reference_time = measure_reference_time(1000, 100);
        let execution_time = self.master.metrics.messages_events_publish_time / reference_time;

        println!("messages_count: {}", self.master.metrics.messages_count);
        println!(
            "messages_event_records_count: {}",
            self.master.metrics.messages_event_records_count
        );
        println!(
            "messages_events_count: {}",
            self.master.metrics.messages_events_count
        );
        println!(
            "messages_handles_time: {} seconds",
            self.master.metrics.messages_handles_time
        );
        println!(
            "messages_events_publish_time: {} seconds",
            self.master.metrics.messages_events_publish_time
        );
        println!("reference_time: {} seconds", reference_time);

        assert_that!(between!(execution_time, min_time, max_time));
        self
    }
}

pub fn get_observers(observing: &Observing, group: GroupId, space: SpaceId) -> usize {
    match observing.characters.mapping_inverted.get(&group) {
        None => 0,
        Some(spaces) => match spaces.get(&space).cloned() {
            None => 0,
            Some(counter) => counter,
        },
    }
}
