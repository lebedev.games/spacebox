mod testing;

pub use testing::*;

#[test]
fn test_events_publish_performance_of_1000_characters_move_simultaneously() {
    GameTestScenario::new()
        // Arrange
        .given_characters_distribution(1000, 100, 15)
        // Act
        .when_move_all_characters()
        .when_update_game_for_one_second()
        // Assert
        .then_update_events_publish_time_should_between(5.0, 15.0);
}

#[test]
fn test_events_publish_performance_of_100_players_inspect_game() {
    GameTestScenario::new()
        // Arrange
        .given_characters_distribution(1000, 100, 15)
        // Act
        .when_all_players_inspect_game()
        // Assert
        .then_message_events_publish_time_should_between(0.0, 7.0);
}
