use physics::Physics::{BarrierSpaceUpdated, BodySpaceUpdated, SpaceDisconnected};
use spacebox::game::Appearance::{PlanetAppeared, SystemAppeared};
use spacebox::game::Disappearance::ColonyVanished;
use spacebox::game::Event::{Appearance, Disappearance, Physics};
mod testing;

pub use testing::*;

#[test]
fn test_just_spaceship_launch() {
    GameTestScenario::new()
        // Arrange
        .given_system_type("system")
        .given_system("system", "Solar")
        .given_planet_type("planet")
        .given_planet("planet", "Mars", "Solar", (1000, 1100))
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_colonization("Mars", "at Mars")
        .given_spaceship_type("shuttle")
        .given_spaceship_deck("shuttle", "Donkey", "Solar", (1000, 1000))
        .given_player("Alice")
        .given_character_type("human")
        .given_character("Alice", "human", "pilot", "on Donkey", (50, 51))
        .given_spaceship_landing("pilot", "bridge of Donkey", "at Mars", (10, 10))
        .given_character_command("pilot", "bridge of Donkey")
        // Act
        .when_player_launch_spaceship("Alice", "pilot")
        // Assert
        .then_body_position_should_be("Donkey", (1000, 1100).into())
        .then_body_velocity_should_be("Donkey", (0, 0).into())
        .then_body_steering_should_be("Donkey", None)
        .then_spaceship_landing_should_be("Donkey", |_| None)
        .then_player_events_should_be("Alice", |it| {
            vec![
                Appearance(SystemAppeared {
                    id: it.system("Solar"),
                    key: "system".into(),
                    space: it.space("Solar"),
                }),
                Appearance(PlanetAppeared {
                    id: it.planet("Mars"),
                    key: "planet".into(),
                    body: it.body("Mars"),
                    space: it.space("Solar"),
                    position: (1000, 1100).into(),
                    colonies: vec![it.colony("at Mars")],
                }),
                Physics(BodySpaceUpdated {
                    body: it.body("Donkey"),
                    space: it.space("Solar"),
                    position: (1000, 1100).into(),
                    velocity: (0, 0).into(),
                    offset: (0, 0).into(),
                }),
                Physics(SpaceDisconnected {
                    space: it.space("on Donkey"),
                }),
                Physics(BodySpaceUpdated {
                    body: it.body("pilot"),
                    space: it.space("on Donkey"),
                    position: (50, 51).into(),
                    velocity: (0, 0).into(),
                    offset: (-10, -10).into(),
                }),
                Physics(BarrierSpaceUpdated {
                    barrier: it.barrier("bridge of Donkey"),
                    space: it.space("on Donkey"),
                    position: (50, 50).into(),
                    offset: (-10, -10).into(),
                }),
                Disappearance(ColonyVanished {
                    id: it.colony("at Mars"),
                }),
            ]
        });
}

#[test]
fn test_spaceship_launch_after_building() {}

#[test]
fn test_spaceship_launch_after_bridge_building() {}
