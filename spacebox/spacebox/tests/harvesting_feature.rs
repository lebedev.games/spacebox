use farming::Farming::{LandChanged, PlantDestroyed, PlantHarvestingProgressed};
use farming::Land::Fertile;
use inventory::Inventory::ItemCreated;
use inventory::ItemFunction;
use spacebox::game::Disappearance::CropVanished;
use spacebox::game::Event::{Disappearance, Farming, Inventory, Universe};
use universe::Activity::CropHarvesting;
use universe::Time;
use universe::Universe::{
    ActivityBegun, ActivityEnded, ActivityStarted, ActivityStopped, TimeUpdated,
};

mod testing;

pub use testing::*;

#[test]
pub fn test_regular_harvesting() {
    GameTestScenario::new()
        // Arrange
        .given_player("Alice")
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_character_type("human")
        .given_character("Alice", "human", "farmer", "at Mars", (100, 100))
        .given_crop_type_params("garlic", 0.1, 0.2)
        .given_crop_ready_harvest("garlic", "own garlic", "at Mars", (101, 100))
        // Act
        .when_player_harvest_crop("Alice", "farmer", "own garlic")
        .when_update_game_for(3, game_hours())
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![
                Universe(ActivityBegun {
                    character: it.character("farmer"),
                    activity: CropHarvesting {
                        crop: it.crop("own garlic"),
                    },
                    time: Time(0),
                }),
                Universe(ActivityStarted {
                    character: it.character("farmer"),
                    activity: CropHarvesting {
                        crop: it.crop("own garlic"),
                    },
                }),
                Universe(TimeUpdated { ms: 10000000 }),
                Farming(PlantHarvestingProgressed {
                    id: it.plant("own garlic"),
                    farmland: it.farmland("at Mars"),
                    time: 0.1,
                    watered: 1.0,
                }),
                Universe(TimeUpdated { ms: 20000000 }),
                Farming(PlantDestroyed {
                    id: it.plant("own garlic"),
                    farmland: it.farmland("at Mars"),
                }),
                Farming(LandChanged {
                    place: (101, 100).into(),
                    land: Fertile,
                    farmland: it.farmland("at Mars"),
                }),
                Inventory(ItemCreated {
                    id: it.last_id().into(),
                    key: "garlic".into(),
                    container: it.container("farmer"),
                    weight: 0.0,
                    functions: vec![ItemFunction::Consumable { effects: vec![] }],
                }),
                Disappearance(CropVanished {
                    id: it.crop("own garlic"),
                }),
                Universe(ActivityStopped {
                    character: it.character("farmer"),
                    activity: CropHarvesting {
                        crop: it.crop("own garlic"),
                    },
                    time: Time(0),
                }),
                Universe(ActivityEnded {
                    character: it.character("farmer"),
                    activity: CropHarvesting {
                        crop: it.crop("own garlic"),
                    },
                }),
                Universe(TimeUpdated { ms: 30000000 }),
            ]
        });
}
