use physics::Physics::{BarrierSpaceUpdated, BodySpaceUpdated, SpaceConnected};
use spacebox::game::Action::LandSpaceship;
use spacebox::game::Appearance::ColonyAppeared;
use spacebox::game::Disappearance::SystemVanished;
use spacebox::game::Event::{Appearance, Disappearance, Game, Physics};
use spacebox::game::Game::ActionFailed;
use universe::UniverseError::CharacterInvalidTask;
use universe::{Landing, Task};
mod testing;

pub use testing::*;

#[test]
fn test_just_spaceship_landing() {
    GameTestScenario::new()
        // Arrange
        .given_system_type("system")
        .given_system("system", "Solar")
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_spaceship_type("shuttle")
        .given_spaceship_deck("shuttle", "Donkey", "Solar", (1000, 1000))
        .given_player("Alice")
        .given_character_type("human")
        .given_character("Alice", "human", "pilot", "on Donkey", (50, 51))
        .given_character_command("pilot", "bridge of Donkey")
        // Act
        .when_player_land_spaceship("Alice", "pilot", "at Mars", (27, 27))
        // Assert
        .then_body_position_should_be("Donkey", (27, 27).into())
        .then_body_velocity_should_be("Donkey", (0, 0).into())
        .then_body_steering_should_be("Donkey", None)
        .then_spaceship_landing_should_be("Donkey", |_| {
            Some(Landing {
                bounds_min: (27, 27).into(),
                bounds_max: (127, 127).into(),
                point: (27, 27).into(),
            })
        })
        .then_player_events_should_be("Alice", |it| {
            vec![
                Appearance(ColonyAppeared {
                    id: it.colony("at Mars"),
                    key: "colony".into(),
                    space: it.space("at Mars"),
                    mesh: it.mesh("at Mars"),
                    farmland: it.farmland("at Mars"),
                }),
                Physics(BodySpaceUpdated {
                    body: it.body("Donkey"),
                    space: it.space("at Mars"),
                    position: (27, 27).into(),
                    velocity: (0, 0).into(),
                    offset: (0, 0).into(),
                }),
                Physics(SpaceConnected {
                    space: it.space("on Donkey"),
                    parent: it.space("at Mars"),
                    connection_point: (27, 27).into(),
                }),
                Physics(BodySpaceUpdated {
                    body: it.body("pilot"),
                    space: it.space("at Mars"),
                    position: (77, 78).into(),
                    velocity: (0, 0).into(),
                    offset: (27, 27).into(),
                }),
                Physics(BarrierSpaceUpdated {
                    barrier: it.barrier("bridge of Donkey"),
                    space: it.space("at Mars"),
                    position: (77, 77).into(),
                    offset: (27, 27).into(),
                }),
                Disappearance(SystemVanished {
                    id: it.system("Solar"),
                }),
            ]
        });
}

#[test]
fn test_land_spaceship_out_of_bridge() {
    GameTestScenario::new()
        // Arrange
        .given_system_type("system")
        .given_system("system", "Solar")
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_spaceship_type("shuttle")
        .given_spaceship_deck("shuttle", "Donkey", "Solar", (1000, 1000))
        .given_player("Alice")
        .given_character_type("human")
        .given_character("Alice", "human", "pilot", "on Donkey", (50, 51))
        // Act
        .when_player_land_spaceship("Alice", "pilot", "at Mars", (27, 27))
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![Game(ActionFailed {
                character: it.character("pilot"),
                action: LandSpaceship {
                    colony: it.colony("at Mars"),
                    position: (27, 27).into(),
                },
                error: CharacterInvalidTask { task: Task::Idle }.into(),
            })]
        });
}
