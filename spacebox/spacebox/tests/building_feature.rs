use farming::Farming::LandChanged;
use farming::Land::Infertile;
use inventory::InventoryError::ItemBuildingRecipeNotSpecified;
use navigation::NavigationError::ObstacleCreationOverExistingObstacle;
use navigation::{Obstacle, ObstacleId, PathPoint};
use physics::PhysicsError::DistantObjectsIsTooFar;
use physics::{Barrier, BarrierId, BarrierOrigin};
use spacebox::game::Action::{Build, DesignBuilding, UseWorkbench};
use spacebox::game::Appearance::{ConstructionAppeared, PropAppeared};
use spacebox::game::Event::{Appearance, Farming, Game, Universe};
use spacebox::game::Game::ActionFailed;
use universe::Activity::{Building, BuildingDesign};
use universe::ConstructionProject::PropProject;
use universe::Universe::{
    ActivityBegun, ActivityEnded, ActivityStarted, ActivityStopped, ConstructionComplete,
    ConstructionProgressed, TimeUpdated,
};
use universe::UniverseError::CharacterInvalidTask;
use universe::{ConstructionId, ConstructionKey, ConstructionProject, Task, Time};

mod testing;

pub use testing::*;

#[test]
fn test_regular_building_process() {
    GameTestScenario::new()
        // Arrange
        .given_player("Alice")
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_prop_type("prop", (1, 1))
        .given_construction_type("design", 0.2, PropProject { key: "prop".into() })
        .given_construction("design", "own prop", "at Mars", (101, 100))
        .given_character_type("human")
        .given_character("Alice", "human", "builder", "at Mars", (100, 100))
        // Act
        .when_player_build("Alice", "builder", "own prop")
        .when_update_game_for(3, game_hours())
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![
                Universe(ActivityBegun {
                    character: it.character("builder"),
                    activity: Building {
                        construction: it.construction("own prop"),
                    },
                    time: Time(0),
                }),
                Universe(ActivityStarted {
                    character: it.character("builder"),
                    activity: Building {
                        construction: it.construction("own prop"),
                    },
                }),
                Universe(TimeUpdated { ms: 10000000 }),
                Universe(ConstructionProgressed {
                    id: it.construction("own prop"),
                    progress: 0.1,
                    amount: 0.1,
                    space: it.space("at Mars"),
                }),
                Universe(TimeUpdated { ms: 20000000 }),
                Universe(ConstructionComplete {
                    id: it.construction("own prop"),
                    space: it.space("at Mars"),
                }),
                Appearance(PropAppeared {
                    id: it.last_id().into(),
                    key: "prop".into(),
                    barrier: it.last_id().into(),
                    obstacle: it.last_id().into(),
                    space: it.space("at Mars"),
                    position: (101, 100).into(),
                    rotation: 0.0,
                }),
                Farming(LandChanged {
                    place: (101, 100).into(),
                    land: Infertile,
                    farmland: it.farmland("at Mars"),
                }),
                Universe(ActivityStopped {
                    character: it.character("builder"),
                    activity: Building {
                        construction: it.construction("own prop"),
                    },
                    time: Time(0),
                }),
                Universe(ActivityEnded {
                    character: it.character("builder"),
                    activity: Building {
                        construction: it.construction("own prop"),
                    },
                }),
                Universe(TimeUpdated { ms: 30000000 }),
            ]
        });
}

#[test]
fn test_building_on_distance() {
    GameTestScenario::new()
        // Arrange
        .given_player("Alice")
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_prop_type("prop", (2, 2))
        .given_construction_type("design", 0.2, PropProject { key: "prop".into() })
        .given_construction("design", "own prop", "at Mars", (200, 100))
        .given_character_type("human")
        .given_character("Alice", "human", "builder", "at Mars", (100, 100))
        // Act
        .when_player_build("Alice", "builder", "own prop")
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![Game(ActionFailed {
                character: it.character("builder"),
                action: Build {
                    construction: it.construction("own prop"),
                },
                error: DistantObjectsIsTooFar.into(),
            })]
        });
}

#[test]
fn test_regular_design_building() {
    GameTestScenario::new()
        // Arrange
        .given_player("Alice")
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_workbench_type("workbench")
        .given_workbench("workbench", "the Bureau", "at Mars", (101, 100))
        .given_prop_type("prop", (2, 2))
        .given_prop_construction_type("prop")
        .given_character_type("human")
        .given_character("Alice", "human", "builder", "at Mars", (100, 100))
        .given_item_building_recipe_type("recipe", "prop")
        .given_item("recipe", "recipe", "builder", 1)
        // Act
        .when_player_use_workbench("Alice", "builder", "the Bureau")
        .when_update_game_for_one_second()
        .when_player_design_building("Alice", "builder", "recipe", (100, 105))
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![
                Universe(ActivityBegun {
                    character: it.character("builder"),
                    activity: BuildingDesign {
                        workbench: it.workbench("the Bureau"),
                    },
                    time: Time(0),
                }),
                Universe(ActivityStarted {
                    character: it.character("builder"),
                    activity: BuildingDesign {
                        workbench: it.workbench("the Bureau"),
                    },
                }),
                Universe(TimeUpdated { ms: 100000 }),
                Appearance(ConstructionAppeared {
                    id: ConstructionId(6),
                    space: it.space("at Mars"),
                    key: ConstructionKey("prop".into()),
                    progress: 0.0,
                    project: ConstructionProject::PropProject { key: "prop".into() },
                    barrier: BarrierId(6),
                    position: (100, 105).into(),
                    rotation: 0.0,
                }),
            ]
        })
        .then_obstacle_should_be("at Mars", ObstacleId(6), |it| Obstacle {
            id: ObstacleId(6),
            key: "prop".into(),
            location: PathPoint::new(100.0, 105.0),
            rotation: false,
            size: (2, 2).into(),
            mesh: it.mesh("at Mars"),
        })
        .then_barrier_should_be(BarrierId(6), |it| Barrier {
            id: BarrierId(6),
            key: "prop".into(),
            space: it.space("at Mars"),
            position: (100, 105).into(),
            rotation: 0.0,
            size: (2, 2).into(),
            origin: BarrierOrigin::Construction,
        });
}

#[test]
fn test_use_workbench_on_distance() {
    GameTestScenario::new()
        // Arrange
        .given_player("Alice")
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_workbench_type("workbench")
        .given_workbench("workbench", "the Bureau", "at Mars", (200, 100))
        .given_character_type("human")
        .given_character("Alice", "human", "builder", "at Mars", (100, 100))
        // Act
        .when_player_use_workbench("Alice", "builder", "the Bureau")
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![Game(ActionFailed {
                character: it.character("builder"),
                action: UseWorkbench {
                    workbench: it.workbench("the Bureau"),
                },
                error: DistantObjectsIsTooFar.into(),
            })]
        });
}

#[test]
fn test_design_building_out_of_workbench() {
    GameTestScenario::new()
        // Arrange
        .given_player("Alice")
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_workbench_type("workbench")
        .given_workbench("workbench", "the Bureau", "at Mars", (101, 100))
        .given_prop_type("prop", (2, 2))
        .given_prop_construction_type("prop")
        .given_character_type("human")
        .given_character("Alice", "human", "builder", "at Mars", (100, 100))
        .given_item_building_recipe_type("recipe", "prop")
        .given_item("recipe", "recipe", "builder", 1)
        // Act
        .when_player_design_building("Alice", "builder", "recipe", (100, 105))
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![Game(ActionFailed {
                character: it.character("builder"),
                action: DesignBuilding {
                    recipe: it.item("recipe"),
                    position: (100, 105).into(),
                    rotation: 0.0,
                },
                error: CharacterInvalidTask { task: Task::Idle }.into(),
            })]
        });
}

#[test]
fn test_design_building_with_non_recipe_item() {
    GameTestScenario::new()
        // Arrange
        .given_player("Alice")
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_workbench_type("workbench")
        .given_workbench("workbench", "the Bureau", "at Mars", (101, 100))
        .given_prop_type("prop", (2, 2))
        .given_prop_construction_type("prop")
        .given_character_type("human")
        .given_character("Alice", "human", "builder", "at Mars", (100, 100))
        .given_item_type("non_recipe", vec![])
        .given_item("non_recipe", "recipe", "builder", 1)
        .given_character_building_design("builder", "the Bureau")
        // Act
        .when_player_design_building("Alice", "builder", "recipe", (100, 105))
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![Game(ActionFailed {
                character: it.character("builder"),
                action: DesignBuilding {
                    recipe: it.item("recipe"),
                    position: (100, 105).into(),
                    rotation: 0.0,
                },
                error: ItemBuildingRecipeNotSpecified {
                    id: it.item("recipe"),
                }
                .into(),
            })]
        });
}

#[test]
fn test_design_building_over_existing_prop() {
    GameTestScenario::new()
        // Arrange
        .given_player("Alice")
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_workbench_type("workbench")
        .given_workbench("workbench", "the Bureau", "at Mars", (101, 100))
        .given_prop_type("prop", (2, 2))
        .given_prop_construction_type("prop")
        .given_character_type("human")
        .given_character("Alice", "human", "builder", "at Mars", (100, 100))
        .given_item_building_recipe_type("recipe", "prop")
        .given_item("recipe", "recipe", "builder", 1)
        .given_prop("prop", "cabinet", "at Mars", (100, 105))
        // Act
        .when_player_use_workbench("Alice", "builder", "the Bureau")
        .when_update_game_for_one_second()
        .when_player_design_building("Alice", "builder", "recipe", (100, 105))
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![
                Universe(ActivityBegun {
                    character: it.character("builder"),
                    activity: BuildingDesign {
                        workbench: it.workbench("the Bureau"),
                    },
                    time: Time(0),
                }),
                Universe(ActivityStarted {
                    character: it.character("builder"),
                    activity: BuildingDesign {
                        workbench: it.workbench("the Bureau"),
                    },
                }),
                Universe(TimeUpdated { ms: 100000 }),
                Game(ActionFailed {
                    character: it.character("builder"),
                    action: DesignBuilding {
                        recipe: it.item("recipe"),
                        position: (100, 105).into(),
                        rotation: 0.0,
                    },
                    error: ObstacleCreationOverExistingObstacle.into(),
                }),
            ]
        });
}
