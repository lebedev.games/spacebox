use spacebox::game::Action::UseBed;
use spacebox::game::Event::{Game, Universe};
use spacebox::game::Game::ActionFailed;
use universe::Activity::Sleeping;
use universe::Time;
use universe::Universe::{
    ActivityBegun, ActivityEnded, ActivityStarted, ActivityStopped, BedFreed, BedUsed,
    CharacterEnergyRestored, TimeUpdated,
};
use universe::UniverseError::BedAlreadyInUse;
mod testing;

pub use testing::*;

#[test]
fn test_sleep_on_used_bed() {
    GameTestScenario::new()
        // Arrange
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_player("Alice")
        .given_character_type("human")
        .given_character("Alice", "human", "farmer", "at Mars", (100, 100))
        .given_character("Alice", "human", "ranger", "at Mars", (102, 100))
        .given_character_energy("farmer", 50.0)
        .given_bed_type("bed", (1, 2))
        .given_bed("bed", "bed", "at Mars", (101, 100))
        // Act
        .when_player_use_bed("Alice", "farmer", "bed")
        .when_update_game_for(1, real_seconds())
        .when_player_use_bed("Alice", "ranger", "bed")
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![
                Universe(BedUsed {
                    id: it.bed("bed"),
                    user: it.character("farmer"),
                }),
                Universe(ActivityBegun {
                    character: it.character("farmer"),
                    activity: Sleeping { bed: it.bed("bed") },
                    time: Time(100000),
                }),
                Universe(TimeUpdated { ms: 100000 }),
                Game(ActionFailed {
                    character: it.character("ranger"),
                    action: UseBed { bed: it.bed("bed") },
                    error: BedAlreadyInUse {
                        user: it.character("farmer"),
                    }
                    .into(),
                }),
            ]
        });
}

#[test]
fn test_regular_sleeping() {
    GameTestScenario::new()
        // Arrange
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_player("Alice")
        .given_character_type("human")
        .given_character("Alice", "human", "farmer", "at Mars", (100, 100))
        .given_character_energy("farmer", 50.0)
        .given_bed_type("bed", (1, 2))
        .given_bed("bed", "bed", "at Mars", (101, 100))
        // Act
        .when_player_use_bed("Alice", "farmer", "bed")
        .when_update_game_for(3, real_seconds())
        .when_player_cancel_activity("Alice", "farmer")
        .when_update_game_for(4, real_seconds())
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![
                Universe(BedUsed {
                    id: it.bed("bed"),
                    user: it.character("farmer"),
                }),
                Universe(ActivityBegun {
                    character: it.character("farmer"),
                    activity: Sleeping { bed: it.bed("bed") },
                    time: Time(100000),
                }),
                Universe(TimeUpdated { ms: 100000 }),
                Universe(ActivityStarted {
                    character: it.character("farmer"),
                    activity: Sleeping { bed: it.bed("bed") },
                }),
                Universe(TimeUpdated { ms: 200000 }),
                Universe(CharacterEnergyRestored {
                    id: it.character("farmer"),
                    amount: 1.0,
                    energy: 51.0,
                    space: it.space("at Mars"),
                }),
                Universe(TimeUpdated { ms: 300000 }),
                Universe(ActivityStopped {
                    character: it.character("farmer"),
                    activity: Sleeping { bed: it.bed("bed") },
                    time: Time(300000),
                }),
                Universe(TimeUpdated { ms: 400000 }),
                Universe(TimeUpdated { ms: 500000 }),
                Universe(TimeUpdated { ms: 600000 }),
                Universe(ActivityEnded {
                    character: it.character("farmer"),
                    activity: Sleeping { bed: it.bed("bed") },
                }),
                Universe(BedFreed { id: it.bed("bed") }),
                Universe(TimeUpdated { ms: 700000 }),
            ]
        });
}
