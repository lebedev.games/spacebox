use inventory::Inventory::{ItemCreated, ItemMoved, ItemRemoved};
use inventory::InventoryError::ItemNotInContainer;
use physics::PhysicsError::DistantObjectsIsTooFar;
use spacebox::game::Action::{StoreItem, TakeItem, UseStorage};
use spacebox::game::Event::{Game, Inventory, Universe};
use spacebox::game::Game::ActionFailed;
use universe::Activity::StorageUsage;
use universe::Task::Idle;
use universe::Time;
use universe::Universe::{
    ActivityBegun, ActivityEnded, ActivityStarted, ActivityStopped, TimeUpdated,
};
use universe::UniverseError::CharacterInvalidTask;

mod testing;

pub use testing::*;

#[test]
fn test_store_item_out_of_storage() {
    GameTestScenario::new()
        // Arrange
        .given_players(&["Alice", "Boris"])
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_storage_type("storage")
        .given_storage("storage", "in chest", "at Mars", (100, 101))
        .given_item_type("fruit", vec![])
        .given_character_type("human")
        .given_character("Alice", "human", "farmer", "at Mars", (100, 100))
        .given_character("Boris", "human", "solder", "at Mars", (100, 100))
        .given_item("fruit", "apple", "farmer", 1)
        .given_item("fruit", "berry", "farmer", 1)
        // Act
        .when_player_store_item("Alice", "farmer", "apple")
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![Game(ActionFailed {
                character: it.character("farmer"),
                action: StoreItem {
                    item: it.item("apple"),
                },
                error: CharacterInvalidTask { task: Idle }.into(),
            })]
        })
        .then_player_events_should_be("Boris", |_| vec![]);
}

#[test]
fn test_store_item_in_storage_with_other_users() {
    GameTestScenario::new()
        // Arrange
        .given_players(&["Alice", "Boris"])
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_storage_type("storage")
        .given_storage("storage", "in chest", "at Mars", (100, 101))
        .given_item_type("fruit", vec![])
        .given_character_type("human")
        .given_character("Alice", "human", "farmer", "at Mars", (100, 100))
        .given_character("Boris", "human", "solder", "at Mars", (100, 100))
        .given_item("fruit", "apple", "farmer", 1)
        .given_item("fruit", "berry", "farmer", 1)
        .given_character_storage_usage("farmer", "in chest")
        .given_character_storage_usage("solder", "in chest")
        // Act
        .when_player_store_item("Alice", "farmer", "apple")
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![Inventory(ItemMoved {
                id: it.item("apple"),
                source: it.container("farmer"),
                target: it.container("in chest"),
            })]
        })
        .then_player_events_should_be("Boris", |it| {
            vec![Inventory(ItemMoved {
                id: it.item("apple"),
                source: it.container("farmer"),
                target: it.container("in chest"),
            })]
        });
}

#[test]
fn test_regular_store_item_in_storage() {
    GameTestScenario::new()
        // Arrange
        .given_players(&["Alice", "Boris"])
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_storage_type("storage")
        .given_storage("storage", "in chest", "at Mars", (100, 101))
        .given_item_type("fruit", vec![])
        .given_character_type("human")
        .given_character("Alice", "human", "farmer", "at Mars", (100, 100))
        .given_character("Boris", "human", "solder", "at Mars", (100, 100))
        .given_item("fruit", "apple", "farmer", 1)
        .given_item("fruit", "berry", "farmer", 1)
        .given_character_storage_usage("farmer", "in chest")
        // Act
        .when_player_store_item("Alice", "farmer", "apple")
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![Inventory(ItemMoved {
                id: it.item("apple"),
                source: it.container("farmer"),
                target: it.container("in chest"),
            })]
        })
        .then_player_events_should_be("Boris", |_| vec![]);
}

#[test]
fn test_take_item_out_of_storage() {
    GameTestScenario::new()
        // Arrange
        .given_players(&["Alice", "Boris"])
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_storage_type("storage")
        .given_storage("storage", "in chest", "at Mars", (100, 101))
        .given_item_type("fruit", vec![])
        .given_item("fruit", "apple", "in chest", 1)
        .given_item("fruit", "berry", "in chest", 1)
        .given_character_type("human")
        .given_character("Alice", "human", "farmer", "at Mars", (100, 100))
        .given_character("Boris", "human", "solder", "at Mars", (100, 100))
        // Act
        .when_player_take_item("Alice", "farmer", "apple")
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![Game(ActionFailed {
                character: it.character("farmer"),
                action: TakeItem {
                    item: it.item("apple"),
                },
                error: CharacterInvalidTask { task: Idle }.into(),
            })]
        })
        .then_player_events_should_be("Boris", |_| vec![]);
}

#[test]
fn test_take_item_from_storage_with_other_users() {
    GameTestScenario::new()
        // Arrange
        .given_players(&["Alice", "Boris"])
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_storage_type("storage")
        .given_storage("storage", "in chest", "at Mars", (100, 101))
        .given_item_type("fruit", vec![])
        .given_item("fruit", "apple", "in chest", 1)
        .given_item("fruit", "berry", "in chest", 1)
        .given_character_type("human")
        .given_character("Alice", "human", "farmer", "at Mars", (100, 100))
        .given_character("Boris", "human", "solder", "at Mars", (100, 100))
        .given_character_storage_usage("farmer", "in chest")
        .given_character_storage_usage("solder", "in chest")
        // Act
        .when_player_take_item("Alice", "farmer", "apple")
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![Inventory(ItemMoved {
                id: it.item("apple"),
                source: it.container("in chest"),
                target: it.container("farmer"),
            })]
        })
        .then_player_events_should_be("Boris", |it| {
            vec![Inventory(ItemMoved {
                id: it.item("apple"),
                source: it.container("in chest"),
                target: it.container("farmer"),
            })]
        });
}

#[test]
fn test_take_item_from_other_storage() {
    GameTestScenario::new()
        // Arrange
        .given_players(&["Alice", "Boris"])
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_storage_type("storage")
        .given_storage("storage", "in chest", "at Mars", (100, 101))
        .given_storage("storage", "in crate", "at Mars", (100, 102))
        .given_item_type("fruit", vec![])
        .given_item("fruit", "apple", "in crate", 1)
        .given_item("fruit", "berry", "in crate", 1)
        .given_character_type("human")
        .given_character("Alice", "human", "farmer", "at Mars", (100, 100))
        .given_character("Boris", "human", "solder", "at Mars", (100, 100))
        .given_character_storage_usage("farmer", "in chest")
        // Act
        .when_player_take_item("Alice", "farmer", "apple")
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![Game(ActionFailed {
                character: it.character("farmer"),
                action: TakeItem {
                    item: it.item("apple"),
                },
                error: ItemNotInContainer {
                    id: it.item("apple"),
                    container: it.container("in chest"),
                }
                .into(),
            })]
        })
        .then_player_events_should_be("Boris", |_| vec![]);
}

#[test]
fn test_regular_take_item_from_storage() {
    GameTestScenario::new()
        // Arrange
        .given_players(&["Alice", "Boris"])
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_storage_type("storage")
        .given_storage("storage", "in chest", "at Mars", (100, 101))
        .given_item_type("fruit", vec![])
        .given_item("fruit", "apple", "in chest", 1)
        .given_item("fruit", "berry", "in chest", 1)
        .given_character_type("human")
        .given_character("Alice", "human", "farmer", "at Mars", (100, 100))
        .given_character("Boris", "human", "solder", "at Mars", (100, 100))
        .given_character_storage_usage("farmer", "in chest")
        // Act
        .when_player_take_item("Alice", "farmer", "apple")
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![Inventory(ItemMoved {
                id: it.item("apple"),
                source: it.container("in chest"),
                target: it.container("farmer"),
            })]
        })
        .then_player_events_should_be("Boris", |_| vec![]);
}

fn test_close_storage_with_teammate() {
    GameTestScenario::new()
        // Arrange
        .given_players(&["Alice", "Boris"])
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_storage_type("storage")
        .given_storage("storage", "in chest", "at Mars", (100, 101))
        .given_item_type("fruit", vec![])
        .given_item("fruit", "apple", "in chest", 1)
        .given_item("fruit", "berry", "in chest", 1)
        .given_character_type("human")
        .given_character("Alice", "human", "farmer", "at Mars", (100, 100))
        .given_character("Alice", "human", "dancer", "at Mars", (100, 100))
        .given_character("Boris", "human", "solder", "at Mars", (100, 100))
        .given_character_storage_usage("farmer", "in chest")
        .given_character_storage_usage("dancer", "in chest")
        // Act
        .when_player_cancel_activity("Alice", "farmer")
        .when_update_game_for_one_second()
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![
                Universe(ActivityStopped {
                    character: it.character("farmer"),
                    activity: StorageUsage {
                        storage: it.storage("in chest"),
                    },
                    time: Time(0),
                }),
                Universe(ActivityEnded {
                    character: it.character("farmer"),
                    activity: StorageUsage {
                        storage: it.storage("in chest"),
                    },
                }),
                Universe(TimeUpdated { ms: 100000 }),
            ]
        })
        .then_player_events_should_be("Boris", |it| {
            vec![
                Universe(ActivityStopped {
                    character: it.character("farmer"),
                    activity: StorageUsage {
                        storage: it.storage("in chest"),
                    },
                    time: Time(0),
                }),
                Universe(ActivityEnded {
                    character: it.character("farmer"),
                    activity: StorageUsage {
                        storage: it.storage("in chest"),
                    },
                }),
                Universe(TimeUpdated { ms: 100000 }),
            ]
        });
}

#[test]
fn test_regular_close_storage() {
    GameTestScenario::new()
        // Arrange
        .given_players(&["Alice", "Boris"])
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_storage_type("storage")
        .given_storage("storage", "in chest", "at Mars", (100, 101))
        .given_item_type("fruit", vec![])
        .given_item("fruit", "apple", "in chest", 1)
        .given_item("fruit", "berry", "in chest", 1)
        .given_character_type("human")
        .given_character("Alice", "human", "farmer", "at Mars", (100, 100))
        .given_character("Boris", "human", "solder", "at Mars", (100, 100))
        .given_character_storage_usage("farmer", "in chest")
        // Act
        .when_player_cancel_activity("Alice", "farmer")
        .when_update_game_for_one_second()
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![
                Universe(ActivityStopped {
                    character: it.character("farmer"),
                    activity: StorageUsage {
                        storage: it.storage("in chest"),
                    },
                    time: Time(0),
                }),
                Universe(ActivityEnded {
                    character: it.character("farmer"),
                    activity: StorageUsage {
                        storage: it.storage("in chest"),
                    },
                }),
                Inventory(ItemRemoved {
                    id: it.item("apple"),
                    container: it.container("in chest"),
                }),
                Inventory(ItemRemoved {
                    id: it.item("berry"),
                    container: it.container("in chest"),
                }),
                Universe(TimeUpdated { ms: 100000 }),
            ]
        })
        .then_player_events_should_be("Boris", |it| {
            vec![
                Universe(ActivityStopped {
                    character: it.character("farmer"),
                    activity: StorageUsage {
                        storage: it.storage("in chest"),
                    },
                    time: Time(0),
                }),
                Universe(ActivityEnded {
                    character: it.character("farmer"),
                    activity: StorageUsage {
                        storage: it.storage("in chest"),
                    },
                }),
                Universe(TimeUpdated { ms: 100000 }),
            ]
        });
}

fn test_open_storage_with_teammate() {
    GameTestScenario::new()
        // Arrange
        .given_players(&["Alice", "Boris"])
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_storage_type("storage")
        .given_storage("storage", "in chest", "at Mars", (100, 101))
        .given_item_type("fruit", vec![])
        .given_item("fruit", "apple", "in chest", 1)
        .given_item("fruit", "berry", "in chest", 1)
        .given_character_type("human")
        .given_character("Alice", "human", "farmer", "at Mars", (100, 100))
        .given_character("Alice", "human", "dancer", "at Mars", (100, 100))
        .given_character("Boris", "human", "solder", "at Mars", (100, 100))
        .given_character_storage_usage("dancer", "in chest")
        // Act
        .when_player_use_storage("Alice", "farmer", "in chest")
        .when_update_game_for_one_second()
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![
                Universe(ActivityBegun {
                    character: it.character("farmer"),
                    activity: StorageUsage {
                        storage: it.storage("in chest"),
                    },
                    time: Time(0),
                }),
                Universe(ActivityStarted {
                    character: it.character("farmer"),
                    activity: StorageUsage {
                        storage: it.storage("in chest"),
                    },
                }),
                Universe(TimeUpdated { ms: 100000 }),
            ]
        })
        .then_player_events_should_be("Boris", |it| {
            vec![
                Universe(ActivityBegun {
                    character: it.character("farmer"),
                    activity: StorageUsage {
                        storage: it.storage("in chest"),
                    },
                    time: Time(0),
                }),
                Universe(ActivityStarted {
                    character: it.character("farmer"),
                    activity: StorageUsage {
                        storage: it.storage("in chest"),
                    },
                }),
                Universe(TimeUpdated { ms: 100000 }),
            ]
        });
}

#[test]
fn test_regular_open_storage() {
    GameTestScenario::new()
        // Arrange
        .given_players(&["Alice", "Boris"])
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_storage_type("storage")
        .given_storage("storage", "in chest", "at Mars", (100, 101))
        .given_item_type("fruit", vec![])
        .given_item("fruit", "apple", "in chest", 1)
        .given_item("fruit", "berry", "in chest", 1)
        .given_character_type("human")
        .given_character("Alice", "human", "farmer", "at Mars", (100, 100))
        .given_character("Boris", "human", "solder", "at Mars", (100, 100))
        // Act
        .when_player_use_storage("Alice", "farmer", "in chest")
        .when_update_game_for_one_second()
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![
                Inventory(ItemCreated {
                    id: it.item("apple"),
                    key: "fruit".into(),
                    container: it.container("in chest"),
                    weight: 0.0,
                    functions: vec![],
                }),
                Inventory(ItemCreated {
                    id: it.item("berry"),
                    key: "fruit".into(),
                    container: it.container("in chest"),
                    weight: 0.0,
                    functions: vec![],
                }),
                Universe(ActivityBegun {
                    character: it.character("farmer"),
                    activity: StorageUsage {
                        storage: it.storage("in chest"),
                    },
                    time: Time(0),
                }),
                Universe(ActivityStarted {
                    character: it.character("farmer"),
                    activity: StorageUsage {
                        storage: it.storage("in chest"),
                    },
                }),
                Universe(TimeUpdated { ms: 100000 }),
            ]
        })
        .then_player_events_should_be("Boris", |it| {
            vec![
                Universe(ActivityBegun {
                    character: it.character("farmer"),
                    activity: StorageUsage {
                        storage: it.storage("in chest"),
                    },
                    time: Time(0),
                }),
                Universe(ActivityStarted {
                    character: it.character("farmer"),
                    activity: StorageUsage {
                        storage: it.storage("in chest"),
                    },
                }),
                Universe(TimeUpdated { ms: 100000 }),
            ]
        });
}

#[test]
fn test_use_storage_on_distance() {
    GameTestScenario::new()
        // Arrange
        .given_player("Alice")
        .given_colony_type("colony", (400, 400))
        .given_colony("colony", "at Mars")
        .given_storage_type("storage")
        .given_storage("storage", "chest", "at Mars", (200, 100))
        .given_character_type("human")
        .given_character("Alice", "human", "farmer", "at Mars", (100, 100))
        // Act
        .when_player_use_storage("Alice", "farmer", "chest")
        // Assert
        .then_player_events_should_be("Alice", |it| {
            vec![Game(ActionFailed {
                character: it.character("farmer"),
                action: UseStorage {
                    storage: it.storage("chest"),
                },
                error: DistantObjectsIsTooFar.into(),
            })]
        });
}
