use serde::{Deserialize, Serialize};

use crate::{ContainerId, ContainerKey, ItemId, ItemKey};

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(tag = "$type")]
pub enum InventoryError {
    ContainerTypeNotFound { key: ContainerKey },
    ContainerNotFound { id: ContainerId },
    ItemTypeNotFound { key: ItemKey },
    ItemNotFound { id: ItemId },
    ItemNotInContainer { id: ItemId, container: ContainerId },
    ItemBuildingRecipeNotSpecified { id: ItemId },
    ItemSeedNotSpecified { id: ItemId },
    ItemConsumableNotSpecified { id: ItemId },
}
