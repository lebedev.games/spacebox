use crate::{Container, ContainerId, ContainerKey, Inventory, InventoryDomain, InventoryError};

pub struct CreateContainerOperation {
    pub container: Container,
}

impl InventoryDomain {
    pub fn prepare_container(
        &self,
        key: &ContainerKey,
        id: ContainerId,
    ) -> Result<CreateContainerOperation, InventoryError> {
        let container = self.get_container_type(key)?;
        let container = Container {
            id,
            key: container.key.clone(),
            size: container.size,
            slots: vec![],
        };

        Ok(CreateContainerOperation { container })
    }

    pub fn create_container(&mut self, operation: CreateContainerOperation) -> Vec<Inventory> {
        self.state.containers.push(operation.container);
        vec![]
    }
}
