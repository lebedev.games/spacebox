use crate::{Inventory, InventoryDomain, InventoryError, ItemId};

pub struct RemoveItemOperation {
    pub item_index: usize,
}

impl InventoryDomain {
    pub fn prepare_remove_item_operation(
        &self,
        item: ItemId,
    ) -> Result<RemoveItemOperation, InventoryError> {
        let item_index = self.index_item(item)?;
        Ok(RemoveItemOperation { item_index })
    }

    pub fn remove_item(&mut self, operation: RemoveItemOperation) -> Vec<Inventory> {
        let item = &self.state.items[operation.item_index];
        let events = vec![Inventory::ItemRemoved {
            id: item.id,
            container: item.container,
        }];
        self.state.items.remove(operation.item_index);
        events
    }
}
