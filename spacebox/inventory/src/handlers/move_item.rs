use crate::{ContainerId, Inventory, InventoryDomain, InventoryError, ItemId};

pub struct MoveItemOperation {
    pub item_index: usize,
    pub source: ContainerId,
    pub target: ContainerId,
}

impl InventoryDomain {
    pub fn prepare_move_item_operation(
        &self,
        container_from: ContainerId,
        item: ItemId,
        container_to: ContainerId,
    ) -> Result<MoveItemOperation, InventoryError> {
        let item_index = self.index_item(item)?;
        let container = self.get_container(container_from)?;
        let item = &self.state.items[item_index];
        if item.container != container.id {
            return Err(InventoryError::ItemNotInContainer {
                id: item.id,
                container: container_from,
            });
        }
        self.get_container(container_to)?;

        Ok(MoveItemOperation {
            item_index,
            source: container_from,
            target: container_to,
        })
    }

    pub fn move_item(&mut self, operation: MoveItemOperation) -> Vec<Inventory> {
        let item = &mut self.state.items[operation.item_index];
        item.container = operation.target;

        vec![Inventory::ItemMoved {
            id: item.id,
            source: operation.source,
            target: operation.target,
        }]
    }
}
