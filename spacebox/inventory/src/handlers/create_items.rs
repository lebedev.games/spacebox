use crate::{ContainerId, Inventory, InventoryDomain, InventoryError, Item, ItemId, ItemKey};

pub struct CreateItemsOperation {
    pub items: Vec<Item>,
}

impl InventoryDomain {
    pub fn prepare_items(
        &self,
        key: &ItemKey,
        container: ContainerId,
        ids: Vec<ItemId>,
    ) -> Result<CreateItemsOperation, InventoryError> {
        let item = self.get_item_type(key)?;
        let container = self.get_container(container)?;

        let mut items = Vec::new();
        for id in ids {
            items.push(Item {
                id,
                key: item.key.clone(),
                container: container.id,
                weight: item.weight,
                functions: item.functions.clone(),
            })
        }

        Ok(CreateItemsOperation { items })
    }

    pub fn create_items(&mut self, operation: CreateItemsOperation) -> Vec<Inventory> {
        let mut events = vec![];
        for item in &operation.items {
            events.push(Inventory::ItemCreated {
                id: item.id,
                key: item.key.clone(),
                container: item.container,
                weight: item.weight,
                functions: item.functions.clone(),
            })
        }
        self.state.items.extend(operation.items);
        events
    }
}
