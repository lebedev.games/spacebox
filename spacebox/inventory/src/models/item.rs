use serde::{Deserialize, Serialize};

use crate::models::ContainerId;

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq, Hash, Eq)]
pub struct ItemId(pub usize);

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct ItemKey(pub String);

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ItemType {
    pub key: ItemKey,
    pub able_to_stack: bool,
    pub weight: f32,
    pub functions: Vec<ItemFunction>,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct GenericKey(pub String);

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq)]
#[serde(tag = "$type")]
pub enum Effect {
    RestoreEnergy { amount: f32 },
    ReduceEnergy { amount: f32 },
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(tag = "$type")]
pub enum ItemFunction {
    Seed { crop: GenericKey },
    Consumable { effects: Vec<Effect> },
    BuildingRecipe { construction: GenericKey },
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Item {
    pub id: ItemId,
    pub key: ItemKey,
    pub container: ContainerId,
    pub weight: f32,
    pub functions: Vec<ItemFunction>,
}

impl From<usize> for ItemId {
    fn from(value: usize) -> Self {
        Self(value)
    }
}

impl From<&str> for ItemKey {
    fn from(value: &str) -> Self {
        Self(value.into())
    }
}
