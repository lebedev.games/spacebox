use serde::{Deserialize, Serialize};

use crate::{ItemId, ItemKey};

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq, Hash, Eq)]
pub struct ContainerId(pub usize);

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct ContainerKey(pub String);

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ContainerType {
    pub key: ContainerKey,
    pub size: usize,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Slot {
    pub key: ItemKey,
    pub top: ItemId,
    pub stack: Vec<ItemId>,
    pub quantity: usize,
    pub able_to_stack: bool,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Container {
    pub id: ContainerId,
    pub key: ContainerKey,
    pub size: usize,
    pub slots: Vec<Slot>,
}

impl From<usize> for ContainerId {
    fn from(value: usize) -> Self {
        Self(value)
    }
}

impl From<&str> for ContainerKey {
    fn from(value: &str) -> Self {
        Self(value.into())
    }
}
