use std::rc::Rc;

use crate::Inventory::{ItemCreated, ItemRemoved};
use crate::{
    Container, ContainerId, ContainerKey, ContainerType, Effect, GenericKey, Inventory,
    InventoryDatabase, InventoryError, InventoryState, Item, ItemFunction, ItemId, ItemKey,
    ItemType,
};

#[derive(Default)]
pub struct InventoryDomain {
    pub database: Rc<InventoryDatabase>,
    pub state: InventoryState,
}

impl InventoryDomain {
    pub fn open_container(&self, container: ContainerId) -> Result<Vec<Inventory>, InventoryError> {
        let container = self.get_container(container)?;
        let mut events = vec![];
        for item in &self.state.items {
            if item.container == container.id {
                events.push(ItemCreated {
                    id: item.id,
                    key: item.key.clone(),
                    container: container.id,
                    weight: item.weight,
                    functions: item.functions.clone(),
                })
            }
        }
        Ok(events)
    }

    pub fn close_container(
        &self,
        container: ContainerId,
    ) -> Result<Vec<Inventory>, InventoryError> {
        let container = self.get_container(container)?;
        let mut events = vec![];
        for item in &self.state.items {
            if item.container == container.id {
                events.push(ItemRemoved {
                    id: item.id,
                    container: container.id,
                })
            }
        }
        Ok(events)
    }

    pub fn get_item_type(&self, key: &ItemKey) -> Result<&ItemType, InventoryError> {
        self.database
            .items
            .iter()
            .find(|item| &item.key == key)
            .ok_or(InventoryError::ItemTypeNotFound { key: key.clone() })
    }

    pub fn get_container_type(&self, key: &ContainerKey) -> Result<&ContainerType, InventoryError> {
        self.database
            .containers
            .iter()
            .find(|container| &container.key == key)
            .ok_or(InventoryError::ContainerTypeNotFound { key: key.clone() })
    }

    pub fn index_item(&self, id: ItemId) -> Result<usize, InventoryError> {
        self.state
            .items
            .iter()
            .position(|item| item.id == id)
            .ok_or(InventoryError::ItemNotFound { id })
    }

    pub fn get_container(&self, id: ContainerId) -> Result<&Container, InventoryError> {
        self.state
            .containers
            .iter()
            .find(|container| container.id == id)
            .ok_or(InventoryError::ContainerNotFound { id })
    }

    pub fn get_item(&self, id: ItemId) -> Result<&Item, InventoryError> {
        self.state
            .items
            .iter()
            .find(|item| item.id == id)
            .ok_or(InventoryError::ItemNotFound { id })
    }
}

impl Item {
    pub fn ensure_building_recipe(&self) -> Result<GenericKey, InventoryError> {
        for function in &self.functions {
            match function {
                ItemFunction::BuildingRecipe { construction } => return Ok(construction.clone()),
                _ => continue,
            }
        }

        Err(InventoryError::ItemBuildingRecipeNotSpecified { id: self.id })
    }

    pub fn ensure_seed(&self) -> Result<GenericKey, InventoryError> {
        for function in &self.functions {
            match function {
                ItemFunction::Seed { crop } => return Ok(crop.clone()),
                _ => continue,
            }
        }

        Err(InventoryError::ItemSeedNotSpecified { id: self.id })
    }

    pub fn ensure_consumable(&self) -> Result<Vec<Effect>, InventoryError> {
        for function in &self.functions {
            match function {
                ItemFunction::Consumable { effects } => return Ok(effects.clone()),
                _ => continue,
            }
        }

        Err(InventoryError::ItemConsumableNotSpecified { id: self.id })
    }
}
