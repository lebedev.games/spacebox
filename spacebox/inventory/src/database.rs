use serde::{Deserialize, Serialize};

use crate::{ContainerType, ItemType};

#[derive(Debug, Clone, Serialize, Deserialize, Default)]
pub struct InventoryDatabase {
    pub containers: Vec<ContainerType>,
    pub items: Vec<ItemType>,
}
