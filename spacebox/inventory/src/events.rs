use serde::{Deserialize, Serialize};

use crate::{ContainerId, ItemFunction, ItemId, ItemKey};

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(tag = "$type")]
pub enum Inventory {
    ItemCreated {
        id: ItemId,
        key: ItemKey,
        container: ContainerId,
        weight: f32,
        functions: Vec<ItemFunction>,
    },
    ItemRemoved {
        id: ItemId,
        container: ContainerId,
    },
    ItemMoved {
        id: ItemId,
        source: ContainerId,
        target: ContainerId,
    },
}
