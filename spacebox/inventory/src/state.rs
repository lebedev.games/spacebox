use serde::{Deserialize, Serialize};

use crate::{Container, Item};

#[derive(Debug, Clone, Serialize, Deserialize, Default)]
pub struct InventoryState {
    pub containers: Vec<Container>,
    pub items: Vec<Item>,
}
