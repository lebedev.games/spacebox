use physics::{Body, BodyId, BodyKey, SpaceId};
use serde::{Deserialize, Serialize};

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq, Eq, Hash)]
pub struct PlanetId(pub usize);

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct PlanetKey(pub String);

#[derive(Debug, Serialize, Deserialize)]
pub struct PlanetType {
    pub key: PlanetKey,
    pub body: BodyKey,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Planet {
    pub id: PlanetId,
    pub key: PlanetKey,
    pub body: BodyId,
}

impl Planet {
    pub fn space(&self, body: &Body) -> SpaceId {
        body.space
    }
}

impl From<usize> for PlanetId {
    fn from(value: usize) -> Self {
        Self(value)
    }
}

impl From<&str> for PlanetKey {
    fn from(value: &str) -> Self {
        Self(value.into())
    }
}
