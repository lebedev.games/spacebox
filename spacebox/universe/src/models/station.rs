use serde::{Deserialize, Serialize};

use physics::{BodyId, BodyKey};
use piloting::{SpacecraftId, SpacecraftKey};

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq)]
pub struct StationId(pub usize);

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct StationKey(pub String);

#[derive(Debug, Serialize, Deserialize)]
pub struct StationType {
    pub key: StationKey,
    pub spacecraft: SpacecraftKey,
    pub body: BodyKey,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Station {
    pub id: StationId,
    pub key: StationKey,
    pub spacecraft: SpacecraftId,
    pub body: BodyId,
}
