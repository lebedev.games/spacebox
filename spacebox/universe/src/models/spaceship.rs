use navigation::{NavigatorId, NavigatorKey};
use physics::{Body, BodyId, BodyKey, SpaceId};
use piloting::{SpacecraftId, SpacecraftKey};
use serde::{Deserialize, Serialize};

use crate::{Coordinates, UniverseError};

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq, Eq, Hash)]
pub struct SpaceshipId(pub usize);

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct SpaceshipKey(pub String);

#[derive(Debug, Serialize, Deserialize)]
pub struct SpaceshipType {
    pub key: SpaceshipKey,
    pub spacecraft: SpacecraftKey,
    pub body: BodyKey,
    pub navigator: NavigatorKey,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct Landing {
    pub bounds_min: Coordinates,
    pub bounds_max: Coordinates,
    pub point: Coordinates,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Spaceship {
    pub id: SpaceshipId,
    pub key: SpaceshipKey,
    pub spacecraft: SpacecraftId,
    pub body: BodyId,
    pub landing: Option<Landing>,
    pub navigator: NavigatorId,
}

impl Spaceship {
    pub fn space(&self, body: &Body) -> SpaceId {
        body.space
    }

    pub fn ensure_landing(&self) -> Result<&Landing, UniverseError> {
        self.landing
            .as_ref()
            .ok_or(UniverseError::SpaceshipNotLanded)
    }
}

impl From<usize> for SpaceshipId {
    fn from(value: usize) -> Self {
        Self(value)
    }
}

impl From<&str> for SpaceshipKey {
    fn from(value: &str) -> Self {
        Self(value.into())
    }
}
