use farming::PlantId;
use serde::{Deserialize, Serialize};

use crate::{BedId, BridgeId, CropId, StorageId, WorkbenchId};

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq, Hash, Eq)]
pub struct GroupId(pub usize);

/// A set of characters acting together with a common interest or purpose,
/// especially within a larger organization.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Group {
    pub id: GroupId,
    pub name: String,
    pub playable: bool,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq)]
#[serde(tag = "$type")]
pub enum Securable {
    Bed { id: BedId },
    Storage { id: StorageId },
    Workbench { id: WorkbenchId },
    Planter { id: PlantId },
    Crop { id: CropId },
    Bridge { id: BridgeId },
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq)]
pub struct Permission {
    pub securable: Securable,
    pub principal: GroupId,
}
