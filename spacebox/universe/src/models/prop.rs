use navigation::{ObstacleId, ObstacleKey};
use physics::{Barrier, BarrierId, BarrierKey, SpaceId};
use serde::{Deserialize, Serialize};

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq)]
pub struct PropId(pub usize);

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct PropKey(pub String);

#[derive(Debug, Serialize, Deserialize)]
pub struct PropType {
    pub key: PropKey,
    pub barrier: BarrierKey,
    pub obstacle: ObstacleKey,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Prop {
    pub id: PropId,
    pub key: PropKey,
    pub barrier: BarrierId,
    pub obstacle: ObstacleId,
}

impl Prop {
    pub fn space(&self, barrier: &Barrier) -> SpaceId {
        barrier.space
    }
}

impl From<usize> for PropId {
    fn from(value: usize) -> Self {
        Self(value)
    }
}

impl From<&str> for PropKey {
    fn from(value: &str) -> Self {
        Self(value.into())
    }
}
