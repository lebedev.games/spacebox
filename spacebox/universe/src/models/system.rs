use navigation::{MeshId, MeshKey};
use physics::{SpaceId, SpaceKey};
use serde::{Deserialize, Serialize};

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq)]
pub struct SystemId(pub usize);

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct SystemKey(pub String);

#[derive(Debug, Serialize, Deserialize)]
pub struct SystemType {
    pub key: SystemKey,
    pub space: SpaceKey,
    pub mesh: MeshKey,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct System {
    pub id: SystemId,
    pub key: SystemKey,
    pub space: SpaceId,
    pub mesh: MeshId,
}

impl From<usize> for SystemId {
    fn from(value: usize) -> Self {
        Self(value)
    }
}

impl From<&str> for SystemKey {
    fn from(value: &str) -> Self {
        Self(value.into())
    }
}
