use serde::{Deserialize, Serialize};

use farming::{PlantId, PlantKey};
use navigation::{ObstacleId, ObstacleKey};
use physics::{BarrierId, BarrierKey};

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq, Hash, Eq)]
pub struct TreeId(pub usize);

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct TreeKey(pub String);

#[derive(Debug, Serialize, Deserialize)]
pub struct TreeType {
    pub key: TreeKey,
    pub barrier: BarrierKey,
    pub obstacle: ObstacleKey,
    pub plant: PlantKey,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Tree {
    pub id: TreeId,
    pub key: TreeKey,
    pub barrier: BarrierId,
    pub obstacle: ObstacleId,
    pub plant: PlantId,
}
