use farming::{FarmlandId, FarmlandKey};
use navigation::{MeshId, MeshKey};
use physics::{SpaceId, SpaceKey};
use serde::{Deserialize, Serialize};

use crate::SpaceshipId;

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq)]
pub struct DeckId(pub usize);

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct DeckKey(pub String);

#[derive(Debug, Serialize, Deserialize)]
pub struct DeckType {
    pub key: DeckKey,
    pub space: SpaceKey,
    pub farmland: FarmlandKey,
    pub mesh: MeshKey,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Deck {
    pub id: DeckId,
    pub key: DeckKey,
    pub space: SpaceId,
    pub farmland: FarmlandId,
    pub mesh: MeshId,
    pub spaceship: SpaceshipId,
}

impl From<usize> for DeckId {
    fn from(value: usize) -> Self {
        Self(value)
    }
}

impl From<&str> for DeckKey {
    fn from(value: &str) -> Self {
        Self(value.into())
    }
}
