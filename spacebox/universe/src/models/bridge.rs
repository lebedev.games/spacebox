use navigation::{ObstacleId, ObstacleKey, PathPoint};
use physics::{Barrier, BarrierId, BarrierKey, SpaceId};
use serde::{Deserialize, Serialize};

use crate::{Coordinates, SpaceshipId};

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq)]
pub struct BridgeId(pub usize);

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct BridgeKey(pub String);

#[derive(Debug, Serialize, Deserialize)]
pub struct BridgeType {
    pub key: BridgeKey,
    pub barrier: BarrierKey,
    pub obstacle: ObstacleKey,
    pub relative_entry: Coordinates,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Bridge {
    pub id: BridgeId,
    pub key: BridgeKey,
    pub barrier: BarrierId,
    pub obstacle: ObstacleId,
    pub relative_entry: Coordinates,
    pub spaceship: Option<SpaceshipId>,
}

impl Bridge {
    pub fn space(&self, barrier: &Barrier) -> SpaceId {
        barrier.space
    }

    pub fn entry(&self, barrier: &Barrier) -> PathPoint {
        (self.relative_entry + barrier.position.into()).into()
    }
}

impl From<usize> for BridgeId {
    fn from(value: usize) -> Self {
        Self(value)
    }
}

impl From<&str> for BridgeKey {
    fn from(value: &str) -> Self {
        Self(value.into())
    }
}
