use std::ops;

use farming::Place;
use navigation::{PathPoint, Point};
use physics::Vector;
use serde::{Deserialize, Serialize};

#[derive(Default, Debug, Copy, Clone, Serialize, Deserialize, PartialEq)]
pub struct Coordinates {
    pub x: f32,
    pub y: f32,
}

impl ops::Add<Coordinates> for Coordinates {
    type Output = Coordinates;

    fn add(self, other: Coordinates) -> Coordinates {
        Coordinates {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl ops::AddAssign<Coordinates> for Coordinates {
    fn add_assign(&mut self, other: Coordinates) {
        self.x += other.x;
        self.y += other.y;
    }
}

impl ops::Neg for Coordinates {
    type Output = Coordinates;

    fn neg(self) -> Self::Output {
        Coordinates {
            x: -self.x,
            y: -self.y,
        }
    }
}

impl From<(usize, usize)> for Coordinates {
    fn from(value: (usize, usize)) -> Self {
        Coordinates {
            x: value.0 as f32,
            y: value.1 as f32,
        }
    }
}

impl From<(f32, f32)> for Coordinates {
    fn from(value: (f32, f32)) -> Self {
        Coordinates {
            x: value.0,
            y: value.1,
        }
    }
}

impl From<Vector> for Coordinates {
    fn from(value: Vector) -> Self {
        Coordinates {
            x: value.x,
            y: value.y,
        }
    }
}

impl From<Coordinates> for Vector {
    fn from(value: Coordinates) -> Self {
        Vector {
            x: value.x,
            y: value.y,
        }
    }
}

impl From<Point> for Coordinates {
    fn from(value: Point) -> Self {
        Coordinates {
            x: value.x as f32,
            y: value.y as f32,
        }
    }
}

impl From<Coordinates> for Point {
    fn from(value: Coordinates) -> Self {
        Point {
            x: value.x as usize,
            y: value.y as usize,
        }
    }
}

impl From<Coordinates> for PathPoint {
    fn from(value: Coordinates) -> Self {
        PathPoint {
            x: value.x,
            y: value.y,
        }
    }
}

impl From<Coordinates> for Place {
    fn from(value: Coordinates) -> Self {
        Place {
            x: value.x as usize,
            y: value.y as usize,
        }
    }
}

impl From<Place> for Coordinates {
    fn from(value: Place) -> Self {
        Coordinates {
            x: value.x as f32,
            y: value.y as f32,
        }
    }
}

impl From<PathPoint> for Coordinates {
    fn from(value: PathPoint) -> Self {
        Coordinates {
            x: value.x,
            y: value.y,
        }
    }
}

impl From<(i32, i32)> for Coordinates {
    fn from(values: (i32, i32)) -> Self {
        Self {
            x: values.0 as f32,
            y: values.1 as f32,
        }
    }
}
