use farming::{FarmlandId, FarmlandKey};
use navigation::{MeshId, MeshKey};
use physics::{SpaceId, SpaceKey};
use serde::{Deserialize, Serialize};

use crate::PlanetId;

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq)]
pub struct ColonyId(pub usize);

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct ColonyKey(pub String);

#[derive(Debug, Serialize, Deserialize)]
pub struct ColonyType {
    pub key: ColonyKey,
    pub space: SpaceKey,
    pub farmland: FarmlandKey,
    pub mesh: MeshKey,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Colony {
    pub id: ColonyId,
    pub key: ColonyKey,
    pub space: SpaceId,
    pub farmland: FarmlandId,
    pub mesh: MeshId,
}

impl From<usize> for ColonyId {
    fn from(value: usize) -> Self {
        Self(value)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Colonization {
    pub colony: ColonyId,
    pub planet: PlanetId,
}

impl From<&str> for ColonyKey {
    fn from(value: &str) -> Self {
        Self(value.into())
    }
}
