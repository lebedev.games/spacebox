use navigation::{ObstacleId, ObstacleKey, PathPoint};
use physics::{Barrier, BarrierId, BarrierKey, SpaceId};
use serde::{Deserialize, Serialize};

use crate::Coordinates;

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq)]
pub struct PlanterId(pub usize);

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct PlanterKey(pub String);

#[derive(Debug, Serialize, Deserialize)]
pub struct PlanterType {
    pub key: PlanterKey,
    pub barrier: BarrierKey,
    pub obstacle: ObstacleKey,
    pub relative_entry: Coordinates,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Planter {
    pub id: PlanterId,
    pub key: PlanterKey,
    pub barrier: BarrierId,
    pub obstacle: ObstacleId,
    pub relative_entry: Coordinates,
}

impl Planter {
    pub fn space(&self, barrier: &Barrier) -> SpaceId {
        barrier.space
    }

    pub fn entry(&self, barrier: &Barrier) -> PathPoint {
        (self.relative_entry + barrier.position.into()).into()
    }
}

impl From<usize> for PlanterId {
    fn from(value: usize) -> Self {
        Self(value)
    }
}

impl From<&str> for PlanterKey {
    fn from(value: &str) -> Self {
        Self(value.into())
    }
}
