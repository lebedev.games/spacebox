use std::cmp::Ordering;

use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct Days(pub f32);

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct Hours(pub f32);

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct Minutes(pub f32);

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct Seconds(pub f32);

#[derive(Debug, Clone, Copy, Serialize, Deserialize, Default, PartialEq)]
pub struct Time(pub u64);

impl Time {
    pub fn instant() -> Self {
        Time(0)
    }

    pub fn is_instant(&self) -> bool {
        self.0 == 0
    }

    pub fn from_millis(decimal_millis: u64) -> Self {
        Time(decimal_millis)
    }

    pub fn from_seconds(decimal_seconds: f32) -> Self {
        let decimal_millis = decimal_seconds * 1000.0;
        Time::from_millis(decimal_millis as u64)
    }

    pub fn from_minutes(decimal_minutes: f32) -> Self {
        Time::from_seconds(decimal_minutes * 100.0)
    }

    pub fn increment(&mut self, delta: &Time) {
        self.0 += delta.in_millis()
    }

    pub fn in_millis(&self) -> u64 {
        self.0
    }

    pub fn in_seconds(&self) -> f32 {
        (self.0 as f32) / 1000.0
    }

    pub fn in_minutes(&self) -> f32 {
        self.in_seconds() / 100.0
    }

    pub fn in_hours(&self) -> f32 {
        self.in_minutes() / 100.0
    }

    pub fn in_days(&self) -> f32 {
        self.in_hours() / 10.0
    }
}

impl PartialOrd for Time {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.0.cmp(&other.0))
    }
}

impl std::ops::Sub for Time {
    type Output = Time;

    fn sub(self, other: Self) -> Self::Output {
        Time(self.0 - other.0)
    }
}

impl std::ops::Add for Time {
    type Output = Time;

    fn add(self, other: Self) -> Self::Output {
        Time(self.0 + other.0)
    }
}

impl std::ops::Div for Time {
    type Output = f32;

    fn div(self, rhs: Self) -> Self::Output {
        (self.0 as f32) / (rhs.0 as f32)
    }
}
