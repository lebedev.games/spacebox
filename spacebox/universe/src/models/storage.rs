use inventory::{ContainerId, ContainerKey};
use navigation::{ObstacleId, ObstacleKey, PathPoint};
use physics::{Barrier, BarrierId, BarrierKey, SpaceId};
use serde::{Deserialize, Serialize};

use crate::Coordinates;

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq)]
pub struct StorageId(pub usize);

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct StorageKey(pub String);

#[derive(Debug, Serialize, Deserialize)]
pub struct StorageType {
    pub key: StorageKey,
    pub barrier: BarrierKey,
    pub obstacle: ObstacleKey,
    pub container: ContainerKey,
    pub relative_entry: Coordinates,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Storage {
    pub id: StorageId,
    pub key: StorageKey,
    pub barrier: BarrierId,
    pub obstacle: ObstacleId,
    pub container: ContainerId,
    pub relative_entry: Coordinates,
}

impl Storage {
    pub fn space(&self, barrier: &Barrier) -> SpaceId {
        barrier.space
    }

    pub fn entry(&self, barrier: &Barrier) -> PathPoint {
        (self.relative_entry + barrier.position.into()).into()
    }
}

impl From<usize> for StorageId {
    fn from(value: usize) -> Self {
        Self(value)
    }
}

impl From<&str> for StorageKey {
    fn from(value: &str) -> Self {
        Self(value.into())
    }
}
