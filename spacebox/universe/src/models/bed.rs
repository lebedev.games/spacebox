use navigation::{ObstacleId, ObstacleKey, PathPoint};
use physics::{Barrier, BarrierId, BarrierKey, SpaceId, Vector};
use serde::{Deserialize, Serialize};

use crate::{CharacterId, Coordinates};

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq)]
pub struct BedId(pub usize);

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct BedKey(pub String);

#[derive(Debug, Serialize, Deserialize)]
pub struct BedType {
    pub key: BedKey,
    pub barrier: BarrierKey,
    pub obstacle: ObstacleKey,
    pub relative_entry: Coordinates,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Bed {
    pub id: BedId,
    pub key: BedKey,
    pub barrier: BarrierId,
    pub obstacle: ObstacleId,
    pub relative_entry: Coordinates,
    pub user: Option<CharacterId>,
}

impl Bed {
    pub fn space(&self, barrier: &Barrier) -> SpaceId {
        barrier.space
    }

    pub fn entry(&self, barrier: &Barrier) -> PathPoint {
        (self.relative_entry + barrier.position.into()).into()
    }

    pub fn entry_position(&self, barrier: &Barrier) -> Vector {
        (self.relative_entry + barrier.position.into()).into()
    }
}

impl From<usize> for BedId {
    fn from(value: usize) -> Self {
        Self(value)
    }
}

impl From<&str> for BedKey {
    fn from(value: &str) -> Self {
        Self(value.into())
    }
}
