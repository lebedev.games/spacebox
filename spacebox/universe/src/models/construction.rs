use navigation::ObstacleId;
use physics::{Barrier, BarrierId, SpaceId};
use serde::{Deserialize, Serialize};

use crate::{BedKey, BridgeKey, PlanterKey, PropKey, StorageKey, WorkbenchKey};

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq)]
pub struct ConstructionId(pub usize);

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct ConstructionKey(pub String);

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(tag = "$type")]
pub enum ConstructionProject {
    PropProject { key: PropKey },
    StorageProject { key: StorageKey },
    WorkbenchProject { key: WorkbenchKey },
    PlanterProject { key: PlanterKey },
    BedProject { key: BedKey },
    BridgeProject { key: BridgeKey },
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ConstructionType {
    pub key: ConstructionKey,
    pub project: ConstructionProject,
    pub days_to_construct: f32,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Construction {
    pub id: ConstructionId,
    pub key: ConstructionKey,
    pub project: ConstructionProject,
    pub barrier: BarrierId,
    pub obstacle: ObstacleId,
    pub progress: f32,
}

impl Construction {
    pub fn space(&self, barrier: &Barrier) -> SpaceId {
        barrier.space
    }
}

impl From<usize> for ConstructionId {
    fn from(value: usize) -> Self {
        Self(value)
    }
}

impl From<&str> for ConstructionKey {
    fn from(value: &str) -> Self {
        Self(value.into())
    }
}
