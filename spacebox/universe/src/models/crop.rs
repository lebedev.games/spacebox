use farming::{PlantId, PlantKey};
use inventory::ItemKey;
use physics::{BarrierId, BarrierKey};
use serde::{Deserialize, Serialize};

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq, Hash, Eq)]
pub struct CropId(pub usize);

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct CropKey(pub String);

#[derive(Debug, Serialize, Deserialize)]
pub struct CropType {
    pub key: CropKey,
    pub plant: PlantKey,
    pub consumable: ItemKey,
    pub barrier: BarrierKey,
    pub production: f32,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Crop {
    pub id: CropId,
    pub key: CropKey,
    pub plant: PlantId,
    pub barrier: BarrierId,
}

impl From<usize> for CropId {
    fn from(value: usize) -> Self {
        Self(value)
    }
}

impl From<&str> for CropKey {
    fn from(value: &str) -> Self {
        Self(value.into())
    }
}
