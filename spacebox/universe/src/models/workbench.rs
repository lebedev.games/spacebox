use navigation::{ObstacleId, ObstacleKey, PathPoint};
use physics::{Barrier, BarrierId, BarrierKey, SpaceId};
use serde::{Deserialize, Serialize};

use crate::Coordinates;

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq)]
pub struct WorkbenchId(pub usize);

#[derive(Default, Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct WorkbenchKey(pub String);

#[derive(Default, Debug, Serialize, Deserialize)]
pub struct WorkbenchType {
    pub key: WorkbenchKey,
    pub barrier: BarrierKey,
    pub obstacle: ObstacleKey,
    pub relative_entry: Coordinates,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Workbench {
    pub id: WorkbenchId,
    pub key: WorkbenchKey,
    pub barrier: BarrierId,
    pub obstacle: ObstacleId,
    pub relative_entry: Coordinates,
}

impl Workbench {
    pub fn space(&self, barrier: &Barrier) -> SpaceId {
        barrier.space
    }

    pub fn entry(&self, barrier: &Barrier) -> PathPoint {
        (self.relative_entry + barrier.position.into()).into()
    }
}

impl From<usize> for WorkbenchId {
    fn from(value: usize) -> Self {
        Self(value)
    }
}

impl From<&str> for WorkbenchKey {
    fn from(value: &str) -> Self {
        Self(value.into())
    }
}
