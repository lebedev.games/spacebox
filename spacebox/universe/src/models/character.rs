use inventory::{ContainerId, ContainerKey};
use navigation::{NavigatorId, NavigatorKey};
use physics::{Body, BodyId, BodyKey, SensorId, SpaceId};
use serde::{Deserialize, Serialize};

use crate::{
    BedId, BridgeId, ConstructionId, CropId, GroupId, PlanterId, StorageId, Time, UniverseError,
    WorkbenchId,
};

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq, Eq, Hash)]
pub struct CharacterId(pub usize);

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct CharacterKey(pub String);

#[derive(Debug, Serialize, Deserialize)]
pub struct CharacterType {
    pub key: CharacterKey,
    pub body: BodyKey,
    pub health_limit: f32,
    pub energy_limit: f32,
    pub stress_limit: f32,
    pub container: ContainerKey,
    pub navigator: NavigatorKey,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq)]
#[serde(tag = "$type")]
pub enum Activity {
    Movement,
    Sleeping { bed: BedId },
    CropHarvesting { crop: CropId },
    BuildingDesign { workbench: WorkbenchId },
    Building { construction: ConstructionId },
    StorageUsage { storage: StorageId },
    Planting { planter: PlanterId },
    SpaceshipCommand { bridge: BridgeId },
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq)]
#[serde(tag = "$type")]
pub enum Task {
    Idle,
    PendingBeginning { activity: Activity },
    Beginning { time: Time, activity: Activity },
    Running { activity: Activity },
    PendingEnding { activity: Activity },
    Ending { time: Time, activity: Activity },
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Character {
    pub id: CharacterId,
    pub key: CharacterKey,
    pub group: GroupId,
    pub body: BodyId,
    pub task: Task,
    pub sensor: SensorId,
    pub container: ContainerId,
    pub navigator: NavigatorId,
    pub health: f32,
    pub health_limit: f32,
    pub energy: f32,
    pub energy_limit: f32,
    pub stress: f32,
    pub stress_limit: f32,
}

pub enum TaskStatus {
    OtherRunning,
    TaskCanBeStarted,
    TaskInProgress,
    OtherEnding,
}

impl Character {
    pub fn space(&self, body: &Body) -> SpaceId {
        body.space
    }

    pub fn validate_task(&self, next: Activity) -> TaskStatus {
        match self.task {
            Task::Idle => TaskStatus::TaskCanBeStarted,
            Task::PendingBeginning { .. } => TaskStatus::TaskCanBeStarted,
            Task::Beginning { activity, .. } => {
                if activity == next {
                    TaskStatus::TaskInProgress
                } else {
                    TaskStatus::OtherRunning
                }
            }
            Task::Running { activity } => {
                if activity == next {
                    TaskStatus::TaskInProgress
                } else {
                    TaskStatus::OtherRunning
                }
            }
            Task::PendingEnding { .. } => TaskStatus::OtherEnding,
            Task::Ending { .. } => TaskStatus::OtherEnding,
        }
    }
}

impl Task {
    pub fn ensure_spaceship_command(&self) -> Result<BridgeId, UniverseError> {
        match self {
            Self::Running { activity } => match activity {
                Activity::SpaceshipCommand { bridge } => Ok(*bridge),
                _ => Err(UniverseError::CharacterInvalidActivity {
                    activity: activity.clone(),
                }),
            },
            _ => Err(UniverseError::CharacterInvalidTask { task: self.clone() }),
        }
    }

    pub fn ensure_building_design(&self) -> Result<WorkbenchId, UniverseError> {
        match self {
            Self::Running { activity } => match activity {
                Activity::BuildingDesign { workbench } => Ok(*workbench),
                _ => Err(UniverseError::CharacterInvalidActivity {
                    activity: activity.clone(),
                }),
            },
            _ => Err(UniverseError::CharacterInvalidTask { task: self.clone() }),
        }
    }

    pub fn ensure_planting(&self) -> Result<PlanterId, UniverseError> {
        match self {
            Self::Running { activity } => match activity {
                Activity::Planting { planter } => Ok(*planter),
                _ => Err(UniverseError::CharacterInvalidActivity {
                    activity: activity.clone(),
                }),
            },
            _ => Err(UniverseError::CharacterInvalidTask { task: self.clone() }),
        }
    }

    pub fn ensure_storage_usage(&self) -> Result<StorageId, UniverseError> {
        match self {
            Self::Running { activity } => match activity {
                Activity::StorageUsage { storage } => Ok(*storage),
                _ => Err(UniverseError::CharacterInvalidActivity {
                    activity: activity.clone(),
                }),
            },
            _ => Err(UniverseError::CharacterInvalidTask { task: self.clone() }),
        }
    }
}

impl From<usize> for CharacterId {
    fn from(value: usize) -> Self {
        Self(value)
    }
}

impl From<usize> for GroupId {
    fn from(value: usize) -> Self {
        Self(value)
    }
}

impl From<&str> for CharacterKey {
    fn from(value: &str) -> Self {
        Self(value.into())
    }
}
