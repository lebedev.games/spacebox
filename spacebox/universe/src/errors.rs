use physics::SpaceId;
use piloting::SpacecraftId;
use serde::{Deserialize, Serialize};

use crate::{
    Activity, BedId, BedKey, BridgeId, BridgeKey, CharacterId, CharacterKey, ColonyId, ColonyKey,
    ConstructionId, ConstructionKey, CropId, CropKey, DeckId, PlanetId, PlanterId, PlanterKey,
    PropId, PropKey, SpaceshipId, StorageId, StorageKey, SystemId, SystemKey, Task, WorkbenchId,
    WorkbenchKey,
};

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(tag = "$type")]
pub enum UniverseError {
    ColonyPlanetNotFound { colony: ColonyId },
    ColonyExistInManyPlanets { colony: ColonyId },
    SpaceSystemNotFound { space: SpaceId },
    SpaceDeckNotFound { space: SpaceId },
    SpaceshipDeckNotFound { spaceship: SpaceshipId },
    SpaceColonyNotFound { space: SpaceId },
    CharacterNotFound { id: CharacterId },
    CropTypeNotFound { key: CropKey },
    CropNotFound { id: CropId },
    PropNotFound { id: PropId },
    PropTypeNotFound { key: PropKey },
    DeckNotFound { id: DeckId },
    ColonyTypeNotFound { key: ColonyKey },
    SystemNotFound { id: SystemId },
    SystemTypeNotFound { key: SystemKey },
    CharacterTypeNotFound { key: CharacterKey },
    ColonyNotFound { id: ColonyId },
    SpaceshipNotFound { id: SpaceshipId },
    SpaceshipNotFound2 { id: SpacecraftId },
    SpaceshipNotFound3 { id: DeckId },
    CharacterInvalidActivity { activity: Activity },
    CharacterInvalidTask { task: Task },
    CharacterNotBusy,
    CharacterBusy,
    BedNotFound { id: BedId },
    BedTypeNotFound { key: BedKey },
    BedAlreadyInUse { user: CharacterId },
    PlanterNotFound { id: PlanterId },
    PlanterTypeNotFound { key: PlanterKey },
    BridgeNotFound { id: BridgeId },
    BridgeTypeNotFound { key: BridgeKey },
    WorkbenchNotFound { id: WorkbenchId },
    WorkbenchTypeNotFound { key: WorkbenchKey },
    StorageNotFound { id: StorageId },
    StorageTypeNotFound { key: StorageKey },
    ConstructionNotFound { id: ConstructionId },
    ConstructionTypeNotFound { key: ConstructionKey },
    ConstructionSpaceInvalid,
    BridgeHasNoSpaceshipControl,
    SpaceshipNotLanded,
    PlanetNotFound { id: PlanetId },
}
