use serde::{Deserialize, Serialize};

use crate::{
    Bed, Bridge, Character, Colonization, Colony, Construction, Crop, Deck, Group, Permission,
    Planet, Planter, Prop, Spaceship, Station, Storage, System, Tree, Workbench,
};

#[derive(Debug, Clone, Serialize, Deserialize, Default)]
pub struct UniverseState {
    pub systems: Vec<System>,
    pub planets: Vec<Planet>,
    pub spaceships: Vec<Spaceship>,
    pub stations: Vec<Station>,
    pub decks: Vec<Deck>,
    pub colonies: Vec<Colony>,
    pub colonizations: Vec<Colonization>,
    pub characters: Vec<Character>,
    pub props: Vec<Prop>,
    pub crops: Vec<Crop>,
    pub trees: Vec<Tree>,
    pub groups: Vec<Group>,
    pub permissions: Vec<Permission>,
    pub storages: Vec<Storage>,
    pub workbenches: Vec<Workbench>,
    pub constructions: Vec<Construction>,
    pub beds: Vec<Bed>,
    pub planters: Vec<Planter>,
    pub bridges: Vec<Bridge>,
}
