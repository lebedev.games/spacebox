use std::collections::HashSet;
use std::iter::FromIterator;
use std::rc::Rc;

use physics::SpaceId;

use crate::{
    Bed, BedId, BedKey, BedType, Bridge, BridgeId, BridgeKey, BridgeType, Character, CharacterId,
    CharacterKey, CharacterType, Colonization, Colony, ColonyId, ColonyKey, ColonyType,
    Construction, ConstructionId, ConstructionKey, ConstructionType, Crop, CropId, CropKey,
    CropType, Deck, Planet, PlanetId, Planter, PlanterId, PlanterKey, PlanterType, Prop, PropId,
    PropKey, PropType, Spaceship, SpaceshipId, Storage, StorageId, StorageKey, StorageType, System,
    UniverseDatabase, UniverseError, UniverseState, Workbench, WorkbenchId, WorkbenchKey,
    WorkbenchType,
};

#[derive(Default)]
pub struct UniverseDomain {
    pub database: Rc<UniverseDatabase>,
    pub state: UniverseState,
}

impl UniverseDomain {
    pub fn index_spaceship(&self, id: SpaceshipId) -> Result<usize, UniverseError> {
        self.state
            .spaceships
            .iter()
            .position(|spaceship| spaceship.id == id)
            .ok_or(UniverseError::SpaceshipNotFound { id })
    }

    pub fn index_character(&self, id: CharacterId) -> Result<usize, UniverseError> {
        self.state
            .characters
            .iter()
            .position(|character| character.id == id)
            .ok_or(UniverseError::CharacterNotFound { id })
    }

    pub fn index_construction(&self, id: ConstructionId) -> Result<usize, UniverseError> {
        self.state
            .constructions
            .iter()
            .position(|construction| construction.id == id)
            .ok_or(UniverseError::ConstructionNotFound { id })
    }

    pub fn _index_prop(&self, id: PropId) -> Result<usize, UniverseError> {
        self.state
            .props
            .iter()
            .position(|prop| prop.id == id)
            .ok_or(UniverseError::PropNotFound { id })
    }

    pub fn _index_workbench(&self, id: WorkbenchId) -> Result<usize, UniverseError> {
        self.state
            .workbenches
            .iter()
            .position(|workbench| workbench.id == id)
            .ok_or(UniverseError::WorkbenchNotFound { id })
    }

    pub fn _index_storage(&self, id: StorageId) -> Result<usize, UniverseError> {
        self.state
            .storages
            .iter()
            .position(|storage| storage.id == id)
            .ok_or(UniverseError::StorageNotFound { id })
    }

    pub fn _index_planter(&self, id: PlanterId) -> Result<usize, UniverseError> {
        self.state
            .planters
            .iter()
            .position(|planter| planter.id == id)
            .ok_or(UniverseError::PlanterNotFound { id })
    }

    pub fn _index_bridge(&self, id: BridgeId) -> Result<usize, UniverseError> {
        self.state
            .bridges
            .iter()
            .position(|bridge| bridge.id == id)
            .ok_or(UniverseError::BridgeNotFound { id })
    }

    pub fn index_crop(&self, id: CropId) -> Result<usize, UniverseError> {
        self.state
            .crops
            .iter()
            .position(|crop| crop.id == id)
            .ok_or(UniverseError::CropNotFound { id })
    }

    pub fn get_character_type(&self, key: &CharacterKey) -> Result<&CharacterType, UniverseError> {
        self.database
            .characters
            .iter()
            .find(|character| character.key == *key)
            .ok_or(UniverseError::CharacterTypeNotFound { key: key.clone() })
    }

    pub fn get_system_by_space(&self, space: SpaceId) -> Result<&System, UniverseError> {
        self.state
            .systems
            .iter()
            .find(|system| system.space == space)
            .ok_or(UniverseError::SpaceSystemNotFound { space })
    }

    pub fn get_colony_type(&self, key: ColonyKey) -> Result<&ColonyType, UniverseError> {
        self.database
            .colonies
            .iter()
            .find(|prop| prop.key == key)
            .ok_or(UniverseError::ColonyTypeNotFound { key })
    }

    pub fn get_colony(&self, id: ColonyId) -> Result<&Colony, UniverseError> {
        self.state
            .colonies
            .iter()
            .find(|colony| colony.id == id)
            .ok_or(UniverseError::ColonyNotFound { id })
    }

    pub fn get_planet_by_colony(&self, colony: ColonyId) -> Result<&Planet, UniverseError> {
        let colonizations: Vec<&Colonization> = self
            .state
            .colonizations
            .iter()
            .filter(|colonization| colonization.colony == colony)
            .collect();
        let planets: HashSet<PlanetId> =
            HashSet::from_iter(colonizations.iter().map(|colonization| colonization.planet));
        match planets.len() {
            1 => {
                let planet = planets.into_iter().last().unwrap();
                self.get_planet(planet)
            }
            0 => Err(UniverseError::ColonyPlanetNotFound { colony }),
            _ => Err(UniverseError::ColonyExistInManyPlanets { colony }),
        }
    }

    pub fn get_colony_by_space(&self, space: SpaceId) -> Result<&Colony, UniverseError> {
        self.state
            .colonies
            .iter()
            .find(|colony| colony.space == space)
            .ok_or(UniverseError::SpaceColonyNotFound { space })
    }

    pub fn get_spaceship_deck(&self, spaceship: SpaceshipId) -> Result<&Deck, UniverseError> {
        self.state
            .decks
            .iter()
            .find(|deck| deck.spaceship == spaceship)
            .ok_or(UniverseError::SpaceshipDeckNotFound { spaceship })
    }

    pub fn get_deck_by_space(&self, space: SpaceId) -> Result<&Deck, UniverseError> {
        self.state
            .decks
            .iter()
            .find(|deck| deck.space == space)
            .ok_or(UniverseError::SpaceDeckNotFound { space })
    }

    pub fn get_prop_type(&self, key: &PropKey) -> Result<&PropType, UniverseError> {
        self.database
            .props
            .iter()
            .find(|prop| &prop.key == key)
            .ok_or(UniverseError::PropTypeNotFound { key: key.clone() })
    }

    pub fn get_prop(&self, id: PropId) -> Result<&Prop, UniverseError> {
        self.state
            .props
            .iter()
            .find(|prop| prop.id == id)
            .ok_or(UniverseError::PropNotFound { id })
    }

    pub fn get_construction(&self, id: ConstructionId) -> Result<&Construction, UniverseError> {
        self.state
            .constructions
            .iter()
            .find(|construction| construction.id == id)
            .ok_or(UniverseError::ConstructionNotFound { id })
    }

    pub fn get_construction_mut(
        &mut self,
        id: ConstructionId,
    ) -> Result<&mut Construction, UniverseError> {
        self.state
            .constructions
            .iter_mut()
            .find(|construction| construction.id == id)
            .ok_or(UniverseError::ConstructionNotFound { id })
    }

    pub fn get_construction_type(
        &self,
        key: &ConstructionKey,
    ) -> Result<&ConstructionType, UniverseError> {
        self.database
            .constructions
            .iter()
            .find(|construction| &construction.key == key)
            .ok_or(UniverseError::ConstructionTypeNotFound { key: key.clone() })
    }

    pub fn get_character(&self, id: CharacterId) -> Result<&Character, UniverseError> {
        self.state
            .characters
            .iter()
            .find(|character| character.id == id)
            .ok_or(UniverseError::CharacterNotFound { id })
    }

    pub fn get_crop_type(&self, key: &CropKey) -> Result<&CropType, UniverseError> {
        self.database
            .crops
            .iter()
            .find(|crop| &crop.key == key)
            .ok_or(UniverseError::CropTypeNotFound { key: key.clone() })
    }

    pub fn get_crop(&self, id: CropId) -> Result<&Crop, UniverseError> {
        self.state
            .crops
            .iter()
            .find(|crop| crop.id == id)
            .ok_or(UniverseError::CropNotFound { id })
    }

    pub fn get_character_mut(&mut self, id: CharacterId) -> Result<&mut Character, UniverseError> {
        self.state
            .characters
            .iter_mut()
            .find(|character| character.id == id)
            .ok_or(UniverseError::CharacterNotFound { id })
    }

    pub fn get_planet(&self, id: PlanetId) -> Result<&Planet, UniverseError> {
        self.state
            .planets
            .iter()
            .find(|planet| planet.id == id)
            .ok_or(UniverseError::PlanetNotFound { id })
    }

    pub fn get_planet_colonizations(&self, planet: PlanetId) -> Vec<&Colonization> {
        self.state
            .colonizations
            .iter()
            .filter(|colonization| colonization.planet == planet)
            .collect()
    }

    pub fn get_spaceship(&self, id: SpaceshipId) -> Result<&Spaceship, UniverseError> {
        self.state
            .spaceships
            .iter()
            .find(|spaceship| spaceship.id == id)
            .ok_or(UniverseError::SpaceshipNotFound { id })
    }

    pub fn get_planter(&self, id: PlanterId) -> Result<&Planter, UniverseError> {
        self.state
            .planters
            .iter()
            .find(|planter| planter.id == id)
            .ok_or(UniverseError::PlanterNotFound { id })
    }

    pub fn get_planter_type(&self, key: &PlanterKey) -> Result<&PlanterType, UniverseError> {
        self.database
            .planters
            .iter()
            .find(|planter| &planter.key == key)
            .ok_or(UniverseError::PlanterTypeNotFound { key: key.clone() })
    }

    pub fn get_workbench(&self, id: WorkbenchId) -> Result<&Workbench, UniverseError> {
        self.state
            .workbenches
            .iter()
            .find(|workbench| workbench.id == id)
            .ok_or(UniverseError::WorkbenchNotFound { id })
    }

    pub fn get_workbench_type(&self, key: &WorkbenchKey) -> Result<&WorkbenchType, UniverseError> {
        self.database
            .workbenches
            .iter()
            .find(|workbench| &workbench.key == key)
            .ok_or(UniverseError::WorkbenchTypeNotFound { key: key.clone() })
    }

    pub fn get_bridge(&self, id: BridgeId) -> Result<&Bridge, UniverseError> {
        self.state
            .bridges
            .iter()
            .find(|bridge| bridge.id == id)
            .ok_or(UniverseError::BridgeNotFound { id })
    }

    pub fn get_bridge_type(&self, key: &BridgeKey) -> Result<&BridgeType, UniverseError> {
        self.database
            .bridges
            .iter()
            .find(|bridge| &bridge.key == key)
            .ok_or(UniverseError::BridgeTypeNotFound { key: key.clone() })
    }

    pub fn get_bed(&self, id: BedId) -> Result<&Bed, UniverseError> {
        self.state
            .beds
            .iter()
            .find(|bed| bed.id == id)
            .ok_or(UniverseError::BedNotFound { id })
    }

    pub fn index_bed(&self, id: BedId) -> Result<usize, UniverseError> {
        self.state
            .beds
            .iter()
            .position(|bed| bed.id == id)
            .ok_or(UniverseError::BedNotFound { id })
    }

    pub fn get_bed_type(&self, key: &BedKey) -> Result<&BedType, UniverseError> {
        self.database
            .beds
            .iter()
            .find(|workbench| &workbench.key == key)
            .ok_or(UniverseError::BedTypeNotFound { key: key.clone() })
    }

    pub fn get_storage<I>(&self, id: I) -> Result<&Storage, UniverseError>
    where
        I: Into<StorageId>,
    {
        let id = id.into();
        self.state
            .storages
            .iter()
            .find(|storage| storage.id == id)
            .ok_or(UniverseError::StorageNotFound { id })
    }

    pub fn get_storage_type(&self, key: &StorageKey) -> Result<&StorageType, UniverseError> {
        self.database
            .storages
            .iter()
            .find(|storage| &storage.key == key)
            .ok_or(UniverseError::StorageTypeNotFound { key: key.clone() })
    }
}
