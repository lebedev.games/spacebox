use crate::{CropId, Universe, UniverseDomain, UniverseError};

pub struct DeleteCropOperation {
    pub crop: CropId,
    pub crop_index: usize,
}

impl UniverseDomain {
    pub fn prepare_delete_crop_operation(
        &self,
        crop: CropId,
    ) -> Result<DeleteCropOperation, UniverseError> {
        let crop_index = self.index_crop(crop)?;
        let crop = &self.state.crops[crop_index];
        Ok(DeleteCropOperation {
            crop_index,
            crop: crop.id,
        })
    }

    pub fn delete_crop(&mut self, operation: DeleteCropOperation) -> Vec<Universe> {
        self.state.crops.remove(operation.crop_index);
        vec![]
    }
}
