use crate::{BedId, CharacterId, Universe, UniverseDomain, UniverseError};

pub struct UseBedOperation {
    pub bed_index: usize,
    pub user: CharacterId,
}

impl UniverseDomain {
    pub fn prepare_use_bed_operation(
        &self,
        character: CharacterId,
        bed: BedId,
    ) -> Result<UseBedOperation, UniverseError> {
        let bed_index = self.index_bed(bed)?;
        let bed = &self.state.beds[bed_index];
        if let Some(user) = bed.user {
            return Err(UniverseError::BedAlreadyInUse { user }.into());
        }
        Ok(UseBedOperation {
            bed_index,
            user: character,
        })
    }

    pub fn use_bed(&mut self, operation: UseBedOperation) -> Vec<Universe> {
        let bed = &mut self.state.beds[operation.bed_index];
        bed.user = Some(operation.user);
        vec![Universe::BedUsed {
            id: bed.id,
            user: operation.user,
        }]
    }
}
