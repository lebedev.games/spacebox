use crate::{Activity, CharacterId, Task, Universe, UniverseDomain, UniverseError};

pub struct StartTaskOperation {
    pub character_index: usize,
    pub task: Task,
}

impl UniverseDomain {
    pub fn prepare_task_start(
        &self,
        character: CharacterId,
        activity: Activity,
    ) -> Result<StartTaskOperation, UniverseError> {
        let character_index = self.index_character(character)?;
        let character = &self.state.characters[character_index];

        let task = match character.task {
            Task::Idle => Task::PendingBeginning { activity },
            Task::PendingBeginning { activity } => Task::PendingBeginning { activity },
            _ => return Err(UniverseError::CharacterBusy),
        };

        Ok(StartTaskOperation {
            character_index,
            task,
        })
    }

    pub fn start_task(&mut self, operation: StartTaskOperation) -> Vec<Universe> {
        let character = &mut self.state.characters[operation.character_index];
        character.task = operation.task;
        vec![]
    }
}
