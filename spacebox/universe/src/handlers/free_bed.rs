use crate::{BedId, Universe, UniverseDomain, UniverseError};

pub struct FreeBedOperation {
    pub bed_index: usize,
}

impl UniverseDomain {
    pub fn prepare_free_bed(&self, bed: BedId) -> Result<FreeBedOperation, UniverseError> {
        let bed_index = self.index_bed(bed)?;
        Ok(FreeBedOperation { bed_index })
    }

    pub fn free_bed(&mut self, operation: FreeBedOperation) -> Vec<Universe> {
        let bed = &mut self.state.beds[operation.bed_index];
        bed.user = None;
        vec![Universe::BedFreed { id: bed.id }]
    }
}
