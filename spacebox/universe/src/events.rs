use physics::SpaceId;
use serde::{Deserialize, Serialize};

use crate::{Activity, BedId, CharacterId, ConstructionId, GroupId, Securable, Time};

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(tag = "$type")]
pub enum Universe {
    TimeUpdated {
        ms: u64,
    },
    ActivityBegun {
        character: CharacterId,
        activity: Activity,
        time: Time,
    },
    ActivityStarted {
        character: CharacterId,
        activity: Activity,
    },
    ActivityStopped {
        character: CharacterId,
        activity: Activity,
        time: Time,
    },
    ActivityEnded {
        character: CharacterId,
        activity: Activity,
    },
    ActivityTerminated {
        character: CharacterId,
        activity: Activity,
    },
    CharacterEnergyReduced {
        id: CharacterId,
        amount: f32,
        energy: f32,
        space: SpaceId,
    },
    CharacterEnergyRestored {
        id: CharacterId,
        amount: f32,
        energy: f32,
        space: SpaceId,
    },
    BedUsed {
        id: BedId,
        user: CharacterId,
    },
    BedFreed {
        id: BedId,
    },
    ConstructionProgressed {
        id: ConstructionId,
        progress: f32,
        amount: f32,
        space: SpaceId,
    },
    ConstructionComplete {
        id: ConstructionId,
        space: SpaceId,
    },
    PermissionGranted {
        securable: Securable,
        principal: GroupId,
    },
}
