use serde::{Deserialize, Serialize};

use crate::{
    BedType, BridgeType, CharacterType, ColonyType, ConstructionType, CropType, DeckType,
    PlanetType, PlanterType, PropType, SpaceshipType, StationType, StorageType, SystemType,
    TreeType, WorkbenchKey, WorkbenchType,
};

#[derive(Debug, Serialize, Deserialize, Default)]
pub struct UniverseDatabase {
    pub systems: Vec<SystemType>,
    pub planets: Vec<PlanetType>,
    pub spaceships: Vec<SpaceshipType>,
    pub stations: Vec<StationType>,
    pub decks: Vec<DeckType>,
    pub colonies: Vec<ColonyType>,
    pub characters: Vec<CharacterType>,
    pub props: Vec<PropType>,
    pub crops: Vec<CropType>,
    pub trees: Vec<TreeType>,
    pub storages: Vec<StorageType>,
    pub workbenches_fallback: WorkbenchType,
    pub workbenches: Vec<WorkbenchType>,
    pub constructions: Vec<ConstructionType>,
    pub beds: Vec<BedType>,
    pub planters: Vec<PlanterType>,
    pub bridges: Vec<BridgeType>,
}

impl UniverseDatabase {
    pub fn upsert_workbench(&mut self, workbench: WorkbenchType) {
        for ptr in 0..self.workbenches.len() {
            if self.workbenches[ptr].key == workbench.key {
                info!("Update type {:?}", workbench.key);
                self.workbenches[ptr] = workbench;
                return;
            }
        }

        info!("Append type {:?}", workbench.key);
        self.workbenches.push(workbench);
    }

    pub fn get_workbench_type(&self, key: WorkbenchKey) -> &WorkbenchType {
        match self
            .workbenches
            .iter()
            .find(|workbench| &workbench.key == &key)
        {
            Some(workbench) => workbench,
            None => {
                error!(
                    "Unable to get workbench type {:?}, not found, use fallback",
                    key
                );
                &self.workbenches_fallback
            }
        }
    }

    pub fn remove_workbench(&mut self, key: WorkbenchKey) {
        if let Some(index) = self
            .workbenches
            .iter()
            .position(|workbench| &workbench.key == &key)
        {
            info!("Remove type {:?}", key);
            self.workbenches.remove(index);
        }
    }

    pub fn upsert_bridge(&mut self, bridge: BridgeType) {
        for ptr in 0..self.bridges.len() {
            if self.bridges[ptr].key == bridge.key {
                info!("Update type {:?}", bridge.key);
                self.bridges[ptr] = bridge;
                return;
            }
        }

        info!("Append type {:?}", bridge.key);
        self.bridges.push(bridge);
    }
}
