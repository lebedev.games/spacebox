use std::collections::HashMap;
use std::hash::Hash;
use std::time::Instant;

pub struct NamedMap<T> {
    mapping: HashMap<String, T>,
    mapping_inverted: HashMap<T, String>,
}

impl<T> Default for NamedMap<T> {
    fn default() -> Self {
        NamedMap {
            mapping: Default::default(),
            mapping_inverted: Default::default(),
        }
    }
}

impl<T: Clone + Hash + Eq> NamedMap<T> {
    pub fn get(&self, key: String) -> Option<&T> {
        self.mapping.get(&key)
    }

    pub fn insert(&mut self, key: String, value: T) -> Option<T> {
        self.mapping_inverted.insert(value.clone(), key.clone());
        self.mapping.insert(key, value)
    }

    pub fn name(&self, value: T) -> Option<&String> {
        self.mapping_inverted.get(&value)
    }
}

pub fn measure_reference_time(units: usize, operations: usize) -> f32 {
    let now = Instant::now();
    let mut _data = 0.0;
    for unit in 0..units {
        let mut value = unit as f64;
        for operation in 0..operations {
            value *= value.sqrt();
            value = value + value.log(2.0) + operation as f64;
        }
        _data += value;
    }
    now.elapsed().as_secs_f32()
}

#[macro_export]
macro_rules! step {
    () => {{
        fn f() {}
        fn type_name_of<T>(_: T) -> &'static str {
            std::any::type_name::<T>()
        }
        let name = type_name_of(f);

        // Find and cut the rest of the path
        match &name[..name.len() - 3].rfind(':') {
            Some(pos) => &name[pos + 1..name.len() - 3],
            None => &name[..name.len() - 3],
        }
    }};
}

#[macro_export]
macro_rules! between {
    ($value:expr, $a:expr, $b:expr) => {
        if !($value >= $a && $value <= $b) {
            let error = format!(
                r#"Assertion failed: value {} out of range {} <-> {}"#,
                $value, $a, $b
            );
            Some(error)
        } else {
            None
        }
    };
}

#[macro_export]
macro_rules! is_ok {
    ($value:expr) => {
        if !($value.is_ok()) {
            let error = format!(
                r#"Assertion failed: {} should be Ok, but: {:?}"#,
                stringify!($value),
                $value
            );
            Some(error)
        } else {
            None
        }
    };
}

#[macro_export]
macro_rules! collection_equals {
    ($value:expr, $expected:expr) => {{
        let mut e = None;
        let mut i = 0;
        while i < $expected.len() {
            let expected_item = &$expected[i];
            if i >= $value.len() {
                let error = format!(
                    r#"Assertion failed: {} [{} of {}]
Expected: {:?},
     But: no value,
     All: {:?}"#,
                    stringify!($value),
                    i + 1,
                    $expected.len(),
                    expected_item,
                    $value
                );
                e = Some(error);
                break;
            }
            let value_item = &$value[i];
            if !(value_item == expected_item) {
                let error = format!(
                    r#"Assertion failed: {} [{} of {}]
Expected: {:?},
     But: {:?},
     All: {:?}"#,
                    stringify!($value),
                    i + 1,
                    $expected.len(),
                    expected_item,
                    value_item,
                    $value
                );
                e = Some(error);
                break;
            }
            i += 1;
        }
        while e.is_none() && i < $value.len() {
            let value_item = &$value[i];
            let error = format!(
                r#"Assertion failed: {} [{} of {}]
Expected: no value,
     But: {:?},
     All: {:?}"#,
                stringify!($value),
                i + 1,
                $expected.len(),
                value_item,
                $value
            );
            e = Some(error);
            break;
        }
        e
    }};
}

#[macro_export]
macro_rules! equals {
    ($value:expr, $expected:expr) => {
        if !($value == $expected) {
            let error = format!(
                r#"Assertion failed: {}
Expected: `{:?}`,
     But: `{:?}`"#,
                stringify!($value),
                $expected,
                $value
            );
            Some(error)
        } else {
            None
        }
    };
}

#[macro_export]
macro_rules! is_true {
    ($value:expr) => {
        if !($value) {
            let error = format!(r#"Assertion failed: {} should be true"#, stringify!($value));
            Some(error)
        } else {
            None
        }
    };
}

#[macro_export]
macro_rules! assert_that {
    ($matcher:expr) => {{
        match $matcher {
            Some(error) => {
                panic!(
                    r#"Step `{}` failed
{}"#,
                    step!(),
                    error
                )
            }
            None => {}
        }
    }};
    ($matcher:expr, $debug:expr) => {{
        match $matcher {
            Some(error) => {
                $debug;
                panic!(
                    r#"Step `{}` failed
{}"#,
                    step!(),
                    error
                )
            }
            None => {}
        }
    }};
}
