### Architecture

The fundamental idea is that instead of processing just the current state of the game,
use push-only sequence of events to describe all changes to the game state.
 
The event sequence acts as log and can be used to materialize views of the game objects. 
This leads to a number of features:

*   Flexibility. The game server publish these events so that consumers can be notified and 
    can handle the game state changes if needed. Consumers known about the type of event and 
    event data, but decoupled from the producer. In addition, multiple consumer can handle each event. 
    This enables easy development and integration with other sub-systems of the game: AI, UI, animation, graphics, etc.   
    
*   Performance. The action that initiated an event can continue,
    and consumers that handle the events can run in the background. 
    This can vastly improve performance and respond time of the game server.    

*   Responsive AI. The continual evaluating of transition conditions 
    of AI decision making engine can be expensive and evaluating all game state 
    can be challenging for AI developer. Rather than evaluating everything, 
    it makes more sense to provide game domain events, so that the AI developer 
    can make complex decisions when these events occur.

*   Game replay. It's possible for game client to store the history of events, 
    and use it to materialize the current state of game by playing back and consuming 
    all the events before specified time.

![./readme/fig1.png](.readme/fig1.png)

### Spacebox
    
*   Game

    An application for running game. Designed as server side of client-server architecture 
    to serve local and remote players on host computer.
    
    *   Master. Primary component of game processing. Holds actual state of game, provides for 
        other players what they perceive and apply game changes based on player actions.
        
    *   Observing. In general, observing is group of methods to implement player notification
        about game changes based only on player characters sight.
         
*   [AI](spacebox/src/ai/). A framework for defining and running AI agents for non-player game characters 
    and abstract entities like government of planet colony or flock of animals.
    
*   Network. Configures the network interface and all the network parameters 
    to implement client-server communication of remote players.
    
*   Persistence
    A library for serializing and deserializing game structures using supported database data format.

### Domains

It is the bounds within which certain game processes are implemented 
and certain rules/actions are applied. In fact, each domain is unique game mechanic.
Domains are independent Rust modules.

*   Universe

    A game world is an artificial universe, a core domain in which the events of the game occur.
    When the player performs any action, the universe is the first place where this action should be handled.
    
*   [Physics](physics)

    Provides an real-time simulation of rigid body dynamics including collision detection.
    This domain is the foundation to organize game world in bounded areas for performance optimization 
    and implementation of related algorithms.       
    
*   [Navigation](navigation)

    Navigation domain addresses the problem of finding a path from the starting point to the destination, 
    avoiding obstacles and optimizing costs (time, distance, equipment, etc). 
    By using both pathfinding and movement algorithms of physics domain
    we're trying to achieve best result of task to move game entity to destination.
     
*   Farming 

    Farming domain associated with planting, growing, and harvesting plants on the farm. 
    It's one of the main income sources for the game, and provides most of the ingredients 
    for character life support process.
       
*   Inventory

    While playing game, player will acquire a good many items. Some will be looted from fallen enemies. 
    Some will be purchased from a merchant or crafted. 
    Inventory domain represents these items, solves management and storage problems.       


### Testit

A library of macros allowing you to generally define assertion match rules in tests.  
