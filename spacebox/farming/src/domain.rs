use std::collections::HashMap;
use std::rc::Rc;

use crate::{
    Farming, FarmingDatabase, FarmingError, FarmingState, Farmland, FarmlandId, FarmlandKey,
    FarmlandType, Land, Place, Plant, PlantId, PlantKey, PlantState, PlantType,
};

#[derive(Default)]
pub struct FarmingDomain {
    pub database: Rc<FarmingDatabase>,
    pub state: FarmingState,
}

impl FarmingDomain {
    pub fn get_plant_type(&self, key: &PlantKey) -> Result<&PlantType, FarmingError> {
        self.database
            .plants
            .iter()
            .find(|plant| &plant.key == key)
            .ok_or(FarmingError::PlantTypeNotFound { key: key.clone() })
    }

    pub fn get_plant(&self, id: PlantId) -> Result<&Plant, FarmingError> {
        self.state
            .plants
            .iter()
            .find(|plant| plant.id == id)
            .ok_or(FarmingError::PlantNotFound { id })
    }

    pub fn update(&mut self, elapsed_days: f32) -> Vec<Farming> {
        let mut events = Vec::new();
        for p in 0..self.state.plants.len() {
            let plant = &self.state.plants[p];
            let plant_type = match self.get_plant_type(&plant.key) {
                Ok(plant_type) => plant_type.clone(),
                Err(error) => {
                    error!(
                        "Unable to update plant {:?} key={:?}, {:?}",
                        plant.id, &plant.key, error
                    );
                    continue;
                }
            };

            let next = match plant.state {
                PlantState::PlantingState { growth, watered } => {
                    let watered = watered - elapsed_days * plant_type.water_per_day;
                    if watered >= 0.0 {
                        let growth = growth + elapsed_days;
                        if growth >= plant_type.days_to_maturity {
                            PlantState::Maturity {
                                growth: 0.0,
                                watered,
                            }
                        } else {
                            PlantState::PlantingState { growth, watered }
                        }
                    } else {
                        PlantState::PlantingState {
                            growth,
                            watered: 0.0,
                        }
                    }
                }
                PlantState::Maturity { growth, watered } => {
                    let watered = watered - elapsed_days * plant_type.water_per_day;
                    if watered >= 0.0 {
                        let growth = growth + elapsed_days;
                        if growth >= plant_type.days_to_harvest {
                            PlantState::Harvest { time: 0.0, watered }
                        } else {
                            PlantState::Maturity { growth, watered }
                        }
                    } else {
                        PlantState::Maturity {
                            growth,
                            watered: 0.0,
                        }
                    }
                }
                PlantState::Harvest { time, watered } => {
                    let watered = watered - elapsed_days * plant_type.water_per_day;
                    let watered = watered.max(0.0);
                    PlantState::Harvest { time, watered }
                }
            };

            if plant.state != next {
                match next {
                    PlantState::PlantingState { growth, watered } => {
                        events.push(Farming::PlantGrowthProgressed {
                            id: plant.id,
                            farmland: plant.farmland,
                            growth,
                            watered,
                        })
                    }
                    PlantState::Maturity { growth, watered } => {
                        events.push(Farming::PlantMaturityProgressed {
                            id: plant.id,
                            farmland: plant.farmland,
                            growth,
                            watered,
                        })
                    }
                    PlantState::Harvest { time, watered } => {
                        events.push(Farming::PlantHarvestingProgressed {
                            id: plant.id,
                            farmland: plant.farmland,
                            time,
                            watered,
                        })
                    }
                }
                let plant = &mut self.state.plants[p];
                plant.state = next;
            }
        }

        events
    }

    pub fn prepare_farmland(
        &self,
        id: FarmlandId,
        key: &FarmlandKey,
    ) -> Result<Farmland, FarmingError> {
        let farmland = self.database.get_farmland_type(key)?;
        let farmland = Farmland {
            id,
            key: key.clone(),
            mapping: HashMap::new(),
            fertile: farmland.fertile,
        };

        Ok(farmland)
    }

    pub fn create_farmland(&mut self, farmland: Farmland) -> Vec<Farming> {
        self.state.farmlands.push(farmland);
        vec![]
    }

    pub fn _free_land(
        &mut self,
        farmland: FarmlandId,
        place: Place,
        width: usize,
        height: usize,
    ) -> Result<(), FarmingError> {
        let farmland = self._get_farmland_mut(farmland)?;
        for x in 0..width {
            for y in 0..height {
                let p = Place {
                    x: place.x + x,
                    y: place.y + y,
                };
                farmland.mapping.remove(&p);
            }
        }
        Ok(())
    }

    pub fn determine_land_at(&self, farmland: &Farmland, place: Place) -> Land {
        match farmland.mapping.get(&place) {
            Some(land) => *land,
            None => {
                if farmland.fertile {
                    Land::Fertile
                } else {
                    Land::Infertile
                }
            }
        }
    }

    fn _get_farmland_mut(&mut self, id: FarmlandId) -> Result<&mut Farmland, FarmingError> {
        self.state
            .farmlands
            .iter_mut()
            .find(|farmland| farmland.id == id)
            .ok_or(FarmingError::FarmlandNotFound { id })
    }

    pub fn index_farmland(&self, id: FarmlandId) -> Result<usize, FarmingError> {
        let index = self
            .state
            .farmlands
            .iter()
            .position(|farmland| farmland.id == id)
            .ok_or(FarmingError::FarmlandNotFound { id })?;

        Ok(index)
    }
}

impl FarmingDatabase {
    fn get_farmland_type(&self, key: &FarmlandKey) -> Result<&FarmlandType, FarmingError> {
        self.farmlands
            .iter()
            .find(|farmland| &farmland.key == key)
            .ok_or(FarmingError::FarmlandTypeNotFound { key: key.clone() })
    }
}

impl FarmingState {
    pub fn index_plant(&self, id: PlantId) -> Result<(usize, &Plant), FarmingError> {
        let index = self
            .plants
            .iter()
            .position(|plant| plant.id == id)
            .ok_or(FarmingError::PlantNotFound { id })?;

        Ok((index, &self.plants[index]))
    }
}
