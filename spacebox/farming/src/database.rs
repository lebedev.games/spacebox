use serde::{Deserialize, Serialize};

use crate::{FarmlandType, PlantType};

#[derive(Debug, Serialize, Deserialize, Default)]
pub struct FarmingDatabase {
    pub farmlands: Vec<FarmlandType>,
    pub plants: Vec<PlantType>,
}
