use serde::{Deserialize, Serialize};

use crate::{FarmlandId, FarmlandKey, Place, PlantId, PlantKey, PlantState};

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(tag = "$type")]
pub enum FarmingError {
    FarmlandTypeNotFound { key: FarmlandKey },
    PlantTypeNotFound { key: PlantKey },
    FarmlandNotFound { id: FarmlandId },
    PlantNotFound { id: PlantId },
    IncorrectStateToHarvest { state: PlantState },
    LandAlreadyCultivated { place: Place, plant: PlantId },
    LandInfertile { place: Place },
}
