use crate::{Farming, FarmingDomain, FarmingError, Land, Place, PlantId, PlantState};

#[derive(Debug)]
pub enum Harvesting {
    Progress(HarvestingProgress),
    Result(HarvestingResult),
}

#[derive(Debug)]
pub struct HarvestingProgress {
    pub target_plant_index: usize,
    pub time: f32,
    pub watered: f32,
}

#[derive(Debug)]
pub struct HarvestingResult {
    pub target_farmland_index: usize,
    pub target_plant_index: usize,
    pub plant_is_dead: bool,
    pub plant_place: Place,
    pub watered: f32,
    pub quality: f32,
}

impl FarmingDomain {
    pub fn update_harvesting(
        &self,
        id: PlantId,
        elapsed_time: f32,
    ) -> Result<Harvesting, FarmingError> {
        let (target_plant_index, plant) = self.state.index_plant(id)?;
        let target_farmland_index = self.index_farmland(plant.farmland)?;
        let plant_type = self.get_plant_type(&plant.key)?;

        let (time, watered) = match plant.state {
            PlantState::Harvest { time, watered } => (time, watered),
            _ => {
                return Err(FarmingError::IncorrectStateToHarvest {
                    state: plant.state.clone(),
                })
            }
        };

        let time = time + elapsed_time;

        if time >= plant_type.harvest_time {
            let plant_is_dead = if let Some(limit) = plant_type.max_harvests {
                plant.harvests + 1 >= limit
            } else {
                true
            };
            Ok(Harvesting::Result(HarvestingResult {
                target_farmland_index,
                target_plant_index,
                plant_is_dead,
                plant_place: plant.place,
                watered,
                quality: 1.0,
            }))
        } else {
            Ok(Harvesting::Progress(HarvestingProgress {
                target_plant_index,
                time,
                watered,
            }))
        }
    }

    pub fn continue_harvesting(&mut self, harvesting: HarvestingProgress) -> Vec<Farming> {
        let plant = &mut self.state.plants[harvesting.target_plant_index];
        plant.state = PlantState::Harvest {
            time: harvesting.time,
            watered: harvesting.watered,
        };
        vec![Farming::PlantHarvestingProgressed {
            id: plant.id,
            farmland: plant.farmland,
            time: harvesting.time,
            watered: harvesting.watered,
        }]
    }

    pub fn complete_harvesting(&mut self, harvesting: HarvestingResult) -> Vec<Farming> {
        let farmland = &mut self.state.farmlands[harvesting.target_farmland_index];
        let plant = &mut self.state.plants[harvesting.target_plant_index];

        plant.state = PlantState::Maturity {
            growth: 0.0,
            watered: harvesting.watered,
        };

        let mut events = vec![];

        if harvesting.plant_is_dead {
            events.push(Farming::PlantDestroyed {
                id: plant.id,
                farmland: plant.farmland,
            });
            let land = if farmland.fertile {
                Land::Fertile
            } else {
                Land::Infertile
            };
            events.push(Farming::LandChanged {
                place: plant.place,
                land,
                farmland: farmland.id,
            });
            self.state.plants.remove(harvesting.target_plant_index);
            farmland.mapping.remove(&harvesting.plant_place);
        } else {
            events.push(Farming::PlantMaturityProgressed {
                id: plant.id,
                farmland: plant.farmland,
                growth: 0.0,
                watered: harvesting.watered,
            });
            plant.harvests += 1;
        }

        events
    }
}
