use crate::{
    Farming, FarmingDomain, FarmingError, FarmlandId, Land, Place, Plant, PlantId, PlantKey,
    PlantKind, PlantState,
};

#[derive(Debug)]
pub struct PlantOperation {
    pub farmland_index: usize,
    pub place: Place,
    pub plant: Plant,
}

impl FarmingDomain {
    pub fn prepare_plant(
        &self,
        farmland: FarmlandId,
        id: PlantId,
        key: &PlantKey,
        kind: PlantKind,
        place: Place,
        _elapsed_time: f32,
    ) -> Result<PlantOperation, FarmingError> {
        let farmland_index = self.index_farmland(farmland)?;
        let farmland = &self.state.farmlands[farmland_index];
        let land = self.determine_land_at(farmland, place);

        match land {
            Land::Infertile => return Err(FarmingError::LandInfertile { place }),
            Land::Cultivated { plant } => {
                return Err(FarmingError::LandAlreadyCultivated { place, plant })
            }
            _ => {}
        }

        let plant = Plant {
            id,
            key: key.clone(),
            kind,
            state: PlantState::PlantingState {
                watered: 0.0,
                growth: 0.0,
            },
            farmland: farmland.id,
            harvests: 0,
            place,
        };

        Ok(PlantOperation {
            farmland_index,
            place,
            plant,
        })
    }

    pub fn plant(&mut self, operation: PlantOperation) -> Vec<Farming> {
        let farmland = &mut self.state.farmlands[operation.farmland_index];
        let land = Land::Cultivated {
            plant: operation.plant.id,
        };
        farmland.mapping.insert(operation.place, land);
        let events = vec![Farming::LandChanged {
            place: operation.place,
            land,
            farmland: farmland.id,
        }];
        self.state.plants.push(operation.plant);
        events
    }
}
