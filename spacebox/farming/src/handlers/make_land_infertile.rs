use std::collections::HashMap;

use crate::{Farming, FarmingDomain, FarmingError, FarmlandId, Land, Place};

#[derive(Debug)]
pub struct MakeLandInfertileOperation {
    pub farmland_index: usize,
    pub farmland_mapping_update: HashMap<Place, Land>,
}

impl FarmingDomain {
    pub fn prepare_make_land_infertile_operation(
        &self,
        farmland: FarmlandId,
        place: Place,
        width: usize,
        height: usize,
    ) -> Result<MakeLandInfertileOperation, FarmingError> {
        let farmland_index = self.index_farmland(farmland)?;
        let mut farmland_mapping_update = HashMap::new();
        for x in 0..width {
            for y in 0..height {
                let p = Place {
                    x: place.x + x,
                    y: place.y + y,
                };
                farmland_mapping_update.insert(p, Land::Infertile);
            }
        }
        Ok(MakeLandInfertileOperation {
            farmland_index,
            farmland_mapping_update,
        })
    }

    pub fn make_land_infertile(&mut self, operation: MakeLandInfertileOperation) -> Vec<Farming> {
        let farmland = &mut self.state.farmlands[operation.farmland_index];
        let farmland_id = farmland.id;

        let events = operation
            .farmland_mapping_update
            .keys()
            .into_iter()
            .map(|place| Farming::LandChanged {
                place: *place,
                land: Land::Infertile,
                farmland: farmland_id,
            })
            .collect();
        farmland.mapping.extend(operation.farmland_mapping_update);
        events
    }
}
