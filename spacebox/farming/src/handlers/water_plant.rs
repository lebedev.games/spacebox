use crate::{FarmingDomain, FarmingError, PlantId, PlantState};

#[derive(Debug)]
pub struct WaterOperation {
    pub target_plant_index: usize,
    pub plant_state: PlantState,
}

impl FarmingDomain {
    pub fn _water(&self, id: PlantId) -> Result<WaterOperation, FarmingError> {
        let (target_plant_index, plant) = self.state.index_plant(id)?;

        let plant_state = match plant.state {
            PlantState::PlantingState { growth, .. } => PlantState::PlantingState {
                watered: 1.0,
                growth,
            },
            PlantState::Maturity { growth, .. } => PlantState::Maturity {
                watered: 1.0,
                growth,
            },
            PlantState::Harvest { time, .. } => PlantState::Harvest { time, watered: 1.0 },
        };

        Ok(WaterOperation {
            target_plant_index,
            plant_state,
        })
    }

    pub fn _apply_water_operation(&mut self, water: WaterOperation) {
        let plant = &mut self.state.plants[water.target_plant_index];
        plant.state = water.plant_state;
    }
}
