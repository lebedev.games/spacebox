use serde::{Deserialize, Serialize};

use crate::{FarmlandId, Land, Place, PlantId};

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(tag = "$type")]
pub enum Farming {
    PlantGrowthProgressed {
        id: PlantId,
        farmland: FarmlandId,
        growth: f32,
        watered: f32,
    },
    PlantMaturityProgressed {
        id: PlantId,
        farmland: FarmlandId,
        growth: f32,
        watered: f32,
    },
    PlantHarvestingProgressed {
        id: PlantId,
        farmland: FarmlandId,
        time: f32,
        watered: f32,
    },
    PlantDestroyed {
        id: PlantId,
        farmland: FarmlandId,
    },
    LandChanged {
        place: Place,
        land: Land,
        farmland: FarmlandId,
    },
}
