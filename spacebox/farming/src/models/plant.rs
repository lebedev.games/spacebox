use serde::{Deserialize, Serialize};

use crate::{FarmlandId, Place};

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq, Hash, Eq)]
pub struct PlantId(pub usize);

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct PlantKey(pub String);

impl From<&str> for PlantKey {
    fn from(value: &str) -> Self {
        Self(value.into())
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct PlantType {
    pub key: PlantKey,
    pub days_to_maturity: f32,
    pub days_to_harvest: f32,
    pub max_harvests: Option<usize>,
    pub water_per_day: f32,
    pub harvest_time: f32,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq)]
pub enum PlantKind {
    Crop,
    Tree,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(tag = "$type")]
pub enum PlantState {
    PlantingState { growth: f32, watered: f32 },
    Maturity { growth: f32, watered: f32 },
    Harvest { time: f32, watered: f32 },
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct Plant {
    pub id: PlantId,
    pub key: PlantKey,
    pub kind: PlantKind,
    pub state: PlantState,
    pub farmland: FarmlandId,
    pub harvests: usize,
    pub place: Place,
}

impl From<usize> for PlantId {
    fn from(value: usize) -> Self {
        Self(value)
    }
}
