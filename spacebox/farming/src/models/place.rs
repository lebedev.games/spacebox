use serde::{Deserialize, Serialize};

#[derive(Debug, Copy, Clone, Serialize, Deserialize, Eq, PartialEq, Hash)]
pub struct Place {
    pub x: usize,
    pub y: usize,
}

impl From<(usize, usize)> for Place {
    fn from(value: (usize, usize)) -> Self {
        Place {
            x: value.0,
            y: value.1,
        }
    }
}
