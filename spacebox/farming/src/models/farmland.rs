use std::collections::HashMap;

use serde::{Deserialize, Serialize};

use crate::{Place, PlantId};

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq, Hash, Eq)]
pub struct FarmlandId(pub usize);

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct FarmlandKey(pub String);

impl From<&str> for FarmlandKey {
    fn from(value: &str) -> Self {
        Self(value.into())
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct FarmlandType {
    pub key: FarmlandKey,
    pub size: Place,
    pub fertile: bool,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq)]
#[serde(tag = "$type")]
pub enum Land {
    Infertile,
    Fertile,
    Cultivated { plant: PlantId },
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Farmland {
    pub id: FarmlandId,
    pub key: FarmlandKey,
    pub mapping: HashMap<Place, Land>,
    pub fertile: bool,
}

impl From<usize> for FarmlandId {
    fn from(value: usize) -> Self {
        Self(value)
    }
}
