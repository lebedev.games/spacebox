use serde::{Deserialize, Serialize};

use crate::{Farmland, Plant};

#[derive(Debug, Clone, Serialize, Deserialize, Default)]
pub struct FarmingState {
    pub farmlands: Vec<Farmland>,
    pub plants: Vec<Plant>,
}
