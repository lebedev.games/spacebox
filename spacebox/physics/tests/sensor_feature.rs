mod testing;
use physics::BodyOrigin;
use physics::Physics::{
    BodyMotionSignalLost, BodyMotionSignalReceived, BodyMotionSignalUpdated, BodyPositionUpdated,
};
pub use testing::*;

#[test]
fn test_no_body_motion_signal_lost_in_same_update_frame() {
    PhysicsDomainTestScenario::new()
        // Arrange
        .given_space("space")
        .given_body_type("human", 3.0)
        .given_sensor_type("sensor_key", 20.0)
        .given_body("space", "alice", "human", (50.0, 50.0))
        .given_dynamic_sensor("space", "sensor", "sensor_key", "alice")
        .given_body("space", "boris", "human", (35.0, 50.0))
        // Act
        .when_body_move_instantly("alice", (70.0, 50.0))
        .when_update_physics_for(1, real_seconds())
        // Assert
        .then_signal_should_be("space", "sensor", "boris", 15.0, 1.0);
}

#[test]
fn test_body_motion_signal_lost_on_sensor_move_out() {
    PhysicsDomainTestScenario::new()
        // Arrange
        .given_space("space")
        .given_body_type("human", 3.0)
        .given_sensor_type("sensor_key", 20.0)
        .given_body("space", "alice", "human", (50.0, 50.0))
        .given_dynamic_sensor("space", "sensor", "sensor_key", "alice")
        .given_body("space", "boris", "human", (35.0, 50.0))
        // Act
        .when_body_move_instantly("alice", (70.0, 50.0))
        .when_update_physics_for(2, real_seconds())
        // Assert
        .then_signal_should_not_exist("space", "sensor", "boris")
        .then_events_should_be(|it| {
            vec![
                // frame 0
                BodyPositionUpdated {
                    body: it.body("alice"),
                    space: it.space("space"),
                    position: (70, 50).into(),
                    velocity: (20, 0).into(),
                },
                BodyMotionSignalReceived {
                    body: it.body("alice"),
                    origin: BodyOrigin::Character,
                    strength: 1.0,
                    distance: 20.0,
                    sensor: it.sensor("sensor"),
                },
                BodyMotionSignalUpdated {
                    body: it.body("boris"),
                    strength: 1.0,
                    distance: 15.0,
                    sensor: it.sensor("sensor"),
                },
                // frame 1
                BodyPositionUpdated {
                    body: it.body("alice"),
                    space: it.space("space"),
                    position: (70, 50).into(),
                    velocity: (0, 0).into(),
                },
                BodyMotionSignalUpdated {
                    body: it.body("alice"),
                    strength: 1.0,
                    distance: 0.0,
                    sensor: it.sensor("sensor"),
                },
                BodyMotionSignalLost {
                    body: it.body("boris"),
                    origin: BodyOrigin::Character,
                    sensor: it.sensor("sensor"),
                },
            ]
        });
}

#[test]
fn test_dynamic_sensor_movement() {
    PhysicsDomainTestScenario::new()
        // Arrange
        .given_space("space")
        .given_body_type("body", 3.0)
        .given_body("space", "body", "body", (20.0, 50.0))
        .given_sensor_type("sensor_key", 20.0)
        .given_dynamic_sensor("space", "sensor", "sensor_key", "body")
        // Act
        .when_body_move_towards("body", (30.0, 50.0))
        .when_update_physics_for(4, real_seconds())
        // Assert
        .then_sensor_position_should_be("sensor", (30.0, 50.0).into());
}

#[test]
fn test_single_barrier_creation_signal_update() {
    PhysicsDomainTestScenario::new()
        // Arrange
        .given_space("space")
        .given_sensor_type("sensor_key", 20.0)
        .given_sensor("space", "sensor", "sensor_key", (50.0, 50.0))
        .given_body_type("body", 3.0)
        .given_body("space", "body", "body", (35.0, 50.0))
        .given_barrier_type("barrier", (5.0, 20.0))
        // Act
        .when_create_barrier("space", "barrier", "barrier", (40.0, 50.0))
        // Assert
        .then_signal_should_be("space", "sensor", "body", 15.0, 0.0)
        .then_events_should_be(|it| {
            vec![BodyMotionSignalUpdated {
                body: it.body("body"),
                strength: 0.0,
                distance: 15.0,
                sensor: it.sensor("sensor"),
            }]
        });
}

#[test]
fn test_body_motion_signal_trough_barrier() {
    PhysicsDomainTestScenario::new()
        // Arrange
        .given_space("space")
        .given_sensor_type("sensor_key", 20.0)
        .given_sensor("space", "sensor", "sensor_key", (50.0, 50.0))
        .given_body_type("body", 3.0)
        .given_body("space", "body", "body", (20.0, 50.0))
        .given_barrier_type("barrier", (5.0, 20.0))
        .given_barrier("space", "barrier", "barrier", (40.0, 50.0))
        // Act
        .when_body_move_instantly("body", (35.0, 50.0))
        .when_update_physics_for(1, real_seconds())
        // Assert
        .then_signal_should_be("space", "sensor", "body", 15.0, 0.0)
        .then_events_should_be(|it| {
            vec![
                BodyPositionUpdated {
                    body: it.body("body"),
                    space: it.space("space"),
                    position: (35.0, 50.0).into(),
                    velocity: (15.0, 0.0).into(),
                },
                BodyMotionSignalReceived {
                    body: it.body("body"),
                    origin: BodyOrigin::Character,
                    strength: 0.0,
                    distance: 15.0,
                    sensor: it.sensor("sensor"),
                },
            ]
        });
}

#[test]
fn test_single_body_creation_signal_reception() {
    PhysicsDomainTestScenario::new()
        // Arrange
        .given_space("space")
        .given_sensor_type("sensor_key", 20.0)
        .given_sensor("space", "sensor", "sensor_key", (50.0, 50.0))
        .given_body_type("body", 3.0)
        // Act
        .when_create_body("space", "body", "body", (35.0, 50.0))
        // Assert
        .then_signal_should_be("space", "sensor", "body", 15.0, 1.0)
        .then_events_should_be(|it| {
            vec![BodyMotionSignalReceived {
                body: it.body("body"),
                origin: BodyOrigin::Character,
                strength: 1.0,
                distance: 15.0,
                sensor: it.sensor("sensor"),
            }]
        });
}

#[test]
fn test_single_body_motion_signal_update() {
    PhysicsDomainTestScenario::new()
        // Arrange
        .given_space("space")
        .given_sensor_type("sensor_key", 20.0)
        .given_sensor("space", "sensor", "sensor_key", (50.0, 50.0))
        .given_body_type("body", 3.0)
        .given_body("space", "body", "body", (35.0, 50.0))
        // Act
        .when_body_move_instantly("body", (50.0, 60.0))
        .when_update_physics_for(1, real_seconds())
        // Assert
        .then_signal_should_be("space", "sensor", "body", 10.0, 1.0)
        .then_events_should_be(|it| {
            vec![
                BodyPositionUpdated {
                    body: it.body("body"),
                    space: it.space("space"),
                    position: (50.0, 60.0).into(),
                    velocity: (15.0, 10.0).into(),
                },
                BodyMotionSignalUpdated {
                    body: it.body("body"),
                    strength: 1.0,
                    distance: 10.0,
                    sensor: it.sensor("sensor"),
                },
            ]
        });
}

#[test]
fn test_single_body_motion_signal_lost() {
    PhysicsDomainTestScenario::new()
        // Arrange
        .given_space("space")
        .given_sensor_type("sensor_key", 20.0)
        .given_sensor("space", "sensor", "sensor_key", (50.0, 50.0))
        .given_body_type("body", 3.0)
        .given_body("space", "body", "body", (35.0, 50.0))
        // Act
        .when_body_move_instantly("body", (20.0, 50.0))
        .when_update_physics_for(1, real_seconds())
        // Assert
        .then_signal_should_not_exist("space", "sensor", "body")
        .then_events_should_be(|it| {
            vec![
                BodyPositionUpdated {
                    body: it.body("body"),
                    space: it.space("space"),
                    position: (20.0, 50.0).into(),
                    velocity: (-15.0, 0.0).into(),
                },
                BodyMotionSignalLost {
                    body: it.body("body"),
                    origin: BodyOrigin::Character,
                    sensor: it.sensor("sensor"),
                },
            ]
        });
}

#[test]
fn test_single_body_motion_signal_reception() {
    PhysicsDomainTestScenario::new()
        // Arrange
        .given_space("space")
        .given_sensor_type("sensor_key", 20.0)
        .given_sensor("space", "sensor", "sensor_key", (50.0, 50.0))
        .given_body_type("body", 3.0)
        .given_body("space", "body", "body", (20.0, 50.0))
        // Act
        .when_body_move_instantly("body", (35.0, 50.0))
        .when_update_physics_for(1, real_seconds())
        // Assert
        .then_signal_should_be("space", "sensor", "body", 15.0, 1.0)
        .then_events_should_be(|it| {
            vec![
                BodyPositionUpdated {
                    body: it.body("body"),
                    space: it.space("space"),
                    position: (35.0, 50.0).into(),
                    velocity: (15.0, 0.0).into(),
                },
                BodyMotionSignalReceived {
                    body: it.body("body"),
                    origin: BodyOrigin::Character,
                    strength: 1.0,
                    distance: 15.0,
                    sensor: it.sensor("sensor"),
                },
            ]
        });
}
