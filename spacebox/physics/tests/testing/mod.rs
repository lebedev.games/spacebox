use std::panic;
use std::rc::Rc;
use std::time::Duration;

use env_logger::Env;
use log::{info, LevelFilter};
use physics::{
    is_rotated_horizontally, Barrier, BarrierId, BarrierOrigin, BarrierType, Body, BodyId,
    BodyOrigin, BodyType, Physics, PhysicsDatabase, PhysicsDomain, Portal, PortalId, PortalKey,
    PortalType, Sensor, SensorId, SensorType, Signal, SpaceId, SpaceOrigin, SteeringBehaviour,
    Vector,
};
use plotly::common::{DashType, HoverInfo, Line, Marker, Mode, Title};
use plotly::layout::{Axis, Margin, Shape, ShapeLayer, ShapeLine, ShapeType};
use plotly::{Layout, Plot, Scatter};
use testit::*;

pub struct PhysicsDomainTestScenario {
    physics: PhysicsDomain,
    events: Vec<Physics>,
    named_spaces: NamedMap<SpaceId>,
    named_bodies: NamedMap<BodyId>,
    named_barriers: NamedMap<BarrierId>,
    named_sensors: NamedMap<SensorId>,
    counter: usize,
}

impl PhysicsDomainTestScenario {
    pub fn new() -> Self {
        let _ = env_logger::Builder::from_env(Env::default())
            .filter_level(LevelFilter::Debug)
            .try_init();

        PhysicsDomainTestScenario {
            physics: PhysicsDomain::default(),
            events: Vec::new(),
            named_spaces: Default::default(),
            named_bodies: Default::default(),
            named_barriers: Default::default(),
            named_sensors: Default::default(),
            counter: 0,
        }
    }

    pub fn generate_id(&mut self) -> usize {
        self.counter += 1;
        self.counter
    }

    pub fn space(&self, name: &str) -> SpaceId {
        self.named_spaces
            .get(name.into())
            .expect(&format!("test named space '{}' not specified", name))
            .clone()
    }

    pub fn body(&self, name: &str) -> BodyId {
        self.named_bodies
            .get(name.into())
            .expect(&format!("test named body '{}' not specified", name))
            .clone()
    }

    pub fn _barrier(&self, name: &str) -> BarrierId {
        self.named_barriers
            .get(name.into())
            .expect(&format!("test named barrier '{}' not specified", name))
            .clone()
    }

    pub fn sensor(&self, name: &str) -> SensorId {
        self.named_sensors
            .get(name.into())
            .expect(&format!("test named sensor '{}' not specified", name))
            .clone()
    }

    pub fn database(&mut self) -> &mut PhysicsDatabase {
        Rc::get_mut(&mut self.physics.database).unwrap()
    }

    pub fn given_space(mut self, name: &str) -> Self {
        let key = &"space".into();
        let id = self.generate_id();
        let space = self
            .physics
            .prepare_space(id.into(), key, SpaceOrigin::Colony)
            .unwrap();
        self.physics.create_space(space);
        self.named_spaces.insert(name.into(), id.into());
        self
    }

    pub fn given_sensor_type(mut self, key: &str, radius: f32) -> Self {
        self.database().sensors.push(SensorType {
            key: key.into(),
            radius,
        });
        self
    }

    pub fn given_sensor(
        mut self,
        space: &str,
        name: &str,
        key: &str,
        position: (f32, f32),
    ) -> Self {
        let id = self.generate_id();
        let sensor = self
            .physics
            .prepare_sensor(self.space(space), id.into(), &key.into(), position.into())
            .unwrap();
        self.physics.create_sensor(sensor);
        self.named_sensors.insert(name.into(), id.into());
        self
    }

    pub fn given_dynamic_sensor(mut self, space: &str, name: &str, key: &str, body: &str) -> Self {
        let id = self.generate_id();
        let sensor = self
            .physics
            .prepare_dynamic_sensor(self.space(space), id.into(), &key.into(), self.body(body))
            .unwrap();
        self.physics.create_sensor(sensor);
        self.named_sensors.insert(name.into(), id.into());
        self
    }

    pub fn given_barrier_type(mut self, key: &str, size: (f32, f32)) -> Self {
        self.database().barriers.push(BarrierType {
            key: key.into(),
            size: size.into(),
        });
        self
    }

    pub fn given_barrier(
        mut self,
        space: &str,
        name: &str,
        key: &str,
        position: (f32, f32),
    ) -> Self {
        let id = self.generate_id();
        let barrier = self
            .physics
            .prepare_barrier(
                self.space(space),
                id.into(),
                &key.into(),
                BarrierOrigin::Prop,
                position.into(),
                0.0,
            )
            .unwrap();
        self.physics.create_barrier(barrier);
        self.named_barriers.insert(name.into(), id.into());
        self
    }

    pub fn given_portal_type(mut self, key: &str, offset: (f32, f32)) -> Self {
        self.database().portals.push(PortalType {
            key: PortalKey(key.into()),
            offset: offset.into(),
        });
        self
    }

    pub fn given_portal(mut self, space: &str, key: &str, position: (f32, f32)) -> Self {
        let portal = self
            .physics
            .prepare_portal(
                self.space(space),
                PortalId(0),
                &PortalKey(key.into()),
                position.into(),
            )
            .unwrap();
        self.physics.create_portal(portal);
        self
    }

    pub fn given_body_type(mut self, key: &str, speed: f32) -> Self {
        self.database().bodies.push(BodyType {
            key: key.into(),
            speed,
            origin: BodyOrigin::Character,
        });
        self
    }

    pub fn given_body(mut self, space: &str, name: &str, key: &str, position: (f32, f32)) -> Self {
        let id = self.generate_id();
        let body = self
            .physics
            .prepare_body(self.space(space), id.into(), &key.into(), position.into())
            .unwrap();
        self.physics.create_body(body);
        self.named_bodies.insert(name.into(), id.into());
        self
    }

    pub fn given_spaces_connection(
        mut self,
        space: &str,
        parent: &str,
        connect_point: (f32, f32),
    ) -> Self {
        let changes = self
            .physics
            .prepare_connect_spaces_operation(
                self.space(space),
                self.space(parent),
                connect_point.into(),
            )
            .unwrap();
        self.physics.connect_spaces(changes);
        self
    }

    pub fn when_connect_spaces(
        mut self,
        space: &str,
        parent: &str,
        connect_point: (f32, f32),
    ) -> Self {
        let changes = self
            .physics
            .prepare_connect_spaces_operation(
                self.space(space),
                self.space(parent),
                connect_point.into(),
            )
            .unwrap();
        let events = self.physics.connect_spaces(changes);
        self.events.extend(events);
        self
    }

    pub fn when_body_move_instantly(mut self, body: &str, destination: (f32, f32)) -> Self {
        self.physics
            .set_body_steering_behaviour(
                self.body(body),
                SteeringBehaviour::MoveInstantly {
                    destination: destination.into(),
                },
            )
            .unwrap();
        self
    }

    pub fn when_body_move_towards(mut self, body: &str, destination: (f32, f32)) -> Self {
        self.physics
            .set_body_steering_behaviour(
                self.body(body),
                SteeringBehaviour::MoveTowards {
                    destination: destination.into(),
                },
            )
            .unwrap();
        self
    }

    pub fn when_body_follow_path(mut self, body: &str, path: Vec<(f32, f32)>) -> Self {
        let path: Vec<Vector> = path.into_iter().map(|point| point.into()).collect();
        self.physics
            .set_body_steering_behaviour(self.body(body), SteeringBehaviour::FollowPath { path })
            .unwrap();
        self
    }

    pub fn when_update_physics_for(mut self, iterations: usize, time_delta: Duration) -> Self {
        for _ in 0..iterations {
            let events = self.physics.update(time_delta.as_secs_f32());
            self.events.extend(events);
        }
        self
    }

    pub fn when_create_body(
        mut self,
        space: &str,
        name: &str,
        key: &str,
        position: (f32, f32),
    ) -> Self {
        let id = self.generate_id();
        let body = self
            .physics
            .prepare_body(self.space(space), id.into(), &key.into(), position.into())
            .unwrap();
        let events = self.physics.create_body(body);
        self.events.extend(events);
        self.named_bodies.insert(name.into(), id.into());
        self
    }

    pub fn when_create_barrier(
        mut self,
        space: &str,
        name: &str,
        key: &str,
        position: (f32, f32),
    ) -> Self {
        let id = self.generate_id();
        let barrier = self
            .physics
            .prepare_barrier(
                self.space(space),
                id.into(),
                &key.into(),
                BarrierOrigin::Prop,
                position.into(),
                0.0,
            )
            .unwrap();
        let events = self.physics.create_barrier(barrier);
        self.events.extend(events);
        self.named_barriers.insert(name.into(), id.into());
        self
    }

    pub fn then_body_position_should_be(self, body: &str, expected_position: (f32, f32)) -> Self {
        let body = self.physics.get_body(self.body(body)).unwrap();
        let expected_position: Vector = expected_position.into();
        assert_that!(
            equals!(body.position, expected_position),
            PhysicsDomainDebug::new(&self)
                .render_space(body.space)
                .render_expected_point(body.position, expected_position)
                .output()
        );
        self
    }

    pub fn then_sensor_position_should_be(
        self,
        sensor: &str,
        expected_position: (f32, f32),
    ) -> Self {
        let sensor = self.physics.get_sensor(self.sensor(sensor)).unwrap();
        let expected_position: Vector = expected_position.into();
        assert_that!(
            is_true!(sensor.position.distance(expected_position) < 0.001),
            PhysicsDomainDebug::new(&self)
                .render_space(sensor.space)
                .render_expected_point(sensor.position, expected_position)
                .output()
        );
        self
    }

    pub fn then_signal_should_not_exist(self, space: &str, sensor: &str, body: &str) -> Self {
        let signal = (self.sensor(sensor), self.body(body));
        let signal = self.physics.state.signals.get(&signal);
        assert_that!(
            is_true!(signal.is_none()),
            PhysicsDomainDebug::new(&self)
                .render_space(self.space(space))
                .output()
        );
        self
    }

    pub fn then_signal_should_be(
        self,
        space: &str,
        sensor: &str,
        body: &str,
        distance: f32,
        strength: f32,
    ) -> Self {
        let debug = || {
            PhysicsDomainDebug::new(&self)
                .render_space(self.space(space))
                .render_signal(
                    self.body(body),
                    Signal {
                        sensor: self.sensor(sensor),
                        distance,
                        strength,
                    },
                )
                .output()
        };
        let signal = (self.sensor(sensor), self.body(body));
        match self.physics.state.signals.get(&signal) {
            None => panic!("{} failed, signal not found", step!()),
            Some(signal) => {
                assert_that!(equals!(signal.distance, distance), debug());
                assert_that!(equals!(signal.strength, strength), debug());
            }
        }
        self
    }

    pub fn then_events_should_be<F>(self, event_builder: F) -> Self
    where
        F: Fn(&Self) -> Vec<Physics>,
    {
        assert_that!(collection_equals!(self.events, event_builder(&self)));
        self
    }
}

pub struct PhysicsDomainDebug<'a> {
    scenario: &'a PhysicsDomainTestScenario,
    plot: Plot,
    layout: Layout,
}

impl<'a> PhysicsDomainDebug<'a> {
    pub fn new(scenario: &'a PhysicsDomainTestScenario) -> Self {
        PhysicsDomainDebug {
            scenario,
            plot: Plot::new(),
            layout: Layout::new(),
        }
    }

    pub fn output(mut self) {
        let path = "./output/test_something.html";
        info!("Save debug to {}", path);
        self.plot.set_layout(self.layout);
        self.plot.to_html(path);
    }

    pub fn render_expected_point(mut self, actual: Vector, expected: Vector) -> Self {
        let x_data = vec![actual.x, expected.x];
        let y_data = vec![actual.y, expected.y];
        let trace = Scatter::new(x_data, y_data)
            .name("expected_point")
            .mode(Mode::LinesMarkers)
            .text("")
            .hover_info(HoverInfo::None)
            .line(Line::new().color("#b9b9b9").dash(DashType::Dot))
            .opacity(0.4)
            .show_legend(false);
        self.plot.add_trace(trace);
        self
    }

    pub fn render_signal(mut self, body: BodyId, expected_signal: Signal) -> Self {
        let physics = &self.scenario.physics;
        let body = physics.get_body(body).unwrap();
        let sensor = physics.get_sensor(expected_signal.sensor).unwrap();
        let key = (sensor.id, body.id);
        let actual_signal = physics.state.signals.get(&key);
        if let Some(actual_signal) = actual_signal {
            let x_data = vec![sensor.position.x, body.position.x];
            let y_data = vec![sensor.position.y, body.position.y];
            let trace = Scatter::new(x_data, y_data)
                .name("expected signal")
                .mode(Mode::LinesMarkers)
                .text(&format!(
                    "actual signal distance:{}, strength:{}",
                    &actual_signal.distance, &actual_signal.strength
                ))
                .hover_info(HoverInfo::Text)
                .line(Line::new().color("#ade8f4").dash(DashType::Solid))
                .opacity(1.0)
                .show_legend(false);
            self.plot.add_trace(trace);
        }

        let expected_body_position = sensor.position
            + (body.position - sensor.position).normalized() * expected_signal.distance;

        let x_data = vec![sensor.position.x, expected_body_position.x];
        let y_data = vec![sensor.position.y, expected_body_position.y];
        let trace = Scatter::new(x_data, y_data)
            .name("expected signal")
            .mode(Mode::LinesMarkers)
            .text(&format!(
                "expected signal distance:{}, strength:{}",
                &expected_signal.distance, &expected_signal.strength
            ))
            .hover_info(HoverInfo::Text)
            .line(Line::new().color("#ade8f4").dash(DashType::Dot))
            .opacity(0.8)
            .show_legend(false);
        self.plot.add_trace(trace);

        self
    }

    pub fn render_space(mut self, space: SpaceId) -> Self {
        let scenario = &self.scenario;
        let physics = &self.scenario.physics;

        let axis_range = format!("{}", 100);

        let x_axis = Axis::new()
            .range(vec!["0", &axis_range])
            .auto_margin(true)
            .zero_line(false);

        let y_axis = Axis::new()
            .range(vec!["0", &axis_range])
            .auto_margin(true)
            .zero_line(false)
            .overlaying("x");

        let margin = Margin::new().left(40).right(40).bottom(40).top(40).pad(10);

        let space = physics.get_space(space).unwrap();
        let bodies: Vec<&Body> = physics
            .state
            .bodies
            .iter()
            .filter(|body| body.space == space.id)
            .collect();
        let barriers: Vec<&Barrier> = physics
            .state
            .barriers
            .iter()
            .filter(|barrier| barrier.space == space.id)
            .collect();
        let sensors: Vec<&Sensor> = physics
            .state
            .sensors
            .iter()
            .filter(|sensor| sensor.space == space.id)
            .collect();
        let portals: Vec<&Portal> = physics
            .state
            .portals
            .iter()
            .filter(|portal| portal.space == space.id)
            .collect();

        let title = format!(
            "{} bodies, {} barriers, {} sensors, {} portals",
            bodies.len(),
            barriers.len(),
            sensors.len(),
            portals.len()
        );

        self.layout = Layout::new()
            .margin(margin)
            .x_axis(x_axis)
            .y_axis(y_axis)
            .width(600)
            .height(600)
            .title(Title::new(&title))
            .auto_size(false);

        for portal in portals {
            let x_data = vec![portal.segment.begin.x, portal.segment.end.x];
            let y_data = vec![portal.segment.begin.y, portal.segment.end.y];
            let color = if portal.target.is_some() {
                "#e63946"
            } else {
                "#dcdcdc"
            };
            let text = match portal.target {
                Some(space) => format!("portal to {:?}", space),
                None => "inactive portal".into(),
            };
            let trace = Scatter::new(x_data, y_data)
                .name("portal")
                .mode(Mode::LinesMarkers)
                .text(&text)
                .hover_info(HoverInfo::Text)
                .line(Line::new().color(color).dash(DashType::Solid))
                .opacity(0.8)
                .show_legend(false);
            self.plot.add_trace(trace);
        }

        for sensor in sensors {
            self.layout.add_shape(
                Shape::new()
                    .shape_type(ShapeType::Circle)
                    .x_ref("x")
                    .y_ref("y")
                    .x0((sensor.position.x - sensor.radius).to_string())
                    .y0((sensor.position.y - sensor.radius).to_string())
                    .x1((sensor.position.x + sensor.radius).to_string())
                    .y1((sensor.position.y + sensor.radius).to_string())
                    .fill_color("caf0f8")
                    .layer(ShapeLayer::Below)
                    .opacity(0.5)
                    .line(ShapeLine::new().width(1.0).color("#ade8f4")),
            );
        }

        for barrier in barriers {
            let bounds = barrier.bounds();

            let x_data = vec![
                bounds.left_bottom().x,
                bounds.left_top().x,
                bounds.right_top().x,
                bounds.right_bottom().x,
                bounds.left_bottom().x,
            ];
            let y_data = vec![
                bounds.left_bottom().y,
                bounds.left_top().y,
                bounds.right_top().y,
                bounds.right_bottom().y,
                bounds.left_bottom().y,
            ];
            let barrier_name = scenario.named_barriers.name(barrier.id).unwrap();
            let barrier_trace = Scatter::new(x_data, y_data)
                .name(&format!("{}", barrier_name))
                .mode(Mode::Lines)
                .fill(plotly::common::Fill::ToSelf)
                .line(Line::new().color("#808080").width(1.0))
                .text(&"name")
                .fill_color("#2c2c2c")
                .hover_info(HoverInfo::Name)
                .show_legend(false);

            self.plot.add_trace(barrier_trace);

            // visualize rotation
            let scale = 0.1;
            let (w, h) = if is_rotated_horizontally(barrier.rotation) {
                (4.0 * scale, 1.0 * scale)
            } else {
                (1.0 * scale, 4.0 * scale)
            };
            let x = barrier.position.x as f64 - 0.5 * scale;
            let y = barrier.position.y as f64 - 0.5 * scale;
            self.layout.add_shape(
                Shape::new()
                    .shape_type(ShapeType::Rect)
                    .x0(x)
                    .y0(y)
                    .x1(x + w)
                    .y1(y + h)
                    .layer(ShapeLayer::Above)
                    .line(ShapeLine::new().width(0.0))
                    .fill_color("#808080"),
            );
        }

        for body in bodies {
            let x_data = vec![body.position.x];
            let y_data = vec![body.position.y];

            let body_name = scenario.named_bodies.name(body.id).unwrap();
            let body_trace = Scatter::new(x_data, y_data)
                .name(&format!("{}", body_name))
                .mode(Mode::Markers)
                .text("body")
                .line(Line::new().color("#006d77"))
                .marker(Marker::new().size_array(vec![10]))
                .opacity(1.0)
                .hover_info(HoverInfo::Name)
                .show_legend(false);

            self.plot.add_trace(body_trace);
        }

        self
    }
}

pub fn real_seconds() -> Duration {
    Duration::from_secs_f32(1.0)
}
