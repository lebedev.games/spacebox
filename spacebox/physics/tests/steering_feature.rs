mod testing;
use std::time::Duration;

use physics::Physics::BodyPositionUpdated;
pub use testing::*;

#[test]
fn test_single_body_follow_path_no_completion() {
    PhysicsDomainTestScenario::new()
        // Arrange
        .given_space("space")
        .given_body_type("body", 3.0)
        .given_body("space", "body", "body", (20.0, 50.0))
        // Act
        .when_body_follow_path("body", vec![(26.0, 50.0), (26.0, 60.0)])
        .when_update_physics_for(4, real_seconds())
        // Assert
        .then_events_should_be(|it| {
            vec![
                BodyPositionUpdated {
                    body: it.body("body"),
                    space: it.space("space"),
                    position: (23.0, 50.0).into(),
                    velocity: (3.0, 0.0).into(),
                },
                BodyPositionUpdated {
                    body: it.body("body"),
                    space: it.space("space"),
                    position: (26.0, 50.0).into(),
                    velocity: (3.0, 0.0).into(),
                },
                BodyPositionUpdated {
                    body: it.body("body"),
                    space: it.space("space"),
                    position: (26.0, 53.0).into(),
                    velocity: (0.0, 3.0).into(),
                },
                BodyPositionUpdated {
                    body: it.body("body"),
                    space: it.space("space"),
                    position: (26.0, 56.0).into(),
                    velocity: (0.0, 3.0).into(),
                },
            ]
        })
        .then_body_position_should_be("body", (26.0, 56.0));
}

#[test]
fn test_body_move_towards_with_fractional_values_in_long_time() {
    PhysicsDomainTestScenario::new()
        // Arrange
        .given_space("space")
        .given_body_type("body", 5.0)
        .given_body("space", "body", "body", (9.5, 3.5))
        // Act
        .when_body_move_towards("body", (11.5, 1.5))
        .when_update_physics_for(10, Duration::from_secs_f32(0.5))
        // Assert
        .then_events_should_be(|it| {
            vec![
                BodyPositionUpdated {
                    body: it.body("body"),
                    space: it.space("space"),
                    position: (11.267767, 1.732233).into(),
                    velocity: (1.767767, -1.767767).into(),
                },
                BodyPositionUpdated {
                    body: it.body("body"),
                    space: it.space("space"),
                    position: (11.5, 1.5).into(),
                    velocity: (0.23223305, -0.23223305).into(),
                },
                BodyPositionUpdated {
                    body: it.body("body"),
                    space: it.space("space"),
                    position: (11.5, 1.5).into(),
                    velocity: (0.0, 0.0).into(),
                },
            ]
        });
}

#[test]
fn test_single_body_move_towards() {
    PhysicsDomainTestScenario::new()
        // Arrange
        .given_space("space")
        .given_body_type("body", 3.0)
        .given_body("space", "body", "body", (20.0, 50.0))
        // Act
        .when_body_move_towards("body", (30.0, 50.0))
        .when_update_physics_for(4, real_seconds())
        // Assert
        .then_body_position_should_be("body", (30.0, 50.0))
        .then_events_should_be(|it| {
            vec![
                BodyPositionUpdated {
                    body: it.body("body"),
                    space: it.space("space"),
                    position: (23.0, 50.0).into(),
                    velocity: (3.0, 0.0).into(),
                },
                BodyPositionUpdated {
                    body: it.body("body"),
                    space: it.space("space"),
                    position: (26.0, 50.0).into(),
                    velocity: (3.0, 0.0).into(),
                },
                BodyPositionUpdated {
                    body: it.body("body"),
                    space: it.space("space"),
                    position: (29.0, 50.0).into(),
                    velocity: (3.0, 0.0).into(),
                },
                BodyPositionUpdated {
                    body: it.body("body"),
                    space: it.space("space"),
                    position: (30.0, 50.0).into(),
                    velocity: (1.0, 0.0).into(),
                },
            ]
        });
}
