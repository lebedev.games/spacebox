use physics::Physics::{BodyPositionUpdated, BodySpaceUpdated, SpaceConnected};
pub use testing::*;

mod testing;

#[test]
fn test_vertical_portal_enter_with_offset() {
    PhysicsDomainTestScenario::new()
        // Arrange
        .given_space("colony")
        .given_space("deck")
        .given_body_type("body", 3.0)
        .given_body("colony", "body", "body", (30.0, 75.0))
        .given_portal_type("portal", (0.0, 10.0))
        .given_portal("deck", "portal", (25.0, 45.0))
        .given_spaces_connection("deck", "colony", (0.0, 25.0))
        // Act
        .when_body_move_towards("body", (20.0, 75.0))
        .when_update_physics_for(3, real_seconds())
        // Assert
        .then_body_position_should_be("body", (21.0, 50.0))
        .then_events_should_be(|it| {
            vec![
                BodyPositionUpdated {
                    body: it.body("body"),
                    space: it.space("colony"),
                    position: (27.0, 75.0).into(),
                    velocity: (-3.0, 0.0).into(),
                },
                BodySpaceUpdated {
                    body: it.body("body"),
                    space: it.space("deck"),
                    position: (24.0, 50.0).into(),
                    velocity: (-3.0, 0.0).into(),
                    offset: (0.0, -25.0).into(),
                },
                BodyPositionUpdated {
                    body: it.body("body"),
                    space: it.space("deck"),
                    position: (21.0, 50.0).into(),
                    velocity: (-3.0, 0.0).into(),
                },
            ]
        });
}

#[test]
fn test_vertical_portal_enter_no_offset() {
    PhysicsDomainTestScenario::new()
        // Arrange
        .given_space("colony")
        .given_space("deck")
        .given_body_type("body", 3.0)
        .given_body("colony", "body", "body", (30.0, 50.0))
        .given_portal_type("portal", (0.0, 10.0))
        .given_portal("deck", "portal", (25.0, 45.0))
        .given_spaces_connection("deck", "colony", (0.0, 0.0))
        // Act
        .when_body_move_towards("body", (20.0, 50.0))
        .when_update_physics_for(3, real_seconds())
        // Assert
        .then_body_position_should_be("body", (21.0, 50.0))
        .then_events_should_be(|it| {
            vec![
                BodyPositionUpdated {
                    body: it.body("body"),
                    space: it.space("colony"),
                    position: (27.0, 50.0).into(),
                    velocity: (-3.0, 0.0).into(),
                },
                BodySpaceUpdated {
                    body: it.body("body"),
                    space: it.space("deck"),
                    position: (24.0, 50.0).into(),
                    velocity: (-3.0, 0.0).into(),
                    offset: (0.0, 0.0).into(),
                },
                BodyPositionUpdated {
                    body: it.body("body"),
                    space: it.space("deck"),
                    position: (21.0, 50.0).into(),
                    velocity: (-3.0, 0.0).into(),
                },
            ]
        });
}

#[test]
fn test_vertical_portal_exit_with_offset() {
    PhysicsDomainTestScenario::new()
        // Arrange
        .given_space("colony")
        .given_space("deck")
        .given_body_type("body", 3.0)
        .given_body("deck", "body", "body", (20.0, 50.0))
        .given_portal_type("portal", (0.0, 10.0))
        .given_portal("deck", "portal", (25.0, 45.0))
        .given_spaces_connection("deck", "colony", (7.0, 13.0))
        // Act
        .when_body_move_towards("body", (30.0, 50.0))
        .when_update_physics_for(3, real_seconds())
        // Assert
        .then_body_position_should_be("body", (36.0, 63.0))
        .then_events_should_be(|it| {
            vec![
                BodyPositionUpdated {
                    body: it.body("body"),
                    space: it.space("deck"),
                    position: (23.0, 50.0).into(),
                    velocity: (3.0, 0.0).into(),
                },
                BodySpaceUpdated {
                    body: it.body("body"),
                    space: it.space("colony"),
                    position: (33.0, 63.0).into(),
                    velocity: (3.0, 0.0).into(),
                    offset: (7.0, 13.0).into(),
                },
                BodyPositionUpdated {
                    body: it.body("body"),
                    space: it.space("colony"),
                    position: (36.0, 63.0).into(),
                    velocity: (3.0, 0.0).into(),
                },
            ]
        });
}

#[test]
fn test_vertical_portal_exit_no_offset() {
    PhysicsDomainTestScenario::new()
        // Arrange
        .given_space("colony")
        .given_space("deck")
        .given_body_type("body", 3.0)
        .given_body("deck", "body", "body", (20.0, 50.0))
        .given_portal_type("portal", (0.0, 10.0))
        .given_portal("deck", "portal", (25.0, 45.0))
        .given_spaces_connection("deck", "colony", (0.0, 0.0))
        // Act
        .when_body_move_towards("body", (30.0, 50.0))
        .when_update_physics_for(3, real_seconds())
        // Assert
        .then_body_position_should_be("body", (29.0, 50.0))
        .then_events_should_be(|it| {
            vec![
                BodyPositionUpdated {
                    body: it.body("body"),
                    space: it.space("deck"),
                    position: (23.0, 50.0).into(),
                    velocity: (3.0, 0.0).into(),
                },
                BodySpaceUpdated {
                    body: it.body("body"),
                    space: it.space("colony"),
                    position: (26.0, 50.0).into(),
                    velocity: (3.0, 0.0).into(),
                    offset: (0.0, 0.0).into(),
                },
                BodyPositionUpdated {
                    body: it.body("body"),
                    space: it.space("colony"),
                    position: (29.0, 50.0).into(),
                    velocity: (3.0, 0.0).into(),
                },
            ]
        });
}

#[test]
fn test_space_connection_in_motion() {
    PhysicsDomainTestScenario::new()
        // Arrange
        .given_space("colony")
        .given_space("deck")
        .given_body_type("body", 3.0)
        .given_body("deck", "body", "body", (20.0, 50.0))
        .given_portal_type("portal", (0.0, 10.0))
        .given_portal("deck", "portal", (25.0, 45.0))
        // Act
        .when_body_move_towards("body", (30.0, 50.0))
        .when_update_physics_for(1, real_seconds())
        .when_connect_spaces("deck", "colony", (7.0, 13.0))
        .when_update_physics_for(2, real_seconds())
        // Assert
        .then_body_position_should_be("body", (36.0, 63.0))
        .then_events_should_be(|it| {
            vec![
                BodyPositionUpdated {
                    body: it.body("body"),
                    space: it.space("deck"),
                    position: (23.0, 50.0).into(),
                    velocity: (3.0, 0.0).into(),
                },
                SpaceConnected {
                    space: it.space("deck"),
                    parent: it.space("colony"),
                    connection_point: (7.0, 13.0).into(),
                },
                BodySpaceUpdated {
                    body: it.body("body"),
                    space: it.space("colony"),
                    position: (33.0, 63.0).into(),
                    velocity: (3.0, 0.0).into(),
                    offset: (7.0, 13.0).into(),
                },
                BodyPositionUpdated {
                    body: it.body("body"),
                    space: it.space("colony"),
                    position: (36.0, 63.0).into(),
                    velocity: (3.0, 0.0).into(),
                },
            ]
        });
}

#[test]
fn test_space_connection_with_offset() {
    PhysicsDomainTestScenario::new()
        // Arrange
        .given_space("colony")
        .given_space("deck")
        .given_body_type("body", 3.0)
        .given_body("deck", "body", "body", (20.0, 50.0))
        .given_portal_type("portal", (0.0, 10.0))
        .given_portal("deck", "portal", (25.0, 45.0))
        // Act
        .when_connect_spaces("deck", "colony", (10.0, 10.0))
        // Assert
        .then_body_position_should_be("body", (20.0, 50.0))
        .then_events_should_be(|it| {
            vec![SpaceConnected {
                space: it.space("deck"),
                parent: it.space("colony"),
                connection_point: (10.0, 10.0).into(),
            }]
        });
}

#[test]
fn test_move_through_inactive_portal() {
    PhysicsDomainTestScenario::new()
        // Arrange
        .given_space("colony")
        .given_space("deck")
        .given_body_type("body", 3.0)
        .given_body("deck", "body", "body", (20.0, 50.0))
        .given_portal_type("portal", (0.0, 10.0))
        .given_portal("deck", "portal", (25.0, 45.0))
        // Act
        .when_body_move_towards("body", (30.0, 50.0))
        .when_update_physics_for(3, real_seconds())
        // Assert
        .then_body_position_should_be("body", (29.0, 50.0))
        .then_events_should_be(|it| {
            vec![
                BodyPositionUpdated {
                    body: it.body("body"),
                    space: it.space("deck"),
                    position: (23.0, 50.0).into(),
                    velocity: (3.0, 0.0).into(),
                },
                BodyPositionUpdated {
                    body: it.body("body"),
                    space: it.space("deck"),
                    position: (26.0, 50.0).into(),
                    velocity: (3.0, 0.0).into(),
                },
                BodyPositionUpdated {
                    body: it.body("body"),
                    space: it.space("deck"),
                    position: (29.0, 50.0).into(),
                    velocity: (3.0, 0.0).into(),
                },
            ]
        });
}
