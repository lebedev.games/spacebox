mod testing;

pub use testing::*;

fn test_try_move_between_barriers() {
    // TODO (SPACE-35):
    PhysicsDomainTestScenario::new()
        // Arrange
        .given_space("space")
        .given_barrier_type("barrier", (4.0, 2.0))
        .given_barrier("space", "left wall", "barrier", (8.0, 13.0))
        .given_barrier("space", "right wall", "barrier", (12.0, 13.0))
        .given_body_type("body", 10.0)
        .given_body("space", "body", "body", (10.0, 10.0))
        // Act
        .when_body_move_towards("body", (10.0, 20.0))
        .when_update_physics_for(1, real_seconds())
        // Assert
        .then_body_position_should_be("body", (10.0, 12.0));
}

#[test]
fn test_move_into_zero_bounds_barrier() {
    PhysicsDomainTestScenario::new()
        // Arrange
        .given_space("space")
        .given_barrier_type("barrier", (0.0, 0.0))
        .given_barrier("space", "zero", "barrier", (26.0, 50.0))
        .given_body_type("body", 2.5)
        .given_body("space", "body", "body", (9.5, 3.5))
        // Act
        .when_body_move_towards("body", (11.5, 1.5))
        .when_update_physics_for(5, real_seconds())
        // Assert
        .then_body_position_should_be("body", (11.5, 1.5).into());
}
