use crate::{Body, Vector};

pub fn arrive(body: &Body, destination: Vector, arrive_distance: f32) -> Vector {
    let distance = (destination - body.position).magnitude();
    let mut speed = body.speed;
    if distance < arrive_distance {
        if distance < 0.05 {
            speed = 0.0;
        } else {
            speed *= distance / arrive_distance;
        }
    }

    (destination - body.position).normalized() * speed
}

pub fn move_towards(body: &Body, destination: Vector, time: f32) -> Vector {
    let delta = destination - body.position;
    let distance = delta.magnitude();

    let velocity = if distance < body.speed * time {
        // move body instantly to reach destination point exactly
        delta
    } else {
        delta.normalized() * body.speed * time
    };

    velocity
}

pub fn move_instantly(body: &Body, destination: Vector) -> Vector {
    destination - body.position
}
