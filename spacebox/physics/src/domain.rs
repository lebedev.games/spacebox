use std::rc::Rc;

use crate::{PhysicsDatabase, PhysicsProperties, PhysicsState};

#[derive(Default)]
pub struct PhysicsDomain {
    pub properties: PhysicsProperties,
    pub database: Rc<PhysicsDatabase>,
    pub state: PhysicsState,
}
