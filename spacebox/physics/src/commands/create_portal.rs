use crate::{PhysicsDomain, PhysicsError, Portal, PortalId, PortalKey, Segment, SpaceId, Vector};

impl PhysicsDomain {
    pub fn prepare_portal(
        &self,
        space: SpaceId,
        _id: PortalId,
        key: &PortalKey,
        position: Vector,
    ) -> Result<Portal, PhysicsError> {
        let portal = self.get_portal_type(key)?;

        let portal = Portal {
            target: None,
            segment: Segment {
                begin: position.clone(),
                end: position.clone() + portal.offset,
            },
            space,
            offset: Vector::ZERO,
        };

        Ok(portal)
    }

    pub fn create_portal(&mut self, portal: Portal) {
        self.state.portals.push(portal);
    }
}
