use std::collections::{HashMap, HashSet};

use crate::collision::{
    apply_with_collision_response, get_collision_strength, intersect_segments2,
};
use crate::steering::{arrive, move_instantly, move_towards};
use crate::Physics::{
    BodyMotionSignalLost, BodyMotionSignalReceived, BodyMotionSignalUpdated, BodyPositionUpdated,
    BodySpaceUpdated,
};
use crate::{
    BodyId, Physics, PhysicsDomain, Portal, Segment, Sensor, SensorController, Signal,
    SteeringBehaviour, Vector,
};

impl PhysicsDomain {
    pub fn update(&mut self, elapsed_seconds: f32) -> Vec<Physics> {
        let mut events = vec![];
        let elapsed_time = self.properties.time_simulation_scale * elapsed_seconds;

        let mut translated_bodies: HashSet<BodyId> = HashSet::new();

        for space in self.state.spaces.iter() {
            let filter = translated_bodies.clone();
            let bodies = self
                .state
                .bodies
                .iter_mut()
                .filter(|body| body.space == space.id && !filter.contains(&body.id));

            let barriers = self
                .state
                .barriers
                .iter()
                .filter(|barrier| barrier.space == space.id)
                .collect();

            let sensors: Vec<&Sensor> = self
                .state
                .sensors
                .iter()
                .filter(|sensor| sensor.space == space.id)
                .collect();

            let portals: Vec<&Portal> = self
                .state
                .portals
                .iter()
                .filter(|portal| portal.space == space.id)
                .collect();

            let mut body_position_map = HashMap::new();
            for body in bodies {
                let previous_velocity = body.velocity;
                let previous_position = body.position;

                let velocity = match body.steering.as_ref() {
                    Some(behaviour) => match behaviour {
                        SteeringBehaviour::Arrive { destination } => {
                            arrive(body, destination.clone(), body.speed)
                        }
                        SteeringBehaviour::MoveTowards { destination } => {
                            move_towards(body, destination.clone(), elapsed_time)
                        }
                        SteeringBehaviour::MoveInstantly { destination } => {
                            move_instantly(body, destination.clone())
                        }
                        SteeringBehaviour::FollowPath { path } => {
                            move_towards(body, path[0], elapsed_time)
                        }
                    },
                    None => Vector::ZERO,
                };

                let position = apply_with_collision_response(body.position, velocity, &barriers);

                let motion = Segment {
                    begin: body.position,
                    end: position,
                };
                body.position = position;
                body.velocity = velocity;

                if let Some(SteeringBehaviour::FollowPath { path }) = body.steering.as_ref() {
                    if body.position == path[0] {
                        if path.len() > 1 {
                            let reduce_path = path.clone().into_iter().skip(1).collect();
                            body.steering =
                                Some(SteeringBehaviour::FollowPath { path: reduce_path });
                        }
                    }
                }

                if !(previous_velocity == velocity && previous_position == position) {
                    let mut motion_event = BodyPositionUpdated {
                        body: body.id,
                        space: body.space,
                        position: body.position,
                        velocity: body.velocity,
                    };

                    for portal in portals.iter() {
                        if let Some(parent) = portal.target {
                            if intersect_segments2(motion, portal.segment) {
                                body.space = parent;
                                body.position = body.position + portal.offset;
                                body.offset_steering(portal.offset);
                                motion_event = BodySpaceUpdated {
                                    body: body.id,
                                    space: body.space,
                                    position: body.position,
                                    velocity: body.velocity,
                                    offset: portal.offset,
                                };
                                translated_bodies.insert(body.id);
                                break;
                            }
                        }
                    }

                    events.push(motion_event);
                }

                body_position_map.insert(body.id, body.position);

                for sensor in sensors.iter() {
                    let distance = sensor.position.distance(body.position);
                    let detected = distance < sensor.radius;

                    let signal_key = (sensor.id, body.id);
                    match self.state.signals.get_mut(&signal_key) {
                        Some(signal) => {
                            if detected {
                                signal.distance = distance;
                                signal.strength = get_collision_strength(
                                    body.position,
                                    sensor.position,
                                    &barriers,
                                );
                                events.push(BodyMotionSignalUpdated {
                                    body: body.id,
                                    strength: signal.strength,
                                    distance: signal.distance,
                                    sensor: sensor.id,
                                });
                            } else {
                                self.state.signals.remove(&signal_key);
                                events.push(BodyMotionSignalLost {
                                    body: body.id,
                                    origin: body.origin.clone(),
                                    sensor: sensor.id,
                                })
                            }
                        }
                        None => {
                            let signal = Signal {
                                sensor: sensor.id,
                                distance,
                                strength: get_collision_strength(
                                    body.position,
                                    sensor.position,
                                    &barriers,
                                ),
                            };
                            events.push(BodyMotionSignalReceived {
                                body: body.id,
                                origin: body.origin.clone(),
                                strength: signal.strength,
                                distance: signal.distance,
                                sensor: sensor.id,
                            });
                            self.state.signals.insert(signal_key, signal);
                        }
                    }
                }
            }

            let dynamic_sensors: Vec<&mut Sensor> = self
                .state
                .sensors
                .iter_mut()
                .filter(|sensor| sensor.space == space.id)
                .collect();

            let mut to_delete = Vec::new();

            for sensor in dynamic_sensors {
                match sensor.controller {
                    SensorController::Dynamic { body } => {
                        if let Some(position) = body_position_map.get(&body) {
                            sensor.position = position.clone();
                        } else {
                            error!("Unable to update sensor {:?}, body {:?} not found, delete detached sensor", sensor.id, body);
                            to_delete.push(sensor.id);
                        }
                    }
                    _ => {}
                }
            }

            self.state
                .sensors
                .retain(|sensor| !to_delete.contains(&sensor.id))
        }

        events
    }
}
