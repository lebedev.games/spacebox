use crate::Physics::{BarrierSpaceUpdated, BodySpaceUpdated, SpaceDisconnected};
use crate::{Physics, PhysicsDomain, PhysicsError, SpaceId, Vector};

#[derive(Debug)]
pub struct ExtractSpaceOperation {
    pub source_space_index: usize,
    pub target_space_index: usize,
    pub offset: Vector,
    pub bodies: Vec<usize>,
    pub barriers: Vec<usize>,
}

impl PhysicsDomain {
    pub fn prepare_extract_space_operation(
        &self,
        source: SpaceId,
        bounds_min: Vector,
        bounds_max: Vector,
        target: SpaceId,
    ) -> Result<ExtractSpaceOperation, PhysicsError> {
        let mut bodies = vec![];
        for (index, body) in self.state.bodies.iter().enumerate() {
            if body.space != source || !body.position.in_bounds(bounds_min, bounds_max) {
                continue;
            }
            bodies.push(index);
        }
        let mut barriers = vec![];
        for (index, barrier) in self.state.barriers.iter().enumerate() {
            if barrier.space != source || !barrier.position.in_bounds(bounds_min, bounds_max) {
                continue;
            }
            barriers.push(index);
        }
        let target_space_index = self.index_space(target)?;
        let source_space_index = self.index_space(source)?;
        let target_space = &self.state.spaces[target_space_index];
        let offset = -target_space.ensure_parent_relation(source)?.point;

        Ok(ExtractSpaceOperation {
            source_space_index,
            target_space_index,
            offset,
            barriers,
            bodies,
        })
    }

    pub fn extract_space(&mut self, operation: ExtractSpaceOperation) -> Vec<Physics> {
        let mut events = vec![];

        let offset = operation.offset;
        let target = &mut self.state.spaces[operation.target_space_index];
        target.parent = None;
        let target = target.id;
        events.push(SpaceDisconnected { space: target });

        for body in operation.bodies {
            let body = &mut self.state.bodies[body];
            body.space = target;
            body.position += offset;
            body.offset_steering(offset);
            events.push(BodySpaceUpdated {
                body: body.id,
                space: body.space,
                position: body.position,
                velocity: body.velocity,
                offset,
            })
        }

        for barrier in operation.barriers {
            let barrier = &mut self.state.barriers[barrier];
            barrier.space = target;
            barrier.position += offset;
            events.push(BarrierSpaceUpdated {
                barrier: barrier.id,
                space: barrier.space,
                position: barrier.position,
                offset,
            });
        }

        events
    }
}
