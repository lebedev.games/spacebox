use crate::{
    BodyId, PhysicsDomain, PhysicsError, Sensor, SensorController, SensorId, SensorKey, SpaceId,
    Vector,
};

impl PhysicsDomain {
    pub fn prepare_sensor(
        &self,
        space: SpaceId,
        id: SensorId,
        key: &SensorKey,
        position: Vector,
    ) -> Result<Sensor, PhysicsError> {
        let sensor = self.get_sensor_type(key)?;

        Ok(Sensor {
            id: id.clone(),
            key: key.clone(),
            space,
            position: position.clone(),
            radius: sensor.radius,
            controller: SensorController::Static,
        })
    }

    pub fn prepare_dynamic_sensor(
        &self,
        space: SpaceId,
        id: SensorId,
        key: &SensorKey,
        body: BodyId,
    ) -> Result<Sensor, PhysicsError> {
        let body = self.get_body(body)?;
        let sensor = self.get_sensor_type(key)?;

        Ok(Sensor {
            id: id.clone(),
            key: key.clone(),
            space,
            position: body.position,
            radius: sensor.radius,
            controller: SensorController::Dynamic { body: body.id },
        })
    }

    pub fn create_sensor(&mut self, sensor: Sensor) {
        self.state.sensors.push(sensor);
    }
}
