use crate::Physics::{BarrierSpaceUpdated, BodySpaceUpdated, SpaceConnected};
use crate::{Physics, PhysicsDomain, PhysicsError, SpaceId, SpaceRelation, Vector};

pub struct TranslateSpaceOperation {
    pub source_space_index: usize,
    pub target_space_index: usize,
    pub offset: Vector,
    pub bodies: Vec<usize>,
    pub barriers: Vec<usize>,
}

impl PhysicsDomain {
    pub fn prepare_translate_space_operation(
        &self,
        source: SpaceId,
        target: SpaceId,
        offset: Vector,
    ) -> Result<TranslateSpaceOperation, PhysicsError> {
        let mut bodies = vec![];
        for (index, body) in self.state.bodies.iter().enumerate() {
            if body.space != source {
                continue;
            }
            bodies.push(index);
        }
        let mut barriers = vec![];
        for (index, barrier) in self.state.barriers.iter().enumerate() {
            if barrier.space != source {
                continue;
            }
            barriers.push(index);
        }
        let target_space_index = self.index_space(target)?;
        let source_space_index = self.index_space(source)?;

        Ok(TranslateSpaceOperation {
            source_space_index,
            target_space_index,
            offset,
            barriers,
            bodies,
        })
    }

    pub fn translate_space(&mut self, operation: TranslateSpaceOperation) -> Vec<Physics> {
        let mut events = vec![];

        let offset = operation.offset;
        let target = &mut self.state.spaces[operation.target_space_index];
        let space = target.id;
        let source = &mut self.state.spaces[operation.source_space_index];
        source.parent = Some(SpaceRelation {
            space,
            point: offset,
        });
        events.push(SpaceConnected {
            space: source.id,
            parent: space,
            connection_point: offset,
        });

        for body in operation.bodies {
            let body = &mut self.state.bodies[body];
            body.space = space;
            body.position += offset;
            body.offset_steering(offset);
            events.push(BodySpaceUpdated {
                body: body.id,
                space: body.space,
                position: body.position,
                velocity: body.velocity,
                offset,
            })
        }

        for barrier in operation.barriers {
            let barrier = &mut self.state.barriers[barrier];
            barrier.space = space;
            barrier.position += offset;
            events.push(BarrierSpaceUpdated {
                barrier: barrier.id,
                space: barrier.space,
                position: barrier.position,
                offset,
            });
        }

        events
    }
}
