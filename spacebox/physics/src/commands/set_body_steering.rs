use crate::{BodyId, Physics, PhysicsDomain, PhysicsError, SteeringBehaviour};

impl PhysicsDomain {
    pub fn set_body_steering_behaviour(
        &mut self,
        id: BodyId,
        steering: SteeringBehaviour,
    ) -> Result<Vec<Physics>, PhysicsError> {
        let body = self.index_body(id)?;
        let body = &mut self.state.bodies[body];
        body.steering = Some(steering);
        Ok(vec![])
    }
}
