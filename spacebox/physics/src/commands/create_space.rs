use crate::{Physics, PhysicsDomain, PhysicsError, Space, SpaceId, SpaceKey, SpaceOrigin};

impl PhysicsDomain {
    pub fn prepare_space(
        &mut self,
        id: SpaceId,
        key: &SpaceKey,
        origin: SpaceOrigin,
    ) -> Result<Space, PhysicsError> {
        let space = Space {
            id,
            key: key.clone(),
            origin,
            parent: None,
        };
        Ok(space)
    }

    pub fn create_space(&mut self, space: Space) -> Vec<Physics> {
        self.state.spaces.push(space);
        vec![]
    }
}
