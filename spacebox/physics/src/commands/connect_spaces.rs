use crate::{
    Physics, PhysicsDomain, PhysicsError, Portal, Segment, SpaceId, SpaceRelation, Vector,
};

pub struct ConnectSpacesOperation {
    pub space_index: usize,
    pub space: SpaceId,
    pub parent: SpaceId,
    pub portals_to_delete: Vec<SpaceId>,
    pub portals_to_create: Vec<Portal>,
    pub connection_point: Vector,
}

impl PhysicsDomain {
    pub fn prepare_connect_spaces_operation(
        &self,
        space: SpaceId,
        parent: SpaceId,
        connection_point: Vector,
    ) -> Result<ConnectSpacesOperation, PhysicsError> {
        let space_index = self.index_space(space)?;
        let mut portals_to_delete = vec![];
        let mut portals_to_create = vec![];

        let portals = self.get_space_portals(space);

        for portal in portals {
            if let Some(target) = portal.target {
                portals_to_delete.push(target);
            }

            portals_to_create.push(Portal {
                space: parent,
                target: Some(space),
                segment: Segment {
                    begin: portal.segment.begin + connection_point,
                    end: portal.segment.end + connection_point,
                },
                offset: -connection_point,
            })
        }

        Ok(ConnectSpacesOperation {
            space_index,
            space,
            parent,
            portals_to_delete,
            portals_to_create,
            connection_point,
        })
    }

    pub fn connect_spaces(&mut self, operation: ConnectSpacesOperation) -> Vec<Physics> {
        let mut events = Vec::new();
        self.state
            .portals
            .retain(|portal| !operation.portals_to_delete.contains(&portal.space));
        for portal in self.state.portals.iter_mut() {
            if portal.space == operation.space {
                portal.target = Some(operation.parent);
                portal.offset = operation.connection_point;
            }
        }
        self.state.portals.extend(operation.portals_to_create);
        self.state.spaces[operation.space_index].parent = Some(SpaceRelation {
            space: operation.parent,
            point: operation.connection_point,
        });
        events.push(Physics::SpaceConnected {
            space: operation.space,
            parent: operation.parent,
            connection_point: operation.connection_point,
        });
        events
    }
}
