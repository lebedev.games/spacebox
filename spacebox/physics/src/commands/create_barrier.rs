use std::collections::HashMap;

use crate::collision::intersects_barrier;
use crate::{
    Barrier, BarrierId, BarrierKey, BarrierOrigin, BodyId, Physics, PhysicsDomain, PhysicsError,
    SensorId, Signal, SpaceId, Vector,
};

pub struct CreateBarrierOperation {
    pub barrier: Barrier,
    pub signals_to_update: HashMap<(SensorId, BodyId), Signal>,
}

impl PhysicsDomain {
    pub fn prepare_barrier(
        &self,
        space: SpaceId,
        id: BarrierId,
        key: &BarrierKey,
        origin: BarrierOrigin,
        position: Vector,
        rotation: f32,
    ) -> Result<CreateBarrierOperation, PhysicsError> {
        let barrier = self.get_barrier_type(key)?;

        let barrier = Barrier {
            id: id.clone(),
            key: key.clone(),
            space,
            position,
            rotation,
            size: barrier.size,
            origin,
        };

        let mut signals_to_update = HashMap::new();

        let sensors = self.get_sensors_at(space, position.clone());

        for sensor in sensors {
            for ((sensor_id, body_id), signal) in self.state.signals.iter() {
                if sensor_id == &sensor.id && signal.strength > 0.0 {
                    let key = (*sensor_id, *body_id);
                    let body = self.get_body(*body_id)?;
                    let strength = if intersects_barrier(body.position, sensor.position, &barrier) {
                        0.0
                    } else {
                        signal.strength
                    };
                    signals_to_update.insert(
                        key,
                        Signal {
                            sensor: sensor_id.clone(),
                            distance: signal.distance,
                            strength,
                        },
                    );
                }
            }
        }

        Ok(CreateBarrierOperation {
            barrier,
            signals_to_update,
        })
    }

    pub fn create_barrier(&mut self, changeset: CreateBarrierOperation) -> Vec<Physics> {
        let mut events = Vec::new();
        for ((sensor, body), signal) in changeset.signals_to_update.iter() {
            events.push(Physics::BodyMotionSignalUpdated {
                body: body.clone(),
                strength: signal.strength,
                distance: signal.distance,
                sensor: sensor.clone(),
            });
        }
        self.state.signals.extend(changeset.signals_to_update);
        self.state.barriers.push(changeset.barrier);
        events
    }
}
