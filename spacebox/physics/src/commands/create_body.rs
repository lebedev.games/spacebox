use crate::{
    Body, BodyId, BodyKey, Physics, PhysicsDomain, PhysicsError, Sensor, Signal, SpaceId, Vector,
};

pub struct CreateBodyOperation {
    pub body: Body,
    pub signals: Vec<Signal>,
}

impl PhysicsDomain {
    pub fn prepare_body(
        &self,
        space: SpaceId,
        id: BodyId,
        key: &BodyKey,
        position: Vector,
    ) -> Result<CreateBodyOperation, PhysicsError> {
        let body = self.get_body_type(key)?;

        let body = Body {
            id: id.clone(),
            key: key.clone(),
            space: space,
            origin: body.origin.clone(),
            position: position.clone(),
            velocity: Vector::ZERO,
            speed: body.speed,
            steering: None,
        };

        let mut signals = Vec::new();

        let sensors: Vec<&Sensor> = self
            .state
            .sensors
            .iter()
            .filter(|sensor| sensor.space == space)
            .collect();

        for sensor in sensors.iter() {
            let distance = sensor.position.distance(body.position);
            let detected = distance < sensor.radius;
            if detected {
                signals.push(Signal {
                    sensor: sensor.id,
                    distance,
                    strength: 1.0,
                })
            }
        }

        Ok(CreateBodyOperation { body, signals })
    }

    pub fn create_body(&mut self, changeset: CreateBodyOperation) -> Vec<Physics> {
        let mut events = Vec::new();
        let body = changeset.body;
        for signal in changeset.signals {
            let key = (signal.sensor.clone(), body.id);
            events.push(Physics::BodyMotionSignalReceived {
                body: body.id,
                origin: body.origin.clone(),
                strength: signal.strength,
                distance: signal.distance,
                sensor: signal.sensor.clone(),
            });
            self.state.signals.insert(key, signal);
        }
        self.state.bodies.push(body);
        events
    }
}
