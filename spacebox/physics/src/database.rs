use serde::{Deserialize, Serialize};

use crate::{BarrierType, BodyType, PortalType, SensorType, SpaceType};

#[derive(Debug, Serialize, Deserialize, Default)]
pub struct PhysicsDatabase {
    pub spaces: Vec<SpaceType>,
    pub bodies: Vec<BodyType>,
    pub barriers: Vec<BarrierType>,
    pub sensors: Vec<SensorType>,
    pub portals: Vec<PortalType>,
}
