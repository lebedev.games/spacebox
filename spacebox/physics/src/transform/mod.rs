use crate::Vector;

/// only horizontal (90,270) and vertical (0,180) rotation angles supported
#[inline]
pub fn is_rotated_horizontally(angle: f32) -> bool {
    (angle - 90.0).abs() < 1.0 || (angle - 270.0).abs() < 1.0
}

/// rotates position around zero-point (coordinate system center)
pub fn rotate_relative_point(position: Vector, rotation: f32) -> Vector {
    let s = rotation.to_radians().sin();
    let c = rotation.to_radians().cos();

    let p = position;

    let x = p.x * c - p.y * s;
    let y = p.x * s + p.y * c;

    Vector::new(x, y)
}
