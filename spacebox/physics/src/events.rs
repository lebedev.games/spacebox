use serde::{Deserialize, Serialize};

use crate::{BarrierId, BodyId, BodyOrigin, SensorId, SpaceId, Vector};

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(tag = "$type")]
pub enum Physics {
    SpaceConnected {
        space: SpaceId,
        parent: SpaceId,
        connection_point: Vector,
    },
    SpaceDisconnected {
        space: SpaceId,
    },
    BodyMotionSignalReceived {
        body: BodyId,
        origin: BodyOrigin,
        strength: f32,
        distance: f32,
        sensor: SensorId,
    },
    BodyMotionSignalUpdated {
        body: BodyId,
        strength: f32,
        distance: f32,
        sensor: SensorId,
    },
    BodyMotionSignalLost {
        body: BodyId,
        origin: BodyOrigin,
        sensor: SensorId,
    },
    BodySpaceUpdated {
        body: BodyId,
        space: SpaceId,
        position: Vector,
        velocity: Vector,
        offset: Vector,
    },
    BodyPositionUpdated {
        body: BodyId,
        space: SpaceId,
        position: Vector,
        velocity: Vector,
    },
    BarrierSpaceUpdated {
        barrier: BarrierId,
        space: SpaceId,
        position: Vector,
        offset: Vector,
    },
}
