use std::collections::HashMap;

use serde::{Deserialize, Serialize};

use crate::{Barrier, Body, BodyId, Portal, Sensor, SensorId, Signal, Space};

#[derive(Debug, Serialize, Deserialize)]
pub struct PhysicsProperties {
    pub force_limit: f32,
    pub time_simulation_scale: f32,
}

impl Default for PhysicsProperties {
    fn default() -> Self {
        Self {
            force_limit: 0.1,
            time_simulation_scale: 1.0,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, Default)]
pub struct PhysicsState {
    pub spaces: Vec<Space>,
    pub bodies: Vec<Body>,
    pub barriers: Vec<Barrier>,
    pub sensors: Vec<Sensor>,
    pub portals: Vec<Portal>,
    pub signals: HashMap<(SensorId, BodyId), Signal>,
}
