use serde::{Deserialize, Serialize};

use crate::{BarrierId, BarrierKey, BodyId, BodyKey, PortalKey, SensorId, SensorKey, SpaceId};

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(tag = "$type")]
pub enum PhysicsError {
    BodyNotFound { id: BodyId },
    BarrierNotFound { id: BarrierId },
    SensorNotFound { id: SensorId },
    SpaceNotFound { id: SpaceId },
    BodyTypeNotFound { key: BodyKey },
    BarrierTypeNotFound { key: BarrierKey },
    PortalTypeNotFound { key: PortalKey },
    SensorTypeNotFound { key: SensorKey },
    SpaceHasNoParent { space: SpaceId },
    DistantObjectsInDifferentSpaces,
    DistantObjectsIsTooFar,
}
