use crate::{
    Barrier, BarrierId, BarrierKey, BarrierType, Body, BodyId, BodyKey, BodyType, PhysicsDomain,
    PhysicsError, PortalKey, PortalType, Sensor, SensorId, SensorKey, SensorType, Space, SpaceId,
};

impl PhysicsDomain {
    pub fn get_sensor_type(&self, key: &SensorKey) -> Result<&SensorType, PhysicsError> {
        self.database
            .sensors
            .iter()
            .find(|sensor| &sensor.key == key)
            .ok_or(PhysicsError::SensorTypeNotFound { key: key.clone() })
    }

    pub fn get_barrier_type(&self, key: &BarrierKey) -> Result<&BarrierType, PhysicsError> {
        self.database
            .barriers
            .iter()
            .find(|barrier| &barrier.key == key)
            .ok_or(PhysicsError::BarrierTypeNotFound { key: key.clone() })
    }

    pub fn get_portal_type(&self, key: &PortalKey) -> Result<&PortalType, PhysicsError> {
        self.database
            .portals
            .iter()
            .find(|portal| &portal.key == key)
            .ok_or(PhysicsError::PortalTypeNotFound { key: key.clone() })
    }

    pub fn get_body_type(&self, key: &BodyKey) -> Result<&BodyType, PhysicsError> {
        self.database
            .bodies
            .iter()
            .find(|body| &body.key == key)
            .ok_or(PhysicsError::BodyTypeNotFound { key: key.clone() })
    }

    pub fn get_space(&self, id: SpaceId) -> Result<&Space, PhysicsError> {
        self.state
            .spaces
            .iter()
            .find(|space| space.id == id)
            .ok_or(PhysicsError::SpaceNotFound { id })
    }

    pub fn index_space(&self, id: SpaceId) -> Result<usize, PhysicsError> {
        self.state
            .spaces
            .iter()
            .position(|space| space.id == id)
            .ok_or(PhysicsError::SpaceNotFound { id })
    }

    pub fn index_body(&self, id: BodyId) -> Result<usize, PhysicsError> {
        self.state
            .bodies
            .iter()
            .position(|body| body.id == id)
            .ok_or(PhysicsError::BodyNotFound { id })
    }

    pub fn index_barrier(&self, id: BarrierId) -> Result<usize, PhysicsError> {
        self.state
            .barriers
            .iter()
            .position(|barrier| barrier.id == id)
            .ok_or(PhysicsError::BarrierNotFound { id })
    }

    pub fn get_sensor(&self, id: SensorId) -> Result<&Sensor, PhysicsError> {
        self.state
            .sensors
            .iter()
            .find(|sensor| sensor.id == id)
            .ok_or(PhysicsError::SensorNotFound { id })
    }

    pub fn get_body(&self, id: BodyId) -> Result<&Body, PhysicsError> {
        self.state
            .bodies
            .iter()
            .find(|body| body.id == id)
            .ok_or(PhysicsError::BodyNotFound { id })
    }

    pub fn get_barrier(&self, id: BarrierId) -> Result<&Barrier, PhysicsError> {
        self.state
            .barriers
            .iter()
            .find(|barrier| barrier.id == id)
            .ok_or(PhysicsError::BarrierNotFound { id })
    }
}
