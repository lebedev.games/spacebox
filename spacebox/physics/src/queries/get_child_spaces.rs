use crate::{PhysicsDomain, SpaceId};

impl PhysicsDomain {
    pub fn get_child_spaces(&self, parent: SpaceId) -> Vec<SpaceId> {
        self.state
            .spaces
            .iter()
            .filter(|space| {
                if let Some(relation) = space.parent {
                    relation.space == parent
                } else {
                    false
                }
            })
            .map(|space| space.id)
            .collect()
    }
}
