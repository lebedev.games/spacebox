use crate::{Body, PhysicsDomain, SpaceId};

impl PhysicsDomain {
    pub fn get_space_bodies(&self, space: SpaceId) -> Vec<&Body> {
        self.state
            .bodies
            .iter()
            .filter(|body| body.space == space)
            .collect()
    }
}
