use crate::{BarrierId, BodyId, PhysicsDomain, PhysicsError};

impl PhysicsDomain {
    pub fn ensure_distance(
        &self,
        body: BodyId,
        barrier: BarrierId,
        limit: f32,
    ) -> Result<f32, PhysicsError> {
        let body = self.get_body(body)?;
        let barrier = self.get_barrier(barrier)?;
        if body.space != barrier.space {
            return Err(PhysicsError::DistantObjectsInDifferentSpaces);
        }
        let distance = body.position.distance(barrier.position);
        if distance > limit {
            return Err(PhysicsError::DistantObjectsIsTooFar);
        }
        Ok(distance)
    }
}
