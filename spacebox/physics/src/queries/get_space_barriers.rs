use crate::{Barrier, PhysicsDomain, SpaceId};

impl PhysicsDomain {
    pub fn get_space_barriers(&self, space: SpaceId) -> Vec<&Barrier> {
        self.state
            .barriers
            .iter()
            .filter(|barrier| barrier.space == space)
            .collect()
    }
}
