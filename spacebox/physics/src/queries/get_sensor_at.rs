use crate::{PhysicsDomain, Sensor, SpaceId, Vector};

impl PhysicsDomain {
    pub fn get_sensors_at(&self, space: SpaceId, position: Vector) -> Vec<&Sensor> {
        self.state
            .sensors
            .iter()
            .filter(|sensor| sensor.space == space)
            .filter(|sensor| sensor.position.distance(position) < sensor.radius)
            .collect()
    }
}
