use crate::{PhysicsDomain, Portal, SpaceId};

impl PhysicsDomain {
    pub fn get_space_portals(&self, space: SpaceId) -> Vec<&Portal> {
        self.state
            .portals
            .iter()
            .filter(|portal| portal.space == space)
            .collect()
    }
}
