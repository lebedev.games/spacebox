use crate::{Barrier, Segment, Vector};

pub fn intersects_barrier(a: Vector, b: Vector, barrier: &Barrier) -> bool {
    let bounds = barrier.bounds();
    intersect_segments(a, b, bounds.left_bottom(), bounds.right_bottom())
        || intersect_segments(a, b, bounds.right_bottom(), bounds.right_top())
        || intersect_segments(a, b, bounds.right_top(), bounds.left_top())
        || intersect_segments(a, b, bounds.left_top(), bounds.left_bottom())
}

pub fn intersect_segments(a: Vector, b: Vector, c: Vector, d: Vector) -> bool {
    ccw(a, c, d) != ccw(b, c, d) && ccw(a, b, c) != ccw(a, b, d)
}

pub fn intersect_segments2(from: Segment, to: Segment) -> bool {
    intersect_segments(from.begin, from.end, to.begin, to.end)
}

fn ccw(a: Vector, b: Vector, c: Vector) -> bool {
    (c.y - a.y) * (b.x - a.x) > (b.y - a.y) * (c.x - a.x)
}

pub fn get_collision_strength(a: Vector, b: Vector, barriers: &Vec<&Barrier>) -> f32 {
    let mut strength = 1.0;
    for barrier in barriers {
        if intersects_barrier(a, b, barrier) {
            strength *= 0.0; // barrier opacity
        }

        if strength == 0.0 {
            break;
        }
    }

    strength
}
