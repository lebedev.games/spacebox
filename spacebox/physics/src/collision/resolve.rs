use crate::{Barrier, Vector};

pub fn apply_with_collision_response(
    position: Vector,
    delta: Vector,
    barriers: &Vec<&Barrier>,
) -> Vector {
    let mut x = position.x + delta.x;
    let mut y = position.y + delta.y;

    let move_left = delta.x < 0.0;
    let move_right = delta.x > 0.0;
    let move_forward = delta.y > 0.0;
    let move_backward = delta.y < 0.0;

    for barrier in barriers {
        let bounds = barrier.bounds();
        let horizontal = position.y > bounds.min.y && position.y < bounds.max.y;

        if y > bounds.min.y && y < bounds.max.y && x > bounds.min.x && x < bounds.max.x {
            if horizontal {
                if move_left {
                    x = bounds.max.x;
                }

                if move_right {
                    x = bounds.min.x;
                }
            } else {
                if move_forward {
                    y = bounds.min.y;
                }

                if move_backward {
                    y = bounds.max.y;
                }
            }
        }
    }

    Vector { x, y }
}
