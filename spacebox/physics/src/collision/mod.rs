pub use detection::*;
pub use resolve::*;

mod detection;
mod resolve;
