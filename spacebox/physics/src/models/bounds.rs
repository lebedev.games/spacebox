use crate::Vector;

pub struct Bounds {
    pub min: Vector,
    pub max: Vector,
}

impl Bounds {
    pub fn left_bottom(&self) -> Vector {
        self.min
    }

    pub fn right_bottom(&self) -> Vector {
        Vector::new(self.max.x, self.min.y)
    }

    pub fn right_top(&self) -> Vector {
        self.max
    }

    pub fn left_top(&self) -> Vector {
        Vector::new(self.min.x, self.max.y)
    }
}
