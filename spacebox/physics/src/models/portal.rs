use serde::{Deserialize, Serialize};

use crate::{Segment, SpaceId, Vector};

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq, Hash, Eq)]
pub struct PortalId(pub usize);

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct PortalKey(pub String);

#[derive(Debug, Serialize, Deserialize)]
pub struct PortalType {
    pub key: PortalKey,
    pub offset: Vector,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Portal {
    pub space: SpaceId,
    pub target: Option<SpaceId>,
    pub segment: Segment,
    pub offset: Vector,
}
