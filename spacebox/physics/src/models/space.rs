use serde::{Deserialize, Serialize};

use crate::{PhysicsError, Vector};

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq, Eq, Hash)]
pub struct SpaceId(pub usize);

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SpaceKey(pub String);

#[derive(Debug, Serialize, Deserialize)]
pub struct SpaceType {
    pub key: SpaceKey,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub enum SpaceOrigin {
    Deck,
    Colony,
    System,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq)]
pub struct SpaceRelation {
    pub space: SpaceId,
    pub point: Vector,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Space {
    pub id: SpaceId,
    pub key: SpaceKey,
    pub origin: SpaceOrigin,
    pub parent: Option<SpaceRelation>,
}

impl Space {
    pub fn ensure_parent_relation(&self, parent: SpaceId) -> Result<SpaceRelation, PhysicsError> {
        let relation = self
            .parent
            .ok_or(PhysicsError::SpaceHasNoParent { space: self.id })?;
        if relation.space == parent {
            Ok(relation)
        } else {
            Err(PhysicsError::SpaceHasNoParent { space: self.id })
        }
    }
}

impl From<&str> for SpaceKey {
    fn from(value: &str) -> Self {
        SpaceKey(String::from(value))
    }
}

impl From<usize> for SpaceId {
    fn from(value: usize) -> Self {
        Self(value)
    }
}
