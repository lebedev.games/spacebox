use serde::{Deserialize, Serialize};

use crate::{is_rotated_horizontally, Bounds, SpaceId, Vector};

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq, Hash, Eq)]
pub struct BarrierId(pub usize);

#[derive(Default, Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct BarrierKey(pub String);

#[derive(Debug, Serialize, Deserialize)]
pub struct BarrierType {
    pub key: BarrierKey,
    pub size: Vector,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq)]
pub enum BarrierOrigin {
    Prop,
    Crop,
    Bed,
    Workbench,
    Storage,
    Construction,
    Planter,
    Bridge,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct Barrier {
    pub id: BarrierId,
    pub key: BarrierKey,
    pub space: SpaceId,
    pub position: Vector,
    pub rotation: f32,
    pub size: Vector,
    pub origin: BarrierOrigin,
}

impl Barrier {
    pub fn bounds(&self) -> Bounds {
        // implicit transformation for rectangle barrier
        // (for square barrier does not matter)
        let (width, height) = if is_rotated_horizontally(self.rotation) {
            (self.size.y, self.size.x)
        } else {
            (self.size.x, self.size.y)
        };
        let offset = Vector::new(width, height) / 2.0;
        Bounds {
            min: self.position - offset,
            max: self.position + offset,
        }
    }
}

impl From<usize> for BarrierId {
    fn from(value: usize) -> Self {
        Self(value)
    }
}

impl From<&str> for BarrierKey {
    fn from(value: &str) -> Self {
        Self(value.into())
    }
}
