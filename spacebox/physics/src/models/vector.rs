use std::ops;

use serde::{Deserialize, Serialize};

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq)]
pub struct Segment {
    pub begin: Vector,
    pub end: Vector,
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq)]
pub struct Vector {
    pub x: f32,
    pub y: f32,
}

impl Vector {
    pub const ZERO: Vector = Vector::new(0.0, 0.0);

    pub const fn new(x: f32, y: f32) -> Self {
        Vector { x, y }
    }

    pub fn distance(&self, other: Vector) -> f32 {
        (other - *self).magnitude()
    }

    pub fn magnitude(&self) -> f32 {
        self.sqr_magnitude().sqrt()
    }

    pub fn normalized(&self) -> Vector {
        let len = self.magnitude();
        if len != 0.0 {
            *self / len
        } else {
            Vector::ZERO
        }
    }

    pub fn in_bounds(&self, min: Vector, max: Vector) -> bool {
        self.x >= min.x && self.x <= max.x && self.y >= min.y && self.y <= max.y
    }

    pub fn sqr_magnitude(&self) -> f32 {
        self.x * self.x + self.y * self.y
    }

    pub fn _clamp_magnitude(&self, max_length: f32) -> Vector {
        let sqr_magnitude = self.sqr_magnitude();
        if sqr_magnitude <= max_length * max_length {
            return *self;
        }

        let length = sqr_magnitude.sqrt();
        let x = (self.x / length) * max_length;
        let y = (self.y / length) * max_length;

        Vector { x, y }
    }
}

impl From<(f32, f32)> for Vector {
    fn from(value: (f32, f32)) -> Self {
        Vector::new(value.0, value.1)
    }
}

impl From<(usize, usize)> for Vector {
    fn from(value: (usize, usize)) -> Self {
        Vector::new(value.0 as f32, value.1 as f32)
    }
}

impl ops::Add<Vector> for Vector {
    type Output = Vector;

    fn add(self, other: Vector) -> Vector {
        Vector {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl ops::AddAssign<Vector> for Vector {
    fn add_assign(&mut self, other: Vector) {
        self.x += other.x;
        self.y += other.y;
    }
}

impl ops::Sub<Vector> for Vector {
    type Output = Vector;

    fn sub(self, other: Vector) -> Vector {
        Vector {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl ops::Mul<f32> for Vector {
    type Output = Vector;

    fn mul(self, scalar: f32) -> Vector {
        Vector {
            x: self.x * scalar,
            y: self.y * scalar,
        }
    }
}

impl ops::Div<f32> for Vector {
    type Output = Vector;

    fn div(self, scalar: f32) -> Vector {
        Vector {
            x: self.x / scalar,
            y: self.y / scalar,
        }
    }
}

impl ops::Neg for Vector {
    type Output = Vector;

    fn neg(self) -> Self::Output {
        Vector {
            x: -self.x,
            y: -self.y,
        }
    }
}

impl From<(i32, i32)> for Vector {
    fn from(values: (i32, i32)) -> Self {
        Self {
            x: values.0 as f32,
            y: values.1 as f32,
        }
    }
}
