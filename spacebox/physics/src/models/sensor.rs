use serde::{Deserialize, Serialize};

use crate::{BodyId, SpaceId, Vector};

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq, Hash, Eq)]
pub struct SensorId(pub usize);

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct SensorKey(pub String);

#[derive(Debug, Serialize, Deserialize)]
pub struct SensorType {
    pub key: SensorKey,
    pub radius: f32,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(tag = "$type")]
pub enum SensorController {
    Static,
    Dynamic { body: BodyId },
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Sensor {
    pub id: SensorId,
    pub key: SensorKey,
    pub space: SpaceId,
    pub position: Vector,
    pub radius: f32,
    pub controller: SensorController,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Signal {
    pub sensor: SensorId,
    pub distance: f32,
    pub strength: f32,
}

impl From<&str> for SensorKey {
    fn from(value: &str) -> Self {
        SensorKey(String::from(value))
    }
}

impl From<usize> for SensorId {
    fn from(value: usize) -> Self {
        Self(value)
    }
}
