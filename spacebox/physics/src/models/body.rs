use serde::{Deserialize, Serialize};

use crate::{SpaceId, Vector};

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq, Hash, Eq)]
pub struct BodyId(pub usize);

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct BodyKey(pub String);

#[derive(Debug, Serialize, Deserialize)]
pub struct BodyType {
    pub key: BodyKey,
    pub origin: BodyOrigin,
    pub speed: f32,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub enum BodyOrigin {
    Character,
    Spaceship,
    Planet,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Body {
    pub id: BodyId,
    pub key: BodyKey,
    pub space: SpaceId,
    pub origin: BodyOrigin,
    pub position: Vector,
    pub velocity: Vector,
    pub speed: f32,
    pub steering: Option<SteeringBehaviour>,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(tag = "$type")]
pub enum SteeringBehaviour {
    Arrive { destination: Vector },
    MoveTowards { destination: Vector },
    MoveInstantly { destination: Vector },
    FollowPath { path: Vec<Vector> },
}

impl Body {
    pub fn offset_steering(&mut self, offset: Vector) {
        if let Some(steering) = self.steering.as_ref() {
            let steering = match steering {
                SteeringBehaviour::Arrive { destination: point } => SteeringBehaviour::Arrive {
                    destination: *point + offset,
                },
                SteeringBehaviour::MoveTowards { destination: point } => {
                    SteeringBehaviour::MoveTowards {
                        destination: *point + offset,
                    }
                }
                SteeringBehaviour::MoveInstantly { destination: point } => {
                    SteeringBehaviour::MoveInstantly {
                        destination: *point + offset,
                    }
                }
                SteeringBehaviour::FollowPath { path } => {
                    let path = path.into_iter().map(|point| *point + offset).collect();
                    SteeringBehaviour::FollowPath { path }
                }
            };
            self.steering = Some(steering);
        }
    }
}

impl From<&str> for BodyKey {
    fn from(value: &str) -> Self {
        BodyKey(String::from(value))
    }
}

impl From<usize> for BodyId {
    fn from(value: usize) -> Self {
        Self(value)
    }
}
