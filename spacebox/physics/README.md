### Models

#### Space

Spaces are containers for simulating objects in physics domain. 
They control how all the bodies, barriers, and other physics objects interact together.

#### Bodies

A body represents the physical properties of an game entity: mass, position, rotation, velocity, etc.
They react to collisions, are affected by forces and gravity, and does not have a shape. 

#### Barriers 

A barrier represents the physical properties of game entities that never (or rarely) move. 
Barriers differ in that they are have shape.
Using barriers for things like static scene props offers a big performance boost 
because physics engine doesn’t need to check for collisions between barriers.

### Parent Space Relationship

All spaces can have parent and "reparent" function.

"Reparenting" means that all physics objects are taken out of its current space 
and are nested within another space with specified offset. Implemented by two methods:
* `translate_space`
* `extract_space`

For example this can be used for spaceship landing/launch:
1. Deck of spaceship represents by space with no parent
2. (on landing) Deck space translated to colony space
3. Now all deck objects are nested within colony space
4. (on launch) Deck space extracted from colony space in corresponded bounds
5. Now all deck objects are taken out of colony space

![./readme/fig1.png](.readme/fig1.png)

**NOTE:** This mechanism is critically necessary in order 
not to implement additional processing of nested objects in other game domains.

### Steering Behaviour

Steering behaviors algorithms aim to help bodies update 
by using simple forces that are combined to produce life-like movement in space. 

