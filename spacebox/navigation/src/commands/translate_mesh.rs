use crate::{
    CreateObstacleOperation, Mesh, MeshId, Navigation, NavigationDomain, NavigationError, PathPoint,
};

pub struct TranslateMeshOperation {
    pub mesh_index: usize,
    pub mesh: Mesh,
    pub offset: PathPoint,
    pub destination: MeshId,
    pub navigators: Vec<usize>,
    pub obstacles: Vec<CreateObstacleOperation>,
}

impl NavigationDomain {
    pub fn prepare_translate_mesh_operation(
        &self,
        source: MeshId,
        destination: MeshId,
        offset: PathPoint,
    ) -> Result<TranslateMeshOperation, NavigationError> {
        let navigators = self
            .state
            .navigators
            .iter()
            .enumerate()
            .filter(|pair| pair.1.mesh == source)
            .map(|pair| pair.0)
            .collect();

        let translated_obstacles = self.get_mesh(source)?.obstacles.iter();

        let mut obstacles = vec![];
        for obstacle in translated_obstacles {
            let obstacle = self.prepare_create_obstacle_operation(
                destination,
                &obstacle.key,
                obstacle.id,
                obstacle.location + offset,
                obstacle.rotation,
            )?;
            obstacles.push(obstacle);
        }

        // erase source mesh
        let mesh_index = self.index_mesh(source)?;
        let mesh = &self.state.meshes[mesh_index];
        let mesh = self.prepare_mesh(mesh.id, &mesh.key)?;

        Ok(TranslateMeshOperation {
            mesh,
            mesh_index,
            destination,
            navigators,
            offset,
            obstacles,
        })
    }

    pub fn translate_mesh(&mut self, operation: TranslateMeshOperation) -> Vec<Navigation> {
        let mut events = vec![];

        for obstacle in operation.obstacles {
            events.extend(self.create_obstacle(obstacle));
        }

        let offset = operation.offset;
        for index in operation.navigators {
            let navigator = &mut self.state.navigators[index];

            navigator.location += offset;
            navigator.mesh = operation.destination;
            if let Some(path) = navigator.path.as_ref() {
                let path = path.iter().map(|point| *point + offset).collect();
                navigator.path = Some(path);
            }
        }

        self.state.meshes[operation.mesh_index] = operation.mesh;

        events
    }
}
