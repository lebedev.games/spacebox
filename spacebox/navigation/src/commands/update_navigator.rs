use crate::{NavigationDomain, NavigatorId, PathPoint};

impl NavigationDomain {
    pub fn update_navigator(&mut self, id: NavigatorId, location: PathPoint) {
        match self.index_navigator(id) {
            Ok(index) => {
                let navigator = &mut self.state.navigators[index];
                navigator.location = location;
            }
            Err(error) => error!("Unable to update navigator location, {:?}", error),
        }
    }
}
