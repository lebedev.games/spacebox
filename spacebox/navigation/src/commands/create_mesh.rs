use crate::{
    Fragment, FragmentId, Mesh, MeshId, MeshKey, Navigation, NavigationDomain, NavigationError,
    Point,
};

impl NavigationDomain {
    pub fn prepare_mesh(&self, id: MeshId, key: &MeshKey) -> Result<Mesh, NavigationError> {
        let mesh = self.get_mesh_type(key)?;
        let mesh = Mesh {
            id,
            key: key.clone(),
            counter: 0,
            size: mesh.size,
            fragments: vec![Fragment {
                id: FragmentId(0),
                position: Point { x: 0, y: 0 },
                size: mesh.size,
                obstacle: None,
            }],
            links: vec![],
            obstacles: vec![],
        };
        Ok(mesh)
    }

    pub fn create_mesh(&mut self, mesh: Mesh) -> Vec<Navigation> {
        self.state.meshes.push(mesh);
        vec![]
    }
}
