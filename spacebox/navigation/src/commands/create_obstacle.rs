use crate::{
    create_bounds_from, Edge, Fragment, FragmentId, FragmentLink, MeshId, Navigation,
    NavigationDomain, NavigationError, Obstacle, ObstacleId, ObstacleKey, PathPoint, Point,
    Segment,
};

pub struct CreateObstacleOperation {
    pub target_mesh_index: usize,
    pub counter: usize,
    pub obstacle: Obstacle,
    pub fragments_to_create: Vec<Fragment>,
    pub fragments_to_delete: Vec<FragmentId>,
    pub links_to_create: Vec<FragmentLink>,
}

struct CoveredFragment {
    pub id: FragmentId,
    pub left_bottom: Point,
    pub right_top: Point,
}

impl NavigationDomain {
    pub fn create_obstacle(&mut self, operation: CreateObstacleOperation) -> Vec<Navigation> {
        let mut mesh = &mut self.state.meshes[operation.target_mesh_index];
        mesh.fragments
            .retain(|fragment| !operation.fragments_to_delete.contains(&fragment.id));
        mesh.links.retain(|link| {
            !(operation.fragments_to_delete.contains(&link.fragment_from)
                || operation.fragments_to_delete.contains(&link.fragment_to))
        });
        mesh.fragments
            .extend(operation.fragments_to_create.into_iter());
        mesh.links.extend(operation.links_to_create.into_iter());
        mesh.counter = operation.counter;
        mesh.obstacles.push(operation.obstacle);
        vec![]
    }

    pub fn prepare_create_obstacle_operation(
        &self,
        mesh: MeshId,
        key: &ObstacleKey,
        obstacle_id: ObstacleId,
        location: PathPoint,
        rotation: bool,
    ) -> Result<CreateObstacleOperation, NavigationError> {
        let target_mesh_index = self.index_mesh(mesh)?;
        let mesh = &self.state.meshes[target_mesh_index];
        let obstacle = self.get_obstacle_type(key)?;

        ensure_location_in_bounds(location, PathPoint { x: 0.0, y: 0.0 }, mesh.size).ok_or(
            NavigationError::ObstacleCreationOutOfMeshBounds {
                obstacle: obstacle_id,
                key: key.clone(),
                location,
                mesh: mesh.id,
            },
        )?;

        let mut covered_fragments: Vec<CoveredFragment> = Vec::new();

        let (bounds_min, bounds_max) = create_bounds_from(location, obstacle.size, rotation);
        for y in bounds_min.y..bounds_max.y {
            for x in bounds_min.x..bounds_max.x {
                let point = Point { x, y };
                let fragment = self.get_fragment_at(mesh, point)?;

                if fragment.obstacle.is_some() {
                    return Err(NavigationError::ObstacleCreationOverExistingObstacle);
                }

                match covered_fragments
                    .iter_mut()
                    .find(|covered_fragment| covered_fragment.id == fragment.id)
                {
                    Some(covered_fragment) => {
                        if point < covered_fragment.left_bottom {
                            covered_fragment.left_bottom = point;
                        } else if point > covered_fragment.right_top {
                            covered_fragment.right_top = point;
                        }
                    }
                    None => covered_fragments.push(CoveredFragment {
                        id: fragment.id,
                        left_bottom: point,
                        right_top: point,
                    }),
                }
            }
        }

        let mut counter = mesh.counter;
        let mut fragments_to_delete: Vec<FragmentId> = Vec::new();
        let mut fragments_to_create = Vec::new();
        let mut links_to_create: Vec<FragmentLink> = Vec::new();

        let mut links_snapshot = mesh.links.clone();

        for covered_fragment in covered_fragments.iter() {
            let fragment_id = covered_fragment.id;
            let fragment = self.get_fragment(mesh, fragment_id)?;

            // to prevent mesh state modification we should copy links
            // it naive approach to implement obstacle creation transaction
            // for multiple fragments modification per operation
            // note: this code candidate for optimization in large navigation mesh
            links_snapshot.retain(|link| {
                !(fragments_to_delete.contains(&link.fragment_from)
                    || fragments_to_delete.contains(&link.fragment_to))
            });
            links_snapshot.extend(links_to_create.clone());
            fragments_to_delete.push(fragment_id);
            // in created links we should remove previous deleted fragment links
            links_to_create.retain(|link| {
                !(fragments_to_delete.contains(&link.fragment_from)
                    || fragments_to_delete.contains(&link.fragment_to))
            });

            let mut fragment_links = Vec::new();
            // convert links to directed from fragment
            for link in links_snapshot
                .iter()
                .filter(|link| link.fragment_from == fragment_id)
            {
                fragment_links.push(FragmentLink {
                    fragment_from: link.fragment_from,
                    fragment_to: link.fragment_to,
                    contact_edge: link.contact_edge,
                    contact_segment: link.contact_segment,
                })
            }
            for link in links_snapshot
                .iter()
                .filter(|link| link.fragment_to == fragment_id)
            {
                fragment_links.push(FragmentLink {
                    fragment_from: link.fragment_to,
                    fragment_to: link.fragment_from,
                    contact_edge: link.contact_edge.opposite(),
                    contact_segment: link.contact_segment,
                })
            }

            let left_bottom = covered_fragment.left_bottom;
            let right_top = covered_fragment.right_top;
            let cover_position = left_bottom;
            let cover_size = right_top - left_bottom + Point::ONE;
            let cover_local_position = cover_position - fragment.position;

            // + . .
            // + O .
            // + . .
            let width = cover_local_position.x;
            let height = fragment.size.y;
            let mut left = None;
            if width != 0 && height != 0 {
                counter += 1;
                let id = FragmentId(counter);
                let new = Fragment {
                    id,
                    position: Point {
                        x: fragment.position.x,
                        y: fragment.position.y,
                    },
                    size: Point::new(width, height),
                    obstacle: None,
                };

                for link in fragment_links.iter() {
                    let sibling = link.fragment_to;

                    match link.contact_edge {
                        Edge::North => {
                            if let Some(intersection) =
                                intersect(&new, Edge::North, link.contact_segment)
                            {
                                links_to_create.push(FragmentLink::new(
                                    id,
                                    sibling,
                                    Edge::North,
                                    intersection,
                                ));
                            }
                        }
                        Edge::South => {
                            if let Some(intersection) =
                                intersect(&new, Edge::South, link.contact_segment)
                            {
                                links_to_create.push(FragmentLink::new(
                                    id,
                                    sibling,
                                    Edge::South,
                                    intersection,
                                ));
                            }
                        }
                        Edge::West => {
                            links_to_create.push(FragmentLink::new(
                                id,
                                sibling,
                                Edge::West,
                                link.contact_segment,
                            ));
                        }
                        Edge::East => {
                            // east sibling blocked by obstacle fragment
                        }
                    }
                }

                left = Some(new);
            }

            // l . .
            // l O .
            // l + +
            let width = fragment.size.x - cover_local_position.x;
            let height = cover_local_position.y;
            let mut bottom = None;
            if width != 0 && height != 0 {
                counter += 1;
                let id = FragmentId(counter);
                let new = Fragment {
                    id,
                    position: Point {
                        x: fragment.position.x + cover_local_position.x,
                        y: fragment.position.y,
                    },
                    size: Point::new(width, height),
                    obstacle: None,
                };

                if let Some(ref left) = left {
                    links_to_create.push(FragmentLink::from_contact(Edge::East, &left, &new));
                }

                for link in fragment_links.iter() {
                    let sibling = link.fragment_to;

                    match link.contact_edge {
                        Edge::North => {
                            // north sibling blocked by obstacle fragment
                        }
                        Edge::South => {
                            if let Some(intersection) =
                                intersect(&new, Edge::South, link.contact_segment)
                            {
                                links_to_create.push(FragmentLink::new(
                                    id,
                                    sibling,
                                    Edge::South,
                                    intersection,
                                ));
                            }
                        }
                        Edge::West => {
                            if left.is_none() {
                                if let Some(intersection) =
                                    intersect(&new, Edge::West, link.contact_segment)
                                {
                                    links_to_create.push(FragmentLink::new(
                                        id,
                                        sibling,
                                        Edge::West,
                                        intersection,
                                    ));
                                }
                            }
                        }
                        Edge::East => {
                            if let Some(intersection) =
                                intersect(&new, Edge::East, link.contact_segment)
                            {
                                links_to_create.push(FragmentLink::new(
                                    id,
                                    sibling,
                                    Edge::East,
                                    intersection,
                                ));
                            }
                        }
                    }
                }

                bottom = Some(new);
            };

            // l . +
            // l O +
            // l b b
            let width = fragment.size.x - cover_local_position.x - cover_size.x;
            let height = fragment.size.y - cover_local_position.y;
            let mut right = None;
            if width != 0 && height != 0 {
                counter += 1;
                let id = FragmentId(counter);
                let new = Fragment {
                    id,
                    position: Point {
                        x: fragment.position.x + cover_local_position.x + cover_size.x,
                        y: fragment.position.y + cover_local_position.y,
                    },
                    size: Point::new(width, height),
                    obstacle: None,
                };

                if let Some(ref bottom) = bottom {
                    links_to_create.push(FragmentLink::from_contact(Edge::North, &bottom, &new));
                }

                for link in fragment_links.iter() {
                    let sibling = link.fragment_to;

                    match link.contact_edge {
                        Edge::North => {
                            if let Some(intersection) =
                                intersect(&new, Edge::North, link.contact_segment)
                            {
                                links_to_create.push(FragmentLink::new(
                                    id,
                                    sibling,
                                    Edge::North,
                                    intersection,
                                ));
                            }
                        }
                        Edge::South => {
                            if bottom.is_none() {
                                if let Some(intersection) =
                                    intersect(&new, Edge::South, link.contact_segment)
                                {
                                    links_to_create.push(FragmentLink::new(
                                        id,
                                        sibling,
                                        Edge::South,
                                        intersection,
                                    ));
                                }
                            }
                        }
                        Edge::West => {
                            // west sibling blocked by obstacle fragment
                        }
                        Edge::East => {
                            if let Some(intersection) =
                                intersect(&new, Edge::East, link.contact_segment)
                            {
                                links_to_create.push(FragmentLink::new(
                                    id,
                                    sibling,
                                    Edge::East,
                                    intersection,
                                ));
                            }
                        }
                    }
                }

                right = Some(new);
            }

            // l + r
            // l O r
            // l b b
            let width = cover_size.x;
            let height = fragment.size.y - cover_local_position.y - cover_size.y;
            let mut top = None;
            if width != 0 && height != 0 {
                counter += 1;
                let id = FragmentId(counter);
                let new = Fragment {
                    id,
                    position: Point {
                        x: fragment.position.x + cover_local_position.x,
                        y: fragment.position.y + cover_local_position.y + cover_size.y,
                    },
                    size: Point::new(width, height),
                    obstacle: None,
                };

                if let Some(ref right) = right {
                    links_to_create.push(FragmentLink::from_contact(Edge::West, &right, &new));
                }

                if let Some(ref left) = left {
                    links_to_create.push(FragmentLink::from_contact(Edge::East, &left, &new));
                }

                for link in fragment_links.iter() {
                    let sibling = link.fragment_to;

                    match link.contact_edge {
                        Edge::North => {
                            if let Some(intersection) =
                                intersect(&new, Edge::North, link.contact_segment)
                            {
                                links_to_create.push(FragmentLink::new(
                                    id,
                                    sibling,
                                    Edge::North,
                                    intersection,
                                ));
                            }
                        }
                        Edge::South => {
                            // south sibling blocked by obstacle fragment
                        }
                        Edge::West => {
                            if left.is_none() {
                                if let Some(intersection) =
                                    intersect(&new, Edge::West, link.contact_segment)
                                {
                                    links_to_create.push(FragmentLink::new(
                                        id,
                                        sibling,
                                        Edge::West,
                                        intersection,
                                    ));
                                }
                            }
                        }
                        Edge::East => {
                            if right.is_none() {
                                if let Some(intersection) =
                                    intersect(&new, Edge::East, link.contact_segment)
                                {
                                    links_to_create.push(FragmentLink::new(
                                        id,
                                        sibling,
                                        Edge::East,
                                        intersection,
                                    ));
                                }
                            }
                        }
                    }
                }

                top = Some(new);
            }

            if let Some(left) = left {
                fragments_to_create.push(left);
            }
            if let Some(bottom) = bottom {
                fragments_to_create.push(bottom);
            }
            if let Some(right) = right {
                fragments_to_create.push(right);
            }
            if let Some(top) = top {
                fragments_to_create.push(top);
            }

            counter += 1;
            let block = Fragment {
                id: FragmentId(counter),
                position: cover_position.clone(),
                size: cover_size.clone(),
                obstacle: Some(obstacle_id),
            };
            fragments_to_create.push(block);
        }

        let creation = CreateObstacleOperation {
            target_mesh_index,
            counter,
            obstacle: Obstacle {
                id: obstacle_id,
                key: key.clone(),
                location,
                rotation,
                size: obstacle.size,
                mesh: mesh.id,
            },
            fragments_to_delete,
            fragments_to_create,
            links_to_create,
        };

        Ok(creation)
    }
}

fn intersect(fragment: &Fragment, edge: Edge, segment: Segment) -> Option<Segment> {
    match edge {
        Edge::North => {
            let x0 = segment.begin.x.max(fragment.position.x);
            let x1 = segment.end.x.min(fragment.position.x + fragment.size.x);
            if x1 > x0 {
                Some(Segment {
                    begin: Point {
                        x: x0,
                        y: segment.begin.y,
                    },
                    end: Point {
                        x: x1,
                        y: segment.begin.y,
                    },
                })
            } else {
                None
            }
        }
        Edge::South => {
            let x0 = segment.begin.x.max(fragment.position.x);
            let x1 = segment.end.x.min(fragment.position.x + fragment.size.x);
            if x1 > x0 {
                Some(Segment {
                    begin: Point {
                        x: x0,
                        y: segment.begin.y,
                    },
                    end: Point {
                        x: x1,
                        y: segment.begin.y,
                    },
                })
            } else {
                None
            }
        }
        Edge::West => {
            let y0 = segment.begin.y.max(fragment.position.y);
            let y1 = segment.end.y.min(fragment.position.y + fragment.size.y);
            if y1 > y0 {
                Some(Segment {
                    begin: Point {
                        x: segment.begin.x,
                        y: y0,
                    },
                    end: Point {
                        x: segment.begin.x,
                        y: y1,
                    },
                })
            } else {
                None
            }
        }
        Edge::East => {
            let y0 = segment.begin.y.max(fragment.position.y);
            let y1 = segment.end.y.min(fragment.position.y + fragment.size.y);
            if y1 > y0 {
                Some(Segment {
                    begin: Point {
                        x: segment.begin.x,
                        y: y0,
                    },
                    end: Point {
                        x: segment.begin.x,
                        y: y1,
                    },
                })
            } else {
                None
            }
        }
    }
}

pub fn ensure_location_in_bounds(
    location: PathPoint,
    position: PathPoint,
    size: Point,
) -> Option<()> {
    if (location.x >= position.x && location.y >= position.y)
        && location.x < position.x + size.x as f32
        && location.y < position.y + size.y as f32
    {
        Some(())
    } else {
        None
    }
}
