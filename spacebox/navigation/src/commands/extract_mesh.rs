use crate::{
    CreateObstacleOperation, MeshId, Navigation, NavigationDomain, NavigationError, NavigatorId,
    ObstacleId, PathPoint, RemoveObstacleOperation,
};

pub struct ExtractMeshOperation {
    pub offset: PathPoint,
    pub destination: MeshId,
    pub navigators: Vec<usize>,
    pub obstacles_to_create: Vec<CreateObstacleOperation>,
    pub obstacles_to_remove: Vec<RemoveObstacleOperation>,
}

impl NavigationDomain {
    pub fn prepare_extract_mesh_operation(
        &self,
        source: MeshId,
        destination: MeshId,
        offset: PathPoint,
        target_navigators: Vec<NavigatorId>,
        target_obstacles: Vec<ObstacleId>,
    ) -> Result<ExtractMeshOperation, NavigationError> {
        let mut navigators = vec![];
        for navigator in target_navigators {
            navigators.push(self.index_navigator(navigator)?);
        }

        let obstacles = self
            .get_mesh(source)?
            .obstacles
            .iter()
            .filter(|obstacle| target_obstacles.contains(&obstacle.id));

        let mut obstacles_to_create = vec![];
        let mut obstacles_to_remove = vec![];
        for obstacle in obstacles {
            let operation = self.prepare_create_obstacle_operation(
                destination,
                &obstacle.key,
                obstacle.id,
                obstacle.location + offset,
                obstacle.rotation,
            )?;
            obstacles_to_create.push(operation);
            let operation = self.prepare_obstacle_remove_operation(source, obstacle.id)?;
            obstacles_to_remove.push(operation);
        }

        Ok(ExtractMeshOperation {
            destination,
            navigators,
            offset,
            obstacles_to_create,
            obstacles_to_remove,
        })
    }

    pub fn extract_mesh(&mut self, operation: ExtractMeshOperation) -> Vec<Navigation> {
        let mut events = vec![];

        for obstacle in operation.obstacles_to_remove {
            events.extend(self.remove_obstacle(obstacle));
        }

        for obstacle in operation.obstacles_to_create {
            events.extend(self.create_obstacle(obstacle));
        }

        let offset = operation.offset;
        for index in operation.navigators {
            let navigator = &mut self.state.navigators[index];

            navigator.location += offset;
            navigator.mesh = operation.destination;
            if let Some(path) = navigator.path.as_ref() {
                let path = path.iter().map(|point| *point + offset).collect();
                navigator.path = Some(path);
            }
        }

        events
    }
}
