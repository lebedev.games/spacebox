use crate::{
    MeshId, Navigation, NavigationDomain, NavigationError, Navigator, NavigatorId, NavigatorKey,
    PathPoint,
};

pub struct CreateNavigatorOperation {
    pub navigator: Navigator,
}

impl NavigationDomain {
    pub fn prepare_navigator(
        &self,
        key: &NavigatorKey,
        id: NavigatorId,
        mesh: MeshId,
        location: PathPoint,
    ) -> Result<CreateNavigatorOperation, NavigationError> {
        let navigator = self.get_navigator_type(key)?;
        let navigator = Navigator {
            id,
            key: navigator.key.clone(),
            location,
            mesh,
            path: None,
        };

        Ok(CreateNavigatorOperation { navigator })
    }

    pub fn create_navigator(&mut self, operation: CreateNavigatorOperation) -> Vec<Navigation> {
        self.state.navigators.push(operation.navigator);
        vec![]
    }
}
