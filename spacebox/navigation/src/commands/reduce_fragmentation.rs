use crate::{Fragment, FragmentId, FragmentLink, NavigationDomain};

pub struct ReduceFragmentationOperation {
    pub target_mesh_index: usize,
    pub counter: usize,
    pub fragments_to_delete: Vec<FragmentId>,
    pub fragment: Fragment,
    pub links_to_create: Vec<FragmentLink>,
}

impl NavigationDomain {
    pub fn reduce_fragmentation(&mut self) {
        let mesh = &self.state.meshes[0];

        if let Some(changeset) = self.find_defragmentation_candidate(mesh, 0) {
            let mesh = &mut self.state.meshes[changeset.target_mesh_index];
            mesh.counter = changeset.counter;
            mesh.fragments
                .retain(|fragment| !changeset.fragments_to_delete.contains(&fragment.id));
            mesh.links.retain(|link| {
                !(changeset.fragments_to_delete.contains(&link.fragment_from)
                    || changeset.fragments_to_delete.contains(&link.fragment_to))
            });
            mesh.fragments.push(changeset.fragment);
            mesh.links.extend(changeset.links_to_create.into_iter());
        }
    }
}
