use crate::{
    Edge, Fragment, FragmentId, FragmentLink, MeshId, Navigation, NavigationDomain,
    NavigationError, ObstacleId, Point,
};

pub struct RemoveObstacleOperation {
    pub target_mesh_index: usize,
    pub obstacle: ObstacleId,
    pub fragments_to_convert: Vec<FragmentId>,
    pub links_to_create: Vec<FragmentLink>,
}

impl NavigationDomain {
    pub fn remove_obstacle(&mut self, operation: RemoveObstacleOperation) -> Vec<Navigation> {
        let mesh = &mut self.state.meshes[operation.target_mesh_index];
        mesh.obstacles
            .retain(|obstacle| obstacle.id != operation.obstacle);
        mesh.links.extend(operation.links_to_create);
        for fragment in mesh.fragments.iter_mut() {
            if operation.fragments_to_convert.contains(&fragment.id) {
                fragment.obstacle = None;
            }
        }
        vec![]
    }

    pub fn prepare_obstacle_remove_operation(
        &self,
        mesh: MeshId,
        obstacle: ObstacleId,
    ) -> Result<RemoveObstacleOperation, NavigationError> {
        let target_mesh_index = self.index_mesh(mesh)?;
        let mesh = &self.state.meshes[target_mesh_index];
        self.get_obstacle(mesh, obstacle)?;

        let mut links_to_create = Vec::new();
        let fragments_to_convert: Vec<&Fragment> = mesh
            .fragments
            .iter()
            .filter(|fragment| fragment.obstacle == Some(obstacle))
            .collect();

        let mut handles_fragments = Vec::new();
        for fragment in fragments_to_convert.iter() {
            let mut siblings: Vec<(&Fragment, Edge)> = Vec::new();

            for pointer_x in 0..fragment.size.x {
                let point = Point {
                    x: fragment.position.x + pointer_x,
                    y: fragment.position.y + fragment.size.y,
                };
                if let Ok(sibling) = self.get_fragment_at(mesh, point) {
                    if (sibling.obstacle.is_none() || fragments_to_convert.contains(&sibling))
                        && !siblings
                            .iter()
                            .any(|(s, e)| s.id == sibling.id && e == &Edge::North)
                        && !handles_fragments.contains(&sibling.id)
                    {
                        siblings.push((sibling, Edge::North));
                    }
                }

                let point = Point {
                    x: fragment.position.x + pointer_x,
                    y: fragment.position.y - 1,
                };
                if let Ok(sibling) = self.get_fragment_at(mesh, point) {
                    if (sibling.obstacle.is_none() || fragments_to_convert.contains(&sibling))
                        && !siblings
                            .iter()
                            .any(|(s, e)| s.id == sibling.id && e == &Edge::South)
                        && !handles_fragments.contains(&sibling.id)
                    {
                        siblings.push((sibling, Edge::South));
                    }
                }
            }

            for pointer_y in 0..fragment.size.y {
                let point = Point {
                    x: fragment.position.x - 1,
                    y: fragment.position.y + pointer_y,
                };
                if let Ok(sibling) = self.get_fragment_at(mesh, point) {
                    if (sibling.obstacle.is_none() || fragments_to_convert.contains(&sibling))
                        && !siblings
                            .iter()
                            .any(|(s, e)| s.id == sibling.id && e == &Edge::West)
                        && !handles_fragments.contains(&sibling.id)
                    {
                        siblings.push((sibling, Edge::West));
                    }
                }

                let point = Point {
                    x: fragment.position.x + fragment.size.x,
                    y: fragment.position.y + pointer_y,
                };
                if let Ok(sibling) = self.get_fragment_at(mesh, point) {
                    if (sibling.obstacle.is_none() || fragments_to_convert.contains(&sibling))
                        && !siblings
                            .iter()
                            .any(|(s, e)| s.id == sibling.id && e == &Edge::East)
                        && !handles_fragments.contains(&sibling.id)
                    {
                        siblings.push((sibling, Edge::East));
                    }
                }
            }

            for (sibling, edge) in siblings.into_iter() {
                links_to_create.push(FragmentLink::from_contact(edge, fragment, sibling));
            }

            handles_fragments.push(fragment.id);
        }

        let operation = RemoveObstacleOperation {
            target_mesh_index,
            obstacle: obstacle.clone(),
            fragments_to_convert: fragments_to_convert
                .iter()
                .map(|fragment| fragment.id)
                .collect(),
            links_to_create,
        };

        Ok(operation)
    }
}
