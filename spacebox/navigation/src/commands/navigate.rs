use crate::{Navigation, NavigationDomain, NavigationError, NavigatorId, PathPoint};

pub struct NavigateOperation {
    pub navigator_index: usize,
    pub path: Vec<PathPoint>,
}

impl NavigationDomain {
    pub fn prepare_navigate_operation(
        &self,
        navigator: NavigatorId,
        destination: PathPoint,
    ) -> Result<NavigateOperation, NavigationError> {
        let navigator_index = self.index_navigator(navigator)?;
        let navigator = &self.state.navigators[navigator_index];
        let mesh = self.get_mesh(navigator.mesh)?;
        let path = self.find_path(mesh, navigator.location, destination)?;

        Ok(NavigateOperation {
            navigator_index,
            path,
        })
    }

    pub fn navigate(&mut self, operation: NavigateOperation) -> Vec<Navigation> {
        let navigator = &mut self.state.navigators[operation.navigator_index];
        navigator.path = Some(operation.path);
        vec![]
    }
}
