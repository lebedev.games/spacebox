use serde::{Deserialize, Serialize};

use crate::{MeshType, NavigatorType, ObstacleType};

#[derive(Debug, Serialize, Deserialize, Default)]
pub struct NavigationDatabase {
    pub meshes: Vec<MeshType>,
    pub obstacles: Vec<ObstacleType>,
    pub navigators: Vec<NavigatorType>,
}
