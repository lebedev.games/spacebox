use serde::{Deserialize, Serialize};

use crate::{
    FragmentId, MeshId, MeshKey, NavigatorId, NavigatorKey, ObstacleId, ObstacleKey, PathPoint,
    Point,
};

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(tag = "$type")]
pub enum NavigationError {
    MeshTypeNotFound {
        key: MeshKey,
    },
    ObstacleTypeNotFound {
        key: ObstacleKey,
    },
    FragmentNotFoundAtPoint {
        point: Point,
    },
    FragmentNotFound {
        id: FragmentId,
    },
    MeshNotFound {
        id: MeshId,
    },
    ObstacleNotFound,
    ObstacleCreationOverExistingObstacle,
    ObstacleCreationOutOfMeshBounds {
        obstacle: ObstacleId,
        key: ObstacleKey,
        location: PathPoint,
        mesh: MeshId,
    },
    NavigatorNotFound {
        id: NavigatorId,
    },
    NavigatorTypeNotFound {
        key: NavigatorKey,
    },
    PathNotFound,
    PointOutOfMesh,
}
