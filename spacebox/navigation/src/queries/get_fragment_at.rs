use crate::{Fragment, Mesh, NavigationDomain, NavigationError, Point};

impl NavigationDomain {
    pub fn get_fragment_at<'a>(
        &self,
        mesh: &'a Mesh,
        point: Point,
    ) -> Result<&'a Fragment, NavigationError> {
        mesh.fragments
            .iter()
            .find(|fragment| {
                (point.x >= fragment.position.x && point.y >= fragment.position.y)
                    && point.x < fragment.position.x + fragment.size.x
                    && point.y < fragment.position.y + fragment.size.y
            })
            .ok_or(NavigationError::FragmentNotFoundAtPoint { point })
    }
}
