use crate::{
    Edge, Fragment, FragmentId, FragmentLink, Mesh, NavigationDomain, Point,
    ReduceFragmentationOperation,
};

impl NavigationDomain {
    pub fn find_defragmentation_candidate(
        &self,
        mesh: &Mesh,
        target_mesh_index: usize,
    ) -> Option<ReduceFragmentationOperation> {
        let mut counter = mesh.counter;

        for link in mesh.links.iter() {
            let fragment_from = self.get_fragment(mesh, link.fragment_from).unwrap();
            let fragment_to = self.get_fragment(mesh, link.fragment_to).unwrap();
            let match_x = fragment_from.position.x == fragment_to.position.x;
            let match_y = fragment_from.position.y == fragment_to.position.y;
            let match_width = fragment_from.size.x == fragment_to.size.x;
            let match_height = fragment_from.size.y == fragment_to.size.y;

            let fragments_to_delete = vec![link.fragment_from, link.fragment_to];

            if (link.contact_edge == Edge::South || link.contact_edge == Edge::North)
                && match_x
                && match_width
            {
                let mut links_to_create = Vec::new();
                counter += 1;
                let fragment = Fragment {
                    id: FragmentId(counter),
                    position: Point {
                        x: fragment_from.position.x,
                        y: fragment_from.position.y.min(fragment_to.position.y),
                    },
                    size: Point {
                        x: fragment_from.size.x,
                        y: fragment_from.size.y + fragment_to.size.y,
                    },
                    obstacle: None,
                };
                for link in self.get_links_from_fragment(mesh, link.fragment_from) {
                    if !fragments_to_delete.contains(&link.fragment_to) {
                        let sibling = self.get_fragment(mesh, link.fragment_to).unwrap();
                        links_to_create.push(FragmentLink::from_contact(
                            link.contact_edge,
                            &fragment,
                            &sibling,
                        ));
                    }
                }
                for link in self.get_links_from_fragment(mesh, link.fragment_to) {
                    if !fragments_to_delete.contains(&link.fragment_to) {
                        let sibling = self.get_fragment(mesh, link.fragment_to).unwrap();
                        links_to_create.push(FragmentLink::from_contact(
                            link.contact_edge,
                            &fragment,
                            &sibling,
                        ));
                    }
                }
                return Some(ReduceFragmentationOperation {
                    counter,
                    target_mesh_index,
                    fragments_to_delete,
                    fragment,
                    links_to_create,
                });
            }

            if (link.contact_edge == Edge::East || link.contact_edge == Edge::West)
                && match_y
                && match_height
            {
                let mut links_to_create = Vec::new();
                counter += 1;
                let fragment = Fragment {
                    id: FragmentId(counter),
                    position: Point {
                        x: fragment_from.position.x.min(fragment_to.position.x),
                        y: fragment_from.position.y,
                    },
                    size: Point {
                        x: fragment_from.size.x + fragment_to.size.x,
                        y: fragment_from.size.y,
                    },
                    obstacle: None,
                };
                for link in self.get_links_from_fragment(mesh, link.fragment_from) {
                    if !fragments_to_delete.contains(&link.fragment_to) {
                        let sibling = self.get_fragment(mesh, link.fragment_to).unwrap();
                        links_to_create.push(FragmentLink::from_contact(
                            link.contact_edge,
                            &fragment,
                            &sibling,
                        ));
                    }
                }
                for link in self.get_links_from_fragment(mesh, link.fragment_to) {
                    if !fragments_to_delete.contains(&link.fragment_to) {
                        let sibling = self.get_fragment(mesh, link.fragment_to).unwrap();
                        links_to_create.push(FragmentLink::from_contact(
                            link.contact_edge,
                            &fragment,
                            &sibling,
                        ));
                    }
                }
                return Some(ReduceFragmentationOperation {
                    counter,
                    target_mesh_index,
                    fragments_to_delete,
                    fragment,
                    links_to_create,
                });
            }
        }

        None
    }
}
