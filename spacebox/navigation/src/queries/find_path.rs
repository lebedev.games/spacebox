use std::borrow::Borrow;
use std::rc::Rc;

use crate::{
    is_intersect_segment, FragmentLink, Mesh, NavigationDomain, NavigationError, Node, PathPoint,
    Point,
};

impl NavigationDomain {
    pub fn find_path(
        &self,
        mesh: &Mesh,
        source: PathPoint,
        destination: PathPoint,
    ) -> Result<Vec<PathPoint>, NavigationError> {
        self.ensure_in_bounds(source)?;
        self.ensure_in_bounds(destination)?;

        let source_fragment = self.get_fragment_at(mesh, source.into())?;
        let destination_fragment = self.get_fragment_at(mesh, destination.into())?;

        let mut open_list: Vec<Node> = Vec::new();
        let mut visited_waypoints: Vec<Point> = Vec::new();

        open_list.push(Node {
            parent: None,
            path_length: 0.0,
            score: 0.0,
            fragment: source_fragment.id,
            point: source,
            waypoint: source.into(),
            segment: None,
        });

        let mut calculations_limiter = 1000;

        while !open_list.is_empty() && calculations_limiter > 0 {
            calculations_limiter -= 1;

            let mut current_node_index = 0;
            let mut min_score = f32::MAX;
            for (index, node) in open_list.iter().enumerate() {
                if node.score < min_score {
                    current_node_index = index;
                    min_score = node.score;
                }
            }
            let current_node = open_list[current_node_index].clone();

            open_list.remove(current_node_index);

            visited_waypoints.push(current_node.waypoint);

            if current_node.fragment == destination_fragment.id {
                let mut current = &current_node;
                let mut path = Vec::new();
                let destination_node = Node {
                    parent: None,
                    path_length: 0.0,
                    score: 0.0,
                    fragment: destination_fragment.id,
                    point: destination,
                    waypoint: destination.into(),
                    segment: None,
                };
                path.push(&destination_node);
                while calculations_limiter > 0 {
                    calculations_limiter -= 1;
                    match &current.parent {
                        Some(parent) => {
                            path.push(current);
                            current = parent.borrow();
                        }
                        None => break,
                    }
                }
                let source_node = Node {
                    parent: None,
                    path_length: 0.0,
                    score: 0.0,
                    fragment: source_fragment.id,
                    point: source,
                    waypoint: source.into(),
                    segment: None,
                };
                path.push(&source_node);
                let graph = path.into_iter().rev().collect();
                let path = smooth_path(graph);
                return Ok(path);
            }

            // convert links to directed from fragment
            let mut current_node_links = vec![];
            for link in self.get_links_from_fragment(mesh, current_node.fragment) {
                current_node_links.push(FragmentLink {
                    fragment_from: link.fragment_from,
                    fragment_to: link.fragment_to,
                    contact_edge: link.contact_edge,
                    contact_segment: link.contact_segment,
                })
            }
            for link in self.get_links_to_fragment(mesh, current_node.fragment) {
                current_node_links.push(FragmentLink {
                    fragment_from: link.fragment_to,
                    fragment_to: link.fragment_from,
                    contact_edge: link.contact_edge.opposite(),
                    contact_segment: link.contact_segment,
                })
            }

            for link in current_node_links {
                let next_fragment = self.get_fragment(mesh, link.fragment_to)?;

                if next_fragment.obstacle.is_some() {
                    continue;
                }

                let waypoints = vec![link.contact_segment.begin, link.contact_segment.end];

                for waypoint in waypoints {
                    if visited_waypoints.iter().any(|point| point == &waypoint) {
                        continue;
                    }

                    let point = waypoint.into();

                    let path_length =
                        &current_node.path_length + &current_node.point.get_distance(point);
                    let heuristic = destination.get_distance(point);
                    let score = path_length + heuristic;

                    if open_list
                        .iter()
                        .any(|node| node.waypoint == waypoint && path_length > node.path_length)
                    {
                        continue;
                    }

                    open_list.push(Node {
                        parent: Some(Rc::new(current_node.clone())),
                        path_length,
                        score,
                        fragment: next_fragment.id,
                        point,
                        waypoint,
                        segment: Some(link.contact_segment),
                    });
                }
            }
        }

        Err(NavigationError::PathNotFound)
    }
}

fn smooth_path(path: Vec<&Node>) -> Vec<PathPoint> {
    let mut result = Vec::new();
    let mut index = 0;
    while index < path.len() {
        let current = path[index];
        result.push(current.point);
        index += 1;

        // current:0 -> next:1 ? skip -> candidate:2
        let mut candidate = index + 1;
        while candidate < path.len() {
            let skip = path[candidate - 1];
            let to = path[candidate];
            if let Some(segment) = skip.segment {
                // Simplify path in place, skip horizontal or vertical line fragmentation
                // points. This will reduce segment intersection calculations.
                let linear = current.point.x == to.point.x || current.point.y == to.point.y;
                if linear || is_intersect_segment(current.point, to.point, segment) {
                    index = candidate;
                    candidate += 1;
                    continue;
                }
            }
            break;
        }
    }
    result
}
