use crate::{NavigationDomain, NavigationError, NavigatorId, PathPoint, RouteStatus};

impl NavigationDomain {
    pub fn validate_route(
        &self,
        navigator: NavigatorId,
        destination: PathPoint,
    ) -> Result<RouteStatus, NavigationError> {
        let navigator = self.get_navigator(navigator)?;
        let status = match navigator.path {
            Some(ref path) => {
                let end = path.iter().last();
                if end == Some(&destination) {
                    if end == Some(&navigator.location) {
                        RouteStatus::RouteComplete
                    } else {
                        RouteStatus::RouteInProgress
                    }
                } else {
                    RouteStatus::RouteNotStarted
                }
            }
            None => RouteStatus::RouteNotStarted,
        };

        Ok(status)
    }
}
