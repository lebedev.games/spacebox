use crate::{Fragment, FragmentId, Mesh, NavigationDomain, NavigationError};

impl NavigationDomain {
    pub fn get_fragment<'a>(
        &self,
        mesh: &'a Mesh,
        id: FragmentId,
    ) -> Result<&'a Fragment, NavigationError> {
        mesh.fragments
            .iter()
            .find(|fragment| fragment.id == id)
            .ok_or(NavigationError::FragmentNotFound { id })
    }
}
