use crate::{Mesh, NavigationDomain, NavigationError, Obstacle, ObstacleId};

impl NavigationDomain {
    pub fn get_obstacle<'a>(
        &self,
        mesh: &'a Mesh,
        obstacle_id: ObstacleId,
    ) -> Result<&'a Obstacle, NavigationError> {
        mesh.obstacles
            .iter()
            .find(|obstacle| obstacle.id == obstacle_id)
            .ok_or(NavigationError::ObstacleNotFound)
    }
}
