use crate::NavigationError::{NavigatorNotFound, NavigatorTypeNotFound};
use crate::{
    Mesh, MeshId, MeshKey, MeshType, NavigationDomain, NavigationError, Navigator, NavigatorId,
    NavigatorKey, NavigatorType, ObstacleKey, ObstacleType,
};

impl NavigationDomain {
    pub fn index_navigator(&self, id: NavigatorId) -> Result<usize, NavigationError> {
        self.state
            .navigators
            .iter()
            .position(|navigator| navigator.id == id)
            .ok_or(NavigatorNotFound { id })
    }

    pub fn get_navigator(&self, id: NavigatorId) -> Result<&Navigator, NavigationError> {
        self.state
            .navigators
            .iter()
            .find(|navigator| navigator.id == id)
            .ok_or(NavigatorNotFound { id })
    }

    pub fn get_navigator_type(
        &self,
        key: &NavigatorKey,
    ) -> Result<&NavigatorType, NavigationError> {
        self.database
            .navigators
            .iter()
            .find(|navigator| &navigator.key == key)
            .ok_or(NavigatorTypeNotFound { key: key.clone() })
    }

    pub fn get_obstacle_type(&self, key: &ObstacleKey) -> Result<&ObstacleType, NavigationError> {
        self.database
            .obstacles
            .iter()
            .find(|obstacle| &obstacle.key == key)
            .ok_or(NavigationError::ObstacleTypeNotFound { key: key.clone() })
    }

    pub fn index_mesh(&self, id: MeshId) -> Result<usize, NavigationError> {
        self.state
            .meshes
            .iter()
            .position(|mesh| mesh.id == id)
            .ok_or(NavigationError::MeshNotFound { id })
    }

    pub fn get_mesh_type(&self, key: &MeshKey) -> Result<&MeshType, NavigationError> {
        self.database
            .meshes
            .iter()
            .find(|mesh| &mesh.key == key)
            .ok_or(NavigationError::MeshTypeNotFound { key: key.clone() })
    }

    pub fn get_mesh(&self, id: MeshId) -> Result<&Mesh, NavigationError> {
        self.state
            .meshes
            .iter()
            .find(|mesh| mesh.id == id)
            .ok_or(NavigationError::MeshNotFound { id })
    }
}
