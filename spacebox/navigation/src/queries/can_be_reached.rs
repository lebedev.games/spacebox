use crate::{MeshId, NavigationDomain, PathPoint};

impl NavigationDomain {
    pub fn can_be_reached(&self, mesh: MeshId, point: PathPoint) -> bool {
        match self.get_mesh(mesh) {
            Ok(mesh) => match self.get_fragment_at(mesh, point.into()) {
                Ok(fragment) => fragment.obstacle.is_none(),
                Err(error) => {
                    error!(
                        "Unable to determine can be reached {:?}, {:?}",
                        point, error
                    );
                    false
                }
            },
            Err(error) => {
                error!(
                    "Unable to determine can be reached {:?}, {:?}",
                    point, error
                );
                false
            }
        }
    }
}
