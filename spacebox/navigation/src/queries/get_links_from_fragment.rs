use crate::{FragmentId, FragmentLink, Mesh, NavigationDomain};

impl NavigationDomain {
    pub fn get_links_from_fragment<'a>(
        &self,
        mesh: &'a Mesh,
        fragment: FragmentId,
    ) -> Vec<&'a FragmentLink> {
        mesh.links
            .iter()
            .filter(|link| link.fragment_from == fragment)
            .collect()
    }
}
