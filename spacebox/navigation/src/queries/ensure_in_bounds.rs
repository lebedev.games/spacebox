use crate::{NavigationDomain, NavigationError, PathPoint, Point};

impl NavigationDomain {
    pub fn ensure_in_bounds(&self, point: PathPoint) -> Result<Point, NavigationError> {
        if point.x < 0.0 || point.y < 0.0 {
            return Err(NavigationError::PointOutOfMesh);
        }

        return Ok(point.into());
    }
}
