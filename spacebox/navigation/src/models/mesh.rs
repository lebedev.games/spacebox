use serde::{Deserialize, Serialize};

use crate::{Fragment, FragmentLink, Obstacle, Point};

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq, Eq, Hash)]
pub struct MeshId(pub usize);

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct MeshKey(pub String);

#[derive(Debug, Serialize, Deserialize)]
pub struct MeshType {
    pub key: MeshKey,
    pub size: Point,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct MeshDef {
    pub id: MeshId,
    pub key: MeshKey,
    pub size: Point,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Mesh {
    pub id: MeshId,
    pub key: MeshKey,
    pub counter: usize,
    pub size: Point,
    pub fragments: Vec<Fragment>,
    pub links: Vec<FragmentLink>,
    pub obstacles: Vec<Obstacle>,
}

impl From<usize> for MeshId {
    fn from(value: usize) -> Self {
        Self(value)
    }
}

impl From<&str> for MeshKey {
    fn from(value: &str) -> Self {
        Self(value.into())
    }
}
