use std::ops;
use std::rc::Rc;

use serde::{Deserialize, Serialize};

use crate::{FragmentId, Point, Segment};

#[derive(Debug, Clone)]
pub struct Node {
    pub parent: Option<Rc<Node>>,
    pub path_length: f32,
    pub score: f32,
    pub fragment: FragmentId,
    pub point: PathPoint,
    pub waypoint: Point,
    pub segment: Option<Segment>,
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq)]
pub struct PathPoint {
    pub x: f32,
    pub y: f32,
}

impl PathPoint {
    pub fn new(x: f32, y: f32) -> Self {
        Self { x, y }
    }

    pub fn get_distance(&self, destination: PathPoint) -> f32 {
        let (ax, ay) = (self.x, self.y);
        let (bx, by) = (destination.x, destination.y);
        let x = bx - ax;
        let y = by - ay;
        (x * x + y * y).sqrt()
    }
}

impl From<(f32, f32)> for PathPoint {
    fn from(value: (f32, f32)) -> Self {
        Self {
            x: value.0,
            y: value.1,
        }
    }
}

impl From<PathPoint> for Point {
    fn from(point: PathPoint) -> Self {
        Point {
            x: point.x as usize,
            y: point.y as usize,
        }
    }
}

impl From<Point> for PathPoint {
    fn from(point: Point) -> Self {
        PathPoint {
            x: point.x as f32,
            y: point.y as f32,
        }
    }
}

impl ops::Add<PathPoint> for PathPoint {
    type Output = PathPoint;

    fn add(self, other: PathPoint) -> PathPoint {
        PathPoint {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl ops::Sub<PathPoint> for PathPoint {
    type Output = PathPoint;

    fn sub(self, other: PathPoint) -> Self::Output {
        PathPoint {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl ops::AddAssign<PathPoint> for PathPoint {
    fn add_assign(&mut self, other: PathPoint) {
        self.x += other.x;
        self.y += other.y;
    }
}
