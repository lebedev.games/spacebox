use serde::{Deserialize, Serialize};

use crate::{PathPoint, Point};

#[derive(Debug, Copy, Clone, Serialize, Deserialize, Eq, PartialEq)]
pub struct Segment {
    pub begin: Point,
    pub end: Point,
}

impl Segment {
    pub fn new(ax: usize, ay: usize, bx: usize, by: usize) -> Self {
        Segment {
            begin: Point::new(ax, ay),
            end: Point::new(bx, by),
        }
    }
}

fn ccw(a: PathPoint, b: PathPoint, c: PathPoint) -> bool {
    (c.y - a.y) * (b.x - a.x) > (b.y - a.y) * (c.x - a.x)
}

pub fn is_intersect_segment(from: PathPoint, to: PathPoint, segment: Segment) -> bool {
    intersect_segments(from, to, segment.begin.into(), segment.end.into())
}

fn intersect_segments(a: PathPoint, b: PathPoint, c: PathPoint, d: PathPoint) -> bool {
    ccw(a, c, d) != ccw(b, c, d) && ccw(a, b, c) != ccw(a, b, d)
}
