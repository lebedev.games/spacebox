use serde::{Deserialize, Serialize};

use crate::{MeshId, PathPoint};

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq, Eq, Hash)]
pub struct NavigatorId(pub usize);

#[derive(Debug, Clone, Eq, Serialize, Deserialize, PartialEq)]
pub struct NavigatorKey(pub String);

#[derive(Debug, Serialize, Deserialize)]
pub struct NavigatorType {
    pub key: NavigatorKey,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct Navigator {
    pub id: NavigatorId,
    pub key: NavigatorKey,
    pub location: PathPoint,
    pub mesh: MeshId,
    pub path: Option<Vec<PathPoint>>,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum RouteStatus {
    RouteComplete,
    RouteInProgress,
    RouteNotStarted,
}

impl From<usize> for NavigatorId {
    fn from(value: usize) -> Self {
        NavigatorId(value)
    }
}

impl From<&str> for NavigatorKey {
    fn from(value: &str) -> Self {
        Self(value.into())
    }
}
