use serde::{Deserialize, Serialize};

use crate::{Fragment, FragmentId, Point, Segment};

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq)]
pub enum Edge {
    North,
    South,
    West,
    East,
}

impl Edge {
    pub fn opposite(&self) -> Edge {
        match self {
            Edge::North => Edge::South,
            Edge::South => Edge::North,
            Edge::West => Edge::East,
            Edge::East => Edge::West,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct FragmentLink {
    pub fragment_from: FragmentId,
    pub fragment_to: FragmentId,
    pub contact_edge: Edge,
    pub contact_segment: Segment,
}

impl FragmentLink {
    pub fn new(
        fragment_from: FragmentId,
        fragment_to: FragmentId,
        contact_edge: Edge,
        contact_segment: Segment,
    ) -> Self {
        FragmentLink {
            fragment_from,
            fragment_to,
            contact_edge,
            contact_segment,
        }
    }

    pub fn from_contact(
        contact_edge: Edge,
        current_fragment: &Fragment,
        next_fragment: &Fragment,
    ) -> Self {
        let contact_segment = match contact_edge {
            Edge::East => Segment {
                // bottom
                begin: Point {
                    x: next_fragment.position.x,
                    y: current_fragment.position.y.max(next_fragment.position.y),
                },
                // top
                end: Point {
                    x: next_fragment.position.x,
                    y: (current_fragment.position.y + current_fragment.size.y)
                        .min(next_fragment.position.y + next_fragment.size.y),
                },
            },
            Edge::West => Segment {
                // bottom
                begin: Point {
                    x: current_fragment.position.x,
                    y: current_fragment.position.y.max(next_fragment.position.y),
                },
                // top
                end: Point {
                    x: current_fragment.position.x,
                    y: (current_fragment.position.y + current_fragment.size.y)
                        .min(next_fragment.position.y + next_fragment.size.y),
                },
            },
            Edge::North => Segment {
                // left
                begin: Point {
                    x: current_fragment.position.x.max(next_fragment.position.x),
                    y: next_fragment.position.y,
                },
                // right
                end: Point {
                    x: (current_fragment.position.x + current_fragment.size.x)
                        .min(next_fragment.position.x + next_fragment.size.x),
                    y: next_fragment.position.y,
                },
            },
            Edge::South => Segment {
                // left
                begin: Point {
                    x: current_fragment.position.x.max(next_fragment.position.x),
                    y: current_fragment.position.y,
                },
                // right
                end: Point {
                    x: (current_fragment.position.x + current_fragment.size.x)
                        .min(next_fragment.position.x + next_fragment.size.x),
                    y: current_fragment.position.y,
                },
            },
        };
        FragmentLink {
            fragment_from: current_fragment.id,
            fragment_to: next_fragment.id,
            contact_edge,
            contact_segment,
        }
    }
}
