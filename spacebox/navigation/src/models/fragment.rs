use serde::{Deserialize, Serialize};

use crate::{ObstacleId, Point};

#[derive(Debug, Copy, Clone, Serialize, Deserialize, Hash, PartialEq, Eq)]
pub struct FragmentId(pub usize);

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct Fragment {
    pub id: FragmentId,
    pub position: Point,
    pub size: Point,
    pub obstacle: Option<ObstacleId>,
}
