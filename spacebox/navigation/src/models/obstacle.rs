use serde::{Deserialize, Serialize};

use crate::{MeshId, PathPoint, Point};

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq, Eq, Hash)]
pub struct ObstacleId(pub usize);

#[derive(Default, Debug, Clone, Eq, Serialize, Deserialize, PartialEq)]
pub struct ObstacleKey(pub String);

#[derive(Debug, Serialize, Deserialize)]
pub struct ObstacleType {
    pub key: ObstacleKey,
    pub size: Point,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct Obstacle {
    pub id: ObstacleId,
    pub key: ObstacleKey,
    pub location: PathPoint,
    pub rotation: bool,
    pub size: Point,
    pub mesh: MeshId,
}

#[inline]
pub fn create_bounds_from(location: PathPoint, size: Point, rotation: bool) -> (Point, Point) {
    let (width, height) = if rotation {
        (size.y, size.x)
    } else {
        (size.x, size.y)
    };
    let offset = PathPoint {
        x: width as f32 / 2.0,
        y: height as f32 / 2.0,
    };
    let min = location - offset;
    let max = location + offset;
    (min.into(), max.into())
}

impl Obstacle {
    pub fn bounds(&self) -> (Point, Point) {
        create_bounds_from(self.location, self.size, self.rotation)
    }
}

impl From<usize> for ObstacleId {
    fn from(value: usize) -> Self {
        Self(value)
    }
}

impl From<&str> for ObstacleKey {
    fn from(value: &str) -> Self {
        Self(value.into())
    }
}
