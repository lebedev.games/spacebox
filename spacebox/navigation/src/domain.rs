use std::rc::Rc;

use crate::{NavigationDatabase, NavigationState};

#[derive(Default)]
pub struct NavigationDomain {
    pub database: Rc<NavigationDatabase>,
    pub state: NavigationState,
}
