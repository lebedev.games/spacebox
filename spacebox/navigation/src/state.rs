use serde::{Deserialize, Serialize};

use crate::{Mesh, Navigator};

#[derive(Debug, Clone, Serialize, Deserialize, Default)]
pub struct NavigationState {
    pub meshes: Vec<Mesh>,
    pub navigators: Vec<Navigator>,
}
