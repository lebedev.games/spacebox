## Game Scenes Representation

### Classic Approaches

#### Grids

Grids are simple and easy to understand, and many games use them for world representation.
The grid is robust, easy to implement, and can be easily integrated 
with discrete search methods such as A* or Breadth First Search.

The quality and speed of path finding are highly dependent on grid resolution. 
Game actions take place both strategic scenes (star systems) and local scenes (character movement).
Unfortunately, the problem of choosing and adapting a suitable grid resolution 
in different game scenes may be unsolvable in itself, 
or it will take a lot of time without guarantees. 

#### Navigation Meshes

Representation of the game world in the form of polygons. 
This is a more efficient method, because it is conventionally not limited by resolution.

The speed of dynamic changes, such as the creation or remove obstacles,
is very important for the game server to quickly respond to the requests of the game actors: 
 
* actions and game experience of the players 
* actions and update of AI agents 

Unfortunately, math calculations on polygons is still an expensive operation for 
a large number of scenes.

### Current Solution

The advantages of grids and navigation meshes can be realized 
in a structure similar to the "[Quadtree](https://en.wikipedia.org/wiki/Quadtree)" 
that represents the game scenes into fragments - rectangular regions of arbitrary sizes.

The disadvantages of this approach:

* does not provide the accuracy of the navigation mesh
* calculation speed depends on the selected resolution (the minimum size of one fragment) 

But in general, the compromise is acceptable, given requirements for gameplay.

#### Structure

Four navigation fragments represents scene regions and contain information 
about possible links on the corresponding edges: north, south, east, west.
Another fragment represents the obstacle itself.

![.readme/fig1.png](.readme/fig1.png)

#### Local Changes

Creation of obstacle is, in most cases, constant five operations of 
creating new fragments and deleting the parent in which the obstacle is located.

If an obstacle overlaps several fragments at once, the obstacle splits to parts,
corresponding to the overlapping fragments. 
At the same time, a pointer to the original obstacle is saved 
to consistent remove of obstacle in future.

![.readme/fig2.png](.readme/fig2.png)

#### Defragmentation

All obstacle creation or removal will fragment the original navigation mesh.
The result will be an increase in the number of fragments processed, 
which can slow down algorithms and in the worst case lead to performance
equal to the performance of a grid-based solution with same resolution.

Defragmentation is a "healing" process when navigation fragments 
of the same size vertically or horizontally are merged into one.

Depending on the changes, you can adjust the frequency and depth of defragmentation.
This important property of the current solution allows to increase
the respond time of the game server at high loaded game processes,
in order to then perform optimization at the times of the least load.

![.readme/fig3.png](.readme/fig3.png)

## Path Finding Algorithm

#### A*

The most popular choice for tree search tasks,
because it is flexible and can be used in different contexts, 
in particular, in planning the movement of the game character on the scene.

The algorithm works on a graph, it is a set of nodes and a set of edges,
that is, connections between pairs of nodes. 
Current solution can easily be in the form of a graph.
Each fragment is node, the links between the fragments are edges.

To calculate the actual length of the traversed path, 
we can use one or several points of fragment:
 
* contact point of fragments
* center of fragment
* fragment vertices
* ...

Depending on the selected points, the search accuracy, 
path smoothness and calculation performance may change.

![.readme/fig4.png](.readme/fig4.png)