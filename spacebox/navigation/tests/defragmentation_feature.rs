mod testing;

use navigation::{Edge, Fragment, FragmentId, FragmentLink, Point, Segment};
pub use testing::*;

#[test]
fn test_reduce_fragmentation_after_square_obstacle_level_1() {
    NavigationDomainTestScenario::new()
        // Arrange
        .given_mesh_with_type("mesh", 10, 10)
        .given_obstacle_type("obstacle_type", 2, 2)
        // Act
        .when_create_obstacle("mesh", "obstacle_type", "obstacle", (3.0, 3.0))
        .when_remove_obstacle("mesh", "obstacle")
        .when_reduce_fragmentation()
        // Assert
        .then_mesh_links_count_should_be("mesh", 5)
        .then_mesh_obstacles_count_should_be("mesh", 0)
        .then_mesh_fragments_count_should_be("mesh", 4)
        .then_fragment_should_be("mesh", 3, |_| Fragment {
            id: FragmentId(6),
            position: Point::new(2, 2),
            size: Point::new(2, 8),
            obstacle: None,
        })
        .then_fragment_links_should_be(
            "mesh",
            FragmentId(6),
            vec![
                &FragmentLink::new(
                    FragmentId(6),
                    FragmentId(2),
                    Edge::South,
                    Segment::new(2, 2, 4, 2),
                ),
                &FragmentLink::new(
                    FragmentId(6),
                    FragmentId(1),
                    Edge::West,
                    Segment::new(2, 2, 2, 10),
                ),
                &FragmentLink::new(
                    FragmentId(6),
                    FragmentId(3),
                    Edge::East,
                    Segment::new(4, 2, 4, 10),
                ),
            ],
        );
}

#[test]
fn test_reduce_fragmentation_after_square_obstacle_level_2() {
    NavigationDomainTestScenario::new()
        // Arrange
        .given_mesh_with_type("mesh", 10, 10)
        .given_obstacle_type("obstacle_type", 2, 2)
        // Act
        .when_create_obstacle("mesh", "obstacle_type", "obstacle", (3.0, 3.0))
        .when_remove_obstacle("mesh", "obstacle")
        .when_reduce_fragmentation()
        .when_reduce_fragmentation()
        // Assert
        .then_mesh_links_count_should_be("mesh", 3)
        .then_mesh_obstacles_count_should_be("mesh", 0)
        .then_mesh_fragments_count_should_be("mesh", 3)
        .then_fragment_should_be("mesh", 2, |_| Fragment {
            id: FragmentId(7),
            position: Point::new(2, 2),
            size: Point::new(8, 8),
            obstacle: None,
        })
        .then_fragment_links_should_be(
            "mesh",
            FragmentId(7),
            vec![
                &FragmentLink::new(
                    FragmentId(7),
                    FragmentId(2),
                    Edge::South,
                    Segment::new(2, 2, 10, 2),
                ),
                &FragmentLink::new(
                    FragmentId(7),
                    FragmentId(1),
                    Edge::West,
                    Segment::new(2, 2, 2, 10),
                ),
            ],
        );
}

#[test]
fn test_reduce_fragmentation_after_square_obstacle_level_3() {
    NavigationDomainTestScenario::new()
        // Arrange
        .given_mesh_with_type("mesh", 10, 10)
        .given_obstacle_type("obstacle_type", 2, 2)
        // Act
        .when_create_obstacle("mesh", "obstacle_type", "obstacle", (3.0, 3.0))
        .when_remove_obstacle("mesh", "obstacle")
        .when_reduce_fragmentation()
        .when_reduce_fragmentation()
        .when_reduce_fragmentation()
        // Assert
        .then_mesh_links_count_should_be("mesh", 1)
        .then_mesh_obstacles_count_should_be("mesh", 0)
        .then_mesh_fragments_count_should_be("mesh", 2)
        .then_fragment_should_be("mesh", 1, |_| Fragment {
            id: FragmentId(8),
            position: Point::new(2, 0),
            size: Point::new(8, 10),
            obstacle: None,
        })
        .then_fragment_links_should_be(
            "mesh",
            FragmentId(8),
            vec![&FragmentLink::new(
                FragmentId(8),
                FragmentId(1),
                Edge::West,
                Segment::new(2, 0, 2, 10),
            )],
        );
}
