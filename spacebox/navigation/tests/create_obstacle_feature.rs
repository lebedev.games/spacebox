mod testing;

use navigation::{Edge, Fragment, FragmentId, FragmentLink, Obstacle, PathPoint, Point, Segment};
pub use testing::*;

#[test]
fn test_room_building_scenario() {
    NavigationDomainTestScenario::new()
        // Arrange
        .given_mesh_with_type("mesh", 10, 10)
        .given_obstacle_type("wall_h", 1, 2)
        .given_obstacle_type("wall_v", 2, 1)
        // Act - Assert
        .when_create_obstacle("mesh", "wall_h", "1", (7.0, 2.0))
        .then_mesh_links_count_should_be("mesh", 4)
        .then_mesh_fragments_count_should_be("mesh", 5)
        .when_create_obstacle("mesh", "wall_h", "2", (7.0, 4.0))
        .then_mesh_links_count_should_be("mesh", 4)
        .then_mesh_fragments_count_should_be("mesh", 6)
        .when_create_obstacle("mesh", "wall_h", "3", (7.0, 6.0))
        .then_mesh_links_count_should_be("mesh", 4)
        .then_mesh_fragments_count_should_be("mesh", 7)
        .when_create_obstacle("mesh", "wall_v", "4", (5.0, 7.0))
        .then_mesh_links_count_should_be("mesh", 6)
        .then_mesh_fragments_count_should_be("mesh", 10)
        .when_create_obstacle("mesh", "wall_v", "5", (3.0, 7.0))
        .then_mesh_links_count_should_be("mesh", 8)
        .then_mesh_fragments_count_should_be("mesh", 13)
        .when_create_obstacle("mesh", "wall_v", "6", (5.0, 2.0))
        .then_mesh_links_count_should_be("mesh", 9)
        .then_mesh_fragments_count_should_be("mesh", 15)
        .when_create_obstacle("mesh", "wall_v", "7", (3.0, 2.0))
        .then_mesh_links_count_should_be("mesh", 10)
        .then_mesh_fragments_count_should_be("mesh", 17)
        .when_create_obstacle("mesh", "wall_h", "8", (2.0, 2.0))
        .then_mesh_links_count_should_be("mesh", 12)
        .then_mesh_fragments_count_should_be("mesh", 20)
        .when_create_obstacle("mesh", "wall_h", "9", (2.0, 6.0))
        .then_mesh_links_count_should_be("mesh", 13)
        .then_mesh_fragments_count_should_be("mesh", 22);
}

#[test]
fn test_create_obstacle_covers_two_fragments() {
    NavigationDomainTestScenario::new()
        // Arrange
        .given_mesh_with_type("mesh", 10, 10)
        .given_obstacle_type("chair_type", 1, 1)
        .given_obstacle_type("table_type", 3, 3)
        // Act
        .when_create_obstacle("mesh", "chair_type", "chair", (2.5, 2.5))
        .when_create_obstacle("mesh", "table_type", "table", (3.5, 8.5))
        // Assert
        .then_mesh_links_count_should_be("mesh", 5)
        .then_mesh_obstacles_count_should_be("mesh", 2)
        .then_mesh_fragments_count_should_be("mesh", 8)
        .then_fragment_at_point_should_be("mesh", (2, 7), |it| Fragment {
            id: FragmentId(7),
            position: Point::new(2, 7),
            size: Point::new(1, 3),
            obstacle: Some(it.obstacle("table")),
        })
        .then_fragment_at_point_should_be("mesh", (3, 7), |it| Fragment {
            id: FragmentId(10),
            position: Point::new(3, 7),
            size: Point::new(2, 3),
            obstacle: Some(it.obstacle("table")),
        })
        .then_fragment_links_should_be(
            "mesh",
            FragmentId(1),
            vec![
                &FragmentLink::new(
                    FragmentId(1),
                    FragmentId(2),
                    Edge::East,
                    Segment::new(2, 0, 2, 2),
                ),
                &FragmentLink::new(
                    FragmentId(6),
                    FragmentId(1),
                    Edge::West,
                    Segment::new(2, 3, 2, 7),
                ),
            ],
        )
        .then_fragment_links_should_be(
            "mesh",
            FragmentId(6),
            vec![
                &FragmentLink::new(
                    FragmentId(6),
                    FragmentId(1),
                    Edge::West,
                    Segment::new(2, 3, 2, 7),
                ),
                &FragmentLink::new(
                    FragmentId(8),
                    FragmentId(6),
                    Edge::West,
                    Segment::new(3, 3, 3, 7),
                ),
            ],
        );
}

#[test]
fn test_create_obstacle_on_fragment_top_left_corner() {
    NavigationDomainTestScenario::new()
        // Arrange
        .given_mesh_with_type("mesh", 10, 10)
        .given_obstacle_type("chair_type", 1, 1)
        .given_obstacle_type("table_type", 1, 1)
        // Act
        .when_create_obstacle("mesh", "chair_type", "chair", (2.5, 2.5))
        .when_create_obstacle("mesh", "table_type", "table", (3.5, 9.5))
        // Assert
        .then_mesh_links_count_should_be("mesh", 5)
        .then_mesh_obstacles_count_should_be("mesh", 2)
        .then_mesh_fragments_count_should_be("mesh", 7)
        .then_fragment_at_point_should_be("mesh", (3, 3), |_| Fragment {
            id: FragmentId(6),
            position: Point::new(3, 2),
            size: Point::new(7, 7),
            obstacle: None,
        });
}

#[test]
fn test_create_obstacle_on_full_fragment_width() {
    NavigationDomainTestScenario::new()
        // Arrange
        .given_mesh_with_type("mesh", 10, 10)
        .given_obstacle_type("chair_type", 1, 1)
        .given_obstacle_type("table_type", 1, 1)
        // Act
        .when_create_obstacle("mesh", "chair_type", "chair", (2.5, 2.5))
        .when_create_obstacle("mesh", "table_type", "table", (2.5, 9.5))
        // Assert
        .then_mesh_links_count_should_be("mesh", 4)
        .then_mesh_obstacles_count_should_be("mesh", 2)
        .then_mesh_fragments_count_should_be("mesh", 6)
        .then_fragment_at_point_should_be("mesh", (2, 3), |_| Fragment {
            id: FragmentId(6),
            position: Point::new(2, 3),
            size: Point::new(1, 6),
            obstacle: None,
        });
}

#[test]
fn test_create_obstacle_on_full_fragment() {
    NavigationDomainTestScenario::new()
        // Arrange
        .given_mesh_with_type("mesh", 10, 10)
        .given_obstacle_type("chair_type", 1, 1)
        .given_obstacle_type("table_type", 8, 2)
        // Act
        .when_create_obstacle("mesh", "chair_type", "chair", (2.5, 2.5))
        .when_create_obstacle("mesh", "table_type", "table", (6.0, 1.0))
        // Assert
        .then_mesh_links_count_should_be("mesh", 2)
        .then_mesh_obstacles_count_should_be("mesh", 2)
        .then_mesh_fragments_count_should_be("mesh", 5)
        .then_fragment_at_point_should_be("mesh", (2, 0), |it| Fragment {
            id: FragmentId(6),
            position: Point::new(2, 0),
            size: Point::new(8, 2),
            obstacle: Some(it.obstacle("table")),
        });
}

#[test]
fn test_create_second_obstacle() {
    NavigationDomainTestScenario::new()
        // Arrange
        .given_mesh_with_type("mesh", 10, 10)
        .given_obstacle_type("obstacle_type", 1, 1)
        // Act
        .when_create_obstacle("mesh", "obstacle_type", "first", (2.5, 2.5))
        .when_create_obstacle("mesh", "obstacle_type", "second", (5.5, 4.5))
        // Assert
        .then_mesh_links_count_should_be("mesh", 9)
        .then_mesh_obstacles_count_should_be("mesh", 2)
        .then_mesh_fragments_count_should_be("mesh", 9)
        .then_fragment_at_point_should_be("mesh", (2, 2), |it| Fragment {
            id: FragmentId(5),
            position: Point::new(2, 2),
            size: Point::new(1, 1),
            obstacle: Some(it.obstacle("first")),
        })
        .then_fragment_at_point_should_be("mesh", (5, 4), |it| Fragment {
            id: FragmentId(10),
            position: Point::new(5, 4),
            size: Point::new(1, 1),
            obstacle: Some(it.obstacle("second")),
        })
        .then_fragment_at_point_should_be("mesh", (2, 3), |_| Fragment {
            id: FragmentId(4),
            position: Point::new(2, 3),
            size: Point::new(1, 7),
            obstacle: None,
        })
        .then_fragment_at_point_should_be("mesh", (3, 3), |_| Fragment {
            id: FragmentId(6),
            position: Point::new(3, 2),
            size: Point::new(2, 8),
            obstacle: None,
        });
}

#[test]
fn test_fragmentation_on_horizontal_of_simple_obstacles() {
    // Arrange
    let mut scenario = NavigationDomainTestScenario::new()
        .given_mesh_with_type("mesh", 100, 100)
        .given_obstacle_type("obstacle_type", 1, 1);

    // Act
    for i in 1..49 {
        scenario = scenario.when_create_obstacle(
            "mesh",
            "obstacle_type",
            &i.to_string(),
            (i as f32 * 2.0, 50.0),
        );
    }

    // Assert
    scenario
        .then_mesh_obstacles_count_should_be("mesh", 48)
        .then_mesh_fragments_count_should_be("mesh", 146) // x 3
        .then_mesh_links_count_should_be("mesh", 145); // x 3
}

#[test]
fn test_fragmentation_on_vertical_of_simple_obstacles() {
    // Arrange
    let mut scenario = NavigationDomainTestScenario::new()
        .given_mesh_with_type("mesh", 100, 100)
        .given_obstacle_type("obstacle_type", 1, 1);

    // Act
    for i in 1..49 {
        scenario = scenario.when_create_obstacle(
            "mesh",
            "obstacle_type",
            &i.to_string(),
            (50.0, i as f32 * 2.0),
        );
    }

    // Assert
    scenario
        .then_mesh_obstacles_count_should_be("mesh", 48)
        .then_mesh_fragments_count_should_be("mesh", 99) // x 2
        .then_mesh_links_count_should_be("mesh", 98); // x 2
}

#[test]
fn test_fragmentation_on_diagonal_of_simple_obstacles() {
    // Arrange
    let mut scenario = NavigationDomainTestScenario::new()
        .given_mesh_with_type("mesh", 100, 100)
        .given_obstacle_type("obstacle_type", 1, 1);

    // Act
    for i in 1..49 {
        scenario = scenario.when_create_obstacle(
            "mesh",
            "obstacle_type",
            &i.to_string(),
            (i as f32 * 2.0, i as f32 * 2.0),
        );
    }

    // Assert
    scenario
        .then_mesh_obstacles_count_should_be("mesh", 48)
        .then_mesh_fragments_count_should_be("mesh", 193) // x 4
        .then_mesh_links_count_should_be("mesh", 239); // 5
}

#[test]
fn test_fragmentation_on_rock_village() {
    NavigationDomainTestScenario::new()
        // Arrange
        .given_mesh_with_type("mesh", 100, 100)
        .given_obstacle_type("rock", 1, 1)
        .when_create_house_from("mesh", "rock", 10, 10, 10)
        .when_create_house_from("mesh", "rock", 23, 10, 10)
        .when_create_house_from("mesh", "rock", 17, 42, 23)
        .when_create_house_from("mesh", "rock", 40, 15, 19)
        .when_create_house_from("mesh", "rock", 60, 60, 7)
        // Assert
        .then_mesh_obstacles_count_should_be("mesh", 236)
        .then_mesh_fragments_count_should_be("mesh", 377) // x 1.5
        .then_mesh_links_count_should_be("mesh", 181); // x 0.7
}

#[test]
fn test_create_rectangle_rotated_obstacle() {
    NavigationDomainTestScenario::new()
        // Arrange
        .given_mesh_with_type("mesh", 10, 10)
        .given_obstacle_type("obstacle_type", 3, 2)
        // Act
        .when_create_obstacle_with_rotation("mesh", "obstacle_type", "obstacle", (3.0, 3.5))
        // Assert
        .then_mesh_links_count_should_be("mesh", 4)
        .then_mesh_obstacles_count_should_be("mesh", 1)
        .then_mesh_fragments_count_should_be("mesh", 5)
        .then_obstacle_should_be("mesh", "obstacle", |it| Obstacle {
            id: it.obstacle("obstacle"),
            key: "obstacle_type".into(),
            location: (3.0, 3.5).into(),
            size: Point::new(3, 2),
            mesh: it.mesh("mesh"),
            rotation: true,
        })
        .then_fragment_should_be("mesh", 0, |_| Fragment {
            id: FragmentId(1),
            position: Point::new(0, 0),
            size: Point::new(2, 10),
            obstacle: None,
        })
        .then_fragment_should_be("mesh", 1, |_| Fragment {
            id: FragmentId(2),
            position: Point::new(2, 0),
            size: Point::new(8, 2),
            obstacle: None,
        })
        .then_fragment_should_be("mesh", 2, |_| Fragment {
            id: FragmentId(3),
            position: Point::new(4, 2),
            size: Point::new(6, 8),
            obstacle: None,
        })
        .then_fragment_should_be("mesh", 3, |_| Fragment {
            id: FragmentId(4),
            position: Point::new(2, 5),
            size: Point::new(2, 5),
            obstacle: None,
        })
        .then_fragment_should_be("mesh", 4, |it| Fragment {
            id: FragmentId(5),
            position: Point::new(2, 2),
            size: Point::new(2, 3),
            obstacle: Some(it.obstacle("obstacle")),
        });
}

#[test]
fn test_create_rectangle_obstacle() {
    NavigationDomainTestScenario::new()
        // Arrange
        .given_mesh_with_type("mesh", 10, 10)
        .given_obstacle_type("obstacle_type", 3, 2)
        // Act
        .when_create_obstacle("mesh", "obstacle_type", "obstacle", (3.5, 3.0))
        // Assert
        .then_mesh_links_count_should_be("mesh", 4)
        .then_mesh_obstacles_count_should_be("mesh", 1)
        .then_mesh_fragments_count_should_be("mesh", 5)
        .then_obstacle_should_be("mesh", "obstacle", |it| Obstacle {
            id: it.obstacle("obstacle"),
            key: "obstacle_type".into(),
            location: (3.5, 3.0).into(),
            size: Point::new(3, 2),
            mesh: it.mesh("mesh"),
            rotation: false,
        })
        .then_fragment_should_be("mesh", 0, |_| Fragment {
            id: FragmentId(1),
            position: Point::new(0, 0),
            size: Point::new(2, 10),
            obstacle: None,
        })
        .then_fragment_should_be("mesh", 1, |_| Fragment {
            id: FragmentId(2),
            position: Point::new(2, 0),
            size: Point::new(8, 2),
            obstacle: None,
        })
        .then_fragment_should_be("mesh", 2, |_| Fragment {
            id: FragmentId(3),
            position: Point::new(5, 2),
            size: Point::new(5, 8),
            obstacle: None,
        })
        .then_fragment_should_be("mesh", 3, |_| Fragment {
            id: FragmentId(4),
            position: Point::new(2, 4),
            size: Point::new(3, 6),
            obstacle: None,
        })
        .then_fragment_should_be("mesh", 4, |it| Fragment {
            id: FragmentId(5),
            position: Point::new(2, 2),
            size: Point::new(3, 2),
            obstacle: Some(it.obstacle("obstacle")),
        });
}

#[test]
fn test_create_square_obstacle_on_rectangle_mesh() {
    NavigationDomainTestScenario::new()
        // Arrange
        .given_mesh_with_type("mesh", 15, 10)
        .given_obstacle_type("obstacle_type", 1, 1)
        // Act
        .when_create_obstacle("mesh", "obstacle_type", "obstacle", (2.5, 2.5))
        // Assert
        .then_mesh_links_count_should_be("mesh", 4)
        .then_mesh_obstacles_count_should_be("mesh", 1)
        .then_mesh_fragments_count_should_be("mesh", 5)
        .then_obstacle_should_be("mesh", "obstacle", |it| Obstacle {
            id: it.obstacle("obstacle"),
            key: "obstacle_type".into(),
            location: (2.5, 2.5).into(),
            rotation: false,
            size: Point::new(1, 1),
            mesh: it.mesh("mesh"),
        })
        .then_fragment_should_be("mesh", 0, |_| Fragment {
            id: FragmentId(1),
            position: Point::new(0, 0),
            size: Point::new(2, 10),
            obstacle: None,
        })
        .then_fragment_should_be("mesh", 1, |_| Fragment {
            id: FragmentId(2),
            position: Point::new(2, 0),
            size: Point::new(13, 2),
            obstacle: None,
        })
        .then_fragment_should_be("mesh", 2, |_| Fragment {
            id: FragmentId(3),
            position: Point::new(3, 2),
            size: Point::new(12, 8),
            obstacle: None,
        })
        .then_fragment_should_be("mesh", 3, |_| Fragment {
            id: FragmentId(4),
            position: Point::new(2, 3),
            size: Point::new(1, 7),
            obstacle: None,
        })
        .then_fragment_should_be("mesh", 4, |it| Fragment {
            id: FragmentId(5),
            position: Point::new(2, 2),
            size: Point::new(1, 1),
            obstacle: Some(it.obstacle("obstacle")),
        });
}

#[test]
fn test_create_square_obstacle() {
    NavigationDomainTestScenario::new()
        // Arrange
        .given_mesh_with_type("mesh", 10, 10)
        .given_obstacle_type("obstacle_type", 1, 1)
        // Act
        .when_create_obstacle("mesh", "obstacle_type", "obstacle", (2.5, 2.5))
        // Assert
        .then_mesh_links_count_should_be("mesh", 4)
        .then_mesh_obstacles_count_should_be("mesh", 1)
        .then_mesh_fragments_count_should_be("mesh", 5)
        .then_obstacle_should_be("mesh", "obstacle", |it| Obstacle {
            id: it.obstacle("obstacle"),
            key: "obstacle_type".into(),
            location: (2.5, 2.5).into(),
            rotation: false,
            size: Point::new(1, 1),
            mesh: it.mesh("mesh"),
        })
        .then_fragment_should_be("mesh", 0, |_| Fragment {
            id: FragmentId(1),
            position: Point::new(0, 0),
            size: Point::new(2, 10),
            obstacle: None,
        })
        .then_fragment_should_be("mesh", 1, |_| Fragment {
            id: FragmentId(2),
            position: Point::new(2, 0),
            size: Point::new(8, 2),
            obstacle: None,
        })
        .then_fragment_should_be("mesh", 2, |_| Fragment {
            id: FragmentId(3),
            position: Point::new(3, 2),
            size: Point::new(7, 8),
            obstacle: None,
        })
        .then_fragment_should_be("mesh", 3, |_| Fragment {
            id: FragmentId(4),
            position: Point::new(2, 3),
            size: Point::new(1, 7),
            obstacle: None,
        })
        .then_fragment_should_be("mesh", 4, |it| Fragment {
            id: FragmentId(5),
            position: Point::new(2, 2),
            size: Point::new(1, 1),
            obstacle: Some(it.obstacle("obstacle")),
        });
}

#[test]
fn test_create_square_obstacle_high_resolution() {
    NavigationDomainTestScenario::new()
        // Arrange
        .given_mesh_with_type("mesh", 100, 100)
        .given_obstacle_type("obstacle_type", 10, 10)
        // Act
        .when_create_obstacle("mesh", "obstacle_type", "obstacle", (16.0, 16.0))
        // Assert
        .then_mesh_links_count_should_be("mesh", 4)
        .then_mesh_obstacles_count_should_be("mesh", 1)
        .then_mesh_fragments_count_should_be("mesh", 5)
        .then_obstacle_should_be("mesh", "obstacle", |it| Obstacle {
            id: it.obstacle("obstacle"),
            key: "obstacle_type".into(),
            location: PathPoint::new(16.0, 16.0),
            rotation: false,
            size: Point::new(10, 10),
            mesh: it.mesh("mesh"),
        })
        .then_fragment_should_be("mesh", 0, |_| Fragment {
            id: FragmentId(1),
            position: Point::new(0, 0),
            size: Point::new(11, 100),
            obstacle: None,
        })
        .then_fragment_should_be("mesh", 1, |_| Fragment {
            id: FragmentId(2),
            position: Point::new(11, 0),
            size: Point::new(89, 11),
            obstacle: None,
        })
        .then_fragment_should_be("mesh", 2, |_| Fragment {
            id: FragmentId(3),
            position: Point::new(21, 11),
            size: Point::new(79, 89),
            obstacle: None,
        })
        .then_fragment_should_be("mesh", 3, |_| Fragment {
            id: FragmentId(4),
            position: Point::new(11, 21),
            size: Point::new(10, 79),
            obstacle: None,
        })
        .then_fragment_should_be("mesh", 4, |it| Fragment {
            id: FragmentId(5),
            position: Point::new(11, 11),
            size: Point::new(10, 10),
            obstacle: Some(it.obstacle("obstacle")),
        });
}
