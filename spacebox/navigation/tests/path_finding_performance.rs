mod testing;

pub use testing::*;

#[test]
fn test_calculation_performance_of_1000_identical_path_find_operations() {
    NavigationDomainTestScenario::new()
        // Arrange
        .given_mesh_with_room_scene("mesh")
        // Act
        .when_measure_operations(1000, |_, scenario| {
            scenario.when_find_path("mesh", (10.0, 10.0), (19.5, 10.0))
        })
        // Assert
        .then_path_find_execution_time_should_between("mesh", 15.0, 45.0);
}
