mod testing;

use navigation::{Edge, Fragment, FragmentId, FragmentLink, Point, Segment};
pub use testing::*;

#[test]
fn test_remove_one_of_two_obstacle_covers_two_fragments() {
    NavigationDomainTestScenario::new()
        // Arrange
        .given_mesh_with_type("mesh", 10, 10)
        .given_obstacle_type("chair_type", 1, 1)
        .given_obstacle_type("table_type", 3, 3)
        // Act
        .when_create_obstacle("mesh", "chair_type", "chair", (2.5, 2.5))
        .when_create_obstacle("mesh", "table_type", "table", (3.5, 8.5))
        .when_remove_obstacle("mesh", "table")
        // Assert
        .then_mesh_links_count_should_be("mesh", 10)
        .then_mesh_obstacles_count_should_be("mesh", 1)
        .then_mesh_fragments_count_should_be("mesh", 8)
        .then_fragment_at_point_should_be("mesh", (2, 7), |_| Fragment {
            id: FragmentId(7),
            position: Point::new(2, 7),
            size: Point::new(1, 3),
            obstacle: None,
        })
        .then_fragment_at_point_should_be("mesh", (3, 7), |_| Fragment {
            id: FragmentId(10),
            position: Point::new(3, 7),
            size: Point::new(2, 3),
            obstacle: None,
        })
        .then_fragment_links_should_be(
            "mesh",
            FragmentId(7),
            vec![
                &FragmentLink::new(
                    FragmentId(7),
                    FragmentId(6),
                    Edge::South,
                    Segment::new(2, 7, 3, 7),
                ),
                &FragmentLink::new(
                    FragmentId(7),
                    FragmentId(1),
                    Edge::West,
                    Segment::new(2, 7, 2, 10),
                ),
                &FragmentLink::new(
                    FragmentId(7),
                    FragmentId(10),
                    Edge::East,
                    Segment::new(3, 7, 3, 10),
                ),
            ],
        )
        .then_fragment_links_should_be(
            "mesh",
            FragmentId(10),
            vec![
                &FragmentLink::new(
                    FragmentId(10),
                    FragmentId(8),
                    Edge::South,
                    Segment::new(3, 7, 5, 7),
                ),
                &FragmentLink::new(
                    FragmentId(10),
                    FragmentId(9),
                    Edge::East,
                    Segment::new(5, 7, 5, 10),
                ),
                &FragmentLink::new(
                    FragmentId(7),
                    FragmentId(10),
                    Edge::East,
                    Segment::new(3, 7, 3, 10),
                ),
            ],
        );
}

#[test]
fn test_remove_square_obstacle() {
    NavigationDomainTestScenario::new()
        // Arrange
        .given_mesh_with_type("mesh", 10, 10)
        .given_obstacle_type("obstacle_type", 2, 2)
        // Act
        .when_create_obstacle("mesh", "obstacle_type", "obstacle", (3.0, 3.0))
        .when_remove_obstacle("mesh", "obstacle")
        // Assert
        .then_mesh_links_count_should_be("mesh", 8)
        .then_mesh_obstacles_count_should_be("mesh", 0)
        .then_mesh_fragments_count_should_be("mesh", 5)
        .then_fragment_should_be("mesh", 4, |_| Fragment {
            id: FragmentId(5),
            position: Point::new(2, 2),
            size: Point::new(2, 2),
            obstacle: None,
        })
        .then_fragment_links_should_be(
            "mesh",
            FragmentId(5),
            vec![
                &FragmentLink::new(
                    FragmentId(5),
                    FragmentId(4),
                    Edge::North,
                    Segment::new(2, 4, 4, 4),
                ),
                &FragmentLink::new(
                    FragmentId(5),
                    FragmentId(2),
                    Edge::South,
                    Segment::new(2, 2, 4, 2),
                ),
                &FragmentLink::new(
                    FragmentId(5),
                    FragmentId(1),
                    Edge::West,
                    Segment::new(2, 2, 2, 4),
                ),
                &FragmentLink::new(
                    FragmentId(5),
                    FragmentId(3),
                    Edge::East,
                    Segment::new(4, 2, 4, 4),
                ),
            ],
        );
}
