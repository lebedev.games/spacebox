use std::path::Path;
use std::rc::Rc;
use std::time::{Duration, Instant};

use env_logger::Env;
use log::LevelFilter;
use navigation::{
    Edge, Fragment, FragmentId, FragmentLink, Mesh, MeshId, MeshKey, MeshType, NavigationDatabase,
    NavigationDomain, NavigationError, NavigatorId, Obstacle, ObstacleId, ObstacleType, PathPoint,
    Point,
};
use plotly::common::{DashType, Font, HoverInfo, Line, Mode, Position, Title};
use plotly::layout::{Axis, Margin, Shape, ShapeLayer, ShapeLine, ShapeType};
use plotly::{Layout, Plot, Rgba, Scatter};
use testit::*;

pub struct NavigationDomainTestScenario {
    navigation: NavigationDomain,
    path: Result<Vec<PathPoint>, NavigationError>,
    operations_execution_time: Duration,
    named_meshes: NamedMap<MeshId>,
    named_obstacles: NamedMap<ObstacleId>,
    _named_navigators: NamedMap<NavigatorId>,
    counter: usize,
}

impl NavigationDomainTestScenario {
    pub fn new() -> Self {
        let _ = env_logger::Builder::from_env(Env::default())
            .filter_level(LevelFilter::Debug)
            .try_init();

        NavigationDomainTestScenario {
            navigation: NavigationDomain::default(),
            path: Err(NavigationError::PathNotFound),
            operations_execution_time: Duration::new(0, 0),
            named_meshes: Default::default(),
            named_obstacles: Default::default(),
            _named_navigators: Default::default(),
            counter: 0,
        }
    }

    pub fn generate_id(&mut self) -> usize {
        self.counter += 1;
        self.counter
    }

    pub fn mesh(&self, name: &str) -> MeshId {
        self.named_meshes
            .get(name.into())
            .expect(&format!("test named mesh '{}' not specified", name))
            .clone()
    }

    pub fn _navigator(&self, name: &str) -> NavigatorId {
        self._named_navigators
            .get(name.into())
            .expect(&format!("test named navigator '{}' not specified", name))
            .clone()
    }

    pub fn obstacle(&self, name: &str) -> ObstacleId {
        self.named_obstacles
            .get(name.into())
            .expect(&format!("test named obstacle '{}' not specified", name))
            .clone()
    }

    pub fn database(&mut self) -> &mut NavigationDatabase {
        Rc::get_mut(&mut self.navigation.database).unwrap()
    }

    pub fn given_mesh_with_room_scene(self, mesh: &str) -> Self {
        self.given_mesh_with_type(mesh, 20, 20)
            .given_obstacle_type("wall_h", 1, 2)
            .given_obstacle_type("wall_v", 2, 1)
            .when_create_obstacle("mesh", "wall_h", "1", (11.5, 8.0))
            .when_create_obstacle("mesh", "wall_h", "2", (11.5, 10.0))
            .when_create_obstacle("mesh", "wall_h", "3", (11.5, 12.0))
            .when_create_obstacle("mesh", "wall_v", "4", (10.0, 12.5))
            .when_create_obstacle("mesh", "wall_v", "5", (8.0, 12.5))
            .when_create_obstacle("mesh", "wall_v", "6", (10.0, 7.5))
            .when_create_obstacle("mesh", "wall_v", "7", (8.0, 7.5))
            .when_create_obstacle("mesh", "wall_h", "8", (6.5, 8.0))
            .when_create_obstacle("mesh", "wall_h", "9", (6.5, 12.0))
    }

    pub fn given_mesh_with_type(mut self, name: &str, size_x: usize, size_y: usize) -> Self {
        let id = self.generate_id();
        let key = MeshKey(id.to_string());
        self.database().meshes.push(MeshType {
            key: key.clone(),
            size: Point {
                x: size_x,
                y: size_y,
            },
        });
        let mesh = self.navigation.prepare_mesh(id.into(), &key).unwrap();
        self.navigation.create_mesh(mesh);
        self.named_meshes.insert(name.into(), id.into());
        self
    }

    pub fn given_obstacle_type(mut self, key: &str, size_x: usize, size_y: usize) -> Self {
        self.database().obstacles.push(ObstacleType {
            key: key.into(),
            size: Point {
                x: size_x,
                y: size_y,
            },
        });
        self
    }

    pub fn when_create_obstacle(
        mut self,
        mesh: &str,
        key: &str,
        name: &str,
        location: (f32, f32),
    ) -> Self {
        let id = self.generate_id();
        let changeset = self
            .navigation
            .prepare_create_obstacle_operation(
                self.mesh(mesh),
                &key.into(),
                id.into(),
                location.into(),
                false,
            )
            .unwrap();

        self.navigation.create_obstacle(changeset);
        self.named_obstacles.insert(name.into(), id.into());
        self
    }

    pub fn when_create_obstacle_with_rotation(
        mut self,
        mesh: &str,
        key: &str,
        name: &str,
        location: (f32, f32),
    ) -> Self {
        let id = self.generate_id();
        let rotation = true;
        let changeset = self
            .navigation
            .prepare_create_obstacle_operation(
                self.mesh(mesh),
                &key.into(),
                id.into(),
                location.into(),
                rotation,
            )
            .unwrap();

        self.navigation.create_obstacle(changeset);
        self.named_obstacles.insert(name.into(), id.into());
        self
    }

    pub fn when_create_house_from(
        mut self,
        mesh: &str,
        obstacle_type: &str,
        position_x: usize,
        position_y: usize,
        size: usize,
    ) -> Self {
        for x in position_x..position_x + size {
            for y in position_y..position_y + size {
                if (x == position_x
                    || y == position_y
                    || x == position_x + size - 1
                    || y == position_y + size - 1)
                    && x != position_x + size / 2
                    && y != position_y + size / 2
                {
                    self =
                        self.when_create_obstacle(mesh, obstacle_type, "0", (x as f32, y as f32));
                }
            }
        }
        self
    }

    pub fn when_remove_obstacle(mut self, mesh: &str, obstacle: &str) -> Self {
        let changeset = self
            .navigation
            .prepare_obstacle_remove_operation(self.mesh(mesh), self.obstacle(obstacle))
            .unwrap();

        self.navigation.remove_obstacle(changeset);
        self
    }

    pub fn when_reduce_fragmentation(mut self) -> Self {
        self.navigation.reduce_fragmentation();
        self
    }

    pub fn when_measure_operations<F>(mut self, operations: usize, repeat: F) -> Self
    where
        F: Fn(usize, NavigationDomainTestScenario) -> Self,
    {
        let now = Instant::now();
        for operation in 0..operations {
            self = repeat(operation, self)
        }
        self.operations_execution_time = now.elapsed();
        self
    }

    pub fn when_find_path(
        mut self,
        mesh: &str,
        source: (f32, f32),
        destination: (f32, f32),
    ) -> Self {
        let mesh = self.mesh(mesh);
        let mesh = self.navigation.get_mesh(mesh).unwrap();

        self.path = self
            .navigation
            .find_path(mesh, source.into(), destination.into());
        self
    }

    pub fn then_path_find_execution_time_should_between(
        self,
        mesh: &str,
        min_time: f32,
        max_time: f32,
    ) -> Self {
        let reference_time = measure_reference_time(1000, 100);
        let execution_time = self.operations_execution_time.as_secs_f32() / reference_time;
        let mesh = self.mesh(mesh);
        let mesh = self.navigation.get_mesh(mesh).unwrap();

        println!("execution time: {}", execution_time);

        assert_that!(
            between!(execution_time, min_time, max_time),
            render_mesh(
                mesh,
                "./output/test_something.html",
                Some(self.path.unwrap()),
                None
            )
        );
        self
    }

    fn then_assert_no_broken_links(self, id: MeshId) -> Self {
        let mesh = self.navigation.get_mesh(id).unwrap();
        for link in mesh.links.iter() {
            let fragment_from = self.navigation.get_fragment(mesh, link.fragment_from);
            let fragment_to = self.navigation.get_fragment(mesh, link.fragment_to);

            assert_that!(
                is_ok!(fragment_from),
                render_mesh(mesh, "./output/test_something.html", None, None)
            );
            assert_that!(
                is_ok!(fragment_to),
                render_mesh(mesh, "./output/test_something.html", None, None)
            );
        }
        self
    }

    pub fn then_path_should_be(self, mesh: &str, length: f32, path: Vec<(f32, f32)>) -> Self {
        let mesh = self.mesh(mesh);
        let mesh = self.navigation.get_mesh(mesh).unwrap();
        let actual_path = self.path.as_ref().unwrap().clone();

        let mut actual_length = 0.0;
        for i in 0..actual_path.len() - 1 {
            actual_length += actual_path[i].get_distance(actual_path[i + 1]);
        }

        let path: Vec<PathPoint> = path.into_iter().map(|point| point.into()).collect();

        if actual_length > length {
            render_mesh(
                mesh,
                "./output/test_something.html",
                Some(actual_path),
                Some(path),
            );
            panic!(
                "Assertion failed: path length should be less than {} but {}",
                length, actual_length
            );
        }

        assert_that!(
            equals!(actual_path, path),
            render_mesh(
                mesh,
                "./output/test_something.html",
                Some(actual_path),
                Some(path)
            )
        );
        self
    }

    pub fn then_mesh_obstacles_count_should_be(self, mesh: &str, count: usize) -> Self {
        let mesh = self.mesh(mesh);
        let mesh = self.navigation.get_mesh(mesh).unwrap();
        assert_that!(
            equals!(mesh.obstacles.len(), count),
            render_mesh(mesh, "./output/test_something.html", None, None)
        );
        self
    }

    pub fn then_mesh_fragments_count_should_be(self, mesh: &str, count: usize) -> Self {
        let mesh = self.mesh(mesh);
        let mesh = self.navigation.get_mesh(mesh).unwrap();
        assert_that!(
            equals!(mesh.fragments.len(), count),
            render_mesh(mesh, "./output/test_something.html", None, None)
        );
        self
    }

    pub fn then_mesh_links_count_should_be(self, mesh: &str, count: usize) -> Self {
        let mesh_id = self.mesh(mesh);
        let mesh = self.navigation.get_mesh(mesh_id).unwrap();
        assert_that!(
            equals!(mesh.links.len(), count),
            render_mesh(mesh, "./output/test_something.html", None, None)
        );

        self.then_assert_no_broken_links(mesh_id)
    }

    pub fn then_obstacle_should_be<F>(self, mesh: &str, name: &str, obstacle_builder: F) -> Self
    where
        F: Fn(&Self) -> Obstacle,
    {
        let mesh = self.navigation.get_mesh(self.mesh(mesh)).unwrap();
        let actual_obstacle = self
            .navigation
            .get_obstacle(mesh, self.obstacle(name))
            .unwrap();
        let obstacle = obstacle_builder(&self);
        assert_that!(
            equals!(actual_obstacle, &obstacle),
            render_mesh(mesh, "./output/test_something.html", None, None)
        );
        self
    }

    pub fn then_fragment_at_point_should_be<F>(
        self,
        mesh: &str,
        coordinates: (usize, usize),
        fragment_builder: F,
    ) -> Self
    where
        F: Fn(&Self) -> Fragment,
    {
        let mesh = self.mesh(mesh);
        let mesh = self.navigation.get_mesh(mesh).unwrap();
        let actual_fragment = self.navigation.get_fragment_at(
            mesh,
            Point {
                x: coordinates.0,
                y: coordinates.1,
            },
        );

        assert_that!(is_true!(actual_fragment.is_ok()));
        let actual_fragment = actual_fragment.unwrap();
        let fragment = &fragment_builder(&self);
        assert_that!(
            equals!(actual_fragment, fragment),
            render_mesh(mesh, "./output/test_something.html", None, None)
        );
        self
    }

    pub fn then_fragment_should_be<F>(self, mesh: &str, index: usize, fragment_builder: F) -> Self
    where
        F: Fn(&Self) -> Fragment,
    {
        let mesh = self.mesh(mesh);
        let mesh = self.navigation.get_mesh(mesh).unwrap();
        let actual_fragment = &mesh.fragments[index];
        let fragment = &fragment_builder(&self);
        assert_that!(
            equals!(actual_fragment, fragment),
            render_mesh(mesh, "./output/test_something.html", None, None)
        );
        self
    }

    pub fn then_fragment_links_should_be(
        self,
        mesh: &str,
        fragment: FragmentId,
        links: Vec<&FragmentLink>,
    ) -> Self {
        let mesh = self.mesh(mesh);
        let mesh = self.navigation.get_mesh(mesh).unwrap();
        let mut actual_links = self.navigation.get_links_from_fragment(mesh, fragment);
        actual_links.extend(self.navigation.get_links_to_fragment(mesh, fragment));

        assert_that!(
            equals!(actual_links, links),
            render_mesh(mesh, "./output/test_something.html", None, None)
        );
        self
    }
}

fn render_mesh<P>(
    mesh: &Mesh,
    html_output: P,
    actual_path: Option<Vec<PathPoint>>,
    expected_path: Option<Vec<PathPoint>>,
) where
    P: AsRef<Path>,
{
    let mut plot = Plot::new();

    let axis_range = mesh.size.x.max(mesh.size.y);
    let tick = 1.0;
    /*if axis_range > 20 {
        tick = 10.0;
    }*/
    let scale = tick / 10.0;
    let axis_range = format!("{}", axis_range);

    let x_axis = Axis::new()
        .range(vec!["0", &axis_range])
        // .dtick(tick)
        // .auto_range(true)
        .auto_margin(true)
        .zero_line(false);

    let y_axis = Axis::new()
        .range(vec!["0", &axis_range])
        // .dtick(tick)
        .auto_margin(true)
        .zero_line(false)
        // .tick_mode(TickMode::Linear)
        .overlaying("x");

    let margin = Margin::new().left(40).right(40).bottom(40).top(40).pad(10);

    let title = format!(
        "{} obstacles, {} fragments, {} links",
        mesh.obstacles.len(),
        mesh.fragments.len(),
        mesh.links.len()
    );

    let mut layout = Layout::new()
        .margin(margin)
        .x_axis(x_axis)
        .y_axis(y_axis)
        .width(600)
        .height(600)
        .title(Title::new(&title))
        .auto_size(false);

    let colors = [
        "#003f5c", "#19466f", "#364c80", "#56508c", "#775194", "#985196", "#b85091", "#d45087",
        "#ec5577", "#fe6263", "#ff754c", "#ff8c30", "#ffa600",
    ];

    for fragment in mesh.fragments.iter() {
        let color = match fragment.obstacle.is_none() {
            true => colors[fragment.id.0 % colors.len()],
            false => "#111111",
        };

        let fragment_obstacle = match fragment.obstacle {
            Some(fragment_obstacle) => Some(
                mesh.obstacles
                    .iter()
                    .find(|obstacle| obstacle.id == fragment_obstacle)
                    .unwrap(),
            ),
            None => None,
        };

        let name = match fragment_obstacle {
            Some(obstacle) => {
                format!(
                    "{}x{} {}:{}",
                    fragment.size.x, fragment.size.y, obstacle.key.0, obstacle.id.0
                )
            }
            None => format!("{}x{}", fragment.size.x, fragment.size.y),
        };

        let line = match fragment.obstacle.is_none() {
            true => Line::new().color(color).width(0.0),
            false => Line::new().color("#000000").width(0.0),
        };

        let x_data = vec![
            fragment.position.x,
            fragment.position.x,
            fragment.position.x + fragment.size.x,
            fragment.position.x + fragment.size.x,
            fragment.position.x,
        ];
        let y_data = vec![
            fragment.position.y,
            fragment.position.y + fragment.size.y,
            fragment.position.y + fragment.size.y,
            fragment.position.y,
            fragment.position.y,
        ];

        let fragment_trace = Scatter::new(x_data, y_data)
            .name(&format!("{}", fragment.id.0))
            .mode(Mode::Lines)
            .fill(plotly::common::Fill::ToSelf)
            .line(line)
            .text(&name)
            .fill_color(color)
            .show_legend(false);

        plot.add_trace(fragment_trace);

        if let Some(obstacle) = fragment_obstacle {
            let (w, h) = if obstacle.rotation {
                (4.0 * scale, 1.0 * scale)
            } else {
                (1.0 * scale, 4.0 * scale)
            };
            let x = obstacle.location.x as f64 - 0.5 * scale;
            let y = obstacle.location.y as f64 - 0.5 * scale;
            layout.add_shape(
                Shape::new()
                    .shape_type(ShapeType::Rect)
                    .x0(x)
                    .y0(y)
                    .x1(x + w)
                    .y1(y + h)
                    .layer(ShapeLayer::Above)
                    .line(ShapeLine::new().width(0.0))
                    .fill_color("#1f1f1f"),
            );
        }
    }

    for link in mesh.links.iter() {
        let edge = link.contact_edge;
        let segment = link.contact_segment;

        let contact_point = match edge {
            Edge::South => {
                let contact_width = (segment.end.x - segment.begin.x) as f64;
                let contact_x = segment.begin.x as f64 + contact_width / 2.0;
                let contact_y = segment.begin.y as f64;
                (contact_x, contact_y)
            }
            Edge::North => {
                let contact_width = (segment.end.x - segment.begin.x) as f64;
                let contact_x = segment.begin.x as f64 + contact_width / 2.0;
                let contact_y = segment.begin.y as f64;
                (contact_x, contact_y)
            }
            Edge::West => {
                let contact_height = (segment.end.y - segment.begin.y) as f64;
                let contact_x = segment.begin.x as f64;
                let contact_y = segment.begin.y as f64 + contact_height / 2.0;
                (contact_x, contact_y)
            }
            Edge::East => {
                let contact_height = (segment.end.y - segment.begin.y) as f64;
                let contact_x = segment.begin.x as f64;
                let contact_y = segment.begin.y as f64 + contact_height / 2.0;
                (contact_x, contact_y)
            }
        };

        let size = 1.0 * scale;
        let offset = 0.0;
        let (width, height) = (size, size);
        let (x, y) = contact_point;
        let (x, y, width, height) = match link.contact_edge {
            Edge::North => (x, y + offset, size, size),
            Edge::South => (x, y - height - offset, size, size),
            Edge::East => (x + offset, y, size, size),
            Edge::West => (x - width - offset, y, size, size),
        };

        let color = colors[link.fragment_from.0 % colors.len()];
        layout.add_shape(
            Shape::new()
                // .x_ref("x")
                // .y_ref("y")
                .shape_type(ShapeType::Rect)
                .x0(x)
                .y0(y)
                .x1(x + width)
                .y1(y + height)
                .layer(ShapeLayer::Above)
                .line(ShapeLine::new().width(0.0))
                .fill_color(color),
        );

        let color = colors[link.fragment_to.0 % colors.len()];
        let (x, y) = contact_point;
        let (x, y, width, height) = match link.contact_edge.opposite() {
            Edge::North => (x, y + offset, size, size),
            Edge::South => (x, y - height - offset, size, size),
            Edge::East => (x + offset, y, size, size),
            Edge::West => (x - width - offset, y, size, size),
        };
        layout.add_shape(
            Shape::new()
                // .x_ref("x")
                // .y_ref("y")
                .shape_type(ShapeType::Rect)
                .x0(x)
                .y0(y)
                .x1(x + width)
                .y1(y + height)
                .layer(ShapeLayer::Above)
                .line(ShapeLine::new().width(0.0))
                .fill_color(color),
        );
    }

    if let Some(path) = expected_path {
        let x_data = path.iter().map(|point| point.x);
        let y_data = path.iter().map(|point| point.y);
        let trace = Scatter::new(x_data, y_data)
            .name("expected path")
            .mode(Mode::LinesMarkers)
            .text("")
            .hover_info(HoverInfo::None)
            .line(
                Line::new()
                    .color(Rgba::new(255, 255, 255, 0.7))
                    .dash(DashType::Dot),
            )
            .show_legend(false);
        plot.add_trace(trace);
    }

    if let Some(path) = actual_path {
        let x_data = path.iter().map(|point| point.x);
        let y_data = path.iter().map(|point| point.y);
        let trace = Scatter::new(x_data, y_data)
            .name("actual path")
            .mode(Mode::LinesMarkers)
            .text("")
            .hover_info(HoverInfo::None)
            //.line(Line::new().color("#ffffff"))
            .line(Line::new().color(Rgba::new(255, 255, 255, 1.0)))
            .show_legend(false);
        plot.add_trace(trace);

        let x_data = vec![path.iter().last().unwrap().x];
        let y_data = vec![path.iter().last().unwrap().y];
        let trace = Scatter::new(x_data, y_data)
            .name("destination")
            .mode(Mode::Text)
            .text("⚑")
            .text_font(Font::new().color(Rgba::new(255, 255, 255, 1.0)).size(19))
            .text_position(Position::TopCenter)
            .hover_info(HoverInfo::None)
            .show_legend(false);
        plot.add_trace(trace);
    }

    plot.set_layout(layout);
    plot.to_html(html_output);
}
