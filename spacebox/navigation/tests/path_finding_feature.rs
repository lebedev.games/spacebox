mod testing;

pub use testing::*;

#[test]
fn test_find_path_from_created_room_after_defragmentation() {
    NavigationDomainTestScenario::new()
        // Arrange
        .given_mesh_with_room_scene("mesh")
        // Act
        // .then_mesh_links_count_should_be("mesh", 11)
        .when_reduce_fragmentation()
        // .then_mesh_links_count_should_be("mesh", 10)
        .when_find_path("mesh", (10.0, 10.0), (19.5, 10.0));
}

#[test]
fn test_find_path_from_created_room() {
    NavigationDomainTestScenario::new()
        // Arrange
        .given_mesh_with_room_scene("mesh")
        // Act
        .when_find_path("mesh", (10.0, 10.0), (19.5, 10.0))
        // Assert
        .then_path_should_be(
            "mesh",
            21.55,
            vec![
                (10.0, 10.0),
                (6.0, 9.0),
                (6.0, 7.0),
                (12.0, 7.0),
                (19.5, 10.0),
            ],
        );
}

#[test]
fn test_find_path_straight_vertically_around_one_obstacle() {
    NavigationDomainTestScenario::new()
        // Arrange
        .given_mesh_with_type("mesh", 20, 20)
        .given_obstacle_type("table_type", 2, 2)
        // Act
        .when_create_obstacle("mesh", "table_type", "table", (10.0, 10.0))
        .when_find_path("mesh", (10.0, 19.5), (10.0, 0.5))
        // Assert
        .then_path_should_be(
            "mesh",
            19.12,
            vec![(10.0, 19.5), (11.0, 11.0), (11.0, 9.0), (10.0, 0.5)],
        );
}

#[test]
fn test_find_path_straight_vertically_around_two_obstacle() {
    NavigationDomainTestScenario::new()
        // Arrange
        .given_mesh_with_type("mesh", 20, 20)
        .given_obstacle_type("table_type", 2, 2)
        // Act
        .when_create_obstacle("mesh", "table_type", "1", (10.0, 5.0))
        .when_create_obstacle("mesh", "table_type", "2", (10.0, 15.0))
        .when_find_path("mesh", (10.0, 0.5), (10.0, 19.5))
        // Assert
        .then_path_should_be(
            "mesh",
            19.29,
            vec![(10.0, 0.5), (11.0, 4.0), (11.0, 16.0), (10.0, 19.5)],
        );
}

#[test]
fn test_find_path_straight_horizontally_around_two_obstacle() {
    NavigationDomainTestScenario::new()
        // Arrange
        .given_mesh_with_type("mesh", 20, 20)
        .given_obstacle_type("table_type", 2, 2)
        // Act
        .when_create_obstacle("mesh", "table_type", "1", (5.0, 10.0))
        .when_create_obstacle("mesh", "table_type", "1", (15.0, 10.0))
        .when_find_path("mesh", (19.5, 10.0), (0.5, 10.0))
        // Assert
        .then_path_should_be(
            "mesh",
            19.29,
            vec![(19.5, 10.0), (16.0, 9.0), (4.0, 9.0), (0.5, 10.0)],
        );
}

#[test]
fn test_find_path_straight_horizontally_around_one_obstacle() {
    NavigationDomainTestScenario::new()
        // Arrange
        .given_mesh_with_type("mesh", 20, 20)
        .given_obstacle_type("table_type", 2, 2)
        // Act
        .when_create_obstacle("mesh", "table_type", "table", (10.0, 10.0))
        .when_find_path("mesh", (0.5, 10.0), (19.5, 10.0))
        // Assert
        .then_path_should_be(
            "mesh",
            25.5,
            vec![(0.5, 10.0), (9.0, 9.0), (11.0, 9.0), (19.5, 10.0)],
        );
}

#[test]
fn test_find_path() {
    NavigationDomainTestScenario::new()
        // Arrange
        .given_mesh_with_type("mesh", 10, 10)
        .given_obstacle_type("chair_type", 1, 1)
        .given_obstacle_type("table_type", 3, 3)
        // Act
        .when_create_obstacle("mesh", "chair_type", "chair", (2.5, 2.5))
        .when_create_obstacle("mesh", "table_type", "table", (3.5, 8.5))
        .when_find_path("mesh", (0.5, 0.5), (9.5, 9.5))
        // Assert
        .then_path_should_be("mesh", 13.54, vec![(0.5, 0.5), (2.0, 3.0), (9.5, 9.5)]);
}
