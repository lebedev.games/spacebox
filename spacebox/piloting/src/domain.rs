use std::rc::Rc;

use crate::{PilotingDatabase, PilotingState};

#[derive(Default)]
pub struct PilotingDomain {
    pub database: Rc<PilotingDatabase>,
    pub state: PilotingState,
}
