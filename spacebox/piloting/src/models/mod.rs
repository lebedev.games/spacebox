use serde::{Deserialize, Serialize};

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq, Hash, Eq)]
pub struct SpacecraftId(pub usize);

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct SpacecraftKey(pub String);

#[derive(Debug, Serialize, Deserialize)]
pub struct SpacecraftType {
    pub key: SpacecraftKey,
}

/// Overall mission success, safety of crew and `Spacecraft`
#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq, Hash, Eq)]
pub struct CommanderId(pub usize);

/// Assisted the `Commander` in maneuvering the `Spacecraft`.
/// May have also been responsible for release and recovery of satellites
#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq, Hash, Eq)]
pub struct PilotId(pub usize);

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Spacecraft {
    pub id: SpacecraftId,
    pub key: SpacecraftKey,
    pub commander: Option<CommanderId>,
    pub pilot: Option<PilotId>,
}

impl From<usize> for SpacecraftId {
    fn from(value: usize) -> Self {
        Self(value)
    }
}

impl From<usize> for CommanderId {
    fn from(value: usize) -> Self {
        Self(value)
    }
}

impl From<&str> for SpacecraftKey {
    fn from(value: &str) -> Self {
        Self(value.into())
    }
}
