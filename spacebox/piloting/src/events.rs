use serde::{Deserialize, Serialize};

use crate::{CommanderId, PilotId, SpacecraftId};

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
#[serde(tag = "$type")]
pub enum Piloting {
    CommanderAssigned {
        spacecraft: SpacecraftId,
        commander: CommanderId,
    },
    CommanderDismissed {
        spacecraft: SpacecraftId,
        commander: CommanderId,
    },
    PilotAssigned {
        spacecraft: SpacecraftId,
        pilot: PilotId,
    },
    PilotDismissed {
        spacecraft: SpacecraftId,
        pilot: PilotId,
    },
}
