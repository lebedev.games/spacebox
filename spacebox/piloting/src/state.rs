use serde::{Deserialize, Serialize};

use crate::Spacecraft;

#[derive(Debug, Clone, Serialize, Deserialize, Default)]
pub struct PilotingState {
    pub spacecrafts: Vec<Spacecraft>,
}
