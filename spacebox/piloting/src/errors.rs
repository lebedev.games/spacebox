use serde::{Deserialize, Serialize};

use crate::{CommanderId, SpacecraftId};

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(tag = "$type")]
pub enum PilotingError {
    SpacecraftNotFound { id: SpacecraftId },
    CommanderSpacecraftNotFound { commander: CommanderId },
}
