use serde::{Deserialize, Serialize};

use crate::SpacecraftType;

#[derive(Debug, Serialize, Deserialize, Default)]
pub struct PilotingDatabase {
    pub spacecrafts: Vec<SpacecraftType>,
}
