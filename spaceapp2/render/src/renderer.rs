use std::borrow::Borrow;
use std::cell::UnsafeCell;
use std::collections::HashMap;
use std::fs;
use std::mem::transmute;
use std::rc::Rc;

use glium::backend::{Context, Facade};
use glium::index::PrimitiveType::TrianglesList;
use glium::texture::{RawImage2d, TextureCreationError};
use glium::uniforms::{
    AsUniformValue,
    EmptyUniforms,
    Sampler,
    SamplerBehavior,
    UniformValue,
    Uniforms,
    UniformsStorage,
};
use glium::{
    index,
    vertex,
    IncompatibleOpenGl,
    IndexBuffer,
    Program,
    ProgramCreationError,
    Surface,
    SwapBuffersError,
    VertexBuffer,
};
use interop::Operation::{ClearColorAndDepth, CreateMesh, CreateShader, CreateTexture};
use interop::{Frame, GenericVertex, InputEvent, Operation, Scancode, Uniform, Value};
use sdl2::event::{Event, WindowEvent};
use sdl2::hint::{set_with_priority, Hint};
use sdl2::video::{GLProfile, WindowBuildError, WindowPos};
use sdl2::{sys, EventPump};

use crate::{SdlBackend, SdlFacade};

pub struct Renderer {
    pub(crate) event_pump: EventPump,
    pub(crate) facade: SdlFacade,
    pub(crate) state: RendererState,
    pub(crate) dummy: RendererDummy,
}

#[derive(Default)]
pub struct RendererState {
    textures: HashMap<String, Texture>,
    shaders: HashMap<String, Shader>,
    meshes: HashMap<String, Mesh>,
}

pub struct RendererDummy {
    texture: Texture,
    shader: Shader,
}

#[derive(Debug, Copy, Clone)]
pub struct Vertex {
    pub position: [f32; 3],
    pub normal: [f32; 3],
    pub tex_coords: [f32; 2],
}

implement_vertex!(Vertex, position, normal, tex_coords);

pub struct Mesh {
    pub vertex_buffer: VertexBuffer<Vertex>,
    pub index_buffer: IndexBuffer<u32>,
}

impl Mesh {
    fn create<F>(
        facade: &F,
        vertices: Vec<GenericVertex>,
        indices: Vec<u32>,
    ) -> Result<Mesh, RendererError>
    where
        F: Facade,
    {
        let vertices: Vec<Vertex> = vertices
            .into_iter()
            .map(|vertex| Vertex {
                position: vertex.position,
                normal: vertex.normal,
                tex_coords: vertex.uv,
            })
            .collect();

        let vertex_buffer = VertexBuffer::new(facade, &vertices)
            .map_err(RendererError::VertexBufferCreationError)?;

        let index_buffer = IndexBuffer::new(facade, TrianglesList, &indices[..])
            .map_err(RendererError::IndexBufferCreationError)?;

        Ok(Mesh { vertex_buffer, index_buffer })
    }
}

pub struct Texture {
    pub handle: glium::texture::Texture2d,
    pub size: (u32, u32),
}

impl Texture {
    fn create<F>(facade: &F, size: (u32, u32), data: Vec<u8>) -> Result<Texture, RendererError>
    where
        F: Facade,
    {
        let raw = RawImage2d::from_raw_rgba_reversed(&data, size);
        let texture = glium::texture::Texture2d::new(facade, raw)
            .map_err(RendererError::CreateTextureError)?;
        Ok(Texture { handle: texture, size })
    }
}

pub struct Shader {
    pub program: Program,
}

impl Shader {
    fn create<F>(
        facade: &F,
        vertex: String,
        fragment: String,
        geometry: Option<String>,
    ) -> Result<Shader, RendererError>
    where
        F: Facade,
    {
        let program = Program::from_source(
            facade,
            &vertex,
            &fragment,
            geometry.as_ref().map(|source| source.as_str()),
        )
        .map_err(RendererError::CreateProgramError)?;
        Ok(Shader { program })
    }
}

const DUMMY_TEXTURE_SIZE: (u32, u32) = (4, 4);
const DUMMY_TEXTURE_DATA: [u8; 64] = [0_u8; 64];
const DUMMY_SHADER_VERTEX: &str = r#"
    #version 410

    in vec3 position;
    in vec3 normal;
    in vec2 tex_coords;
    
    out vec2 out_tex_coords;
    out vec3 out_normal;

    void main() {
        out_tex_coords = tex_coords;
        out_normal = normal; 
        gl_Position = vec4(position, 1.0);
    }
"#;

const DUMMY_SHADER_FRAGMENT: &str = r#"
    #version 410

    in vec2 out_tex_coords;
    in vec3 out_normal;
    
    out vec4 color;

    uniform sampler2D tex;

    void main() {
        vec3 objectColor = texture(tex, out_tex_coords).xyz;
        color = vec4(objectColor, 1.0);
    }
"#;

#[derive(Debug)]
pub enum RendererError {
    SwapBuffersError(SwapBuffersError),
    Sdl(String),
    WindowBuildError(WindowBuildError),
    IncompatibleOpenGl(IncompatibleOpenGl),
    CreateTextureError(TextureCreationError),
    CreateProgramError(ProgramCreationError),
    VertexBufferCreationError(vertex::BufferCreationError),
    IndexBufferCreationError(index::BufferCreationError),
}

impl Renderer {
    pub fn new(title: &str, width: u32, height: u32) -> Result<Renderer, RendererError> {
        let sdl = sdl2::init().map_err(RendererError::Sdl)?;

        set_with_priority("SDL_HINT_RENDER_VSYNC", "0", &Hint::Override);

        let video = sdl.video().map_err(RendererError::Sdl)?;

        let gl = video.gl_attr();
        gl.set_context_profile(GLProfile::Core);
        gl.set_context_version(4, 1);

        info!("OpenGL Context Profile: {:?}", gl.context_profile());
        info!("OpenGL Version: {:?}", gl.context_version());

        let window = video
            .window(title, width, height)
            .set_window_flags(sys::SDL_WindowFlags::SDL_WINDOW_ALWAYS_ON_TOP as u32)
            .position_centered()
            .opengl()
            .build()
            .map_err(RendererError::WindowBuildError)?;

        let context = window.gl_create_context().map_err(RendererError::Sdl)?;
        let backend = SdlBackend { window: UnsafeCell::new(window), context };
        let backend = Rc::new(backend);
        let context = unsafe { Context::new(backend.clone(), true, Default::default()) };
        let context = context.map_err(RendererError::IncompatibleOpenGl)?;
        let facade = SdlFacade { context, backend };

        let event_pump = sdl.event_pump().map_err(RendererError::Sdl)?;

        let dummy = RendererDummy {
            texture: Texture::create(&facade, DUMMY_TEXTURE_SIZE, DUMMY_TEXTURE_DATA.to_vec())?,
            shader: Shader::create(
                &facade,
                String::from(DUMMY_SHADER_VERTEX),
                String::from(DUMMY_SHADER_FRAGMENT),
                None,
            )?,
        };

        let mut renderer = Renderer { facade, event_pump, state: Default::default(), dummy };
        renderer.load();
        renderer.render(Frame {
            operations: vec![ClearColorAndDepth { color: (0.0, 0.0, 1.0, 1.0), depth: 1.0 }],
        })?;
        Ok(renderer)
    }

    pub fn save(x: i32, y: i32) {
        let data = format!("{}:{}", x, y);
        if let Err(error) = fs::write("./renderer.conf", data) {
            error!("Unable to save renderer conf, {:?}", error);
        }
    }

    pub fn load(&mut self) {
        match fs::read("./renderer.conf") {
            Ok(data) => match String::from_utf8(data) {
                Ok(data) => {
                    let window = self.facade.window_mut();
                    let pair: Vec<&str> = data.split(":").collect();
                    let x = pair[0].parse::<i32>().unwrap();
                    let y = pair[1].parse::<i32>().unwrap();
                    window.set_position(WindowPos::Positioned(x), WindowPos::Positioned(y));
                }
                Err(error) => {
                    error!("Unable to load renderer conf, {:?}", error);
                }
            },
            Err(error) => {
                error!("Unable to load renderer conf, {:?}", error);
            }
        }
    }

    pub fn render(&mut self, frame: Frame) -> Result<(), RendererError> {
        let mut target = self.facade.draw();

        for operation in frame.operations {
            match operation {
                ClearColorAndDepth { color, depth } => {
                    target.clear_color_and_depth(color, depth);
                }
                CreateTexture { id, size, data } => {
                    match Texture::create(&self.facade, size, data) {
                        Ok(texture) => {
                            info!("Texture {} created", id);
                            self.state.textures.insert(id, texture);
                        }
                        Err(error) => {
                            error!(
                                "Unable to create texture {}, use dummy instead. Error: {:?}",
                                id, error
                            );
                        }
                    };
                }
                CreateMesh { id, vertices, indices } => {
                    match Mesh::create(&self.facade, vertices, indices) {
                        Ok(mesh) => {
                            info!("Mesh {} created", id);
                            self.state.meshes.insert(id, mesh);
                        }
                        Err(error) => {
                            error!(
                                "Unable to create mesh {}, use dummy instead. Error: {:?}",
                                id, error
                            );
                        }
                    }
                }
                CreateShader { id, vertex, fragment, geometry } => {
                    match Shader::create(&self.facade, vertex, fragment, geometry) {
                        Ok(shader) => {
                            info!("Shader {} created", id);
                            self.state.shaders.insert(id, shader);
                        }
                        Err(error) => {
                            error!(
                                "Unable to create shader {}, use dummy instead. Error: {:?}",
                                id, error
                            );
                        }
                    }
                }
                Operation::DrawMesh { id, shader, uniforms } => {
                    let mesh = match self.get_mesh(&id) {
                        Some(mesh) => mesh,
                        None => {
                            error!("Unable to draw mesh {}, dummy not implemented yet", id);
                            continue;
                        }
                    };
                    let shader = self.get_shader(&shader);

                    let mut input = UniformsVec::new();
                    for uniform in uniforms {
                        let name = uniform.name;
                        let value = match uniform.value {
                            Value::Vec2(value) => UniformValue::Vec2(value),
                            Value::Vec3(value) => UniformValue::Vec3(value),
                            Value::Vec4(value) => UniformValue::Vec4(value),
                            Value::Mat2(value) => UniformValue::Mat2(value),
                            Value::Mat3(value) => UniformValue::Mat3(value),
                            Value::Mat4(value) => UniformValue::Mat4(value),
                            Value::Sampler2d(texture) => self.get_uniform_texture(&texture),
                        };
                        input.push(name, value);
                    }
                    let uniforms = input;

                    let parameters = glium::DrawParameters {
                        depth: glium::Depth {
                            test: glium::draw_parameters::DepthTest::IfLess,
                            write: true,
                            ..Default::default()
                        },
                        backface_culling:
                            glium::draw_parameters::BackfaceCullingMode::CullingDisabled,
                        ..Default::default()
                    };
                    if let Err(error) = target.draw(
                        &mesh.vertex_buffer,
                        &mesh.index_buffer,
                        &shader.program,
                        &uniforms,
                        &parameters,
                    ) {
                        error!("Unable to draw mesh {}, {:?}", id, error);
                    }
                }
            }
        }

        target.finish().map_err(RendererError::SwapBuffersError)
    }

    fn get_mesh(&self, id: &String) -> Option<&Mesh> {
        self.state.meshes.get(id)
    }

    fn get_texture(&self, id: &String) -> &Texture {
        match self.state.textures.get(id) {
            Some(texture) => texture,
            None => &self.dummy.texture,
        }
    }

    fn get_uniform_texture(&self, id: &String) -> UniformValue {
        let texture = match self.state.textures.get(id) {
            Some(texture) => texture,
            None => &self.dummy.texture,
        };
        UniformValue::Texture2d(&texture.handle, Some(SamplerBehavior::default()))
    }

    fn get_shader(&self, id: &String) -> &Shader {
        match self.state.shaders.get(id) {
            Some(shader) => shader,
            None => &self.dummy.shader,
        }
    }
}

struct UniformsVec<'v> {
    pub uniforms: Vec<(String, UniformValue<'v>)>,
}

impl<'v> UniformsVec<'v> {
    pub fn new() -> Self {
        UniformsVec { uniforms: vec![] }
    }
    pub fn push(&mut self, name: String, value: UniformValue<'v>) {
        self.uniforms.push((name, value))
    }
}

impl<'v> Uniforms for UniformsVec<'v> {
    #[inline]
    fn visit_values<'a, F: FnMut(&str, UniformValue<'a>)>(&'a self, mut output: F) {
        for (name, value) in &self.uniforms {
            output(&name, *value);
        }
    }
}
