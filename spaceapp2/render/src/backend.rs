extern crate glium;
extern crate sdl2;

use std::cell::UnsafeCell;
use std::mem;
use std::ops::Deref;
use std::os::raw::c_void;
use std::rc::Rc;

use glium::backend::{Backend, Context, Facade};
use glium::SwapBuffersError;
use sdl2::video::Window;
use sdl2::VideoSubsystem;

use self::sdl2::video::GLContext;

#[derive(Clone)]
pub struct SdlFacade {
    pub context: Rc<Context>,
    pub backend: Rc<SdlBackend>,
}

impl Facade for SdlFacade {
    fn get_context(&self) -> &Rc<Context> {
        &self.context
    }
}

impl Deref for SdlFacade {
    type Target = Context;

    fn deref(&self) -> &Context {
        &self.context
    }
}

impl SdlFacade {
    pub fn window(&self) -> &Window {
        self.backend.window()
    }

    pub fn window_mut(&mut self) -> &mut Window {
        self.backend.window_mut()
    }

    pub fn draw(&self) -> glium::Frame {
        glium::Frame::new(self.context.clone(), self.backend.get_framebuffer_dimensions())
    }
}

pub struct SdlBackend {
    pub window: UnsafeCell<Window>,
    pub context: GLContext,
}

impl SdlBackend {
    pub fn subsystem(&self) -> &VideoSubsystem {
        let ptr = self.window.get();
        let window: &Window = unsafe { mem::transmute(ptr) };
        window.subsystem()
    }

    pub fn window(&self) -> &Window {
        let ptr = self.window.get();
        unsafe { mem::transmute(ptr) }
    }

    pub fn window_mut(&self) -> &mut Window {
        let ptr = self.window.get();
        unsafe { mem::transmute(ptr) }
    }
}

unsafe impl Backend for SdlBackend {
    fn swap_buffers(&self) -> Result<(), SwapBuffersError> {
        self.window().gl_swap_window();
        Ok(())
    }

    unsafe fn get_proc_address(&self, symbol: &str) -> *const c_void {
        self.subsystem().gl_get_proc_address(symbol) as *const c_void
    }

    fn get_framebuffer_dimensions(&self) -> (u32, u32) {
        let (width, height) = self.window().drawable_size();
        (width as u32, height as u32)
    }

    fn is_current(&self) -> bool {
        self.context.is_current()
    }

    unsafe fn make_current(&self) {
        self.window().gl_make_current(&self.context).unwrap()
    }
}
