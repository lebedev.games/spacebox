use std::mem::transmute;

use interop::{InputEvent, Scancode};
use sdl2::event::{Event, WindowEvent};
use sdl2::video::WindowPos;

use crate::Renderer;

impl Renderer {
    pub fn poll_events(&mut self) -> Option<Vec<InputEvent>> {
        let mut events = vec![];
        for event in self.event_pump.poll_iter() {
            match event {
                Event::Quit { .. } => return None,
                Event::MouseMotion { .. } => {
                    // println!("mouse motion x:{}", x);
                }
                Event::KeyDown { scancode, keymod, .. } => {
                    let event = InputEvent::KeyDown {
                        scancode: unsafe {
                            transmute(
                                scancode
                                    .map(|value| value as i32)
                                    .unwrap_or(Scancode::Unknown as i32),
                            )
                        },
                        modifier: keymod.bits(),
                    };
                    events.push(event);
                }
                Event::KeyUp { scancode, keymod, .. } => {
                    let event = InputEvent::KeyUp {
                        scancode: unsafe {
                            transmute(
                                scancode
                                    .map(|value| value as i32)
                                    .unwrap_or(Scancode::Unknown as i32),
                            )
                        },
                        modifier: keymod.bits(),
                    };
                    events.push(event);
                }
                Event::TextInput { text, .. } => {
                    let event = InputEvent::TextInput { text };
                    events.push(event)
                }
                Event::TextEditing { text, start, length, .. } => {
                    events.push(InputEvent::TextEditing { text, start, length })
                }
                Event::Window { win_event, .. } => match win_event {
                    WindowEvent::FocusGained => {
                        let window = self.facade.backend.window_mut();
                        let (x, y) = window.position();
                        window.set_position(
                            WindowPos::Positioned(x - 640 - 160),
                            WindowPos::Positioned(y),
                        );
                        window.set_size(1280, 960).unwrap();
                        Renderer::save(x - 640 - 160, y);
                    }
                    WindowEvent::FocusLost => {
                        let window = self.facade.backend.window_mut();
                        let (x, y) = window.position();
                        window.set_position(
                            WindowPos::Positioned(x + 640 + 160),
                            WindowPos::Positioned(y),
                        );
                        window.set_size(480, 360).unwrap();
                        Renderer::save(x + 640 + 160, y);
                    }
                    _ => {}
                },
                _ => {}
            }
        }

        Some(events)
    }
}
