#[macro_use]
extern crate log;

use std::path::{Path, PathBuf};
use std::sync::mpsc::channel;
use std::thread::sleep;
use std::time::{Duration, Instant};

use engine::camera::Camera;
use engine::data::{read_file, DataError, DataPath};
use engine::space3::read_scene_from_file;
use engine::texture::import_texture;
use env_logger::Env;
use interop::Operation::{ClearColorAndDepth, CreateMesh, CreateShader, CreateTexture, DrawMesh};
use interop::{connect_remote_application, Frame, GenericVertex, Uniform, Value};
use log::LevelFilter;
use notify::{watcher, DebouncedEvent, RecursiveMode, Watcher};

pub struct Scene {
    pub camera: Camera,
}

pub fn load_scene(path: &PathBuf) -> Result<Scene, DataError> {
    let data = path.join("state");

    let camera = read_file(data.join("camera.yaml"))?;
    let camera = Camera::from_data(camera);

    Ok(Scene { camera })
}

pub fn save_scene(scene: &Scene, path: &PathBuf) -> Result<(), DataError> {
    let data = DataPath::new(path);
    let data = data.join("state");

    data.write("camera.yaml", &scene.camera.data)?;

    Ok(())
}

fn main() {
    env_logger::Builder::from_env(Env::default()).filter_level(LevelFilter::Debug).init();

    let (mut frames, mut input) = connect_remote_application("0.0.0.0:7666").unwrap();

    let assets_path = Path::new("./assets");
    let scene_path = Path::new("../scene");

    let mut my_scene = load_scene(&scene_path.to_path_buf()).unwrap();

    let (tx, rx) = channel();
    let mut watcher = watcher(tx, Duration::from_secs_f32(1.0)).unwrap();
    watcher.watch(assets_path, RecursiveMode::Recursive).unwrap();

    // 1
    let mut operations = vec![];
    let texture_id = "./assets/textures/nature.png";
    let texture = import_texture(texture_id).unwrap();
    operations.push(CreateTexture { id: texture.id, size: texture.size, data: texture.data });
    // 2 FBX
    // let meshes = import_meshes("./assets/meshes/cube.fbx").unwrap();
    // for mesh in meshes {
    //     operations.push(CreateMesh {
    //         id: mesh.id,
    //         vertices: mesh.vertices,
    //         normals: mesh.normals,
    //         coords: mesh.coords,
    //         indices: mesh.indices,
    //     });
    // }
    // 2 Space3
    let scene = read_scene_from_file("./assets/meshes/nature.space3").unwrap();
    for mesh in scene.meshes {
        operations.push(CreateMesh {
            id: "mesh".to_string(),
            vertices: mesh
                .vertices
                .into_iter()
                .map(|vertex| GenericVertex {
                    position: vertex.position,
                    normal: vertex.normal,
                    uv: vertex.uv,
                    bones: vertex.bones,
                    weights: vertex.weights,
                })
                .collect(),
            indices: mesh.triangles,
        })
    }
    // 3
    let vertex_shader_src = r#"
        #version 330

        in vec3 position;
        in vec3 normal;
        in vec2 tex_coords;
        
        out vec2 out_tex_coords;
        out vec3 out_normal;

        uniform mat4 perspective;
        uniform mat4 view;

        void main() {
            out_tex_coords = tex_coords;
            out_normal = normal; 
            gl_Position = perspective * view * vec4(position, 1.0);
        }
    "#;

    let fragment_shader_src = r#"
        #version 330

        in vec2 out_tex_coords;
        in vec3 out_normal;
        
        out vec4 color;
        uniform vec3 u_light;

        uniform sampler2D tex;

        void main() {
            float brightness = dot(normalize(out_normal), normalize(u_light));
            vec3 dark_color = vec3(0.1, 0.0, 0.0);
            vec3 objectColor = texture(tex, out_tex_coords).xyz;
            vec3 ambient = mix(dark_color, objectColor, brightness);
        
            vec3 phong = ambient * objectColor;
            
            color = vec4(phong, 1.0);
        }
    "#;
    operations.push(CreateShader {
        id: "my-material".to_string(),
        vertex: vertex_shader_src.to_string(),
        fragment: fragment_shader_src.to_string(),
        geometry: None,
    });
    /*
    // 4 canvas
    let canvas = test_canvas();
    operations.push(CreateTexture { id: "canvas".to_string(), size: (512, 512), data: canvas });
    // 5 font
    let (size, data) = test_render_text();
    operations.push(CreateTexture { id: "font".to_string(), size, data });
    */

    // on start
    frames.send_frame(Frame { operations }).unwrap();

    let mut time = Instant::now();
    loop {
        let elapsed = time.elapsed();
        time = Instant::now();

        for event in rx.try_recv() {
            match event {
                DebouncedEvent::NoticeWrite(_source) => {}
                DebouncedEvent::NoticeRemove(_source) => {}
                DebouncedEvent::Create(_source) => {}
                DebouncedEvent::Write(_source) => {}
                DebouncedEvent::Remove(_source) => {}
                DebouncedEvent::Rename(_source, _destination) => {}
                DebouncedEvent::Error(error, source) => {
                    error!("Unable to watch asset because of {:?}, {:?}", error, source);
                }
                _ => {}
            }
        }

        let camera = &mut my_scene.camera;
        if let Some(input) = input.receive_input() {
            for event in input {
                camera.process_input(&event);
            }
        }
        camera.update(elapsed, 10.0);

        let uniforms = vec![
            Uniform { name: "tex".to_string(), value: Value::Sampler2d(texture_id.to_string()) },
            Uniform { name: "u_light".to_string(), value: Value::Vec3([-1.0, 0.4, 0.9]) },
            Uniform {
                name: "perspective".to_string(),
                value: Value::Mat4(camera.get_perspective()),
            },
            Uniform { name: "view".to_string(), value: Value::Mat4(camera.get_view()) },
        ];

        frames
            .send_frame(Frame {
                operations: vec![
                    ClearColorAndDepth { color: (0.2, 0.2, 0.2, 1.0), depth: 1.0 },
                    DrawMesh {
                        id: "mesh".to_string(),
                        shader: "my-material".to_string(),
                        uniforms,
                    },
                ],
            })
            .unwrap();

        sleep(Duration::from_millis(20));
        if let Err(error) = save_scene(&my_scene, &scene_path.to_path_buf()) {
            error!("Unable to save scene, {:?}", error);
        }
    }
}
