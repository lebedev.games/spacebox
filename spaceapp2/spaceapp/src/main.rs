#[macro_use]
extern crate log;

use std::thread::sleep;
use std::time::{Duration, Instant};

use env_logger::Env;
use interop::{accept_remote_application, RemoteApplication};
use log::LevelFilter;
use render::Renderer;

fn main() {
    env_logger::Builder::from_env(Env::default()).filter_level(LevelFilter::Debug).init();

    let remote_application = accept_remote_application("0.0.0.0:7666").unwrap();
    let mut renderer = Renderer::new("Hello", 800, 600).unwrap();

    let mut engine_app = None;
    loop {
        for app in remote_application.try_recv() {
            info!("Remote application reloaded");
            engine_app = Some(app);
        }

        let time = Instant::now();
        if let Some(engine) = engine_app.as_mut() {
            let input = match renderer.poll_events() {
                Some(events) => events,
                None => {
                    info!("Window is closed, stop application");
                    break;
                }
            };

            if engine.sender.send_input(input).is_none() {
                info!("Unable to send input, connection lost");
                engine_app = None;
                continue;
            }

            if let Some(frame) = engine.receiver.receive_frame() {
                if let Err(error) = renderer.render(frame) {
                    error!("Unable to render frame, {:?}", error);
                }
            } else {
                info!("Unable to receive frame, connection lost");
                engine_app = None;
                continue;
            }
        } else {
            let input = match renderer.poll_events() {
                Some(events) => events,
                None => {
                    info!("Window is closed, stop application");
                    break;
                }
            };
            sleep(Duration::from_millis(50));
        }
        // println!("frame time: {:?}", time.elapsed());
    }
}
