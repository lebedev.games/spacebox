use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Value {
    Vec2([f32; 2]),
    Vec3([f32; 3]),
    Vec4([f32; 4]),
    Mat2([[f32; 2]; 2]),
    Mat3([[f32; 3]; 3]),
    Mat4([[f32; 4]; 4]),
    Sampler2d(String),
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Uniform {
    pub name: String,
    pub value: Value,
}
