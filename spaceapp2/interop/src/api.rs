use serde::{Deserialize, Serialize};

use crate::{Scancode, Uniform};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Frame {
    pub operations: Vec<Operation>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct GenericVertex {
    pub position: [f32; 3],
    pub normal: [f32; 3],
    pub uv: [f32; 2],
    pub bones: [i8; 4],
    pub weights: [f32; 4],
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Operation {
    ClearColorAndDepth {
        color: (f32, f32, f32, f32),
        depth: f32,
    },
    CreateTexture {
        id: String,
        size: (u32, u32),
        data: Vec<u8>,
    },
    CreateShader {
        id: String,
        vertex: String,
        fragment: String,
        geometry: Option<String>,
    },
    CreateMesh {
        id: String,
        vertices: Vec<GenericVertex>,
        indices: Vec<u32>,
    },
    DrawMesh {
        id: String,
        shader: String,
        uniforms: Vec<Uniform>,
    },
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum InputEvent {
    KeyDown {
        scancode: Scancode,
        modifier: u16,
    },
    KeyUp {
        scancode: Scancode,
        modifier: u16,
    },
    TextEditing {
        text: String,
        start: i32,
        length: i32,
    },
    TextInput {
        text: String,
    },
}
