use std::convert::TryFrom;
use std::io::{Read, Write};
use std::net::{TcpListener, TcpStream, ToSocketAddrs};
use std::sync::mpsc::{channel, Receiver, Sender};
use std::{io, thread};

use crate::{Frame, InputEvent};

type Input = Vec<InputEvent>;

pub struct RemoteApplication {
    pub receiver: SyncFrameReceiver,
    pub sender: SyncInputSender,
}

#[derive(Debug)]
pub enum NetworkError {
    Io(io::Error),
}

pub fn accept_remote_application<A>(address: A) -> Result<Receiver<RemoteApplication>, NetworkError>
where
    A: ToSocketAddrs,
{
    let listener = TcpListener::bind(address).map_err(NetworkError::Io)?;
    let (application_sender, application) = channel();
    thread::spawn(move || serve_incoming_connections(listener, application_sender));
    Ok(application)
}

fn serve_incoming_connections(listener: TcpListener, application: Sender<RemoteApplication>) {
    info!("Serve incoming connections on {:?}", listener.local_addr());
    for stream in listener.incoming() {
        let stream = match stream {
            Ok(stream) => stream,
            Err(error) => {
                error!("Unable to make connection stream, {:?}", error);
                continue;
            }
        };
        let reader = stream.try_clone().unwrap();
        let writer = stream;

        application
            .send(RemoteApplication {
                receiver: SyncFrameReceiver { reader },
                sender: SyncInputSender { writer },
            })
            .unwrap();
    }
}

pub struct SyncFrameReceiver {
    pub reader: TcpStream,
}

impl SyncFrameReceiver {
    pub fn receive_frame(&mut self) -> Option<Frame> {
        let mut header_buffer = [0_u8; 4];
        if let Err(error) = self.reader.read_exact(&mut header_buffer) {
            error!("Unable to receive frame because of header read, {}", error);
            return None;
        }
        let body_length = u32::from_le_bytes(header_buffer) as usize;
        let mut body_buffer = vec![0; body_length];
        if let Err(error) = self.reader.read_exact(body_buffer.as_mut_slice()) {
            error!("Unable to receive frame because of body read, {}", error);
            return None;
        }
        let frame: Frame = match serde_json::from_slice(body_buffer.as_slice()) {
            Ok(frame) => frame,
            Err(error) => {
                error!("Unable to receive frame because of deserialization, {}", error);
                return None;
            }
        };

        Some(frame)
    }
}

pub struct SyncInputSender {
    pub writer: TcpStream,
}

impl SyncInputSender {
    pub fn send_input(&mut self, input: Input) -> Option<()> {
        match serde_json::to_vec(&input) {
            Ok(body) => {
                let header = match u16::try_from(body.len()) {
                    Ok(body_length) => body_length.to_le_bytes(),
                    Err(error) => {
                        error!("Unable to send input because of body length {}", error);
                        return None;
                    }
                };
                if let Err(error) = self.writer.write_all(&header) {
                    error!("Unable to send input because of header write, {}", error);
                    return None;
                }
                if let Err(error) = self.writer.write_all(&body) {
                    error!("Unable to send input because of body write, {}", error);
                    return None;
                }
            }
            Err(error) => {
                error!("Unable to send input because of serialization, {}", error)
            }
        }

        Some(())
    }
}

pub fn connect_remote_application<A>(
    address: A,
) -> Result<(SyncFrameSender, SyncInputReceiver), NetworkError>
where
    A: ToSocketAddrs,
{
    let stream = TcpStream::connect(address).map_err(NetworkError::Io)?;
    let reader = stream.try_clone().unwrap();
    let writer = stream;

    let sync_frame_sender = SyncFrameSender { writer };
    let sync_input_receiver = SyncInputReceiver { reader };

    Ok((sync_frame_sender, sync_input_receiver))
}

pub struct SyncInputReceiver {
    pub reader: TcpStream,
}

impl SyncInputReceiver {
    pub fn receive_input(&mut self) -> Option<Input> {
        let mut header_buffer = [0_u8; 2];
        if let Err(error) = self.reader.read_exact(&mut header_buffer) {
            error!("Unable to receive input because of header read, {}", error);
            return None;
        }
        let body_length = u16::from_le_bytes(header_buffer) as usize;
        let mut body_buffer = vec![0; body_length];
        if let Err(error) = self.reader.read_exact(body_buffer.as_mut_slice()) {
            error!("Unable to receive input because of body read, {}", error);
            return None;
        }
        let input: Input = match serde_json::from_slice(body_buffer.as_slice()) {
            Ok(events) => events,
            Err(error) => {
                error!("Unable to receive input because of deserialization, {}", error);
                return None;
            }
        };

        Some(input)
    }
}

pub struct SyncFrameSender {
    pub writer: TcpStream,
}

impl SyncFrameSender {
    pub fn send_frame(&mut self, frame: Frame) -> Option<()> {
        match serde_json::to_vec(&frame) {
            Ok(body) => {
                let header = match u32::try_from(body.len()) {
                    Ok(body_length) => body_length.to_le_bytes(),
                    Err(error) => {
                        error!("Unable to send frame because of body length {}", error);
                        return None;
                    }
                };
                if let Err(error) = self.writer.write_all(&header) {
                    error!("Unable to send frame because of header write, {}", error);
                    return None;
                }
                if let Err(error) = self.writer.write_all(&body) {
                    error!("Unable to send frame because of body write, {}", error);
                    return None;
                }
            }
            Err(error) => {
                error!("Unable to send frame because of serialization, {}", error)
            }
        }

        return Some(());
    }
}
