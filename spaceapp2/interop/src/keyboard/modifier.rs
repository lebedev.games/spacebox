#![allow(dead_code)]

const NO_MODIFIER: u16 = 0x0000;
const L_SHIFT: u16 = 0x0001;
const R_SHIFT: u16 = 0x0002;
const L_CTRL: u16 = 0x0040;
const R_CTRL: u16 = 0x0080;
const L_ALT: u16 = 0x0100;
const R_ALT: u16 = 0x0200;
const L_GUI: u16 = 0x0400;
const R_GUI: u16 = 0x0800;
const NUM: u16 = 0x1000;
const CAPS: u16 = 0x2000;
const MODE: u16 = 0x4000;
const RESERVED: u16 = 0x8000;
