use std::fs::File;
use std::io;
use std::io::BufReader;

use image::{load, ImageError, ImageFormat};

pub struct TextureData {
    pub id: String,
    pub size: (u32, u32),
    pub data: Vec<u8>,
}

#[derive(Debug)]
pub enum ImportTextureError {
    Io(io::Error),
    ImageError(ImageError),
}

pub fn import_texture(path: &str) -> Result<TextureData, ImportTextureError> {
    let file = File::open(path)?;
    let image = load(BufReader::new(file), ImageFormat::Png)?.to_rgba8();
    let size = image.dimensions();
    let data = image.into_raw();
    Ok(TextureData { id: path.to_string(), size, data })
}

impl From<io::Error> for ImportTextureError {
    fn from(error: io::Error) -> Self {
        Self::Io(error)
    }
}

impl From<ImageError> for ImportTextureError {
    fn from(error: ImageError) -> Self {
        Self::ImageError(error)
    }
}
