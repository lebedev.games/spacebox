use std::time::Duration;

use interop::{InputEvent, Scancode};
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct CameraData {
    aspect_ratio: f32,
    position: (f32, f32, f32),
    direction: (f32, f32, f32),
}

pub struct Camera {
    pub data: CameraData,
    moving_up: bool,
    moving_left: bool,
    moving_down: bool,
    moving_right: bool,
    moving_forward: bool,
    moving_backward: bool,
}

impl Camera {
    pub fn from_data(data: CameraData) -> Camera {
        Camera {
            data,
            moving_up: false,
            moving_left: false,
            moving_down: false,
            moving_right: false,
            moving_forward: false,
            moving_backward: false,
        }
    }

    pub fn new() -> Camera {
        Camera {
            data: CameraData {
                aspect_ratio: 1024.0 / 768.0,
                position: (0.1, 0.1, 1.0),
                direction: (0.0, 0.0, -1.0),
            },
            moving_up: false,
            moving_left: false,
            moving_down: false,
            moving_right: false,
            moving_forward: false,
            moving_backward: false,
        }
    }

    pub fn set_position(&mut self, position: (f32, f32, f32)) {
        self.data.position = position;
    }

    pub fn set_direction(&mut self, direction: (f32, f32, f32)) {
        self.data.direction = direction;
    }

    pub fn get_perspective(&self) -> [[f32; 4]; 4] {
        let fov: f32 = 3.141592 / 2.0;
        let zfar = 1024.0;
        let znear = 0.1;

        let f = 1.0 / (fov / 2.0).tan();

        // note: remember that this is column-major, so the lines of code are actually
        // columns
        [
            [f / self.data.aspect_ratio, 0.0, 0.0, 0.0],
            [0.0, f, 0.0, 0.0],
            [0.0, 0.0, (zfar + znear) / (zfar - znear), 1.0],
            [0.0, 0.0, -(2.0 * zfar * znear) / (zfar - znear), 0.0],
        ]
    }

    pub fn get_view(&self) -> [[f32; 4]; 4] {
        let f = {
            let f = self.data.direction;
            let len = f.0 * f.0 + f.1 * f.1 + f.2 * f.2;
            let len = len.sqrt();
            (f.0 / len, f.1 / len, f.2 / len)
        };

        let up = (0.0, 1.0, 0.0);

        let s = (f.1 * up.2 - f.2 * up.1, f.2 * up.0 - f.0 * up.2, f.0 * up.1 - f.1 * up.0);

        let s_norm = {
            let len = s.0 * s.0 + s.1 * s.1 + s.2 * s.2;
            let len = len.sqrt();
            (s.0 / len, s.1 / len, s.2 / len)
        };

        let u = (
            s_norm.1 * f.2 - s_norm.2 * f.1,
            s_norm.2 * f.0 - s_norm.0 * f.2,
            s_norm.0 * f.1 - s_norm.1 * f.0,
        );

        let position = self.data.position;
        let p = (
            -position.0 * s.0 - position.1 * s.1 - position.2 * s.2,
            -position.0 * u.0 - position.1 * u.1 - position.2 * u.2,
            -position.0 * f.0 - position.1 * f.1 - position.2 * f.2,
        );

        // note: remember that this is column-major, so the lines of code are actually
        // columns
        [[s_norm.0, u.0, f.0, 0.0], [s_norm.1, u.1, f.1, 0.0], [s_norm.2, u.2, f.2, 0.0], [
            p.0, p.1, p.2, 1.0,
        ]]
    }

    pub fn update(&mut self, time_delta: Duration, speed: f32) {
        let f = {
            let f = self.data.direction;
            let len = f.0 * f.0 + f.1 * f.1 + f.2 * f.2;
            let len = len.sqrt();
            (f.0 / len, f.1 / len, f.2 / len)
        };

        let up = (0.0, 1.0, 0.0);

        let s = (f.1 * up.2 - f.2 * up.1, f.2 * up.0 - f.0 * up.2, f.0 * up.1 - f.1 * up.0);

        let s = {
            let len = s.0 * s.0 + s.1 * s.1 + s.2 * s.2;
            let len = len.sqrt();
            (s.0 / len, s.1 / len, s.2 / len)
        };

        let u = (s.1 * f.2 - s.2 * f.1, s.2 * f.0 - s.0 * f.2, s.0 * f.1 - s.1 * f.0);

        let delta = time_delta.as_secs_f32() * speed;

        if self.moving_up {
            self.data.position.0 += u.0 * delta;
            self.data.position.1 += u.1 * delta;
            self.data.position.2 += u.2 * delta;
        }

        if self.moving_left {
            self.data.position.0 -= s.0 * delta;
            self.data.position.1 -= s.1 * delta;
            self.data.position.2 -= s.2 * delta;
        }

        if self.moving_down {
            self.data.position.0 -= u.0 * delta;
            self.data.position.1 -= u.1 * delta;
            self.data.position.2 -= u.2 * delta;
        }

        if self.moving_right {
            self.data.position.0 += s.0 * delta;
            self.data.position.1 += s.1 * delta;
            self.data.position.2 += s.2 * delta;
        }

        if self.moving_forward {
            self.data.position.0 += f.0 * delta;
            self.data.position.1 += f.1 * delta;
            self.data.position.2 += f.2 * delta;
        }

        if self.moving_backward {
            self.data.position.0 -= f.0 * delta;
            self.data.position.1 -= f.1 * delta;
            self.data.position.2 -= f.2 * delta;
        }
    }

    pub fn process_input(&mut self, event: &InputEvent) {
        let (pressed, key) = match event {
            InputEvent::KeyDown { scancode, .. } => (true, scancode),
            InputEvent::KeyUp { scancode, .. } => (false, scancode),
            _ => return,
        };
        match key {
            Scancode::Up => self.moving_up = pressed,
            Scancode::Down => self.moving_down = pressed,
            Scancode::A => self.moving_left = pressed,
            Scancode::D => self.moving_right = pressed,
            Scancode::W => self.moving_forward = pressed,
            Scancode::S => self.moving_backward = pressed,
            _ => (),
        };
    }
}
