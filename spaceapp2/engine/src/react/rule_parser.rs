use crate::react::{
    parse_style,
    Combinator,
    Compound,
    Condition,
    Pseudo,
    Rule,
    Selector,
    Style,
    Stylesheet,
};

pub fn parse_condition(code: &str) -> Condition {
    let code = code.trim();
    match code.chars().nth(0) {
        Some(char) => match char {
            '*' => Condition::Universal,
            '.' => Condition::Class(code[1..].into()),
            '#' => Condition::Id(code[1..].into()),
            '[' => {
                let pair: Vec<&str> = code[1..code.len() - 1].split('=').collect();
                Condition::Attribute {
                    attribute: pair[0].trim().into(),
                    value: pair[pair.len() - 1].trim().into(),
                }
            }
            ':' => {
                if code.chars().nth(1) == Some(':') {
                    Condition::PseudoElement(code[2..].into())
                } else {
                    let keyword = code;
                    let pseudo = match keyword {
                        ":active" => Pseudo::Active,
                        ":checked" => Pseudo::Checked,
                        ":default" => Pseudo::Default,
                        ":disabled" => Pseudo::Disabled,
                        ":empty" => Pseudo::Empty,
                        ":enabled" => Pseudo::Enabled,
                        ":first-child" => Pseudo::FirstChild,
                        ":first-of-type" => Pseudo::FirstOfType,
                        ":focus" => Pseudo::Focus,
                        ":fullscreen" => Pseudo::Fullscreen,
                        ":hover" => Pseudo::Hover,
                        ":in-range" => Pseudo::InRange,
                        ":indeterminate" => Pseudo::Indeterminate,
                        ":invalid" => Pseudo::Invalid,
                        ":last-child" => Pseudo::LastChild,
                        ":last-of-type" => Pseudo::LastOfType,
                        ":link" => Pseudo::Link,
                        ":only-of-type" => Pseudo::OnlyOfType,
                        ":only-child" => Pseudo::OnlyChild,
                        ":optional" => Pseudo::Optional,
                        ":out-of-range" => Pseudo::OutOfRange,
                        ":read-only" => Pseudo::ReadOnly,
                        ":read-write" => Pseudo::ReadWrite,
                        ":required" => Pseudo::Required,
                        ":root" => Pseudo::Root,
                        ":target" => Pseudo::Target,
                        ":valid" => Pseudo::Valid,
                        ":visited" => Pseudo::Visited,
                        _ => {
                            error!("Unable to parse pseudo class, unknown keyword {}", keyword);
                            Pseudo::Root
                        }
                    };
                    Condition::PseudoClass(pseudo)
                }
            }
            _ => Condition::Type(code.into()),
        },
        None => {
            error!("Unable to parse condition, no code");
            Condition::Universal
        }
    }
}

pub fn is_compound(code: &str) -> bool {
    if code.is_empty() {
        return false;
    }

    for (p, char) in code[1..].chars().enumerate() {
        if [':', '.', '[', '#'].contains(&char) {
            if p == 0 && char == ':' && code.starts_with(":") {
                continue;
            }
            return true;
        }
    }
    return false;
}

pub fn parse_compound(code: &str) -> Compound {
    let code = code.trim();
    let mut conditions = Vec::new();
    let mut current = 0;
    for (ptr, char) in code.chars().enumerate() {
        if ptr > 0 {
            if [':', '.', '[', '#'].contains(&char) {
                if code.chars().nth(ptr + 1) == Some(':') {
                    continue;
                }
                if code.chars().nth(ptr - 1) == Some(':') {
                    // parse pseudo element
                    let condition = parse_condition(&code[current..ptr - 1]);
                    conditions.push(condition);
                    current = ptr - 1;
                } else {
                    let condition = parse_condition(&code[current..ptr]);
                    conditions.push(condition);
                    current = ptr;
                }
            }
        }
    }
    let condition = parse_condition(&code[current..]);
    conditions.push(condition);

    conditions
}

pub fn parse_selector(code: &str) -> Selector {
    let code = code.trim();
    let items: Vec<&str> = code.split(',').collect();
    if items.len() == 1 {
        let mut start_combinator = 0;
        for (p, char) in code.chars().enumerate() {
            if [' ', '+', '>', '~'].contains(&char) && start_combinator == 0 {
                start_combinator = p;
                continue;
            }
            if ![' ', '+', '>', '~'].contains(&char) && start_combinator > 0 {
                let end_combinator = p;
                let combinator =
                    &code[start_combinator..end_combinator].trim().chars().nth(0).unwrap_or(' ');

                let first = parse_compound(&code[0..start_combinator]);
                let second = parse_compound(&code[end_combinator..]);
                let combinator = match combinator {
                    '+' => Combinator::Adjacent { first, second },
                    '>' => Combinator::Child { first, second },
                    '~' => Combinator::Sibling { first, second },
                    _ => Combinator::Descendant { first, second },
                };
                return Selector::Complex(combinator);
            }
        }
        if is_compound(code) {
            Selector::Compound(parse_compound(code))
        } else {
            Selector::Simple(parse_condition(code))
        }
    } else {
        let mut selectors = Vec::new();
        for item in items {
            selectors.push(parse_selector(item));
        }
        Selector::List(selectors)
    }
}

pub fn parse_rule(code: &str) -> Rule {
    let (selector, style) = if let Some(position) = code.find('{') {
        let selector = parse_selector(&code[0..position]);
        let style = parse_style(&code[position + 1..code.len() - 1]);
        (selector, style)
    } else {
        error!("Unable to parse rule correctly, no selector in code");
        (Selector::default(), Style::default())
    };

    Rule { selector, style }
}

pub fn parse_stylesheet(code: &str) -> Stylesheet {
    let mut rules = Vec::new();

    let mut previous = 0;
    for (ptr, symbol) in code.chars().enumerate() {
        if symbol == '}' {
            rules.push(parse_rule(&code[previous..ptr + 1].trim()));
            previous = ptr + 1;
        }
    }

    Stylesheet { rules }
}
