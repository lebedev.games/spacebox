use std::collections::HashMap;

use regex::{Captures, Regex};

use crate::react::color_parser::{
    parse_color_from_hex,
    parse_color_from_hsla,
    parse_color_from_name,
    parse_color_from_rgba,
};
use crate::react::style::*;

pub fn parse_css_number(code: &str) -> f32 {
    code.parse().unwrap_or(0.0)
}

pub fn parse_css_color(code: &str) -> Color {
    let code = code.trim();
    if let Some(code) = code.strip_prefix('#') {
        parse_color_from_hex(code)
    } else if code.starts_with("rgba") {
        parse_color_from_rgba(code)
    } else if code.starts_with("rgb") {
        parse_color_from_rgba(code)
    } else if code.starts_with("hsla") {
        parse_color_from_hsla(code)
    } else if code.starts_with("hsl") {
        parse_color_from_hsla(code)
    } else if code.starts_with("currentcolor") {
        Color::Current
    } else {
        parse_color_from_name(code)
    }
}

pub struct CssParser {
    length_pattern: Regex,
}

impl CssParser {
    pub fn new() -> Self {
        CssParser { length_pattern: Regex::new(r#"([-\d\.]+)(px|%|\w+|)"#).unwrap() }
    }

    pub fn parse_css_length(&self, code: &str) -> Length {
        let code = code.trim();
        for group in self.length_pattern.captures_iter(code) {
            let value = &group[1];
            let unit = &group[2];

            let value: f32 = value.parse::<f32>().unwrap_or(0.0);

            return match unit {
                "" => Length::Px(value as u32),
                "px" => Length::Px(value as u32),
                "em" => Length::Em(value),
                "rem" => Length::Rem(value),
                "%" => Length::Percent(value),
                _ => {
                    error!("Unable to parse length {}, not implemented yet", code);
                    continue;
                }
            };
        }

        Length::Percent(100.0)
    }
}

pub fn parse_css_length(code: &str) -> Length {
    let code = code.trim();
    let digits = "-0123456789.";
    let mut p = code.len();
    for (ptr, c) in code.chars().enumerate() {
        if !digits.contains(c) {
            p = ptr;
            break;
        }
    }
    let value = &code[0..p];
    let unit = &code[p..];
    let value: f32 = value.parse::<f32>().unwrap_or(0.0);
    match unit {
        "" => Length::Px(value as u32),
        "px" => Length::Px(value as u32),
        "em" => Length::Em(value),
        "rem" => Length::Rem(value),
        "%" => Length::Percent(value),
        _ => {
            error!("Unable to parse length {}, not implemented yet", code);
            Length::Percent(100.0)
        }
    }
}

pub fn parse_css_url(code: &str) -> Url {
    lazy_static! {
        static ref URL_PATTERN: Regex = Regex::new(r#"\("?'?([^'^"]+)'?"?\)"#).unwrap();
    }
    for group in URL_PATTERN.captures_iter(code) {
        let path: &str = &group[1];
        if path.starts_with("http") {
            return Url::Absolute(path.into());
        } else if path.starts_with("data:") {
            let data: Vec<&str> = (&path[5..]).split(',').collect();
            match (data.get(0), data.get(1)) {
                (Some(content_type), Some(content)) => {
                    return Url::Data {
                        content_type: content_type.to_string(),
                        content: content.to_string(),
                    };
                }
                _ => {
                    error!("Unable to parse url uri")
                }
            }
        } else {
            return Url::Relative(path.into());
        }
    }

    Url::Relative("".into())
}

pub fn parse_css_position(code: &str) -> BackgroundPosition {
    let code = code.trim();
    let items: Vec<&str> = code.split_whitespace().collect();
    let (horizontal, vertical) = match (items.get(0), items.get(1)) {
        (Some(horizontal), Some(vertical)) => (horizontal.trim(), vertical.trim()),
        (Some(horizontal), _) => (horizontal.trim(), ""),
        _ => ("0%", "0%"),
    };
    if ["left", "right", "center"].contains(&horizontal) {
        let horizontal = match &horizontal[..] {
            "left" => HorizontalKeyword::Left,
            "right" => HorizontalKeyword::Right,
            _ => HorizontalKeyword::Center,
        };
        let vertical = match &vertical[..] {
            "top" => VerticalKeyword::Top,
            "Bottom" => VerticalKeyword::Bottom,
            _ => VerticalKeyword::Center,
        };
        BackgroundPosition::Keyword { horizontal, vertical }
    } else {
        BackgroundPosition::Length {
            horizontal: parse_css_length(horizontal),
            vertical: parse_css_length(vertical),
        }
    }
}

/// Used to handle parsing of compound property value with color
pub fn mask_colors(code: &str) -> (String, HashMap<String, String>) {
    let code = code.trim();
    let mut colors = HashMap::new();

    lazy_static! {
        static ref COLOR_PATTERN: Regex = Regex::new(r#"((rgb|rgba|hsl|hsla)\(.+\))"#).unwrap();
    }

    let code = COLOR_PATTERN
        .replace_all(code, |captures: &Captures| {
            let key = format!("$color{}", colors.len());
            colors.insert(key.clone(), captures[0].to_string());
            key
        })
        .to_string();

    (code, colors)
}

pub fn parse_css_shadow(code: &str, colors: &HashMap<String, String>) -> Shadow {
    let code = code.trim();
    let outset = !code.contains("inset");
    let code = code.replace("inset", "");
    let props: Vec<&str> = code.split_whitespace().collect();
    match props.len() {
        5 => Shadow {
            horizontal_offset: parse_css_length(props[0]),
            vertical_offset: parse_css_length(props[1]),
            blur: parse_css_length(props[2]),
            spread: parse_css_length(props[3]),
            color: parse_css_color(colors.get(props[4]).unwrap_or(&props[4].to_string())),
            outset,
        },
        4 => Shadow {
            horizontal_offset: parse_css_length(props[0]),
            vertical_offset: parse_css_length(props[1]),
            blur: parse_css_length(props[2]),
            spread: Length::Px(0),
            color: parse_css_color(colors.get(props[3]).unwrap_or(&props[3].to_string())),
            outset,
        },
        3 => Shadow {
            horizontal_offset: parse_css_length(props[0]),
            vertical_offset: parse_css_length(props[1]),
            blur: Length::Px(0),
            spread: Length::Px(0),
            color: parse_css_color(colors.get(props[2]).unwrap_or(&props[2].to_string())),
            outset,
        },
        2 => Shadow {
            horizontal_offset: parse_css_length(props[0]),
            vertical_offset: parse_css_length(props[1]),
            blur: Length::Px(0),
            spread: Length::Px(0),
            color: Color::default(),
            outset,
        },
        _ => {
            error!("Unable to parse shadow, {}", code);
            Shadow {
                horizontal_offset: Length::Px(0),
                vertical_offset: Length::Px(0),
                blur: Length::Px(0),
                spread: Length::Px(0),
                color: Color::default(),
                outset: true,
            }
        }
    }
}

pub fn parse_border_width(code: &str) -> Length {
    match &code[..] {
        "medium" => Length::Px(2),
        "thin" => Length::Px(1),
        "thick" => Length::Px(3),
        value => parse_css_length(value),
    }
}

pub fn parse_border_color(code: &str) -> BorderSideColor {
    match &code[..] {
        "transparent" => BorderSideColor::Transparent,
        value => BorderSideColor::Color(parse_css_color(value)),
    }
}

pub fn parse_border_image_slice(code: &str) -> BorderImageSideSlice {
    match &code[..] {
        "fill" => BorderImageSideSlice::Fill,
        value => BorderImageSideSlice::Length(parse_css_length(value)),
    }
}

pub fn parse_border_image_outset(code: &str) -> BorderImageSideOutset {
    match &code[..] {
        value => BorderImageSideOutset::Length(parse_css_length(value)),
    }
}

pub fn parse_border_image_width(code: &str) -> BorderImageSideWidth {
    match &code[..] {
        "auto" => BorderImageSideWidth::Auto,
        value => BorderImageSideWidth::Length(parse_css_length(value)),
    }
}

pub fn parse_border_style(code: &str) -> BorderSideStyle {
    match &code[..] {
        "none" => BorderSideStyle::None,
        "hidden" => BorderSideStyle::Hidden,
        "dotted" => BorderSideStyle::Dotted,
        "dashed" => BorderSideStyle::Dashed,
        "solid" => BorderSideStyle::Solid,
        "double" => BorderSideStyle::Double,
        "groove" => BorderSideStyle::Groove,
        "ridge" => BorderSideStyle::Ridge,
        "inset" => BorderSideStyle::Inset,
        "outset" => BorderSideStyle::Outset,
        _ => {
            error!("Unable to parse border style {}", code);
            BorderSideStyle::None
        }
    }
}

pub fn parse_flex_basis(code: &str) -> FlexBasis {
    match code {
        "auto" => FlexBasis::Auto,

        value => FlexBasis::Length(parse_css_length(value)),
    }
}

pub fn parse_font_family(code: &str) -> String {
    let names: Vec<&str> = code.split_whitespace().collect();
    names[0].trim().to_string()
}

pub fn parse_margin_side(code: &str) -> MarginSide {
    match &code[..] {
        "auto" => MarginSide::Auto,

        value => MarginSide::Length(parse_css_length(value)),
    }
}

pub fn parse_style(code: &str) -> Style {
    let mut style = Style::default();
    let declarations: Vec<&str> = code.split(';').collect();
    for declaration in declarations.iter() {
        let items: Vec<&str> = declaration.split(':').collect();
        let (name, repr) = match (items.get(0), items.get(1)) {
            (Some(name), Some(repr)) => (name.trim().to_lowercase(), repr.trim().to_lowercase()),
            _ => {
                error!("Unable to parse property, {}", declaration);
                continue;
            }
        };
        match &name[..] {
            "align-content" => {
                style.align_content = match &repr[..] {
                    "stretch" => AlignContent::Stretch,
                    "center" => AlignContent::Center,
                    "flex-start" => AlignContent::FlexStart,
                    "flex-end" => AlignContent::FlexEnd,
                    "space-between" => AlignContent::SpaceBetween,
                    "space-around" => AlignContent::SpaceAround,
                    _ => style.align_content,
                }
            }
            "align-items" => {
                style.align_items = match &repr[..] {
                    "stretch" => AlignItems::Stretch,
                    "center" => AlignItems::Center,
                    "flex-start" => AlignItems::FlexStart,
                    "flex-end" => AlignItems::FlexEnd,
                    "baseline" => AlignItems::Baseline,
                    _ => style.align_items,
                }
            }
            "align-self" => {
                style.align_self = match &repr[..] {
                    "stretch" => AlignSelf::Stretch,
                    "center" => AlignSelf::Center,
                    "flex-start" => AlignSelf::FlexStart,
                    "flex-end" => AlignSelf::FlexEnd,
                    "baseline" => AlignSelf::Baseline,
                    _ => style.align_self,
                }
            }
            "background-attachment" => {
                style.background.attachment = match &repr[..] {
                    "scroll" => BackgroundAttachment::Scroll,
                    "fixed" => BackgroundAttachment::Fixed,
                    "local" => BackgroundAttachment::Local,
                    _ => style.background.attachment,
                }
            }
            "background-blend-mode" => {
                style.background.blend_mode = match &repr[..] {
                    "normal" => BackgroundBlendMode::Normal,
                    "multiply" => BackgroundBlendMode::Multiply,
                    "screen" => BackgroundBlendMode::Screen,
                    "overlay" => BackgroundBlendMode::Overlay,
                    "darken" => BackgroundBlendMode::Darken,
                    "lighten" => BackgroundBlendMode::Lighten,
                    "color-dodge" => BackgroundBlendMode::ColorDodge,
                    "saturation" => BackgroundBlendMode::Saturation,
                    "color" => BackgroundBlendMode::Color,
                    "luminosity" => BackgroundBlendMode::Luminosity,
                    _ => style.background.blend_mode,
                }
            }
            "background-clip" => {
                style.background.clip = match &repr[..] {
                    "border-box" => BackgroundClip::BorderBox,
                    "padding-box" => BackgroundClip::PaddingBox,
                    "content-box" => BackgroundClip::ContentBox,
                    _ => style.background.clip,
                }
            }
            "background-color" => {
                style.background.color = match &repr[..] {
                    "transparent" => BackgroundColor::Transparent,
                    code => BackgroundColor::Color(parse_css_color(code)),
                }
            }
            "background-image" => {
                style.background.image = match &repr[..] {
                    "none" => BackgroundImage::None,
                    code => BackgroundImage::Url(parse_css_url(code)),
                }
            }
            "background-origin" => {
                style.background.origin = match &repr[..] {
                    "border-box" => BackgroundOrigin::BorderBox,
                    "padding-box" => BackgroundOrigin::PaddingBox,
                    "content-box" => BackgroundOrigin::ContentBox,
                    _ => style.background.origin,
                }
            }
            "background-position" => {
                style.background.position = match &repr[..] {
                    code => parse_css_position(code),
                }
            }
            "background-repeat" => {
                style.background.repeat = match &repr[..] {
                    "repeat" => BackgroundRepeat::Repeat,
                    "repeat-x" => BackgroundRepeat::RepeatX,
                    "repeat-y" => BackgroundRepeat::RepeatY,
                    "no-repeat" => BackgroundRepeat::NoRepeat,
                    _ => style.background.repeat,
                }
            }
            "background-size" => {
                style.background.size = match &repr[..] {
                    "cover" => BackgroundSize::Cover,
                    "contain" => BackgroundSize::Contain,
                    code => {
                        let items: Vec<&str> = code.split_whitespace().collect();
                        match (items.get(0), items.get(1)) {
                            (Some(width), Some(height)) => BackgroundSize::Length {
                                width: parse_css_length(width),
                                height: parse_css_length(height),
                            },
                            (Some(width), _) => BackgroundSize::Length {
                                width: parse_css_length(width),
                                height: parse_css_length(width),
                            },
                            _ => style.background.size,
                        }
                    }
                }
            }
            "border" => {
                let props: Vec<&str> = repr.split_whitespace().collect();
                match props.len() {
                    3 => {
                        let border_width = parse_border_width(props[0]);
                        let border_style = parse_border_style(props[1]);
                        let border_color = parse_border_color(props[2]);

                        style.border.width.top = border_width;
                        style.border.style.top = border_style;
                        style.border.color.top = border_color.clone();

                        style.border.width.bottom = border_width;
                        style.border.style.bottom = border_style;
                        style.border.color.bottom = border_color.clone();

                        style.border.width.left = border_width;
                        style.border.style.left = border_style;
                        style.border.color.left = border_color.clone();

                        style.border.width.right = border_width;
                        style.border.style.right = border_style;
                        style.border.color.right = border_color.clone();
                    }
                    _ => {
                        error!("Unable to parse border, {}", repr);
                    }
                }
            }
            "border-top" => {
                let props: Vec<&str> = repr.split_whitespace().collect();
                match props.len() {
                    3 => {
                        style.border.width.top = parse_border_width(props[0]);
                        style.border.style.top = parse_border_style(props[1]);
                        style.border.color.top = parse_border_color(props[2]);
                    }
                    _ => {
                        error!("Unable to parse border-top, {}", repr);
                    }
                }
            }
            "border-top-color" => {
                style.border.color.top = parse_border_color(&repr);
            }
            "border-top-left-radius" => {
                style.border.radius.top_left = parse_css_length(&repr);
            }
            "border-top-right-radius" => {
                style.border.radius.top_right = parse_css_length(&repr);
            }
            "border-top-style" => {
                style.border.style.top = parse_border_style(&repr);
            }
            "border-top-width" => {
                style.border.width.top = parse_border_width(&repr);
            }
            "border-bottom" => {
                let props: Vec<&str> = repr.split_whitespace().collect();
                match props.len() {
                    3 => {
                        style.border.width.bottom = parse_border_width(props[0]);
                        style.border.style.bottom = parse_border_style(props[1]);
                        style.border.color.bottom = parse_border_color(props[2]);
                    }
                    _ => {
                        error!("Unable to parse border-bottom, {}", repr);
                    }
                }
            }
            "border-bottom-color" => {
                style.border.color.bottom = parse_border_color(&repr);
            }
            "border-bottom-left-radius" => {
                style.border.radius.bottom_left = parse_css_length(&repr);
            }
            "border-bottom-right-radius" => {
                style.border.radius.bottom_right = parse_css_length(&repr);
            }
            "border-bottom-style" => {
                style.border.style.bottom = parse_border_style(&repr);
            }
            "border-bottom-width" => {
                style.border.width.bottom = parse_border_width(&repr);
            }
            "border-right" => {
                let props: Vec<&str> = repr.split_whitespace().collect();
                match props.len() {
                    3 => {
                        style.border.width.right = parse_border_width(props[0]);
                        style.border.style.right = parse_border_style(props[1]);
                        style.border.color.right = parse_border_color(props[2]);
                    }
                    _ => {
                        error!("Unable to parse border-right, {}", repr);
                    }
                }
            }
            "border-right-color" => {
                style.border.color.right = parse_border_color(&repr);
            }
            "border-right-style" => {
                style.border.style.right = parse_border_style(&repr);
            }
            "border-right-width" => {
                style.border.width.right = parse_border_width(&repr);
            }
            "border-left" => {
                let props: Vec<&str> = repr.split_whitespace().collect();
                match props.len() {
                    3 => {
                        style.border.width.left = parse_border_width(props[0]);
                        style.border.style.left = parse_border_style(props[1]);
                        style.border.color.left = parse_border_color(props[2]);
                    }
                    _ => {
                        error!("Unable to parse border-left, {}", repr);
                    }
                }
            }
            "border-left-color" => {
                style.border.color.left = parse_border_color(&repr);
            }
            "border-left-style" => {
                style.border.style.left = parse_border_style(&repr);
            }
            "border-left-width" => {
                style.border.width.left = parse_border_width(&repr);
            }
            "border-spacing" => {
                style.border.spacing = match &repr[..] {
                    value => BorderSpacing::Length(parse_css_length(value)),
                }
            }
            "border-radius" => {
                let sides: Vec<&str> = repr.split_whitespace().collect();
                match sides.len() {
                    3 => {
                        style.border.radius = BorderRadius {
                            top_left: parse_css_length(sides[0]),
                            top_right: parse_css_length(sides[1]),
                            bottom_right: parse_css_length(sides[2]),
                            bottom_left: parse_css_length(sides[1]),
                        }
                    }
                    2 => {
                        style.border.radius = BorderRadius {
                            top_left: parse_css_length(sides[0]),
                            top_right: parse_css_length(sides[1]),
                            bottom_right: parse_css_length(sides[0]),
                            bottom_left: parse_css_length(sides[1]),
                        }
                    }
                    1 => {
                        style.border.radius = BorderRadius {
                            top_left: parse_css_length(sides[0]),
                            top_right: parse_css_length(sides[0]),
                            bottom_right: parse_css_length(sides[0]),
                            bottom_left: parse_css_length(sides[0]),
                        }
                    }
                    // 4 +
                    _ => {
                        style.border.radius = BorderRadius {
                            top_left: parse_css_length(sides[0]),
                            top_right: parse_css_length(sides[1]),
                            bottom_right: parse_css_length(sides[2]),
                            bottom_left: parse_css_length(sides[3]),
                        }
                    }
                }
            }
            "border-style" => {
                let sides: Vec<&str> = repr.split_whitespace().collect();
                match sides.len() {
                    3 => {
                        style.border.style = BorderStyle {
                            top: parse_border_style(sides[0]),
                            right: parse_border_style(sides[1]),
                            bottom: parse_border_style(sides[2]),
                            left: parse_border_style(sides[1]),
                        }
                    }
                    2 => {
                        style.border.style = BorderStyle {
                            top: parse_border_style(sides[0]),
                            right: parse_border_style(sides[1]),
                            bottom: parse_border_style(sides[0]),
                            left: parse_border_style(sides[1]),
                        }
                    }
                    1 => {
                        style.border.style = BorderStyle {
                            top: parse_border_style(sides[0]),
                            right: parse_border_style(sides[0]),
                            bottom: parse_border_style(sides[0]),
                            left: parse_border_style(sides[0]),
                        }
                    }
                    // 4 +
                    _ => {
                        style.border.style = BorderStyle {
                            top: parse_border_style(sides[0]),
                            right: parse_border_style(sides[1]),
                            bottom: parse_border_style(sides[2]),
                            left: parse_border_style(sides[3]),
                        }
                    }
                }
            }
            "border-collapse" => {
                style.border.collapse = match &repr[..] {
                    "separate" => BorderCollapse::Separate,
                    "collapse" => BorderCollapse::Collapse,

                    _ => style.border.collapse,
                }
            }
            "border-color" => {
                let sides: Vec<&str> = repr.split_whitespace().collect();
                match sides.len() {
                    3 => {
                        style.border.color.top = parse_border_color(sides[0]);
                        style.border.color.right = parse_border_color(sides[1]);
                        style.border.color.bottom = parse_border_color(sides[2]);
                        style.border.color.left = parse_border_color(sides[1]);
                    }
                    2 => {
                        style.border.color.top = parse_border_color(sides[0]);
                        style.border.color.right = parse_border_color(sides[1]);
                        style.border.color.bottom = parse_border_color(sides[0]);
                        style.border.color.left = parse_border_color(sides[1]);
                    }
                    1 => {
                        style.border.color.top = parse_border_color(sides[0]);
                        style.border.color.right = parse_border_color(sides[0]);
                        style.border.color.bottom = parse_border_color(sides[0]);
                        style.border.color.left = parse_border_color(sides[0]);
                    }
                    // 4 +
                    _ => {
                        style.border.color.top = parse_border_color(sides[0]);
                        style.border.color.right = parse_border_color(sides[1]);
                        style.border.color.bottom = parse_border_color(sides[2]);
                        style.border.color.left = parse_border_color(sides[3]);
                    }
                }
            }
            "border-width" => {
                let sides: Vec<&str> = repr.split_whitespace().collect();
                match sides.len() {
                    3 => {
                        style.border.width.top = parse_border_width(sides[0]);
                        style.border.width.right = parse_border_width(sides[1]);
                        style.border.width.bottom = parse_border_width(sides[2]);
                        style.border.width.left = parse_border_width(sides[1]);
                    }
                    2 => {
                        style.border.width.top = parse_border_width(sides[0]);
                        style.border.width.right = parse_border_width(sides[1]);
                        style.border.width.bottom = parse_border_width(sides[0]);
                        style.border.width.left = parse_border_width(sides[1]);
                    }
                    1 => {
                        style.border.width.top = parse_border_width(sides[0]);
                        style.border.width.right = parse_border_width(sides[0]);
                        style.border.width.bottom = parse_border_width(sides[0]);
                        style.border.width.left = parse_border_width(sides[0]);
                    }
                    // 4 +
                    _ => {
                        style.border.width.top = parse_border_width(sides[0]);
                        style.border.width.right = parse_border_width(sides[1]);
                        style.border.width.bottom = parse_border_width(sides[2]);
                        style.border.width.left = parse_border_width(sides[3]);
                    }
                }
            }
            "border-image-source" => {
                style.border.image.source = match &repr[..] {
                    "none" => BorderImageSource::None,

                    value => BorderImageSource::Image(parse_css_url(value)),
                }
            }
            "border-image-repeat" => {
                style.border.image.repeat = match &repr[..] {
                    "stretch" => BorderImageRepeat::Stretch,
                    "repeat" => BorderImageRepeat::Repeat,
                    "round" => BorderImageRepeat::Round,

                    _ => style.border.image.repeat,
                }
            }
            "border-image-outset" => {
                let sides: Vec<&str> = repr.split_whitespace().collect();
                match sides.len() {
                    3 => {
                        style.border.image.outset = BorderImageOutset {
                            top: parse_border_image_outset(sides[0]),
                            right: parse_border_image_outset(sides[1]),
                            bottom: parse_border_image_outset(sides[2]),
                            left: parse_border_image_outset(sides[1]),
                        }
                    }
                    2 => {
                        style.border.image.outset = BorderImageOutset {
                            top: parse_border_image_outset(sides[0]),
                            right: parse_border_image_outset(sides[1]),
                            bottom: parse_border_image_outset(sides[0]),
                            left: parse_border_image_outset(sides[1]),
                        }
                    }
                    1 => {
                        style.border.image.outset = BorderImageOutset {
                            top: parse_border_image_outset(sides[0]),
                            right: parse_border_image_outset(sides[0]),
                            bottom: parse_border_image_outset(sides[0]),
                            left: parse_border_image_outset(sides[0]),
                        }
                    }
                    // 4 +
                    _ => {
                        style.border.image.outset = BorderImageOutset {
                            top: parse_border_image_outset(sides[0]),
                            right: parse_border_image_outset(sides[1]),
                            bottom: parse_border_image_outset(sides[2]),
                            left: parse_border_image_outset(sides[3]),
                        }
                    }
                }
            }
            "border-image-slice" => {
                let sides: Vec<&str> = repr.split_whitespace().collect();
                match sides.len() {
                    3 => {
                        style.border.image.slice = BorderImageSlice {
                            top: parse_border_image_slice(sides[0]),
                            right: parse_border_image_slice(sides[1]),
                            bottom: parse_border_image_slice(sides[2]),
                            left: parse_border_image_slice(sides[1]),
                        }
                    }
                    2 => {
                        style.border.image.slice = BorderImageSlice {
                            top: parse_border_image_slice(sides[0]),
                            right: parse_border_image_slice(sides[1]),
                            bottom: parse_border_image_slice(sides[0]),
                            left: parse_border_image_slice(sides[1]),
                        }
                    }
                    1 => {
                        style.border.image.slice = BorderImageSlice {
                            top: parse_border_image_slice(sides[0]),
                            right: parse_border_image_slice(sides[0]),
                            bottom: parse_border_image_slice(sides[0]),
                            left: parse_border_image_slice(sides[0]),
                        }
                    }
                    // 4 +
                    _ => {
                        style.border.image.slice = BorderImageSlice {
                            top: parse_border_image_slice(sides[0]),
                            right: parse_border_image_slice(sides[1]),
                            bottom: parse_border_image_slice(sides[2]),
                            left: parse_border_image_slice(sides[3]),
                        }
                    }
                }
            }
            "border-image-width" => {
                let sides: Vec<&str> = repr.split_whitespace().collect();
                match sides.len() {
                    3 => {
                        style.border.image.width = BorderImageWidth {
                            top: parse_border_image_width(sides[0]),
                            right: parse_border_image_width(sides[1]),
                            bottom: parse_border_image_width(sides[2]),
                            left: parse_border_image_width(sides[1]),
                        }
                    }
                    2 => {
                        style.border.image.width = BorderImageWidth {
                            top: parse_border_image_width(sides[0]),
                            right: parse_border_image_width(sides[1]),
                            bottom: parse_border_image_width(sides[0]),
                            left: parse_border_image_width(sides[1]),
                        }
                    }
                    1 => {
                        style.border.image.width = BorderImageWidth {
                            top: parse_border_image_width(sides[0]),
                            right: parse_border_image_width(sides[0]),
                            bottom: parse_border_image_width(sides[0]),
                            left: parse_border_image_width(sides[0]),
                        }
                    }
                    // 4 +
                    _ => {
                        style.border.image.width = BorderImageWidth {
                            top: parse_border_image_width(sides[0]),
                            right: parse_border_image_width(sides[1]),
                            bottom: parse_border_image_width(sides[2]),
                            left: parse_border_image_width(sides[3]),
                        }
                    }
                }
            }
            "bottom" => {
                style.bottom = match &repr[..] {
                    "auto" => Bottom::Auto,

                    value => Bottom::Length(parse_css_length(value)),
                }
            }
            "box-decoration-break" => {
                style.box_decoration_break = match &repr[..] {
                    "slice" => BoxDecorationBreak::Slice,
                    "clone" => BoxDecorationBreak::Clone,

                    _ => style.box_decoration_break,
                }
            }
            "box-shadow" => {
                style.box_shadow = match &repr[..] {
                    "none" => BoxShadow::None,

                    value => {
                        let (value, colors) = mask_colors(value);
                        BoxShadow::Shadows(
                            value
                                .split(',')
                                .map(|value| parse_css_shadow(value, &colors))
                                .collect(),
                        )
                    }
                }
            }
            "box-sizing" => {
                style.box_sizing = match &repr[..] {
                    "content-box" => BoxSizing::ContentBox,
                    "border-box" => BoxSizing::BorderBox,

                    _ => style.box_sizing,
                }
            }
            "caption-side" => {
                style.caption_side = match &repr[..] {
                    "top" => CaptionSide::Top,
                    "bottom" => CaptionSide::Bottom,

                    _ => style.caption_side,
                }
            }
            "color" => {
                style.color = parse_css_color(&repr);
            }
            "display" => {
                style.display = match &repr[..] {
                    "inline" => Display::Inline,
                    "block" => Display::Block,
                    "contents" => Display::Contents,
                    "flex" => Display::Flex,
                    "grid" => Display::Grid,
                    "inline-block" => Display::InlineBlock,
                    "inline-flex" => Display::InlineFlex,
                    "inline-grid" => Display::InlineGrid,
                    "inline-table" => Display::InlineTable,
                    "list-item" => Display::ListItem,
                    "run-in" => Display::RunIn,
                    "table" => Display::Table,
                    "table-caption" => Display::TableCaption,
                    "table-column-group" => Display::TableColumnGroup,
                    "table-header-group" => Display::TableHeaderGroup,
                    "table-footer-group" => Display::TableFooterGroup,
                    "table-row-group" => Display::TableRowGroup,
                    "table-cell" => Display::TableCell,
                    "table-column" => Display::TableColumn,
                    "table-row" => Display::TableRow,
                    "none" => Display::None,

                    _ => style.display,
                }
            }
            "flex" => {
                let props: Vec<&str> = repr.split_whitespace().collect();
                match props.len() {
                    1 => {
                        style.flex.basis = parse_flex_basis(props[0]);
                    }
                    _ => {}
                }
            }
            "flex-basis" => {
                style.flex.basis = parse_flex_basis(&repr[..]);
            }
            "flex-direction" => {
                style.flex.direction = match &repr[..] {
                    "row" => FlexDirection::Row,
                    "row-reverse" => FlexDirection::RowReverse,
                    "column" => FlexDirection::Column,
                    "column-reverse" => FlexDirection::ColumnReverse,

                    _ => style.flex.direction,
                }
            }
            "flex-grow" => style.flex.grow = parse_css_number(&repr),
            "flex-shrink" => style.flex.shrink = parse_css_number(&repr),
            "flex-wrap" => {
                style.flex.wrap = match &repr[..] {
                    "nowrap" => FlexWrap::Nowrap,
                    "wrap" => FlexWrap::Wrap,
                    "wrap-reverse" => FlexWrap::WrapReverse,

                    _ => style.flex.wrap,
                }
            }
            "float" => {
                style.float = match &repr[..] {
                    "none" => Float::None,
                    "left" => Float::Left,
                    "right" => Float::Right,

                    _ => style.float,
                }
            }
            "font" => {
                style.font.family = Some(parse_font_family(&repr));
            }
            "font-family" => {
                style.font.family = Some(parse_font_family(&repr));
            }
            "font-kerning" => {
                style.font.kerning = match &repr[..] {
                    "normal" => FontKerning::Normal,
                    "auto" => FontKerning::Auto,
                    "none" => FontKerning::None,
                    _ => style.font.kerning,
                }
            }
            "font-size" => style.font.size = parse_css_length(&repr),
            "font-size-adjust" => {
                style.font.size_adjust = match &repr[..] {
                    "none" => FontSizeAdjust::None,

                    value => FontSizeAdjust::Number(parse_css_number(value)),
                }
            }
            "font-style" => {
                style.font.style = match &repr[..] {
                    "normal" => FontStyle::Normal,
                    "italic" => FontStyle::Italic,
                    "oblique" => FontStyle::Oblique,

                    _ => style.font.style,
                }
            }
            "font-variant" => {
                style.font.variant = match &repr[..] {
                    "normal" => FontVariant::Normal,
                    "small-caps" => FontVariant::SmallCaps,

                    _ => style.font.variant,
                }
            }
            "font-variant-caps" => {
                style.font.variant_caps = match &repr[..] {
                    "normal" => FontVariantCaps::Normal,
                    "small-caps" => FontVariantCaps::SmallCaps,
                    "all-small-caps" => FontVariantCaps::AllSmallCaps,
                    "petite-caps" => FontVariantCaps::PetiteCaps,
                    "all-petite-caps" => FontVariantCaps::AllPetiteCaps,
                    "unicase" => FontVariantCaps::Unicase,
                    "titling-caps" => FontVariantCaps::TitlingCaps,
                    "unset" => FontVariantCaps::Unset,

                    _ => style.font.variant_caps,
                }
            }
            "font-weight" => {
                style.font.weight = match &repr[..] {
                    "normal" => FontWeight::Normal,
                    "bold" => FontWeight::Bold,
                    "bolder" => FontWeight::Bolder,
                    "lighter" => FontWeight::Lighter,

                    value => FontWeight::Number(parse_css_number(value) as u32),
                }
            }
            "height" => {
                style.height = match &repr[..] {
                    "auto" => Height::Auto,

                    value => Height::Length(parse_css_length(value)),
                }
            }
            "justify-content" => {
                style.justify_content = match &repr[..] {
                    "flex-start" => JustifyContent::FlexStart,
                    "flex-end" => JustifyContent::FlexEnd,
                    "center" => JustifyContent::Center,
                    "space-between" => JustifyContent::SpaceBetween,
                    "space-around" => JustifyContent::SpaceAround,
                    "space-evenly" => JustifyContent::SpaceEvenly,

                    _ => style.justify_content,
                }
            }
            "left" => {
                style.left = match &repr[..] {
                    "auto" => Left::Auto,

                    value => Left::Length(parse_css_length(value)),
                }
            }
            "line-height" => {
                style.line_height = match &repr[..] {
                    "normal" => LineHeight::Normal,

                    value => LineHeight::Length(parse_css_length(value)),
                }
            }
            "margin" => {
                let sides: Vec<&str> = repr.split_whitespace().collect();
                match sides.len() {
                    3 => {
                        style.margin = Margin {
                            top: parse_margin_side(sides[0]),
                            right: parse_margin_side(sides[1]),
                            bottom: parse_margin_side(sides[2]),
                            left: parse_margin_side(sides[1]),
                        }
                    }
                    2 => {
                        style.margin = Margin {
                            top: parse_margin_side(sides[0]),
                            right: parse_margin_side(sides[1]),
                            bottom: parse_margin_side(sides[0]),
                            left: parse_margin_side(sides[1]),
                        }
                    }
                    1 => {
                        style.margin = Margin {
                            top: parse_margin_side(sides[0]),
                            right: parse_margin_side(sides[0]),
                            bottom: parse_margin_side(sides[0]),
                            left: parse_margin_side(sides[0]),
                        }
                    }
                    // 4 +
                    _ => {
                        style.margin = Margin {
                            top: parse_margin_side(sides[0]),
                            right: parse_margin_side(sides[1]),
                            bottom: parse_margin_side(sides[2]),
                            left: parse_margin_side(sides[3]),
                        }
                    }
                }
            }
            "margin-bottom" => {
                style.margin.bottom = parse_margin_side(&repr);
            }
            "margin-left" => {
                style.margin.left = parse_margin_side(&repr);
            }
            "margin-right" => {
                style.margin.right = parse_margin_side(&repr);
            }
            "margin-top" => {
                style.margin.top = parse_margin_side(&repr);
            }
            "max-height" => {
                style.max_height = match &repr[..] {
                    "none" => MaxHeight::None,

                    value => MaxHeight::Length(parse_css_length(value)),
                }
            }
            "max-width" => {
                style.max_width = match &repr[..] {
                    "none" => MaxWidth::None,

                    value => MaxWidth::Length(parse_css_length(value)),
                }
            }
            "min-height" => {
                style.min_height = match &repr[..] {
                    value => MinHeight::Length(parse_css_length(value)),
                }
            }
            "min-width" => {
                style.min_width = match &repr[..] {
                    value => MinWidth::Length(parse_css_length(value)),
                }
            }
            "opacity" => style.opacity = parse_css_number(&repr),
            "order" => style.order = parse_css_number(&repr),
            "overflow" => {
                style.overflow = match &repr[..] {
                    "visible" => Overflow::Visible,
                    "hidden" => Overflow::Hidden,
                    "scroll" => Overflow::Scroll,
                    "auto" => Overflow::Auto,

                    _ => style.overflow,
                }
            }
            "overflow-x" => {
                style.overflow_x = match &repr[..] {
                    "visible" => Overflow::Visible,
                    "hidden" => Overflow::Hidden,
                    "scroll" => Overflow::Scroll,
                    "auto" => Overflow::Auto,

                    _ => style.overflow_x,
                }
            }
            "overflow-y" => {
                style.overflow_y = match &repr[..] {
                    "visible" => Overflow::Visible,
                    "hidden" => Overflow::Hidden,
                    "scroll" => Overflow::Scroll,
                    "auto" => Overflow::Auto,

                    _ => style.overflow_y,
                }
            }
            "padding" => {
                let sides: Vec<&str> = repr.split_whitespace().collect();
                match sides.len() {
                    3 => {
                        style.padding = Padding {
                            top: parse_css_length(sides[0]),
                            right: parse_css_length(sides[1]),
                            bottom: parse_css_length(sides[2]),
                            left: parse_css_length(sides[1]),
                        }
                    }
                    2 => {
                        style.padding = Padding {
                            top: parse_css_length(sides[0]),
                            right: parse_css_length(sides[1]),
                            bottom: parse_css_length(sides[0]),
                            left: parse_css_length(sides[1]),
                        }
                    }
                    1 => {
                        style.padding = Padding {
                            top: parse_css_length(sides[0]),
                            right: parse_css_length(sides[0]),
                            bottom: parse_css_length(sides[0]),
                            left: parse_css_length(sides[0]),
                        }
                    }
                    // 4 +
                    _ => {
                        style.padding = Padding {
                            top: parse_css_length(sides[0]),
                            right: parse_css_length(sides[1]),
                            bottom: parse_css_length(sides[2]),
                            left: parse_css_length(sides[3]),
                        }
                    }
                }
            }
            "padding-bottom" => {
                style.padding.bottom = parse_css_length(&repr);
            }
            "padding-left" => {
                style.padding.left = parse_css_length(&repr);
            }
            "padding-right" => {
                style.padding.right = parse_css_length(&repr);
            }
            "padding-top" => {
                style.padding.top = parse_css_length(&repr);
            }
            "pointer-events" => {
                style.pointer_events = match &repr[..] {
                    "auto" => PointerEvents::Auto,
                    "none" => PointerEvents::None,
                    _ => style.pointer_events,
                }
            }
            "position" => {
                style.position = match &repr[..] {
                    "static" => Position::Static,
                    "absolute" => Position::Absolute,
                    "relative" => Position::Relative,
                    "fixed" => Position::Fixed,
                    "sticky" => Position::Sticky,

                    _ => style.position,
                }
            }
            "right" => {
                style.right = match &repr[..] {
                    "auto" => Right::Auto,

                    value => Right::Length(parse_css_length(value)),
                }
            }
            "top" => {
                style.top = match &repr[..] {
                    "auto" => Top::Auto,

                    value => Top::Length(parse_css_length(value)),
                }
            }
            "text-align" => {
                style.text.align = match &repr[..] {
                    "left" => TextAlign::Left,
                    "right" => TextAlign::Right,
                    "center" => TextAlign::Center,
                    "justify" => TextAlign::Justify,

                    _ => style.text.align,
                }
            }
            "text-align-last" => {
                style.text.align_last = match &repr[..] {
                    "auto" => TextAlignLast::Auto,
                    "left" => TextAlignLast::Left,
                    "right" => TextAlignLast::Right,
                    "center" => TextAlignLast::Center,
                    "justify" => TextAlignLast::Justify,
                    "start" => TextAlignLast::Start,
                    "end" => TextAlignLast::End,

                    _ => style.text.align_last,
                }
            }
            "text-decoration-color" => style.text.decoration.color = parse_css_color(&repr),
            "text-decoration-line" => {
                style.text.decoration.line = match &repr[..] {
                    "none" => TextDecorationLine::None,
                    "underline" => TextDecorationLine::Underline,
                    "overline" => TextDecorationLine::Overline,
                    "line-through" => TextDecorationLine::LineThrough,

                    _ => style.text.decoration.line,
                }
            }
            "text-decoration-style" => {
                style.text.decoration.style = match &repr[..] {
                    "solid" => TextDecorationStyle::Solid,
                    "double" => TextDecorationStyle::Double,
                    "dotted" => TextDecorationStyle::Dotted,
                    "dashed" => TextDecorationStyle::Dashed,
                    "wavy" => TextDecorationStyle::Wavy,

                    _ => style.text.decoration.style,
                }
            }
            "text-indent" => style.text.indent = parse_css_length(&repr),
            "text-overflow" => {
                style.text.overflow = match &repr[..] {
                    "clip" => TextOverflow::Clip,
                    "ellipsis" => TextOverflow::Ellipsis,

                    value => TextOverflow::Custom(value.to_string()),
                }
            }
            "text-justify" => {
                style.text.justify = match &repr[..] {
                    "auto" => TextJustify::Auto,
                    "inter-word" => TextJustify::InterWord,
                    "inter-character" => TextJustify::InterCharacter,
                    "none" => TextJustify::None,

                    _ => style.text.justify,
                }
            }
            "text-shadow" => {
                style.text.shadow = match &repr[..] {
                    "none" => TextShadow::None,

                    value => {
                        let (value, colors) = mask_colors(value);
                        TextShadow::Shadow(parse_css_shadow(&value, &colors))
                    }
                }
            }
            "text-transform" => {
                style.text.transform = match &repr[..] {
                    "none" => TextTransform::None,
                    "capitalize" => TextTransform::Capitalize,
                    "uppercase" => TextTransform::Uppercase,
                    "lowercase" => TextTransform::Lowercase,

                    _ => style.text.transform,
                }
            }
            "user-select" => {
                style.user_select = match &repr[..] {
                    "none" => UserSelect::None,
                    "auto" => UserSelect::Auto,
                    "text" => UserSelect::Text,
                    "all" => UserSelect::All,
                    _ => style.user_select,
                }
            }
            "vertical-align" => {
                style.vertical_align = match &repr[..] {
                    "baseline" => VerticalAlign::Baseline,
                    "sub" => VerticalAlign::Sub,
                    "super" => VerticalAlign::Super,
                    "top" => VerticalAlign::Top,
                    "text-top" => VerticalAlign::TextTop,
                    "middle" => VerticalAlign::Middle,
                    "bottom" => VerticalAlign::Bottom,
                    "text-bottom" => VerticalAlign::TextBottom,

                    value => VerticalAlign::Length(parse_css_length(value)),
                }
            }
            "visibility" => {
                style.visibility = match &repr[..] {
                    "visible" => Visibility::Visible,
                    "hidden" => Visibility::Hidden,
                    "collapse" => Visibility::Collapse,

                    _ => style.visibility,
                }
            }
            "white-space" => {
                style.white_space = match &repr[..] {
                    "normal" => WhiteSpace::Normal,
                    "nowrap" => WhiteSpace::Nowrap,
                    "pre" => WhiteSpace::Pre,
                    "pre-line" => WhiteSpace::PreLine,
                    "pre-wrap" => WhiteSpace::PreWrap,

                    _ => style.white_space,
                }
            }
            "width" => {
                style.width = match &repr[..] {
                    "auto" => Width::Auto,

                    value => Width::Length(parse_css_length(value)),
                }
            }
            "word-wrap" => {
                style.word_wrap = match &repr[..] {
                    "normal" => WordWrap::Normal,
                    "break-word" => WordWrap::BreakWord,

                    _ => style.word_wrap,
                }
            }
            "z-index" => {
                style.z_index = match &repr[..] {
                    "auto" => ZIndex::Auto,

                    value => ZIndex::Number(parse_css_number(value) as i32),
                }
            }
            _ => {
                error!("Unable to parse property {}, not implemented yet", name)
            }
        }
    }
    style
}
