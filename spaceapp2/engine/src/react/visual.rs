use std::fs::File;
use std::io;
use std::io::{Error, Read};
use std::time::Instant;

use fontdue::layout::{
    CoordinateSystem,
    GlyphPosition,
    HorizontalAlign,
    Layout,
    LayoutSettings,
    TextStyle,
    VerticalAlign,
    WrapStyle,
};
use fontdue::{Font, FontSettings};
use rusttype;
use tiny_skia::{FillRule, FilterQuality, Paint, PathBuilder, Pixmap, PixmapPaint, Transform};

use crate::react::Color::Rgba8;
use crate::react::Length::Px;
use crate::react::{
    BackgroundColor,
    BorderSideStyle,
    BoxSizing,
    Display,
    Height,
    Length,
    Style,
    Width,
};

#[derive(Debug)]
pub enum LoadFontError {
    Io(io::Error),
    Font(String),
}

impl From<io::Error> for LoadFontError {
    fn from(error: io::Error) -> Self {
        Self::Io(error)
    }
}

pub fn load_font(path: &str) -> Result<Vec<FontInstance>, LoadFontError> {
    let mut file = File::open(path)?;
    let mut buf = Vec::new();
    file.read_to_end(&mut buf)?;

    let mut instances = Vec::new();
    for scale in [72.0] {
        let settings = FontSettings { collection_index: 0, scale };
        let font = Font::from_bytes(&buf[..], settings)
            .map_err(|error| LoadFontError::Font(error.to_string()))?;
        instances.push(FontInstance { scale, path: path.to_string(), font })
    }

    Ok(instances)
}

pub struct FontInstance {
    pub scale: f32,
    pub path: String,
    pub font: Font,
}

pub struct FontLibrary {
    pub instances: Vec<FontInstance>,
    pub default_font: String,
}

impl FontLibrary {
    pub fn new(default_font: &str) -> Self {
        FontLibrary { instances: vec![], default_font: default_font.to_string() }
    }

    pub fn unwrap_font_family(&self, family: &Option<String>) -> String {
        match family {
            Some(family) => family.clone(),
            None => self.default_font.clone(),
        }
    }

    pub fn load_font(&mut self, path: &str) {
        match load_font(path) {
            Ok(instances) => self.instances.extend(instances),
            Err(error) => error!("Unable to load font {}, {:?}", path, error),
        }
    }

    pub fn index_best_match_font(&self, _font_family: &String, font_size: f32) -> usize {
        let mut best = 0;
        let mut best_scale = 0.0;
        for (index, instance) in self.instances.iter().enumerate() {
            // todo: family match
            let scale = instance.scale;
            if scale == font_size {
                best = index;
                break;
            }
            if scale > best_scale {
                best = index;
                best_scale = scale;
            }
            if best_scale > font_size {
                break;
            }
        }
        best
    }

    pub fn fonts(&self) -> Vec<&Font> {
        self.instances.iter().map(|instance| &instance.font).collect()
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Default)]
pub struct Parent {
    pub root_font_size: f32,
    pub rect: RenderRect,
    pub font_size: f32,
}

#[derive(Debug, Clone, Copy, PartialEq, Default)]
pub struct RenderRect {
    pub left: f32,
    pub top: f32,
    pub width: f32,
    pub height: f32,
    pub content_left: f32,
    pub content_top: f32,
    pub content_width: f32,
    pub content_height: f32,
}

/// Unlike HTML, for each node we support only one type of child:
/// list render objects (HTML Element Nodes) or text (HTML Text Nodes).
/// So, text need to be wrapped in "span" elements to achieve something like
/// this:
///
/// ```HTML
/// <div>
///     Hello <b>World!</b>
/// </div>
/// ```
///
/// # Example:
///
/// ```HTML
/// <div>
///     <span>Hello</span>
///     <b>World!</b>
/// </div>
/// ```
#[derive(Debug, PartialEq)]
pub enum RenderChild {
    Text(String),
    Objects(Vec<RenderObject>),
}

#[derive(Debug, PartialEq)]
pub struct RenderObject {
    pub style: Style,
    pub text: Option<String>,
    pub rect: RenderRect,
    pub font_size: f32,
    pub dirty: bool,
    pub children: Vec<RenderObject>,
}

impl RenderObject {
    pub fn viewport(width: u32, height: u32) -> Self {
        RenderObject {
            style: Style { display: Display::None, ..Style::default() },
            text: None,
            rect: RenderRect {
                left: 0.0,
                top: 0.0,
                width: width as f32,
                height: height as f32,
                content_left: 0.0,
                content_top: 0.0,
                content_width: width as f32,
                content_height: height as f32,
            },
            font_size: 16.0,
            dirty: false,
            children: vec![],
        }
    }

    pub fn new(style: Style) -> Self {
        RenderObject {
            style,
            text: None,
            rect: Default::default(),
            font_size: 0.0,
            dirty: true,
            children: vec![],
        }
    }

    pub fn with_text(text: &str, style: Style) -> Self {
        RenderObject {
            style,
            text: Some(text.to_string()),
            rect: Default::default(),
            font_size: 0.0,
            dirty: true,
            children: vec![],
        }
    }

    pub fn with_children(style: Style, children: Vec<RenderObject>) -> Self {
        RenderObject {
            style,
            text: None,
            rect: Default::default(),
            font_size: 0.0,
            dirty: true,
            children,
        }
    }

    pub fn as_parent(&self) -> Parent {
        Parent { root_font_size: 16.0, rect: self.rect, font_size: self.font_size }
    }

    pub fn draw(&self, parent: Parent, canvas: &mut Pixmap, library: &FontLibrary) {
        let time = Instant::now();

        let style = &self.style;
        let rect = &self.rect;

        let top = rect.top;
        let left = rect.left;
        let width = rect.width;
        let height = rect.height;

        let background = {
            let radius = &style.border.radius;
            let radius_top_left = RenderObject::calc_width(radius.top_left, parent);
            let radius_top_right = RenderObject::calc_width(radius.top_right, parent);
            let radius_bottom_left = RenderObject::calc_width(radius.bottom_left, parent);
            let radius_bottom_right = RenderObject::calc_width(radius.bottom_right, parent);
            let mut path = PathBuilder::new();
            path.move_to(left + radius_top_left, top);
            path.line_to(left + width - radius_top_right, top);
            path.quad_to(left + width, top, left + width, top + radius_top_right);
            path.line_to(left + width, top + height - radius_bottom_right);
            path.quad_to(
                left + width,
                top + height,
                left + width - radius_bottom_right,
                top + height,
            );
            path.line_to(left + radius_bottom_left, top + height);
            path.quad_to(left, top + height, left, top + height - radius_bottom_left);
            path.line_to(left, top + radius_top_left);
            path.quad_to(left, top, left + radius_top_left, top);
            path.close();
            path.finish().unwrap()
        };

        if let BackgroundColor::Color(Rgba8 { r, g, b, a }) = &style.background.color {
            let mut paint = Paint::default();
            paint.set_color_rgba8(*r, *g, *b, *a);
            paint.anti_alias = true;
            canvas.fill_path(&background, &paint, FillRule::Winding, Transform::identity(), None);
        }

        if let Some(text) = self.text.as_ref() {
            let font_family = library.unwrap_font_family(&self.style.font.family);
            let font_index = library.index_best_match_font(&font_family, self.font_size);

            let mut text_layout = Layout::new(CoordinateSystem::PositiveYDown);
            let setting = LayoutSettings {
                x: left,
                y: top,
                max_width: None,
                max_height: None,
                horizontal_align: HorizontalAlign::Left,
                vertical_align: VerticalAlign::Top,
                wrap_style: WrapStyle::Word,
                wrap_hard_breaks: false,
            };
            text_layout.reset(&setting);
            let style = TextStyle::new(text, self.font_size, font_index);
            let fonts = &library.fonts();
            text_layout.append(fonts, &style);

            // println!(
            //     "draw {} font_size: {} font_index: {}, scale_factor: {}, units_per_em:
            // {}",     text,
            //     self.font_size,
            //     font_index,
            //     fonts[font_index].scale_factor(16.0),
            //     fonts[font_index].units_per_em()
            // );

            for glyph in text_layout.glyphs() {
                if glyph.width == 0 || glyph.height == 0 {
                    continue;
                }
                let mut pixmap = Pixmap::new(glyph.width as u32, glyph.height as u32).unwrap();
                let data = pixmap.data_mut();

                /*let (metrics, bitmap) = fonts[glyph.key.font_index]
                    .rasterize_indexed_subpixel(glyph.key.glyph_index as usize, glyph.key.px);
                */
                let (metrics, bitmap) = fonts[glyph.key.font_index]
                    .rasterize_indexed(glyph.key.glyph_index as usize, glyph.key.px);

                for y in 0..metrics.height {
                    for x in 0..metrics.width {
                        // let r = bitmap[x * 3 + 0 + y * metrics.width * 3];
                        // let g = bitmap[x * 3 + 1 + y * metrics.width * 3];
                        // let b = bitmap[x * 3 + 2 + y * metrics.width * 3];
                        // let a = (r as f32 / 255.0 + g as f32 / 255.0 + b as f32 / 255.0) / 3.0;
                        // let a = (255.0 * a) as u8;
                        let s = bitmap[x + y * metrics.width];
                        let (r, g, b, a) = (s, s, s, s);
                        // print!("\x1B[48;2;{};{};{}m   ", r, g, b);

                        data[4 * y * metrics.width + x * 4 + 0] = r;
                        data[4 * y * metrics.width + x * 4 + 1] = g;
                        data[4 * y * metrics.width + x * 4 + 2] = b;
                        data[4 * y * metrics.width + x * 4 + 3] = a;
                    }
                    // println!("\x1B[0m");
                }

                let mut paint = PixmapPaint::default();
                paint.quality = FilterQuality::Nearest;
                // paint.blend_mode

                canvas.draw_pixmap(
                    glyph.x as i32,
                    glyph.y as i32,
                    pixmap.as_ref(),
                    &paint,
                    Transform::identity(),
                    None,
                );
            }
        }

        println!("time: {:?}", time.elapsed());
        for node in self.children.iter() {
            node.draw(self.as_parent(), canvas, library);
        }
    }

    pub fn breaks_line(&self) -> bool {
        match self.style.display {
            Display::Inline => false,
            Display::InlineBlock => false,
            Display::InlineFlex => false,
            Display::InlineGrid => false,
            Display::InlineTable => false,
            Display::None => false,
            _ => true,
        }
    }

    pub fn layout(&mut self, parent: Parent, library: &FontLibrary) {
        self.dirty = false;
        self.font_size = self.calc_font_size(parent);

        match self.style.display {
            Display::Block => {
                self.rect = self.calculate_block_width(parent);

                if let Some(text) = self.text.as_ref() {
                    let (_, text_height) = self.measure_text(text, library, Some(self.rect.width));
                    self.rect.content_height = text_height;
                    self.rect.height = text_height;
                } else {
                    let parent = self.as_parent();
                    let mut top = 0.0;
                    let mut left = 0.0;
                    let mut last_line_height: f32 = 0.0;
                    for child in self.children.iter_mut() {
                        if child.breaks_line() {
                            left = 0.0;
                            top += last_line_height;
                            last_line_height = 0.0;
                        }
                        if child.dirty {
                            child.layout(parent, library);
                        }
                        child.rect.left = left;
                        child.rect.top = top;
                        left += child.rect.width;
                        last_line_height = last_line_height.max(child.rect.height);

                        if left >= parent.rect.width || child.breaks_line() {
                            left = 0.0;
                            top += last_line_height;
                            last_line_height = 0.0;
                        }
                    }
                    self.rect.content_height = top + last_line_height;
                    self.rect.height = top + last_line_height;
                }
            }
            Display::Inline => {
                if let Some(text) = self.text.as_ref() {
                    let (width, height) = self.measure_text(text, library, None);
                    self.rect.width = width;
                    self.rect.content_width = width;
                    self.rect.content_height = height;
                    self.rect.height = height;
                    // x
                } else {
                    error!("Unable to display inline children");
                }
            }
            Display::InlineBlock => {
                if let Some(text) = self.text.as_ref() {
                    let (width, height) = self.measure_text(text, library, None);
                    self.rect.width = width;
                    self.rect.content_width = width;

                    match self.style.height {
                        Height::Auto => {
                            self.rect.content_height = height;
                            self.rect.height = height;
                        }
                        Height::Length(length) => {
                            let height = RenderObject::calc_height(length, parent);
                            self.rect.content_height = height;
                            self.rect.height = height;
                        }
                    }
                } else {
                    error!("Unable to display inline children");
                }
            }
            _ => {
                error!("Unable to layout display {:?}", self.style.display);
            }
        }
    }

    pub fn layout_old(&mut self, parent: Parent, library: &FontLibrary) -> f32 {
        self.rect = self.calculate_block_width(parent);
        self.dirty = false;
        self.font_size = self.calc_font_size(parent);

        if let Some(text) = self.text.as_ref() {
            let font_family = library.unwrap_font_family(&self.style.font.family);
            let font_index = library.index_best_match_font(&font_family, self.font_size);

            let mut text_layout = Layout::new(CoordinateSystem::PositiveYDown);
            let setting = LayoutSettings {
                x: 0.0,
                y: 0.0,
                max_width: Some(self.rect.width),
                max_height: None,
                horizontal_align: HorizontalAlign::Left,
                vertical_align: VerticalAlign::Top,
                wrap_style: WrapStyle::Word,
                wrap_hard_breaks: false,
            };
            text_layout.reset(&setting);
            let style = TextStyle::new(text, self.font_size, font_index);
            text_layout.append(&library.fonts(), &style);
            let a: &Vec<GlyphPosition<()>> = text_layout.glyphs();

            let mut right_x = 0.0;
            let mut right_width = 0;
            let mut bottom_y = 0.0;
            let mut bottom_height = 0;
            for glyph in a {
                if glyph.width == 0 || glyph.height == 0 {
                    continue;
                }

                if glyph.x > right_x {
                    right_x = glyph.x;
                    right_width = glyph.width;
                }
                if glyph.y > bottom_y {
                    bottom_y = glyph.y;
                    bottom_height = glyph.height;
                }
            }
            let text_width = right_x + right_width as f32;
            let text_height = bottom_y + bottom_height as f32;
            self.rect.height = text_height;
        }

        let mut children_height = 0.0;
        let parent = self.as_parent();
        for child in self.children.iter_mut() {
            if child.dirty {
                children_height += child.layout_old(parent, library);
            }
        }

        if self.style.height == Height::Auto && children_height != 0.0 {
            self.rect.height = children_height;
            self.rect.content_height = children_height;
        }

        self.rect.height
    }

    pub fn calc_font_size(&self, parent: Parent) -> f32 {
        match self.style.font.size {
            Px(pixels) => pixels as f32,
            Length::Em(em) => parent.font_size * em,
            Length::Rem(rem) => parent.root_font_size * rem,
            Length::Percent(percent) => parent.font_size * percent,
        }
    }

    pub fn measure_text(
        &self,
        text: &String,
        library: &FontLibrary,
        max_width: Option<f32>,
    ) -> (f32, f32) {
        let font_family = library.unwrap_font_family(&self.style.font.family);
        let font_index = library.index_best_match_font(&font_family, self.font_size);

        let mut text_layout = Layout::new(CoordinateSystem::PositiveYDown);
        let setting = LayoutSettings {
            x: 0.0,
            y: 0.0,
            max_width,
            max_height: None,
            horizontal_align: HorizontalAlign::Left,
            vertical_align: VerticalAlign::Bottom,
            wrap_style: WrapStyle::Word,
            wrap_hard_breaks: false,
        };
        text_layout.reset(&setting);
        let style = TextStyle::new(text, self.font_size, font_index);
        let fonts = &library.fonts();
        text_layout.append(fonts, &style);

        println!("text: {} height: {}, lines: {}", text, text_layout.height(), text_layout.lines());

        let mut width = 0.0;
        for glyph in text_layout.glyphs() {
            let font = fonts[glyph.key.font_index];

            let m = font.metrics_indexed(glyph.key.glyph_index as usize, glyph.key.px);

            if glyph.x > width {
                width = glyph.x + glyph.width as f32;
            }

            // let (metrics, offset_x, offset_y) = self.metrics_raw(scale, glyph, 0.0);
            // let mut canvas = Raster::new(metrics.width, metrics.height);
            // canvas.draw(&glyph, scale, scale, offset_x, offset_y);

            println!("aw: {}", m.advance_width);
            println!("x: {}, y: {}, w: {}, h: {}", glyph.x, glyph.y, glyph.width, glyph.height);
        }

        println!("width: {}", width);

        (width, text_layout.height())
    }

    pub fn calculate_block_width(&self, parent: Parent) -> RenderRect {
        let padding_left = RenderObject::calc_width(self.style.padding.left, parent);
        let padding_right = RenderObject::calc_width(self.style.padding.right, parent);
        let padding_top = RenderObject::calc_height(self.style.padding.top, parent);
        let padding_bottom = RenderObject::calc_height(self.style.padding.bottom, parent);

        let border = &self.style.border;
        let border_left = if border.style.left != BorderSideStyle::None {
            RenderObject::calc_width(border.width.left, parent)
        } else {
            0.0
        };
        let border_right = if border.style.right != BorderSideStyle::None {
            RenderObject::calc_width(border.width.right, parent)
        } else {
            0.0
        };
        let border_top = if border.style.top != BorderSideStyle::None {
            RenderObject::calc_height(border.width.top, parent)
        } else {
            0.0
        };
        let border_bottom = if border.style.bottom != BorderSideStyle::None {
            RenderObject::calc_height(border.width.bottom, parent)
        } else {
            0.0
        };

        let box_width = padding_left + padding_right + border_left + border_right;
        let box_height = padding_top + padding_bottom + border_top + border_bottom;

        let content_width = match self.style.width {
            Width::Auto => (parent.rect.content_width - box_width).max(0.0),
            Width::Length(length) => {
                let content_width = RenderObject::calc_width(length, parent);
                match self.style.box_sizing {
                    BoxSizing::ContentBox => content_width,
                    BoxSizing::BorderBox => (content_width - box_width).max(0.0),
                }
            }
        };
        let content_height = match self.style.height {
            Height::Auto => 0.0,
            Height::Length(length) => {
                let content_height = RenderObject::calc_width(length, parent);
                match self.style.box_sizing {
                    BoxSizing::ContentBox => content_height,
                    BoxSizing::BorderBox => (content_height - box_height).max(0.0),
                }
            }
        };

        let left = 0.0;
        let top = 0.0;
        let content_left = left + border_left + padding_left;
        let content_top = top + border_top + padding_top;
        let width = content_width + box_width;
        let height = content_height + box_height;

        RenderRect {
            left,
            top,
            width,
            height,
            content_left,
            content_top,
            content_width,
            content_height,
        }
    }

    #[inline]
    fn calc_width(length: Length, parent: Parent) -> f32 {
        match length {
            Length::Px(pixels) => pixels as f32,
            Length::Em(em) => parent.font_size * em,
            Length::Rem(rem) => parent.root_font_size * rem,
            Length::Percent(percent) => parent.rect.width * percent,
        }
    }

    #[inline]
    fn calc_height(length: Length, parent: Parent) -> f32 {
        match length {
            Length::Px(pixels) => pixels as f32,
            Length::Em(em) => parent.font_size * em,
            Length::Rem(rem) => parent.root_font_size * rem,
            Length::Percent(percent) => parent.rect.height * percent,
        }
    }
}
