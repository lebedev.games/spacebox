use crate::react::Style;

#[derive(Debug, PartialEq)]
pub enum Pseudo {
    Active,
    Checked,
    Default,
    Disabled,
    Empty,
    Enabled,
    FirstChild,
    FirstOfType,
    Focus,
    Fullscreen,
    Hover,
    InRange,
    Indeterminate,
    Invalid,
    LastChild,
    LastOfType,
    Link,
    OnlyOfType,
    OnlyChild,
    Optional,
    OutOfRange,
    ReadOnly,
    ReadWrite,
    Required,
    Root,
    Target,
    Valid,
    Visited,
}

#[derive(Debug, PartialEq)]
pub enum Condition {
    /// Selects all elements.
    ///
    /// # Example
    ///
    /// ```css
    /// * {
    ///   color: #43ea12
    /// }
    /// ```
    Universal,
    /// Selects all elements that have the given node name.
    ///
    /// # Example
    ///
    /// ```css
    /// div {
    ///   color: #43ea12
    /// }
    /// ```
    Type(String),
    /// Selects all elements that have the given class attribute.
    ///
    /// # Example
    ///
    /// ```css
    /// .button {
    ///   color: #43ea12
    /// }
    /// ```
    Class(String),
    /// Selects an element based on the value of its id attribute.
    ///
    /// # Example
    ///
    /// ```css
    /// #firstname {
    ///   color: #43ea12
    /// }
    /// ```
    Id(String),
    /// Selects all elements that have the given attribute.
    ///
    /// # Example
    ///
    /// ```css
    /// [target=_blank] {
    ///   color: #43ea12
    /// }
    /// ```
    Attribute {
        attribute: String,
        value: String,
    },
    /// Allow the selection of elements based on state information that is not
    /// contained in the document tree.
    ///
    /// # Example
    ///
    /// ```css
    /// :visited {
    ///   color: #43ea12
    /// }
    /// ```
    PseudoClass(Pseudo),
    /// Represent entities that are not included in HTML.
    ///
    /// # Example
    ///
    /// ```css
    /// ::first-line {
    ///   color: #43ea12
    /// }
    /// ```
    PseudoElement(String),
}

impl Default for Condition {
    fn default() -> Self {
        Condition::Universal
    }
}

pub type Compound = Vec<Condition>;

#[derive(Debug, PartialEq)]
pub enum Selector {
    Simple(Condition),
    /// Is a sequence of simple selectors that are not separated by a
    /// combinator.
    ///
    /// # Example
    ///
    /// ```css
    /// .link:visited {
    ///   color: #43ea12
    /// }
    /// ```
    Compound(Compound),
    /// Is a sequence of one or more compound selectors separated by
    /// combinators.
    Complex(Combinator),
    /// Grouping method that selects all the matching nodes.
    ///
    /// # Example
    ///
    /// ```css
    /// div, .button, a > #firstname {
    ///   color: #43ea12
    /// }
    /// ```
    List(Vec<Selector>),
}

impl Default for Selector {
    fn default() -> Self {
        Self::Simple(Condition::default())
    }
}

#[derive(Debug, PartialEq)]
pub enum Combinator {
    /// Selects nodes that are descendants of the first element.
    ///
    /// # Example
    ///
    /// ```css
    /// div .button {
    ///   color: #43ea12
    /// }
    /// ```
    Descendant {
        first: Compound,
        second: Compound,
    },
    /// Selects nodes that are direct children of the first element
    ///
    /// # Example
    ///
    /// ```css
    /// div > .button {
    ///   color: #43ea12
    /// }
    /// ```
    Child {
        first: Compound,
        second: Compound,
    },
    /// Selects siblings. This means that the second element follows the first
    /// (though not necessarily immediately), and both share the same parent.
    Sibling {
        first: Compound,
        second: Compound,
    },
    /// Matches the second element only if it immediately follows the first
    /// element.
    ///
    /// # Example
    ///
    /// ```css
    /// div + .button {
    ///   color: #43ea12
    /// }
    /// ```
    Adjacent {
        first: Compound,
        second: Compound,
    },
}

pub struct Rule {
    pub selector: Selector,
    pub style: Style,
}

pub struct Stylesheet {
    pub rules: Vec<Rule>,
}
