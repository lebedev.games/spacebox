#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Length {
    Px(u32),
    Em(f32),
    Rem(f32),
    Percent(f32),
}

#[derive(Debug, PartialEq)]
pub enum Url {
    Absolute(String),
    Relative(String),
    Data {
        content_type: String,
        content: String,
    },
}

#[derive(Debug, PartialEq)]
pub struct LinearGradient {}

#[derive(Debug, PartialEq)]
pub struct RadialGradient {}

#[derive(Debug, PartialEq)]
pub struct RepeatingLinearGradient {}

#[derive(Debug, PartialEq)]
pub struct RepeatingRadialGradient {}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Color {
    Rgba8 {
        r: u8,
        g: u8,
        b: u8,
        a: u8,
    },
    Current,
}

impl Default for Color {
    fn default() -> Self {
        Self::Rgba8 { r: 0, g: 0, b: 0, a: 255 }
    }
}

#[derive(Debug, PartialEq)]
pub enum AlignContent {
    Stretch,
    Center,
    FlexStart,
    FlexEnd,
    SpaceBetween,
    SpaceAround,
}

#[derive(Debug, PartialEq)]
pub enum AlignItems {
    Stretch,
    Center,
    FlexStart,
    FlexEnd,
    Baseline,
}

#[derive(Debug, PartialEq)]
pub enum AlignSelf {
    Auto,
    Stretch,
    Center,
    FlexStart,
    FlexEnd,
    Baseline,
}

#[derive(Debug, PartialEq)]
pub struct Background {
    pub color: BackgroundColor,
    pub image: BackgroundImage,
    pub position: BackgroundPosition,
    pub repeat: BackgroundRepeat,
    pub origin: BackgroundOrigin,
    pub clip: BackgroundClip,
    pub attachment: BackgroundAttachment,
    pub blend_mode: BackgroundBlendMode,
    pub size: BackgroundSize,
}

#[derive(Debug, PartialEq)]
pub enum BackgroundImage {
    Url(Url),
    None,
    LinearGradient(LinearGradient),
    RadialGradient(RadialGradient),
    RepeatingLinearGradient(RepeatingLinearGradient),
    RepeatingRadialGradient(RepeatingRadialGradient),
}

#[derive(Debug, PartialEq)]
pub enum BackgroundBlendMode {
    Normal,
    Multiply,
    Screen,
    Overlay,
    Darken,
    Lighten,
    ColorDodge,
    Saturation,
    Color,
    Luminosity,
}

#[derive(Debug, PartialEq)]
pub enum BackgroundAttachment {
    Scroll,
    Fixed,
    Local,
}

#[derive(Debug, PartialEq)]
pub enum BackgroundClip {
    BorderBox,
    PaddingBox,
    ContentBox,
}

#[derive(Debug, PartialEq)]
pub enum BackgroundOrigin {
    PaddingBox,
    BorderBox,
    ContentBox,
}

#[derive(Debug, PartialEq)]
pub enum HorizontalKeyword {
    Left,
    Center,
    Right,
}

#[derive(Debug, PartialEq)]
pub enum VerticalKeyword {
    Top,
    Center,
    Bottom,
}

#[derive(Debug, PartialEq)]
pub enum BackgroundPosition {
    Keyword {
        horizontal: HorizontalKeyword,
        vertical: VerticalKeyword,
    },
    Length {
        horizontal: Length,
        vertical: Length,
    },
}

#[derive(Debug, PartialEq)]
pub enum BackgroundColor {
    Color(Color),
    Transparent,
}

#[derive(Debug, PartialEq)]
pub enum BackgroundRepeat {
    Repeat,
    RepeatX,
    RepeatY,
    NoRepeat,
    Space,
    Round,
}

#[derive(Debug, PartialEq)]
pub enum BackgroundSize {
    Auto,
    Length {
        width: Length,
        height: Length,
    },
    Cover,
    Contain,
}

#[derive(Debug, PartialEq)]
pub struct BorderWidth {
    pub top: Length,
    pub right: Length,
    pub bottom: Length,
    pub left: Length,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum BorderSideStyle {
    None,
    Hidden,
    Dotted,
    Dashed,
    Solid,
    Double,
    Groove,
    Ridge,
    Inset,
    Outset,
}

#[derive(Debug, PartialEq)]
pub struct BorderStyle {
    pub top: BorderSideStyle,
    pub right: BorderSideStyle,
    pub bottom: BorderSideStyle,
    pub left: BorderSideStyle,
}

#[derive(Debug, Clone, PartialEq)]
pub enum BorderSideColor {
    Color(Color),
    Transparent,
}

#[derive(Debug, PartialEq)]
pub struct BorderColor {
    pub top: BorderSideColor,
    pub right: BorderSideColor,
    pub bottom: BorderSideColor,
    pub left: BorderSideColor,
}

#[derive(Debug, PartialEq)]
pub struct BorderRadius {
    pub top_left: Length,
    pub top_right: Length,
    pub bottom_right: Length,
    pub bottom_left: Length,
}

#[derive(Debug, PartialEq)]
pub enum BorderSpacing {
    Length(Length),
}

#[derive(Debug, PartialEq)]
pub enum BorderCollapse {
    Separate,
    Collapse,
}

#[derive(Debug, PartialEq)]
pub enum BorderImageRepeat {
    Stretch,
    Repeat,
    Round,
}

#[derive(Debug, PartialEq)]
pub enum BorderImageSource {
    None,
    Image(Url),
}

#[derive(Debug, PartialEq)]
pub enum BorderImageSideOutset {
    Length(Length),
}

#[derive(Debug, PartialEq)]
pub struct BorderImageOutset {
    pub top: BorderImageSideOutset,
    pub right: BorderImageSideOutset,
    pub bottom: BorderImageSideOutset,
    pub left: BorderImageSideOutset,
}

#[derive(Debug, PartialEq)]
pub enum BorderImageSideSlice {
    Length(Length),
    Fill,
}

#[derive(Debug, PartialEq)]
pub struct BorderImageSlice {
    pub top: BorderImageSideSlice,
    pub right: BorderImageSideSlice,
    pub bottom: BorderImageSideSlice,
    pub left: BorderImageSideSlice,
}

#[derive(Debug, PartialEq)]
pub enum BorderImageSideWidth {
    Auto,
    Length(Length),
}

#[derive(Debug, PartialEq)]
pub struct BorderImageWidth {
    pub top: BorderImageSideWidth,
    pub right: BorderImageSideWidth,
    pub bottom: BorderImageSideWidth,
    pub left: BorderImageSideWidth,
}

#[derive(Debug, PartialEq)]
pub struct BorderImage {
    pub source: BorderImageSource,
    pub slice: BorderImageSlice,
    pub width: BorderImageWidth,
    pub outset: BorderImageOutset,
    pub repeat: BorderImageRepeat,
}

#[derive(Debug, PartialEq)]
pub struct Border {
    pub width: BorderWidth,
    pub style: BorderStyle,
    pub color: BorderColor,
    pub image: BorderImage,
    pub radius: BorderRadius,
    pub spacing: BorderSpacing,
    pub collapse: BorderCollapse,
}

#[derive(Debug, PartialEq)]
pub enum Bottom {
    Auto,
    Length(Length),
}

#[derive(Debug, PartialEq)]
pub enum BoxDecorationBreak {
    Slice,
    Clone,
}

#[derive(Debug, PartialEq)]
pub struct Shadow {
    pub horizontal_offset: Length,
    pub vertical_offset: Length,
    pub blur: Length,
    pub spread: Length,
    pub color: Color,
    pub outset: bool,
}

#[derive(Debug, PartialEq)]
pub enum BoxShadow {
    None,
    Shadows(Vec<Shadow>),
}

#[derive(Debug, PartialEq)]
pub enum BoxSizing {
    ContentBox,
    BorderBox,
}

#[derive(Debug, PartialEq)]
pub enum CaptionSide {
    Top,
    Bottom,
}

#[derive(Debug, PartialEq)]
pub enum Display {
    Inline,
    Block,
    Contents,
    Flex,
    Grid,
    InlineBlock,
    InlineFlex,
    InlineGrid,
    InlineTable,
    ListItem,
    RunIn,
    Table,
    TableCaption,
    TableColumnGroup,
    TableHeaderGroup,
    TableFooterGroup,
    TableRowGroup,
    TableCell,
    TableColumn,
    TableRow,
    None,
}

#[derive(Debug, PartialEq)]
pub enum Filter {
    None,
    Blur(u32),
    Brightness(u32),
    Contrast(u32),
    DropShadow(Shadow),
    Grayscale(u32),
    HueRotate(u32),
    Invert(u32),
    Opacity(u32),
    Saturate(u32),
    Sepia(u32),
    Url(Url),
}

#[derive(Debug, PartialEq)]
pub enum FlexBasis {
    Auto,
    Length(Length),
}

#[derive(Debug, PartialEq)]
pub enum FlexDirection {
    Row,
    RowReverse,
    Column,
    ColumnReverse,
}

#[derive(Debug, PartialEq)]
pub enum FlexWrap {
    Nowrap,
    Wrap,
    WrapReverse,
}

#[derive(Debug, PartialEq)]
pub struct Flex {
    pub shrink: f32,
    pub grow: f32,
    pub basis: FlexBasis,
    pub wrap: FlexWrap,
    pub direction: FlexDirection,
}

#[derive(Debug, PartialEq)]
pub enum Float {
    None,
    Left,
    Right,
}

#[derive(Debug, PartialEq)]
pub enum FontKerning {
    Auto,
    Normal,
    None,
}

#[derive(Debug, PartialEq)]
pub enum FontSizeAdjust {
    None,
    Number(f32),
}

#[derive(Debug, PartialEq)]
pub enum FontStretch {
    Normal,
    UltraCondensed,
    ExtraCondensed,
    Condensed,
    SemiCondensed,
    SemiExpanded,
    Expanded,
    ExtraExpanded,
    UltraExpanded,
}

#[derive(Debug, PartialEq)]
pub enum FontStyle {
    Normal,
    Italic,
    Oblique,
}

#[derive(Debug, PartialEq)]
pub enum FontVariant {
    Normal,
    SmallCaps,
}

#[derive(Debug, PartialEq)]
pub enum FontVariantCaps {
    Normal,
    SmallCaps,
    AllSmallCaps,
    PetiteCaps,
    AllPetiteCaps,
    Unicase,
    TitlingCaps,
    Unset,
}

#[derive(Debug, PartialEq)]
pub enum FontWeight {
    Normal,
    Bold,
    Bolder,
    Lighter,
    Number(u32),
}

#[derive(Debug, PartialEq)]
pub struct Font {
    pub family: Option<String>,
    pub kerning: FontKerning,
    pub size: Length,
    pub size_adjust: FontSizeAdjust,
    pub stretch: FontStretch,
    pub style: FontStyle,
    pub variant: FontVariant,
    pub variant_caps: FontVariantCaps,
    pub weight: FontWeight,
}

#[derive(Debug, PartialEq)]
pub enum Height {
    Auto,
    Length(Length),
}

#[derive(Debug, PartialEq)]
pub enum JustifyContent {
    FlexStart,
    FlexEnd,
    Center,
    SpaceBetween,
    SpaceAround,
    SpaceEvenly,
}

#[derive(Debug, PartialEq)]
pub enum Left {
    Auto,
    Length(Length),
}

#[derive(Debug, PartialEq)]
pub enum LineHeight {
    Normal,
    Length(Length),
}

#[derive(Debug, PartialEq)]
pub enum MarginSide {
    Length(Length),
    Auto,
}

#[derive(Debug, PartialEq)]
pub struct Margin {
    pub top: MarginSide,
    pub right: MarginSide,
    pub bottom: MarginSide,
    pub left: MarginSide,
}

#[derive(Debug, PartialEq)]
pub enum MaxHeight {
    None,
    Length(Length),
}

#[derive(Debug, PartialEq)]
pub enum MaxWidth {
    None,
    Length(Length),
}

#[derive(Debug, PartialEq)]
pub enum MinHeight {
    Length(Length),
}

#[derive(Debug, PartialEq)]
pub enum MinWidth {
    Length(Length),
}

#[derive(Debug, PartialEq)]
pub enum Overflow {
    Visible,
    Hidden,
    Scroll,
    Auto,
}

#[derive(Debug, PartialEq)]
pub struct Padding {
    pub top: Length,
    pub right: Length,
    pub bottom: Length,
    pub left: Length,
}

#[derive(Debug, PartialEq)]
pub enum PointerEvents {
    Auto,
    None,
}

#[derive(Debug, PartialEq)]
pub enum Position {
    Static,
    Absolute,
    Fixed,
    Relative,
    Sticky,
}

#[derive(Debug, PartialEq)]
pub enum Right {
    Auto,
    Length(Length),
}

#[derive(Debug, PartialEq)]
pub enum TextAlign {
    Left,
    Right,
    Center,
    Justify,
}

#[derive(Debug, PartialEq)]
pub enum TextAlignLast {
    Auto,
    Left,
    Right,
    Center,
    Justify,
    Start,
    End,
}

#[derive(Debug, PartialEq)]
pub enum TextDecorationLine {
    None,
    Underline,
    Overline,
    LineThrough,
}

#[derive(Debug, PartialEq)]
pub enum TextDecorationStyle {
    Solid,
    Double,
    Dotted,
    Dashed,
    Wavy,
}

#[derive(Debug, PartialEq)]
pub enum TextJustify {
    Auto,
    InterWord,
    InterCharacter,
    None,
}

#[derive(Debug, PartialEq)]
pub enum TextOverflow {
    Clip,
    Ellipsis,
    Custom(String),
}

#[derive(Debug, PartialEq)]
pub enum TextShadow {
    None,
    Shadow(Shadow),
}

#[derive(Debug, PartialEq)]
pub enum TextTransform {
    None,
    Capitalize,
    Uppercase,
    Lowercase,
}

#[derive(Debug, PartialEq)]
pub struct TextDecoration {
    pub color: Color,
    pub line: TextDecorationLine,
    pub style: TextDecorationStyle,
}

#[derive(Debug, PartialEq)]
pub struct Text {
    pub align: TextAlign,
    pub align_last: TextAlignLast,
    pub decoration: TextDecoration,
    pub indent: Length,
    pub justify: TextJustify,
    pub overflow: TextOverflow,
    pub shadow: TextShadow,
    pub transform: TextTransform,
}

#[derive(Debug, PartialEq)]
pub enum Top {
    Auto,
    Length(Length),
}

#[derive(Debug, PartialEq)]
pub enum UserSelect {
    Auto,
    None,
    Text,
    All,
}

#[derive(Debug, PartialEq)]
pub enum VerticalAlign {
    Baseline,
    Length(Length),
    Sub,
    Super,
    Top,
    TextTop,
    Middle,
    Bottom,
    Text,
    TextBottom,
}

#[derive(Debug, PartialEq)]
pub enum Visibility {
    Visible,
    Hidden,
    Collapse,
}

#[derive(Debug, PartialEq)]
pub enum WhiteSpace {
    Normal,
    Nowrap,
    Pre,
    PreLine,
    PreWrap,
}

#[derive(Debug, PartialEq)]
pub enum Width {
    Auto,
    Length(Length),
}

#[derive(Debug, PartialEq)]
pub enum WordWrap {
    Normal,
    BreakWord,
}

#[derive(Debug, PartialEq)]
pub enum ZIndex {
    Auto,
    Number(i32),
}

#[derive(Debug, PartialEq)]
pub struct Style {
    pub align_content: AlignContent,
    pub align_items: AlignItems,
    pub align_self: AlignSelf,
    pub background: Background,
    pub border: Border,
    pub bottom: Bottom,
    pub box_decoration_break: BoxDecorationBreak,
    pub box_shadow: BoxShadow,
    pub box_sizing: BoxSizing,
    pub caption_side: CaptionSide,
    pub color: Color,
    pub display: Display,
    pub filter: Filter,
    pub flex: Flex,
    pub float: Float,
    pub font: Font,
    pub height: Height,
    pub justify_content: JustifyContent,
    pub left: Left,
    pub line_height: LineHeight,
    pub margin: Margin,
    pub max_height: MaxHeight,
    pub max_width: MaxWidth,
    pub min_height: MinHeight,
    pub min_width: MinWidth,
    pub opacity: f32,
    pub order: f32,
    pub overflow: Overflow,
    pub overflow_x: Overflow,
    pub overflow_y: Overflow,
    pub padding: Padding,
    pub pointer_events: PointerEvents,
    pub position: Position,
    pub right: Right,
    pub text: Text,
    pub top: Top,
    pub user_select: UserSelect,
    pub vertical_align: VerticalAlign,
    pub visibility: Visibility,
    pub white_space: WhiteSpace,
    pub width: Width,
    pub word_wrap: WordWrap,
    pub z_index: ZIndex,
}

impl Default for Background {
    fn default() -> Self {
        Background {
            attachment: BackgroundAttachment::Scroll,
            blend_mode: BackgroundBlendMode::Normal,
            clip: BackgroundClip::BorderBox,
            color: BackgroundColor::Transparent,
            image: BackgroundImage::None,
            origin: BackgroundOrigin::PaddingBox,
            position: BackgroundPosition::Length {
                horizontal: Length::Percent(0.0),
                vertical: Length::Percent(0.0),
            },
            repeat: BackgroundRepeat::Repeat,
            size: BackgroundSize::Auto,
        }
    }
}

impl Default for Font {
    fn default() -> Self {
        Font {
            family: None,
            kerning: FontKerning::Auto,
            size: Length::Px(16),
            size_adjust: FontSizeAdjust::None,
            stretch: FontStretch::Normal,
            style: FontStyle::Normal,
            variant: FontVariant::Normal,
            variant_caps: FontVariantCaps::Normal,
            weight: FontWeight::Normal,
        }
    }
}

impl Default for Style {
    fn default() -> Self {
        Style {
            align_content: AlignContent::Stretch,
            align_items: AlignItems::Stretch,
            align_self: AlignSelf::Auto,
            background: Background::default(),
            border: Border {
                width: BorderWidth {
                    top: Length::Px(2),
                    right: Length::Px(2),
                    bottom: Length::Px(2),
                    left: Length::Px(2),
                },
                style: BorderStyle {
                    top: BorderSideStyle::None,
                    right: BorderSideStyle::None,
                    bottom: BorderSideStyle::None,
                    left: BorderSideStyle::None,
                },
                color: BorderColor {
                    top: BorderSideColor::Transparent,
                    right: BorderSideColor::Transparent,
                    bottom: BorderSideColor::Transparent,
                    left: BorderSideColor::Transparent,
                },
                image: BorderImage {
                    source: BorderImageSource::None,
                    slice: BorderImageSlice {
                        top: BorderImageSideSlice::Length(Length::Percent(100.0)),
                        right: BorderImageSideSlice::Length(Length::Percent(100.0)),
                        bottom: BorderImageSideSlice::Length(Length::Percent(100.0)),
                        left: BorderImageSideSlice::Length(Length::Percent(100.0)),
                    },
                    width: BorderImageWidth {
                        top: BorderImageSideWidth::Length(Length::Px(1)),
                        right: BorderImageSideWidth::Length(Length::Px(1)),
                        bottom: BorderImageSideWidth::Length(Length::Px(1)),
                        left: BorderImageSideWidth::Length(Length::Px(1)),
                    },
                    outset: BorderImageOutset {
                        top: BorderImageSideOutset::Length(Length::Px(0)),
                        right: BorderImageSideOutset::Length(Length::Px(0)),
                        bottom: BorderImageSideOutset::Length(Length::Px(0)),
                        left: BorderImageSideOutset::Length(Length::Px(0)),
                    },
                    repeat: BorderImageRepeat::Stretch,
                },
                radius: BorderRadius {
                    top_left: Length::Px(0),
                    top_right: Length::Px(0),
                    bottom_right: Length::Px(0),
                    bottom_left: Length::Px(0),
                },
                spacing: BorderSpacing::Length(Length::Px(2)),
                collapse: BorderCollapse::Separate,
            },
            bottom: Bottom::Auto,
            box_decoration_break: BoxDecorationBreak::Slice,
            box_shadow: BoxShadow::None,
            box_sizing: BoxSizing::ContentBox,
            caption_side: CaptionSide::Top,
            color: Color::Rgba8 { r: 0, g: 0, b: 0, a: 255 },
            display: Display::Block,
            filter: Filter::None,
            flex: Flex {
                shrink: 1.0,
                grow: 0.0,
                basis: FlexBasis::Auto,
                wrap: FlexWrap::Nowrap,
                direction: FlexDirection::Row,
            },
            float: Float::None,
            font: Font::default(),
            height: Height::Auto,
            justify_content: JustifyContent::FlexStart,
            left: Left::Auto,
            line_height: LineHeight::Normal,
            margin: Margin {
                top: MarginSide::Length(Length::Px(0)),
                right: MarginSide::Length(Length::Px(0)),
                bottom: MarginSide::Length(Length::Px(0)),
                left: MarginSide::Length(Length::Px(0)),
            },
            max_height: MaxHeight::None,
            max_width: MaxWidth::None,
            min_height: MinHeight::Length(Length::Px(0)),
            min_width: MinWidth::Length(Length::Px(0)),
            opacity: 1.0,
            order: 0.0,
            overflow: Overflow::Visible,
            overflow_x: Overflow::Visible,
            overflow_y: Overflow::Visible,
            padding: Padding {
                top: Length::Px(0),
                right: Length::Px(0),
                bottom: Length::Px(0),
                left: Length::Px(0),
            },
            pointer_events: PointerEvents::Auto,
            position: Position::Static,
            right: Right::Auto,
            text: Text {
                align: TextAlign::Left,
                align_last: TextAlignLast::Auto,
                decoration: TextDecoration {
                    color: Color::Current,
                    line: TextDecorationLine::None,
                    style: TextDecorationStyle::Solid,
                },
                indent: Length::Px(0),
                justify: TextJustify::Auto,
                overflow: TextOverflow::Clip,
                shadow: TextShadow::None,
                transform: TextTransform::None,
            },
            top: Top::Auto,
            user_select: UserSelect::Auto,
            vertical_align: VerticalAlign::Baseline,
            visibility: Visibility::Visible,
            white_space: WhiteSpace::Normal,
            width: Width::Auto,
            word_wrap: WordWrap::Normal,
            z_index: ZIndex::Auto,
        }
    }
}
