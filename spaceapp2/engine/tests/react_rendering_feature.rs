#[cfg(test)]
mod tests {
    use engine::react::{parse_style, FontLibrary, RenderObject};
    use tiny_skia::Pixmap;

    #[test]
    fn test_two_spans() {
        let mut library = FontLibrary::new("Roboto");
        library.load_font("./tests/data/fonts/Roboto/Roboto-Medium.ttf");

        let mut canvas = Pixmap::new(256, 256).unwrap();

        let mut viewport = RenderObject::viewport(512, 256);
        let mut body = RenderObject::with_children(
            parse_style(
                r#"
                background-color: red;
                display: block;
                "#,
            ),
            vec![
                RenderObject::with_text(
                    "Item t",
                    parse_style(
                        r#"
                        display: inline;
                        background-color: green;
                        "#,
                    ),
                ),
                RenderObject::with_text(
                    "Item B",
                    parse_style(
                        r#"
                        display: inline;
                        background-color: green;
                        "#,
                    ),
                ),
            ],
        );

        body.layout(viewport.as_parent(), &library);
        body.draw(viewport.as_parent(), &mut canvas, &library);

        canvas.save_png("./tests/output/output.png").unwrap();
    }

    #[test]
    fn test_inline_block_and_line_breaking() {
        let mut library = FontLibrary::new("Roboto");
        library.load_font("./tests/data/fonts/Roboto/Roboto-Medium.ttf");

        let mut canvas = Pixmap::new(512, 512).unwrap();

        let span = |text| {
            RenderObject::with_text(
                text,
                parse_style(
                    r#"
                    background-color: green;
                    display: inline;
                    "#,
                ),
            )
        };
        let block = |text| {
            RenderObject::with_text(
                text,
                parse_style(
                    r#"
                    background-color: blue;
                    display: block;
                    "#,
                ),
            )
        };
        let inline = |text| {
            RenderObject::with_text(
                text,
                parse_style(
                    r#"
                    background-color: cyan;
                    display: inline-block;
                    height: 40px;
                    "#,
                ),
            )
        };

        let mut viewport = RenderObject::viewport(512, 512);
        let mut body = RenderObject::with_children(
            parse_style(
                r#"
                background-color: red;
                width: 128px;
                display: block;
                "#,
            ),
            vec![
                span("Item A"),
                inline("Item B"),
                block("Item C"),
                span("Item D"),
                span("Lorem ipsum dolor sit amet"),
                span("Lorem+ipsum+dolor+sit+amet"),
                block("Lorem ipsum dolor sit amet"),
                block("Lorem+ipsum+dolor+sit+amet"),
                inline("Lorem ipsum dolor sit amet"),
                inline("Lorem+ipsum+dolor+sit+amet"),
            ],
        );

        body.layout(viewport.as_parent(), &library);
        body.draw(viewport.as_parent(), &mut canvas, &library);

        canvas.save_png("./tests/output/output.png").unwrap();
    }

    #[test]
    fn test_text_child_with_fixed_width() {
        let mut library = FontLibrary::new("Roboto");
        library.load_font("./tests/data/fonts/Roboto/Roboto-Medium.ttf");

        let mut canvas = Pixmap::new(128, 128).unwrap();

        let mut viewport = RenderObject::viewport(128, 128);
        let mut body = RenderObject::with_children(
            parse_style(
                r#"
                background-color: red;
                "#,
            ),
            vec![RenderObject::with_text(
                "    Lorem ipsum dolor sit amet",
                parse_style(
                    r#"
                    background-color: green;
                    width: 100px;
                    "#,
                ),
            )],
        );

        body.layout(viewport.as_parent(), &library);
        body.draw(viewport.as_parent(), &mut canvas, &library);

        canvas.save_png("./tests/output/output.png").unwrap();

        assert_eq!(body.rect.width, 128.0, "body width");
        assert_eq!(body.rect.height, 57.0, "body height");
        let child = &body.children[0];
        assert_eq!(child.rect.width, 100.0, "child width");
        assert_eq!(child.rect.height, 57.0, "child height");
    }

    #[test]
    fn test_empty_child_with_fixed_width_and_padding_by_border_box_sizing() {
        let mut library = FontLibrary::new("Roboto");
        library.load_font("./tests/data/fonts/Roboto/Roboto-Medium.ttf");

        let mut viewport = RenderObject::viewport(256, 256);
        let mut body = RenderObject::with_children(
            parse_style(
                r#"
                background-color: red;
                "#,
            ),
            vec![RenderObject::new(parse_style(
                r#"
                background-color: green;
                width: 100px;
                padding: 10px;
                box-sizing: border-box;
                "#,
            ))],
        );

        body.layout_old(viewport.as_parent(), &library);

        assert_eq!(body.rect.width, 256.0, "body width");
        assert_eq!(body.rect.height, 20.0, "body height");
        let child = &body.children[0];
        assert_eq!(child.rect.width, 100.0, "child width");
        assert_eq!(child.rect.height, 20.0, "child height");
    }

    #[test]
    fn test_something() {
        let mut library = FontLibrary::new("Roboto");
        library.load_font("./tests/data/fonts/Roboto/Roboto-Medium.ttf");

        let mut viewport = RenderObject::viewport(256, 256);
        let mut body = RenderObject::with_children(
            parse_style(
                r#"
                width: 200px;
                height: 150px;
                background-color: #04AA6D;
                border-radius: 15px 50px;
                "#,
            ),
            vec![RenderObject::new(parse_style(
                r#"
                width: 50px;
                height: 50px;
                background-color: red;
                "#,
            ))],
        );
        // body.layout(&viewport);

        let mut canvas = Pixmap::new(256, 256).unwrap();
        body.layout_old(viewport.as_parent(), &library);
        body.draw(viewport.as_parent(), &mut canvas, &library);
        canvas.save_png("./tests/output/output.png").unwrap();
    }
}
