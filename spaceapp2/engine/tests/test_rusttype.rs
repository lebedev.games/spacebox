#[cfg(test)]
mod tests {
    use std::fs::File;
    use std::io::Read;

    use image::{DynamicImage, Rgba};
    use rusttype::{point, Font, Scale};
    use tiny_skia::Pixmap;

    #[test]
    fn test_something() {
        // Load the font

        let mut file = File::open("./tests/data/fonts/Roboto/Roboto-Medium.ttf").unwrap();
        let mut buf = Vec::new();
        file.read_to_end(&mut buf).unwrap();

        // This only succeeds if collection consists of one font
        let font = Font::try_from_bytes(&buf[..]).expect("Error constructing Font");

        // The font size to use
        let scale = Scale::uniform(32.0);

        // The text to render
        let text = "This is RustType rendered into a png!";

        // Use a dark red colour
        let colour = (150, 0, 0);

        let v_metrics = font.v_metrics(scale);

        // layout the glyphs in a line with 20 pixels padding
        let glyphs: Vec<_> =
            font.layout(text, scale, point(20.0, 20.0 + v_metrics.ascent)).collect();

        // work out the layout size
        let glyphs_height = (v_metrics.ascent - v_metrics.descent).ceil() as u32;
        let glyphs_width = {
            let min_x = glyphs.first().map(|g| g.pixel_bounding_box().unwrap().min.x).unwrap();
            let max_x = glyphs.last().map(|g| g.pixel_bounding_box().unwrap().max.x).unwrap();
            (max_x - min_x) as u32
        };

        println!("width: {}, height: {}", glyphs_width, glyphs_height);

        // Create a new rgba image with some padding
        let mut image = DynamicImage::new_rgba8(glyphs_width + 40, glyphs_height + 40).to_rgba();

        let height = glyphs_height + 40;
        let width = glyphs_width + 40;
        let mut canvas = Pixmap::new(width, height).unwrap();
        let data = canvas.data_mut();

        // Loop through the glyphs in the text, positing each one on a line
        for glyph in glyphs {
            if let Some(bounding_box) = glyph.pixel_bounding_box() {
                // Draw the glyph into the image per-pixel by using the draw closure

                glyph.draw(|x, y, v| {
                    image.put_pixel(
                        // Offset the position by the glyph bounding box
                        x + bounding_box.min.x as u32,
                        y + bounding_box.min.y as u32,
                        // Turn the coverage into an alpha value
                        Rgba([colour.0, colour.1, colour.2, (v * 255.0) as u8]),
                    );

                    data[(4 * y * width + x * 4 + 0) as usize] = colour.0;
                    data[(4 * y * width + x * 4 + 1) as usize] = colour.1;
                    data[(4 * y * width + x * 4 + 2) as usize] = colour.2;
                    data[(4 * y * width + x * 4 + 3) as usize] = (v * 255.0) as u8;
                });
            }
        }

        // Save the image to a png file
        image.save("./tests/output/image_example.png").unwrap();
        canvas.save_png("./tests/output/image2.png").unwrap();
        println!("Generated: image_example.png");
    }
}
