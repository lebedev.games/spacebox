#[cfg(test)]
mod tests {
    use engine::mesh::import_meshes;

    #[test]
    fn test_two_planes() {
        let meshes = import_meshes("./tests/data/two_planes.fbx").unwrap();
        assert_eq!(meshes.len(), 1);
        let mesh = &meshes[0];
        assert_eq!(mesh.id, "./tests/data/two_planes.fbx#1");
        assert_eq!(mesh.vertices.len(), 24);
        assert_eq!(mesh.normals.len(), 24);
        assert_eq!(mesh.coords.len(), 16);
        assert_eq!(mesh.indices, vec![3, 2, 0, 3, 1, 0, 4, 5, 7, 4, 6, 7]);
        assert_eq!(mesh.coords, vec![
            1.0, 1.0, 0.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0
        ]);
    }

    #[test]
    fn test_nature() {
        let meshes = import_meshes("./tests/data/nature.fbx").unwrap();
        assert_eq!(meshes.len(), 1);
        let mesh = &meshes[0];
        assert_eq!(mesh.id, "./tests/data/nature.fbx#1");
        assert_eq!(mesh.vertices.len(), 636);
        assert_eq!(mesh.normals.len(), 636);
        assert_eq!(mesh.coords.len(), 424);
        assert_eq!(mesh.indices.len(), 1248);
    }

    #[test]
    fn test_production_ready_mesh() {
        let meshes = import_meshes("./tests/data/production_ready_mesh.fbx").unwrap();
        assert_eq!(meshes.len(), 1);
        let mesh = &meshes[0];
        assert_eq!(mesh.id, "./tests/data/production_ready_mesh.fbx#1");
        assert_eq!(mesh.vertices.len(), 216);
        assert_eq!(mesh.normals.len(), 216);
        // assert_eq!(mesh.coords.len(), 246);
        assert_eq!(mesh.indices, vec![
            0, 1, 7, 0, 6, 7, 1, 2, 8, 1, 7, 8, 2, 3, 9, 2, 8, 9, 3, 4, 10, 3, 9, 10, 4, 5, 11, 4,
            10, 11, 5, 0, 6, 5, 11, 6, 13, 17, 12, 8, 9, 13, 8, 12, 13, 9, 10, 14, 9, 13, 14, 10,
            11, 15, 10, 14, 15, 11, 6, 16, 11, 15, 16, 6, 7, 17, 6, 16, 17, 7, 8, 12, 7, 17, 12,
            13, 14, 16, 13, 17, 16, 16, 14, 15, 40, 21, 19, 40, 41, 19, 20, 39, 25, 26, 21, 40, 36,
            35, 23, 36, 22, 23, 23, 35, 34, 23, 24, 34, 38, 33, 25, 26, 25, 33, 26, 32, 33, 38, 37,
            30, 38, 33, 30, 35, 28, 29, 35, 34, 29, 36, 27, 28, 36, 35, 28, 34, 29, 37, 34, 38, 37,
            34, 38, 39, 34, 24, 39, 20, 25, 26, 20, 40, 26, 18, 20, 40, 18, 41, 40, 18, 49, 39, 18,
            20, 39, 27, 42, 43, 27, 28, 43, 30, 44, 45, 30, 31, 45, 28, 43, 46, 28, 29, 46, 29, 46,
            47, 29, 37, 47, 37, 47, 44, 37, 30, 44, 42, 48, 43, 44, 48, 45, 43, 48, 46, 46, 48, 47,
            47, 48, 44, 49, 24, 39, 41, 50, 18, 23, 50, 22, 24, 50, 23, 19, 50, 41, 49, 50, 24, 18,
            50, 49, 65, 66, 19, 65, 21, 19, 52, 55, 64, 26, 65, 21, 36, 22, 53, 36, 61, 53, 53, 54,
            60, 53, 61, 60, 63, 55, 59, 26, 32, 59, 26, 55, 59, 63, 59, 58, 63, 62, 58, 61, 60, 57,
            61, 56, 57, 36, 61, 56, 36, 27, 56, 60, 63, 62, 60, 57, 62, 60, 54, 64, 60, 63, 64, 51,
            66, 65, 51, 52, 65, 51, 52, 64, 51, 71, 64, 27, 56, 67, 27, 42, 67, 58, 31, 45, 58, 68,
            45, 56, 57, 69, 56, 67, 69, 57, 62, 70, 57, 69, 70, 62, 58, 68, 62, 70, 68, 42, 67, 48,
            68, 45, 48, 67, 69, 48, 69, 70, 48, 70, 68, 48, 71, 64, 54, 66, 51, 50, 53, 22, 50, 54,
            53, 50, 19, 66, 50, 71, 54, 50, 51, 71, 50, 63, 64, 55, 38, 25, 39, 33, 30, 31, 33, 32,
            31, 59, 32, 31, 59, 58, 31, 65, 26, 55, 65, 52, 55
        ]);
    }
}
