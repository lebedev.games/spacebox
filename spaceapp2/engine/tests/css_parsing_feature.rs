#[cfg(test)]
mod tests {
    use engine::react::Color::Rgba8;
    use engine::react::Combinator::{Child, Descendant};
    use engine::react::Condition::{
        Attribute,
        Class,
        Id,
        PseudoClass,
        PseudoElement,
        Type,
        Universal,
    };
    use engine::react::Length::{Em, Percent, Px, Rem};
    use engine::react::Selector::{Complex, Compound, List, Simple};
    use engine::react::*;

    #[test]
    fn test_colors() {
        let examples = vec![
            ("#ff0000", Color::Rgba8 { r: 255, g: 0, b: 0, a: 255 }),
            ("#0000ff80", Color::Rgba8 { r: 0, g: 0, b: 255, a: 128 }),
            ("rgb(255, 0, 0)", Color::Rgba8 { r: 255, g: 0, b: 0, a: 255 }),
            ("rgba(0, 255, 0, 0.3)", Color::Rgba8 { r: 0, g: 255, b: 0, a: 76 }),
            // ("hsl(120, 100%, 50%)", Color::Rgba8 {r: 0, g: 0, b: 0, a: 255}),
            // ("hsla(120, 100%, 75%, 0.3)", Color::Rgba8 {r: 255, g: 0, b: 0, a: 255}),
            ("blue", Color::Rgba8 { r: 0, g: 0, b: 255, a: 255 }),
            ("currentcolor", Color::Current),
            ("invalid$color", Color::Rgba8 { r: 0, g: 0, b: 0, a: 255 }),
            ("", Color::Rgba8 { r: 0, g: 0, b: 0, a: 255 }),
        ];
        for (code, value) in examples {
            let color = parse_css_color(code);
            assert_eq!(color, value, "color: {}", code);
        }
    }

    #[test]
    fn test_length_values() {
        // let parser = CssParser::new();
        let examples = vec![
            ("10px", Length::Px(10)),
            ("1", Length::Px(1)),
            ("-2", Length::Px(0)),
            ("1.3rem", Length::Rem(1.3)),
            ("2em", Length::Em(2.0)),
            ("-2em", Length::Em(-2.0)),
            ("50%", Length::Percent(50.0)),
        ];
        for (code, value) in examples {
            let length = parse_css_length(code);
            assert_eq!(length, value, "length: {}", code);
        }
    }

    #[test]
    fn test_urls() {
        let examples = vec![
            (
                "url(https://example.com/img.jpg)",
                Url::Absolute("https://example.com/img.jpg".into()),
            ),
            (r#"url("pdficon.jpg")"#, Url::Relative("pdficon.jpg".into())),
            ("url('../images/bullet.jpg')", Url::Relative("../images/bullet.jpg".into())),
            ("url(#IDofSVGpath)", Url::Relative("#IDofSVGpath".into())),
            ("url(masks.svg#mask1)", Url::Relative("masks.svg#mask1".into())),
            ("url(data:image/png;base64,iRxVB==);", Url::Data {
                content_type: "image/png;base64".into(),
                content: "iRxVB==".into(),
            }),
        ];
        for (code, value) in examples {
            let url = parse_css_url(code);
            assert_eq!(url, value);
        }
    }

    #[test]
    fn test_simple_condition_fallback() {
        let code = "{}";
        let rule = parse_rule(code);
        assert_eq!(rule.selector, Simple(Universal));
    }

    #[test]
    fn test_simple_universal_condition() {
        let code = "* {}";
        let rule = parse_rule(code);
        assert_eq!(rule.selector, Simple(Universal));
    }

    #[test]
    fn test_simple_type_condition() {
        let code = "div {}";
        let rule = parse_rule(code);
        assert_eq!(rule.selector, Simple(Type("div".into())));
    }

    #[test]
    fn test_simple_class_condition() {
        let code = ".button {}";
        let rule = parse_rule(code);
        assert_eq!(rule.selector, Simple(Class("button".into())));
    }

    #[test]
    fn test_simple_id_selector() {
        let code = "#button {}";
        let rule = parse_rule(code);
        assert_eq!(rule.selector, Simple(Id("button".into())));
    }

    #[test]
    fn test_simple_attribute_selector() {
        let code = "[target=_blank] {}";
        let rule = parse_rule(code);
        assert_eq!(
            rule.selector,
            Simple(Attribute { attribute: "target".into(), value: "_blank".into() })
        );
    }

    #[test]
    fn test_simple_pseudo_class_selector() {
        let code = ":root {}";
        let rule = parse_rule(code);
        assert_eq!(rule.selector, Simple(PseudoClass(Pseudo::Root)));
    }

    #[test]
    fn test_simple_pseudo_element_selector() {
        let code = "::first-line {}";
        let rule = parse_rule(code);
        assert_eq!(rule.selector, Simple(PseudoElement("first-line".into())));
    }

    #[test]
    fn test_compound_type_and_class_selector() {
        let code = "link.me {}";
        let rule = parse_rule(code);
        assert_eq!(rule.selector, Compound(vec![Type("link".into()), Class("me".into())]));
    }

    #[test]
    fn test_compound_type_and_attribute_selector() {
        let code = "link[target=_blank] {}";
        let rule = parse_rule(code);
        assert_eq!(
            rule.selector,
            Compound(vec![Type("link".into()), Attribute {
                attribute: "target".into(),
                value: "_blank".into()
            }])
        );
    }

    #[test]
    fn test_compound_type_and_pseudo_class_selector() {
        let code = "link:visited {}";
        let rule = parse_rule(code);
        assert_eq!(
            rule.selector,
            Compound(vec![Type("link".into()), PseudoClass(Pseudo::Visited)])
        );
    }

    #[test]
    fn test_compound_short_type_and_pseudo_class_selector() {
        let code = "a:visited {}";
        let rule = parse_rule(code);
        assert_eq!(rule.selector, Compound(vec![Type("a".into()), PseudoClass(Pseudo::Visited)]));
    }

    #[test]
    fn test_compound_type_and_pseudo_element_selector() {
        let code = "link::after {}";
        let rule = parse_rule(code);
        assert_eq!(
            rule.selector,
            Compound(vec![Type("link".into()), PseudoElement("after".into())])
        );
    }

    #[test]
    fn test_descendant_simple_selectors() {
        let code = "div .button {}";
        let rule = parse_rule(code);
        assert_eq!(
            rule.selector,
            Complex(Descendant {
                first: vec![Type("div".into())],
                second: vec![Class("button".into())]
            })
        )
    }

    #[test]
    fn test_descendant_compound_selectors() {
        let code = "div#main .link:visited {}";
        let rule = parse_rule(code);
        assert_eq!(
            rule.selector,
            Complex(Descendant {
                first: vec![Type("div".into()), Id("main".into())],
                second: vec![Class("link".into()), PseudoClass(Pseudo::Visited)]
            })
        )
    }

    #[test]
    fn test_child_simple_selectors() {
        let code = "div > .button {}";
        let rule = parse_rule(code);
        assert_eq!(
            rule.selector,
            Complex(Child {
                first: vec![Type("div".into())],
                second: vec![Class("button".into())]
            })
        )
    }

    #[test]
    fn test_child_simple_selectors_no_spaces() {
        let code = "div>.button {}";
        let rule = parse_rule(code);
        assert_eq!(
            rule.selector,
            Complex(Child {
                first: vec![Type("div".into())],
                second: vec![Class("button".into())]
            })
        )
    }

    #[test]
    fn test_child_compound_selectors() {
        let code = "div#main > .link:visited {}";
        let rule = parse_rule(code);
        assert_eq!(
            rule.selector,
            Complex(Child {
                first: vec![Type("div".into()), Id("main".into())],
                second: vec![Class("link".into()), PseudoClass(Pseudo::Visited)]
            })
        )
    }

    #[test]
    fn test_simple_selectors_grouping() {
        let code = "*, div, .button, #me, :root {}";
        let rule = parse_rule(code);
        assert_eq!(
            rule.selector,
            List(vec![
                Simple(Universal),
                Simple(Type("div".into())),
                Simple(Class("button".into())),
                Simple(Id("me".into())),
                Simple(PseudoClass(Pseudo::Root)),
            ])
        )
    }

    #[test]
    fn test_parse_smelly_code_stylesheet() {
        let code = r#"
        :root {
            color: black;
        }
        #name {
            color: red
        }
        button>.link{color: rgba(255, 255,255, 0.5);}
        a:visited {
        }

        * {
        color:blue;
        }
        "#;
        let stylesheet = parse_stylesheet(code);
        assert_eq!(stylesheet.rules.len(), 5);

        let rule = &stylesheet.rules[0];
        assert_eq!(rule.selector, Simple(PseudoClass(Pseudo::Root)));
        assert_eq!(rule.style.color, Rgba8 { r: 0, g: 0, b: 0, a: 255 });

        let rule = &stylesheet.rules[1];
        assert_eq!(rule.selector, Simple(Id("name".into())));
        assert_eq!(rule.style.color, Rgba8 { r: 255, g: 0, b: 0, a: 255 });

        let rule = &stylesheet.rules[2];
        assert_eq!(
            rule.selector,
            Complex(Child {
                first: vec![Type("button".into())],
                second: vec![Class("link".into())]
            })
        );
        assert_eq!(rule.style.color, Rgba8 { r: 255, g: 255, b: 255, a: 127 });

        let rule = &stylesheet.rules[3];
        assert_eq!(rule.selector, Compound(vec![Type("a".into()), PseudoClass(Pseudo::Visited)]));
        assert_eq!(rule.style.color, Rgba8 { r: 0, g: 0, b: 0, a: 255 });

        let rule = &stylesheet.rules[4];
        assert_eq!(rule.selector, Simple(Universal));
        assert_eq!(rule.style.color, Rgba8 { r: 0, g: 0, b: 255, a: 255 });
    }

    #[test]
    fn test_align_content_property() {
        let code = "div {align-content: center}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.align_content, AlignContent::Center);
    }

    #[test]
    fn test_align_items_property() {
        let code = "div {align-items: flex-start}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.align_items, AlignItems::FlexStart);
    }

    #[test]
    fn test_align_self_property() {
        let code = "div {align-self: flex-start}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.align_self, AlignSelf::FlexStart);
    }

    #[test]
    fn test_background_attachment_property() {
        let code = "div {background-attachment: fixed}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.background.attachment, BackgroundAttachment::Fixed);
    }

    #[test]
    fn test_background_blend_mode_property() {
        let code = "div {background-blend-mode: darken}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.background.blend_mode, BackgroundBlendMode::Darken);
    }

    #[test]
    fn test_background_clip_property() {
        let code = "div {background-clip: content-box}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.background.clip, BackgroundClip::ContentBox);
    }

    #[test]
    fn test_background_color_property() {
        let code = "div {background-color: red;}";
        let rule = parse_rule(code);
        assert_eq!(
            rule.style.background.color,
            BackgroundColor::Color(Color::Rgba8 { r: 255, g: 0, b: 0, a: 255 })
        );
    }

    #[test]
    fn test_background_image_property_url() {
        let code = r#"div {background-image: url("image.png");}"#;
        let rule = parse_rule(code);
        assert_eq!(
            rule.style.background.image,
            BackgroundImage::Url(Url::Relative("image.png".into()))
        );
    }

    #[test]
    fn test_background_origin_property() {
        let code = "div {background-origin: content-box;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.background.origin, BackgroundOrigin::ContentBox);
    }

    #[test]
    fn test_background_position_property_only_horizontal() {
        let code = "div {background-position: right}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.background.position, BackgroundPosition::Keyword {
            horizontal: HorizontalKeyword::Right,
            vertical: VerticalKeyword::Center
        });
    }

    #[test]
    fn test_background_position_property() {
        let code = "div {background-position: 10% 20%}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.background.position, BackgroundPosition::Length {
            horizontal: Length::Percent(10.0),
            vertical: Length::Percent(20.0)
        });
    }

    #[test]
    fn test_background_repeat_property() {
        let code = "div {background-repeat: no-repeat;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.background.repeat, BackgroundRepeat::NoRepeat);
    }

    #[test]
    fn test_background_size_property() {
        let code = "div {background-size: cover;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.background.size, BackgroundSize::Cover);
    }

    #[test]
    fn test_background_all_properties() {
        let code = r#"div {
            background-attachment: fixed;
            background-blend-mode: darken;
            background-clip: content-box;
            background-color: red;
            background-image: url("image.png");
            background-origin: content-box;
            background-position: 10% 20%;
            background-repeat: no-repeat;
            background-size: cover;
        }"#;
        let rule = parse_rule(code);
        assert_eq!(rule.style.background.attachment, BackgroundAttachment::Fixed);
        assert_eq!(rule.style.background.blend_mode, BackgroundBlendMode::Darken);
        assert_eq!(rule.style.background.clip, BackgroundClip::ContentBox);
        assert_eq!(
            rule.style.background.color,
            BackgroundColor::Color(Color::Rgba8 { r: 255, g: 0, b: 0, a: 255 })
        );
        assert_eq!(
            rule.style.background.image,
            BackgroundImage::Url(Url::Relative("image.png".into()))
        );
        assert_eq!(rule.style.background.origin, BackgroundOrigin::ContentBox);
        assert_eq!(rule.style.background.position, BackgroundPosition::Length {
            horizontal: Length::Percent(10.0),
            vertical: Length::Percent(20.0)
        });
        assert_eq!(rule.style.background.repeat, BackgroundRepeat::NoRepeat);
        assert_eq!(rule.style.background.size, BackgroundSize::Cover);
    }

    #[test]
    fn test_background_border_property() {
        let code = "div {border: 4px dotted blue;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.border.width, BorderWidth {
            top: Px(4),
            right: Px(4),
            bottom: Px(4),
            left: Px(4)
        });
        assert_eq!(rule.style.border.style, BorderStyle {
            top: BorderSideStyle::Dotted,
            right: BorderSideStyle::Dotted,
            bottom: BorderSideStyle::Dotted,
            left: BorderSideStyle::Dotted
        });
        assert_eq!(rule.style.border.color, BorderColor {
            top: BorderSideColor::Color(Color::Rgba8 { r: 0, g: 0, b: 255, a: 255 }),
            right: BorderSideColor::Color(Color::Rgba8 { r: 0, g: 0, b: 255, a: 255 }),
            bottom: BorderSideColor::Color(Color::Rgba8 { r: 0, g: 0, b: 255, a: 255 }),
            left: BorderSideColor::Color(Color::Rgba8 { r: 0, g: 0, b: 255, a: 255 })
        });
    }

    #[test]
    fn test_background_border_left_property() {
        let code = "div {border-left: 4px solid currentcolor;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.border.width.left, Px(4));
        assert_eq!(rule.style.border.style.left, BorderSideStyle::Solid);
        assert_eq!(rule.style.border.color.left, BorderSideColor::Color(Color::Current));
    }

    #[test]
    fn test_background_border_right_property() {
        let code = "div {border-right: 4px solid currentcolor;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.border.width.right, Px(4));
        assert_eq!(rule.style.border.style.right, BorderSideStyle::Solid);
        assert_eq!(rule.style.border.color.right, BorderSideColor::Color(Color::Current));
    }

    #[test]
    fn test_background_border_top_property() {
        let code = "div {border-top: 4px solid currentcolor;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.border.width.top, Px(4));
        assert_eq!(rule.style.border.style.top, BorderSideStyle::Solid);
        assert_eq!(rule.style.border.color.top, BorderSideColor::Color(Color::Current));
    }

    #[test]
    fn test_background_border_bottom_property() {
        let code = "div {border-bottom: 4px solid currentcolor;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.border.width.bottom, Px(4));
        assert_eq!(rule.style.border.style.bottom, BorderSideStyle::Solid);
        assert_eq!(rule.style.border.color.bottom, BorderSideColor::Color(Color::Current));
    }

    #[test]
    fn test_background_border_property_overrides() {
        let code = r#"div {
            border: 1px dotted blue;
            border-bottom: 4px solid currentcolor;
        }"#;
        let rule = parse_rule(code);
        assert_eq!(rule.style.border.width.bottom, Px(4));
        assert_eq!(rule.style.border.style.bottom, BorderSideStyle::Solid);
        assert_eq!(rule.style.border.color.bottom, BorderSideColor::Color(Color::Current));
    }

    #[test]
    fn test_background_border_radius_property() {
        let code = "div {border-radius: 25px;}";
        let rule = parse_rule(code);
        let radius = rule.style.border.radius;
        assert_eq!(radius.top_left, Px(25));
        assert_eq!(radius.top_right, Px(25));
        assert_eq!(radius.bottom_right, Px(25));
        assert_eq!(radius.bottom_left, Px(25));
    }

    #[test]
    fn test_background_border_radius_property_two_values() {
        let code = "div {border-radius: 15px 50px;}";
        let rule = parse_rule(code);
        let radius = rule.style.border.radius;
        assert_eq!(radius.top_left, Px(15));
        assert_eq!(radius.top_right, Px(50));
        assert_eq!(radius.bottom_right, Px(15));
        assert_eq!(radius.bottom_left, Px(50));
    }

    #[test]
    fn test_background_border_radius_property_override() {
        let code = r#"div {
            border-radius: 25px;
            border-bottom-left-radius: 10px;
        }"#;
        let rule = parse_rule(code);
        let radius = rule.style.border.radius;
        assert_eq!(radius.top_left, Px(25));
        assert_eq!(radius.top_right, Px(25));
        assert_eq!(radius.bottom_right, Px(25));
        assert_eq!(radius.bottom_left, Px(10));
    }

    #[test]
    fn test_bottom_property() {
        let code = "div {bottom: 10px;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.bottom, Bottom::Length(Px(10)));
    }

    #[test]
    fn test_box_decoration_break_property() {
        let code = "div {box-decoration-break: clone;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.box_decoration_break, BoxDecorationBreak::Clone);
    }

    #[test]
    fn test_box_shadow_property() {
        let code = "div {box-shadow: 0px 4px 5px rgba(161, 180, 203, 0.17);}";
        let rule = parse_rule(code);
        assert_eq!(
            rule.style.box_shadow,
            BoxShadow::Shadows(vec![Shadow {
                horizontal_offset: Px(0),
                vertical_offset: Px(4),
                blur: Px(5),
                spread: Px(0),
                color: Color::Rgba8 { r: 161, g: 180, b: 203, a: 43 },
                outset: true
            }])
        );
    }

    #[test]
    fn test_box_shadow_property_inset() {
        let code = "div {box-shadow: 5px 10px inset}";
        let rule = parse_rule(code);
        assert_eq!(
            rule.style.box_shadow,
            BoxShadow::Shadows(vec![Shadow {
                horizontal_offset: Px(5),
                vertical_offset: Px(10),
                blur: Px(0),
                spread: Px(0),
                color: Color::Rgba8 { r: 0, g: 0, b: 0, a: 255 },
                outset: false
            }])
        );
    }

    #[test]
    fn test_box_shadow_property_multiple() {
        let code = "div {box-shadow: 5px 5px blue, 10px 10px red;}";
        let rule = parse_rule(code);
        assert_eq!(
            rule.style.box_shadow,
            BoxShadow::Shadows(vec![
                Shadow {
                    horizontal_offset: Px(5),
                    vertical_offset: Px(5),
                    blur: Px(0),
                    spread: Px(0),
                    color: Color::Rgba8 { r: 0, g: 0, b: 255, a: 255 },
                    outset: true
                },
                Shadow {
                    horizontal_offset: Px(10),
                    vertical_offset: Px(10),
                    blur: Px(0),
                    spread: Px(0),
                    color: Color::Rgba8 { r: 255, g: 0, b: 0, a: 255 },
                    outset: true
                }
            ])
        );
    }

    #[test]
    fn test_box_sizing_property() {
        let code = "div {box-sizing: border-box;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.box_sizing, BoxSizing::BorderBox);
    }

    #[test]
    fn test_caption_side_property() {
        let code = "div {caption-side: bottom;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.caption_side, CaptionSide::Bottom);
    }

    #[test]
    fn test_color_property() {
        let code = "div {color: rgba(161, 180, 203, 0.17);}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.color, Color::Rgba8 { r: 161, g: 180, b: 203, a: 43 });
    }

    #[test]
    fn test_display_property() {
        let code = "div {display: flex;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.display, Display::Flex);
    }

    #[test]
    fn test_flex_property() {
        let code = "div {flex: 1;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.flex.basis, FlexBasis::Length(Px(1)));
    }

    #[test]
    fn test_flex_basis_property() {
        let code = "div {flex-basis: 50%;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.flex.basis, FlexBasis::Length(Percent(50.0)));
    }

    #[test]
    fn test_flex_direction_property() {
        let code = "div {flex-direction: column-reverse;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.flex.direction, FlexDirection::ColumnReverse);
    }

    #[test]
    fn test_flex_grow_property() {
        let code = "div {flex-grow: 1.5;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.flex.grow, 1.5);
    }

    #[test]
    fn test_flex_shrink_property() {
        let code = "div {flex-shrink: 2;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.flex.shrink, 2.0);
    }

    #[test]
    fn test_flex_wrap_property() {
        let code = "div {flex-wrap: wrap-reverse;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.flex.wrap, FlexWrap::WrapReverse);
    }

    #[test]
    fn test_flex_all_properties() {
        let code = r#"div {
            flex-basis: 50%;
            flex-direction: column-reverse;
            flex-grow: 1.5;
            flex-shrink: 2;
            flex-wrap: wrap-reverse;
        }"#;
        let rule = parse_rule(code);
        assert_eq!(rule.style.flex.basis, FlexBasis::Length(Percent(50.0)));
        assert_eq!(rule.style.flex.direction, FlexDirection::ColumnReverse);
        assert_eq!(rule.style.flex.grow, 1.5);
        assert_eq!(rule.style.flex.shrink, 2.0);
        assert_eq!(rule.style.flex.wrap, FlexWrap::WrapReverse);
    }

    #[test]
    fn test_float_property() {
        let code = "div {float: right;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.float, Float::Right);
    }

    #[test]
    fn test_font_family_property() {
        let code = "div {font-family: sans-serif;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.font.family, Some("sans-serif".to_string()));
    }

    #[test]
    fn test_font_kerning_property() {
        let code = "div {font-kerning: auto;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.font.kerning, FontKerning::Auto);
    }

    #[test]
    fn test_font_size_property() {
        let code = "div {font-size: 32px;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.font.size, Px(32));
    }

    #[test]
    fn test_font_style_property() {
        let code = "div {font-style: italic;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.font.style, FontStyle::Italic);
    }

    #[test]
    fn test_font_weight_property() {
        let code = "div {font-weight: 900;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.font.weight, FontWeight::Number(900));
    }

    #[test]
    fn test_font_all_property() {
        let code = r#"div {
            font-family: /assets/font.ttf;
            font-style: normal;
            font-weight: normal;
            font-size: 2rem;
        }"#;
        let rule = parse_rule(code);
        assert_eq!(rule.style.font.family, Some("/assets/font.ttf".to_string()));
        assert_eq!(rule.style.font.style, FontStyle::Normal);
        assert_eq!(rule.style.font.weight, FontWeight::Normal);
        assert_eq!(rule.style.font.size, Rem(2.0));
    }

    #[test]
    fn test_height_property() {
        let code = "div {height: 10px;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.height, Height::Length(Px(10)));
    }

    #[test]
    fn test_justify_content_property() {
        let code = "div {justify-content: space-evenly;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.justify_content, JustifyContent::SpaceEvenly);
    }

    #[test]
    fn test_left_property() {
        let code = "div {left: 50%;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.left, Left::Length(Percent(50.0)));
    }

    #[test]
    fn test_line_height_property() {
        let code = "div {line-height: 2rem;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.line_height, LineHeight::Length(Rem(2.0)));
    }

    #[test]
    fn test_margin_all_property() {
        let code = r#"div {
            margin-bottom: 10px;
            margin-left: 20px;
            margin-right: 30px;
            margin-top: 40px;
        }"#;
        let rule = parse_rule(code);
        assert_eq!(rule.style.margin.bottom, MarginSide::Length(Px(10)));
        assert_eq!(rule.style.margin.left, MarginSide::Length(Px(20)));
        assert_eq!(rule.style.margin.right, MarginSide::Length(Px(30)));
        assert_eq!(rule.style.margin.top, MarginSide::Length(Px(40)));
    }

    #[test]
    fn test_margin_property_override() {
        let code = r#"div {
            margin: 5px;
            margin-top: 40px;
        }"#;
        let rule = parse_rule(code);
        assert_eq!(rule.style.margin.bottom, MarginSide::Length(Px(5)));
        assert_eq!(rule.style.margin.left, MarginSide::Length(Px(5)));
        assert_eq!(rule.style.margin.right, MarginSide::Length(Px(5)));
        assert_eq!(rule.style.margin.top, MarginSide::Length(Px(40)));
    }

    #[test]
    fn test_max_height_property() {
        let code = "div {max-height: 10px;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.max_height, MaxHeight::Length(Px(10)));
    }

    #[test]
    fn test_max_width_property() {
        let code = "div {max-width: 10px;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.max_width, MaxWidth::Length(Px(10)));
    }

    #[test]
    fn test_min_height_property() {
        let code = "div {min-height: 10px;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.min_height, MinHeight::Length(Px(10)));
    }

    #[test]
    fn test_min_width_property() {
        let code = "div {min-width: 10px;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.min_width, MinWidth::Length(Px(10)));
    }

    #[test]
    fn test_opacity_property() {
        let code = "div {opacity: 0.5;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.opacity, 0.5);
    }

    #[test]
    fn test_order_property() {
        let code = "div {order: 5;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.order, 5.0);
    }

    #[test]
    fn test_overflow_property() {
        let code = "div {overflow: hidden;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.overflow, Overflow::Hidden);
    }

    #[test]
    fn test_overflow_x_property() {
        let code = "div {overflow-x: hidden;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.overflow_x, Overflow::Hidden);
    }

    #[test]
    fn test_overflow_y_property() {
        let code = "div {overflow-y: hidden;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.overflow_y, Overflow::Hidden);
    }

    #[test]
    fn test_padding_all_property() {
        let code = r#"div {
            padding-bottom: 10px;
            padding-left: 20px;
            padding-right: 30px;
            padding-top: 40px;
        }"#;
        let rule = parse_rule(code);
        assert_eq!(rule.style.padding.bottom, Px(10));
        assert_eq!(rule.style.padding.left, Px(20));
        assert_eq!(rule.style.padding.right, Px(30));
        assert_eq!(rule.style.padding.top, Px(40));
    }

    #[test]
    fn test_padding_property_override() {
        let code = r#"div {
            padding: 5px;
            padding-top: 40px;
        }"#;
        let rule = parse_rule(code);
        assert_eq!(rule.style.padding.bottom, Px(5));
        assert_eq!(rule.style.padding.left, Px(5));
        assert_eq!(rule.style.padding.right, Px(5));
        assert_eq!(rule.style.padding.top, Px(40));
    }

    #[test]
    fn test_pointer_events_property() {
        let code = "div {pointer-events: none;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.pointer_events, PointerEvents::None);
    }

    #[test]
    fn test_position_property() {
        let code = "div {position: fixed;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.position, Position::Fixed);
    }

    #[test]
    fn test_right_property() {
        let code = "div {right: 10px;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.right, Right::Length(Px(10)));
    }

    #[test]
    fn test_text_align_property() {
        let code = "div {text-align: center;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.text.align, TextAlign::Center);
    }

    #[test]
    fn test_text_align_last_property() {
        let code = "div {text-align-last: center;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.text.align_last, TextAlignLast::Center);
    }

    #[test]
    fn test_text_decoration_color_property() {
        let code = "div {text-decoration-color: red;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.text.decoration.color, Color::Rgba8 { r: 255, g: 0, b: 0, a: 255 });
    }

    #[test]
    fn test_text_decoration_line_property() {
        let code = "div {text-decoration-line: line-through;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.text.decoration.line, TextDecorationLine::LineThrough);
    }

    #[test]
    fn test_text_decoration_style_property() {
        let code = "div {text-decoration-style: double;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.text.decoration.style, TextDecorationStyle::Double);
    }

    #[test]
    fn test_text_indent_property() {
        let code = "div {text-indent: -2em;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.text.indent, Em(-2.0));
    }

    #[test]
    fn test_text_justify_property() {
        let code = "div {text-justify: inter-character;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.text.justify, TextJustify::InterCharacter);
    }

    #[test]
    fn test_text_shadow_property() {
        let code = "div {text-shadow: 2px 2px #ff0000;}";
        let rule = parse_rule(code);
        assert_eq!(
            rule.style.text.shadow,
            TextShadow::Shadow(Shadow {
                horizontal_offset: Px(2),
                vertical_offset: Px(2),
                blur: Px(0),
                spread: Px(0),
                color: Color::Rgba8 { r: 255, g: 0, b: 0, a: 255 },
                outset: true
            })
        );
    }

    #[test]
    fn test_text_transform_property() {
        let code = "div {text-transform: uppercase;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.text.transform, TextTransform::Uppercase);
    }

    #[test]
    fn test_top_property() {
        let code = "div {top: 10px;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.top, Top::Length(Px(10)));
    }

    #[test]
    fn test_user_select_property() {
        let code = "div {user-select: none;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.user_select, UserSelect::None);
    }

    #[test]
    fn test_vertical_align_property() {
        let code = "div {vertical-align: text-bottom;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.vertical_align, VerticalAlign::TextBottom);
    }

    #[test]
    fn test_visibility_property() {
        let code = "div {visibility: hidden;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.visibility, Visibility::Hidden);
    }

    #[test]
    fn test_white_space_property() {
        let code = "div {white-space: nowrap;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.white_space, WhiteSpace::Nowrap);
    }

    #[test]
    fn test_width_property() {
        let code = "div {width: 10px;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.width, Width::Length(Px(10)));
    }

    #[test]
    fn test_word_wrap_property() {
        let code = "div {word-wrap: break-word;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.word_wrap, WordWrap::BreakWord);
    }

    #[test]
    fn test_z_index_property() {
        let code = "div {z-index: -2;}";
        let rule = parse_rule(code);
        assert_eq!(rule.style.z_index, ZIndex::Number(-2));
    }
}
