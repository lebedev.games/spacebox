### Components

*   [spacebox](spacebox)

    Spacebox is heart of the game. Handles game mechanics and gameplay in general. 
    Acts as server in client-server architecture and provides 
    authorized instructions for spaceapp.         
    
*   spaceapp

    Spaceapp is a game engine primarily designed for game development 
    with "hot reload" experience for editing game scenes.
    Includes a rendering for 2D or 3D graphics, sound, animation, 
    AI configuration tools, networking, localization support.
    Acts as client in client-server architecture, 
    works only with spacebox.

*   database

    In general speaking database is preconfigured "instance" ready to store data of game entities, player progress.
    In current implementation it files in YAML format to provide versioning via Git and 
    simple changes detection for "hot reload" experience. 


*   space3

    Space3 is a file format developed to provide 3D game content 
    from 3D content creation software 
    (avoiding unnecessary complexity of generic solutions, like FBX for example).
    

![./readme/fig1.png](.readme/fig1.png)

### Environment Setup

1. Install [Nightly](https://doc.rust-lang.org/stable/book/appendix-07-nightly-rust.html) Rust  
    ```shell script
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
    ```
   ```shell script
    rustup toolchain install nightly
    ```
