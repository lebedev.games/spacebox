pub use keyboard::*;

mod keyboard;

#[derive(Debug, Clone)]
pub struct Frame {
    pub operations: Vec<Operation>,
}

#[derive(Debug, Clone)]
pub struct GenericVertex {
    pub position: [f32; 3],
    pub normal: [f32; 3],
    pub uv: [f32; 2],
    pub bones: [i8; 4],
    pub weights: [f32; 4],
}

#[derive(Debug, Clone)]
pub enum Value {
    Vec2([f32; 2]),
    Vec3([f32; 3]),
    Vec4([f32; 4]),
    Mat2([[f32; 2]; 2]),
    Mat3([[f32; 3]; 3]),
    Mat4([[f32; 4]; 4]),
    Sampler2d(String),
}

#[derive(Debug, Clone)]
pub struct Uniform {
    pub name: String,
    pub value: Value,
}

#[derive(Debug, Clone)]
pub enum Operation {
    ClearColorAndDepth {
        color: (f32, f32, f32, f32),
        depth: f32,
    },
    CreateTexture {
        id: String,
        size: (u32, u32),
        data: Vec<u8>,
    },
    CreateShader {
        id: String,
        vertex: String,
        fragment: String,
        geometry: Option<String>,
    },
    CreateMesh {
        id: String,
        vertices: Vec<GenericVertex>,
        indices: Vec<u32>,
    },
    DrawMesh {
        id: String,
        shader: String,
        uniforms: Vec<Uniform>,
    },
}

#[derive(Debug, Clone)]
pub enum InputEvent {
    KeyDown {
        scancode: Scancode,
        modifier: u16,
    },
    KeyUp {
        scancode: Scancode,
        modifier: u16,
    },
    TextEditing {
        text: String,
        start: i32,
        length: i32,
    },
    TextInput {
        text: String,
    },
}
