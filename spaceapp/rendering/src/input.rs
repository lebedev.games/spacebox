use std::mem::transmute;

use sdl2::event::{Event, WindowEvent};
use sdl2::video::WindowPos;

use crate::api::{InputEvent, Scancode};
use crate::Window;

impl Window {
    pub fn poll_events(&mut self) -> Option<Vec<InputEvent>> {
        let mut events = vec![];
        for event in self.event_pump.poll_iter() {
            match event {
                Event::Quit { .. } => return None,
                Event::MouseMotion { .. } => {
                    // println!("mouse motion x:{}", x);
                }
                Event::KeyDown {
                    scancode, keymod, ..
                } => {
                    let event = InputEvent::KeyDown {
                        scancode: unsafe {
                            transmute(
                                scancode
                                    .map(|value| value as i32)
                                    .unwrap_or(Scancode::Unknown as i32),
                            )
                        },
                        modifier: keymod.bits(),
                    };
                    events.push(event);
                }
                Event::KeyUp {
                    scancode, keymod, ..
                } => {
                    let event = InputEvent::KeyUp {
                        scancode: unsafe {
                            transmute(
                                scancode
                                    .map(|value| value as i32)
                                    .unwrap_or(Scancode::Unknown as i32),
                            )
                        },
                        modifier: keymod.bits(),
                    };
                    events.push(event);
                }
                Event::TextInput { text, .. } => {
                    let event = InputEvent::TextInput { text };
                    events.push(event)
                }
                Event::TextEditing {
                    text,
                    start,
                    length,
                    ..
                } => events.push(InputEvent::TextEditing {
                    text,
                    start,
                    length,
                }),
                Event::Window { win_event, .. } => match win_event {
                    WindowEvent::Moved(x, y) => {
                        Window::save_position(x, y);
                    }
                    WindowEvent::FocusGained => {
                        let window = self.facade.backend.window_mut();
                        let (width, height) = self.size;
                        window.set_size(width, height).unwrap();
                        let (x, y) = Window::load_position();
                        window.set_position(WindowPos::Positioned(x), WindowPos::Positioned(y));
                    }
                    WindowEvent::FocusLost => {
                        let window = self.facade.backend.window_mut();
                        let (x, y) = window.position();
                        let (width, _) = self.size;
                        let (w, h) = (480, 360);
                        window.set_position(
                            WindowPos::Positioned(x + width as i32 - w),
                            WindowPos::Positioned(y),
                        );
                        window.set_size(w as u32, h).unwrap();
                    }
                    _ => {}
                },
                _ => {}
            }
        }

        Some(events)
    }
}
