use client::{Appearance, Disappearance, Event, Physics};

use crate::state::{Barrier, MyCoordinates, Prop};
use crate::App;

impl App {
    pub fn handle_game_events(&mut self, events: Vec<Event>) {
        for event in events {
            match event {
                Event::Physics(event) => self.handle_physics(event),
                Event::Appearance(event) => self.handle_appearance(event),
                Event::Disappearance(event) => self.handle_disappearance(event),
                Event::Universe(event) => match event {
                    /*Universe::TimeUpdated { ms } => {
                        info!("ms: {}", ms);
                    }*/
                    _ => {}
                },
                _ => {}
            }
        }
    }

    fn handle_physics(&mut self, event: Physics) {
        match event {
            Physics::SpaceConnected { .. } => {}
            Physics::SpaceDisconnected { .. } => {}
            Physics::BodySpaceUpdated { .. } => {}
            Physics::BodyPositionUpdated { .. } => {}
            Physics::BarrierSpaceUpdated { .. } => {}
            _ => {}
        }
    }

    fn handle_appearance(&mut self, event: Appearance) {
        let state = &mut self.state;
        match event {
            Appearance::CharacterAppeared { .. } => {}
            Appearance::PropAppeared {
                id,
                key,
                barrier,
                obstacle,
                space,
                position,
                rotation,
            } => {
                // let prefab = self.load_prop_prefab(&key);
                /*let prop = Prop { id };
                let barrier = Barrier { id: barrier, position: MyCoordinates {} };
                state.props.insert(id, prop);
                state.barriers.insert(id, barrier);*/
            }
            _ => {}
        }
    }

    fn handle_disappearance(&mut self, event: Disappearance) {
        let state = &mut self.state;
        match event {
            Disappearance::CharacterVanished { .. } => {}
            Disappearance::PropVanished { id } => {
                // ? barrier
                // state.props.remove(&id);
            }
            _ => {}
        }
    }
}
