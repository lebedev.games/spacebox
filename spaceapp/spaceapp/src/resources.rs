use std::path::{Path, PathBuf};

use notify::DebouncedEvent;

use crate::state::PropPrefab;
use crate::{App, EditCommand};

#[derive(Debug, Clone, Copy, Hash, PartialEq)]
pub struct ResourceVersion(usize);

#[derive(Debug)]
pub enum ResourceFormat {
    Unknown,
    PropPrefab,
    Texture,
    Mesh,
}

impl ResourceFormat {
    pub fn from(path: String) -> Self {
        let components: Vec<&str> = path.split("/").collect();
        match &components[..] {
            ["assets", "prefabs", "props", _] => ResourceFormat::PropPrefab,
            ["assets", "textures", ..] => ResourceFormat::Texture,
            ["assets", "meshes", ..] => ResourceFormat::Mesh,
            _ => ResourceFormat::Unknown,
        }
    }
}

#[derive(Debug)]
pub struct Resource {
    version: ResourceVersion,
}

impl App {
    pub fn load_prop_prefab(&mut self, key: &String) -> &PropPrefab {
        if !self.state.prop_prefabs.contains_key(key) {
            let path = Path::new(&format!("/assets/prefabs/props/{}.yaml", key));
            // self.state.resources.insert(path.to_path_buf(), Resource)
        }

        self.get_prop_prefab(key)
    }

    pub fn load_resource(&mut self) {}

    pub fn load_resource_async(&mut self, command: EditCommand) {
        if let Err(error) = self.editor.send(command.clone()) {
            error!("Unable to send edit command {:?}, {:?}", command, error);
        }
    }

    pub fn get_prop_prefab(&self, key: &String) -> &PropPrefab {
        match self.state.prop_prefabs.get(key) {
            Some(prefab) => prefab,
            None => {
                error!("Unable to get prop prefab key={}, use fallback", key);
                &self.state.prop_prefab_fallback
            }
        }
    }

    pub fn handle_file_events(&mut self, events: Vec<DebouncedEvent>) {
        for event in events {
            match event {
                DebouncedEvent::Create(path) => {
                    if self.is_resource_used_now(&path) {
                        self.update_resource(path)
                    }
                }
                DebouncedEvent::Write(path) => {
                    if self.is_resource_used_now(&path) {
                        self.update_resource(path)
                    }
                }
                DebouncedEvent::Remove(path) => {
                    if self.is_resource_used_now(&path) {
                        self.remove_resource(path)
                    }
                }
                DebouncedEvent::Rename(source, destination) => {
                    if self.is_resource_used_now(&source) {
                        self.remove_resource(source);
                    }
                    if self.is_resource_used_now(&destination) {
                        self.update_resource(destination);
                    }
                }
                DebouncedEvent::Error(error, path) => {
                    error!("Unable to handle file path={:?} change, {:?}", path, error)
                }
                _ => {}
            }
        }
    }

    pub fn is_resource_used_now(&self, path: &PathBuf) -> bool {
        self.resources.contains_key(path)
    }

    pub fn update_resource(&mut self, path: PathBuf) {
        info!("Update resource {}", path.display())
    }

    pub fn remove_resource(&mut self, path: PathBuf) {
        info!("Remove resource {}", path.display())
    }
}
