#[macro_use]
extern crate log;

use std::collections::HashMap;
use std::fs;
use std::path::{Path, PathBuf};
use std::rc::Rc;
use std::sync::mpsc::{channel, SendError, Sender};
use std::thread::sleep;
use std::time::Duration;

use client::{Client, ClientError, Event};
use log::LevelFilter;
use log4rs::append::console::ConsoleAppender;
use log4rs::append::file::FileAppender;
use log4rs::config::{Appender, Logger, Root};
use log4rs::Config;
use notify::DebouncedEvent;
use rendering::api::{InputEvent, Scancode};
use rendering::{RendererError, Window};

use crate::persistence::{read_file, DataError, DataPath};
use crate::resources::Resource;
use crate::state::{PropPrefab, State};
use log4rs::append::Append;

mod persistence;
mod projection;
mod resources;
mod state;

pub enum LoadEvent {
    TextureLoaded,
    SpaceContentLoaded,
}

pub enum LoadCommand {}

pub struct Material {}

pub struct Mesh {}

#[derive(Debug, Clone)]
pub enum EditCommand {
    CreateProp { key: String },
}

pub struct App {
    pub state: State,
    pub resources: HashMap<PathBuf, Rc<Resource>>,
    pub loader: Sender<LoadCommand>,
    pub editor: Sender<EditCommand>,
    pub window: Window,
    pub client: Client,
}

#[derive(Debug)]
pub enum AppError {
    Renderer(RendererError),
    Data(DataError),
    Client(ClientError),
}

impl From<DataError> for AppError {
    fn from(error: DataError) -> Self {
        Self::Data(error)
    }
}

impl From<RendererError> for AppError {
    fn from(error: RendererError) -> Self {
        Self::Renderer(error)
    }
}

impl From<ClientError> for AppError {
    fn from(error: ClientError) -> Self {
        Self::Client(error)
    }
}

pub enum Edit {
    Game(EditCommand),
}

impl App {
    pub fn new(assets_path: PathBuf) -> Result<App, AppError> {
        let window = Window::new("Hello", 1280, 960)?;
        let (loader, loader_receiver) = channel();
        let (editor, editor_receiver) = channel();

        let prop_prefab_fallback = read_file(assets_path.join("prefabs/props.fallback.yaml"))?;

        Ok(App {
            state: State {
                prop_prefab_fallback,
                prop_prefabs: HashMap::new(),
                props: HashMap::new(),
                barriers: HashMap::new(),
            },
            resources: HashMap::new(),
            loader,
            editor,
            window,
            client: Client::new("0.0.0.0:8087")?,
        })
    }

    pub fn handle_edit(&mut self, operations: Vec<Edit>) {
        for operation in operations {
            match operation {
                Edit::Game(command) => self.send_game_edit(command),
            }
        }
    }

    pub fn send_game_edit(&mut self, command: EditCommand) {
        if let Err(error) = self.editor.send(command.clone()) {
            error!("Unable to send edit command {:?}, {:?}", command, error);
        }
    }

    pub fn handle_file_changes(&mut self) {}

    pub fn handle_game_changes(&mut self) {}

    pub fn handle_user_input(&mut self) {}

    pub fn render(&mut self) {}

    pub fn update(&mut self) {
        loop {
            self.handle_file_changes();
            self.handle_game_changes();
            self.handle_user_input();
            self.render();

            sleep(Duration::from_millis(10));
        }
    }

    pub fn connect_game_server(&mut self) {
        self.client.connect("editor".to_string());
    }

    pub fn disconnect_game_server(&mut self) {
        self.client.disconnect();
    }

    pub fn run(&mut self) {
        loop {
            let user_events = match self.window.poll_events() {
                Some(events) => events,
                None => {
                    info!("Window is closed, stop application");
                    break;
                }
            };
            for event in user_events {
                match event {
                    InputEvent::KeyDown { scancode, .. } => match scancode {
                        Scancode::F2 => self.connect_game_server(),
                        Scancode::F3 => self.disconnect_game_server(),
                        _ => {}
                    },
                    _ => {}
                }
            }

            let game_events = self.client.poll_events();
            self.handle_game_events(game_events);
            if self.client.is_connection_lost() {
                info!("Connection lost, destroy game scene");
                // self.client.flush();
            }

            sleep(Duration::from_millis(50));
        }
    }
}

fn main() {
    let stdout = ConsoleAppender::builder().build();
    let file = FileAppender::builder().build("spaceapp.log").unwrap();
    let config = Config::builder()
        .appender(Appender::builder().build("stdout", Box::new(stdout)))
        .appender(Appender::builder().build("file", Box::new(file)))
        .build(
            Root::builder()
                .appender("stdout")
                .appender("file")
                .build(LevelFilter::Info),
        )
        .unwrap();
    log4rs::init_config(config).unwrap();

    let assets_path = "../assets";
    let assets_path = fs::canonicalize(assets_path).unwrap();

    match App::new(assets_path) {
        Ok(mut app) => app.run(),
        Err(error) => error!("Unable to run app, {:?}", error),
    }
}
