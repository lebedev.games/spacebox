use std::collections::HashMap;
use std::path::PathBuf;
use std::rc::Rc;

use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PropPrefab {
    pub key: String,
    pub barrier: String,
    pub obstacle: String,
}

pub struct MyCoordinates {}

pub struct Prop {
    pub id: usize,
}

pub struct Barrier {
    pub id: usize,
    pub position: MyCoordinates,
}

pub struct State {
    pub prop_prefab_fallback: PropPrefab,
    pub prop_prefabs: HashMap<String, PropPrefab>,
    pub props: HashMap<usize, Prop>,
    pub barriers: HashMap<usize, Barrier>,
}
