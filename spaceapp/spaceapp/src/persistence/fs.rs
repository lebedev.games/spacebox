use std::fmt::Debug;
use std::fs;
use std::fs::File;
use std::path::{Display, Path, PathBuf};

use serde::de::DeserializeOwned;
use serde::Serialize;

#[derive(Debug)]
pub enum DataError {
    FileNotFound(String),
    Io(std::io::Error),
    Yaml {
        path: String,
        error: serde_yaml::Error,
    },
}

impl From<std::io::Error> for DataError {
    fn from(error: std::io::Error) -> Self {
        Self::Io(error)
    }
}

pub struct DataPath {
    pub base: PathBuf,
}

impl DataPath {
    pub fn display(&self) -> Display {
        self.base.display()
    }

    pub fn new<P>(base: P) -> Self
    where
        P: AsRef<Path>,
    {
        Self {
            base: base.as_ref().to_path_buf(),
        }
    }

    pub fn join<P>(&self, path: P) -> Self
    where
        P: AsRef<Path>,
    {
        Self {
            base: self.base.join(path),
        }
    }

    pub fn write<T, P>(&self, path: P, value: T) -> Result<(), DataError>
    where
        T: Serialize + Debug,
        P: AsRef<Path>,
    {
        let path = self.base.join(path);
        if let Some(dir) = path.parent() {
            fs::create_dir_all(dir)?;
        }
        if let Err(error) = write_file(&path, value) {
            error!(
                "Unable to write {} to {}, {:?}",
                std::any::type_name::<T>(),
                path.display(),
                error
            );
            Err(error)
        } else {
            Ok(())
        }
    }
}

pub fn read_file_quietly<T, P>(path: P) -> T
where
    T: DeserializeOwned + Default,
    P: AsRef<Path>,
{
    match read_file(&path) {
        Ok(value) => value,
        Err(error) => {
            error!(
                "Unable to read file {}, {:?}",
                path.as_ref().display(),
                error
            );
            T::default()
        }
    }
}

pub fn read_file<T, P>(path: P) -> Result<T, DataError>
where
    T: DeserializeOwned,
    P: AsRef<Path>,
{
    let file = match File::open(&path) {
        Ok(file) => file,
        Err(error) => {
            error!(
                "Unable to read {} from {}, {:?}",
                std::any::type_name::<T>(),
                path.as_ref().display(),
                error
            );
            return Err(DataError::FileNotFound(path.as_ref().display().to_string()));
        }
    };
    let data = serde_yaml::from_reader(file).map_err(|error| DataError::Yaml {
        path: path.as_ref().display().to_string(),
        error,
    })?;
    Ok(data)
}

pub fn write_file<P, T>(path: P, value: T) -> Result<(), DataError>
where
    T: Serialize + Debug,
    P: AsRef<Path>,
{
    let file = File::create(&path)?;
    Ok(
        serde_yaml::to_writer(file, &value).map_err(|error| DataError::Yaml {
            path: path.as_ref().display().to_string(),
            error,
        })?,
    )
}
