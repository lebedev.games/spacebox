use serde::{Deserialize, Serialize};
use spacebox::game::{Message, PlayerId, PlayerRole};
use std::convert::TryFrom;
use std::fmt::Debug;
use std::io::{Error, Read, Write};
use std::net::{AddrParseError, SocketAddr, TcpStream, ToSocketAddrs};
use std::sync::atomic::AtomicBool;
use std::sync::mpsc::{channel, Receiver, RecvTimeoutError, Sender, TryRecvError};
use std::sync::Arc;
use std::time::{Duration, Instant};
use std::{io, thread};

pub use spacebox::game::{Appearance, Disappearance, Event, Navigation, Physics};

#[derive(Debug, Clone, Deserialize)]
#[serde(tag = "$type")]
pub enum Inbound {
    Events { events: Vec<Event> },
    Heartbeat,
}

#[derive(Debug, Clone, Serialize)]
#[serde(tag = "$type")]
pub enum Outbound {
    Authorization { player: PlayerId, role: PlayerRole },
    Message { message: Message },
    Heartbeat,
}

pub struct Client {
    address: SocketAddr,
    connection: Option<Connection>,
}

#[derive(Debug)]
pub enum ClientError {
    Address(AddrParseError),
}

impl From<AddrParseError> for ClientError {
    fn from(error: AddrParseError) -> Self {
        Self::Address(error)
    }
}

#[derive(Debug)]
struct Connection {
    events: Receiver<Inbound>,
    messages: Sender<Outbound>,
    receiving: Arc<Instant>,
    sending: Arc<Instant>,
}

impl Client {
    pub fn new(address: &str) -> Result<Client, ClientError> {
        let address: SocketAddr = address.parse()?;
        Ok(Client {
            address,
            connection: None,
        })
    }

    pub fn reset(&mut self) {
        self.connection = None;
    }

    pub fn connect(&mut self, player: String) {
        let address = self.address;

        let (events_sender, events_receiver) = channel();
        let (message_sender, message_receiver) = channel();

        let connection_id = Instant::now();
        let receiving = Arc::new(connection_id);
        let sending = Arc::new(connection_id);

        let receiving_thread_id = receiving.clone();
        let sending_thread_id = sending.clone();
        thread::spawn(move || {
            info!("Begin game server connection thread");
            info!("Open game server connection to {}", address);
            let stream = TcpStream::connect(address).unwrap();
            let reader = stream.try_clone().unwrap();
            let writer = stream;

            let mut receiver = SyncEventsReceiver { reader };
            let mut sender = SyncMessageSender { writer };

            info!("Begin player {} authorization", player);
            sender.send_message(Outbound::Authorization {
                player: PlayerId(player.clone()),
                role: PlayerRole::Editor,
            });
            info!("End player {} authorization", player);

            thread::spawn(move || {
                info!("Begin message sending thread id={:?}", sending_thread_id);
                let timeout = Duration::from_millis(200);
                loop {
                    let message = match message_receiver.recv_timeout(timeout) {
                        Ok(message) => message,
                        Err(error) => match error {
                            RecvTimeoutError::Timeout => Outbound::Heartbeat,
                            RecvTimeoutError::Disconnected => break,
                        },
                    };
                    if let None = sender.send_message(message) {
                        break;
                    }
                }
                info!("End message sending thread id={:?}", sending_thread_id);
            });

            info!("Begin event receiving thread id={:?}", receiving_thread_id);
            while let Some(events) = receiver.receive_events() {
                if events_sender.send(events).is_err() {
                    break;
                }
            }
            info!("End event receiving thread id={:?}", receiving_thread_id);
            info!("End game server connection thread");
        });

        self.connection = Some(Connection {
            events: events_receiver,
            messages: message_sender,
            receiving,
            sending,
        });
    }

    pub fn disconnect(&mut self) {
        self.connection = None;
    }

    pub fn is_connection_lost(&self) -> bool {
        if let Some(connection) = self.connection.as_ref() {
            if Arc::strong_count(&connection.sending) == 1
                || Arc::strong_count(&connection.receiving) == 1
            {
                return true;
            }
        }

        false
    }

    pub fn poll_events(&self) -> Vec<Event> {
        if let Some(connection) = self.connection.as_ref() {
            if let Ok(inbound) = connection.events.try_recv() {
                return match inbound {
                    Inbound::Events { events } => events,
                    Inbound::Heartbeat => vec![],
                };
            }
        }

        vec![]
    }
}

pub struct SyncEventsReceiver {
    pub reader: TcpStream,
}

impl SyncEventsReceiver {
    pub fn receive_events(&mut self) -> Option<Inbound> {
        let mut header_buffer = [0_u8; 2];
        if let Err(error) = self.reader.read_exact(&mut header_buffer) {
            error!("Unable to receive events because of header read, {}", error);
            return None;
        }
        let body_length = u16::from_be_bytes(header_buffer) as usize;

        let mut body_buffer = vec![0; body_length];
        if let Err(error) = self.reader.read_exact(body_buffer.as_mut_slice()) {
            error!("Unable to receive events because of body read, {}", error);
            return None;
        }
        let events = match serde_json::from_slice(body_buffer.as_slice()) {
            Ok(events) => events,
            Err(error) => {
                error!(
                    "Unable to receive events because of deserialization, {}",
                    error
                );
                return None;
            }
        };

        Some(events)
    }
}

pub struct SyncMessageSender {
    pub writer: TcpStream,
}

impl SyncMessageSender {
    pub fn send_message(&mut self, outbound: Outbound) -> Option<()> {
        match serde_json::to_vec(&outbound) {
            Ok(body) => {
                let header = match u16::try_from(body.len()) {
                    Ok(body_length) => body_length.to_be_bytes(),
                    Err(error) => {
                        error!("Unable to send message because of body length {}", error);
                        return None;
                    }
                };
                if let Err(error) = self.writer.write_all(&header) {
                    error!("Unable to send message because of header write, {}", error);
                    return None;
                }
                if let Err(error) = self.writer.write_all(&body) {
                    error!("Unable to send message because of body write, {}", error);
                    return None;
                }
            }
            Err(error) => {
                error!("Unable to send message because of serialization, {}", error)
            }
        }

        return Some(());
    }
}
